"use strict";
const path = require('path');
/**
 * android翻译配置文件
 *
 * language 对应表格中的语言设置
 * values 对应Android中需要转换的语言
 * locale 对应rn需要转换的语言
 *
 * @author nichenjian
 * @date 2018-07-07
 */
module.exports = {
    email :'xumingliu@clubfactory.com',
    password : 'googlejldh766',
    grantType : 'password',
    projectId: "5d98d416-7240-4473-bf62-d07687c9253c", // 翻译平台中的项目id
    clientId: "5824176d-6d70-47fc-8e76-0b8fc72ad718", // 在翻译平台点击'API Keys' -> 'Add API client'获得id和secret
    clientSecret: "sOK94biy1Z6uz5YfdCSsI5KBkflTTlfQ",
    mainLanCode:'zh',//参照语言
    locales: ["en"], // 语言代码，如en, ar
    isHasSpecialCase:false,
    format:'xml', //翻译的文件格式
    fileType:'.xml', //翻译的文件类型
    translateFilePath: path.join(__dirname, '../') + 'app/src/main/res/values/strings.xml',
    outputDir:path.join(__dirname, '../') + 'app/src/main/res/',
    fileName:"strings.xml", //如果是通用的文件名 优先使用lanconfig的fileName名字
    isNeedFormat:false,
    isJsonFile:false,
    isXmlFile:true,
    lanConfig:()=>{
        return [
            {
                "language": "en",
                "values": "values-en",
                "locale": "en",
                "fileName":""
            },
            {
                "language": "zh(China)",
                "values": "values-zh",
                "locale": "zh",
                "fileName":""
            }
        ]
    }
};
