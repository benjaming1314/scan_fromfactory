
const config =require('./config')

module.exports = {
  projects: [{
    projectId: config.projectId, // 翻译平台中的项目id
    clientId: config.clientId, // 在翻译平台点击'API Keys' -> 'Add API client'获得id和secret
    clientSecret: config.clientSecret,
  }], // 支持多项目合并导出，各项目需要分别添加API keys
  locales: config.locales, // 语言代码，如en, ar
  format: config.format, // 静态文件格式，如csv, json, yaml, strings, properties, xliff12, po
  export_path: "/src", // 导入文件路径，如/source/locales
  filter: 1, // 可选，默认为0。1: 过滤空翻译
}
