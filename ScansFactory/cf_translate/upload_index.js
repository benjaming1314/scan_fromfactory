let Translate = require("./translate");

/**
 *获取生成其他翻译文件 首次获取其他翻译文件  src对应语言目录下 并上传 首次上传 ；
 */
//new Translate().uploadOtherTranslateFile();

/**
 * 获取待翻译的文件 并上传到翻译平台  src/en en.json
 */
new Translate().uploadTranslateFile();
