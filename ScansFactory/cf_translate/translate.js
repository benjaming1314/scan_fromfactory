'use strict';

const exec = require('child_process').exec;

/**
 * 写文件
 */
const writeFile = require('write');
const fs = require('fs');
const path = require('path');

const CHARSET = 'utf-8';
/**
 * xml格式化模块
 */
const xmlFormatter = require('xml-formatter')
const config = require('./config.js')
const fastXmlParser = require("fast-xml-parser");
// fast-xml-parser 配置
const options = {
    ignoreAttributes: false
};

const Utils = require("./utils")
const HttpRequest = require("./HttpRequest")

class Translate {
    /**
     * 更新翻译文件
     */
    updateTranslate () {

        new Utils().checkPathExist({
            path:path.join(__dirname, '/') + 'src/',
            isSync:false,
        });
        //先下载
        this.downloadFile({
            onSuccess () {
                //根据语言配置 逐一替换工程中xml文件
                for (let language of config.lanConfig()) {
                    //替换本地文件
                    let locale = language.locale;
                    //读取下载的xml文件
                    let newFilePath = path.join(__dirname, '/') + 'src/' + locale + config.fileType;
                    if(config.isHasSpecialCase){
                        //特殊处理
                        if (locale === 'hi') {
                            newFilePath = path.join(__dirname, '/') + 'src/hi_IN'+config.fileType
                        }
                        if (locale === 'en') {
                            newFilePath = path.join(__dirname, '/') + 'src/en_US'+config.fileType
                        }
                    }
                    const isExist = fs.existsSync(newFilePath);
                    console.log(newFilePath);
                    if(!isExist) {
                        continue;
                    }
                    const isValidFromFile =  new Utils().isValidFromFile(newFilePath);
                    if (isValidFromFile){
                        continue;
                    }
                    const data = fs.readFileSync(newFilePath, CHARSET);
                    let filePath = "";
                    let outputDir = config.outputDir;
                    let values = language.values;
                    let fileName = language.fileName;
                    if (fileName.length === 0){
                        fileName = config.fileName
                    }
                    if (values.length > 0){
                        filePath = outputDir + language.values + '/' + fileName;
                    }else {
                        filePath = outputDir + fileName;
                    }
                    if(config.isNeedFormat){
                        if (config.isXmlFile){
                            writeFile(filePath, xmlFormatter(xml))
                        }else if (config.isJsonFile) {
                            writeFile(filePath, new Utils().formatJson(data))
                        }else {
                            writeFile(filePath, data)
                        }
                    }else {
                        writeFile(filePath, data)
                    }
                }
            },
            onFailed () {
            },
        })
    }

    /**
     * 下载翻译文件
     * @param params
     */
    downloadFile (params) {
        try {
            let cmdStr = 'npx tran export';
            console.log(cmdStr);
            exec(cmdStr, function (err, stdout, stderr) {
                if (err) {
                    params.onFailed();
                    console.log('downloadFile error:' + stderr)
                } else {
                    params.onSuccess();
                    console.log('downloadFile success')
                }
            })
        } catch (e) {
            params.onFailed();
            console.log('downloadFile failed ', e)
        }
    }

    /**
     * 上传待翻译文件
     */
    uploadTranslateFile(){
        let filePath = config.translateFilePath;

        const outputDir = path.join(__dirname, '/') + 'src/resource/';
        new Utils().checkPathExist({
            path:outputDir,
            isSync:false,
        });
        if (config.isXmlFile){
            const xml = fs.readFileSync(filePath, CHARSET);
            const json = fastXmlParser.parse(xml, options);
            let jsonObject = {};
            //先忽略string-array
            for (let item of json.resources.string) {
                if (item["@_translatable"] !== "false") {
                    jsonObject[item['@_name']] = item['#text']
                }
            }
            // noinspection JSCheckFunctionSignatures
            writeFile(outputDir+'resource.json', JSON.stringify(jsonObject));
        }else if (config.isJsonFile) {
            new Utils().copyFile(filePath,outputDir+'resource.json',true)
        }else {
            console.log('not support the format'+config.format);
            return
        }
        let httpRequest = new HttpRequest();
        httpRequest.login({
            onSuceess(data) {
                httpRequest.uploadsTranslateFile({
                    locale: config.mainLanCode,
                    token: data.data.accessToken,
                    format: "jsonflat",
                    file:fs.createReadStream(outputDir+'resource.json'),
                    onSuceess(data) {
                        console.log(data)
                    },
                    onFailed(msg) {
                    }
                })
            },
            onFailed(msg) {

            }
        })

    }

    /**
     * 获取其他语言的文件
     */
    uploadOtherTranslateFile() {
        //获取本地各语言文件并转正json文件保存到src下
        for (let language of config.lanConfig()) {
            let filePath = "";
            let outputDir = config.outputDir;
            let values = language.values;
            let fileName = language.fileName;
            if (fileName.length === 0){
                fileName = config.fileName
            }
            if (values.length > 0){
                filePath = outputDir + language.values + '/' + fileName;
            }else {
                filePath = outputDir + fileName;
            }
            const isExist = fs.existsSync(filePath);
            console.log(filePath);
            if(!isExist) {
                continue;
            }
            const jsonFilePath = path.join(__dirname, '/') + 'src/'+language.locale +"/";
            new Utils().checkPathExist({
                path:jsonFilePath,
                isSync:false,
            });

            if (config.isXmlFile){
                const xml = fs.readFileSync(filePath, CHARSET);
                const json = fastXmlParser.parse(xml, options);
                let jsonObject = {};
                //先忽略string-array
                for (let item of json.resources.string) {
                    jsonObject[item['@_name']] = item['#text']
                }
                // noinspection JSCheckFunctionSignatures
                // noinspection JSCheckFunctionSignatures
                writeFile(jsonFilePath+language.locale +'.json', JSON.stringify(jsonObject))
            }else if (config.isJsonFile) {
                new Utils().copyFile(filePath,jsonFilePath+'resource.json',true)
            }else {
                console.log('not support the format'+config.format);
                return
            }

            let httpRequest = new HttpRequest();
            httpRequest.login({
                onSuceess(data) {
                    httpRequest.uploadsTranslateFile({
                        locale: language.locale,
                        token: data.data.accessToken,
                        format: "jsonflat",
                        file:fs.createReadStream(jsonFilePath+'resource.json'),
                        onSuceess(data) {
                            console.log(data)
                        },
                        onFailed(msg) {
                        }
                    })
                },
                onFailed(msg) {

                }
            })
        }
    }
}

module.exports = Translate
