'use strict'
const request = require('request')
const hostName = 'http://tr.yuceyi.com/'

const config =require('./config')

class HttpRequest {

    /**
     * 模拟登录 获取token
     * @param params
     */
    login (params) {
        let paramsBody = {}
        paramsBody.email = config.email
        paramsBody.password = config.password
        paramsBody.grantType = config.grantType
        request({
            url: hostName + 'api/v1/auth/token',
            method: 'POST',
            json: true,
            body: paramsBody,
        }, function (error, response, body) {
            if (!error && response.statusCode === 200) {
                params.onSuceess(body)
            } else {
                params.onFailed(body)
            }
        })
    }

    /**
     * 上传翻译文件
     * @param params
     */
    uploadsTranslateFile (params) {

        const form = {
            file:params.file
        }
        request({
            url: hostName + 'api/v1/projects/'+config.projectId+'/imports?locale=' + params.locale + '&format=' + params.format,
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': 'Bearer ' + params.token,
            },
            formData: form,
        }, function (error, response, body) {
            if (!error && response.statusCode === 200) {
                params.onSuceess(body)
            } else {
                console.log(error,body)
            }
        })
    }
}

module.exports = HttpRequest
