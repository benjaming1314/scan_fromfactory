'use strict';
const fs = require('fs');
const config = require('./config');

const fastXmlParser = require("fast-xml-parser");
// fast-xml-parser 配置
const options = {
    ignoreAttributes: false
};

const CHARSET = 'utf-8'

class Utils {

    checkPathExist(params) {
        try {
            const path = params.path;
            const isSync = params.isSync;
            if (isSync) {
                const isExist = fs.existsSync(path);
                if (!isExist) {
                    fs.mkdirSync(path, {recursive: true});
                }
            } else {
                fs.exists(path, function (exists) {
                    if (exists === false)
                        fs.mkdir(path, {recursive: true}, err => {
                            console.log('caught', err);
                        })
                    // fs.mkdirSync(path);
                });
            }
        } catch (e) {
            console.log('checkPathExist', JSON.stringify(params) + '\n' + e);
        }
    }

    isValidFromFile(path) {
        let format = config.format;
        if ('xml' === format) {
            const xml = fs.readFileSync(path, CHARSET);
            const json = fastXmlParser.parse(xml, options);
            return json.resources.length === 0;
        } else if ('jsonflat' === format) {
            var data = fs.readFileSync(path);
            return data.length === 0;
        } else {
            return false;
        }
    }

    copyFile(src, dist) {
        try {
            fs.writeFileSync(dist, fs.readFileSync(src));
        } catch (e) {
            console.log('checkPathExist', JSON.stringify(params) + '\n' + e);
        }
    }

    formatJson(data) {
        let newData = '{\n';
        let dataObject = JSON.parse(data);
        Object.keys(dataObject).forEach(function (key) {
            let dataObjectElement = dataObject[key];
            newData += '  \"' + key + '\":\"' + dataObjectElement + '\",' + '\n'
        });
        newData = newData.substring(0, newData.length - 2);
        newData += '\n}';
        return newData
    }
}

module.exports = Utils;
