package club.fromfactory.router;

import java.util.HashMap;

public interface IRouterMap {
    HashMap<String, PageInfo> init();
}
