package club.fromfactory.router;

import android.app.Activity;

import java.util.HashMap;
import java.util.Map;

/**
 * 页面的配置信息
 */
public class PageInfo {
    /**
     * class名称
     */
    private Class<? extends Activity> activityClass;

    /**
     * 设置的参数信息
     */
    private final Map<String, Class> params = new HashMap<>();

    public void addParam(String key, Class className) {
        params.put(key, className);
    }

    public void setActivityClass(Class<? extends Activity> activityClass) {
        this.activityClass = activityClass;
    }

    public Class<? extends Activity> getActivityClass() {
        return activityClass;
    }

    public Map<String, Class> getParams() {
        return params;
    }
}