package club.fromfactory.router;

import android.os.Bundle;

public interface IRouter<T> {
    void bind(T router, Bundle bundle);
}
