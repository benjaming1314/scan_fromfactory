package club.fromfactory.router;

import android.content.Context;
import android.net.Uri;

/**
 * Router Callback
 */
public interface IRouterCallback {
    /**
     * before open router callback
     *
     * @param context
     * @param uri     the current uri
     */
    void beforeOpenRouter(Context context, Uri uri);

    /**
     * after open router callback
     *
     * @param context
     * @param uri     the current uri
     */
    void afterOpenRouter(Context context, Uri uri);
}
