'use strict';
/**
 * 翻译模块
 *
 * @author nichenjian
 * @date 2018-07-07
 */

/**
 * json转换xml模块
 */
const json2XmlParser = require("fast-xml-parser").j2xParser;

/**
 * 写文件
 */
const writeFile = require("write");

const fs = require('fs');

const CHARSET = "utf-8";

const config = require("./config.js");

const beautify = require('js-beautify').js;

/**
 * xml格式化模块
 */
const xmlFormatter = require('xml-formatter');

/**
 * 读取tsv格式的翻译源文件
 */
const TranslateTsv = require("./translate_tsv.js");

/**
 * 语言配置
 */
const TranslateConfig = require("./language_config.js");

// fast-xml-parser 配置
const options = {
    ignoreAttributes: false
};

/**
 * parser
 */
let parser = new json2XmlParser(options);

// 删除反斜杠的正则
let removeBackSlantRegex = /\\/g;

// 添加反斜杠的正则
let addBackSlantRegex = /(["'])/g;

class Translate {

    /**
     * 格式化输出
     */
    formatConsole(message) {
        console.log('\x1b[33m%s\x1b[0m', message);
    }

    /**
     * 翻译入口函数
     *
     * @param inputDir 输入文件目录
     * @param outputDir 输出文件目录
     * @param fileName 文件名称
     */
    translate(inputDir, outputDir, fileName) {
        let languageArr = new TranslateConfig().transformXmlToJson(inputDir + fileName);
        let translateArr = new TranslateTsv().getTranslateArr();

        for (let index in languageArr) {
            const languageItem = languageArr[index];

            this.translateString(languageItem, index, translateArr);
            this.translateStringArray(languageItem, index, translateArr);
            let filePath = outputDir + languageItem.values + "/" + fileName;

            // 跳过英文的翻译
//            if (languageItem.language === "zh") {
//                continue;
//            }
            // noinspection JSCheckFunctionSignatures
            writeFile(filePath, xmlFormatter(parser.parse(languageItem.data)));
        }
    }

    /**
     * 翻译rn入口函数
     *
     * @param inputDir
     * @param outputDir
     * @param fileName
     */
    translateRn(inputDir, outputDir, fileName) {
        const translateArr = new TranslateTsv().getTranslateArr();
        const json = require("./src/en.js").default;
        // 从配置文件里读取语言的配置
        for (let i in config) {
            // 读取en.js的翻译，进行深拷贝
            let result = Object.assign({}, json);

            // 针对非英文的语言需要翻译（英文是基础对照的语言，不需要翻译）
            if (config[i].language !== "zh") {
                // 遍历en的翻译文案
                for (let key in result) {
                    let value = result[key];
                    let isTranslate = false;

                    // 遍历表格中的翻译
                    for (let index in translateArr) {
                        if (value === translateArr[index][0]) {
                            isTranslate = true;
                            let translate = translateArr[index][i];
                            if (!this.isTranslateEmpty(translate)) {
                                result[key] = translateArr[index][i];
                            }
                        }
                    }

                    // 打印未翻译的文案
                    if (i === "1" && !isTranslate) {
                        this.formatConsole(value);
                    }
                }

                let outputPath = outputDir + config[i].locale + ".js";
                // noinspection JSCheckFunctionSignatures
                fs.writeFileSync(outputPath, beautify("export default " + JSON.stringify(result)));
            }
        }
    }

    /**
     * 翻译字符串
     *
     * @param languageItem 待翻译的语言配置
     * @param index        索引
     * @param translateArr 翻译的文案
     */
    translateString(languageItem, index, translateArr) {
        let languageStr = languageItem.data.resources.string;
        let _this = this;
        // 遍历string 替换相应的文本
        languageStr.forEach(function (elem) {
            let isMatch = false;

            for (let translate of translateArr) {
                let result = _this.reallyTranslate(elem["#text"], translate[index], translate[0])
                if (result !== undefined) {
                    isMatch = true;
                    elem["#text"] = result;
                    break
                }
            }

            // 打印未添加到google doc的文案
            if (!isMatch && index == 0) {
                _this.formatConsole(elem["#text"]);
            }
        })
    }

    /**
     * 翻译是否为空
     *
     * @param translate 翻译文案
     * @return boolean 是否空
     */
    isTranslateEmpty(translate) {
        return translate === undefined || translate.trim() === ""
    }

    /**
     * 真实翻译
     *
     * @param elem 待翻译元素
     * @param translateTxt 翻译文案
     * @param enTxt 英文文案
     *
     * @return boolean 是否翻译
     */
    reallyTranslate(elem, translateTxt, enTxt) {
        let result = undefined;
        if (elem === enTxt) {
            result = this.isTranslateEmpty(translateTxt) ? enTxt : translateTxt;
            result = result.replace(addBackSlantRegex, '\\$1');
        } else if (elem.replace(removeBackSlantRegex, "") == enTxt) {
            let translate = (this.isTranslateEmpty(translateTxt) ? enTxt : translateTxt).replace(removeBackSlantRegex, "");
            result = translate.replace(addBackSlantRegex, '\\$1');
        }

        return result
    }

    /**
     * 翻译字符串数组
     *
     * @param languageItem 待翻译的语言配置
     * @param index        索引
     * @param translateArr 翻译的文案
     */
    translateStringArray(languageItem, index, translateArr) {
        let languageStrArr = languageItem.data.resources["string-array"];
        let _this = this;

        // 遍历string-array 替换相应的文本
        if (languageStrArr != null) {
            let stringArray = [];
            if (!(languageStrArr instanceof Array)) {
                stringArray.push(languageStrArr);
            }

            stringArray.forEach(function (arr) {
                for (let i in arr["item"]) {
                    let isMatch = false;
                    for (let translate of translateArr) {
                        let result = _this.reallyTranslate(arr["item"][i], translate[index], translate[0]);
                        if (result !== undefined) {
                            isMatch = true;
                            arr["item"][i] = result;
                            break
                        }
                    }

                    // 打印未添加到google doc的文案
                    if (!isMatch && index === 0) {
                        _this.formatConsole(arr["item"][i]);
                    }
                }
            })
        }
    }
}

module.exports = Translate;
