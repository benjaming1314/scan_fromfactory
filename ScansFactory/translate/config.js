"use strict";

/**
 * android翻译配置文件
 *
 * language 对应表格中的语言设置
 * values 对应Android中需要转换的语言
 * locale 对应rn需要转换的语言
 *
 * @author nichenjian
 * @date 2018-07-07
 */
module.exports = [
    {
        "language": "zh",
        "values": "values-zh",
        "locale": "zh"
    },
    {
        "language": "en",
        "values": "values-en",
        "locale": "en"
    },
    {
        "language": "it(Italian)",
        "values": "values-it",
        "locale": "it"
    },
    {
        "language": "es-MX(Mexico)",
        "values": "values-es",
        "locale": "es"
    },
    {
        "language": "pt-BR(Brazil)",
        "values": "values-pt",
        "locale": "pt"
    },
    {
        "language": "id(Indonesia)",
        "values": "values-in",
        "locale": "id"
    },
    {
        "language": "ar(AE BH EG KW......)",
        "values": "values-ar",
        "locale": "ar"
    },
    {
        "language": "hi(India)",
        "values": "values-hi",
        "locale": "hi"
    }
];