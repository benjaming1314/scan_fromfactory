'use strict';
/**
 * xml转换json辅助类
 *
 * @author nichenjian
 * @date 2018-07-07
 */
const config = require("./config.js");
const fs = require('fs');
const fastXmlParser = require("fast-xml-parser");

// fast-xml-parser 配置
const options = {
    ignoreAttributes: false
};

const CHARSET = "utf-8";

class TransformUtil {
    /**
     * 将xml文件转换成json
     *
     * @param srcXmlFile
     * @return 转换的json数据
     */
    transformXmlToJson(srcXmlFile) {
        const xml = fs.readFileSync(srcXmlFile, CHARSET);
        const json = fastXmlParser.parse(xml, options);
        const data = JSON.parse(JSON.stringify(json));

        const stringArr = [];
        data.resources.string.forEach(function (elem) {
            // 过滤不需要翻译的文案
            if (elem["@_translatable"] !== "false") {
                stringArr.push(elem)
            }
        });

        data.resources.string = stringArr;

        const languageArr = [];
        for (let language of config) {
            languageArr.push({
                "language": language.language,
                "values": language.values,
                "data": JSON.parse(JSON.stringify(data))
            });
        }

        return languageArr;
    }
}


module.exports = TransformUtil;
