'use strict';

/**
 * 翻译入口
 *
 * @author nichenjian
 * @date 2018-07-08
 */

let Translate = require("./translate.js");

// 翻译app模块strings.xml的文本
new Translate().translate("../app/src/main/res/values/", "../app/src/main/res/", "strings.xml");

// 翻译baselibrary模块strings.xml的文本
// new Translate().translate("../baselibrary/src/main/res/values/", "../baselibrary/src/main/res/", "strings.xml");

// 翻译rn的文本
// new Translate().translateRn("../../src/res/locales/", "../../src/res/locales/", "en.js");