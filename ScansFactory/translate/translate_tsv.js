/**
 * 读取翻译asv文件
 *
 * @author nichenjian
 * @date 2018-07-06
 */
'use strict';

const fs = require('fs');

/**
 * 翻译的源文件
 * 需要将下载的tsv文件重命名到src/translate.txt文件下
 */
const TRANSLATE_SRC_FILE = "src/translate.txt";
const languageSize = require("./config.js").length;
const CHARSET = "utf-8";

class ReadTranslateTsv {
    /**
     * 读取翻译的文本，转换成二维数组
     */
     getTranslateArr() {
        const translateStrings = fs.readFileSync(TRANSLATE_SRC_FILE, CHARSET);
        const translateArr = translateStrings.split("\n");
        const translateArrList = [];
        const _this = this;
        translateArr.forEach(function(elem) {
            const lineArr = elem.split("\t");
            if (_this.isLineValid(lineArr)) {
                translateArrList.push(lineArr.splice(2, languageSize));
            }
        });

        return translateArrList;
    }

    /**
     * 行是否合法
     *
     * @param lineArr 字符串数组
     */
     isLineValid(lineArr) {
         // 过滤第一行和空行
         if (lineArr[1] === 'ID' || lineArr[2] === '' || lineArr[2] === undefined) {
            return false
         }

         return true
     }
}

module.exports = ReadTranslateTsv;
