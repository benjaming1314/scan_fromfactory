## 翻译

- 安装node和npm

- 安装依赖
    - 进入`translate`目录，执行`npm install`

### 获取翻译文案

- 从`google doc`上下载翻译文件，翻译地址: `https//docs.google.com/spreadsheets/d/1NwTCLZXwXdP49j9oaZymdSLzLnHTZDipqKa9_zZHDiI/edit#gid=1902208321`

- 选择 文件 -> 下载为 -> 制表符分隔值(tsv)

- 将下载的文件保存到 `src/translate.txt`　文件

### 执行翻译脚本

- 打开终端，执行 `node index.js`，命令行中会输出未匹配成功的翻译，请关注
