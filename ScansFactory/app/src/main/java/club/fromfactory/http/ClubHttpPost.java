//package club.fromfactory.http;
//
//import com.google.gson.reflect.TypeToken;
//
//import org.json.JSONObject;
//
//import java.util.HashMap;
//import java.util.Iterator;
//import java.util.Map;
//
//import club.fromfactory.baselibrary.language.LanguageUtils;
//import club.fromfactory.baselibrary.utils.PreferenceStorageUtils;
//import club.fromfactory.utils.Zlog;
//import club.fromfactory.constant.Constants;
//import club.fromfactory.okhttp.OkHttpUtil;
//
///**
// * Created by lxm on 2016/11/9.
// */
//
//public class ClubHttpPost {
//
//    private static ClubHttpPost httpPost;
//
//    public static ClubHttpPost getInstance() {
//
//        if (httpPost == null) {
//            httpPost = new ClubHttpPost();
//        }
//        return httpPost;
//    }
//
//    /*
//     * 取消请求
//     */
//    public void clearTags(String url) {
//        OkHttpUtil.getInstance().cancelTag(url);
//    }
//
//
//    private String getCookieString(Map<String, String> cookieMap) {
//
//
//        StringBuilder cookieHeader = new StringBuilder();
//
//        Iterator<String> iterator = cookieMap.keySet().iterator();
//
//
//        while (iterator.hasNext()) {
//
//            String next = iterator.next();
//            cookieHeader.append(next).append('=').append(cookieMap.get(next));
//
//            if (iterator.hasNext()) {
//                cookieHeader.append(";");
//            }
//        }
//        return cookieHeader.toString();
//    }
//
//    private Map<String, String> getCommonCookie(Map<String, String> cookieMap) {
//
//        if (cookieMap == null) {
//            cookieMap = new HashMap<>();
//        }
//        String language = LanguageUtils.getLanguageCode();
//        cookieMap.put("warehouse", Constants.SERVER_LOCATION);
//        cookieMap.put("language", language);
//        Zlog.ii("lxm cookie : " + language);
//
//        return cookieMap;
//
//    }
//
//    private Map<String, String> getCommonCookie() {
//
//        Map<String, String> cookieMap = new HashMap<>();
//        String language = LanguageUtils.getLanguageCode();
//        cookieMap.put("warehouse", Constants.SERVER_LOCATION);
//        cookieMap.put("language", language);
//        Zlog.ii("lxm cookie : " + language);
//
//        return cookieMap;
//
//    }
//
//
//    public synchronized <T> void doPostHttpPost(String url, Map<String, Object> params,
//                                                int returnType, TypeToken<T> typeToken, HttpRequestResultListner httpRequestResultListner) {// TODO
//
//
//        Map<String, String> headerMap = new HashMap<>();
//        String token = PreferenceStorageUtils.getInstance().getToken();
//        Zlog.ii("lxm header : " + token);
//        headerMap.put("Authorization", token);
//        headerMap.put("token", token);
//
//        OkHttpListenerInterface okHttpListenerInterface = new OkHttpListenerInterface(returnType, typeToken, url, System.currentTimeMillis(), httpRequestResultListner);
//        JSONObject jsonObject = new JSONObject(params);
//        OkHttpUtil.getInstance().httpPost(url, headerMap, jsonObject.toString(), getCookieString(getCommonCookie()), okHttpListenerInterface.mStringCallback);
//
//    }
//
//    public synchronized <T> void doGetHttp(String url,
//                                           int returnType, TypeToken<T> typeToken, HttpRequestResultListner httpRequestResultListner) {
//
//        Map<String, String> headerMap = new HashMap<>();
//        String token = PreferenceStorageUtils.getInstance().getToken();
//        Zlog.ii("lxm header : " + token);
//        headerMap.put("Authorization", token);
//        headerMap.put("token", token);
//        Map<String, String> params = new HashMap<>();
//
//        OkHttpListenerInterface okHttpListenerInterface = new OkHttpListenerInterface(returnType, typeToken, url, System.currentTimeMillis(), httpRequestResultListner);
//        OkHttpUtil.getInstance().httpGet(url, headerMap, params, getCookieString(getCommonCookie()), okHttpListenerInterface.mStringCallback);
//
//    }
//
//    public synchronized <T> void doGetHttp(String url, Map<String, String> params,
//                                           int returnType, TypeToken<T> typeToken, HttpRequestResultListner httpRequestResultListner) {
//
//        Map<String, String> headerMap = new HashMap<>();
//        String token = PreferenceStorageUtils.getInstance().getToken();
//        Zlog.ii("lxm header : " + token);
//        headerMap.put("Authorization", token);
//        headerMap.put("token", token);
//        if (params == null) {
//            params = new HashMap<>();
//        }
//
//        OkHttpListenerInterface okHttpListenerInterface = new OkHttpListenerInterface(returnType, typeToken, url, System.currentTimeMillis(), httpRequestResultListner);
//        OkHttpUtil.getInstance().httpGet(url, headerMap, params, getCookieString(getCommonCookie()), okHttpListenerInterface.mStringCallback);
//
//    }
//
//    public synchronized <T> void doPatchHttp(String url, Map<String, Object> params,
//                                             int returnType, TypeToken<T> typeToken, HttpRequestResultListner httpRequestResultListner) {
//        Map<String, String> headerMap = new HashMap<>();
//        String token = PreferenceStorageUtils.getInstance().getToken();
//        Zlog.ii("lxm header : " + token);
//        headerMap.put("Authorization", token);
//        headerMap.put("token", token);
//        if (params == null) {
//            params = new HashMap<>();
//        }
//        JSONObject jsonObject = new JSONObject(params);
//        OkHttpListenerInterface okHttpListenerInterface = new OkHttpListenerInterface(returnType, typeToken, url, System.currentTimeMillis(), httpRequestResultListner);
//        OkHttpUtil.getInstance().httpPatch(url, headerMap, jsonObject.toString(), getCookieString(getCommonCookie()), okHttpListenerInterface.mStringCallback);
//    }
//
//    public synchronized <T> void doPutHttp(String url, Map<String, Object> params,
//                                           int returnType, TypeToken<T> typeToken, HttpRequestResultListner httpRequestResultListner) {
//
//        Map<String, String> headerMap = new HashMap<>();
//        String token = PreferenceStorageUtils.getInstance().getToken();
//        Zlog.ii("lxm header : " + token);
//        headerMap.put("Authorization", token);
//        headerMap.put("token", token);
//        if (params == null) {
//            params = new HashMap<>();
//        }
//        JSONObject jsonObject = new JSONObject(params);
//        OkHttpListenerInterface okHttpListenerInterface = new OkHttpListenerInterface(returnType, typeToken, url, System.currentTimeMillis(), httpRequestResultListner);
//        OkHttpUtil.getInstance().httpPut(url, headerMap, jsonObject.toString(), getCookieString(getCommonCookie()), okHttpListenerInterface.mStringCallback);
//    }
//
//    public synchronized <T> void doDeleteHttp(String url, Map<String, Object> params,
//                                              int returnType, TypeToken<T> typeToken, HttpRequestResultListner httpRequestResultListner) {
//
//        Map<String, String> headerMap = new HashMap<>();
//        String token = PreferenceStorageUtils.getInstance().getToken();
//        Zlog.ii("lxm header : " + token);
//        headerMap.put("Authorization", token);
//        headerMap.put("token", token);
//        if (params == null) {
//            params = new HashMap<>();
//        }
//        JSONObject jsonObject = new JSONObject(params);
//        OkHttpListenerInterface okHttpListenerInterface = new OkHttpListenerInterface(returnType, typeToken, url, System.currentTimeMillis(), httpRequestResultListner);
//        OkHttpUtil.getInstance().httpDelete(url, headerMap, jsonObject.toString(), getCookieString(getCommonCookie()), okHttpListenerInterface.mStringCallback);
//    }
//
//}
