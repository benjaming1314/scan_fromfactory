//package club.fromfactory.http;
//
//import android.os.Handler;
//import android.os.Looper;
//import android.text.TextUtils;
//
//import com.google.gson.reflect.TypeToken;
//
//import club.fromfactory.FFApplication;
//import club.fromfactory.utils.GsonUtils;
//import club.fromfactory.utils.Zlog;
//import club.fromfactory.baselibrary.utils.StringUtils;
//import club.fromfactory.okhttp.callback.StringCallback;
//import club.fromfactory.http.model.HttpJsonErrorResult;
//import okhttp3.Call;
//
///**
// * Created by lxm on 2016/11/9.
// */
//
//public class OkHttpListenerInterface<T> {
//
//    private String failedPrompt = "Server connection failure";
//    private int returnType; // 是否需要进一步解析
//    private TypeToken<T> typeToken;// 返回数据类型
//    private String url;// 请求地址
//
//    private HttpRequestResultListner mHttpRequestResultListner ;
//
//    public OkHttpListenerInterface(int returnType,
//                                   TypeToken<T> typeToken, String url, long begin,HttpRequestResultListner mHttpRequestResultListner) {
//
//        OkHttpListenerInterface.this.returnType = returnType;
//        OkHttpListenerInterface.this.typeToken = typeToken;
//        OkHttpListenerInterface.this.url = url;
//        OkHttpListenerInterface.this.mHttpRequestResultListner = mHttpRequestResultListner;
//    }
//
//    private OkHttpListenerInterface() {
//        super();
//    }
//
//    public StringCallback mStringCallback = new StringCallback() {
//        @Override
//        public void onError(final Call call, final Exception e) {
//            if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
//                Zlog.ii("lxm ss okhttp Thread:Main Thread");
//            } else {
//                Zlog.ii("lxm ss okhttp Thread:Not Main Thread");
//            }
//            FFApplication.getCacheThreadExecutor().execute(new Runnable() {
//                @Override
//                public void run() {
//                    Zlog.ii("lxm okhttp onErrorResponse:"+ url+  e.getMessage());
//                    String errorMessege = null;
//                    try {
//                        TypeToken<HttpJsonErrorResult> token = new TypeToken<HttpJsonErrorResult>() {
//                        };
//                        HttpJsonErrorResult httpJsonErrorResult = (HttpJsonErrorResult) GsonUtils.getInstance().parseJson(e.getMessage(), token);
//                        if (httpJsonErrorResult != null) {
//                            errorMessege = httpJsonErrorResult.getMessage();
//                        }
//                    } catch (Exception e1) {
//                        e1.printStackTrace();
//                    }
//                    if (StringUtils.isNull(errorMessege)) {
//                        errorMessege = e.getMessage();
//                    }
//                    onFailed(call.hashCode(),errorMessege);
//                }
//            });
//
//        }
//
//        @Override
//        public void onResponse(final String response) {
//            if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
//                Zlog.ii("lxm ss okhttp Thread:Main Thread");
//            } else {
//                Zlog.ii("lxm ss okhttp Thread:Not Main Thread");
//            }
//            FFApplication.getCacheThreadExecutor().execute(new Runnable() {
//                @Override
//                public void run() {
//                    if (response != null) {
//                        Zlog.ii("lxm okhttp onResponse:"+ url + "  "+  response);
//                        initJson(response);
//                    }else {
//                        Zlog.ii("lxm okhttp onResponse:null" + url);
//                        onFailed(400,failedPrompt);
//                    }
//                }
//            });
//
//        }
//    };
//
//    /**
//     * 解析
//     *
//     */
//    private synchronized void initJson(String response) {
//        Zlog.ii("lxm httppost initJson 1:" + response);
//
//        Object obj = null;
//        if (TextUtils.isEmpty(response)) {
//            Zlog.ii("lxm httppost initJson 4:" + response);
//            onFailed(400,failedPrompt);
//        } else {
//            Zlog.ii("lxm httppost  initJson 2:" + response);
//            switch (returnType) {
//                case RequestTypeConstant.RETURN_INITJSON_DATA: {
//                    Zlog.ii("lxm httppost  initJson 3:" + response);
//                    obj = parseJsonMessage(response);
//                    if (obj != null) {
//                        Zlog.ii("lxm httppost  initJson 4:" + obj);
//                        onSucceed(obj);
//                    } else {
//                        onFailed(400,failedPrompt);
//                    }
//                }
//                break;
//                case RequestTypeConstant.RETURN_JSON_MESSAGE: {
//                    Zlog.ii("lxm httppost  initJson 5:" + response);
//                        if (response != null) {
//                            onSucceed(response);
//                        } else {
//                            onFailed(400,failedPrompt);
//                        }
//                }
//                break;
//
//                default:
//                    break;
//            }
//
//        }
//    }
//
//    /**
//     * 解析返回json的message
//     *
//     * @return
//     */
//    private synchronized T parseJsonMessage(String message) {
//        Zlog.ii("lxm okhttp parseJsonMessage 1:"+ message);
//        if (message == null) {
//            return null;
//        }
//        T o = null;
//        try {
//            if (mHttpRequestResultListner != null) {
//                if (typeToken == null) {
//                    o = (T) message;
//                } else {
//                    o = (T) GsonUtils.getInstance().parseJson(message, typeToken);
//                }
//
//                Zlog.ii("lxm volley parseJsonMessage 2:" + o.toString());
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            Zlog.i("lxm okhttp parseJsonMessage Exception:" + url + e.getMessage());
//        }
//        return o;
//    }
//
//    private void onFailed(final int code, final String message) {
//        Handler handler = new Handler(Looper.getMainLooper());
//        handler.post(new Runnable() {
//            @Override
//            public void run() {
//
//                if (mHttpRequestResultListner != null) {
//                    mHttpRequestResultListner.requestFailed(message);
//                }
//            }
//        });
//    }
//
//    private void onSucceed( final Object obj) {
//        Handler handler = new Handler(Looper.getMainLooper());
//        handler.post(new Runnable() {
//            @Override
//            public void run() {
//                if (mHttpRequestResultListner != null) {
//                    mHttpRequestResultListner.requestSucceed(obj);
//                }
//            }
//        });
//    }
//
//}
