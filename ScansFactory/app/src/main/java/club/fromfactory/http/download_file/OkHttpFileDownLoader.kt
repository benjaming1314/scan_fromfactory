package club.fromfactory.http.download_file

import club.fromfactory.baselibrary.BuildConfig
import club.fromfactory.baselibrary.net.retrofit.BaseRetrofit
import club.fromfactory.baselibrary.utils.ThreadUtils
import club.fromfactory.baselibrary.view.IBaseView
import club.fromfactory.http.FileDownLoaderListener
import com.ihsanbal.logging.Level
import com.ihsanbal.logging.LoggingInterceptor
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import okio.Buffer
import okio.BufferedSink
import okio.Okio
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream
import java.util.concurrent.TimeUnit


class OkHttpFileDownLoader(var iBaseView: IBaseView, var path: String, var fileName: String, var mFileDownLoaderListener: FileDownLoaderListener) {

    private var disposable: Disposable? = null

    private var breakPoint: Long? = 0L

    fun startDownload(url: String, length: Long) {
        breakPoint = length
        BaseRetrofit
                .createService(IDownloadApi::class.java)
                .download(url)
                .doOnNext {
                    ThreadUtils.runOnIO {
                        saveApkFile(it)
//                        saveApkFile(it.byteStream(),it.contentLength(),length)
                    }
                }
                .subscribe(object : Observer<ResponseBody> {

                    override fun onSubscribe(d: Disposable) {
                        disposable = d
                    }

                    override fun onNext(it: ResponseBody) {
                    }

                    override fun onComplete() {
                    }

                    override fun onError(e: Throwable) {
                        mFileDownLoaderListener.onFail(e.hashCode(), e.message)
                    }

                })
    }

    private fun saveApkFile(responseBody: ResponseBody) {
        val file = File(path, fileName)

        var bufferedSink: BufferedSink? = null
        var currentLength = 0L
        try {
            val source = responseBody.source()
            bufferedSink = Okio.buffer(Okio.sink(file))
            val buffer = Buffer()
            while (!source.exhausted()) {
                val count = source.read(buffer, 1024)

                currentLength += count
                bufferedSink.write(buffer, count)

                //计算当前下载进度
                mFileDownLoaderListener.onDownloading((100 * currentLength / responseBody.contentLength()).toInt(), currentLength)
            }
            bufferedSink.flush()

            //下载完成，并返回保存的文件路径
            mFileDownLoaderListener.onSuccess()
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            try {
                bufferedSink?.close()
            } catch (e: Exception) {
            }
        }
        return
    }

    private fun saveApkFile(byteStream: InputStream, totalLength: Long, startsPoint: Long) {

        // 设置下载文件的保存位置
        val file = File(path, fileName)

        var os: FileOutputStream? = null
        var currentLength = if (startsPoint >= totalLength) 0 else startsPoint

        try {

            os = FileOutputStream(file)
            val data = ByteArray(1024)

            var len = 0
            while (true) {
                len = byteStream.read(data)

                if (len == -1) {
                    break
                }
                os.write(data, 0, len)
                currentLength += len
                //计算当前下载进度
                mFileDownLoaderListener.onDownloading((100 * currentLength / totalLength).toInt(), currentLength)
            }
            os.flush()
            //下载完成，并返回保存的文件路径
            mFileDownLoaderListener.onSuccess()
        } catch (e: IOException) {
            e.printStackTrace()
            mFileDownLoaderListener.onFail(-1, "exception")
        } finally {
            try {
                byteStream.close()
            } catch (e: Exception) {
            }
            try {
                os?.close()
            } catch (e: Exception) {
            }
        }
    }

    private val client: OkHttpClient
        get() = OkHttpClient.Builder()
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(LoggingInterceptor.Builder()
                        .loggable(BuildConfig.DEBUG)
                        .setLevel(Level.BODY)
                        .build())
                .addInterceptor(FileDownloadInterceptor(mFileDownLoaderListener, breakPoint
                        ?: 0)).build()


    fun cancelDownload() {

        disposable?.dispose()

    }


}