package club.fromfactory.http.model;

/**
 * Created by lxm on 2016/11/18.
 *
 * 用于解析数据
 */

public class HttpJsonErrorResult {

    private String message ;
    private String msg ;
    private boolean is_success ;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean is_success() {
        return is_success;
    }

    public void setIs_success(boolean is_success) {
        this.is_success = is_success;
    }

    @Override
    public String toString() {
        return "HttpJsonResult{" +
                "message='" + message + '\'' +
                ", is_success=" + is_success +
                '}';
    }
}
