package club.fromfactory.http.download_file

import club.fromfactory.baselibrary.utils.ThreadUtils
import club.fromfactory.http.FileDownLoaderListener
import okhttp3.MediaType
import okhttp3.ResponseBody
import okio.*
import java.io.IOException
import java.util.concurrent.TimeUnit


class ProgressResponseBody(var mFileDownLoaderListener: FileDownLoaderListener,var breakPonint: Long , var responseBody: ResponseBody) : ResponseBody() {

    private var bufferedSource: BufferedSource? = null

    override fun contentType(): MediaType? {
        return responseBody.contentType()
    }

    override fun contentLength(): Long {
        return responseBody.contentLength()
    }

    override fun source(): BufferedSource {
        if (bufferedSource == null) {
            bufferedSource = Okio.buffer(source(responseBody.source()))
        }
        return bufferedSource!!
    }

    private fun source(source: Source): Source {
        return object : ForwardingSource(source) {
             var bytesReaded: Long = if (breakPonint >= contentLength()) 0 else breakPonint
            @Throws(IOException::class)
            override fun read(sink: Buffer, byteCount: Long): Long {
                val bytesRead = super.read(sink, byteCount)
                bytesReaded += if (bytesRead == -1L) 0 else bytesRead
                //实时发送当前已读取的字节和总字节
                mFileDownLoaderListener.onDownloading((bytesReaded * 100 / contentLength()).toInt(),bytesReaded)

                if (bytesReaded == contentLength()){
                    ThreadUtils.runOnUi({ mFileDownLoaderListener.onSuccess() },500,TimeUnit.MILLISECONDS
                    )
                }
                return bytesRead
            }
        }
    }
}