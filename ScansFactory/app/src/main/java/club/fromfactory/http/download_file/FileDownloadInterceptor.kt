package club.fromfactory.http.download_file

import club.fromfactory.baselibrary.net.retrofit.BaseInterceptor
import club.fromfactory.http.FileDownLoaderListener
import okhttp3.Interceptor
import okhttp3.Response

class FileDownloadInterceptor(var mFileDownLoaderListener: FileDownLoaderListener,var breakPoint :Long) : BaseInterceptor() {

    override fun intercept(chain: Interceptor.Chain): Response {

        val originalResponse = chain.proceed(chain.request())
        return originalResponse.newBuilder()
                .body(ProgressResponseBody(mFileDownLoaderListener,breakPoint ,originalResponse.body()!!))
                .build()
    }

}