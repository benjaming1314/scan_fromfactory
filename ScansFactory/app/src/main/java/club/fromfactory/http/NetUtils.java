package club.fromfactory.http;

import club.fromfactory.app_config.WareHouseManager;

/**
 * Created by lxm on 2016/11/9.
 */

public class NetUtils {

    // APP主页的网址；

    public static String APP_WAREHOUSE_URL ;
    public static String APP_WAREHOUSE_URL_TEST =  "http://yh.erp.test.yuceyi.com:5678/" ;

    public static String APP_MAIN_URL;

    public static String APP_MAIN_URL_LOGIN;
    public static String APP_MAIN_URL_LOGIN_TEST = "http://dev.yuceyi.com:8899/";

    public static String APP_MAIN_URL_PROCUREMENT;

    public static String APP_MAIN_URL_TRACKING;
    public static String APP_MAIN_URL_TRACKING_TEST = "http://tracking.dev.yuceyi.com/";

    static {
        APP_WAREHOUSE_URL = WareHouseManager.getInstance().getWareHouseInfoServer();
        APP_MAIN_URL = WareHouseManager.getInstance().getServerPath();

        APP_MAIN_URL_LOGIN = WareHouseManager.getInstance().getServerPathLogin();

        APP_MAIN_URL_PROCUREMENT = WareHouseManager.getInstance().getServerProcurement();

        APP_MAIN_URL_TRACKING = WareHouseManager.getInstance().getServerPathTracking();
    }

}
