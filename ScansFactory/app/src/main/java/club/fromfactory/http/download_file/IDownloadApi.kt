package club.fromfactory.http.download_file

import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.http.*

interface IDownloadApi {

    /**
     */
    @Streaming
    @GET
    fun download(@Url url :String): Observable<ResponseBody>


}