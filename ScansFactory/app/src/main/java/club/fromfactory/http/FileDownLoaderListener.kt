package club.fromfactory.http

interface FileDownLoaderListener {

//    fun startDownloading()

     fun onSuccess()
     fun onDownloading(progress: Int,currentLength :Long)
     fun onFail(code: Int, msg: String?)
}