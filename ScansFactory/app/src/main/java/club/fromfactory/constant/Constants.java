package club.fromfactory.constant;

import club.fromfactory.app_config.DeviceManager;
import club.fromfactory.app_config.WareHouseManager;

/**
 * Created by Peter on 2016/11/18.
 */

public class Constants {

    public static final String BOX_NAME = "box_name";

    public static String PDA_TYPE ;
    public static String PDA_TYPE_NF3501 = "nf3501";
    public static String PDA_TYPE_NEWPDA4001 = "newpda4001";
    public static String PDA_TYPE_ZEBRA_MC36 = "zebra_mc36";
    public static String PDA_TYPE_SEUIC_AUTOID9= "seuic_autoid9";
    public static String PDA_TYPE_50_SERIES= "50_Series";

    public static String PDA_TYPE_OTHER= "other";
    public static String SERVER_LOCATION ;
    public static String HOUSE_NAME ;

    static {
        PDA_TYPE = DeviceManager.getInstance().getPdaType();
        SERVER_LOCATION = WareHouseManager.getInstance().getServerLotion();
        HOUSE_NAME = WareHouseManager.getInstance().getHouseName();

    }
}
