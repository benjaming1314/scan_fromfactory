package club.fromfactory.utils;

import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * md5工具包
 */

public class MDFive {

    public static final int BUFF_SIZE = 1024;

    /**
     * get md5 of the input stream.
     *
     * @param in input stream. Could be FileInputStream, DataInputStream.
     * @return md5 if success, null if error occurs.
     */
    public static String getMD5(InputStream in) {
        if (in == null) {
            return null;
        }
        MessageDigest digest = null;
        byte buffer[] = new byte[BUFF_SIZE];
        int len;
        try {
            digest = MessageDigest.getInstance("MD5");
            while ((len = in.read(buffer, 0, BUFF_SIZE)) != -1) {
                digest.update(buffer, 0, len);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return bytesToHexString(digest.digest());
    }

    private static String bytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder("");
        if (src == null || src.length <= 0) {
            return null;
        }
        for (int i = 0; i < src.length; i++) {
            int v = src[i] & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
        }
        return stringBuilder.toString();
    }

    /**
     * get md5 of the string
     *
     * @param plainText md5的字符串
     * @return md5 if success, null if error occurs.
     */
    public static String getMD5(String plainText) {
        String str = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(plainText.getBytes());
            byte[] b = md.digest();

            int i;

            StringBuffer buf = new StringBuffer("");
            for (int offset = 0; offset < b.length; offset++) {
                i = b[offset];
                if (i < 0) {
                    i += 256;
                }
                if (i < 16) {
                    buf.append("0");
                }
                buf.append(Integer.toHexString(i));
            }
            str = buf.toString();
        } catch (NoSuchAlgorithmException e) {
            // ignore
        }

        return str;
    }
}
