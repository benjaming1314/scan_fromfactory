package club.fromfactory.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import club.fromfactory.FFApplication;
import club.fromfactory.R;
import club.fromfactory.baselibrary.utils.StringUtils;
import club.fromfactory.baselibrary.view.BaseActivity;
import club.fromfactory.baselibrary.widget.CustomDialog;

/**
 * Created by lxm on 2016/11/21.
 */

public class PromptDialogUtils {

    private static PromptDialogUtils dialogUtils;

    private PromptDialogUtils() {
    }

    public static PromptDialogUtils getInstance() {
        if (dialogUtils == null) {
            dialogUtils = new PromptDialogUtils();
        }
        return dialogUtils;
    }

    public interface DialogCallback {
        void onClickPositiveBtn();

        void onClickNegativeBtn();
    }
    /**
     * @param context
     * @return
     */
    public CustomDialog getPromptDialog(Context context,String prompt) {

        CustomDialog promptDialog = getPromptDialog(context, prompt, FFApplication.getInstance().getResources().getString(R.string.btn_ok), "", null);

        return promptDialog ;
    }

    /**
     * @param context
     * @return
     */
    public CustomDialog getPromptDialog(Context context  ,String msg,
                                        String positive,
                                        String negative,
                                        final DialogCallback dialogCallback) {

        final CustomDialog dialog = new CustomDialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_popup_window);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();

        int screenW = Utils.getScreenWidth(context);
        lp.width = (int) (0.9 * screenW);
        dialog.setCanceledOnTouchOutside(false);

        Button btnCancel = dialog.findViewById(R.id.popup_window_cancel);
        View btnLine = dialog.findViewById(R.id.popup_window_line);
        Button btnOk = dialog.findViewById(R.id.popup_window_ok);
        TextView editText = dialog.findViewById(R.id.popup_window_txt_message);

        if (StringUtils.isNotBlank(negative)) {
            btnCancel.setVisibility(View.VISIBLE);
            btnCancel.setText(negative);
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    if (dialogCallback != null) {
                        dialogCallback.onClickNegativeBtn();
                    }
                }
            });
        }else {
            btnCancel.setVisibility(View.GONE);
            btnLine.setVisibility(View.GONE);
        }

        if (StringUtils.isNotBlank(positive)) {
            btnOk.setVisibility(View.VISIBLE);
            btnOk.setText(positive);
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    if (dialogCallback != null) {
                        dialogCallback.onClickPositiveBtn();
                    }
                }
            });
        }else {
            btnOk.setVisibility(View.GONE);
            btnLine.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(msg)) {
            editText.setText(msg);
        }
        return dialog;
    }


    /**
     * 显示对话框
     *
     * @param activity
     * @param msg            对话框内容
     * @param positive       positive的按钮内容
     * @param negative       negative的按钮内容
     * @param dialogCallback 对话框回调
     */
    public static void showDialog(Activity activity,
                                  String msg,
                                  String positive,
                                  String negative,
                                  final DialogCallback dialogCallback) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(msg);
        builder.setCancelable(false);
        if (StringUtils.isNotBlank(positive)) {
            builder.setPositiveButton(positive,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            if (dialogCallback != null) {
                                dialogCallback.onClickPositiveBtn();
                            }
                            dialog.dismiss();
                        }
                    });

        }

        if (StringUtils.isNotBlank(negative)) {
            builder.setNegativeButton(negative,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            dialog.dismiss();
                            if (dialogCallback != null) {
                                dialogCallback.onClickNegativeBtn();
                            }
                        }
                    });
        }

        builder.show();
    }

    public void showDialog(Activity activity, String msg, DialogCallback dialogCallback) {
        showDialog(activity, msg, activity.getString(R.string.btn_yes), activity.getString(R.string.btn_no), dialogCallback);
    }

    public void showDialogDefine(Activity activity, String msg, DialogCallback dialogCallback) {
//        showDialog(activity, msg, activity.getString(R.string.btn_yes), activity.getString(R.string.btn_no), dialogCallback);
        CustomDialog promptDialog = getPromptDialog(activity, msg, activity.getString(R.string.btn_yes), activity.getString(R.string.btn_no), dialogCallback);
        if (promptDialog != null &&((BaseActivity)activity).isAlive()) {
            promptDialog.show();
        }
    }
    public void showDialogDefine(Activity activity, String msg, String positive,String negative, DialogCallback dialogCallback) {
//        showDialog(activity, msg, activity.getString(R.string.btn_yes), activity.getString(R.string.btn_no), dialogCallback);
        CustomDialog promptDialog = getPromptDialog(activity, msg, positive, negative, dialogCallback);

        if (promptDialog != null &&((BaseActivity)activity).isAlive()) {
            promptDialog.show();
        }
    }

}
