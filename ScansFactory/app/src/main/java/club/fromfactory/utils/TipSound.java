/**
 * @author xuxl
 * @email leoxuxl@163.com
 * @version 1.0 2015年12月24日 下午2:23:26
 */
package club.fromfactory.utils;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;

import club.fromfactory.FFApplication;
import club.fromfactory.R;

/**
 * This class is used for :
 *
 * @author xuxl
 * @version 1.0 2015年12月24日 下午2:23:26
 * @email leoxuxl@163.com
 */
public class TipSound {


    private static TipSound tipSound;

    private MediaPlayer mPlayer;

    private TipSound() {
        mPlayer = new MediaPlayer();
    }

    public synchronized static TipSound getInstance() {
        if (tipSound == null) {
            tipSound = new TipSound();
        }
        return tipSound;

    }

    private void play(Context context, int rid) {
        try {
            Uri uri = Uri.parse("android.resource://" + FFApplication.getInstance().getPackageName() + "/" + rid);
            if (mPlayer == null) {
                mPlayer = new MediaPlayer();
            }
            mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mPlayer.setDataSource(FFApplication.getInstance().getApplicationContext(), uri);
            mPlayer.setVolume(1.0f, 1.0f);
            mPlayer.setLooping(false);
            mPlayer.prepare();
            mPlayer.start();

            mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mPlayer.reset();
                    mPlayer.release();
                    mPlayer = null;
                }
            });
            mPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    mPlayer.start();
                    return false;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 播放扫描声音<br/>
     * play scan sound
     *
     * @param context
     */
    public void playScanSucceedSound(Context context) {
        play(context, R.raw.success);
    }

    /**
     * 播放失败声音<br/>
     * play scan sound 2017/2/24
     *
     * @param context
     */
    public void playScanFailedSound(Context context) {
        play(context, R.raw.jingling);
    }

    public void playJinglingdSound(Context context) {
        play(context, R.raw.jingling);
    }

    public void playScanSound(Context context) {
        play(context, R.raw.scan);
    }

    public void playBigSound(Context context) {
        play(context, R.raw.big);
    }
}
