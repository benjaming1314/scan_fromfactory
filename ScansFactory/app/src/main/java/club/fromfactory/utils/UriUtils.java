package club.fromfactory.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;

import club.fromfactory.baselibrary.net.NetUtils;
import club.fromfactory.baselibrary.router.RouterManager;
import club.fromfactory.baselibrary.utils.AppLinkUtils;
import club.fromfactory.baselibrary.utils.StringUtils;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * url相关的辅助方法 <p> Created by lxm on 2017/5/31.
 */

public class UriUtils {

    private static UriUtils sUriUtils;

    private UriUtils() {

    }

    public static UriUtils getInstance() {
        if (sUriUtils == null) {
            sUriUtils = new UriUtils();
        }

        return sUriUtils;
    }

    public static String urlChangeAddParams(String oldUrl, HashMap<String, String> params) {

        if (TextUtils.isEmpty(oldUrl) || params == null || params.size() == 0) {
            return oldUrl;
        }
        Map<String, String> map = new HashMap<>();
        StringBuilder mStringBuilder = new StringBuilder();

        String sharpValue = null;

        if (oldUrl.contains("#")) {
            int sharpIndex = oldUrl.indexOf("#");
            sharpValue = oldUrl.substring(sharpIndex);
            oldUrl = oldUrl.substring(0, sharpIndex);
        }
        if (oldUrl.contains("?")) {
            Uri uri = Uri.parse(oldUrl);
            mStringBuilder.append(oldUrl.substring(0, oldUrl.indexOf("?")));
            Set<String> stringSet = uri.getQueryParameterNames();
            if (stringSet != null) {
                for (String key : stringSet) {
                    if (!params.containsKey(key)) {
                        map.put(key, uri.getQueryParameter(key));
                    }
                }
            }
        } else {
            mStringBuilder.append(oldUrl);
        }
        String paramsValue = "";
        if (map.size() > 0) {
            for (String key : map.keySet()) {
                paramsValue += (key + "=" + map.get(key) + "&");
            }
        }

        for (String key : params.keySet()) {
            paramsValue += (key + "=" + params.get(key) + "&");
        }
        paramsValue = paramsValue.substring(0, paramsValue.length() - 1);

        mStringBuilder.append("?").append(paramsValue);

        if (sharpValue != null) {
            mStringBuilder.append(sharpValue);
        }

        return mStringBuilder.toString();
    }

    /**
     * 对原有url加参数，
     *
     * @param baseUrl 原始url
     * @param params 参数
     */
    private static String assembleUrl(String baseUrl, String params) {
        String newUrl = baseUrl;
        if (StringUtils.isNotBlank(baseUrl) && StringUtils.isNotBlank(params)) {
            if (baseUrl.contains("?")) {
                newUrl = baseUrl + "&" + params;
            } else {
                newUrl = baseUrl + "?" + params;
            }
        }
        return newUrl;
    }

    public boolean isUriNull(Uri deepLinkUri) {
        return deepLinkUri == null || StringUtils.isNull(deepLinkUri.toString());
    }

    /**
     * 获取跳转的Intent
     *
     * @param context 上下文
     * @param deepLinkUri 需要跳转的url
     */
    public Intent getJumpIntent(Context context, Uri deepLinkUri) {
        Intent intent = new Intent();

//        if (deepLinkUri != null && deepLinkUri.getScheme() != null) {
//            String url = generateJumpUrl(deepLinkUri);
//            if (TextUtils.isEmpty(Uri.parse(url).getPath())) { // 对path为空的情况特殊处理
//                intent.setClass(context, MainActivity.class);
//            } else {
//                intent.setClass(context, DetailsPageActivity.class);
//                intent.putExtra(INTENT_URL, url);
//            }
//        } else if (deepLinkUri != null) {
//            intent.setClass(context, MainActivity.class);
//            intent.putExtra(INTENT_URL_SUFFIX, deepLinkUri.toString());
//        } else {
//            intent.setClass(context, MainActivity.class);
//        }

        return intent;
    }

    /**
     * 生成需要跳转的链接
     *
     * @param deepLinkUri 透传的deepLink地址
     * @return 跳转的url
     */
    private String generateJumpUrl(Uri deepLinkUri) {

        String deepLinkUriStr = deepLinkUri.toString();

        String host = deepLinkUri.getHost();
        String query = deepLinkUri.getQuery();
        String path = deepLinkUri.getPath();

        // 添加特殊scheme判断
        String scheme = deepLinkUri.getScheme();
        if (!"clubboss".equals(scheme)) {
            return deepLinkUriStr;
        }

        if (path == null) {
            path = "";
        }
        String url;
        if (host != null && host.endsWith(".hibossapp.com")) {
            url = "http://" + host + path;
        }  else {
            url = NetUtils.APP_MAIN_URL + path;
        }

        //这个是把#内容拆开来
        String[] arrayStr = null;
        try {
            arrayStr = deepLinkUriStr.split("#");
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (arrayStr != null && arrayStr.length == 2) {
            if (StringUtils.isNotBlank(query)) {
                url += "?" + query;
            }
            url +="#" + arrayStr[1];
        } else {

            if (StringUtils.isNotBlank(query)) {
                url += "?" + query;
            }
        }
        return url;
    }

    /**
     * 打开url的通用方法入口
     */
    public static void openUrl(Context context, String url) {
        if (context == null || TextUtils.isEmpty(url)) {
            return;
        }

        // 支持相对路径
        if (url.startsWith("/")) {
            url = NetUtils.APP_MAIN_URL.substring(0, NetUtils.APP_MAIN_URL.length() - 1) + url;
        }

        Uri jumpUri = Uri.parse(url);
        // 如果是通过univeral link打开，则替换为app.clubboss.club域名
        if (AppLinkUtils.INSTANCE.isAppLink(jumpUri)) {
            jumpUri = jumpUri.buildUpon().scheme("clubboss").authority("www.hibossapp.com")
                    .build();
        }

        String schema = jumpUri.getScheme();

        // 判断是否可以用新的路由打开
        if (RouterManager.open(context, url)) {
            return;
        }

        // 判断是否可以用web页面打开
        if ("http".equals(schema) || "https".equals(schema)) {
//            DetailsPageActivity.Companion.launchActivity(context, url);
            return;
        }

        // 判断是否可以通过原有的路由打开
        Intent intent = UriUtils.getInstance().getJumpIntent(context, jumpUri);
        if (!(context instanceof Activity)) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }

        context.startActivity(intent);
    }

}
