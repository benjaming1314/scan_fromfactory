package club.fromfactory.utils;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

/**
 * Created by lxm on 2016/11/18.
 */

public class GsonUtils<T>  {

    private static GsonUtils gsonUtils ;
    private Gson gson = new Gson();

    public static GsonUtils getInstance() {
        if (gsonUtils == null) {
            gsonUtils = new GsonUtils();
        }
        return gsonUtils ;
    }

    public T parseJson(String message, TypeToken<T> typeToken) {
        T t = null;
        try {
            t = gson.fromJson(message, typeToken.getType());
            if(t != null) {
                Zlog.ii("lxm volley parseJson :"+ t.toString());
            }
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
        return t ;
    }

    public String toJson(Object o , TypeToken<T> typeToken) {
        String json = null;
        try {
            json = gson.toJson(o,typeToken.getType());
            if(json != null) {
                Zlog.ii("lxm volley toJson :"+ json);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json ;
    }
}
