package club.fromfactory.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.drawable.Animatable;
import android.net.Uri;
import android.util.Base64;
import android.view.ViewGroup;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import club.fromfactory.FFApplication;
import club.fromfactory.baselibrary.utils.DensityUtils;
import club.fromfactory.baselibrary.utils.ScreenUtils;
import club.fromfactory.baselibrary.utils.StringUtils;
import club.fromfactory.fresco.FrescoUtils;
import club.fromfactory.fresco.view.FrescoImageView;

/**
 * Created by lxm on 2017/8/4.
 */

public class ImageUtils {


    //s3的图片地址转换到cdn

//    链接分成两部分：
//    第一部分：https://s3.amazonaws.com
//    转换规则：https://s3.amazonaws.com ->http://prod.image.yuceyi.com/s3_amazonaws
//
//    第二部分：fromfactory.club.image/21/aa/21c0fc9b7c7b96e258181ba7832d77aa.jpg
//    转换规则：不变直接拼接组装成链接
//
//    转换后：
//    http://prod.image.yuceyi.com/s3_amazonaws/fromfactory.club.image/21/aa/21c0fc9b7c7b96e258181ba7832d77aa.jpg
//    s3.amazonaws.com

    //    http://prod.image.yuceyi.com/s3_amazonaws/fromfactory.club.image/21/aa/21c0fc9b7c7b96e258181ba7832d77aa.jpg
    //规格： 800x800 650x650 350x350  200x200 50x50
    private static String imageUrlConversion(String imageUrl) {

        if (StringUtils.isNull(imageUrl)) {
            return "";
        }

        StringBuilder stringBuilder = new StringBuilder();

        Uri uri = Uri.parse(imageUrl);
        String host = uri.getHost();
        String path = uri.getPath();


        if ("s3.amazonaws.com".equals(host)) {
            stringBuilder.append("http://prod.image.yuceyi.com/s3_amazonaws").append(path);
        } else {
            return imageUrl;
        }

        //加规格

        return addSizeImageUrl(stringBuilder.toString());

    }

    //规格： 800x800 650x650 350x350 200x200 50x50
    private static String addSizeImageUrl(String imageUrl) {

        if (StringUtils.isNull(imageUrl)) {
            return "";
        }

        String newImageUrl;
        String picNamePre;
        String picNameSuf = null;

        try {
            if (imageUrl.endsWith(".jpg") || imageUrl.endsWith(".png")) {

                picNamePre = imageUrl.substring(0, imageUrl.length() - 4);
                picNameSuf = imageUrl.substring(imageUrl.length() - 3);

            } else if (imageUrl.endsWith(".webp")) {
                picNamePre = imageUrl.substring(0, imageUrl.length() - 5);
                picNameSuf = imageUrl.substring(imageUrl.length() - 4);
            } else {
                picNamePre = imageUrl;
            }
        } catch (Exception e) {
            e.printStackTrace();
            picNamePre = imageUrl;
            picNameSuf = "jpg";
        }

        String imageSizeStyle = getImageSizeStyle();

        if (StringUtils.isNull(picNameSuf)) {

            if (StringUtils.isNotBlank(imageSizeStyle)) {
                newImageUrl = picNamePre + "_" + getImageSizeStyle();
            }else {
                newImageUrl = picNamePre ;
            }
        } else {
            if (StringUtils.isNotBlank(imageSizeStyle)) {
                newImageUrl = picNamePre + "_" + getImageSizeStyle() + "." + picNameSuf;
            }else {
                newImageUrl = picNamePre + "." + picNameSuf;
            }
        }
        return newImageUrl;
    }

    //规格： 800x800 650x650 350x350  200x200 50x50
    private static String getImageSizeStyle() {

        int selectSize = 0;

        int screenWidth = ScreenUtils.getScreenWidth();



        switch (selectSize) {

            case 800:
                return "800x800";

            case 600:
                return "650x650";

            case 350:
                return "350x350";

            case 200:
                return "200x200";

            case 50:
                return "50x50";

            default:
                return "350x350";
        }


    }

    public static void loadImageNew(FrescoImageView imageView, String imageUrl, int defaultImg) {
        loadImage(imageView, imageUrlConversion(imageUrl), defaultImg);
    }

    /**
     * 加载网络图片
     *
     * @param imageView  需要显示图片imageView
     * @param imageUrl   图片网址
     * @param defaultImg 占位图
     */
    public static void loadImage(FrescoImageView imageView, String imageUrl, int defaultImg) {
        loadImage(imageView, imageUrl, false, defaultImg);
    }

    public static void loadImage(FrescoImageView imageView, String imageUrl, boolean isLocalPath,
                                 int defaultImg) {
        loadImage(imageView, imageUrl, isLocalPath, defaultImg, null);
    }

    public static void loadImage(FrescoImageView imageView, String imageUrl, boolean isLocalPath,
                                 int defaultImg, Point size) {
        loadImage(imageView, imageUrl, isLocalPath, defaultImg, size, 0);
    }

    public static void loadImage(FrescoImageView imageView, String imageUrl, boolean isLocalPath,
                                 int defaultImg, Point size, int cornerRadius) {
        if (StringUtils.isNull(imageUrl)) {
            imageUrl =  "";
        }
        int radius = ScreenUtils.dip2px(FFApplication.getInstance(), cornerRadius);
        FrescoUtils.getInstance().getFrescoImageLoad()
                .setImageView(imageView)
                .setUrl(imageUrl)
                .setSize(size)
                .setCornerRadius(radius)
                .setLoadLocalPath(isLocalPath)
//                .setPostprocessor(new BlurPostprocessor(context, 10))
                .setDefaultImg(defaultImg)
                .load();
    }

    public static void loadLocalImage(FrescoImageView mImageView, String path) {
        Uri uri = Uri.parse(path);
        mImageView.setImageURI(uri);
    }

    public static void loadLocalResImage(FrescoImageView mImageView, int res) {
        Uri uri = Uri.parse("res://" + FFApplication.getInstance().getPackageName() + "/" + res);
        mImageView.setImageURI(uri);
    }

    public static void loadLocalGifImage(FrescoImageView mImageView, int res) {
        Uri uri = Uri.parse("res://" + FFApplication.getInstance().getPackageName() + "/" + res);

        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setUri(uri)
                .setAutoPlayAnimations(true)//设置为true将循环播放Gif动画
                .setOldController(mImageView.getController())
                .build();

        // 设置控制器
        mImageView.setController(controller);

        Animatable animation = mImageView.getController().getAnimatable();

        if (animation != null && !animation.isRunning()) {
            animation.start();
        }
    }


    // 分类页的广告图片 750*240
    public static void setImageParams(FrescoImageView view) {
        int screenWidth = ScreenUtils.getScreenWidth();
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        int width = view.getWidth();

        if (width == 0) {
            width = screenWidth * 3 / 4 - DensityUtils.dp2px(FFApplication.getInstance(), 20);
        }

        layoutParams.height = width * 240 / 750;
    }

    public static void setMessageCenterImageParams(FrescoImageView view) {

        int screenWidth = ScreenUtils.getScreenWidth();
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        int width = view.getWidth();

        if (width == 0) {
            width = screenWidth - DensityUtils.dp2px(FFApplication.getInstance(), 60);
        }

        layoutParams.height = width * 240 / 750;
    }


    /**
     * 获取图片的base64编码
     *
     * @param bitmap 需要编码的图片
     * @return base64编码
     */
    public static String getBitmapBase64(Bitmap bitmap) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
        byte[] bytes = bos.toByteArray();
        return Base64.encodeToString(bytes, Base64.DEFAULT);
    }


    public static Bitmap getBitmapFromUri(Context context, Uri uri, int maxWidth, int compress) {
        Bitmap bitmap = null;
        InputStream decodeStream = null;
        InputStream inputStream = null;
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();

            //设置inJustDecodeBounds为true表示只获取大小，不生成Bitmap
            options.inJustDecodeBounds = true;

            //解析图片大小
            decodeStream = context.getContentResolver().openInputStream(uri);
            BitmapFactory.decodeStream(decodeStream, null, options);
            int width = options.outWidth;

            //计算取样比例
            int sampleRatio = width / maxWidth;
            //定义图片解码选项
            BitmapFactory.Options bitmapOption = new BitmapFactory.Options();
            //读取图片，并将图片缩放到指定的目标大小
            bitmapOption.inSampleSize = sampleRatio;
            if ("image/png".equals(options.outMimeType)) {
                // png设置读取图片的色值配置RGB_8888
                bitmapOption.inPreferredConfig = Bitmap.Config.ARGB_8888;
            } else {
                // jpeg设置读取图片的色值配置RGB_565
                bitmapOption.inPreferredConfig = Bitmap.Config.RGB_565;
            }
            inputStream = context.getContentResolver().openInputStream(uri);
            bitmap = BitmapFactory.decodeStream(inputStream, null, bitmapOption);

            // oppo手机兼容处理，防止bitmap获取为空
            if (bitmap != null) {
                //如果这时候图片大小仍超出最大宽度限制，通过矩阵scale的方式来处理
                if (bitmap.getWidth() > maxWidth) {
                    float ratio = (float) maxWidth / (float) bitmap.getWidth();
                    Matrix matrix = new Matrix();
                    matrix.setScale(ratio, ratio);
                    bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap
                            .getHeight(), matrix, true);
                }
                if (bitmap != null) {
                    //如果这时候图片大小仍超出最大宽度限制，通过矩阵scale的方式来处理
                    if (bitmap.getWidth() > maxWidth) {
                        float ratio = (float) maxWidth / (float) bitmap
                                .getWidth();
                        Matrix matrix = new Matrix();
                        matrix.setScale(ratio, ratio);
                        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap
                                .getHeight(), matrix, true);
                    }

                    // 对jpeg的图片进行压缩
                    if ("image/jpeg".equals(options.outMimeType)) {
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.JPEG, compress,
                                baos);
                        byte[] bytes = baos.toByteArray();
                        bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }

        return bitmap;
    }

    /**
     * 缓存bitmap到本地文件
     *
     * @param file   本地文件
     * @param bitmap 图片
     */
    public static void saveBitmapToLocal(File file, Bitmap bitmap) {
        try {
            // file其实是图片，它的父级File是文件夹，判断一下文件夹是否存在，如果不存在，创建文件夹
            File fileParent = file.getParentFile();
            if (!fileParent.exists()) {
                // 文件夹不存在
                fileParent.mkdirs();// 创建文件夹
            }
            // 将图片保存到本地
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100,
                    new FileOutputStream(file));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据图片类型转换成前端需要的类型
     */
    private static String formatBitmapType(String imageType) {
        if ("image/jpeg".equals(imageType)) {
            return "jpg";
        } else if ("image/png".equals(imageType)) {
            return "png";
        }

        return "jpg";
    }

    /**
     * base64的string转换成bitmap
     *
     * @param base64 待转换的base64数据
     * @return Bitmap 转换的bitmap
     */
    public static Bitmap getBitmapFromBase64(String base64) {
        byte[] decodedString = Base64.decode(base64, Base64.DEFAULT);
        Bitmap bitmap = null;
        try {
            bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return bitmap;
    }
}
