package club.fromfactory.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import club.fromfactory.R;
import club.fromfactory.adapter.GoodsStateAdapter;
import club.fromfactory.baselibrary.utils.ScreenUtils;
import club.fromfactory.baselibrary.widget.CustomDialog;
import club.fromfactory.pojo.QueryGoodsState;
import club.fromfactory.view.OnAlterClickListner;
import club.fromfactory.view.SelectOnlickListner;

/**
 * Created by lxm on 2017/5/10.
 */

public class AlterSkuSateDialog {
    private static AlterSkuSateDialog dialogUtils;

    private AlterSkuSateDialog() {

    }

    public static AlterSkuSateDialog getInstance() {
        if (dialogUtils == null) {
            dialogUtils = new AlterSkuSateDialog();
        }
        return dialogUtils;
    }

    @BindView(R.id.img_close)
    ImageView imgClose;
    @BindView(R.id.loading_dialog)
    LinearLayout linearLayout;
    @BindView(R.id.btn_ok)
    Button btnOk;
    @BindView(R.id.lv_statelist)
    ListView lvStatelist;

    private CustomDialog loadingDialog;
    private SelectOnlickListner selectOnlickListner;
    private GoodsStateAdapter goodsStateAdapter;

    private List<QueryGoodsState> queryGoodsStates;

    private String skuState = "0";
    private int position = 0;

    /**
     * 获取加载页的对话框
     *
     * @param context
     * @return
     */
    public CustomDialog getAlterSkuSateDialog(Context context, List<QueryGoodsState> queryGoodsStates, SelectOnlickListner selectOnlickListner) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.dialog_alter_skustate, null);
        ButterKnife.bind(this, view);
//        FFApplication.getInstance().width * 2 / 3
        loadingDialog = new CustomDialog(context, R.style.Loading_dialog_alter);
        loadingDialog.setContentView(linearLayout, new LinearLayout.LayoutParams(
                ScreenUtils.getScreenWidth() * 2 / 3,
                LinearLayout.LayoutParams.WRAP_CONTENT));// 设置布局
        loadingDialog.setCanceledOnTouchOutside(true);
        this.queryGoodsStates = queryGoodsStates;
        this.selectOnlickListner = selectOnlickListner;
        goodsStateAdapter = new GoodsStateAdapter(context, -1, queryGoodsStates);
        goodsStateAdapter.setOnAlterListner(new OnAlterClickListner() {
            @Override
            public void clickAlter(int position) {
//                refreshSelect(positon);
                AlterSkuSateDialog.this.position = position;
                String nameValue = AlterSkuSateDialog.this.queryGoodsStates.get(position).getNameValue();
                Zlog.ii("lxm ss setOnAlterListner:" + position + "  " + nameValue);
                setSkuState(nameValue);
            }
        });
        lvStatelist.setAdapter(goodsStateAdapter);

        return loadingDialog;
    }

//    private void refreshSelect(int positon) {
//
//        for (int i = 0 ; i < queryGoodsStates.size() ; i++ ) {
//            if (i == positon) {
//                skuState = queryGoodsStates.get(i).getName();
//                queryGoodsStates.get(i).setSelect(true);
//            }else {
//                queryGoodsStates.get(i).setSelect(false);
//            }
//        }
//        goodsStateAdapter.notifyDataSetChanged();
//    }

    @OnClick({R.id.img_close, R.id.btn_ok})
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.img_close: {
                Zlog.ii("lxm ss onClick: img_close" + "  " + loadingDialog);
                if (loadingDialog != null && loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
            }
            break;
            case R.id.btn_ok: {
                if (selectOnlickListner != null) {
                    selectOnlickListner.onSelectItem(position);
                }
                if (loadingDialog != null && loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
            }
            break;

            default:
                break;
        }
    }

    public void setSkuState(String skuState) {

        this.skuState = skuState;
        for (int i = 0; i < queryGoodsStates.size(); i++) {
            QueryGoodsState queryGoodsState = queryGoodsStates.get(i);

            String nameValue = queryGoodsState.getNameValue();
            if (nameValue.equals(skuState)) {
                queryGoodsState.setSelect(true);
            } else {
                queryGoodsState.setSelect(false);
            }
            Zlog.ii("lxm ss setSkuState: " + queryGoodsState);
        }
        goodsStateAdapter.notifyDataSetChanged();
//        if (skuState.equals("1")) {
//            lyIdleInventory.setSelected(true);
//            lyOutboundOccupy.setSelected(false);
//            lyScrapInventory.setSelected(false);
//        } else if (skuState.equals("2")) {
//            lyIdleInventory.setSelected(false);
//            lyOutboundOccupy.setSelected(true);
//            lyScrapInventory.setSelected(false);
//        } else if (skuState.equals("3")) {
//            lyIdleInventory.setSelected(false);
//            lyOutboundOccupy.setSelected(false);
//            lyScrapInventory.setSelected(true);
//        } else {
//            lyIdleInventory.setSelected(true);
//            lyOutboundOccupy.setSelected(false);
//            lyScrapInventory.setSelected(false);
//        }

    }
}
