package club.fromfactory.utils;

import android.util.Log;

import club.fromfactory.BuildConfig;

public class Zlog {
	public static boolean isDebug = BuildConfig.DEBUG;
	private static final String TAG = "scan_factory";

	// 
	public static void i(String msg) {
		if (isDebug && msg!=null)
			Log.i(TAG, "scan_factory:"+msg);
	}

	public static void d(String msg) {
		if (isDebug && msg!=null)
			Log.d(TAG,  "scan_factory:"+msg);
	}

	public static void e(String msg) {
		if (isDebug && msg!=null)
			Log.e(TAG,  "scan_factory:"+msg);
	}

	public static void v(String msg) {
		if (isDebug && msg!=null)
			Log.v(TAG, msg);
	}
	//
	//*
	public static void i(Class<?> _class, String msg){
		if (isDebug && msg!=null)
			Log.i(_class.getName(),  "scan_factory:"+msg);
	}
	//*/
	public static void d(Class<?> _class, String msg){
		if (isDebug && msg!=null)
			Log.i(_class.getName(),  "scan_factory:"+msg);
	}
	public static void e(Class<?> _class, String msg){
		if (isDebug && msg!=null)
			Log.i(_class.getName(),  "scan_factory:"+msg);
	}
	public static void v(Class<?> _class, String msg){
		if (isDebug && msg!=null)
			Log.i(_class.getName(),  "scan_factory:"+msg);
	}
	//
	public static void i(String tag, String msg) {
		if (isDebug && msg!=null)
			Log.i(tag, "scan_factory:"+ msg);
	}
	public static void ii( String msg) {
		if (isDebug && msg!=null)
			Log.i(TAG, "scan_factory:"+ msg);
	}

	public static void d(String tag, String msg) {
		if (isDebug && msg!=null)
			Log.d(tag, msg);
	}

	public static void e(String tag, String msg) {
		if (isDebug && msg!=null)
			Log.e(tag,  "scan_factory:"+msg);
	}

	public static void v(String tag, String msg) {
		if (isDebug && msg!=null)
			Log.v(tag,  "scan_factory:"+msg);
	}
}
