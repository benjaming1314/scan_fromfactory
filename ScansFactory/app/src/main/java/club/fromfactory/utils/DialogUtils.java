//package club.fromfactory.utils;
//
//import android.content.Context;
//import android.text.TextUtils;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//import club.fromfactory.R;
//import club.fromfactory.baselibrary.widget.CustomDialog;
//
///**
// * Created by lxm on 2016/11/21.
// */
//
//public class DialogUtils {
//
//    private static DialogUtils dialogUtils;
//
//    private DialogUtils() {
//
//    }
//
//    public static DialogUtils getInstance() {
//        if (dialogUtils == null) {
//            dialogUtils = new DialogUtils();
//        }
//        return dialogUtils;
//    }
//
//    private TextView textView;
//
//    /**
//     * 获取加载页的对话框
//     *
//     * @param context
//     * @return
//     */
//    public CustomDialog getProgressDialog(Context context, String prompt) {
//
//
//        LayoutInflater inflater = LayoutInflater.from(context);
//        View view = inflater.inflate(R.layout.loading_dialog, null);
//        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.loading_dialog);
//
//        CustomDialog loadingDialog = new CustomDialog(context, R.style.Loading_dialog);
//        loadingDialog.setContentView(linearLayout, new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT));// 设置布局
//
//        textView = (TextView) view.findViewById(R.id.txt_prompt);
//        if (TextUtils.isEmpty(prompt)) {
//            textView.setVisibility(View.GONE);
//        } else {
//            textView.setVisibility(View.VISIBLE);
//            textView.setText(prompt);
//        }
//        return loadingDialog;
//    }
//
//    public void setTextPrompt(String str) {
//        if (textView != null) {
//            textView.setText(str);
//        }
//    }
//}
