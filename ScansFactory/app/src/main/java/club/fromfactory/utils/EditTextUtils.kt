package club.fromfactory.utils

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText

object EditTextUtils {

    private var isClearEditText = false

    fun addEditTextListener(editText: EditText) {

        editText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (isClearEditText) {
                    isClearEditText = false
                    if (s?.contains("\n") == true) {
                        editText.setText("")
                    }
                } else {
                    if (s?.contains("\n") == true) {
                        editText.setText(s.toString().replace("\n", ""))

                    }
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                isClearEditText = s?.length == 0
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

        })
    }

}