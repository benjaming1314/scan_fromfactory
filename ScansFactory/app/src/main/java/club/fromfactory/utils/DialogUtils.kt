package club.fromfactory.utils

import android.content.Context
import android.view.Window
import android.widget.Button
import android.widget.EditText
import club.fromfactory.R
import club.fromfactory.baselibrary.utils.StringUtils
import club.fromfactory.baselibrary.utils.ToastUtils
import club.fromfactory.baselibrary.widget.CustomDialog

object DialogUtils {

    interface DialogUtilsCallback {
        fun onClickPositiveBtn()

        fun onClickNegativeBtn()
    }
    /**
     * @param context
     * @return
     */
    fun handoverComfirmDialog(context: Context,countNum :Int,dialogUtilsCallback : DialogUtilsCallback?) : CustomDialog{

        val dialog = CustomDialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_handover_item)
        val window = dialog.window
        val lp = window!!.attributes

        val screenW = Utils.getScreenWidth(context)
        lp.width = (0.9 * screenW).toInt()
        dialog.setCanceledOnTouchOutside(false)


        val edtNum = dialog.findViewById<EditText>(R.id.dialog_handover_edt_num)
        val cancelButton = dialog.findViewById<Button>(R.id.dialog_handover_cancel)
        val confirmButton = dialog.findViewById<Button>(R.id.dialog_handover_ok)


        dialog.setOnCancelListener {
            edtNum.setText("")
        }

        cancelButton.setOnClickListener {
            dialogUtilsCallback?.onClickNegativeBtn()
            dialog.dismiss()
        }
        confirmButton.setOnClickListener {
            val num = edtNum.text.toString()
            if (StringUtils.isNotBlank(num) && num.toInt() == countNum) {
                dialog.dismiss()
                dialogUtilsCallback?.onClickPositiveBtn()
            }else if (StringUtils.isNotBlank(num)){
                TipSound.getInstance().playScanFailedSound(context)
                ToastUtils.show(context.resources.getString(R.string.check_bag_num_prompt))
            }else {
                ToastUtils.show(context.resources.getString(R.string.input_prompt) + context.resources.getString(R.string.handover_package_num_prompt))
            }

        }
        dialog.show()
        return dialog
    }


}