package club.fromfactory.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import club.fromfactory.FFApplication;
import club.fromfactory.ui.sign.model.TrackingsNoDetail;

/**
 * Created by lxm on 2016/11/9.
 */

public class Utils {


    /**
     * 重启app
     *
     * @param mActivity
     */
    public static void restartApp(Activity mActivity) {
        Intent i = mActivity.getBaseContext().getPackageManager()
                .getLaunchIntentForPackage(mActivity.getBaseContext().getPackageName());
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mActivity.startActivity(i);
    }

    /*
     * 获取当前程序的版本号
     */
    public static int getApplicationVersionCode(Context context) {
        // 获取packagemanager的实例
        PackageManager packageManager = context.getPackageManager();
        // getPackageName()是你当前类的包名，0代表是获取版本信息
        PackageInfo packInfo = null;
        try {
            packInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
            return packInfo.versionCode;

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return Integer.MAX_VALUE;
    }

    /**
     * 请求商品类别标签，sendemptymessage 3
     *
     * @return
     */
    public static String getAndroidId() {
        return Settings.Secure.getString(FFApplication.getInstance().getContentResolver(), Settings.Secure.ANDROID_ID);
    }


    public static int getErrorTrackingNo(List<TrackingsNoDetail> trackingsNoList) {

        int errorNum = 0;

        for (int i = 0; i < trackingsNoList.size(); i++) {
            TrackingsNoDetail trackingsNoDetail = trackingsNoList.get(i);
            int signedStatus = trackingsNoDetail.getSigned_status();
            if (signedStatus == 1) {
                errorNum++;
            }
        }

        return errorNum;

    }

    /**
     * 10位的长度
     *
     * @param sku_id
     * @return
     */
    public static String skuIdToSkuNo(int sku_id) {
        String sku_no = "";
        String skuIdStr = String.valueOf(sku_id);
        for (int i = 0; i < 10 - skuIdStr.length(); i++) {
            sku_no += "0";
        }
        Zlog.ii("lxm skuIdToSkuNo:" + sku_no + skuIdStr);
        return sku_no + skuIdStr;

    }

    public static String getDeviceId() {
        TelephonyManager telephonyManager;
        telephonyManager =
                (TelephonyManager) FFApplication.getInstance().getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(FFApplication.getInstance(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return null;
        }
        return telephonyManager.getDeviceId();
    }


    public static String getApplicationVersionName(Context context) {
        PackageManager packageManager = context.getPackageManager();
        try {
            PackageInfo packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException ex) {
        } catch (Exception e) {
        }
        return null;
    }

    /**
     * 获取屏幕的宽
     *
     * @param context
     * @return
     */
    @SuppressWarnings("deprecation")
    public static int getScreenWidth(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        int sWidth = display.getWidth();
        return sWidth;
    }

    /**
     * 获取屏宽高
     *
     * @param context
     * @return
     */
    @SuppressWarnings("deprecation")
    public static int getScreenHeight(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        int sHeight = display.getHeight();
        return sHeight;
    }

    /**
     * 获取国家码
     */
    public static String getCountryZipCode(Context context) {
        String CountryID = "";
//        String CountryZipCode = "";
        TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        Configuration config = context.getResources().getConfiguration();
        String countryConfig = config.locale.getCountry();
        CountryID = manager.getSimCountryIso().toUpperCase();
        String country = Locale.getDefault().getCountry();
        Zlog.ii("lxm ss:CountryID--->>>" + CountryID + "   " + country + "   " + countryConfig);
//        String[] rl = context.getResources().getStringArray(R.array.CountryCodes);
//        for (int i = 0; i < rl.length; i++) {
//            String[] g = rl[i].split(",");
//            if (g[1].trim().equals(CountryID.trim())) {
//                CountryZipCode = g[0];
//                break;
//            }
//        }
        return country;
    }

    /**
     * 获取进程号对应的进程名
     *
     * @param pid 进程号
     * @return 进程名
     */
    public static String getProcessName(int pid) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader("/proc/" + pid + "/cmdline"));
            String processName = reader.readLine();
            if (!TextUtils.isEmpty(processName)) {
                processName = processName.trim();
            }
            return processName;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }
        return null;
    }


    public static ArrayList<String> stringToList(String str, boolean isReverse) {
        ArrayList<String> list = new ArrayList();
        if (TextUtils.isEmpty(str)) {
            return list;
        }
        String[] strArr = str.split(",");
        if (isReverse) {
            for (int i = strArr.length - 1; i >= 0; i--) {
                list.add(strArr[i]);
            }
        } else {
            for (int i = 0; i < strArr.length; i++) {
                list.add(strArr[i]);
            }
        }

        return list;
    }

    public static String listToString(ArrayList<String> stringList) {

        StringBuffer newStr = new StringBuffer();
        for (int i = 0; i < stringList.size(); i++) {
            newStr.append(stringList.get(i));
            if (i != stringList.size() - 1) {
                newStr.append(",");
            }
        }
        return newStr.toString();
    }

    public static String stringArrayToString(String[] str) {

        if (str == null) return null;
        StringBuffer stringBuffer = new StringBuffer();

        for (int i = 0; i < str.length; i++) {
            stringBuffer.append(str[i]);
            if (i != str.length - 1) stringBuffer.append(",");
        }

        return stringBuffer.toString();
    }

    //    public String[] stringToStringArray(String string) {
//
//        if (TextUtils.isEmpty(string)) return null;
//
//        String[] strArray =  string.split(",");
//
//        return strArray ;
//    }
    public static String printKeyHash(Activity context) {
        PackageInfo packageInfo;
        String key = null;
        try {
            //getting application package name, as defined in manifest
            String packageName = FFApplication.getInstance().getPackageName();

            //Retriving package info
            packageInfo = FFApplication.getInstance().getPackageManager().getPackageInfo(packageName,
                    PackageManager.GET_SIGNATURES);

            Zlog.ii("lxm ss  Package Name = " + context.getApplicationContext().getPackageName());

            for (Signature signature : packageInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                key = new String(Base64.encode(md.digest(), 0));

//             String key1 = new String(Base64.encodeBytes(md.digest()));
                Zlog.ii("lxm ss Key Hash: " + key);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Zlog.ii("Name not found " + e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Zlog.ii("lxm ss No such an algorithm " + e.toString());
        } catch (Exception e) {
            Zlog.ii("lxm ss Exception " + e.toString());
        }
        return key;
    }


    /**
     * 判断字符是否是中文
     *
     * @param c 字符
     * @return 是否是中文
     */
    public static boolean isChinese(char c) {
        Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
        if (ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS
                || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS
                || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A
                || ub == Character.UnicodeBlock.GENERAL_PUNCTUATION
                || ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION
                || ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS) {
            return true;
        }
        return false;
    }

    /**
     * 判断字符串是否是乱码
     *
     * @param strName 字符串
     * @return 是否是乱码
     */
    public static boolean isMessyCode(String strName) {
        Pattern p = Pattern.compile("\\s*|t*|r*|n*");
        Matcher m = p.matcher(strName);
        String after = m.replaceAll("");
        String temp = after.replaceAll("\\p{P}", "");
        char[] ch = temp.trim().toCharArray();
        float chLength = ch.length;
        float count = 0;
        for (int i = 0; i < ch.length; i++) {
            char c = ch[i];
            if (!Character.isLetterOrDigit(c)) {
                if (!isChinese(c)) {
                    count = count + 1;
                }
            }
        }
        float result = count / chLength;
        if (result > 0.4) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * 动态设置ListView的高度
     *
     * @param listView
     */
    public static void setListViewHeightBasedOnChildren(ListView listView) {

        if (listView == null) return;
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }
        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
            Zlog.ii("lxm setListViewHeightBasedOnChildren:" + listItem.getMeasuredHeight());
        }
        Zlog.ii("lxm setListViewHeightBasedOnChildren:" + listAdapter.getCount());
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);

    }


}
