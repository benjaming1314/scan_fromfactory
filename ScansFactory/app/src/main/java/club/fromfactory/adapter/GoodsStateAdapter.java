package club.fromfactory.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import club.fromfactory.utils.Zlog;
import club.fromfactory.pojo.QueryGoodsState;
import club.fromfactory.R;
import club.fromfactory.view.OnAlterClickListner;

/**
 * Created by lxm on 2017/2/21.
 */

public class GoodsStateAdapter extends ArrayAdapter<QueryGoodsState> {

    private LayoutInflater inflater;
    private Context context;
    private List<QueryGoodsState> object;
    private OnAlterClickListner onAlterListner;

    public GoodsStateAdapter(Context context, int resource, List<QueryGoodsState> objects) {
        super(context, resource, objects);

        this.context = context;
        this.object = objects;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Nullable
    @Override
    public QueryGoodsState getItem(int position) {
        return super.getItem(position);
    }

    @NonNull
    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        ViewHolder holder;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = inflater.inflate(R.layout.dialog_alter_skustate_item, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }

        QueryGoodsState item = getItem(position);
        Zlog.ii("lxm ss goodsstateadpter:" + item);
        String nameValue = item.getNameValue();
        boolean isSelect = item.isSelect();
        holder.txtGoodsState.setText(nameValue);
        holder.txtGoodsState.setSelected(isSelect);
        holder.imgGoodsState.setSelected(isSelect);
//        holder.lyIdleInventory.setSelected(isSelect);
        holder.lyIdleInventory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!v.isSelected()) {
                    if (onAlterListner != null) {
                        onAlterListner.clickAlter(position);
                    }
                }
            }
        });
        return view;
    }

    static class ViewHolder {
        @BindView(R.id.txt_goods_state)
        TextView txtGoodsState;
        @BindView(R.id.img_goods_state)
        ImageView imgGoodsState;
        @BindView(R.id.ly_idle_inventory)
        LinearLayout lyIdleInventory;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public void setOnAlterListner(OnAlterClickListner onAlterListner) {
        this.onAlterListner = onAlterListner;
    }
}

