//package club.fromfactory.adapter;
//
//import android.content.Context;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import android.text.TextUtils;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ArrayAdapter;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import java.util.List;
//
//import butterknife.BindView;
//import butterknife.ButterKnife;
//import club.fromfactory.R;
//import club.fromfactory.Utils.ImageUtils;
//import club.fromfactory.Utils.Zlog;
//import club.fromfactory.fresco.view.FrescoImageView;
//import club.fromfactory.ui.common.model.Goods;
//import club.fromfactory.view.OnAlterClickListner;
//
///**
// * Created by lxm on 2017/2/21.
// */
//
//public class GoodsAdapter extends ArrayAdapter<Goods> {
//
//    public static final int SHOW_HAS_ALTER = 1000;
//    public static final int SHOW_NO_ALTER = 1001;
//
//    public static final int SHOW_NO_IMAGE = 1002;
//    public static final int SHOW_HAS_IMAGE = 1003;
//
//    private LayoutInflater inflater;
//    private Context context;
//    private List<Goods> object;
//    private int showType;
//    private int showImageType;
//    private OnAlterClickListner onAlterListner ;
//
//    public GoodsAdapter(Context context, int resource, List<Goods> objects, int showType) {
//        super(context, resource, objects);
//
//        this.context = context;
//        this.object = objects;
//        this.showType = showType;
//        this.showImageType = SHOW_NO_IMAGE ;
//        inflater = LayoutInflater.from(context);
//    }
//    public GoodsAdapter(Context context, int resource, List<Goods> objects, int showType,int showImageType) {
//        super(context, resource, objects);
//
//        this.context = context;
//        this.object = objects;
//        this.showType = showType;
//        this.showImageType = showImageType;
//        inflater = LayoutInflater.from(context);
//    }
//
//    @Override
//    public int getCount() {
//        return super.getCount();
//    }
//
//    @Nullable
//    @Override
//    public Goods getItem(int position) {
//        return super.getItem(position);
//    }
//
//    @NonNull
//    @Override
//    public View getView(final int position, View view, ViewGroup parent) {
//        ViewHolder holder;
//        if (view != null) {
//            holder = (ViewHolder) view.getTag();
//        } else {
//            view = inflater.inflate(R.layout.goods_alter_list_item, parent, false);
//            holder = new ViewHolder(view);
//            view.setTag(holder);
//        }
//        holder.lyNavigation.setVisibility(View.GONE);
//        if (position == 0) {
//            holder.viewLine.setVisibility(View.GONE);
////            holder.lyNavigation.setVisibility(View.VISIBLE);
//        } else {
//            holder.viewLine.setVisibility(View.VISIBLE);
//        }
//
//        Goods item = getItem(position);
//        Zlog.ii("lxm ss goodsadpter:" + item + "  " + showImageType + "  " + showType);
//        String imagUrl = item.getSku_image_url();
//        String skuStr = item.getSku_no();
//        String skuState = item.getStatus();
//        int scanState = item.getScanState();
//        boolean isAlter = item.isAlter();
//
//
//        switch (showType) {
//            case SHOW_HAS_ALTER:{
//                holder.lySkustate.setVisibility(View.VISIBLE);
//            }break;
//            case SHOW_NO_ALTER:{
//                holder.lySkustate.setVisibility(View.GONE);
//            }break;
//            default:
//                break;
//        }
//        switch (showImageType) {
//            case SHOW_HAS_IMAGE:{
//                Zlog.ii("lxm ss goodsadpter:11111111111");
//                holder.lyImage.setVisibility(View.VISIBLE);
//                loadImage(holder.imgGoods, imagUrl);
//            }break;
//            case SHOW_NO_IMAGE: {
//                Zlog.ii("lxm ss goodsadpter:222222");
//                holder.lyImage.setVisibility(View.GONE);
//            }break;
//            default:
//                break;
//        }
//
//        if (TextUtils.isEmpty(skuStr)) {
//            holder.txtSku.setText("");
//        } else {
//            holder.txtSku.setText(skuStr);
//        }
//        if (TextUtils.isEmpty(skuState)) {
//            holder.txtSkustate.setText("");
//        } else {
//            holder.txtSkustate.setText(skuState);
//        }
//
//        Zlog.ii("lxm setListViewHeightBasedOnChildren:1   " + holder.lyGoodListItem.getLayoutParams().height);
//        switch (scanState) {
//
//            case Goods.SCAN_STATE_DEFAULT: {
//                holder.imgState.setVisibility(View.GONE);
//                holder.txtState.setVisibility(View.GONE);
//            }
//            break;
//            case Goods.SCAN_STATE_FAILED: {
//                holder.imgState.setVisibility(View.GONE);
//                holder.txtState.setVisibility(View.VISIBLE);
//            }
//            break;
//            case Goods.SCAN_STATE_SUCCEED: {
//                holder.imgState.setVisibility(View.VISIBLE);
//                holder.txtState.setVisibility(View.GONE);
//            }
//            break;
//
//            default:
//                holder.imgState.setVisibility(View.GONE);
//                holder.txtState.setVisibility(View.GONE);
//                break;
//        }
//        if (isAlter) {
//            holder.btnAlter.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (onAlterListner != null) {
//                        onAlterListner.clickAlter(position);
//                    }
//                }
//            });
//        }else {
//            holder.btnAlter.setBackgroundResource(R.color.gray_light);
//        }
//        return view;
//    }
//
//    private void loadImage(FrescoImageView mImageView, String imageUrl) {
//
//        ImageUtils.loadImage(mImageView,imageUrl,false,R.mipmap.logo);
//    }
//
//    static class ViewHolder {
////        @BindView(R.id.ly_skustate_prompt)
////        LinearLayout lySkustatePrompt;
////        @BindView(R.id.txt_state_prompt)
////        TextView txtStatePrompt;
//        @BindView(R.id.ly_good_list_item)
//        LinearLayout lyGoodListItem;
//        @BindView(R.id.ly_navigation)
//        LinearLayout lyNavigation;
//        @BindView(R.id.view_line)
//        View viewLine;
//        @BindView(R.id.ly_image)
//        LinearLayout lyImage;
//        @BindView(R.id.img_goods)
//        FrescoImageView imgGoods;
//        @BindView(R.id.txt_sku)
//        TextView txtSku;
//        @BindView(R.id.txt_skustate)
//        TextView txtSkustate;
//        @BindView(R.id.ly_skustate)
//        LinearLayout lySkustate;
//        @BindView(R.id.btn_alter)
//        Button btnAlter;
//        @BindView(R.id.img_state)
//        ImageView imgState;
//        @BindView(R.id.txt_state)
//        TextView txtState;
//
//        ViewHolder(View view) {
//            ButterKnife.bind(this, view);
//        }
//    }
//
//    public void setOnAlterListner(OnAlterClickListner onAlterListner) {
//        this.onAlterListner = onAlterListner ;
//    }
//
//}
//
