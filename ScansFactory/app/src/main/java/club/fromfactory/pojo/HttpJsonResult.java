package club.fromfactory.pojo;

import club.fromfactory.baselibrary.model.NoProguard;

/**
 * Created by lxm on 2016/11/18.
 *
 * 用于解析数据
 */

public class HttpJsonResult<T> implements NoProguard {

    private String message ;
    private String msg ;
    private String status ;

    private T data ;
    private boolean is_success ;


    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean is_success() {
        return is_success;
    }

    public void setIs_success(boolean is_success) {
        this.is_success = is_success;
    }

    @Override
    public String toString() {
        return "HttpJsonResult{" +
                "message='" + message + '\'' +
                ", status='" + status + '\'' +
                ", data=" + data +
                ", is_success=" + is_success +
                '}';
    }
}
