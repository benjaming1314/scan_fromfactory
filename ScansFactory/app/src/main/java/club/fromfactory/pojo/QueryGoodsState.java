package club.fromfactory.pojo;

import club.fromfactory.baselibrary.model.NoProguard;

/**
 * Created by lxm on 2017/3/6.
 */

public class QueryGoodsState implements NoProguard {

    private String name ;
    private String nameValue ;

    private boolean isSelect  = false;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameValue() {
        return nameValue;
    }

    public void setNameValue(String nameValue) {
        this.nameValue = nameValue;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

    @Override
    public String toString() {
        return "QueryGoodsState{" +
                "name='" + name + '\'' +
                ", nameValue='" + nameValue + '\'' +
                ", isSelect=" + isSelect +
                '}';
    }
}
