package club.fromfactory.deeplink

import android.content.Context
import android.net.Uri
import android.os.Build
import club.fromfactory.FFApplication
import club.fromfactory.utils.UriUtils
import club.fromfactory.baselibrary.utils.ThreadUtils
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * 延时deepLink管理
 *
 * @author nichenjian
 * @date 2018/8/28
 */
class DeferredAppLinkManager(val context: Context) {
    // 需要打开的 appLink
    private var deferredAppLink: Uri? = null
    private var isOpenAppLink = false
    private var isIntervalExecuted = false

    companion object {
        const val TIME_OUT = 2L
        const val DEFAULT_FACEBOOK_ARGS = "utm_source=facebook&utm_medium=dpa"
        /**
         * 待处理的链接
         */
        var pendingUrl: Uri? = null
    }

    /**
     * 同时请求 google 的链接和 facebook 的链接
     * google的优先级最高，其次是 facebook
     */
    fun handleAppLink() {

        // 如果存在需要打开的链接，直接打开
        if (pendingUrl != null) {
            openUrl(context, pendingUrl!!)
            return
        }

        ThreadUtils.runOnUi({
            if (deferredAppLink != null && !isOpenAppLink) {
                isIntervalExecuted = true
                openUrl(context, deferredAppLink!!)
            }
        }, TIME_OUT, TimeUnit.SECONDS)
    }

    private fun buildUserAgent(): String {
        val systemLocal = FFApplication.getInstance().getSystemDefaultLocal()
        val local = if (systemLocal.isNotEmpty()) systemLocal else Locale.getDefault()
        return "(Android ${Build.VERSION.RELEASE}; $local; ${Build.MODEL}; Build/${Build.ID}; Proxy)"
    }


    private inline fun handleAppLink(appLink: Uri) {
        val url = appLink.toString()
        deferredAppLink = if (!url.contains("utm_source")) {
            Uri.parse("$url${if (url.contains("?")) "&" else "?"}$DEFAULT_FACEBOOK_ARGS")
        } else {
            appLink
        }
        // 查看当前是否已经执行过延时检查
        if (!isOpenAppLink && isIntervalExecuted) {
            openUrl(context, deferredAppLink!!)
        }
    }

    /**
     * 打开链接
     *
     * @param context 上下文
     * @param uri 需要打开的链接
     */
    private fun openUrl(context: Context, uri: Uri) {
        isOpenAppLink = true
        // 打开链接
        UriUtils.openUrl(context, uri.toString())
    }
}
