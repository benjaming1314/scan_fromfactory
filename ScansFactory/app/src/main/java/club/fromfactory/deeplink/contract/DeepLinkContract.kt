package club.fromfactory.deeplink.contract

import club.fromfactory.baselibrary.presenter.IPresenter
import club.fromfactory.baselibrary.view.IBaseMVPView

class DeepLinkContract {

    interface View : IBaseMVPView<Presenter> {

    }

    interface Presenter : IPresenter<View> {

    }
}