package club.fromfactory.deeplink

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import club.fromfactory.R
import club.fromfactory.utils.UriUtils
import club.fromfactory.baselibrary.view.BaseMVPActivity
import club.fromfactory.baselibrary.utils.PreferenceStorageUtils
import club.fromfactory.baselibrary.utils.StringUtils
import club.fromfactory.deeplink.contract.DeepLinkContract
import club.fromfactory.deeplink.presenter.DeepLinkPresenter
import club.fromfactory.ui.splash.SplashActivity

class DeepLinkActivity : BaseMVPActivity<DeepLinkContract.Presenter>(), DeepLinkContract.View{

    override fun clickKeyCodeBack() {
        finish()
    }

    override fun getLayoutResId(): Int = R.layout.activity_deeplink

    override fun useCustomStatusBar(): Boolean  = false

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        handleDeepLink(intent)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        handleDeepLink(intent)
    }


    /**
     * 处理deepLink的入口方法
     */
    private fun handleDeepLink(intent: Intent?) {
        var source = Source.Link
        var url = intent?.data

        // 当前的url不为空 打开对应的链接
        if (url != null && StringUtils.isNotBlank(url.toString())) {
            if (isFirstOpen()) {
                // 首次打开跳转到deepLink页面，同步webView的cookie
                DeferredAppLinkManager.pendingUrl = url
                startActivity(Intent(this, SplashActivity::class.java))
            } else {
                openUrl(this, url)
            }
        }
        finish()
    }

    override fun isNeedOpenMainActivityWhenFinish(): Boolean = false

    /**
     * 是否首次打开
     *
     * @return boolean 是否首次打开
     */
    private fun isFirstOpen(): Boolean {
        return PreferenceStorageUtils.getInstance().isFirstInstall
    }

    /**
     * 打开链接
     *
     * @param uri 需要打开的链接
     */
    fun openUrl(context: Context, uri: Uri) {
        // 打开链接
        UriUtils.openUrl(context, uri.toString())
    }

    /**
     * 链接来源
     */
    internal enum class Source(name: String?)  {
        /**
         * 外部链接
         */
        Link("link"),
        /**
         * app推广链接
         */
        AppLink("applink"),
        /**
         * push打开
         */
        Push("push")
    }

    override fun createPresenter(): DeepLinkContract.Presenter = DeepLinkPresenter(this)


}