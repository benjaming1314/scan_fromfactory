package club.fromfactory.deeplink.presenter

import club.fromfactory.baselibrary.presenter.BasePresenter
import club.fromfactory.deeplink.contract.DeepLinkContract

class DeepLinkPresenter(view: DeepLinkContract.View) :BasePresenter<DeepLinkContract.View>(view), DeepLinkContract.Presenter {
}