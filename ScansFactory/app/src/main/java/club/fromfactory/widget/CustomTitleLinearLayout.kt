package club.fromfactory.widget

import android.content.Context
import android.graphics.drawable.Drawable
import android.text.TextUtils
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import club.fromfactory.FFApplication
import club.fromfactory.R
import kotlinx.android.synthetic.main.layout_title_custom.view.*

class CustomTitleLinearLayout : FrameLayout {

    private var mListener: CustomTitleListener? = null

    private var mTitleLeftStr: String? = null
    private var mDrawableLeft: Drawable? = null
    private var mTitleCenterStr: String? = null
    private var mDrawableCenter: Drawable? = null
    private var mTitleRightStr: String? = null
    private var mBgColor = DEFAULT_BG_COLOR
    private var mTitleColor = DEFAULT_TITLE_COLOR
    private var mLeftTitleColor = DEFAULT_LEFT_TITLE_COLOR

    companion object {

        private val DEFAULT_BG_COLOR = FFApplication.getInstance().resources.getColor(R.color.color_3B3B3B)
        private val DEFAULT_LEFT_TITLE_COLOR = FFApplication.getInstance().resources.getColor(R.color.color_ffffff)
        private val DEFAULT_TITLE_COLOR = FFApplication.getInstance().resources.getColor(R.color.color_ffffff)
    }

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attributeSet: AttributeSet?) : super(context, attributeSet) {
        initAttr(context, attributeSet)
        init()
    }

    private fun initAttr(context: Context, attrs: AttributeSet?) {
        if (attrs == null) {
            return
        }
        val a = context.obtainStyledAttributes(attrs, R.styleable.TitleItemCustom) ?: return
        val n = a.indexCount
        for (i in 0 until n) {
            val attr = a.getIndex(i)
            if (attr == R.styleable.TitleItemCustom_imageleft) {
                mDrawableLeft = a.getDrawable(attr)
            } else if (attr == R.styleable.TitleItemCustom_txt_center) {
                mTitleCenterStr = a.getString(attr)
            } else if (attr == R.styleable.TitleItemCustom_txt_left) {
                mTitleLeftStr = a.getString(attr)
            } else if (attr == R.styleable.TitleItemCustom_titleright) {
                mTitleRightStr = a.getString(attr)
            } else if (attr == R.styleable.TitleItemCustom_imagecenter) {
                mDrawableCenter = a.getDrawable(attr)
            } else if (attr == R.styleable.TitleItemCustom_bgcolor) {
                mBgColor = a.getColor(attr, DEFAULT_BG_COLOR)
            } else if (attr == R.styleable.TitleItemCustom_titlecolor) {
                mTitleColor = a.getColor(attr, DEFAULT_LEFT_TITLE_COLOR)
            } else if (attr == R.styleable.TitleItemCustom_lefttitlecolor) {
                mLeftTitleColor = a.getColor(attr, DEFAULT_TITLE_COLOR)
            }
        }
        a.recycle()
    }

    private fun init() {
        LayoutInflater.from(context).inflate(R.layout.layout_title_custom, this)

        if (mDrawableLeft != null) {
            layout_title_img_left.visibility = View.VISIBLE
            layout_title_img_left.setImageDrawable(mDrawableLeft)
        } else {
            layout_title_img_left.setVisibility(View.GONE)
        }
        //修改下逻辑
        if (mDrawableCenter != null) {
            layout_title_logo.visibility = View.VISIBLE
            layout_title_txt_title_center.visibility = View.GONE
            layout_title_logo.setImageDrawable(mDrawableCenter)
        } else if (!TextUtils.isEmpty(mTitleCenterStr)) {
            layout_title_logo.setVisibility(View.GONE)
            layout_title_txt_title_center.setVisibility(View.VISIBLE)
            layout_title_txt_title_center.setText(mTitleCenterStr)
        } else {
            layout_title_logo.setVisibility(View.VISIBLE)
            layout_title_txt_title_center.setVisibility(View.GONE)
        }

        setTxtTitleLeft(mTitleLeftStr)

        if (!TextUtils.isEmpty(mTitleRightStr)) {
            layout_title_right.setText(mTitleRightStr)
        } else {
            layout_title_right.setText("")
        }

        layout_title_container.setBackgroundColor(mBgColor)
        layout_title_txt_title_left.setTextColor(mLeftTitleColor)
        layout_title_right.setTextColor(mLeftTitleColor)
        layout_title_txt_title_center.setTextColor(mTitleColor)
        setRightVisible(false)

        setTitleListener()
    }

    private fun setTitleListener() {
        layout_title_txt_title_center.setOnClickListener {
            mListener?.clickCenter()
        }
        layout_title_logo.setOnClickListener {
            mListener?.clickCenter()
        }
        layout_title_container_left.setOnClickListener {
            mListener?.clickLeft()
        }
        layout_title_ly_right.setOnClickListener {
            mListener?.clickRight()
        }

    }

    fun setTxtTitleLeft(titleLeft: String?) {
        if (!TextUtils.isEmpty(titleLeft)) {
            layout_title_txt_title_left.text = titleLeft
        } else {
            layout_title_txt_title_left.text = ""
        }
    }

    fun setRightView(layoutId: Int) {
        layout_title_ly_right.removeAllViews()
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(layoutId, layout_title_ly_right, false)
        layout_title_ly_right.addView(view)
    }

    fun setLeftTextVisible(isVisible: Boolean) {
        if (isVisible) {
            layout_title_txt_title_left.visibility = View.VISIBLE
        } else {
            layout_title_txt_title_left.visibility = View.GONE
        }
    }

    fun setTitleCenter(title: String?) {
        if (!TextUtils.isEmpty(title)) {
            layout_title_logo.visibility = View.GONE
            layout_title_txt_title_center.visibility = View.VISIBLE

            layout_title_txt_title_center.text = title
        } else {
            layout_title_txt_title_center.visibility = View.GONE
            layout_title_logo.visibility = View.VISIBLE
        }
    }

    fun setRightVisible(isVisible: Boolean) {
        if (isVisible) {
            layout_title_ly_right.visibility = View.VISIBLE
        } else {
            layout_title_ly_right.visibility = View.GONE
        }
    }

    fun setListener(listener: CustomTitleListener) {
        mListener = listener
    }


    abstract class CustomTitleListener {

        open fun clickLeft() {}

        open fun clickCenter() {}

        open fun clickRight() {}

    }


}