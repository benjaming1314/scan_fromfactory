package club.fromfactory.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

import club.fromfactory.R;
import club.fromfactory.utils.FontManager;
import club.fromfactory.baselibrary.utils.LayoutUtils;

/**
 * 渲染自定义字体的TextView
 *
 * @author lxm
 * @date 2018/4/25/025
 */
public class IconFontTextView extends TextView {

    private boolean isSupportRtl = false;

    public IconFontTextView(Context context) {
        this(context, null);
    }

    public IconFontTextView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, -1);
    }

    public IconFontTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.IconFontTextView);
        isSupportRtl = ta.getBoolean(R.styleable.IconFontTextView_supportRtl, true);
        ta.recycle();
        this.setTypeface(FontManager.getTypeface(context, FontManager.FONTAWESOME));
    }

    /**
     * 适配rtl的布局，将图标进行镜像翻转
     */
    private void adjustRtlLayout(Canvas canvas) {
        boolean isRtlLayout = isSupportRtl && LayoutUtils.isRtlLayout();
        if (isRtlLayout) {
            canvas.scale(-1, 1, getWidth() / 2, getHeight() / 2);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        adjustRtlLayout(canvas);
        super.onDraw(canvas);
    }
}
