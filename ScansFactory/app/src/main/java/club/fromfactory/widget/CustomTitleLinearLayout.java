//package club.fromfactory.widget;
//
//import android.content.Context;
//import android.content.res.TypedArray;
//import android.graphics.drawable.Drawable;
//import android.text.TextUtils;
//import android.util.AttributeSet;
//import android.view.Gravity;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.widget.FrameLayout;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//
//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
//import club.fromfactory.FFApplication;
//import club.fromfactory.Utils.FontManager;
//import club.fromfactory.baselibrary.utils.StringUtils;
//import club.fromfactory.R;
//
///**
// * Created by lxm on 2017/2/21.
// */
//
//public class CustomTitleLinearLayout extends LinearLayout {
//
//    private static final int DEFAULT_TITLE_BG = FFApplication.getInstance().getResources().getColor(R.color.color_3B3B3B);
//    private static final int DEFAULT_TXT_COLOR = FFApplication.getInstance().getResources().getColor(R.color.color_white);
//
//    @BindView(R.id.layout_title)
//    protected RelativeLayout mRlTitle;
//    @BindView(R.id.layout_title_txt_center)
//    protected TextView mTxtCenter;
//    @BindView(R.id.layout_title_img_center)
//    protected ImageView mImgCenter;
//    @BindView(R.id.layout_title_txt_left)
//    protected TextView mTxtLeft;
//    @BindView(R.id.layout_title_img_left)
//    protected ImageView mImgLeft;
//    @BindView(R.id.layout_title_txt_right)
//    protected TextView mTxtRight;
//    @BindView(R.id.layout_title_img_right)
//    protected ImageView mImgRight;
//    @BindView(R.id.layout_title_fl_center)
//    FrameLayout mFlCenter;
//    @BindView(R.id.layout_title_fl_left)
//    protected FrameLayout mFlLeft;
//    @BindView(R.id.layout_title_fl_right)
//    protected FrameLayout mFlRight;
//    //    @BindView(R.id.layout_title_ly_center)
////    LinearLayout mLyCenter;
////    @BindView(R.id.layout_title_ly_left)
////    LinearLayout mLyLeft;
////    @BindView(R.id.layout_title_ly_right)
////    LinearLayout mLyRight;
//    private Context mContext;
//
//    private int mTitleBgColor = DEFAULT_TITLE_BG;
//
//    private Drawable mDrawableLeft;
//    private Drawable mDrawableCenter;
//    private Drawable mDrawableRight;
//
//    private String mLeftStr;
//    private String mCenterStr;
//    private String mRightStr;
//
//    private boolean mIsLeftImage;
//    private boolean mIsCenterImage;
//    private boolean mIsRightImage;
//
//    private int mTxtLeftColor = DEFAULT_TXT_COLOR;
//    private int mTxtCenterColor = DEFAULT_TXT_COLOR;
//    private int mTxtRightColor = DEFAULT_TXT_COLOR;
//
//    private int mRightLayoutRes;
//
//    public CustomTitleLinearLayout(Context context) {
//        this(context, null);
//    }
//
//    public CustomTitleLinearLayout(Context context, AttributeSet attrs) {
//        this(context, attrs, -1);
//    }
//
//    public CustomTitleLinearLayout(Context context, AttributeSet attrs, int defStyleAttr) {
//        super(context, attrs, defStyleAttr);
//        this.mContext = context;
//        initAttr(context, attrs);
//        init();
//    }
//
//    //    public ScanTitleLinearlayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
////        super(context, attrs, defStyleAttr, defStyleRes);
////        this.mContext = context;
////        init(attrs);
////    }
//
//    private void initAttr(Context context, AttributeSet attrs) {
//        if (attrs == null) {
//            return;
//        }
//        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.TitleItemCustom);
//        if (a == null) {
//            return;
//        }
//        int n = a.getIndexCount();
//        for (int i = 0; i < n; i++) {
//            int attr = a.getIndex(i);
//            if (attr == R.styleable.TitleItemCustom_title_background) {
//                mTitleBgColor = a.getInteger(attr, DEFAULT_TITLE_BG);
//            } else if (attr == R.styleable.TitleItemCustom_image_left) {
//                mDrawableLeft = a.getDrawable(attr);
//            } else if (attr == R.styleable.TitleItemCustom_txt_left) {
//                mLeftStr = a.getString(attr);
//            } else if (attr == R.styleable.TitleItemCustom_txt_left_color) {
//                mTxtLeftColor = a.getInteger(attr, DEFAULT_TXT_COLOR);
//            } else if (attr == R.styleable.TitleItemCustom_left_is_txt_image) {
//                mIsLeftImage = a.getBoolean(attr, false);
//            } else if (attr == R.styleable.TitleItemCustom_image_center) {
//                mDrawableCenter = a.getDrawable(attr);
//            } else if (attr == R.styleable.TitleItemCustom_txt_center) {
//                mCenterStr = a.getString(attr);
//            } else if (attr == R.styleable.TitleItemCustom_txt_center_color) {
//                mTxtCenterColor = a.getInteger(attr, DEFAULT_TXT_COLOR);
//            } else if (attr == R.styleable.TitleItemCustom_center_is_txt_image) {
//                mIsCenterImage = a.getBoolean(attr, false);
//            } else if (attr == R.styleable.TitleItemCustom_image_right) {
//                mDrawableRight = a.getDrawable(attr);
//            } else if (attr == R.styleable.TitleItemCustom_txt_right) {
//                mRightStr = a.getString(attr);
//            } else if (attr == R.styleable.TitleItemCustom_txt_right_color) {
//                mTxtRightColor = a.getInteger(attr, DEFAULT_TXT_COLOR);
//            } else if (attr == R.styleable.TitleItemCustom_right_is_txt_image) {
//                mIsRightImage = a.getBoolean(attr, false);
//            } else if (attr == R.styleable.TitleItemCustom_right_layout) {
//                mRightLayoutRes = a.getResourceId(attr, 0);
//            }
//        }
//        a.recycle();
//    }
//
//    @SuppressWarnings("ResourceType")
//    private void init() {
//
//        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_title_custom, this);
//        ButterKnife.bind(view, this);
//
//        mRlTitle.setBackgroundColor(mTitleBgColor);
//        //左边
//        if (mDrawableLeft != null) {
//            mImgLeft.setImageDrawable(mDrawableLeft);
//            mImgLeft.setVisibility(View.VISIBLE);
//        } else {
//            mImgLeft.setVisibility(View.GONE);
//        }
//
//        if (StringUtils.isNotBlank(mLeftStr)) {
//            mTxtLeft.setTextColor(mTxtLeftColor);
//            mTxtLeft.setVisibility(View.VISIBLE);
//            mTxtLeft.setText(mLeftStr);
//            if (mIsLeftImage) {
//                mTxtLeft.setTypeface(FontManager.getTypeface(mContext, FontManager.FONTAWESOME));
//            }
//        } else {
//            mTxtLeft.setVisibility(View.GONE);
//        }
//
//        //中间
//        if (mDrawableCenter != null) {
//            mImgCenter.setImageDrawable(mDrawableCenter);
//            mImgCenter.setVisibility(View.VISIBLE);
//        } else {
//            mImgLeft.setVisibility(View.GONE);
//        }
//
//        if (StringUtils.isNotBlank(mCenterStr)) {
//            mTxtCenter.setTextColor(mTxtCenterColor);
//            mTxtCenter.setVisibility(View.VISIBLE);
//            mTxtCenter.setText(mCenterStr);
//            if (mIsCenterImage) {
//                mTxtCenter.setTypeface(FontManager.getTypeface(mContext, FontManager.FONTAWESOME));
//            }
//        } else {
//            mTxtCenter.setVisibility(View.GONE);
//        }
//
//        //右边
//        if (mDrawableRight != null) {
//            mImgRight.setImageDrawable(mDrawableLeft);
//            mImgRight.setVisibility(View.VISIBLE);
//        } else {
//            mImgLeft.setVisibility(View.GONE);
//        }
//        if (StringUtils.isNotBlank(mRightStr)) {
//            mTxtRight.setTextColor(mTxtRightColor);
//            mTxtRight.setVisibility(View.VISIBLE);
//            mTxtRight.setText(mRightStr);
//            if (mIsRightImage) {
//                mTxtRight.setTypeface(FontManager.getTypeface(mContext, FontManager.FONTAWESOME));
//            }
//        } else {
//            mTxtRight.setVisibility(View.GONE);
//        }
//
//        if (mRightLayoutRes != 0) {
//            setRightView(mRightLayoutRes);
//        }
//    }
//
//    public void setTitleCenter(String title) {
//        if (!TextUtils.isEmpty(title)) {
//            mTxtCenter.setVisibility(View.VISIBLE);
//            mTxtCenter.setText(title);
//        }
//    }
//
//    public void setTitleCenter(String title, boolean isTxtImg) {
//        if (!TextUtils.isEmpty(title)) {
//            mTxtCenter.setVisibility(View.VISIBLE);
//            mTxtCenter.setText(title);
//        }
//    }
//
//    public void setTitleLeft(String title) {
//        if (!TextUtils.isEmpty(title)) {
//            mTxtLeft.setVisibility(View.VISIBLE);
//            mTxtLeft.setText(title);
//        }
//    }
//
//    public void setTitleLeft(String title, boolean isTxtImg) {
//        if (!TextUtils.isEmpty(title)) {
//            mTxtLeft.setVisibility(View.VISIBLE);
//            mTxtLeft.setText(title);
//            if (isTxtImg) {
//                mTxtLeft.setTypeface(FontManager.getTypeface(mContext, FontManager.FONTAWESOME));
//            }
//        }
//    }
//
//    public void setTitleRight(String title) {
//        if (!TextUtils.isEmpty(title)) {
//            mTxtRight.setVisibility(View.VISIBLE);
//            mTxtRight.setText(title);
//        }
//    }
//
//    public void setTitleRight(String title, boolean isTxtImg) {
//        if (!TextUtils.isEmpty(title)) {
//            mTxtRight.setVisibility(View.VISIBLE);
//            mTxtRight.setText(title);
//            if (isTxtImg) {
//                mTxtRight.setTypeface(FontManager.getTypeface(mContext, FontManager.FONTAWESOME));
//            }
//        }
//    }
//
//    public void setLeftImg(int res) {
//        mTxtLeft.setVisibility(View.GONE);
//        mImgLeft.setVisibility(View.VISIBLE);
//        mImgLeft.setImageResource(res);
//    }
//
//    public void setCenterView(int layoutId) {
//        mTxtCenter.setVisibility(View.GONE);
//        mImgCenter.setVisibility(View.GONE);
//
//        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        View view = inflater.inflate(layoutId, mFlCenter, false);
//
//        view.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (mListener != null) {
//                    mListener.clickCenter();
//                }
//            }
//        });
//        mFlCenter.addView(view);
//    }
//
//    public void setCenterView(View view) {
//        mTxtCenter.setVisibility(View.GONE);
//        mImgCenter.setVisibility(View.GONE);
//
//        view.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (mListener != null) {
//                    mListener.clickCenter();
//                }
//            }
//        });
//        mFlCenter.addView(view);
//    }
//
//    public void setLeftView(int layoutId) {
//        mTxtLeft.setVisibility(View.GONE);
//        mImgLeft.setVisibility(View.GONE);
//
//        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        View view = inflater.inflate(layoutId, mFlLeft, false);
//
//        view.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (mListener != null) {
//                    mListener.clickLeft();
//                }
//            }
//        });
//        mFlLeft.addView(view);
//    }
//
//    public void setLeftView(View view) {
//        mTxtLeft.setVisibility(View.GONE);
//        mImgLeft.setVisibility(View.GONE);
//        view.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (mListener != null) {
//                    mListener.clickLeft();
//                }
//            }
//        });
//        mFlLeft.addView(view);
//    }
//
//    public void setRightView(int layoutId) {
//        mTxtRight.setVisibility(View.GONE);
//        mImgRight.setVisibility(View.GONE);
//        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        View view = inflater.inflate(layoutId, mFlRight, false);
//        view.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (mListener != null) {
//                    mListener.clickRight();
//                }
//            }
//        });
//        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(
//                FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
//        lp.gravity = Gravity.RIGHT;
//
//        mFlRight.addView(view, lp);
//    }
//
//    public void setRightView(View view) {
//        mTxtRight.setVisibility(View.GONE);
//        mImgRight.setVisibility(View.GONE);
//        view.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (mListener != null) {
//                    mListener.clickRight();
//                }
//            }
//        });
//        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(
//                FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
//        lp.gravity = Gravity.RIGHT;
//        mFlRight.addView(view);
//    }
//
//    private CustomTitleListener mListener;
//
//    public void setListener(CustomTitleListener listener) {
//        mListener = listener;
//    }
//
//    @OnClick({R.id.layout_title_ly_center, R.id.layout_title_txt_center, R.id.layout_title_img_center, R.id.layout_title_ly_left, R.id.layout_title_txt_left, R.id.layout_title_img_left, R.id.layout_title_ly_right, R.id.layout_title_txt_right, R.id.layout_title_img_right})
//    public void onViewClicked(View view) {
//        switch (view.getId()) {
//            case R.id.layout_title_ly_center:
//            case R.id.layout_title_txt_center:
//            case R.id.layout_title_img_center:
//                if (mListener != null) {
//                    mListener.clickCenter();
//                }
//                break;
//            case R.id.layout_title_ly_left:
//            case R.id.layout_title_txt_left:
//            case R.id.layout_title_img_left:
//                if (mListener != null) {
//                    mListener.clickLeft();
//                }
//                break;
//            case R.id.layout_title_ly_right:
//            case R.id.layout_title_txt_right:
//            case R.id.layout_title_img_right:
//                if (mListener != null) {
//                    mListener.clickRight();
//                }
//                break;
//        }
//    }
//
//    public abstract static class CustomTitleListener {
//
//        public void clickLeft() {
//        }
//
//        ;
//
//        public void clickCenter() {
//        }
//
//        ;
//
//        public void clickRight() {
//        }
//
//        ;
//
//    }
//
//
//}
