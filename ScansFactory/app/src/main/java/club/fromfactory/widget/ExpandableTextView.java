package club.fromfactory.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.text.DynamicLayout;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import club.fromfactory.FFApplication;
import club.fromfactory.R;
import club.fromfactory.baselibrary.utils.StringUtils;

/**
 * @author lxm
 * @date 2018/5/10/010
 */
public class ExpandableTextView extends android.support.v7.widget.AppCompatTextView {

    public static final int STATE_SHRINK = 0;
    public static final int STATE_EXPAND = 1;
    private static final String ELLIPSIS_HINT = "......";
    private static final int MAX_LINES_ON_SHRINK = 5;
    private static final boolean SHOW_TO_EXPAND_HINT = true;
    private static final boolean SHOW_TO_SHRINK_HINT = false;
    private static final String GAP_TO_EXPAND_HINT = " ";
    private static final String GAP_TO_SHRINK_HINT = " ";
    private static final boolean DEFAULT_USER_LINK = false;

    private String mEllipsisHint;
    private String mToExpandHint = "";
    private String mToShrinkHint;
    private boolean mShowToExpandHint = SHOW_TO_EXPAND_HINT;
    private boolean mShowToShrinkHint = SHOW_TO_SHRINK_HINT;
    private String mGapToExpandHint = GAP_TO_EXPAND_HINT;
    private String mGapToShrinkHint = GAP_TO_SHRINK_HINT;
    private static final int TO_EXPAND_HINT_COLOR = FFApplication.getInstance().getResources()
            .getColor(R.color.color_4a90e2);
    private static final int TO_SHRINK_HINT_COLOR = FFApplication.getInstance().getResources()
            .getColor(R.color.color_4a90e2);
    private static final int TO_EXPAND_HINT_COLOR_BG_PRESSED = FFApplication.getInstance()
            .getResources().getColor(R.color.color_4a90e2);
    private static final int TO_SHRINK_HINT_COLOR_BG_PRESSED = FFApplication.getInstance()
            .getResources().getColor(R.color.color_4a90e2);

    private int mToExpandHintColor = TO_EXPAND_HINT_COLOR;
    private int mToShrinkHintColor = TO_SHRINK_HINT_COLOR;
    private int mToExpandHintColorBgPressed = TO_EXPAND_HINT_COLOR_BG_PRESSED;
    private int mToShrinkHintColorBgPressed = TO_SHRINK_HINT_COLOR_BG_PRESSED;

    private boolean mIsUserLink = DEFAULT_USER_LINK ;

    private TouchableSpan mTouchableSpan;
    private BufferType mBufferType = BufferType.NORMAL;
    private Layout mLayout;
    private int mFutureTextViewWidth = 0;

    private int mCurrState = STATE_SHRINK;

    private int mMaxLinesOnShrink = MAX_LINES_ON_SHRINK;
    //  the original text of this view
    private CharSequence mOrigText;

    // Pattern for gathering http:// links from the Text
    private OnExpandListener mOnExpandListener;

    private List<Hyperlink> listOfLinks;

    private boolean isNeedShowFold = false;

    public ExpandableTextView(Context context) {
        this(context, null);
    }

    public ExpandableTextView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, -1);
    }

    public ExpandableTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAttr(context, attrs);
        init();
    }

    private void initAttr(Context context, AttributeSet attrs) {
        if (attrs == null) {
            return;
        }
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ExpandableTextView);
        if (a == null) {
            return;
        }
        int n = a.getIndexCount();
        for (int i = 0; i < n; i++) {
            int attr = a.getIndex(i);
            if (attr == R.styleable.ExpandableTextView_etv_MaxLinesOnShrink) {
                mMaxLinesOnShrink = a.getInteger(attr, MAX_LINES_ON_SHRINK);
            } else if (attr == R.styleable.ExpandableTextView_etv_EllipsisHint) {
                mEllipsisHint = a.getString(attr);
            } else if (attr == R.styleable.ExpandableTextView_etv_ToExpandHint) {
                mToExpandHint = a.getString(attr);
            } else if (attr == R.styleable.ExpandableTextView_etv_ToShrinkHint) {
                mToShrinkHint = a.getString(attr);
            } else if (attr == R.styleable.ExpandableTextView_etv_ToExpandHintShow) {
                mShowToExpandHint = a.getBoolean(attr, SHOW_TO_EXPAND_HINT);
            } else if (attr == R.styleable.ExpandableTextView_etv_ToShrinkHintShow) {
                mShowToShrinkHint = a.getBoolean(attr, SHOW_TO_SHRINK_HINT);
            } else if (attr == R.styleable.ExpandableTextView_etv_GapToExpandHint) {
                mGapToExpandHint = a.getString(attr);
            } else if (attr == R.styleable.ExpandableTextView_etv_GapToShrinkHint) {
                mGapToShrinkHint = a.getString(attr);
            } else if (attr == R.styleable.ExpandableTextView_etv_ToExpandHintColor) {
                mToExpandHintColor = a.getInteger(attr, TO_EXPAND_HINT_COLOR);
            } else if (attr == R.styleable.ExpandableTextView_etv_ToShrinkHintColor) {
                mToShrinkHintColor = a.getInteger(attr, TO_SHRINK_HINT_COLOR);
            } else if (attr == R.styleable.ExpandableTextView_etv_ToExpandHintColorBgPressed) {
                mToExpandHintColorBgPressed = a.getInteger(attr, TO_EXPAND_HINT_COLOR_BG_PRESSED);
            } else if (attr == R.styleable.ExpandableTextView_etv_ToShrinkHintColorBgPressed) {
                mToShrinkHintColorBgPressed = a.getInteger(attr, TO_SHRINK_HINT_COLOR_BG_PRESSED);
            } else if (attr == R.styleable.ExpandableTextView_etv_Text_Content) {
                mOrigText = a.getString(attr);
            } else if (attr == R.styleable.ExpandableTextView_etv_IsLink) {
                mIsUserLink = a.getBoolean(attr,DEFAULT_USER_LINK);
            }
        }
        a.recycle();
    }

    private void init() {
        mTouchableSpan = new TouchableSpan();
        setMovementMethod(LinkMovementMethod.getInstance());
        if (TextUtils.isEmpty(mEllipsisHint)) {
            mEllipsisHint = ELLIPSIS_HINT;
        }
        if (mOrigText != null) {
            setTextInternal();
        }
    }

    /**
     * used in ListView or RecyclerView to update ExpandableTextView
     */
    public void updateForRecyclerView(CharSequence text, int futureTextViewWidth, int expandState) {
        mFutureTextViewWidth = futureTextViewWidth;
        mCurrState = expandState;
        setTextInternal(text, mBufferType);
    }

    public void setTextContent(String content) {
        mOrigText = content;
        setTextInternal();
    }

    public int getExpandState() {
        return mCurrState;
    }

    private void setTextInternal(CharSequence text, BufferType type) {
        mOrigText = text;
        setTextInternal(type);

    }

    private void setTextInternal(BufferType type) {
        mBufferType = type;
        setTextInternal();
    }

    private void setTextInternal() {

        if (listOfLinks == null) {
            listOfLinks = new ArrayList<>();
        } else {
            listOfLinks.clear();
        }
        listOfLinks.addAll(filterWebLink(mOrigText));
        CharSequence newTextByConfig = getNewTextByConfig();
        setText(newTextByConfig);
    }

    /**
     * refresh and get a will-be-displayed text by current configuration
     *
     * @return get a will-be-displayed text
     */
    private CharSequence getNewTextByConfig() {

        if (TextUtils.isEmpty(mOrigText)) {
            return mOrigText;
        }

        TextPaint mTextPaint;
        int mTextLineCount = -1;
        int mLayoutWidth = 0;

        mLayout = getLayout();
        if (mLayout != null) {
            mLayoutWidth = mLayout.getWidth();
        }

        if (mLayoutWidth <= 0) {
            if (getWidth() == 0) {
                if (mFutureTextViewWidth == 0) {
                    return addLinkSpan(mOrigText);
                } else {
                    mLayoutWidth = mFutureTextViewWidth - getPaddingLeft() - getPaddingRight();
                }
            } else {
                mLayoutWidth = getWidth() - getPaddingLeft() - getPaddingRight();
            }
        }

        mTextPaint = getPaint();

        switch (mCurrState) {
            case STATE_SHRINK: {
                mLayout = new DynamicLayout(mOrigText, mTextPaint, mLayoutWidth,
                        Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
                mTextLineCount = mLayout.getLineCount();

                if (mTextLineCount <= mMaxLinesOnShrink) {
                    isNeedShowFold = false;
                    return addLinkSpan(mOrigText);
                }

                isNeedShowFold = true;
                int indexEnd = getValidLayout().getLineEnd(mMaxLinesOnShrink - 1);
                int indexStart = getValidLayout().getLineStart(mMaxLinesOnShrink - 1);

                int indexEndTrimmed = indexEnd
                        - getLengthOfString(mEllipsisHint)
                        - (mShowToExpandHint ? getLengthOfString(mToExpandHint) + getLengthOfString(
                        mGapToExpandHint) : 0);

                if (indexEndTrimmed <= indexStart) {
                    indexEndTrimmed = indexEnd;
                }

                int remainWidth = getValidLayout().getWidth() -
                        (int) (mTextPaint.measureText(
                                mOrigText.subSequence(indexStart, indexEndTrimmed).toString())
                                + 0.5);
                float widthTailReplaced = mTextPaint.measureText(getContentOfString(mEllipsisHint)
                        + (mShowToExpandHint ? (getContentOfString(mToExpandHint)
                        + getContentOfString(mGapToExpandHint)) : ""));

                int indexEndTrimmedRevised = indexEndTrimmed;
                int maxExtraOffset = indexEnd - indexEndTrimmedRevised + 1;
                if (remainWidth > widthTailReplaced) {

                    int extraOffset = 0;
                    int extraWidth = 0;

                    while (remainWidth > widthTailReplaced + extraWidth) {
                        extraOffset++;
                        if (indexEndTrimmed + extraOffset <= mOrigText.length()) {
                            extraWidth = (int) (mTextPaint.measureText(
                                    mOrigText.subSequence(indexEndTrimmed,
                                            indexEndTrimmed + extraOffset).toString()) + 0.5);
                        } else {
                            break;
                        }
                        //解决换行的BUG
                        if (extraOffset >= maxExtraOffset) {
                            break;
                        }
                    }
                    indexEndTrimmedRevised += extraOffset - 1;
                } else {
                    int extraOffset = 0;
                    int extraWidth = 0;
                    while (remainWidth + extraWidth < widthTailReplaced) {
                        extraOffset--;
                        if (indexEndTrimmed + extraOffset > indexStart) {
                            extraWidth = (int) (mTextPaint.measureText(mOrigText
                                    .subSequence(indexEndTrimmed + extraOffset, indexEndTrimmed)
                                    .toString()) + 0.5);
                        } else {
                            break;
                        }
                        //解决换行的BUG
                        if (extraOffset >= maxExtraOffset) {
                            break;
                        }
                    }
                    indexEndTrimmedRevised += extraOffset;
                }

                String fixText = removeEndLineBreak(
                        mOrigText.subSequence(0, indexEndTrimmedRevised));
                SpannableStringBuilder ssbShrink = new SpannableStringBuilder(fixText)
                        .append(mEllipsisHint);
                if (mShowToExpandHint) {
                    ssbShrink.append(getContentOfString(mGapToExpandHint) + getContentOfString(
                            mToExpandHint));
                    ssbShrink.setSpan(mTouchableSpan,
                            ssbShrink.length() - getLengthOfString(mToExpandHint),
                            ssbShrink.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
                addLinkListener(ssbShrink);
                return ssbShrink;
            }
            case STATE_EXPAND: {
                if (!mShowToShrinkHint) {
                    return addLinkSpan(mOrigText);
                }
                mLayout = new DynamicLayout(mOrigText, mTextPaint, mLayoutWidth,
                        Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);
                mTextLineCount = mLayout.getLineCount();

                if (mTextLineCount <= mMaxLinesOnShrink) {
                    isNeedShowFold = false;
                    return addLinkSpan(mOrigText);
                }

                isNeedShowFold = true;
                SpannableStringBuilder ssbExpand = new SpannableStringBuilder(mOrigText)
                        .append(mGapToShrinkHint).append(mToShrinkHint);
                addLinkListener(ssbExpand);
                ssbExpand.setSpan(mTouchableSpan,
                        ssbExpand.length() - getLengthOfString(mToShrinkHint), ssbExpand.length(),
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                return ssbExpand;
            }
        }
        return addLinkSpan(mOrigText);
    }


    private SpannableStringBuilder addLinkSpan(CharSequence text) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(text);
        addLinkListener(spannableStringBuilder);

        return spannableStringBuilder;
    }

    private void addLinkListener(SpannableStringBuilder linkableText) {

        if (!mIsUserLink) {
            return ;
        }

        List<Hyperlink> listOfLinks = new ArrayList<>();

        Pattern hyperLinksPattern = Pattern
                .compile("([Hh][tT][tT][pP][sS]?:\\/\\/[^ ,'\">\\]\\)]*[^\\. ,'\">\\]\\)])");

        gatherLinks(listOfLinks, linkableText, hyperLinksPattern);

        for (int i = 0; i < listOfLinks.size(); i++) {
            Hyperlink linkSpec = listOfLinks.get(i);
            /*
             * this process here makes the Clickable Links from the text
             */
            linkableText.setSpan(linkSpec.span, linkSpec.start, linkSpec.end,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
    }

    private Layout getValidLayout() {
        return mLayout != null ? mLayout : getLayout();
    }

    private String removeEndLineBreak(CharSequence text) {
        String str = text.toString();
        while (str.endsWith("\n")) {
            str = str.substring(0, str.length() - 1);
        }
        return str;
    }

    private int getLengthOfString(String string) {
        if (StringUtils.isNull(string)) {
            return 0;
        }
        return string.length();
    }

    private String getContentOfString(String string) {
        if (StringUtils.isNull(string)) {
            return "";
        }
        return string;
    }

    private class TouchableSpan extends ClickableSpan {

        private boolean mIsPressed;

        public void setPressed(boolean isSelected) {
            mIsPressed = isSelected;
        }

        @Override
        public void onClick(View textView) {

            switch (mCurrState) {
                case STATE_SHRINK:
                    mCurrState = STATE_EXPAND;
                    if (mOnExpandListener != null) {
                        mOnExpandListener.onExpand(ExpandableTextView.this);
                    }
                    break;
                case STATE_EXPAND:
                    mCurrState = STATE_SHRINK;
                    if (mOnExpandListener != null) {
                        mOnExpandListener.onShrink(ExpandableTextView.this);
                    }
                    break;
            }
            setTextInternal();

        }

        @Override
        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            switch (mCurrState) {
                case STATE_SHRINK:
                    ds.setColor(mToExpandHintColor);
                    ds.bgColor = mIsPressed ? mToExpandHintColorBgPressed : 0;
                    break;
                case STATE_EXPAND:
                    ds.setColor(mToShrinkHintColor);
                    ds.bgColor = mIsPressed ? mToShrinkHintColorBgPressed : 0;
                    break;
            }
            ds.setUnderlineText(false);
        }
    }

    private List<Hyperlink> filterWebLink(CharSequence s) {
        List<Hyperlink> links = new ArrayList<>();
        Pattern hyperLinksPattern = Pattern
                .compile("([Hh][tT][tT][pP][sS]?:\\/\\/[^ ,'\">\\]\\)]*[^\\. ,'\">\\]\\)])");

        gatherLinks(links, s, hyperLinksPattern);
        return links;
    }


    private void gatherLinks(List<Hyperlink> links,
            CharSequence s, Pattern pattern) {
        // Matcher matching the pattern
        Matcher m = pattern.matcher(s);

        while (m.find()) {
            int start = m.start();
            int end = m.end();
            Hyperlink spec = new Hyperlink();
            spec.textSpan = s.subSequence(start, end);
            spec.span = new InternalURLSpan(spec.textSpan.toString(), start);
            spec.start = start;
            spec.end = end;

            links.add(spec);
        }
    }

    /*
     * This is class which gives us the clicks on the links which we then can use.
     */
    public class InternalURLSpan extends ClickableSpan {

        private int start;
        private String clickedSpan;

        public InternalURLSpan(String clickedSpan, int start) {
            InternalURLSpan.this.start = start;
            InternalURLSpan.this.clickedSpan = clickedSpan;
        }

        @Override
        public void onClick(View textView) {
            for (int i = 0; i < listOfLinks.size(); i++) {
                Hyperlink hyperlink = listOfLinks.get(i);
                if (hyperlink.start == start) {
                    clickedSpan = String.valueOf(hyperlink.textSpan);
                }
            }

            if (mOnExpandListener != null) {
                mOnExpandListener.onTextLinkClick(clickedSpan);
            }
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setColor(mToShrinkHintColor);
        }
    }

    public boolean isShowFoldHint() {
        return isNeedShowFold;
    }

    class Hyperlink {

        CharSequence textSpan;
        InternalURLSpan span;
        int start;
        int end;
    }

    public void setExpandListener(OnExpandListener listener) {
        mOnExpandListener = listener;
    }

    public interface OnExpandListener {

        void onExpand(ExpandableTextView view);

        void onShrink(ExpandableTextView view);

        void onTextLinkClick(String clickedString);
    }


}
