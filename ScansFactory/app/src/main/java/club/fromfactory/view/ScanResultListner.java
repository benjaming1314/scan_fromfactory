package club.fromfactory.view;

/**
 * Created by lxm on 2017/2/21.
 */

public interface ScanResultListner {

    void scanSucceed(String result);
    void scanFailed(String message);

    void showDialog(String str);

    /**
     * 显示加载提示视图
     */
    void showLoadingView();

    /**
     * 隐藏加载视图
     */
    void hideLoadingView();

}
