package club.fromfactory.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by lxm on 2017/4/10.
 */

public class MeasureTextView extends TextView {


    private Context mContext ;

    public MeasureTextView(Context context) {
        this(context,null);
    }

    public MeasureTextView(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    public MeasureTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr,0);
    }

    public MeasureTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.mContext = context ;
    }

//    @Override
//    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//
//        Layout layout = getLayout();
//        if (layout != null) {
//            int height = (int) Math.ceil(getMaxLineHeight(this.getText().toString()))
//                    + getCompoundPaddingTop() + getCompoundPaddingBottom();
//            int width = getMeasuredWidth();
//            setMeasuredDimension(width, height);
//        }
//    }
//
//    private float getMaxLineHeight(String str) {
//        Zlog.ii("lxm getMaxLineHeight:" +str);
//        float height = 0.0f;
////        float screenW = ((Activity)mContext).getWindowManager().getDefaultDisplay().getWidth();
//        float paddingLeft = ((LinearLayout)this.getParent()).getPaddingLeft();
//        float paddingReft = ((LinearLayout)this.getParent()).getPaddingRight();
////这里具体this.getPaint()要注意使用，要看你的TextView在什么位置，这个是拿TextView父控件的Padding的，为了更准确的算出换行
//        int line = (int) Math.ceil( (this.getPaint().measureText(str)/(getMeasuredWidth()-paddingLeft-paddingReft)));
//        Zlog.ii("lxm getMaxLineHeight:" +line);
//        height = (this.getPaint().getFontMetrics().descent-this.getPaint().getFontMetrics().ascent)*line;
//        return height;
//    }
}
