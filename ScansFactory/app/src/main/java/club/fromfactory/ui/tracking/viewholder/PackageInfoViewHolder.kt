package club.fromfactory.ui.tracking.viewholder

import android.view.View
import android.view.ViewGroup
import club.fromfactory.R
import club.fromfactory.baselibrary.view.recyclerview.BaseViewHolder
import club.fromfactory.ui.tracking.model.PackageInfo
import club.fromfactory.ui.tracking.view.PackageInfoInterface
import kotlinx.android.synthetic.main.tracking_info_parcel_item.*

class PackageInfoViewHolder(parent:ViewGroup) :BaseViewHolder<PackageInfo>(parent, R.layout.tracking_info_parcel_item) {

    override fun bindData(data: PackageInfo) {
        super.bindData(data)

        tracking_info_parcel_item_txt_no.text = data.packageNo
        tracking_info_parcel_item_txt_weight.text = data.packageWeight.toString()+ "kg"
        tracking_info_parcel_item_txt_sonum.text = data.totalBags.toString()

        tracking_info_parcel_item_delete.setOnClickListener {
            if (mRecyclerItemViewClickListener != null) {
                (mRecyclerItemViewClickListener as PackageInfoInterface).deletePackageInfo(layoutPosition,data)
            }
        }


    }
}