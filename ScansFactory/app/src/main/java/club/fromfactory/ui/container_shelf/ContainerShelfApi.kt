package club.fromfactory.ui.container_shelf

import club.fromfactory.baselibrary.model.EmptyResponse
import club.fromfactory.pojo.HttpJsonResult
import club.fromfactory.ui.container_shelf.model.ContainerResponseData
import club.fromfactory.ui.hawb_binding.model.HawbBindingResult
import club.fromfactory.ui.hawb_binding.model.HawbDetailInfo
import io.reactivex.Observable
import retrofit2.http.*

interface ContainerShelfApi {

    /**
     */
    @GET
    fun batchShelveContainerConfirm(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>,@QueryMap queryParams: Map<String, @JvmSuppressWildcards Any>): Observable<ContainerResponseData>

    /**
     */
    @POST
    fun batchShelveContainerShelf(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>, @Body bodyParams: Map<String, @JvmSuppressWildcards Any>): Observable<HttpJsonResult<EmptyResponse>>


}