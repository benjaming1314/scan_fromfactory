package club.fromfactory.ui.tracking.viewholder

import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import club.fromfactory.FFApplication
import club.fromfactory.R
import club.fromfactory.baselibrary.utils.DateUtil
import club.fromfactory.baselibrary.utils.StringUtils
import club.fromfactory.baselibrary.view.recyclerview.BaseViewHolder
import club.fromfactory.ui.tracking.model.MawbInfo
import kotlinx.android.synthetic.main.tracking_list_item.*

class MawbInfoViewHolder(parent :ViewGroup) :BaseViewHolder<MawbInfo>(parent, R.layout.tracking_list_item) {

    override fun bindData(data: MawbInfo) {
        super.bindData(data)

        val airlineName = data.airlineName
        val freightForwardingCompanyName = data.freightForwardingCompanyName
        val destinationPortName = data.destinationPortName

        val clearanceModelName = data.clearanceModelName

        if (StringUtils.isNotBlank(freightForwardingCompanyName)){
            tracking_list_item_txt_freightForwardingCompany.text = freightForwardingCompanyName
        }else {
            tracking_list_item_txt_freightForwardingCompany.text = FFApplication.getInstance().resources.getString(R.string.no_valus)
        }
        if (StringUtils.isNotBlank(airlineName)){
            tracking_list_item_txt_airline.text = airlineName
        }else {
            tracking_list_item_txt_airline.text = FFApplication.getInstance().resources.getString(R.string.no_valus)
        }
        if (StringUtils.isNotBlank(destinationPortName)){
            tracking_list_item_txt_destinationPort.text = destinationPortName
        }else {
            tracking_list_item_txt_destinationPort.text = FFApplication.getInstance().resources.getString(R.string.no_valus)
        }

        tracking_list_item_txt_mawb.text = data.mawbNo
        tracking_list_item_txt_plan_weight.text = data.planWeight.toString()+ "kg"
        tracking_list_item_txt_current_wight.text = data.totalWeight.toString()+ "kg"
        tracking_list_item_txt_type.text = if (data.isHasSpaciesSku()) FFApplication.getInstance().resources.getString(R.string.sku_type_has_special) else  FFApplication.getInstance().resources.getString(R.string.sku_type_prompt_costom)
        tracking_list_item_txt_mode.text = if (StringUtils.isNotBlank(clearanceModelName)) clearanceModelName else ""


        val planOutTime = data.planOutTime
        if (StringUtils.isNotBlank(planOutTime)) {

            tracking_list_item_txt_plan_time.text = DateUtil.utc2Local(planOutTime?:DateUtil.getCurrentUtcTime())

        }else {
            tracking_list_item_txt_plan_time.text = ""
        }

        tracking_list_item_txt_complete.visibility = if(data.isStatusComplete()) View.VISIBLE else View.GONE
        tracking_list_item_txt_pending.visibility = if(data.isStatusComplete()) View.GONE else View.VISIBLE
    }


}