package club.fromfactory.ui.wms_signing.contract

import club.fromfactory.baselibrary.presenter.IPresenter
import club.fromfactory.baselibrary.view.IBaseMVPView
import club.fromfactory.ui.wms_signing.model.OrderDetailInfo

class SigningTypeListContract {


    interface View : IBaseMVPView<Presenter> {

        fun requestCreateSignOrderSucceed(orderDetailInfo : OrderDetailInfo)

        fun requestFailed(message :String?)
    }

    interface Presenter : IPresenter<View> {
        fun createSignOrder(type :String)
    }
}