package club.fromfactory.ui.tracking.model

import club.fromfactory.baselibrary.model.NoProguard
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class PackageInfo: NoProguard, Serializable{

        var packageNo: String ?=null//大包裹号
        @SerializedName("large_parcel_name")
        var largeParcelName: String ?=null//hawb大包裹号
        var packageWeight: Double ?=0.0// 大包裹重量
        var totalBags: Int ?=0 //小包裹数即大包裹包含的运单数
}