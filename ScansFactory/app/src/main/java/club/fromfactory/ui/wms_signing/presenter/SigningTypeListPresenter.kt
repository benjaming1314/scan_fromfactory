package club.fromfactory.ui.wms_signing.presenter

import club.fromfactory.baselibrary.language.LanguageUtils
import club.fromfactory.baselibrary.net.CookieHelper
import club.fromfactory.baselibrary.net.retrofit.BaseRetrofit
import club.fromfactory.baselibrary.presenter.BasePresenter
import club.fromfactory.baselibrary.rx.NetObserver
import club.fromfactory.baselibrary.rx.loadingView
import club.fromfactory.baselibrary.utils.PreferenceStorageUtils
import club.fromfactory.baselibrary.view.RxAppCompatActivity
import club.fromfactory.constant.Constants
import club.fromfactory.http.NetUtils
import club.fromfactory.ui.wms_signing.IWmsSigningApi
import club.fromfactory.ui.wms_signing.model.OrderDetailInfo
import club.fromfactory.ui.wms_signing.contract.SigningTypeListContract
import com.trello.rxlifecycle2.kotlin.bindToLifecycle
import java.util.HashMap

class SigningTypeListPresenter(view : SigningTypeListContract.View) :BasePresenter<SigningTypeListContract.View>(view),SigningTypeListContract.Presenter {

    var CREATE_SIGN_ORDER_URL = NetUtils.APP_MAIN_URL + "wms-center/create_sign_order"

    override fun createSignOrder(type: String) {
        val token = PreferenceStorageUtils.getInstance().token

        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        val params = HashMap<String, Any>()
        params["type"] = type

        BaseRetrofit
                .createService(IWmsSigningApi::class.java)
                .createSignOrder(CREATE_SIGN_ORDER_URL,headerMap,params)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : NetObserver<OrderDetailInfo>() {
                    override fun onSuccess(t: OrderDetailInfo?) {
                        if (t != null) {
                            view.requestCreateSignOrderSucceed(t)
                        }else {
                            view.requestFailed("no data")
                        }
                    }

                    override fun onFailure(message: String) {
                        view.requestFailed(message)
                    }
                })

    }
}