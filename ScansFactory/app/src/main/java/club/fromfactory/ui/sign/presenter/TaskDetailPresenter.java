//package club.fromfactory.ui.sign.presenter;
//
//import java.util.List;
//
//import club.fromfactory.utils.Zlog;
//import club.fromfactory.http.HttpRequestResultListner;
//import club.fromfactory.ui.sign.biz.ITaskDetailBiz;
//import club.fromfactory.ui.sign.biz.TaskDetailBiz;
//import club.fromfactory.ui.sign.view.ITaskDetailView;
//import club.fromfactory.pojo.HttpJsonResult;
//import club.fromfactory.ui.sign.model.HttpResultTrackingNo;
//import club.fromfactory.ui.sign.model.TrackingsNoDetail;
//import club.fromfactory.ui.sign.model.TrackingsNoDetailModel;
//
///**
// * Created by lxm on 2017/7/25.
// */
//
//public class TaskDetailPresenter {
//
//
//    private ITaskDetailBiz iTaskDetailBiz  ;
//    private ITaskDetailView iTaskDetailView;
//
//    public TaskDetailPresenter(ITaskDetailView iTaskDetailView)
//    {
//        this.iTaskDetailView = iTaskDetailView;
//        this.iTaskDetailBiz = new TaskDetailBiz();
//    }
//
//
//    public void updateSignedTask() {
//        int task_id = iTaskDetailView.getTaskId();
//        iTaskDetailView.showLoadingView();
//        iTaskDetailBiz.updateSignedTask(task_id, new HttpRequestResultListner() {
//            @Override
//            public void requestSucceed(Object o) {
//                iTaskDetailView.hideLoadingView();
//                if (o != null) {
//                    HttpJsonResult httpJsonResult = (HttpJsonResult) o;
//                    Zlog.ii("lxm ss updateSignedTask:" + httpJsonResult);
//                    if (httpJsonResult.is_success()) {
//                        iTaskDetailView.updateTaskSuccess();
//                    }else {
//                        iTaskDetailView.showFailedError(httpJsonResult.getMsg());
//                    }
//                }else {
//                    iTaskDetailView.showFailedError("failed");
//                }
//            }
//
//            @Override
//            public void requestFailed(String messae) {
//                iTaskDetailView.hideLoadingView();
//                iTaskDetailView.showFailedError("failed");
//            }
//        });
//
//    }
//
//    public void trackingNo(String tracking_no)  {
//        int task_id = iTaskDetailView.getTaskId();
//        iTaskDetailBiz.trackingNo(task_id, tracking_no, new HttpRequestResultListner() {
//            @Override
//            public void requestSucceed(Object o) {
//                if (o != null) {
//                    HttpResultTrackingNo httpResultTrackingNo = (HttpResultTrackingNo) o;
//                    Zlog.ii("lxm ss trackingNo:" + httpResultTrackingNo);
//                    if (httpResultTrackingNo.is_success()) {
//                        TrackingsNoDetail trackingsNo = httpResultTrackingNo.getData();
//                        iTaskDetailView.trackingsNo(trackingsNo);
//                    }else {
//                        iTaskDetailView.showFailedError(httpResultTrackingNo.getMsg());
//                    }
//
//                }else {
//                    iTaskDetailView.showFailedError("failed");
//                }
//            }
//
//            @Override
//            public void requestFailed(String messae) {
//                iTaskDetailView.showFailedError("failed");
//            }
//        });
//    }
//
//    public void getTaskDetail()  {
//        int task_id = iTaskDetailView.getTaskId();
//        iTaskDetailBiz.getTaskDetail(task_id, new HttpRequestResultListner() {
//            @Override
//            public void requestSucceed(Object o) {
//                if (o != null) {
//                    TrackingsNoDetailModel trackingsNoModel = (TrackingsNoDetailModel) o;
//                    Zlog.ii("lxm ss getTaskDetail:" + trackingsNoModel);
//                    List<TrackingsNoDetail> trackingsNoList =  trackingsNoModel.getResults();
//                    if (trackingsNoList != null) {
//                        iTaskDetailView.httpResult(trackingsNoList);
//                    }else {
//                        iTaskDetailView.showFailedError("failed");
//                    }
//
//                }else {
//                    iTaskDetailView.showFailedError("failed");
//                }
//            }
//
//            @Override
//            public void requestFailed(String messae) {
//                iTaskDetailView.showFailedError("failed");
//            }
//        });
//
//
//    }
//
//
//
//}
