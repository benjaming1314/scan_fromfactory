package club.fromfactory.ui.tracking.view

import club.fromfactory.baselibrary.view.recyclerview.BaseRecyclerItemViewClickListener
import club.fromfactory.ui.tracking.model.PackageInfo

/**
 *@author lxm
 *@date 2018/10/29
 */
interface PackageInfoInterface : BaseRecyclerItemViewClickListener<PackageInfo> {

    fun deletePackageInfo(position:Int,data: PackageInfo)

}