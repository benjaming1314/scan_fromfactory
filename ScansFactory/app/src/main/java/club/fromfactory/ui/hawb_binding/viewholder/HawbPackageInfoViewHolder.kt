package club.fromfactory.ui.hawb_binding.viewholder

import android.view.ViewGroup
import club.fromfactory.R
import club.fromfactory.baselibrary.view.recyclerview.BaseViewHolder
import club.fromfactory.ui.tracking.model.PackageInfo
import club.fromfactory.ui.tracking.view.PackageInfoInterface
import kotlinx.android.synthetic.main.hawb_info_parcel_item.*

class HawbPackageInfoViewHolder(parent:ViewGroup) :BaseViewHolder<PackageInfo>(parent, R.layout.hawb_info_parcel_item) {

    override fun bindData(data: PackageInfo) {
        super.bindData(data)

        hawb_info_parcel_item_txt_no.text = data.largeParcelName

        hawb_info_parcel_item_delete.setOnClickListener {
            if (mRecyclerItemViewClickListener != null) {
                (mRecyclerItemViewClickListener as PackageInfoInterface).deletePackageInfo(layoutPosition,data)
            }
        }


    }
}