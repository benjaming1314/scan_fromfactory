package club.fromfactory.ui.sign.contract

import club.fromfactory.baselibrary.presenter.IPresenter
import club.fromfactory.baselibrary.view.IBaseMVPView
import club.fromfactory.ui.sign.model.SignedTask

class ParcelTaskContract {

    interface View : IBaseMVPView<Presenter> {

        fun httpResult(signedTask: SignedTask)
        fun httpResult(signedTask: List<SignedTask>)
        fun showFailedError(message: String?)

    }

    interface Presenter : IPresenter<View> {

        fun createSignedTask()
        fun getSignTaskList()
    }
}