package club.fromfactory.ui.moveout.presenter

import club.fromfactory.FFApplication
import club.fromfactory.R
import club.fromfactory.baselibrary.language.LanguageUtils
import club.fromfactory.baselibrary.net.CookieHelper
import club.fromfactory.baselibrary.net.retrofit.BaseRetrofit
import club.fromfactory.baselibrary.presenter.BasePresenter
import club.fromfactory.baselibrary.rx.loadingView
import club.fromfactory.baselibrary.utils.JsonUtils
import club.fromfactory.baselibrary.utils.PreferenceStorageUtils
import club.fromfactory.baselibrary.view.RxAppCompatActivity
import club.fromfactory.constant.Constants
import club.fromfactory.http.NetUtils
import club.fromfactory.http.model.HttpJsonErrorResult
import club.fromfactory.ui.common.model.Goods
import club.fromfactory.ui.moveout.IMoveOutApi
import club.fromfactory.ui.moveout.contract.MoveOutGoodsListContract
import com.trello.rxlifecycle2.kotlin.bindToLifecycle
import io.reactivex.observers.DefaultObserver
import retrofit2.HttpException
import java.util.*

class MoveOutListPresenter(view: MoveOutGoodsListContract.View) : BasePresenter<MoveOutGoodsListContract.View>(view), MoveOutGoodsListContract.Presenter {
    /**
     * 移出确认 - 扫库位码拿列表
     */
    private var offshelfskuListUrl = NetUtils.APP_MAIN_URL + "wms/pda/off_shelf/sku_list?box_name="

    /**
     * 移出确认 - 扫描sku_id
     */
    private var operateOffshelveUrl = NetUtils.APP_MAIN_URL + "wms/pda/operate/off_shelf"


    override fun getSkuList() {

        val token = PreferenceStorageUtils.getInstance().token

        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        BaseRetrofit
                .createService(IMoveOutApi::class.java)
                .getSkuList(offshelfskuListUrl + view.getBoxName(), headerMap)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : DefaultObserver<List<Goods>>() {
                    override fun onComplete() {
                    }

                    override fun onNext(t: List<Goods>) {
                        val qtyCount = getQtyCount(t)

                        if (qtyCount > 100) {
                            view.startMunGoodsPage(qtyCount)
                        }else {
                            view.refreshData(t)
                        }

                    }

                    override fun onError(e: Throwable) {
                        if (e is HttpException && e.code() in 400..404) {
                            val string = e.response()?.errorBody()?.string()
                            if (string != null) {
                                val parseJson = JsonUtils.parseJson<HttpJsonErrorResult>(string)
                                view.showFailedError(parseJson?.message)
                            }
                        } else {
                            view.showFailedError(FFApplication.getInstance().resources.getString(R.string.net_error))
                        }
                    }

                })
    }

    private fun getQtyCount(t: List<Goods>) :Int {
        var count = 0 ;
        t.forEach {
            count += it.qty
        }
        return count
    }

    override fun operateOffShelve(barcode: String) {

        val token = PreferenceStorageUtils.getInstance().token

        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        val params = HashMap<String, Any>()
        params["box_name"] = view.getBoxName()
        params["barcode"] = barcode
//        params["id"] = id

        BaseRetrofit
                .createService(IMoveOutApi::class.java)
                .operateOffShelve(operateOffshelveUrl, headerMap, params)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : DefaultObserver<Goods>() {
                    override fun onComplete() {
                    }

                    override fun onNext(t: Goods) {
                        view.moveoutConfirm(t)
                    }

                    override fun onError(e: Throwable) {
                        if (e is HttpException && e.code() in 400..404) {
                            val string = e.response()?.errorBody()?.string()
                            if (string != null) {
                                val parseJson = JsonUtils.parseJson<HttpJsonErrorResult>(string)
                                view.moveoutConfirmFailed(parseJson?.message)
                            }
                        } else {
                            view.moveoutConfirmFailed(FFApplication.getInstance().resources.getString(R.string.net_error))
                        }
                    }

                })
    }
}