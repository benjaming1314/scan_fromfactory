package club.fromfactory.ui.splash;

import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;

import club.fromfactory.R;
import club.fromfactory.baselibrary.utils.DeviceUtils;
import club.fromfactory.baselibrary.utils.PreferenceStorageUtils;
import club.fromfactory.baselibrary.utils.StringUtils;
import club.fromfactory.baselibrary.utils.ToastUtils;
import club.fromfactory.baselibrary.view.BaseActivity;
import club.fromfactory.constant.Constants;
import club.fromfactory.ui.login.LoginScanActivity;
import club.fromfactory.ui.setting.SelectWareHouseActivity;
import club.fromfactory.utils.Zlog;

/**
 * 欢迎页，闪屏图片显示1s
 */
public class SplashActivity extends BaseActivity {

    private static final int DELAY_TIME = 2000;  //闪屏页存在的时间

    private boolean isFirstInstall;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_splash;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        requestFullScreen();
        hideBottomUIMenu();
        super.onCreate(savedInstanceState);
    }

    @Override
    protected boolean useCustomStatusBar() {
        return false;
    }

    @Override
    public void initData() {
        super.initData();
        isFirstInstall = PreferenceStorageUtils.getInstance().getIsFirstInstall();
    }

    @Override
    public void initView() {
        super.initView();
//        CountryLanguageUtils.getInstance().setDefaultLanguage();

        Zlog.ii("SplashActivity:onCreate" + DeviceUtils.getDeviceInfo());
        Zlog.ii("SplashActivity:onCreate" + DeviceUtils.getPhoneModel());

        String pdaType = "";
        //根据型号设置pad类型
        String phoneModel = DeviceUtils.getPhoneModel().replace(" ", "");
        if ("ww808_emmc".equalsIgnoreCase(phoneModel) || "nf3501".equalsIgnoreCase(phoneModel)) {
            pdaType = Constants.PDA_TYPE_NF3501;
        } else if ("pda".equalsIgnoreCase(phoneModel) || "U8000S".equalsIgnoreCase(phoneModel)) {
            pdaType = Constants.PDA_TYPE_NEWPDA4001;
        } else if ("mc36".equalsIgnoreCase(phoneModel)) {
            pdaType = Constants.PDA_TYPE_ZEBRA_MC36;
        } else if ("PDT-90P".equalsIgnoreCase(phoneModel)) {
            pdaType = Constants.PDA_TYPE_SEUIC_AUTOID9;
        } else if ("50Series".equalsIgnoreCase(phoneModel) || "95WSeries".equalsIgnoreCase(phoneModel)) {
            pdaType = Constants.PDA_TYPE_50_SERIES;
        } else {
            ToastUtils.show("not support the device");
        }

        String pdaTypeLocation = PreferenceStorageUtils.getInstance().getPdaType();

        if (StringUtils.isNull(pdaTypeLocation)) {
            if (!Constants.PDA_TYPE.equals(pdaType)) {
                Constants.PDA_TYPE = pdaType;
            }
            PreferenceStorageUtils.getInstance().savePdaType(Constants.PDA_TYPE);
        }

        closeSelf();

    }


    private void closeSelf() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isFirstInstall) {
                    PreferenceStorageUtils.getInstance().saveIsFirstInstall(false);

                    SelectWareHouseActivity.Companion.launchActivity(SplashActivity.this, false, 100);
                } else {

                    LoginScanActivity.Companion.launchActivity(SplashActivity.this);
                }
                finish();
            }
        }, DELAY_TIME);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    finish();
                    return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void clickKeyCodeBack() {
        finish();
    }

}
