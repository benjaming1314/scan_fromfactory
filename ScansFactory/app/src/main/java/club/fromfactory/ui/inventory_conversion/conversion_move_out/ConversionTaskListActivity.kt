//package club.fromfactory.ui.inventory_conversion.conversion_move_out
//
//import android.content.Intent
//import club.fromfactory.R
//import club.fromfactory.base.BaseActivity
//import club.fromfactory.baselibrary.view.BaseActivity
//import club.fromfactory.ui.inventory_conversion.conversion_move_out.adapter.ConversionTaskAdapter
//import club.fromfactory.ui.inventory_conversion.conversion_move_out.presenter.ConversionTaskListPresenter
//import club.fromfactory.ui.inventory_conversion.conversion_move_out.view.IConversionTaskListView
//import club.fromfactory.widget.CustomTitleLinearLayout
//import kotlinx.android.synthetic.main.activity_conversion_task_list.*
//
///**
// * 废弃
// */
//class ConversionTaskListActivity : BaseActivity(), IConversionTaskListView {
//
//
//    companion object {
//
//    }
//
//    private var mConversionTaskListPresenter : ConversionTaskListPresenter ?= null
//
//    private var mConversionTaskAdapter : ConversionTaskAdapter ?= null
//
//
//    override fun clickKeyCodeBack() {
//
//    }
//
//    override fun getLayoutResId(): Int  = R.layout.activity_conversion_task_list
//
//
//    override fun initData() {
//        super.initData()
//
//        mConversionTaskListPresenter = ConversionTaskListPresenter(this)
//    }
//
//    override fun initView() {
//        super.initView()
//
//        conversion_task_list_ctl_title.setListener(object :CustomTitleLinearLayout.CustomTitleListener(){
//
//            override fun clickLeft() {
//                super.clickLeft()
//                finish()
//            }
//        })
//
//        mConversionTaskAdapter = ConversionTaskAdapter()
//
//       conversion_task_list_recycler_view.adapter =  mConversionTaskAdapter
//
//        mConversionTaskAdapter?.setOnItemViewClickListener { data, clickedView, position ->
//
//
//        }
//
//    }
//
//    override fun fetchDada() {
//        super.fetchDada()
//        mConversionTaskListPresenter?.getTaskList()
//    }
//
//
//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//
//
//    }
//
//    override fun showLoadingView() {
//        showBaseProgressDialog()
//    }
//
//    override fun hideLoadingView() {
//        hideBaseProgressDialog()
//    }
//
//}
