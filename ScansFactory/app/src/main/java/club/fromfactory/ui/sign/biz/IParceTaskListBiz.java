//package club.fromfactory.ui.sign.biz;
//
//import android.content.Context;
//
//import club.fromfactory.base.BaseBiz;
//import club.fromfactory.http.HttpRequestResultListner;
//
///**
// * Created by lxm on 2017/7/25.
// */
//
//public interface IParceTaskListBiz extends BaseBiz {
//
//    void createSignedTask(HttpRequestResultListner httpRequestResultListner) ;
//
//    void getSignTaskList(HttpRequestResultListner httpRequestResultListner) ;
//
//
//}
