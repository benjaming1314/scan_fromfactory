package club.fromfactory.ui.container_shelf.contract

import club.fromfactory.baselibrary.presenter.IPresenter
import club.fromfactory.baselibrary.view.IBaseMVPView
import club.fromfactory.ui.container_shelf.model.ContainerResponseData

class ContainerConfirmContract {

    interface View:IBaseMVPView<Presenter> {
        fun requestSuccess(containerResponseData: ContainerResponseData)

        fun requestFailed(message :String?)
    }
    interface Presenter:IPresenter<View> {

        fun checkContainer(str:String)

    }
}