package club.fromfactory.ui.common.viewholder

import android.view.View
import android.view.ViewGroup
import club.fromfactory.R
import club.fromfactory.utils.ImageUtils
import club.fromfactory.baselibrary.view.recyclerview.BaseViewHolder
import club.fromfactory.baselibrary.view.recyclerview.ViewHolderCreator
import club.fromfactory.ui.common.model.Goods
import kotlinx.android.synthetic.main.goods_item.*

class GoodViewHolder(parent: ViewGroup?) : BaseViewHolder<Goods>(parent, R.layout.goods_item)  {


    override fun bindData(goods: Goods) {
        super.bindData(goods)

        ImageUtils.loadImageNew(goods_item_img, goods.sku_image_url, R.color.white)


        goods_item_txt_sku.text = goods.getBarCodeShowStr()

        val scanState = goods.scanState

        when (scanState) {
            Goods.SCAN_STATE_DEFAULT -> {
                goods_item_state_right.visibility = View.GONE
                goods_item_state_error.visibility = View.GONE
            }
            Goods.SCAN_STATE_FAILED -> {
                goods_item_state_right.visibility = View.GONE
                goods_item_state_error.visibility = View.VISIBLE
            }
            Goods.SCAN_STATE_SUCCEED -> {
                goods_item_state_right.visibility = View.VISIBLE
                goods_item_state_error.visibility = View.GONE
            }

            else -> {
                goods_item_state_right.visibility = View.GONE
                goods_item_state_error.visibility = View.GONE
            }
        }

    }

}

class GoodsCreator : ViewHolderCreator<Goods> {

    override fun isForViewType(item: Any, position: Int): Boolean {
        return item is Goods
    }

    override fun onCreateBaseViewHolder(parent: ViewGroup): BaseViewHolder<Goods> {
        return GoodViewHolder(parent)
    }

}