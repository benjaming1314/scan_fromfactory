package club.fromfactory.ui.main.model;

import club.fromfactory.baselibrary.model.NoProguard;

public class PermissionResponseData implements NoProguard {

    private String username ;
    private String pk ;
    private boolean is_success ;
    private String[] perms ;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public boolean isIs_success() {
        return is_success;
    }

    public void setIs_success(boolean is_success) {
        this.is_success = is_success;
    }

    public String[] getPerms() {
        return perms;
    }

    public void setPerms(String[] perms) {
        this.perms = perms;
    }
}
