package club.fromfactory.ui.common.model

import android.widget.EditText
import club.fromfactory.baselibrary.model.NoProguard

class EditTextModel(var editText: EditText ? =null,
                    var nextEditText: EditText ? =null,
                    var isFoucus: Boolean ? =false
): NoProguard {
    override fun toString(): String {
        return "EditTextModel(editText=$editText, nextEditText=$nextEditText, isFoucus=$isFoucus)"
    }
}