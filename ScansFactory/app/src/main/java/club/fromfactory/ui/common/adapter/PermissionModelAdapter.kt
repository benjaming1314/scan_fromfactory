package club.fromfactory.ui.common.adapter

import android.view.ViewGroup
import club.fromfactory.baselibrary.view.recyclerview.BaseRecyclerAdapter
import club.fromfactory.baselibrary.view.recyclerview.BaseViewHolder
import club.fromfactory.ui.main.model.PermissionModel
import club.fromfactory.ui.common.viewholder.PermissionModelViewHolder

class PermissionModelAdapter :BaseRecyclerAdapter<PermissionModel>() {

    override fun onCreateBaseViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<PermissionModel> = PermissionModelViewHolder(parent)
}