package club.fromfactory.ui.tracking.model

import club.fromfactory.baselibrary.model.NoProguard
import java.io.Serializable

class MawbInfo(
        var mawbId : Long = 0,
        var mawbNo :String ?= null,
        var planOutTime :String?= null ,
        var airlineName :String?= null , //航司
        var destinationPortName :String?= null ,//港口
        var freightForwardingCompanyName :String?= null ,//货代公司
        var planWeight : Double = 0.0,
        var totalWeight  : Double  = 0.0,
        var stockContainment   : Int = 0 ,//内容物 0仅普通商品 1包含特殊商品
        var status : Int ?= null, //0未交接 1已交接

        var clearanceModelName : String ?= null, //清报关模式

        var packageList : List<PackageInfo> ?=null

) : NoProguard ,Serializable {

    fun isStatusComplete() : Boolean {
        return  1 == status
    }
    fun isHasSpaciesSku() : Boolean {
        return stockContainment == 1
    }


}