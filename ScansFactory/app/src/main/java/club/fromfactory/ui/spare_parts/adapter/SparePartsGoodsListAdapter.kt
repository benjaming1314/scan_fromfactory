package club.fromfactory.ui.spare_parts.adapter

import android.view.ViewGroup
import club.fromfactory.baselibrary.view.recyclerview.BaseRecyclerAdapter
import club.fromfactory.baselibrary.view.recyclerview.BaseViewHolder
import club.fromfactory.ui.spare_parts.viewholder.SparePartsGoodsListViewHolder

class SparePartsGoodsListAdapter : BaseRecyclerAdapter<String>() {

    override fun onCreateBaseViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<String> = SparePartsGoodsListViewHolder(parent)

}