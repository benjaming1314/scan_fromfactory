package club.fromfactory.ui.versionupdate;

import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import club.fromfactory.R;
import club.fromfactory.baselibrary.utils.ScreenUtils;
import club.fromfactory.baselibrary.utils.StringUtils;
import club.fromfactory.baselibrary.widget.CustomDialog;


/**
 * Created by lxm on 2016/11/21.
 */

public class UpdateDialogUtils {

    private static UpdateDialogUtils dialogUtils;

    private UpdateDialogUtils() {
    }

    public static UpdateDialogUtils getInstance() {
        if (dialogUtils == null) {
            dialogUtils = new UpdateDialogUtils();
        }
        return dialogUtils;
    }

    /**
     * 获取加载页的对话框
     *
     * @param context
     * @return
     */
    public CustomDialog getUptateDialog(Context context, boolean isForce, String description, boolean isDownloadComplete, View.OnClickListener onSaveClickListener,
                                        View.OnClickListener onCancelClickListener) {
        final CustomDialog dialog = new CustomDialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_popup_window);

        dialog.setCanceledOnTouchOutside(false);

        /*
         * 将对话框的大小按屏幕大小的百分比设置
         */
        Window window = dialog.getWindow();
        WindowManager.LayoutParams p = window.getAttributes(); // 获取对话框当前的参数值
        p.width = (int) (ScreenUtils.getScreenWidth() * 0.8);

        Button btnSave = (Button) dialog.findViewById(R.id.popup_window_ok);
        btnSave.setOnClickListener(onSaveClickListener);
        Button btnCancel = (Button) dialog.findViewById(R.id.popup_window_cancel);
        btnCancel.setOnClickListener(onCancelClickListener);

        TextView editText = (TextView) dialog.findViewById(R.id.popup_window_txt_message);

        if (isForce) {
            btnCancel.setVisibility(View.GONE);
        } else {
            btnCancel.setVisibility(View.VISIBLE);
            btnCancel.setText(context.getResources().getString(R.string.update_cancel));
        }
        if (StringUtils.isNotBlank(description)) {
            editText.setText(description.replace("\\n", "\n"));
        }

        if (isDownloadComplete) {
            btnSave.setText(context.getResources().getString(R.string.install_btn));
        } else {
            btnSave.setText(context.getResources().getString(R.string.update_btn));
        }


        return dialog;
    }


}
