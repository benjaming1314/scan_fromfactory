package club.fromfactory.ui.common.model

import club.fromfactory.baselibrary.model.NoProguard
import club.fromfactory.baselibrary.utils.StringUtils
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

class GoodsAttribute : Serializable, NoProguard {

    @SerializedName("color",alternate = ["Color"])
    var color :String ?= null
    @SerializedName("content")
    var content :String ?= null
    @SerializedName("quantity")
    var quantity :String ?= null
    @SerializedName("size",alternate = ["Size"])
    var size :String ?= null
    @SerializedName("unit",alternate = ["Unit"])
    var unit :String ?= null

    override fun toString(): String {
        return "color:$color, content:$content, quantity:$quantity, size:$size, unit:$unit"
    }


    fun show(): String {

        val str = StringBuffer()

        if (StringUtils.isNotBlank(color)) {
            str.append("color:") .append(color).append("  ")
        }
        if (StringUtils.isNotBlank(content)) {
            str.append("content:") .append(content).append("  ")
        }

        if (StringUtils.isNotBlank(quantity)) {
            str.append("quantity:") .append(quantity).append("  ")
        }

        if (StringUtils.isNotBlank(size)) {
            str.append("size:") .append(size).append("  ")
        }

        if (StringUtils.isNotBlank(unit)) {
            str.append("unit:") .append(unit).append("  ")
        }

        return str.toString()

    }


}