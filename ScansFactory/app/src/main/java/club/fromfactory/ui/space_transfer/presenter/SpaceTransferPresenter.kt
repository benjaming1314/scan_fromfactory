package club.fromfactory.ui.space_transfer.presenter

import club.fromfactory.baselibrary.language.LanguageUtils
import club.fromfactory.baselibrary.model.EmptyResponse
import club.fromfactory.baselibrary.net.CookieHelper
import club.fromfactory.baselibrary.net.retrofit.BaseRetrofit
import club.fromfactory.baselibrary.presenter.BasePresenter
import club.fromfactory.baselibrary.rx.NetObserver
import club.fromfactory.baselibrary.rx.loadingView
import club.fromfactory.baselibrary.utils.PreferenceStorageUtils
import club.fromfactory.baselibrary.view.RxAppCompatActivity
import club.fromfactory.constant.Constants
import club.fromfactory.http.NetUtils
import club.fromfactory.ui.space_transfer.SpaceTransferApi
import club.fromfactory.ui.space_transfer.contract.SpaceTransferContract
import com.trello.rxlifecycle2.kotlin.bindToLifecycle
import java.util.*

class SpaceTransferPresenter(view: SpaceTransferContract.View) : BasePresenter<SpaceTransferContract.View>(view), SpaceTransferContract.Presenter {

    private var CHECK_FORM_URL = NetUtils.APP_MAIN_URL + "wms-center/pda/wholebox-move/check-from"
    private var SHELFOFF_SOURCE_LOCATION_URL = NetUtils.APP_MAIN_URL + "wms-center/pda/wholebox-move/from"
    private var SHELF_LOCATION_URL = NetUtils.APP_MAIN_URL + "wms-center/pda/wholebox-move/to"

    override fun checkSourceLocation(boxName: String) {

        val token = PreferenceStorageUtils.getInstance().token
        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)
        val params = HashMap<String, Any>()
        params["name"] = boxName

        BaseRetrofit
                .createService(SpaceTransferApi::class.java)
                .checkForm(CHECK_FORM_URL, headerMap, params)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : NetObserver<EmptyResponse>() {
                    override fun onSuccess(t: EmptyResponse?) {
                        view.checkSourceLocationSuccess()
                    }

                    override fun onFailure(message: String) {
                        view.requestFailed(message)
                    }
                })
    }

    override fun shelfOffSourceLocation(boxName: String) {
        val token = PreferenceStorageUtils.getInstance().token
        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)
        val params = HashMap<String, Any>()
        params["name"] = boxName

        BaseRetrofit
                .createService(SpaceTransferApi::class.java)
                .shelfOffSourceLocation(SHELFOFF_SOURCE_LOCATION_URL, headerMap, params)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : NetObserver<EmptyResponse>() {
                    override fun onSuccess(t: EmptyResponse?) {
                        view.shelfOffSourceLocationSuccess()
                    }

                    override fun onFailure(message: String) {
                        view.requestFailed(message)
                    }
                })
    }

    override fun shelfLocation(boxName: String, toBoxName: String) {

        val token = PreferenceStorageUtils.getInstance().token
        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)
        val params = HashMap<String, Any>()
        params["name"] = boxName
        params["toName"] = toBoxName

        BaseRetrofit
                .createService(SpaceTransferApi::class.java)
                .shelfLocation(SHELF_LOCATION_URL, headerMap, params)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : NetObserver<EmptyResponse>() {
                    override fun onSuccess(t: EmptyResponse?) {
                        view.shelfLocationSuccess()
                    }

                    override fun onFailure(message: String) {
                        view.requestFailed(message)
                    }
                })
    }
}