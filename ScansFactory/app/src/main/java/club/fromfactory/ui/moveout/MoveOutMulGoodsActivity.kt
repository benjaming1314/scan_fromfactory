package club.fromfactory.ui.moveout

import club.fromfactory.R
import club.fromfactory.baselibrary.router.RouterConstants
import club.fromfactory.baselibrary.utils.DensityUtils
import club.fromfactory.baselibrary.utils.StringUtils
import club.fromfactory.baselibrary.utils.ToastUtils
import club.fromfactory.baselibrary.view.recyclerview.decoration.LinearDecoration
import club.fromfactory.baselibrary.widget.CustomDialog
import club.fromfactory.routerannotaions.Router
import club.fromfactory.routerannotaions.RouterParam
import club.fromfactory.ui.common.model.Goods
import club.fromfactory.ui.main.ScanBaseMVPActivity
import club.fromfactory.ui.moveout.adapter.MoveOutMulScanGoodsAdapter
import club.fromfactory.ui.moveout.contract.MoveOutMulGoodsContract
import club.fromfactory.ui.moveout.presenter.MoveOutMulGoodsPresenter
import club.fromfactory.utils.PromptDialogUtils
import club.fromfactory.utils.TipSound
import club.fromfactory.utils.Zlog
import club.fromfactory.widget.CustomTitleLinearLayout
import kotlinx.android.synthetic.main.activity_move_out_mul_goods.*


@Router(RouterConstants.MOVE_OUT_MUL_GOODS)
class MoveOutMulGoodsActivity : ScanBaseMVPActivity<MoveOutMulGoodsContract.Presenter>(), MoveOutMulGoodsContract.View {

    @RouterParam("boxName")
    @JvmField
    var boxName: String? = null

    @RouterParam("barcodeNum")
    @JvmField
    var barcodeNum: Int = 0

    @RouterParam("is_stock")
    @JvmField
    var isStock: Int = 0

    private var scanBarcodeNum = 0

    private var mCustomDialog: CustomDialog? = null

    private var isHandleScanResult = false

    private var mMoveOutMulScanGoodsAdapter : MoveOutMulScanGoodsAdapter ?= null

    override fun requestScanStart() {
//        if (Zlog.isDebug) {
//            requestScanResult("0014516314")
//        }
    }

    override fun getLayoutResId(): Int = R.layout.activity_move_out_mul_goods

    override fun initView() {
        super.initView()
        if (isStock == 1){
            move_out_mul_goods_ctl_title.setTitleCenter(boxName + "(" + resources.getString(R.string.parcel_shopping_type_one) + ")")
        }else {
            move_out_mul_goods_ctl_title.setTitleCenter(boxName + "(" + resources.getString(R.string.parcel_shopping_type_two) + ")")
        }

        move_out_mul_goods_ctl_title.setListener(object : CustomTitleLinearLayout.CustomTitleListener() {

            override fun clickLeft() {
                super.clickLeft()
                finish()
            }
        })
        setScanListener(move_out_mul_goods_scan)

        move_out_mul_goods_btn_ok.setOnClickListener {
            moveoutGoods(move_out_mul_goods_edt_barcode.text.toString())
        }

        move_out_mul_goods_txt_location.text = boxName
        move_out_mul_goods_txt_barcode_num.text = barcodeNum.toString()
        move_out_mul_goods_txt_complete_barcode_num.text = scanBarcodeNum.toString()

        mMoveOutMulScanGoodsAdapter = MoveOutMulScanGoodsAdapter()
        move_out_mul_goods_recycler_view.addItemDecoration(LinearDecoration(DensityUtils.dp2px(this, 1), R.color.color_333333))
        move_out_mul_goods_recycler_view.adapter  = mMoveOutMulScanGoodsAdapter

    }

    override fun requestScanResult(result: String?) {

        if (barcodeNum <= 0) {
            showPromptDialog(resources.getString(R.string.move_out_list_no_goods))
            return
        }

        if (mCustomDialog != null && mCustomDialog?.isShowing == true) {
            return
        }
        if (isHandleScanResult) {
            return
        } else {
            isHandleScanResult = true
        }

        if (StringUtils.isNotBlank(result)) {
            move_out_mul_goods_edt_barcode.setText(result)
        }
        moveoutGoods(result)
    }

    private fun moveoutGoods(result: String?) {
        if (StringUtils.isNotBlank(result)) {
            presenter.operateOffShelve(result?:"")
        }else {
            ToastUtils.show(resources.getString(R.string.scan_prompt_6))
        }

    }

    override fun moveoutConfirm(goods: Goods?) {

        isHandleScanResult = false
        TipSound.getInstance().playScanSucceedSound(this@MoveOutMulGoodsActivity)

        barcodeNum -= 1
        scanBarcodeNum += 1
        refreshData()
    }

    private fun refreshData() {
        move_out_mul_goods_txt_barcode_num.text = barcodeNum.toString()
        move_out_mul_goods_txt_complete_barcode_num.text = scanBarcodeNum.toString()

        val toString = move_out_mul_goods_edt_barcode.text.toString()

        if(mMoveOutMulScanGoodsAdapter?.data?.size == 10) {
            mMoveOutMulScanGoodsAdapter?.remove(9)
        }
        mMoveOutMulScanGoodsAdapter?.insert(toString,0)

        move_out_mul_goods_edt_barcode.setText("")
    }

    override fun moveoutConfirmFailed(message: String?) {
        isHandleScanResult = false
        TipSound.getInstance().playScanFailedSound(this@MoveOutMulGoodsActivity)
        if (message != null) {
//            ToastUtils.show(message)
            showPromptDialog(message)
        }
    }

    override fun showLoadingView() {
        showLoadingView(move_out_mul_goods_progress)
    }

    override fun hideLoadingView() {
        hideLoadingView(move_out_mul_goods_progress)
    }

    override fun getBoxName(): String = boxName ?: ""

    private fun showPromptDialog(message: String) {
        mCustomDialog = PromptDialogUtils.getInstance().getPromptDialog(this@MoveOutMulGoodsActivity, message)
        mCustomDialog?.show()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mCustomDialog != null) {
            if (mCustomDialog?.isShowing == true) {
                mCustomDialog?.dismiss()
            }
            mCustomDialog = null
        }
    }

    override fun createPresenter(): MoveOutMulGoodsContract.Presenter = MoveOutMulGoodsPresenter(this)

}
