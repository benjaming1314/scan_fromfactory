package club.fromfactory.ui.versionupdate

import club.fromfactory.FFApplication
import club.fromfactory.R
import club.fromfactory.baselibrary.utils.ThreadUtils
import club.fromfactory.baselibrary.utils.ToastUtils
import club.fromfactory.baselibrary.view.BaseActivity
import club.fromfactory.baselibrary.view.IBaseView
import club.fromfactory.baselibrary.widget.CustomDialog
import club.fromfactory.ui.versionupdate.model.VersionModel


/**
 *@author lxm
 *@date 2018/9/21
 */
object VersionUpdateUtils {

    /**
     * 是否需要检查更新，每次重新启动都需要检查
     */
    @JvmStatic
    private var mIsNeedCheckUpdate = true

    private var mUpdateDialogCallback: UpdateDialogCallback? = null

    fun setNeedCheckUpdate(isNeedCheckUpdate: Boolean) {
        mIsNeedCheckUpdate = isNeedCheckUpdate
    }

    fun isNeedCheckUpdate(): Boolean {
        return mIsNeedCheckUpdate
    }

    private var mDialog: CustomDialog? = null


    fun resultVersionInfo(versionModel: VersionModel?, iBaseView: IBaseView, updateDialogCallback: UpdateDialogCallback?) {
        mUpdateDialogCallback = updateDialogCallback
        if (versionModel == null || versionModel.updateType == VersionModel.UPDATE_TYPE_NO_UPDATE) {
            mUpdateDialogCallback?.noUpdate()
            return
        }
        showUpdateDialog(versionModel, iBaseView)
    }

    private fun showUpdateDialog(versionModel: VersionModel, iBaseView: IBaseView) {
        if (!iBaseView.isAlive || (iBaseView as BaseActivity).isFinishing || iBaseView.isDestroyed) {
            return
        }

        val updateUrl = versionModel.link_url ?: ""

        val isForce = versionModel.updateType == VersionModel.UPDATE_TYPE_FORCE_UPDATE


        val isDownloadComplete = DownloadApk.isDownloadComplete(updateUrl)

        mDialog = UpdateDialogUtils.getInstance()
                .getUptateDialog(iBaseView, isForce, versionModel.description, isDownloadComplete, {
                    if (iBaseView.isAlive) {
                        mDialog?.dismiss()

                        if (isDownloadComplete) {
                            DownloadApk.installApk(iBaseView, updateUrl)
                            if (isForce) {
                                iBaseView.finish()
                            }
                        } else {
                            DownloadApk.download(iBaseView, updateUrl, object : DownloadDialogCallback {
                                override fun cancelSuccess() {
                                    if (isForce) {
                                        iBaseView.finish()
                                    }
                                }

                                override fun downloadSuccess() {
                                    if (isForce) {
                                        iBaseView.finish()
                                    }
                                }

                                override fun downloadFailed(msg: String?) {
                                    ThreadUtils.runOnUi {
                                        ToastUtils.show(msg
                                                ?: FFApplication.getInstance().resources.getString(R.string.net_error))

                                        if (isForce) {
                                            iBaseView.finish()
                                        } else {
                                            mUpdateDialogCallback?.cancelUpdate()
                                        }
                                    }
                                }

                            })
                        }
                    }
                }, {
                    mUpdateDialogCallback?.cancelUpdate()
                    if (iBaseView.isAlive) {
                        mDialog?.dismiss()
                    }
                })

        if (iBaseView.isAlive) {
            mDialog?.show()
        }
    }


    fun finish(iBaseView: IBaseView) {
        if (mDialog != null) {
            mDialog?.dismiss()
            mDialog = null
        }
//        (iBaseView as BaseActivity).finish()
    }


}

interface UpdateDialogCallback {
    fun cancelUpdate()
    fun noUpdate()
}
