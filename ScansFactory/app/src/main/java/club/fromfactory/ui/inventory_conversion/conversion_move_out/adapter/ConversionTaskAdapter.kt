//package club.fromfactory.ui.inventory_conversion.conversion_move_out.adapter
//
//import android.view.ViewGroup
//import club.fromfactory.baselibrary.view.recyclerview.BaseRecyclerAdapter
//import club.fromfactory.baselibrary.view.recyclerview.BaseViewHolder
//import club.fromfactory.ui.inventory_conversion.conversion_move_out.model.ConversionTaskInfo
//import club.fromfactory.ui.inventory_conversion.conversion_move_out.viewholder.ConversionTaskViewHolder
//
//class ConversionTaskAdapter : BaseRecyclerAdapter<ConversionTaskInfo>() {
//
//    override fun onCreateBaseViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<ConversionTaskInfo> = ConversionTaskViewHolder(parent)
//}