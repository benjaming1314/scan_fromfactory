package club.fromfactory.ui.main.presenter

import club.fromfactory.FFApplication
import club.fromfactory.R
import club.fromfactory.baselibrary.language.LanguageUtils
import club.fromfactory.baselibrary.net.CookieHelper
import club.fromfactory.baselibrary.net.retrofit.BaseRetrofit
import club.fromfactory.baselibrary.presenter.BasePresenter
import club.fromfactory.baselibrary.rx.loadingView
import club.fromfactory.baselibrary.utils.JsonUtils
import club.fromfactory.baselibrary.utils.PreferenceStorageUtils
import club.fromfactory.baselibrary.view.RxAppCompatActivity
import club.fromfactory.constant.Constants
import club.fromfactory.http.NetUtils
import club.fromfactory.http.model.HttpJsonErrorResult
import club.fromfactory.ui.main.IMainApi
import club.fromfactory.ui.main.PermissionUtils
import club.fromfactory.ui.main.contract.MainContract
import club.fromfactory.ui.main.mPermissionResponseData
import club.fromfactory.ui.main.model.PermissionName
import club.fromfactory.ui.main.model.PermissionResponseData
import club.fromfactory.ui.moveout.IMoveOutApi
import club.fromfactory.ui.moveout.model.OffShelfTaskResponse
import com.trello.rxlifecycle2.kotlin.bindToLifecycle
import io.reactivex.observers.DefaultObserver
import retrofit2.HttpException
import java.util.*

class MainPresenter(view: MainContract.View) : BasePresenter<MainContract.View>(view), MainContract.Presenter {

//    private var userPermissionUrl = NetUtils.APP_MAIN_URL_LOGIN + "api/token/check"

    private var offshelfTaskUrl = NetUtils.APP_MAIN_URL + "wms/pda/off_shelf/task"

    private var isRefreshMain = false

    private var moveOutSize = 0

    override fun getUserPermission() {


        val token = PreferenceStorageUtils.getInstance().token

        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        val params = HashMap<String, Any>()
        params["token"] = token

        var url = NetUtils.APP_MAIN_URL_LOGIN + "api/token/check"

        if (PreferenceStorageUtils.getInstance().developentState) {
            url = NetUtils.APP_MAIN_URL_LOGIN_TEST + "api/token/check"
        }

        BaseRetrofit
                .createService(IMainApi::class.java)
                .getUserPermission(url, headerMap, params)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : DefaultObserver<PermissionResponseData>() {
                    override fun onComplete() {

                    }

                    override fun onNext(t: PermissionResponseData) {

                        mPermissionResponseData = t
                        val permissionModelList = PermissionUtils.instance.getMainModelFromPermission()
                        view.refreshModelDada(permissionModelList)

                        if (moveOutSize >0) {
                            refreshMoveOutNumber(moveOutSize)
                        }
                        isRefreshMain = true
                    }

                    override fun onError(e: Throwable) {
                        if (e is HttpException && e.code() in 400..403) {
                            val string = e.response()?.errorBody()?.string()
                            if (string != null) {
                                val parseJson = JsonUtils.parseJson<HttpJsonErrorResult>(string)
                                view.showFailedError(parseJson?.message)
                            }
                        } else {
                            view.showFailedError(FFApplication.getInstance().resources.getString(R.string.net_error))
                        }
                    }

                })
    }

    override fun getOffShelfTask() {

        val token = PreferenceStorageUtils.getInstance().token

        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        BaseRetrofit
                .createService(IMoveOutApi::class.java)
                .getOffshelfTask(offshelfTaskUrl, headerMap)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : DefaultObserver<OffShelfTaskResponse>() {
                    override fun onComplete() {
                    }

                    override fun onNext(t: OffShelfTaskResponse) {

                        if (isRefreshMain) {
                            refreshMoveOutNumber(t.taskList?.size?:0)
                        }else {
                            moveOutSize = t.taskList?.size?:0
                        }
                    }

                    override fun onError(e: Throwable) {
                        refreshMoveOutNumber(0)
                    }

                })
    }

    private fun refreshMoveOutNumber(size: Int) {

        val modelDadaList = view.getModelDadaList()

        for (i in modelDadaList.indices) {
            val permissionModel = modelDadaList.get(i)
            val permissionName = permissionModel.permissionName

            if (permissionName?.equals(PermissionName.MOVE_OUT) == true) {
                permissionModel.unread = moveOutSize
            } else {
                permissionModel.unread = 0
            }
        }
        view.refreshModelDada()
        moveOutSize = 0

    }
}