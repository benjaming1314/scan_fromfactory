package club.fromfactory.ui.sign.model;

import club.fromfactory.baselibrary.model.NoProguard;

/**
 * Created by lxm on 2017/7/26.
 * "is_success": true,
 "data": {
 "package_num": 0,
 "start_time": "2017-07-26T07:03:37.988",
 "create_user": "开发者",
 "end_time": null,
 "task_name": "17072607033774",
 "id": 83
 }
 */

public class CreateNewTaskModel implements NoProguard {

    private boolean is_success ;

    private String msg ;

    private SignedTask data ;

    public boolean is_success() {
        return is_success;
    }

    public void setIs_success(boolean is_success) {
        this.is_success = is_success;
    }

    public SignedTask getData() {
        return data;
    }

    public void setData(SignedTask data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "CreateNewTaskModel{" +
                "is_success=" + is_success +
                ", data=" + data +
                '}';
    }
}
