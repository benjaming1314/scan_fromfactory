package club.fromfactory.ui.tracking.model

import club.fromfactory.baselibrary.model.NoProguard
import java.io.Serializable

class MawbListResponse(
        var mawbs: List<MawbInfo> ?= null
) : NoProguard,Serializable