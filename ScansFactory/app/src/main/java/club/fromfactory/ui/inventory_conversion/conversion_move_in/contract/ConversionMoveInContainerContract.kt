package club.fromfactory.ui.inventory_conversion.conversion_move_in.contract

import club.fromfactory.baselibrary.presenter.IPresenter
import club.fromfactory.baselibrary.view.IBaseMVPView
import club.fromfactory.ui.inventory_conversion.conversion_move_out.model.ConversionTaskInfo

class ConversionMoveInContainerContract {

    interface View :IBaseMVPView<Presenter> {

        fun requestTaskSuccess(conversionTaskInfo : ConversionTaskInfo)

        fun requestFailed(message :String?)


    }
    interface Presenter :IPresenter<View> {

        fun getConversionTask(type : String,containerCode :String)

    }
}