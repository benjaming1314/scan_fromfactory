package club.fromfactory.ui.main

import club.fromfactory.ui.main.model.PermissionResponseData
import io.reactivex.Observable
import retrofit2.http.*

interface IMainApi {

    /**
     * 获取用户权限
     */
    @POST
    fun getUserPermission(@Url url :String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any> ,@Body bodyParams: Map<String, @JvmSuppressWildcards Any>): Observable<PermissionResponseData>

}