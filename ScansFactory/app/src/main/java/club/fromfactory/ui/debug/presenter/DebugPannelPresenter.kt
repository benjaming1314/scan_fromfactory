package club.fromfactory.ui.debug.presenter

import club.fromfactory.baselibrary.presenter.BasePresenter
import club.fromfactory.ui.debug.contract.DebugPannelContract

/**
 *@author lxm
 *@date 2018/11/7
 */
class DebugPannelPresenter(view : DebugPannelContract.View) : BasePresenter<DebugPannelContract.View>(view),DebugPannelContract.Presenter{


}