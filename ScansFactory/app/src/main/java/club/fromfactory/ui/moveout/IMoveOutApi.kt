package club.fromfactory.ui.moveout

import club.fromfactory.app_config.model.WareHouseInfor
import club.fromfactory.ui.common.model.Goods
import club.fromfactory.ui.moveout.model.Locations
import club.fromfactory.ui.moveout.model.OffShelfTaskResponse
import io.reactivex.Observable
import retrofit2.http.*

interface IMoveOutApi {

    /**
     * 获取任务
     */
    @GET
    fun getOffshelfTask(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>): Observable<OffShelfTaskResponse>
    /**
     * 获取商品列表
     */
    @GET
    fun getSkuList(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>): Observable<List<Goods>>
    /**
     * 下架
     */
    @PATCH
    fun operateOffShelve(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>, @Body bodyParams: Map<String, @JvmSuppressWildcards Any>): Observable<Goods>

}