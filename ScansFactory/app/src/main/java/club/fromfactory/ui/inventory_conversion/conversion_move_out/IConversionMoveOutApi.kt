package club.fromfactory.ui.inventory_conversion.conversion_move_out

import club.fromfactory.baselibrary.model.BaseResponse
import club.fromfactory.baselibrary.model.EmptyResponse
import club.fromfactory.ui.inventory_conversion.conversion_move_out.model.ConversionTaskInfo
import club.fromfactory.ui.inventory_conversion.conversion_move_out.model.ConversionTaskItemInfo
import io.reactivex.Observable
import retrofit2.http.*

interface IConversionMoveOutApi {

    /**
     */
    @GET
    fun getCvOrderInfo(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>, @QueryMap queryParams: Map<String, @JvmSuppressWildcards Any>): Observable<BaseResponse<ConversionTaskInfo>>

    /**
     */
    @PUT
    fun bindContainer(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>, @Body bodyParams: Map<String, @JvmSuppressWildcards Any>): Observable<BaseResponse<EmptyResponse>>
    /**
     */
    @GET
    fun getCvItemList(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>, @QueryMap queryParams: Map<String, @JvmSuppressWildcards Any>): Observable<BaseResponse<List<ConversionTaskItemInfo>>>
    /**
     */
    @PUT
    fun takeDown(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>, @Body bodyParams: Map<String, @JvmSuppressWildcards Any>): Observable<BaseResponse<EmptyResponse>>
    /**
     */
    @PUT
    fun misssing(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>, @Body bodyParams: Map<String, @JvmSuppressWildcards Any>): Observable<BaseResponse<EmptyResponse>>

}