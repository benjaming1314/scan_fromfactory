package club.fromfactory.ui.wms_signing.model

class SignPackageResult {

    var id :Int ?=null
    var count :Int ?=null
    var tagView : String ?= null

}