package club.fromfactory.ui.common.viewholder

import android.annotation.SuppressLint
import android.view.ViewGroup
import club.fromfactory.R
import club.fromfactory.baselibrary.view.recyclerview.BaseViewHolder
import club.fromfactory.baselibrary.view.recyclerview.ViewHolderCreator
import club.fromfactory.baselibrary.view.recyclerview.ViewHolderCreatorModel
import club.fromfactory.ui.moveout.adapter.MoveOutGoodsListAdapter
import club.fromfactory.ui.moveout.model.TitleModel
import kotlinx.android.synthetic.main.move_out_good_list_title.*

class GoodListTitleViewHolder(parent: ViewGroup?) : BaseViewHolder<ViewHolderCreatorModel<TitleModel>>(parent, R.layout.move_out_good_list_title) {

    @SuppressLint("SetTextI18n")
    override fun bindData(data: ViewHolderCreatorModel<TitleModel>) {
        super.bindData(data)
        move_out_good_list_title_txt_name.text = data.data.title
        val num = data.data.num
        if (num > 0) {
            move_out_good_list_title_txt_num.text = "($num)"
        }else {
            move_out_good_list_title_txt_num.text = ""
        }
    }

}

class GoodListTitleCreator : ViewHolderCreator<ViewHolderCreatorModel<TitleModel>> {

    override fun isForViewType(item: Any, position: Int): Boolean {
        return item is ViewHolderCreatorModel<*> && item.type == MoveOutGoodsListAdapter.TYPE_TITLE
    }

    override fun onCreateBaseViewHolder(parent: ViewGroup): BaseViewHolder<ViewHolderCreatorModel<TitleModel>> {
        return GoodListTitleViewHolder(parent)
    }

}