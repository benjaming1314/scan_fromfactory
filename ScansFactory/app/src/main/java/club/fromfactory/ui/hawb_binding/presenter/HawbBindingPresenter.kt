package club.fromfactory.ui.hawb_binding.presenter

import club.fromfactory.FFApplication
import club.fromfactory.R
import club.fromfactory.baselibrary.language.LanguageUtils
import club.fromfactory.baselibrary.net.CookieHelper
import club.fromfactory.baselibrary.net.retrofit.BaseRetrofit
import club.fromfactory.baselibrary.presenter.BasePresenter
import club.fromfactory.baselibrary.rx.loadingView
import club.fromfactory.baselibrary.utils.JsonUtils
import club.fromfactory.baselibrary.utils.PreferenceStorageUtils
import club.fromfactory.baselibrary.view.RxAppCompatActivity
import club.fromfactory.constant.Constants
import club.fromfactory.http.NetUtils
import club.fromfactory.http.model.HttpJsonErrorResult
import club.fromfactory.ui.hawb_binding.HawbBindingApi
import club.fromfactory.ui.hawb_binding.contract.HawbBindingContract
import club.fromfactory.ui.hawb_binding.model.HawbDetailInfo
import com.trello.rxlifecycle2.kotlin.bindToLifecycle
import io.reactivex.observers.DefaultObserver
import retrofit2.HttpException
import java.util.*

class HawbBindingPresenter(view: HawbBindingContract.View) : BasePresenter<HawbBindingContract.View>(view), HawbBindingContract.Presenter {

    private var hawbDetailInfoUrl = NetUtils.APP_MAIN_URL + "wms/pda/hawb/"

    override fun getHawbDetail(hawbNum: String) {

        val token = PreferenceStorageUtils.getInstance().token

        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        BaseRetrofit
                .createService(HawbBindingApi::class.java)
                .getHawbDetail(hawbDetailInfoUrl+ hawbNum+"/", headerMap)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : DefaultObserver<HawbDetailInfo>() {
                    override fun onComplete() {
                    }

                    override fun onNext(baseResponse: HawbDetailInfo) {
                        view.requestHawbDetail(baseResponse)
                    }

                    override fun onError(e: Throwable) {
                        if (e is HttpException && e.code() in 400..403) {
                            val string = e.response()?.errorBody()?.string()
                            if (string != null) {
                                val parseJson = JsonUtils.parseJson<HttpJsonErrorResult>(string)
                                view.requestFailed(parseJson?.message)
                            }
                        } else {
                            view.requestFailed(FFApplication.getInstance().resources.getString(R.string.net_error))
                        }
                    }

                })
    }
}