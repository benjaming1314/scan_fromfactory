package club.fromfactory.ui.debug.contract

import club.fromfactory.baselibrary.presenter.IPresenter
import club.fromfactory.baselibrary.view.IBaseMVPView

/**
 *@author lxm
 *@date 2018/11/7
 */
interface DebugPannelContract {


    interface Presenter : IPresenter<View> {

    }
    interface View : IBaseMVPView<Presenter> {

    }
}