package club.fromfactory.ui.setting.presenter

import club.fromfactory.FFApplication
import club.fromfactory.R
import club.fromfactory.app_config.WareHouseManager
import club.fromfactory.app_config.model.WareHouseInfor
import club.fromfactory.baselibrary.language.LanguageUtils
import club.fromfactory.baselibrary.net.CookieHelper
import club.fromfactory.baselibrary.net.retrofit.BaseRetrofit
import club.fromfactory.baselibrary.utils.JsonUtils
import club.fromfactory.baselibrary.utils.PreferenceStorageUtils
import club.fromfactory.baselibrary.view.RxAppCompatActivity
import club.fromfactory.constant.Constants
import club.fromfactory.http.NetUtils
import club.fromfactory.http.model.HttpJsonErrorResult
import club.fromfactory.ui.setting.ISelectWareHouseView
import club.fromfactory.ui.splash.ISplashApi
import com.trello.rxlifecycle2.kotlin.bindToLifecycle
import io.reactivex.observers.DefaultObserver
import retrofit2.HttpException
import java.util.*

class SelectWareHousePresenter(var view: ISelectWareHouseView) {
//    private var warehousePdaInfoUrl = NetUtils.APP_MAIN_URL + "wms/static?query=warehouse_pda_info"
    fun getWareHouse() {

        val wareHouseInfoList = WareHouseManager.getInstance().wareHouseInfo

        if (wareHouseInfoList == null || wareHouseInfoList.size == 0) {
            getWarehousePdaInfo(true)
        } else {
            view.refreshWareHouse(wareHouseInfoList)
            getWarehousePdaInfo(false)
        }

    }

    //获取信息
    private fun getWarehousePdaInfo(isRefresh: Boolean) {

        if (isRefresh) {
            view.showLoadingView()
        }

        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)


        var url = NetUtils.APP_WAREHOUSE_URL + "wms/static?query=warehouse_pda_info"


        if (PreferenceStorageUtils.getInstance().developentState) {
            url = NetUtils.APP_WAREHOUSE_URL_TEST + "wms/static?query=warehouse_pda_info"
        }

        BaseRetrofit
                .createService(ISplashApi::class.java)
                .getWarehousePdaInfo(url, headerMap)
                .bindToLifecycle(view as RxAppCompatActivity)
                .subscribe(object : DefaultObserver<List<WareHouseInfor>>() {
                    override fun onComplete() {
                    }

                    override fun onNext(wareHouseInfoList: List<WareHouseInfor>) {
                        view.hideLoadingView()
                        val wareHouseInfoListNew = WareHouseManager.getInstance().saveWareHouseInfo(wareHouseInfoList)
                        if (isRefresh) {
                            view.refreshWareHouse(wareHouseInfoListNew)
                        }
                    }

                    override fun onError(e: Throwable) {
                        view.hideLoadingView()
                        if (e is HttpException && e.code() in 400..403) {
                            val string = e.response()?.errorBody()?.string()
                            if (string != null) {
                                val parseJson = JsonUtils.parseJson<HttpJsonErrorResult>(string)
                                view.showFailedError(parseJson?.message ?: "")
                            }
                        } else {
                            view.showFailedError(FFApplication.getInstance().resources.getString(R.string.net_error))
                        }
                    }

                })

    }

}