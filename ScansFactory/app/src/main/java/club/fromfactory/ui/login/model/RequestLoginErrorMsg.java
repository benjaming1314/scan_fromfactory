package club.fromfactory.ui.login.model;

import club.fromfactory.baselibrary.model.NoProguard;

/**
 * @author lxm
 * @date 2018/7/11/011
 */
public class RequestLoginErrorMsg implements NoProguard {

    private String[] non_field_errors ;

    public String[] getNon_field_errors() {
        return non_field_errors;
    }

    public void setNon_field_errors(String[] non_field_errors) {
        this.non_field_errors = non_field_errors;
    }
}
