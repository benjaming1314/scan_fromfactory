package club.fromfactory.ui.hawb_binding.contract

import android.view.ViewGroup
import club.fromfactory.baselibrary.view.recyclerview.BaseRecyclerAdapter
import club.fromfactory.baselibrary.view.recyclerview.BaseViewHolder
import club.fromfactory.ui.hawb_binding.viewholder.HawbPackageInfoViewHolder
import club.fromfactory.ui.tracking.model.PackageInfo

class HawbPackageInfoAdapter : BaseRecyclerAdapter<PackageInfo>() {
    override fun onCreateBaseViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<PackageInfo> = HawbPackageInfoViewHolder(parent)
}