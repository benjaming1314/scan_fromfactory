package club.fromfactory.ui.container_shelf

import android.content.Intent
import android.view.View
import android.widget.EditText
import club.fromfactory.R
import club.fromfactory.baselibrary.router.RouterConstants
import club.fromfactory.baselibrary.utils.StringUtils
import club.fromfactory.baselibrary.view.BaseActivity
import club.fromfactory.baselibrary.widget.CustomDialog
import club.fromfactory.routerannotaions.Router
import club.fromfactory.ui.common.model.EditTextModel
import club.fromfactory.ui.container_shelf.contract.ContainerShelfContract
import club.fromfactory.ui.container_shelf.model.ContainerResponseData
import club.fromfactory.ui.container_shelf.presenter.ContainerShelfPresenter
import club.fromfactory.ui.main.ScanBaseMVPActivity
import club.fromfactory.utils.EditTextUtils
import club.fromfactory.utils.PromptDialogUtils
import club.fromfactory.utils.TipSound
import club.fromfactory.widget.CustomTitleLinearLayout
import com.blankj.utilcode.util.ToastUtils
import kotlinx.android.synthetic.main.activity_container_shelf.*

@Router(RouterConstants.CONTAINER_SHELF)
class ContainerShelfActivity  : ScanBaseMVPActivity<ContainerShelfContract.Presenter>(),ContainerShelfContract.View{

    private var isHandleScanResult = false

    private var mEdtMaps = HashMap<EditText, EditTextModel>()

    private var customDialog : CustomDialog?= null

    companion object {

        fun launchActivity(activity: BaseActivity) {

            val intent = Intent(activity, ContainerShelfActivity::class.java)
            activity.startActivity(intent)
        }
    }

//    var click = 0
    override fun requestScanStart() {

//        if (Zlog.isDebug) {
//            if(click == 0) {
//                requestScanResult("M0015")
//            }else if(click == 1){
//                requestScanResult("X03-09-42")
//            }
//            click++
//        }

    }

    override fun getLayoutResId(): Int = R.layout.activity_container_shelf

    override fun initData() {
        super.initData()
    }

    override fun initView() {
        super.initView()

        container_shelf_ctl_title.setListener(object : CustomTitleLinearLayout.CustomTitleListener(){

            override fun clickLeft() {
                super.clickLeft()
                finish()
            }
        })

        setScanListener(container_shelf_scan)

        container_shelf_reset.setOnClickListener {
            resetActivity()
        }

        container_shelf_edt_container.setOnFocusChangeListener { v, hasFocus ->
            mEdtMaps[container_shelf_edt_container]?.isFoucus = hasFocus
        }
        container_shelf_edt_boxname.setOnFocusChangeListener { v, hasFocus ->
            mEdtMaps[container_shelf_edt_boxname]?.isFoucus = hasFocus
        }
        EditTextUtils.addEditTextListener(container_shelf_edt_container)
        EditTextUtils.addEditTextListener(container_shelf_edt_boxname)

        container_shelf_btn_container_ok.setOnClickListener {
            if (container_shelf_edt_container.isEnabled) {
                val str = container_shelf_edt_container.text.toString()
                checkContainer(str)
            }
        }

        container_shelf_btn_boxname_ok.setOnClickListener {

            if (container_shelf_edt_boxname.isEnabled) {
                val str = container_shelf_edt_boxname.text.toString()
                shelfBox(str)
            }

        }

        mEdtMaps[container_shelf_edt_container] = EditTextModel(container_shelf_edt_container, null, true)
        mEdtMaps[container_shelf_edt_boxname] = EditTextModel(container_shelf_edt_boxname, null, false)

        container_shelf_edt_boxname.isEnabled = false

    }

    override fun requestScanResult(result: String?) {


        if (customDialog != null && customDialog?.isShowing == true) {
            return
        }
        if (isHandleScanResult) {
            return
        } else {
            isHandleScanResult = true
        }

        var isHasResult = false

        val iterator = mEdtMaps.keys.iterator()
        while (iterator.hasNext()) {
            val it = iterator.next()

            val editTextModel = mEdtMaps[it]
            if (editTextModel?.isFoucus == true) {

                it.setText(result ?: "")
                when {
                    editTextModel.editText == container_shelf_edt_container -> checkContainer(result)
                    editTextModel.editText == container_shelf_edt_boxname -> shelfBox(result)
                    else -> isHasResult = true
                }
                break
            }
        }

        if (!isHasResult) {
            isHandleScanResult = false
        }

    }

    override fun getContainerCode(): String = container_shelf_edt_container.text.toString()

    private fun checkContainer(str :String?) {
        if (StringUtils.isNotBlank(str)) {
            presenter.checkContainer(str?:"")
        }else {
            requestFailed(resources.getString(R.string.container_num_scan_prompt))
        }
    }

    override fun requestContainerConfirmSuccess(containerResponseData: ContainerResponseData) {
        isHandleScanResult = false
        container_shelf_edt_container.isEnabled = false

        refreshContainerData(containerResponseData)

        container_shelf_edt_boxname.isEnabled = true
        container_shelf_edt_boxname.requestFocus()
        container_shelf_edt_boxname.setText("")
    }

    private fun refreshContainerData(containerResponseData: ContainerResponseData) {

        TipSound.getInstance().playScanSucceedSound(this)

        ToastUtils.showLong(resources.getString(R.string.shelf_success_prompt))

        container_shelf_show_content.visibility = View.VISIBLE

        container_shelf_txt_boxname.text = containerResponseData.box_name
        container_shelf_txt_num.text = containerResponseData.total_to_shelve.toString()

    }

    private fun shelfBox(str :String?) {
        if (StringUtils.isNotBlank(str)) {
            presenter.shelfBox(str?:"")
        }else {
            requestFailed(resources.getString(R.string.boxname_scan_prompt))
        }
    }

    override fun shelfSuccess() {
        isHandleScanResult = false
        TipSound.getInstance().playScanSucceedSound(this)
        ToastUtils.showLong(resources.getString(R.string.shelf_success_prompt))
        resetActivity()

    }

    private fun resetActivity() {
        startActivity(intent)
        finish()
//        container_shelf_edt_container.isEnabled = true
//
//        container_shelf_show_content.visibility = View.GONE
//        container_shelf_edt_container.setText("")
//        container_shelf_edt_boxname.setText("")
//        container_shelf_edt_boxname.isEnabled = false
    }

    override fun requestFailed(message: String?) {
        isHandleScanResult = false
//        ToastUtils.showLong(message)
        TipSound.getInstance().playScanFailedSound(this)
        customDialog = PromptDialogUtils.getInstance().getPromptDialog(this, message)
        if (customDialog != null && isAlive) {
            customDialog?.show()
        }
    }

    override fun showLoadingView() {
        showLoadingView(container_shelf_progress)
    }

    override fun hideLoadingView() {
        hideLoadingView(container_shelf_progress)
    }

    override fun onDestroy() {
        super.onDestroy()

        if (customDialog != null) {
            if (customDialog?.isShowing == null) {
                customDialog?.dismiss()
            }
            customDialog = null
        }
    }


    override fun createPresenter(): ContainerShelfContract.Presenter = ContainerShelfPresenter(this)
}
