package club.fromfactory.ui.main;

import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;

import club.fromfactory.baselibrary.utils.StringUtils;
import club.fromfactory.baselibrary.utils.ThreadUtils;
import club.fromfactory.baselibrary.utils.ToastUtils;
import club.fromfactory.baselibrary.view.BaseActivity;
import club.fromfactory.utils.Zlog;
import club.fromfactory.constant.Constants;
import club.fromfactory.scan.ScanView;
import club.fromfactory.scan.ScanViewUtils;
import club.fromfactory.R;
import club.fromfactory.view.ScanResultListner;

public abstract class ScanBaseActivity extends BaseActivity {

    private final int KEYCODE_KEY_SCAN = 286;
    private ScanView mScanView;


    public abstract void requestScanStart();

    @Override
    public void initData() {
        super.initData();

        mScanView = ScanViewUtils.INSTANCE.getScanView(this);

    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Zlog.ii("lxm onKeyDown:" + keyCode + "   " + event + "  " + event.getKeyCode() + "  " + event.getRepeatCount());
        switch (event.getKeyCode()) {

            case KeyEvent.KEYCODE_F5:
            case KEYCODE_KEY_SCAN: {
                startScan(true);
                return true ;
            }
            case KeyEvent.KEYCODE_F9 :
            case KeyEvent.KEYCODE_F10 :
            case KeyEvent.KEYCODE_F11 :
            case KeyEvent.KEYCODE_BUTTON_L1:
            case KeyEvent.KEYCODE_BUTTON_R1:
            case KeyEvent.KEYCODE_BUTTON_L2: {

                if (event.getRepeatCount() == 0) {
                    stopScan();
                    startScan(true);
                }

                return true ;
            }
        }
        return super.onKeyDown(keyCode, event);
    }


    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        Zlog.ii("lxm onKeyUp:" + keyCode + "   " + event + "  " + event.getKeyCode() + "  " + event.getRepeatCount());

        switch (keyCode) {

            case KeyEvent.KEYCODE_BUTTON_L1:
            case KeyEvent.KEYCODE_BUTTON_R1:
            case KeyEvent.KEYCODE_BUTTON_L2:
            case KeyEvent.KEYCODE_F9:
            case KeyEvent.KEYCODE_F11: {
                stopScan();
                return true ;
            }
            case 140: {
                stopScan();
                mScanView.continceScan();
                return true ;
            }

        }
        return super.onKeyUp(keyCode, event);
    }

    public void setScanListener(View btnScan) {
        //nnf3501 的扫描
        if (Constants.PDA_TYPE.equals(Constants.PDA_TYPE_NF3501)) {
            btnScan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startScan(false);
                }
            });
        }
        else{
            btnScan.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {

                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN: {
                            startScan(false);
                        }
                        break;
                        case MotionEvent.ACTION_UP: {
                            stopScan();
                        }
                        break;

                        default:
                            break;
                    }
                    return true;
                }
            });
        }
    }

    private void startScan(boolean isFromKey) {

        requestScanStart();
        if (mScanView != null) {
            mScanView.startScan(isFromKey);
        }

    }

    private void stopScan() {
        if (mScanView != null) {
            mScanView.stopScan();
        }
    }

    private void setListener() {
        mScanView.setScanResultListner(new ScanResultListner() {
            @Override
            public void scanSucceed(final String result) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        String handleResult;

                        if (StringUtils.isNotBlank(result)) {
                            handleResult = result.trim();
                        }else {
                            handleResult = "";
                        }
                        requestScanResult(handleResult);
                    }
                });

            }

            @Override
            public void scanFailed(final String message) {

                ThreadUtils.runOnUi(new Runnable() {
                    @Override
                    public void run() {
                        if (!TextUtils.isEmpty(message)) {
                            ToastUtils.show(message);
                        } else {
                            ToastUtils.show( getResources().getString(R.string.scan_prompt_10));
                        }
                    }
                });

            }

            @Override
            public void showDialog(String str) {
                //                ToastUtils.show(ScanBaseActivity.this, str);
            }

            @Override
            public void showLoadingView() {

                ThreadUtils.runOnUi(new Runnable() {
                    @Override
                    public void run() {
                        ScanBaseActivity.this.showLoadingView();
                    }
                });

            }

            @Override
            public void hideLoadingView() {

                ThreadUtils.runOnUi(new Runnable() {
                    @Override
                    public void run() {
                        ScanBaseActivity.this.hideLoadingView();
                    }
                });

            }
        });
    }

    public void showLoadingView(View view) {
        if (view.getVisibility() == View.GONE) {
            view.setVisibility(View.VISIBLE);
        }
    }
    public void hideLoadingView(View view) {

        if (view.getVisibility() == View.VISIBLE) {
            view.setVisibility(View.GONE);
        }
    }

    public abstract void requestScanResult(String result);

    @Override
    protected void onResume() {
        if (mScanView != null) {
            setListener();
            mScanView.onResume(this);
        }
        super.onResume();
    }

    @Override
    protected void onPause() {

        if (mScanView != null) {
            mScanView.onPause(this);
        }
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        if (mScanView != null) {
            mScanView.onDestroy(this);
        }

        super.onDestroy();
    }
}
