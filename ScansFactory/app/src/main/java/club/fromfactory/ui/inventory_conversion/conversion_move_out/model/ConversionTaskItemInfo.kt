package club.fromfactory.ui.inventory_conversion.conversion_move_out.model

import java.io.Serializable

class ConversionTaskItemInfo: Serializable {

    var cvItemId :Int ?= null
    var cvId :Int ?=null

    var boxName :String ?=null
    var barcode :String ?=null
    var category :String ?=null
    var color :String ?=null
    var imageUrl :String ?=null

}