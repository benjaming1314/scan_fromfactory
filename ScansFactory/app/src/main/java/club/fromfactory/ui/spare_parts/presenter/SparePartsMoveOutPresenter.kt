package club.fromfactory.ui.spare_parts.presenter

import club.fromfactory.baselibrary.language.LanguageUtils
import club.fromfactory.baselibrary.model.BaseResponse
import club.fromfactory.baselibrary.model.EmptyResponse
import club.fromfactory.baselibrary.net.CookieHelper
import club.fromfactory.baselibrary.net.retrofit.BaseRetrofit
import club.fromfactory.baselibrary.presenter.BasePresenter
import club.fromfactory.baselibrary.rx.NetObserver
import club.fromfactory.baselibrary.rx.loadingView
import club.fromfactory.baselibrary.rx.parseThrowableToBaseResponse
import club.fromfactory.baselibrary.utils.PreferenceStorageUtils
import club.fromfactory.baselibrary.utils.StringUtils
import club.fromfactory.baselibrary.view.RxAppCompatActivity
import club.fromfactory.constant.Constants
import club.fromfactory.http.NetUtils
import club.fromfactory.ui.spare_parts.SparePartsApi
import club.fromfactory.ui.spare_parts.contract.SparePartsMoveOutContract
import club.fromfactory.ui.spare_parts.model.SparePartsMoveOutData
import com.trello.rxlifecycle2.kotlin.bindToLifecycle
import io.reactivex.observers.DefaultObserver
import java.util.*

class SparePartsMoveOutPresenter(view: SparePartsMoveOutContract.View) : BasePresenter<SparePartsMoveOutContract.View>(view), SparePartsMoveOutContract.Presenter {

    private var initCheckUrl = NetUtils.APP_MAIN_URL + "wms-center/pda/inventory-move/init-check"
    private var takeOffBarcodeUrl = NetUtils.APP_MAIN_URL + "wms-center/pda/inventory-move/take-off/barcode"
    private var takeOffOverUrl = NetUtils.APP_MAIN_URL + "wms-center/pda/inventory-move/take-off/over"

    override fun initCheck() {

        val token = PreferenceStorageUtils.getInstance().token
        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        BaseRetrofit
                .createService(SparePartsApi::class.java)
                .initCheck(initCheckUrl, headerMap)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : NetObserver<SparePartsMoveOutData>() {
                    override fun onSuccess(t: SparePartsMoveOutData?) {
                        view.initCheckResponce(t)
                    }

                    override fun onFailure(message: String) {
                        view.initCheckResponce(null)
                    }
                })
    }

    override fun takeOffBarcode(barcode: String,boxName :String?) {

        val token = PreferenceStorageUtils.getInstance().token
        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        val params = HashMap<String, Any>()
        params["barcode"] = barcode
        if (StringUtils.isNotBlank(boxName)) {
            params["boxName"] = boxName?:""
        }

        BaseRetrofit
                .createService(SparePartsApi::class.java)
                .takeOffBarcode(takeOffBarcodeUrl, headerMap, params)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : DefaultObserver<BaseResponse<EmptyResponse>>() {
                    override fun onComplete() {
                    }
                    override fun onNext(t: BaseResponse<EmptyResponse>) {
                        if (t.isSuccess) {
                            view.takeOffBarcodeSuccess()
                        } else if (t.errorCode == 54200){
                            view.takeOffBarcodeRequestBoxName(t.firstErrorMessage.displayMessage?:"")
                        }else {
                            view.takeOffBarcodeFailed(t.firstErrorMessage.displayMessage?:"")
                        }
                    }

                    override fun onError(e: Throwable) {
                        val errorResponse = parseThrowableToBaseResponse(e)
                        if (errorResponse != null) {
                            view.takeOffBarcodeFailed( errorResponse.firstErrorMessage.displayMessage?:"")
                        } else {
                            e.message?.apply {
                                view.takeOffBarcodeFailed(this)
                            }
                        }
                    }

                })
    }

    override fun takeOffOver() {

        val token = PreferenceStorageUtils.getInstance().token
        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        BaseRetrofit
                .createService(SparePartsApi::class.java)
                .takeOffOver(takeOffOverUrl, headerMap)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : NetObserver<EmptyResponse>() {
                    override fun onSuccess(t: EmptyResponse?) {
                        view.takeOffOverSuccess()
                    }

                    override fun onFailure(message: String) {
                        view.requestFailed(message)
                    }
                })
    }

}