package club.fromfactory.ui.hawb_binding.model

import com.google.gson.annotations.SerializedName

class HawbBindingResult {

   @SerializedName("need_confirmed")
   var needConfirmed : Boolean ? = false
}