package club.fromfactory.ui.space_transfer

import android.widget.EditText
import club.fromfactory.R
import club.fromfactory.baselibrary.router.RouterConstants
import club.fromfactory.baselibrary.utils.StringUtils
import club.fromfactory.baselibrary.utils.ToastUtils
import club.fromfactory.baselibrary.widget.CustomDialog
import club.fromfactory.routerannotaions.Router
import club.fromfactory.ui.common.model.EditTextModel
import club.fromfactory.ui.main.ScanBaseMVPActivity
import club.fromfactory.ui.space_transfer.contract.SpaceTransferContract
import club.fromfactory.ui.space_transfer.presenter.SpaceTransferPresenter
import club.fromfactory.utils.EditTextUtils
import club.fromfactory.utils.PromptDialogUtils
import club.fromfactory.utils.TipSound
import club.fromfactory.utils.Zlog
import club.fromfactory.widget.CustomTitleLinearLayout
import kotlinx.android.synthetic.main.activity_space_transfer.*

@Router(RouterConstants.SPACE_TRANSFER)
class SpaceTransferActivity : ScanBaseMVPActivity<SpaceTransferContract.Presenter>(), SpaceTransferContract.View {
    private var isHandleScanResult = false

    private var mEdtMaps = HashMap<EditText, EditTextModel>()

    private var customDialog: CustomDialog? = null

    //    var click = 0
    override fun requestScanStart() {
//
//        if (Zlog.isDebug) {
//            if (click == 0) {
//                requestScanResult("X14-01-23")
//            }else if (click == 1) {
//                requestScanResult("X14-01-25")
//            }
//            click++
//        }
    }

    override fun getLayoutResId(): Int = R.layout.activity_space_transfer

    override fun initData() {
        super.initData()
    }

    override fun initView() {
        super.initView()

        space_transfer_ctl_title.setListener(object : CustomTitleLinearLayout.CustomTitleListener() {

            override fun clickLeft() {
                super.clickLeft()
                finish()
            }
        })

        setScanListener(space_transfer_scan)

        space_transfer_edt_shelf_off_location.setOnFocusChangeListener { v, hasFocus ->
            mEdtMaps[space_transfer_edt_shelf_off_location]?.isFoucus = hasFocus
        }
        space_transfer_edt_shelf_location.setOnFocusChangeListener { v, hasFocus ->
            mEdtMaps[space_transfer_edt_shelf_location]?.isFoucus = hasFocus
        }
        EditTextUtils.addEditTextListener(space_transfer_edt_shelf_off_location)
        EditTextUtils.addEditTextListener(space_transfer_edt_shelf_location)

        space_transfer_btn_shelf_off_location_ok.setOnClickListener {
            if (space_transfer_edt_shelf_off_location.isEnabled) {
                val str = space_transfer_edt_shelf_off_location.text.toString()
                checkSourceLocation(str)
            }
        }

        space_transfer_btn_shelf_location_ok.setOnClickListener {

            if (space_transfer_edt_shelf_location.isEnabled) {
                val str = space_transfer_edt_shelf_location.text.toString()
                shelfLocation(str)
            }
        }

        mEdtMaps[space_transfer_edt_shelf_off_location] = EditTextModel(space_transfer_edt_shelf_off_location, null, true)
        mEdtMaps[space_transfer_edt_shelf_location] = EditTextModel(space_transfer_edt_shelf_location, null, false)

        space_transfer_edt_shelf_location.isEnabled = false
    }

    override fun requestScanResult(result: String?) {
        if (customDialog != null && customDialog?.isShowing == true) {
            return
        }
        if (isHandleScanResult) {
            return
        } else {
            isHandleScanResult = true
        }

        var isHasResult = false

        val iterator = mEdtMaps.keys.iterator()
        while (iterator.hasNext()) {
            val it = iterator.next()

            val editTextModel = mEdtMaps[it]
            if (editTextModel?.isFoucus == true) {

                it.setText(result ?: "")
                when {
                    editTextModel.editText == space_transfer_edt_shelf_off_location -> {
                        isHasResult = true
                        checkSourceLocation(result)
                    }
                    editTextModel.editText == space_transfer_edt_shelf_location -> {
                        isHasResult = true
                        shelfLocation(result)
                    }
                }
                break
            }
        }

        if (!isHasResult) {
            isHandleScanResult = false
        }
    }


    private fun checkSourceLocation(str: String?) {
        if (StringUtils.isNotBlank(str)) {
            presenter.checkSourceLocation(str ?: "")
        } else {
            isHandleScanResult = false
            requestFailed(resources.getString(R.string.shelf_off_location_scan_prompt))
        }
    }

    private fun shelfLocation(str: String?) {

        val shelfOffBoxName = space_transfer_edt_shelf_off_location.text.toString()

        if (StringUtils.isNotBlank(str) && StringUtils.isNotBlank(shelfOffBoxName)) {

            presenter.shelfLocation(shelfOffBoxName, str ?: "")
        } else if (StringUtils.isNull(shelfOffBoxName)) {
            isHandleScanResult = false
            requestFailed(resources.getString(R.string.shelf_off_location_scan_prompt))
        } else {
            isHandleScanResult = false
            requestFailed(resources.getString(R.string.shelf_location_scan_prompt))
        }
    }

    override fun checkSourceLocationSuccess() {
        TipSound.getInstance().playScanSucceedSound(this)
        isHandleScanResult = false
        PromptDialogUtils.getInstance().showDialogDefine(this, resources.getString(R.string.shelf_off_location_prompt), object : PromptDialogUtils.DialogCallback {
            override fun onClickPositiveBtn() {
                val str = space_transfer_edt_shelf_off_location.text.toString()
                presenter.shelfOffSourceLocation(str)
            }

            override fun onClickNegativeBtn() {
                space_transfer_edt_shelf_off_location.setText("")
            }
        })
    }

    override fun shelfOffSourceLocationSuccess() {
        ToastUtils.show(resources.getString(R.string.shelf_success_prompt))
        TipSound.getInstance().playScanSucceedSound(this)
        isHandleScanResult = false
        space_transfer_edt_shelf_off_location.isEnabled = false
        space_transfer_edt_shelf_location.isEnabled = true
        space_transfer_edt_shelf_location.requestFocus()
        space_transfer_edt_shelf_location.setText("")
    }

    override fun shelfLocationSuccess() {
        TipSound.getInstance().playScanSucceedSound(this)
        ToastUtils.show(resources.getString(R.string.shelf_location_prompt))
        isHandleScanResult = false
        resetActivity()
    }

    override fun requestFailed(message: String?) {
        isHandleScanResult = false
//        ToastUtils.showLong(message)
        TipSound.getInstance().playScanFailedSound(this)
        customDialog = PromptDialogUtils.getInstance().getPromptDialog(this, message)
        if (customDialog != null && isAlive) {
            customDialog?.show()
        }
    }

    override fun showLoadingView() {
        showLoadingView(space_transfer_progress)
    }

    override fun hideLoadingView() {
        hideLoadingView(space_transfer_progress)
    }

    private fun resetActivity() {
        startActivity(intent)
        finish()
    }

    override fun onDestroy() {
        super.onDestroy()

        if (customDialog != null) {
            if (customDialog?.isShowing == null) {
                customDialog?.dismiss()
            }
            customDialog = null
        }
    }

    override fun createPresenter(): SpaceTransferContract.Presenter = SpaceTransferPresenter(this)

}
