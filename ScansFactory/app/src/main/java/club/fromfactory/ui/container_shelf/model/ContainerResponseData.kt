package club.fromfactory.ui.container_shelf.model

class ContainerResponseData {

    var box_name : String ?= null
    var total_to_shelve : Int ?= 0
}