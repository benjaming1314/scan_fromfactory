package club.fromfactory.ui.inventory_conversion

import club.fromfactory.R
import club.fromfactory.baselibrary.router.RouterConstants
import club.fromfactory.baselibrary.router.RouterManager
import club.fromfactory.baselibrary.view.BaseActivity
import club.fromfactory.routerannotaions.Router
import club.fromfactory.ui.main.PermissionUtils
import club.fromfactory.ui.common.adapter.PermissionModelAdapter
import club.fromfactory.widget.CustomTitleLinearLayout
import kotlinx.android.synthetic.main.activity_inventory_conversion.*

@Router(RouterConstants.INVENTORY_CONVERSION)
class InventoryConversionActivity : BaseActivity() {

    private var mPermissionModelAdapter: PermissionModelAdapter? = null


    override fun clickKeyCodeBack() {
        finish()
    }

    override fun getLayoutResId(): Int = R.layout.activity_inventory_conversion

    override fun initView() {
        super.initView()

        inventory_conversion_ctl_title.setListener(object : CustomTitleLinearLayout.CustomTitleListener() {
            override fun clickLeft() {
                super.clickLeft()
                finish()
            }
        })

        mPermissionModelAdapter = PermissionModelAdapter()

        inventory_conversion_recycler_view.adapter = mPermissionModelAdapter

        mPermissionModelAdapter?.setOnItemViewClickListener { data, _, _ ->
            RouterManager.open(this, data?.routerUrl)
        }
        mPermissionModelAdapter?.addAll(PermissionUtils.instance.getInventoryConversionModelFromPermission())
    }

}
