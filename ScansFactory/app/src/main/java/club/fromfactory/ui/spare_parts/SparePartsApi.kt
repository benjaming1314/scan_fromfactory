package club.fromfactory.ui.spare_parts

import club.fromfactory.baselibrary.model.BaseResponse
import club.fromfactory.baselibrary.model.EmptyResponse
import club.fromfactory.ui.spare_parts.model.SparePartsMoveOutData
import io.reactivex.Observable
import retrofit2.http.*

interface SparePartsApi {


    /**
     * 初始化检查
     */
    @GET
    fun initCheck(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>): Observable<BaseResponse<SparePartsMoveOutData>>

    /**
     * 下架扫描商品条码
     */
    @PATCH
    fun takeOffBarcode(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>, @Body bodyParams: Map<String, @JvmSuppressWildcards Any>): Observable<BaseResponse<EmptyResponse>>
    /**
     * 结束下架
     */
    @GET
    fun takeOffOver(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>): Observable<BaseResponse<EmptyResponse>>
    /**
     * 扫描上架库位
     */
    @GET
    fun shelfBox(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>,@QueryMap queryParams: Map<String, @JvmSuppressWildcards Any>): Observable<BaseResponse<EmptyResponse>>
    /**
     * 上架扫描商品条码
     */
    @PATCH
    fun shelfBarcode(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>,@Body bodyParams: Map<String, @JvmSuppressWildcards Any>): Observable<BaseResponse<EmptyResponse>>
    /**
     * 全部上架
     */
    @PATCH
    fun shelfAll(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>,@Body bodyParams: Map<String, @JvmSuppressWildcards Any>): Observable<BaseResponse<EmptyResponse>>
}