//package club.fromfactory.ui.sign.presenter;
//
//import java.util.List;
//
//import club.fromfactory.utils.Zlog;
//import club.fromfactory.http.HttpRequestResultListner;
//import club.fromfactory.ui.sign.biz.IParceTaskListBiz;
//import club.fromfactory.ui.sign.biz.ParceTaskListBiz;
//import club.fromfactory.ui.sign.view.IParceTaskListView;
//import club.fromfactory.ui.sign.model.CreateNewTaskModel;
//import club.fromfactory.ui.sign.model.SignedTask;
//import club.fromfactory.ui.sign.model.SignedTaskModel;
//
///**
// * Created by lxm on 2017/7/25.
// */
//
//public class ParceTaskListPresenter {
//
//    private IParceTaskListBiz iParceTaskListBiz ;
//    private IParceTaskListView iParceTaskListView;
//
//    public ParceTaskListPresenter(IParceTaskListView iParceTaskListView)
//    {
//        this.iParceTaskListView = iParceTaskListView;
//        this.iParceTaskListBiz = new ParceTaskListBiz();
//    }
//
//    public void createSignedTask() {
//        iParceTaskListView.showLoadingView();
//        iParceTaskListBiz.createSignedTask(new HttpRequestResultListner() {
//            @Override
//            public void requestSucceed(Object o) {
//                iParceTaskListView.hideLoadingView();
//                if (o != null) {
//                    CreateNewTaskModel createNewTaskModel = (CreateNewTaskModel) o;
//
//                    Zlog.ii("lxm ss createSignedTask:" + createNewTaskModel);
//                    if (createNewTaskModel.is_success()) {
//                        SignedTask signedTask = createNewTaskModel.getData();
//                        if (signedTask != null) {
//                            iParceTaskListView.httpResult(signedTask);
//                        }else {
//                            iParceTaskListView.showFailedError("failed");
//                        }
//                    }else {
//                        iParceTaskListView.showFailedError(createNewTaskModel.getMsg());
//                    }
//
//                }else {
//                    iParceTaskListView.showFailedError("failed");
//                }
//            }
//
//            @Override
//            public void requestFailed(String messae) {
//                iParceTaskListView.hideLoadingView();
//                iParceTaskListView.showFailedError("failed");
//            }
//        });
//
//
//    }
//
//    public void getSignTaskList() {
//        iParceTaskListView.showLoadingView();
//        iParceTaskListBiz.getSignTaskList(new HttpRequestResultListner() {
//            @Override
//            public void requestSucceed(Object o) {
//                iParceTaskListView.hideLoadingView();
//                if (o != null) {
//                    SignedTaskModel signedTaskModel = (SignedTaskModel) o;
//                    Zlog.ii("lxm ss getSignTaskList:" + signedTaskModel);
//                    List<SignedTask> signedTaskList =  signedTaskModel.getResults();
//                    if (signedTaskList != null) {
//                        iParceTaskListView.httpResult(signedTaskList);
//                    }else {
//                        iParceTaskListView.showFailedError("failed");
//                    }
//                }else {
//                    iParceTaskListView.showFailedError("failed");
//                }
//            }
//
//            @Override
//            public void requestFailed(String message) {
//                iParceTaskListView.hideLoadingView();
//                iParceTaskListView.showFailedError(message);
//            }
//        });
//
//    }
//
//}
