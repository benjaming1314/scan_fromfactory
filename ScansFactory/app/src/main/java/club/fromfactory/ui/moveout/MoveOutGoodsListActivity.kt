package club.fromfactory.ui.moveout

import android.app.Activity
import android.util.SparseArray
import android.view.View
import club.fromfactory.R
import club.fromfactory.baselibrary.router.RouterConstants
import club.fromfactory.baselibrary.router.RouterManager
import club.fromfactory.baselibrary.router.RouterUrlProvider
import club.fromfactory.baselibrary.utils.DensityUtils
import club.fromfactory.baselibrary.utils.ToastUtils
import club.fromfactory.baselibrary.view.recyclerview.ViewHolderCreatorModel
import club.fromfactory.baselibrary.view.recyclerview.decoration.LinearDecoration
import club.fromfactory.baselibrary.widget.CustomDialog
import club.fromfactory.constant.Constants
import club.fromfactory.routerannotaions.Router
import club.fromfactory.routerannotaions.RouterParam
import club.fromfactory.ui.common.model.Goods
import club.fromfactory.ui.main.ScanBaseMVPActivity
import club.fromfactory.ui.moveout.adapter.MoveOutGoodsListAdapter
import club.fromfactory.ui.moveout.contract.MoveOutGoodsListContract
import club.fromfactory.ui.moveout.model.TitleModel
import club.fromfactory.ui.moveout.presenter.MoveOutListPresenter
import club.fromfactory.ui.moveout.view.IMoveOutGoodsListInterface
import club.fromfactory.utils.PromptDialogUtils
import club.fromfactory.utils.TipSound
import club.fromfactory.utils.Zlog
import club.fromfactory.widget.CustomTitleLinearLayout
import kotlinx.android.synthetic.main.activity_move_out_goods_list_new.*

@Router(RouterConstants.MOVE_OUT_GOODS_LIST)
class MoveOutGoodsListActivity : ScanBaseMVPActivity<MoveOutGoodsListContract.Presenter>(), MoveOutGoodsListContract.View, IMoveOutGoodsListInterface {


    @RouterParam("box_name")
    @JvmField
    var boxName: String? = null

    @RouterParam("is_stock")
    @JvmField
    var isStock: Int = 0

    private var mMoveOutGoodsListAdapter: MoveOutGoodsListAdapter? = null

    private var isHandleScanResult = false

    private var mCurrentScanGoods: Goods? = null //当前扫描商品

    private var mCurrentSkuNo: String? = null

    private var moveOutGoodsList = ArrayList<Goods>() //待移出
    private var confirmGoodsList = ArrayList<Goods>()  //已经移出

    private var mCustomDialog: CustomDialog? = null

    private val mPositionsAndStates = SparseArray<Int>() //显示文本的状态


    override fun getLayoutResId(): Int = R.layout.activity_move_out_goods_list_new

    //    var click = 0
    override fun requestScanStart() {

//        if (Zlog.isDebug) {
//            click++
//            if (click % 2 == 0 ) {
//                requestScanResult("0000715624")
//            }else {
//                requestScanResult("0002487891")
//            }
//        }
    }

    override fun initData() {
        super.initData()
        boxName = intent.getStringExtra(Constants.BOX_NAME)
    }

    override fun initView() {
        super.initView()

        if (isStock == 1){
            move_out_list_new_ctl_title.setTitleCenter(boxName + "(" + resources.getString(R.string.parcel_shopping_type_one) + ")")
        }else {
            move_out_list_new_ctl_title.setTitleCenter(boxName + "(" + resources.getString(R.string.parcel_shopping_type_two) + ")")
        }

        move_out_list_new_ctl_title.setRightVisible(true)
        move_out_list_new_ctl_title.setRightView(R.layout.right_close_layout)

        move_out_list_new_ctl_title.setListener(object : CustomTitleLinearLayout.CustomTitleListener() {

            override fun clickRight() {
                super.clickRight()
                finishActivity()
            }
        })

        mMoveOutGoodsListAdapter = MoveOutGoodsListAdapter()
        move_out_list_new_recycler_view.addItemDecoration(LinearDecoration(DensityUtils.dp2px(this, 5), R.color.color_333333))
        move_out_list_new_recycler_view.adapter = mMoveOutGoodsListAdapter

        setScanListener(move_out_list_new_scan_btn)


        mMoveOutGoodsListAdapter?.setOnItemViewClickListener(this)

    }

    override fun fetchData() {
        super.fetchData()
        presenter.getSkuList()
    }

    override fun requestScanResult(result: String?) {

        if (moveOutGoodsList.size == 0) {
            ToastUtils.show(resources.getString(R.string.move_out_list_no_goods))

            finishActivity()
            return
        }

        if (mCustomDialog != null && mCustomDialog?.isShowing == true) {
            return
        }
        if (isHandleScanResult) {
            return
        } else {
            isHandleScanResult = true
        }

        presenter.operateOffShelve(result ?: "")

//        var isHasGoods = false

//        for (i in 0 until moveOutGoodsList.size) {
//            val goods = moveOutGoodsList[i]
//            val barcodeList = goods.getBarCodeList()
//
//            if (barcodeList.contains(result)) {
//                mCurrentSkuNo = result
//                isHasGoods = true
//                presenter.operateOffShelve(result ?: "", goods.id)
//                break
//            }
//
//        }

//        //，没有对应商品时
//        if (!isHasGoods) {
//            ToastUtils.show(resources.getString(R.string.move_out_no_sku))
//            val goods = Goods()
//
//            goods.barcodeList = Goods.getBarcodeList(result ?: "")
//            goods.scanState = Goods.SCAN_STATE_FAILED
//            scanResult(goods, false)
//
//            isHandleScanResult = false
//        }

    }

    override fun createPresenter(): MoveOutGoodsListContract.Presenter = MoveOutListPresenter(this)

    override fun startMunGoodsPage(count: Int) {
        TipSound.getInstance().playScanSucceedSound(this@MoveOutGoodsListActivity)
        RouterManager.open(this@MoveOutGoodsListActivity, RouterUrlProvider.getMoveOutMulGoods(boxName?:"",count,isStock))
        finish()
    }

    override fun refreshData(goodsList: List<Goods>?) {

        if (goodsList != null && goodsList.isNotEmpty()) {
            moveOutGoodsList.clear()
            moveOutGoodsList.addAll(goodsList)

            val tilteViewHolderCreatorModel = ViewHolderCreatorModel(MoveOutGoodsListAdapter.TYPE_TITLE, TitleModel(resources.getString(R.string.move_out_list), goodsList.size))
            mMoveOutGoodsListAdapter?.add(tilteViewHolderCreatorModel)
            mMoveOutGoodsListAdapter?.addAll(moveOutGoodsList)

        } else {
            ToastUtils.show("no list data")
        }
    }

    override fun moveoutConfirm(goods: Goods?) {
        isHandleScanResult = false
        if (goods == null) {
            return
        }
        goods.scanState = Goods.SCAN_STATE_SUCCEED
        scanResult(goods, true)
    }

    override fun moveoutConfirmFailed(message: String?) {

        ToastUtils.show(message ?: "")

        isHandleScanResult = false

        val goods = Goods()
        goods.barcodeList = Goods.getBarcodeList(mCurrentSkuNo ?: "")
        goods.scanState = Goods.SCAN_STATE_FAILED
        scanResult(goods, false)
    }

    //对应的移出商品位置 + 3
    private fun scanResult(resultGoods: Goods, isSucceed: Boolean) {

        Zlog.ii("lxm ss scanResult:$resultGoods $isSucceed")

        //处理当前扫描商品
        if (mCurrentScanGoods == null) {
            mMoveOutGoodsListAdapter?.insert(resultGoods, 0)
            val tilteViewHolderCreatorModel = ViewHolderCreatorModel(MoveOutGoodsListAdapter.TYPE_TITLE, TitleModel(resources.getString(R.string.current_scan_goods)))
            mMoveOutGoodsListAdapter?.insert(tilteViewHolderCreatorModel, 0)
        } else {
            mMoveOutGoodsListAdapter?.replace(resultGoods, 1)
        }
        mCurrentScanGoods = resultGoods

        if (isSucceed) {
            TipSound.getInstance().playScanSucceedSound(this@MoveOutGoodsListActivity)

            //从位置判断
            for (i in 0 until moveOutGoodsList.size) {
                val goods = moveOutGoodsList[i]
                val barcodeList = goods.getBarCodeList()
                val resultBarcode = resultGoods.barcode
                if (barcodeList.contains(resultBarcode)) {
                    if (confirmGoodsList.size == 0) {
                        val tilteViewHolderCreatorModel = ViewHolderCreatorModel(MoveOutGoodsListAdapter.TYPE_TITLE, TitleModel(resources.getString(R.string.good_comfirm_list_prompt)))
                        mMoveOutGoodsListAdapter?.add(tilteViewHolderCreatorModel)
                    }

                    goods.scanState = Goods.SCAN_STATE_SUCCEED
                    confirmGoodsList.add(goods)
                    moveOutGoodsList.removeAt(i)

                    val tilteViewHolderCreatorModel = ViewHolderCreatorModel(MoveOutGoodsListAdapter.TYPE_TITLE, TitleModel(resources.getString(R.string.move_out_list), moveOutGoodsList.size))
                    mMoveOutGoodsListAdapter?.replace(tilteViewHolderCreatorModel, 2)

                    mMoveOutGoodsListAdapter?.add(goods)
                    mMoveOutGoodsListAdapter?.remove(i + 3)//只会移除一个

                    break
                }
            }
        } else {
            TipSound.getInstance().playScanFailedSound(this@MoveOutGoodsListActivity)
            moveOutGoodsList.forEach {
                val barcodeList = it.getBarCodeList()
                val resultBarcode = resultGoods.barcode
                if (barcodeList.contains(resultBarcode)) {
                    showPromptDialog(resources.getString(R.string.scan_failed_prompt))
                }

            }

        }

        if (moveOutGoodsList.size == 0) {
            ToastUtils.show(resources.getString(R.string.move_out_list_no_goods))
            finishActivity()

        }


    }

    override fun getTextContentState(): SparseArray<Int> {
        return mPositionsAndStates
    }

    override fun onItemViewClick(data: Any?, clickedView: View?, position: Int) {
    }

    override fun showLoadingView() {
        showLoadingView(move_out_list_new_progress)
    }

    override fun hideLoadingView() {
        hideLoadingView(move_out_list_new_progress)
    }

    override fun showFailedError(message: String?) {
        isHandleScanResult = false
        if (message != null) {
            ToastUtils.show(message)
        }
    }

    override fun getBoxName(): String = boxName ?: ""

    override fun clickKeyCodeBack() {
        finishActivity()
    }

    private fun showPromptDialog(message: String) {
        mCustomDialog = PromptDialogUtils.getInstance().getPromptDialog(this@MoveOutGoodsListActivity, message)
        mCustomDialog?.show()
    }


    private fun finishActivity() {
        setResult(Activity.RESULT_OK)
        finish()
    }

    override fun onDestroy() {
        super.onDestroy()

        if (mCustomDialog != null) {
            if (mCustomDialog?.isShowing == true) {
                mCustomDialog?.dismiss()
            }
            mCustomDialog = null
        }

    }


}