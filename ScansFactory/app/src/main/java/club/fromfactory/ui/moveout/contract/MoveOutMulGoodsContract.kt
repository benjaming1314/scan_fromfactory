package club.fromfactory.ui.moveout.contract

import club.fromfactory.baselibrary.presenter.IPresenter
import club.fromfactory.baselibrary.view.IBaseMVPView
import club.fromfactory.ui.common.model.Goods

class MoveOutMulGoodsContract {

    interface View : IBaseMVPView<Presenter> {

        fun getBoxName(): String

        fun moveoutConfirm(goods: Goods?)
        fun moveoutConfirmFailed(message: String?)

    }

    interface Presenter : IPresenter<View> {

        fun operateOffShelve(barcode: String)
    }
}