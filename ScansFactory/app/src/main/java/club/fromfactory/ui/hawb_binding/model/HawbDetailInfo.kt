package club.fromfactory.ui.hawb_binding.model

import club.fromfactory.baselibrary.model.NoProguard
import club.fromfactory.ui.tracking.model.PackageInfo
import java.io.Serializable

class HawbDetailInfo : NoProguard,Serializable{

    var id : Int ?=null
    var name : String ?=null
    var ior_name : String ?=null
    var dest_port : String ?=null
    var planning_out_time : String ?=null
    var large_parcel_list : List<PackageInfo> ?=null


}