package club.fromfactory.ui.inventory_inquire

import club.fromfactory.ui.inventory_inquire.model.InquireResultData
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.HeaderMap
import retrofit2.http.QueryMap
import retrofit2.http.Url

interface IInquireApi {

    /**
     */
    @GET
    fun getInquireResult(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>, @QueryMap queryParams: Map<String, @JvmSuppressWildcards Any>): Observable<InquireResultData>

}