package club.fromfactory.ui.moveout.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import club.fromfactory.ui.moveout.model.Locations;
import club.fromfactory.R;

/**
 * Created by lxm on 2017/2/24.
 */

public class LocationAdapter extends ArrayAdapter<Locations> {

    private LayoutInflater inflater;
    private Context context;
    private List<Locations> object;

    public LocationAdapter(Context context, int resource, List<Locations> objects) {
        super(context, resource, objects);

        this.context = context;
        this.object = objects;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Nullable
    @Override
    public Locations getItem(int position) {
        return super.getItem(position);
    }

    @NonNull
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = inflater.inflate(R.layout.location_item, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }

        if (position % 2 == 1) {
            holder.lyLocationItem.setBackgroundColor(context.getResources().getColor(R.color.color_EDF2F5));
        }else {
            holder.lyLocationItem.setBackgroundColor(context.getResources().getColor(R.color.white));
        }

        Locations item = getItem(position);

        String locationStr = item.getBox_name();
        int goodsNumber = item.getQty();

        holder.txtLocation.setText(locationStr + " ("+goodsNumber+")");
        return view;
    }


    static class ViewHolder {
        @BindView(R.id.txt_location)
        TextView txtLocation;
        @BindView(R.id.ly_location_item)
        LinearLayout lyLocationItem;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
