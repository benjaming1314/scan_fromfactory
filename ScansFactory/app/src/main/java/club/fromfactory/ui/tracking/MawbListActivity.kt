package club.fromfactory.ui.tracking

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.View
import club.fromfactory.R
import club.fromfactory.baselibrary.router.RouterConstants
import club.fromfactory.baselibrary.utils.DensityUtils
import club.fromfactory.baselibrary.utils.ToastUtils
import club.fromfactory.baselibrary.view.BaseMVPActivity
import club.fromfactory.baselibrary.view.recyclerview.BaseSpaceItemDecoration
import club.fromfactory.routerannotaions.Router
import club.fromfactory.ui.tracking.adapter.MawbInfoAdapter
import club.fromfactory.ui.tracking.contract.MawbListContract
import club.fromfactory.ui.tracking.model.MawbInfo
import club.fromfactory.ui.tracking.presenter.MawbListPresenter
import club.fromfactory.widget.CustomTitleLinearLayout
import kotlinx.android.synthetic.main.activity_tracking_list.*

@Router(RouterConstants.MAWB_LIST)
class MawbListActivity : BaseMVPActivity<MawbListContract.Presenter>(), MawbListContract.View {
    override fun createPresenter(): MawbListContract.Presenter = MawbListPresenter(this)

    private var mTrackingInfoAdapter: MawbInfoAdapter? = null

    private var mPageNo = PAGE_NO_INDEX
    private var mPageSize = PAGE_SIZE

    private var mPosition = -1

    companion object {
        const val PAGE_NO_INDEX = 1
        const val PAGE_SIZE = 10
    }

    override fun getLayoutResId(): Int = R.layout.activity_tracking_list

    override fun initData() {
        super.initData()
    }

    override fun initView() {
        super.initView()
        tracking_list_ctl_title.setListener(object : CustomTitleLinearLayout.CustomTitleListener() {

            override fun clickLeft() {
                super.clickLeft()
                finish()
            }

        })

        mTrackingInfoAdapter = MawbInfoAdapter()
        tracking_list_recycler_view.addItemDecoration(BaseSpaceItemDecoration(0, 0, 0, DensityUtils.dp2px(this, 20)))
        tracking_list_recycler_view.adapter = mTrackingInfoAdapter
        mTrackingInfoAdapter?.setOnItemViewClickListener { data, clickedView, position ->

            mPosition = position
            if (data != null) {
                MawbDetailInfoActivity.launchActivity(this, data, 200)
            }
        }

        tracking_list_recycler_view_refreshLayout.setOnRefreshListener {
            tracking_list_recycler_view_refreshLayout.setNoMoreData(false)
            mPageNo = PAGE_NO_INDEX
            fetchData()
        }.setOnLoadMoreListener {
            mPageNo++
            fetchData()

        }

    }

    override fun getPageNo(): Int = mPageNo
    override fun getPageSize(): Int = mPageSize
    override fun fetchData() {
        super.fetchData()
        presenter.getTrackingList()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            if (data != null) {
                val mTrackingInfo = data.getSerializableExtra(MawbDetailInfoActivity.INTENT_DATA) as MawbInfo?
                if (mPosition > 0) {
                    mTrackingInfoAdapter?.replace(mTrackingInfo, mPosition)

                    mPosition = -1
                }
            } else {
                tracking_list_recycler_view_refreshLayout.setNoMoreData(false)
                mPageNo = PAGE_NO_INDEX
                fetchData()
            }
        }

    }

    override fun refreshMawbList(mList: List<MawbInfo>?) {

        if (mList == null) {
            return
        }

        if (mPageNo == PAGE_NO_INDEX) {
            mTrackingInfoAdapter?.clear()
            if (mList.isEmpty()) {
                mTrackingInfoAdapter?.setEmptyView(tracking_list_empty)
            }
        }

        if (mList.size < PAGE_NO_INDEX) {
            tracking_list_recycler_view_refreshLayout.setNoMoreData(true)
        }
        mTrackingInfoAdapter?.addAll(mList)

    }

    override fun clickKeyCodeBack() {
        finish()
    }

    override fun showFailedError(message: String?) {
        ToastUtils.show(message ?: "")
    }

    override fun showLoadingView() {

        if (tracking_list_progress.visibility == View.GONE) {
            tracking_list_progress.visibility = View.VISIBLE
        }
    }

    override fun hideLoadingView() {
        tracking_list_recycler_view_refreshLayout.finishRefresh()
        tracking_list_recycler_view_refreshLayout.finishLoadMore()
        if (tracking_list_progress.visibility == View.VISIBLE) {
            tracking_list_progress.visibility = View.GONE
        }
    }

}
