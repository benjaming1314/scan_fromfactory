package club.fromfactory.ui.inventory_inquire.contract

import club.fromfactory.baselibrary.presenter.IPresenter
import club.fromfactory.baselibrary.view.IBaseMVPView
import club.fromfactory.ui.inventory_inquire.model.InquireResultData

class InquireResultContract {

    interface View : IBaseMVPView<Presenter> {

        fun getSkuid() : String
        fun getBoxName(): String
        fun getItemNo():String
        fun getBarcode():String
        fun getContainerCode():String


        fun requestFailed(message: String?)

        fun requestInquireResult(inquireResultData: InquireResultData)
    }

    interface Presenter : IPresenter<View> {

        fun getInquireResult()
    }
}