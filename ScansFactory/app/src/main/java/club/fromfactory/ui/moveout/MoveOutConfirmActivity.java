package club.fromfactory.ui.moveout;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import club.fromfactory.R;
import club.fromfactory.baselibrary.router.RouterConstants;
import club.fromfactory.baselibrary.router.RouterManager;
import club.fromfactory.baselibrary.router.RouterUrlProvider;
import club.fromfactory.constant.Constants;
import club.fromfactory.routerannotaions.Router;
import club.fromfactory.routerannotaions.RouterParam;
import club.fromfactory.ui.main.ScanBaseMVPActivity;
import club.fromfactory.ui.moveout.adapter.LocationAdapter;
import club.fromfactory.ui.moveout.contract.MoveOutConfirmContract;
import club.fromfactory.ui.moveout.model.Locations;
import club.fromfactory.ui.moveout.model.OffShelfTaskResponse;
import club.fromfactory.ui.moveout.presenter.MoveOutPresenter;
import club.fromfactory.utils.Zlog;
import club.fromfactory.widget.CustomTitleLinearLayout;

@Router(RouterConstants.MOVE_OUT)
public class MoveOutConfirmActivity extends ScanBaseMVPActivity<MoveOutConfirmContract.Presenter> implements MoveOutConfirmContract.View {

    @BindView(R.id.move_out_ctl_title)
    CustomTitleLinearLayout mCtlTitle;
    @BindView(R.id.move_out_scan)
    LinearLayout mScan;
    @BindView(R.id.lv_location)
    ListView lvLocation;
    @BindView(R.id.move_out_confirm_txt_task_num)
    TextView mTxtTaskNum;

    @BindView(R.id.move_out_confirm_progress)
    View mProgress;

    private final int REQUEST_CODE_SKU = 1000;

    private LocationAdapter locationAdapter;
    private List<Locations> locationsList;

    private boolean isStock = false;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_move_out_confirm;
    }

    @Override
    public void clickKeyCodeBack() {
        finishActivity();
    }

    //    int click = 0 ;
    @Override
    public void requestScanStart() {
//        if (Zlog.isDebug) {
//            if (click == 0) {
//                requestScanResult("X99-99-01");
//            }else if (click == 1) {
//                requestScanResult("X01-01-02");
//            }
//            click++ ;
//        }
    }

    @Override
    public void requestScanResult(String result) {
        handleResult(result);
    }

    @Override
    public void initData() {
        super.initData();
    }

    @Override
    public void initView() {
        super.initView();
        mCtlTitle.setListener(new CustomTitleLinearLayout.CustomTitleListener() {
            @Override
            public void clickLeft() {
                super.clickLeft();
                finishActivity();
            }
        });

        setScanListener(mScan);
        locationsList = new ArrayList<>();
        locationAdapter = new LocationAdapter(MoveOutConfirmActivity.this, -1, locationsList);
        lvLocation.setAdapter(locationAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateDataFromNet();
    }

    private void updateDataFromNet() {

        presenter.getOffShelfTask();
    }

    private boolean mIsScan = false;

    private void handleResult(String result) {
        if (mIsScan) return;
        mIsScan = true;
        Zlog.ii("lxm ss handleResult:" + result);

        boolean isHasLocaiton = false;
        for (int i = 0; i < locationsList.size(); i++) {
            String locationStr = locationsList.get(i).getBox_name();
            Zlog.ii("lxm ss handleResult:" + locationStr);
            if (locationStr.equalsIgnoreCase(result)) {
                isHasLocaiton = true;
            }
        }
        int isStockInt = 0 ;
        if(isStock) {
            isStockInt = 1 ;
        }
        if (isHasLocaiton) {
            RouterManager.open(MoveOutConfirmActivity.this, RouterUrlProvider.INSTANCE.getMoveOutList(result, isStockInt),REQUEST_CODE_SKU);
        } else {
            Toast.makeText(MoveOutConfirmActivity.this, getResources().getString(R.string.scan_prompt_4), Toast.LENGTH_SHORT).show();
        }

        mIsScan = false;

    }

    private void finishActivity() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void refreshData(@NotNull OffShelfTaskResponse offShelfTaskResponse) {
        List<Locations> taskList = offShelfTaskResponse.getTaskList();
        isStock = offShelfTaskResponse.isStock();
        Zlog.ii("lxm ss refreshData:" + taskList.size());
        MoveOutConfirmActivity.this.locationsList.clear();
        MoveOutConfirmActivity.this.locationsList.addAll(taskList);

        mTxtTaskNum.setText("(" + taskList.size() + ")");
        locationAdapter.notifyDataSetChanged();

        refreshTitle();
    }

    private void refreshTitle() {
        if (isStock) {
            mCtlTitle.setTitleCenter(getResources().getString(R.string.move_out) + "(" + getResources().getString(R.string.parcel_shopping_type_one) + ")");
        } else {
            mCtlTitle.setTitleCenter(getResources().getString(R.string.move_out)+ "(" + getResources().getString(R.string.parcel_shopping_type_two) + ")");
        }
    }


    @Override
    public void showFailedError(@Nullable String message) {
        if (message != null) {
            Toast.makeText(MoveOutConfirmActivity.this, message, Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(MoveOutConfirmActivity.this, getResources().getString(R.string.net_error_prompt_one), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @NonNull
    @Override
    public MoveOutConfirmContract.Presenter createPresenter() {
        return new MoveOutPresenter(this);
    }

}
