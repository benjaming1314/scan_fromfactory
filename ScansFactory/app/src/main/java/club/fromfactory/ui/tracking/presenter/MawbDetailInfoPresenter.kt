package club.fromfactory.ui.tracking.presenter

import club.fromfactory.FFApplication
import club.fromfactory.R
import club.fromfactory.baselibrary.language.LanguageUtils
import club.fromfactory.baselibrary.model.BaseResponse
import club.fromfactory.baselibrary.model.EmptyResponse
import club.fromfactory.baselibrary.net.CookieHelper
import club.fromfactory.baselibrary.net.retrofit.BaseRetrofit
import club.fromfactory.baselibrary.presenter.BasePresenter
import club.fromfactory.baselibrary.rx.loadingView
import club.fromfactory.baselibrary.utils.JsonUtils
import club.fromfactory.baselibrary.utils.PreferenceStorageUtils
import club.fromfactory.baselibrary.view.RxAppCompatActivity
import club.fromfactory.constant.Constants
import club.fromfactory.http.NetUtils
import club.fromfactory.http.model.HttpJsonErrorResult
import club.fromfactory.ui.tracking.ITrackingApi
import club.fromfactory.ui.tracking.contract.MawbDetailInfoContract
import club.fromfactory.ui.tracking.model.MawbInfo
import club.fromfactory.ui.tracking.model.PackageInfo
import com.trello.rxlifecycle2.kotlin.bindToLifecycle
import io.reactivex.observers.DefaultObserver
import retrofit2.HttpException
import java.util.*

class MawbDetailInfoPresenter(view: MawbDetailInfoContract.View) : BasePresenter<MawbDetailInfoContract.View>(view), MawbDetailInfoContract.Presenter {

//    private var mawbUrl = NetUtils.APP_MAIN_URL_TRACKING + "mawbs/"

    override fun getMawbDetailInfo() {

        val token = PreferenceStorageUtils.getInstance().token

        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        val mawbId = view.getMawbId()

        //设置参数，访问url获取json
        val params = HashMap<String, String>()
        params["mawbId"] = mawbId.toString()

        var url = NetUtils.APP_MAIN_URL_TRACKING + "mawbs/"

        if (PreferenceStorageUtils.getInstance().developentState) {
            url = NetUtils.APP_MAIN_URL_TRACKING_TEST  + "mawbs/"
        }

        val mawbUrl = "$url$mawbId/pda/detail"
        BaseRetrofit
                .createService(ITrackingApi::class.java)
                .getMawbDetailInfo(mawbUrl, headerMap, params)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : DefaultObserver<BaseResponse<MawbInfo>>() {
                    override fun onComplete() {
                    }

                    override fun onNext(response: BaseResponse<MawbInfo>) {
                        if (response.isSuccess) {
                            view.refreshMawbDetailInfo(response.model)
                        } else if (response.errorCode == -3003) {
                            view.transferConfirmIgnoreOption()
                        } else {
                            view.showFailedError(response.firstErrorMessage.message)
                        }
                    }

                    override fun onError(e: Throwable) {
                        if (e is HttpException && e.code() in 400..403) {
                            val string = e.response()?.errorBody()?.string()
                            if (string != null) {
                                val parseJson = JsonUtils.parseJson<HttpJsonErrorResult>(string)
                                view.showFailedError(parseJson?.message)
                            }
                        } else {
                            view.showFailedError(FFApplication.getInstance().resources.getString(R.string.net_error))
                        }
                    }

                })

    }

    override fun mawbBindPackage(packageNo: String) {

        val token = PreferenceStorageUtils.getInstance().token

        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        val mawbId = view.getMawbId()

        var url = NetUtils.APP_MAIN_URL_TRACKING + "mawbs/"

        if (PreferenceStorageUtils.getInstance().developentState) {
            url = NetUtils.APP_MAIN_URL_TRACKING_TEST  + "mawbs/"
        }

        val mawbUrl = "$url$mawbId/packages/$packageNo"
        BaseRetrofit
                .createService(ITrackingApi::class.java)
                .mawbBindPackage(mawbUrl, headerMap)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : DefaultObserver<BaseResponse<PackageInfo>>() {
                    override fun onComplete() {
                    }

                    override fun onNext(response: BaseResponse<PackageInfo>) {
                        if (response.isSuccess) {
                            view.mawbBindPackageSuccess(response.model)
                        } else {
                            view.showFailedErrorWindow(response.firstErrorMessage.message)
                        }
                    }

                    override fun onError(e: Throwable) {
                        if (e is HttpException && e.code() in 400..403) {
                            val string = e.response()?.errorBody()?.string()
                            if (string != null) {
                                val parseJson = JsonUtils.parseJson<HttpJsonErrorResult>(string)
                                view.showFailedError(parseJson?.message)
                            }
                        } else {
                            view.showFailedError(FFApplication.getInstance().resources.getString(R.string.net_error))
                        }
                    }

                })
    }

    override fun transferConfirm(ignoreOption: Int ?) {


        val token = PreferenceStorageUtils.getInstance().token

        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        //设置参数，访问url获取json
        val params = HashMap<String, Any>()
        params["ignoreOption"] = ignoreOption?:0

        val mawbId = view.getMawbId()

        var url = NetUtils.APP_MAIN_URL_TRACKING + "mawbs/"

        if (PreferenceStorageUtils.getInstance().developentState) {
            url = NetUtils.APP_MAIN_URL_TRACKING_TEST  + "mawbs/"
        }

        val mawbUrl = "$url$mawbId/transfer/confirm"
        BaseRetrofit
                .createService(ITrackingApi::class.java)
                .transferConfirm(mawbUrl, headerMap, params)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : DefaultObserver<BaseResponse<EmptyResponse>>() {
                    override fun onComplete() {
                    }

                    override fun onNext(response: BaseResponse<EmptyResponse>) {
                        if (response.isSuccess) {
                            view.transferConfirmSuccess()
                        } else {
                            view.showFailedErrorWindow(response.firstErrorMessage.message)
                        }
                    }

                    override fun onError(e: Throwable) {
                        if (e is HttpException && e.code() in 400..403) {
                            val string = e.response()?.errorBody()?.string()
                            if (string != null) {
                                val parseJson = JsonUtils.parseJson<HttpJsonErrorResult>(string)
                                view.showFailedError(parseJson?.message)
                            }
                        } else {
                            view.showFailedError(FFApplication.getInstance().resources.getString(R.string.net_error))
                        }
                    }

                })

    }

    override fun mawbUnBindPackage(packageNo: String) {

        val token = PreferenceStorageUtils.getInstance().token

        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        val mawbId = view.getMawbId()

        var url = NetUtils.APP_MAIN_URL_TRACKING + "mawbs/"

        if (PreferenceStorageUtils.getInstance().developentState) {
            url = NetUtils.APP_MAIN_URL_TRACKING_TEST  + "mawbs/"
        }

        val mawbUrl = "$url$mawbId/packages/$packageNo"
        BaseRetrofit
                .createService(ITrackingApi::class.java)
                .mawbUnbindPackage(mawbUrl, headerMap)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : DefaultObserver<BaseResponse<EmptyResponse>>() {
                    override fun onComplete() {
                    }

                    override fun onNext(response: BaseResponse<EmptyResponse>) {
                        if (response.isSuccess) {
                            view.mawbUnBindPackageSuccess()
                        } else {
                            view.showFailedErrorWindow(response.firstErrorMessage.message)
                        }
                    }

                    override fun onError(e: Throwable) {
                        if (e is HttpException && e.code() in 400..403) {
                            val string = e.response()?.errorBody()?.string()
                            if (string != null) {
                                val parseJson = JsonUtils.parseJson<HttpJsonErrorResult>(string)
                                view.showFailedError(parseJson?.message)
                            }
                        } else {
                            view.showFailedError(FFApplication.getInstance().resources.getString(R.string.net_error))
                        }
                    }

                })

    }

}