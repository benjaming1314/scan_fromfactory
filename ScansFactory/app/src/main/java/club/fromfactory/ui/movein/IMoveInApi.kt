package club.fromfactory.ui.movein

import club.fromfactory.ui.common.model.Goods
import io.reactivex.Observable
import retrofit2.http.*

interface IMoveInApi {

    /**
     * 移入确认 - 扫描sku_id - 出库单
     */
    @PATCH
    fun operateShelve(@Url url :String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>, @Body bodyParams: Map<String, @JvmSuppressWildcards Any>): Observable<Goods>

}