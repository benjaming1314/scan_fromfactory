package club.fromfactory.ui.wms_signing

import android.view.View
import club.fromfactory.R
import club.fromfactory.baselibrary.router.RouterConstants
import club.fromfactory.baselibrary.router.RouterManager
import club.fromfactory.baselibrary.view.BaseMVPActivity
import club.fromfactory.routerannotaions.Router
import club.fromfactory.ui.common.adapter.PermissionModelAdapter
import club.fromfactory.ui.main.PermissionUtils
import club.fromfactory.ui.wms_signing.contract.SigningTypeListContract
import club.fromfactory.ui.wms_signing.model.OrderDetailInfo
import club.fromfactory.ui.wms_signing.presenter.SigningTypeListPresenter
import club.fromfactory.widget.CustomTitleLinearLayout
import com.blankj.utilcode.util.ToastUtils
import kotlinx.android.synthetic.main.activity_signing_type_list.*

@Router(RouterConstants.SIGNING_TYPE_LIST)
class SigningTypeListActivity : BaseMVPActivity<SigningTypeListContract.Presenter>(),SigningTypeListContract.View {

    companion object {
        //type": 1 // 1-RTO 2-DTO 3-采购 4-调拨
         const val TYPE_SIGNING_TYPE_RTO = 1
         const val TYPE_SIGNING_TYPE_DTO = 2
         const val TYPE_SIGNING_TYPE_PURCHASE = 3
         const val TYPE_SIGNING_TYPE_TRANSFER = 4
    }

    private var selectType = TYPE_SIGNING_TYPE_RTO
    private var selectTitle = ""

    private var mPermissionModelAdapter: PermissionModelAdapter? = null


    override fun getLayoutResId(): Int = R.layout.activity_signing_type_list


    override fun initData() {
        super.initData()
    }

    override fun initView() {
        super.initView()

        signing_type_list_ctl_title.setListener(object : CustomTitleLinearLayout.CustomTitleListener() {
            override fun clickLeft() {
                super.clickLeft()
                finish()
            }
        })

        mPermissionModelAdapter = PermissionModelAdapter()

        signing_type_list_recycler_view.adapter = mPermissionModelAdapter

        mPermissionModelAdapter?.setOnItemViewClickListener { data, _, _ ->
            RouterManager.open(this@SigningTypeListActivity, data?.routerUrl)
        }

        mPermissionModelAdapter?.addAll(PermissionUtils.instance.getSigningTypeModelFromPermission())
    }

    private fun createSignOrder() {
        presenter.createSignOrder(selectType.toString())

    }
    override fun createPresenter(): SigningTypeListContract.Presenter = SigningTypeListPresenter(this)

    override fun requestCreateSignOrderSucceed(orderDetailInfo: OrderDetailInfo) {

//        RouterManager.open(this@SigningTypeListActivity,RouterUrlProvider.getWmsSigningDetailUrl(selectTitle,selectType.toString() ,orderDetailInfo?:""))
        WmsSigningDetailActivity.launchActivity(this@SigningTypeListActivity,selectTitle,false,orderDetailInfo)
    }

    override fun requestFailed(message: String?) {
        ToastUtils.showLong(message)
    }

    override fun showLoadingView() {
        super.showLoadingView()
        if (signing_type_list_progress.visibility == View.GONE) {
            signing_type_list_progress.visibility = View.VISIBLE
        }
    }

    override fun hideLoadingView() {
        super.hideLoadingView()
        if (signing_type_list_progress.visibility == View.VISIBLE) {
            signing_type_list_progress.visibility = View.GONE
        }
    }
}
