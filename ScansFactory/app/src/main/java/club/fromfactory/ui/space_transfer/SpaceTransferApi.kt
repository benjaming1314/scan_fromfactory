package club.fromfactory.ui.space_transfer

import club.fromfactory.baselibrary.model.BaseResponse
import club.fromfactory.baselibrary.model.EmptyResponse
import io.reactivex.Observable
import retrofit2.http.*

interface SpaceTransferApi {

    /**
     * 整库位移库校验下架库位
     */
    @GET
    fun checkForm(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>, @QueryMap queryParams: Map<String, @JvmSuppressWildcards Any>): Observable<BaseResponse<EmptyResponse>>

    /**
     * 确认整库位商品移出
     */
    @PATCH
    fun shelfOffSourceLocation(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>, @Body bodyParams: Map<String, @JvmSuppressWildcards Any>): Observable<BaseResponse<EmptyResponse>>

    /**
     * 扫描目标货位
     */
    @PATCH
    fun shelfLocation(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>, @Body bodyParams: Map<String, @JvmSuppressWildcards Any>): Observable<BaseResponse<EmptyResponse>>
}