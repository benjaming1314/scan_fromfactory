package club.fromfactory.ui.versionupdate.model

import club.fromfactory.baselibrary.model.NoProguard

class VersionModelResponse : NoProguard{

    var model: VersionModel? = null
}