package club.fromfactory.ui.debug

import android.view.Window
import club.fromfactory.R
import club.fromfactory.baselibrary.utils.StringUtils
import club.fromfactory.baselibrary.utils.ToastUtils
import club.fromfactory.baselibrary.view.BaseActivity
import club.fromfactory.baselibrary.widget.CustomDialog

import kotlinx.android.synthetic.main.dialog_debug_verify.*

/**
 *@author lxm
 *@date 2018/11/7
 */
class DebugVerifyDialogUtils {

    private var dialog: CustomDialog? = null

    fun showVerifyDialog(activity: BaseActivity, mDebugVerifyInterface: DebugVerifyInterface): DebugVerifyDialogUtils? {
        if (isShowing(activity)) {
            return null
        }

        dialog = CustomDialog(activity)
        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog?.setContentView(R.layout.dialog_debug_verify)
        dialog?.setCanceledOnTouchOutside(false)
        dialog?.setCancelable(false)

        dialog?.debug_verify_img_close?.setOnClickListener {
            mDebugVerifyInterface.verifyFailed()
            dialog?.dismiss()
        }

        dialog?.setOnDismissListener {

        }

        dialog?.debug_verify_btn_password?.setOnClickListener {
            val mPasword = dialog?.debug_verify_edt_password?.text.toString()

            if (StringUtils.isNull(mPasword)) {
                ToastUtils.show("please input password")
            } else if (mPasword == "331215") {
                mDebugVerifyInterface.verifySuccess()
                dialog?.dismiss()
            } else {
                ToastUtils.show("please input right password")
            }
        }
        if (activity.isAlive) {
            dialog?.show()
        } else {
            mDebugVerifyInterface.verifyFailed()
        }
        return this
    }

    private fun isShowing(activity: BaseActivity): Boolean {
        if (dialog != null && activity.isAlive) {
            return dialog?.isShowing ?: false
        }

        return false
    }

    fun finish(activity: BaseActivity) {
        if (dialog != null && activity.isAlive) {
            if (dialog?.isShowing == true) {
                dialog?.dismiss()
            }

            dialog = null
        }
    }

    interface DebugVerifyInterface {
        fun verifySuccess()
        fun verifyFailed()
    }
}