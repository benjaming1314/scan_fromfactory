package club.fromfactory.ui.moveout.adapter

import android.view.ViewGroup
import club.fromfactory.baselibrary.view.recyclerview.BaseRecyclerAdapter
import club.fromfactory.baselibrary.view.recyclerview.BaseViewHolder
import club.fromfactory.ui.moveout.viewholder.MoveOutMulGoodsViewHolder

class MoveOutMulScanGoodsAdapter :BaseRecyclerAdapter<String>() {

    override fun onCreateBaseViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<String> = MoveOutMulGoodsViewHolder(parent)
}