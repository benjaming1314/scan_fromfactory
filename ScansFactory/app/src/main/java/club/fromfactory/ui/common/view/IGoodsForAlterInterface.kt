package club.fromfactory.ui.common.view

import club.fromfactory.baselibrary.view.recyclerview.BaseRecyclerItemViewClickListener
import club.fromfactory.ui.common.model.Goods

/**
 *@author lxm
 *@date 2019/03/26
 */
interface IGoodsForAlterInterface : BaseRecyclerItemViewClickListener<Any> {

    fun clickAlter(positon : Int ,goods :Goods)

}