package club.fromfactory.ui.splash

import club.fromfactory.app_config.model.WareHouseInfor
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.HeaderMap
import retrofit2.http.Url

interface ISplashApi {

    /**
     * 获取配置
     */
    @GET
    fun getWarehousePdaInfo(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>): Observable<List<WareHouseInfor>>

}