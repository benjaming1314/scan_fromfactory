package club.fromfactory.ui.inventory_conversion.conversion_move_in

import club.fromfactory.baselibrary.model.BaseResponse
import club.fromfactory.baselibrary.model.EmptyResponse
import club.fromfactory.ui.inventory_conversion.conversion_move_in.model.ShelvedResultData
import club.fromfactory.ui.inventory_conversion.conversion_move_out.model.ConversionTaskItemInfo
import io.reactivex.Observable
import retrofit2.http.*

interface IConversionMoveInApi {

    /**
     */
    @GET
    fun getItemDetailFromBarcode(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>, @QueryMap queryParams: Map<String, @JvmSuppressWildcards Any>): Observable<BaseResponse<ConversionTaskItemInfo>>

    /**
     */
    @PUT
    fun shelvedItem(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>, @Body bodyParams: Map<String, @JvmSuppressWildcards Any>): Observable<BaseResponse<ShelvedResultData>>

    /**
     */
    @PUT
    fun finishShelved(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>): Observable<BaseResponse<EmptyResponse>>

}