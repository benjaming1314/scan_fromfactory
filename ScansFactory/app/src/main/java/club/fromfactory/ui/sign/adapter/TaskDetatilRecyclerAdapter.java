package club.fromfactory.ui.sign.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import club.fromfactory.utils.Zlog;
import club.fromfactory.ui.sign.model.TrackingsNoDetail;
import club.fromfactory.R;

/**
 * Created by lxm on 2017/7/25.
 */

public class TaskDetatilRecyclerAdapter extends RecyclerView.Adapter<TaskDetatilRecyclerAdapter.ViewHolder> {

    private List<TrackingsNoDetail> trackingsNoList;

    private LayoutInflater mLayoutInflater;

    private Context mContext;

    public TaskDetatilRecyclerAdapter(Context mContext, List<TrackingsNoDetail> trackingsNoList) {
        this.mContext = mContext;
        this.trackingsNoList = trackingsNoList;
        mLayoutInflater = LayoutInflater.from(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View vv = mLayoutInflater.inflate(R.layout.task_detail_item, parent,false);
        return new ViewHolder(vv);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Zlog.ii("lxm ss onBindViewHolder" + trackingsNoList.size());
        TrackingsNoDetail trackingsNoDetail = trackingsNoList.get(position);
        String trackingNo = trackingsNoDetail.getTracking_no();

        String voideType = trackingsNoDetail.getVoice_type();
        int qtyPo = trackingsNoDetail.getQty_po();

        int signedStatus = trackingsNoDetail.getSigned_status();
        if (position == 0) {
            holder.lyTastDetailItem.setBackgroundColor(mContext.getResources().getColor(R.color.color_CCFFCC));
        } else {
            holder.lyTastDetailItem.setBackgroundColor(mContext.getResources().getColor(R.color.white));
        }

        if (TextUtils.isEmpty(trackingNo)) {
            trackingNo = "" ;
        }
        holder.txtLogisticsNum.setText(trackingNo);
        holder.txtNum.setText(String.valueOf(qtyPo));

        if (signedStatus == -1) {
            if (voideType == null || voideType.equals("null")) {
                holder.imgState.setImageResource(R.mipmap.round_check);
            }else if (TrackingsNoDetail.VOICE_TYPE_BIG.equals(voideType)) {
                holder.imgState.setImageResource(R.mipmap.round_check);
            }else if (TrackingsNoDetail.VOICE_TYPE_NO_TRACKING.equals(voideType)) {
                holder.imgState.setImageResource(R.mipmap.error);
            }
        }else if (signedStatus == 1){
            holder.imgState.setImageResource(R.mipmap.error);
        }else if (signedStatus == 0) {
            holder.imgState.setImageResource(R.mipmap.round_check);
        }
    }

    @Override
    public int getItemCount() {
        return trackingsNoList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View arg0) {
            super(arg0);
            ButterKnife.bind(this, arg0);
        }
        @BindView(R.id.txt_logistics_num)
        TextView txtLogisticsNum;
        @BindView(R.id.txt_num)
        TextView txtNum;
        @BindView(R.id.img_state)
        ImageView imgState;
        @BindView(R.id.ly_tast_detail_item)
        LinearLayout lyTastDetailItem;


        public void setData() {

        }

    }

    /**
     * ItemClick的回调接口
     */
    public interface OnItemClickLitener {
        void onItemClick(int position);
    }

    private OnItemClickLitener mOnItemClickLitener;

    public void setOnItemClickLitener(OnItemClickLitener mOnItemClickLitener) {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }
}
