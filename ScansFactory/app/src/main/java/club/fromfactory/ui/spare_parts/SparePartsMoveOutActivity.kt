package club.fromfactory.ui.spare_parts

import android.app.Activity
import android.content.Intent
import android.view.View
import android.widget.EditText
import club.fromfactory.R
import club.fromfactory.baselibrary.router.RouterConstants
import club.fromfactory.baselibrary.router.RouterManager
import club.fromfactory.baselibrary.router.RouterUrlProvider
import club.fromfactory.baselibrary.utils.JsonUtils
import club.fromfactory.baselibrary.utils.StringUtils
import club.fromfactory.baselibrary.utils.ToastUtils
import club.fromfactory.baselibrary.widget.CustomDialog
import club.fromfactory.routerannotaions.Router
import club.fromfactory.ui.common.model.EditTextModel
import club.fromfactory.ui.main.ScanBaseMVPActivity
import club.fromfactory.ui.spare_parts.adapter.SparePartsGoodsListAdapter
import club.fromfactory.ui.spare_parts.contract.SparePartsMoveOutContract
import club.fromfactory.ui.spare_parts.model.SparePartsMoveOutData
import club.fromfactory.ui.spare_parts.presenter.SparePartsMoveOutPresenter
import club.fromfactory.utils.EditTextUtils
import club.fromfactory.utils.PromptDialogUtils
import club.fromfactory.utils.TipSound
import club.fromfactory.utils.Zlog
import club.fromfactory.widget.CustomTitleLinearLayout
import kotlinx.android.synthetic.main.activity_spare_parts_move_out.*

/**
 * 散件移库下架
 */
@Router(RouterConstants.SPARE_PARTS_MOVE_OUT)
class SparePartsMoveOutActivity : ScanBaseMVPActivity<SparePartsMoveOutContract.Presenter>(),SparePartsMoveOutContract.View {

    private var isHandleScanResult = false
    private var customDialog : CustomDialog?= null
    private var mSparePartsGoodsListAdapter : SparePartsGoodsListAdapter ?= null

    private var mEdtMaps = HashMap<EditText, EditTextModel>()

    companion object {
        private const val REQUEST_CODE_MOVE_IN = 100
    }

//    var click = 0
    override fun requestScanStart() {
//        H20002LGNC,I3uij87976,I3dgh65478
//        if (Zlog.isDebug) {
//
//            if (click == 0) {
//                requestScanResult("0014518394")
//            }else if (click == 1) {
//                requestScanResult("X03-09-42")
//            }else if (click == 2) {
//                requestScanResult("X03-09-42")
//            }
//            click++
//        }

    }

    override fun getLayoutResId(): Int = R.layout.activity_spare_parts_move_out

    override fun initView() {
        super.initView()

        spare_parts_move_out_ctl_title.setListener(object : CustomTitleLinearLayout.CustomTitleListener(){

            override fun clickLeft() {
                super.clickLeft()
                finish()
            }
        })

        setScanListener(spare_parts_move_out_scan)
        spare_parts_move_out_finish_move_out.isEnabled = false
        spare_parts_move_out_finish_move_out.setOnClickListener {
            if (it.isEnabled) {
                presenter.takeOffOver()
            }
        }
        setListener()

        mSparePartsGoodsListAdapter = SparePartsGoodsListAdapter()
        spare_parts_move_out_recycler_view.adapter = mSparePartsGoodsListAdapter
    }

    private fun setListener() {

        spare_parts_move_out_btn_barcode_ok.setOnClickListener {
            if (spare_parts_move_out_edt_barcode.isEnabled) {
                takeOffBarcode(spare_parts_move_out_edt_barcode.text.toString())
            }
        }

        spare_parts_move_out_btn_boxname_ok.setOnClickListener {
            if (spare_parts_move_out_edt_boxname.isEnabled) {
                takeOffBarcodeAndBoxName(spare_parts_move_out_edt_boxname.text.toString())
            }
        }


        spare_parts_move_out_edt_barcode.setOnFocusChangeListener { _, hasFocus ->
            mEdtMaps[spare_parts_move_out_edt_barcode]?.isFoucus = hasFocus
        }
        spare_parts_move_out_edt_boxname.setOnFocusChangeListener { _, hasFocus ->
            mEdtMaps[spare_parts_move_out_edt_boxname]?.isFoucus = hasFocus
        }
        EditTextUtils.addEditTextListener(spare_parts_move_out_edt_barcode)
        EditTextUtils.addEditTextListener(spare_parts_move_out_edt_boxname)

        mEdtMaps[spare_parts_move_out_edt_barcode] = EditTextModel(spare_parts_move_out_edt_barcode, null, true)
        mEdtMaps[spare_parts_move_out_edt_boxname] = EditTextModel(spare_parts_move_out_edt_boxname, null, false)
    }

    override fun fetchData() {
        super.fetchData()
        presenter.initCheck()
    }

    override fun requestScanResult(result: String?) {
        if (customDialog != null && customDialog?.isShowing == true) {
            return
        }
        if (isHandleScanResult) {
            return
        } else {
            isHandleScanResult = true
        }

        var isHasResult = false

        val iterator = mEdtMaps.keys.iterator()
        while (iterator.hasNext()) {
            val it = iterator.next()

            val editTextModel = mEdtMaps[it]
            if (editTextModel?.isFoucus == true) {
                it.setText(result ?: "")
                when {
                    editTextModel.editText == spare_parts_move_out_edt_barcode -> {
                        isHasResult = true
                        takeOffBarcode(result)
                    }
                    editTextModel.editText == spare_parts_move_out_edt_boxname -> {
                        isHasResult = true
                        takeOffBarcodeAndBoxName(result)
                    }
                }
                break
            }
        }

        if (!isHasResult) {
            isHandleScanResult = false
        }


    }

    private fun refreshTotalNum() {
        val size = mSparePartsGoodsListAdapter?.data?.size?:0
        spare_parts_move_out_total_num.text = size.toString()
        spare_parts_move_out_finish_move_out.isEnabled = size > 0
    }

    private fun takeOffBarcode(barcode:String?){

        if (StringUtils.isNotBlank(barcode)) {
            presenter.takeOffBarcode(barcode?:"","")
        }else {
            isHandleScanResult = false
            requestFailed(resources.getString(R.string.barcode_scan_prompt))
        }
    }
    private fun takeOffBarcodeAndBoxName(boxName:String?){

        val barcode = spare_parts_move_out_edt_barcode.text.toString()

        if (StringUtils.isNotBlank(boxName)) {
            presenter.takeOffBarcode(barcode,boxName)
        }else {
            isHandleScanResult = false
            requestFailed(resources.getString(R.string.scann_prompt_location))
        }
    }

    override fun initCheckResponce(sparePartsMoveOutData: SparePartsMoveOutData?) {
        isHandleScanResult = false
        if (sparePartsMoveOutData != null) {
            mSparePartsGoodsListAdapter?.clear()
            mSparePartsGoodsListAdapter?.addAll(sparePartsMoveOutData.barcodeList)
            refreshTotalNum()
        }
    }

    override fun takeOffBarcodeSuccess() {
        isHandleScanResult = false
        TipSound.getInstance().playScanSucceedSound(this)

        ToastUtils.show(resources.getString(R.string.shelf_off_success))
        val barcode = spare_parts_move_out_edt_barcode.text.toString()
        mSparePartsGoodsListAdapter?.insert(barcode,0)
        mSparePartsGoodsListAdapter?.notifyDataSetChanged()
        refreshTotalNum()

        spare_parts_move_out_edt_barcode.setText("")
        spare_parts_move_out_edt_boxname.setText("")
        spare_parts_move_out_boxname.visibility = View.GONE
        spare_parts_move_out_edt_barcode.isEnabled = true
        spare_parts_move_out_edt_barcode.requestFocus()

    }

    override fun takeOffBarcodeFailed(message: String?) {

        requestFailed(message)
        spare_parts_move_out_edt_boxname.setText("")
        spare_parts_move_out_boxname.visibility = View.GONE
        spare_parts_move_out_edt_barcode.isEnabled = true
        spare_parts_move_out_edt_barcode.requestFocus()

    }

    override fun takeOffBarcodeRequestBoxName(message: String?) {
        isHandleScanResult = false

        ToastUtils.show(message?:"")
        spare_parts_move_out_boxname.visibility = View.VISIBLE
        spare_parts_move_out_edt_barcode.isEnabled = false
        spare_parts_move_out_edt_boxname.isEnabled = true
        spare_parts_move_out_edt_boxname.requestFocus()
    }

    override fun takeOffOverSuccess() {
        TipSound.getInstance().playScanSucceedSound(this)
        isHandleScanResult = false
        val data = mSparePartsGoodsListAdapter?.data
        RouterManager.open(this,RouterUrlProvider.getSparePartsMoveInUrl(JsonUtils.toJson(data?:ArrayList<String>())),REQUEST_CODE_MOVE_IN)
    }

    override fun requestFailed(message: String?) {
        isHandleScanResult = false
//        ToastUtils.showLong(message)
        TipSound.getInstance().playScanFailedSound(this)
        customDialog = PromptDialogUtils.getInstance().getPromptDialog(this, message)
        if (customDialog != null && isAlive) {
            customDialog?.show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CODE_MOVE_IN) {
                startActivity(intent)
                finish()
            }
        } else {
            fetchData()
        }
    }

    override fun showLoadingView() {
        showLoadingView(spare_parts_move_out_progress)
    }

    override fun hideLoadingView() {
        hideLoadingView(spare_parts_move_out_progress)
    }

    override fun onDestroy() {
        super.onDestroy()

        if (customDialog != null) {
            if (customDialog?.isShowing == null) {
                customDialog?.dismiss()
            }
            customDialog = null
        }
    }

    override fun createPresenter(): SparePartsMoveOutContract.Presenter = SparePartsMoveOutPresenter(this)
}
