package club.fromfactory.ui.setting.adapter

import android.view.ViewGroup
import club.fromfactory.app_config.model.DeviceInfo
import club.fromfactory.baselibrary.view.recyclerview.BaseRecyclerAdapter
import club.fromfactory.baselibrary.view.recyclerview.BaseViewHolder
import club.fromfactory.ui.setting.viewholder.DeviceViewHolder

class DeviceAdapter : BaseRecyclerAdapter<DeviceInfo>() {
    override fun onCreateBaseViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<DeviceInfo> = DeviceViewHolder(parent)

}