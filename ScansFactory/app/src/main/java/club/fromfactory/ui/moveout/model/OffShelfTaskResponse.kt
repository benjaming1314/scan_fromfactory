package club.fromfactory.ui.moveout.model

import com.google.gson.annotations.SerializedName

class OffShelfTaskResponse {

    @SerializedName("task_list") var taskList : List<Locations> ?= null
    @SerializedName("is_stock") var isStock : Boolean ?= false
}