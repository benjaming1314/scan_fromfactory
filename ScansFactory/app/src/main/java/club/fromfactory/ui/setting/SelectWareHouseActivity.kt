package club.fromfactory.ui.setting

import android.app.Activity
import android.content.Intent
import android.view.View
import club.fromfactory.R
import club.fromfactory.app_config.WareHouseManager
import club.fromfactory.app_config.model.WareHouseInfor
import club.fromfactory.baselibrary.utils.ToastUtils
import club.fromfactory.baselibrary.view.BaseActivity
import club.fromfactory.constant.Constants
import club.fromfactory.ui.login.LoginScanActivity
import club.fromfactory.ui.setting.adapter.WareHouseAdapter
import club.fromfactory.ui.setting.presenter.SelectWareHousePresenter
import club.fromfactory.utils.Utils
import club.fromfactory.utils.Zlog
import club.fromfactory.widget.CustomTitleLinearLayout
import kotlinx.android.synthetic.main.activity_select_ware_house.*

class SelectWareHouseActivity : BaseActivity(), ISelectWareHouseView {


    private var mSelectWareHousePresenter: SelectWareHousePresenter? = null

    private var mWareHouseAdapter: WareHouseAdapter? = null

    companion object {
        const val IS_SHOW_BACK = "is_show_back"

        fun launchActivity(activity: BaseActivity, isShowBack: Boolean, requestCode: Int) {

            val intent = Intent(activity, SelectWareHouseActivity::class.java)
            intent.putExtra(IS_SHOW_BACK, isShowBack)
            activity.startActivityForResult(intent, requestCode)
        }
    }

    private var isShowBack = false


    override fun getLayoutResId(): Int = R.layout.activity_select_ware_house


    override fun initData() {
        super.initData()
        isShowBack = intent.getBooleanExtra(IS_SHOW_BACK, false)

        mSelectWareHousePresenter = SelectWareHousePresenter(this)
    }

    override fun initView() {
        super.initView()

        select_warehouse_ctl_title.setLeftTextVisible(isShowBack)
        select_warehouse_ctl_title.setListener(object : CustomTitleLinearLayout.CustomTitleListener() {

            override fun clickLeft() {
                super.clickLeft()
                finish()
            }
        })


        select_warehouse_current_warehouse.setOnClickListener {

            if (!isShowBack) {
                LoginScanActivity.launchActivity(this)
            }
            finish()

        }

        select_warehouse_txt_current_warehouse.text = Constants.HOUSE_NAME


        mWareHouseAdapter = WareHouseAdapter()

        select_warehouse_recycler_view.adapter = mWareHouseAdapter

        mWareHouseAdapter?.setOnItemViewClickListener { data, clickedView, position ->


            Zlog.ii("loginscanactivity initData:$data")
            val changeHouse = WareHouseManager.getInstance().changeHouse(data)
            if (changeHouse) {
                if (isShowBack) {
                    setResult(Activity.RESULT_OK)
                } else {
                    Utils.restartApp(this@SelectWareHouseActivity)
                }
                finish()
            } else {
                if (!isShowBack) {
                    LoginScanActivity.launchActivity(this)
                }
                finish()
            }
        }

    }

    override fun fetchData() {
        super.fetchData()
        mSelectWareHousePresenter?.getWareHouse()
    }

    override fun refreshWareHouse(wareHouseInfoList: List<WareHouseInfor>) {
        mWareHouseAdapter?.addAll(wareHouseInfoList)
    }

    override fun showLoadingView() {
        if (select_warehouse_progress.visibility == View.GONE) {
            select_warehouse_progress.visibility = View.VISIBLE
        }
    }

    override fun hideLoadingView() {
        if (select_warehouse_progress.visibility == View.VISIBLE) {
            select_warehouse_progress.visibility = View.GONE
        }
    }

    override fun showFailedError(message: String?) {
        ToastUtils.show(message ?: "")
    }

    override fun clickKeyCodeBack() {

        select_warehouse_current_warehouse.callOnClick()
    }

}
