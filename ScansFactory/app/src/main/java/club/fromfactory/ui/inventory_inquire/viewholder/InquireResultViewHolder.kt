package club.fromfactory.ui.inventory_inquire.viewholder

import android.view.ViewGroup
import club.fromfactory.R
import club.fromfactory.baselibrary.view.recyclerview.BaseViewHolder
import club.fromfactory.ui.common.model.Goods
import kotlinx.android.synthetic.main.inquire_result_item.*

class InquireResultViewHolder(parent:ViewGroup) : BaseViewHolder<Goods>(parent, R.layout.inquire_result_item) {


    override fun bindData(data: Goods) {
        super.bindData(data)
        inquire_result_item_txt_location.text = data.box_name
        inquire_result_item_txt_barcode.text = data.barcode
        inquire_result_item_txt_num.text = data.qty.toString()
        inquire_result_item_txt_state.text = data.getStockState()
        inquire_result_item_txt_container_code.text = data.containerCode

    }

}