package club.fromfactory.ui.main.contract

import club.fromfactory.baselibrary.presenter.IPresenter
import club.fromfactory.baselibrary.view.IBaseMVPView
import club.fromfactory.ui.main.model.PermissionModel

class MainContract {


    interface View : IBaseMVPView<Presenter> {

        fun refreshModelDada(permissionModelList: List<PermissionModel>)

        fun refreshModelDada()

        fun getModelDadaList(): List<PermissionModel>

        fun showFailedError(message: String?)

    }

    interface Presenter : IPresenter<View> {

        fun getUserPermission()

        fun getOffShelfTask()
    }
}