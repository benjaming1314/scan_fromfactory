package club.fromfactory.ui.moveout.viewholder

import android.content.Intent
import android.text.TextUtils
import android.util.SparseArray
import android.view.View
import android.view.ViewGroup
import club.fromfactory.R
import club.fromfactory.utils.ImageUtils
import club.fromfactory.baselibrary.utils.StringUtils
import club.fromfactory.baselibrary.view.recyclerview.BaseViewHolder
import club.fromfactory.baselibrary.view.recyclerview.ViewHolderCreator
import club.fromfactory.ui.common.model.Goods
import club.fromfactory.ui.moveout.view.IMoveOutGoodsListInterface
import club.fromfactory.ui.viewphotos.ViewPhotosActivity
import club.fromfactory.widget.ExpandableTextView
import kotlinx.android.synthetic.main.moveout_list_goods_item.*

class GoodsForMoveOutViewHolder(parent: ViewGroup?) : BaseViewHolder<Goods>(parent, R.layout.moveout_list_goods_item) {

    private var textContentState: SparseArray<Int>? = null

    private var mEtxWidth: Int = 0
    override fun bindData(goods: Goods) {
        super.bindData(goods)

        ImageUtils.loadImageNew(move_out_goods_item_img, goods.sku_image_url, R.color.white)


        move_out_goods_item_img.setOnClickListener {

            val intent = Intent(itemView.context, ViewPhotosActivity::class.java)
            intent.putExtra("images", goods.sku_image_url)
            itemView.context.startActivity(intent)
        }

        val color = goods.goodsAttribute?.color

        if (StringUtils.isNull(color)) {
            move_out_goods_item_txt_color.visibility = View.GONE
        } else {
            move_out_goods_item_txt_color.visibility = View.VISIBLE

            move_out_goods_item_txt_color.text = color
        }

        val skuName = goods.skuName

        if (StringUtils.isNull(skuName)) {
            move_out_goods_item_txt_type.visibility = View.GONE
        } else {
            move_out_goods_item_txt_type.visibility = View.VISIBLE

            move_out_goods_item_txt_type.text = skuName
        }

        if (mRecyclerItemViewClickListener != null) {
            textContentState = (mRecyclerItemViewClickListener as IMoveOutGoodsListInterface).getTextContentState()
        }

        if (textContentState == null) {
            textContentState = SparseArray<Int>()
        }

//        move_out_goods_item_txt_sku.text = goods.getBarCodeShowStr()

        val barCodeShowStr = goods.getBarCodeShowStr()
        refreshBarcode(barCodeShowStr,layoutPosition)

        val scanState = goods.scanState

        when (scanState) {
            Goods.SCAN_STATE_DEFAULT -> {
                move_out_goods_item_state_right.visibility = View.GONE
                move_out_goods_item_state_error.visibility = View.GONE
            }
            Goods.SCAN_STATE_FAILED -> {
                move_out_goods_item_state_right.visibility = View.GONE
                move_out_goods_item_state_error.visibility = View.VISIBLE
            }
            Goods.SCAN_STATE_SUCCEED -> {
                move_out_goods_item_state_right.visibility = View.VISIBLE
                move_out_goods_item_state_error.visibility = View.GONE
            }

            else -> {
                move_out_goods_item_state_right.visibility = View.GONE
                move_out_goods_item_state_error.visibility = View.GONE
            }
        }

    }


    private fun refreshBarcode(content: String?, position: Int) {
        if (TextUtils.isEmpty(content)) {
            move_out_goods_item_txt_sku.visibility = View.GONE
            fold.visibility = View.GONE
            return
        }

        move_out_goods_item_txt_sku.visibility = View.VISIBLE
        move_out_goods_item_txt_sku.post {
            mEtxWidth = move_out_goods_item_txt_sku.width
            val state = textContentState?.get(position)
            move_out_goods_item_txt_sku.updateForRecyclerView(content, mEtxWidth,
                    state ?: ExpandableTextView.STATE_SHRINK)
            if (move_out_goods_item_txt_sku.isShowFoldHint) {
                if (ExpandableTextView.STATE_EXPAND == move_out_goods_item_txt_sku.expandState) {
                    fold.text = itemView.context.getString(R.string.fold)
                } else {
                    fold.text = itemView.context.getString(R.string.unfold)
                }
                fold.visibility = View.VISIBLE
            } else {
                fold.visibility = View.GONE
            }
        }

        fold.setOnClickListener {
            val isUnFold = move_out_goods_item_txt_sku.getExpandState() == ExpandableTextView.STATE_EXPAND

            move_out_goods_item_txt_sku.updateForRecyclerView(content, mEtxWidth,
                    if (isUnFold)
                        ExpandableTextView.STATE_SHRINK
                    else
                        ExpandableTextView.STATE_EXPAND)
            textContentState?.put(position, move_out_goods_item_txt_sku.expandState)
            fold.text = itemView.context.getString(if (isUnFold) R.string.unfold else R.string.fold)
        }

        move_out_goods_item_txt_sku.setExpandListener(object : ExpandableTextView.OnExpandListener {
            override fun onExpand(view: ExpandableTextView) {

            }

            override fun onShrink(view: ExpandableTextView) {

            }

            override fun onTextLinkClick(clickedString: String) {
            }
        })
    }


}

class GoodsForMoveOutCreator : ViewHolderCreator<Goods> {

    override fun isForViewType(item: Any, position: Int): Boolean {
        return item is Goods
    }

    override fun onCreateBaseViewHolder(parent: ViewGroup): BaseViewHolder<Goods> {
        return GoodsForMoveOutViewHolder(parent)
    }

}