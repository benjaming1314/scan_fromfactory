package club.fromfactory.ui.inventory_conversion.conversion_move_out.contract

import club.fromfactory.baselibrary.presenter.IPresenter
import club.fromfactory.baselibrary.view.IBaseMVPView
import club.fromfactory.ui.inventory_conversion.conversion_move_out.model.ConversionTaskInfo

class ConversionTaskContract {

    interface View : IBaseMVPView<Presenter> {

        fun bindContainerSucceed()

        fun requestSuccess(conversionTaskInfo : ConversionTaskInfo)
        fun requestSuccess()

        fun requestFailed(message :String?)

    }
    interface Presenter :IPresenter<View> {
        fun getConversionTask(type : String)
        fun bindContainer(cvId: Int, containerCode: String)

    }
}