package club.fromfactory.ui.inventory_conversion.conversion_move_in

import android.app.Activity
import android.content.Intent
import android.widget.EditText
import club.fromfactory.R
import club.fromfactory.baselibrary.utils.StringUtils
import club.fromfactory.baselibrary.utils.ToastUtils
import club.fromfactory.baselibrary.view.BaseActivity
import club.fromfactory.baselibrary.widget.CustomDialog
import club.fromfactory.ui.common.model.EditTextModel
import club.fromfactory.ui.inventory_conversion.conversion_move_in.contract.ConversionMoveInDetailContract
import club.fromfactory.ui.inventory_conversion.conversion_move_in.model.ShelvedResultData
import club.fromfactory.ui.inventory_conversion.conversion_move_in.presenter.ConversionMoveInDetailPresenter
import club.fromfactory.ui.inventory_conversion.conversion_move_out.model.ConversionTaskInfo
import club.fromfactory.ui.inventory_conversion.conversion_move_out.model.ConversionTaskItemInfo
import club.fromfactory.ui.main.ScanBaseMVPActivity
import club.fromfactory.ui.viewphotos.ViewPhotosActivity
import club.fromfactory.utils.*
import club.fromfactory.widget.CustomTitleLinearLayout
import kotlinx.android.synthetic.main.activity_conversion_move_in_detail.*

/**
 * 转换上架 详情页面
 */
class ConversionMoveInDetailActivity : ScanBaseMVPActivity<ConversionMoveInDetailContract.Presenter>(), ConversionMoveInDetailContract.View {
    override fun createPresenter(): ConversionMoveInDetailContract.Presenter = ConversionMoveInDetailPresenter(this)

    companion object {

        private const val INTENT_TASK_INFO = "task_info"
//        private const val TYPE_WAITING_PUT = "waiting_put"

        fun launchActivity(activity: BaseActivity, conversionTaskInfo: ConversionTaskInfo, requestCode: Int) {

            val intent = Intent(activity, ConversionMoveInDetailActivity::class.java)
            intent.putExtra(INTENT_TASK_INFO, conversionTaskInfo)
            activity.startActivityForResult(intent, requestCode)
        }

    }

    private var customDialog: CustomDialog? = null


    private var mEdtMaps = HashMap<EditText, EditTextModel>()


    private var isHandleScanResult = false

    private var mConversionTaskInfo: ConversionTaskInfo? = null

    private var mConversionTaskItemInfo: ConversionTaskItemInfo? = null

    private var isClearEditText = false

    override fun clickKeyCodeBack() {
        finish()
    }

    //    var click = 1
    override fun requestScanStart() {

//    if (Zlog.isDebug) {
//        if (click % 2 == 1) {
//            requestScanResult("0000743261")
//        }else if (click % 2 == 0) {
//            requestScanResult("X04-03-11")
//        }
//
//        click++
//    }

    }

    override fun getLayoutResId(): Int = R.layout.activity_conversion_move_in_detail


    override fun initData() {
        super.initData()
        mConversionTaskInfo = intent.getSerializableExtra(INTENT_TASK_INFO) as ConversionTaskInfo?

    }

    override fun initView() {
        super.initView()

        conversion_move_in_detail_ctl_title.setListener(object : CustomTitleLinearLayout.CustomTitleListener() {

            override fun clickLeft() {
                super.clickLeft()
                finish()
            }
        })

        setScanListener(conversion_move_in_detail_detail_scan)

        conversion_move_in_detail_item_img.setOnClickListener {
            val intent = Intent(this, ViewPhotosActivity::class.java)
            intent.putExtra("images", mConversionTaskItemInfo?.imageUrl)
            startActivity(intent)
        }

        conversion_move_in_detail_detai_sku_ok.setOnClickListener {
            getItemDetailFromBarcode()
        }
        conversion_move_in_detail_detail_btn_ok.setOnClickListener {
            shelvedItem()
        }
        conversion_move_in_detail_detail_complete_conversion.setOnClickListener {

            PromptDialogUtils.getInstance().showDialog(this, resources.getString(R.string.complet_conversion_move_in_prompt), object : PromptDialogUtils.DialogCallback {
                override fun onClickPositiveBtn() {
                    presenter.finishShelved()
                }

                override fun onClickNegativeBtn() {
                }
            })

        }

        conversion_move_in_detail_detail_edt_sku.setOnFocusChangeListener { v, hasFocus ->

            mEdtMaps[conversion_move_in_detail_detail_edt_sku]?.isFoucus = hasFocus
            if (!hasFocus) {
                getItemDetailFromBarcode()
            }

        }

        conversion_move_in_detail_detail_edt_location.setOnFocusChangeListener { v, hasFocus ->
            mEdtMaps[conversion_move_in_detail_detail_edt_location]?.isFoucus = hasFocus
        }

        EditTextUtils.addEditTextListener(conversion_move_in_detail_detail_edt_sku)
        EditTextUtils.addEditTextListener(conversion_move_in_detail_detail_edt_location)

        mEdtMaps[conversion_move_in_detail_detail_edt_sku] = EditTextModel(conversion_move_in_detail_detail_edt_sku, conversion_move_in_detail_detail_edt_location, true)
        mEdtMaps[conversion_move_in_detail_detail_edt_location] = EditTextModel(conversion_move_in_detail_detail_edt_location, null, false)

    }

    override fun fetchData() {
        super.fetchData()
        refreshTask()
    }

    override fun requestScanResult(result: String?) {

        if (customDialog != null && customDialog?.isShowing == true) {
            return
        }

        if (isHandleScanResult) {
            return
        } else {
            isHandleScanResult = true
        }

        var isHasResult = false

        val iterator = mEdtMaps.keys.iterator()
        while (iterator.hasNext()) {

            val it = iterator.next()
            val editTextModel = mEdtMaps[it]
            Zlog.ii("lxm sda5:" + mEdtMaps[it].toString())
            if (editTextModel?.isFoucus == true) {

                it.setText(result ?: "")

                val nextEditText = editTextModel.nextEditText
                if (nextEditText == null) {
                    isHasResult = true
                    shelvedItem()
                } else {
                    nextEditText.requestFocus()
                }

                break

            }
        }

        if (!isHasResult) {
            isHandleScanResult = false
        }

    }

    private fun getItemDetailFromBarcode() {


        val barcode = conversion_move_in_detail_detail_edt_sku.text.toString()

        if (StringUtils.isNotBlank(barcode)) {
            presenter.getItemDetailFromBarcode(barcode)
        } else {
            ToastUtils.show(resources.getString(R.string.scan_prompt_6))
        }

    }

    private fun shelvedItem() {

        val barcode = conversion_move_in_detail_detail_edt_sku.text.toString()

        if (StringUtils.isNull(barcode)) {
            ToastUtils.show(resources.getString(R.string.scan_prompt_6))
            isHandleScanResult = false
            return
        }
        val location = conversion_move_in_detail_detail_edt_location.text.toString()
        if (StringUtils.isNull(location)) {
            ToastUtils.show(resources.getString(R.string.scann_prompt_location))
            isHandleScanResult = false
            return
        }

        presenter.shelvedItem(barcode, location)

    }

    override fun requestItemInfoFailed() {
        isHandleScanResult = false
        conversion_move_in_detail_detail_edt_sku.requestFocus()
    }

    override fun getCvId(): Int = mConversionTaskInfo?.cvId ?: 0

    override fun getCvItemId(): Int = mConversionTaskItemInfo?.cvItemId ?: 0

    override fun getContainerCode(): String = mConversionTaskInfo?.containerCode ?: ""

    //改成从上个页面传递数据
    private fun refreshTask() {

        conversion_move_in_detail_txt_scaned_container.text = mConversionTaskInfo?.containerCode
        conversion_move_in_detail_txt_num.text = mConversionTaskInfo?.taskCount.toString()

    }

    override fun requestItemInfoSuccess(conversionTaskItemInfo: ConversionTaskItemInfo) {
        isHandleScanResult = false
        mConversionTaskItemInfo = conversionTaskItemInfo

        ImageUtils.loadImageNew(conversion_move_in_detail_item_img, conversionTaskItemInfo.imageUrl, R.color.color_f2f2f2)
        conversion_move_in_detail_detail_edt_location.requestFocus()
    }

    override fun shelvedSuccess(shelvedResultData: ShelvedResultData) {

        if (shelvedResultData.finish == true) {

            ToastUtils.show(resources.getString(R.string.complet_conversion_move_in))

            finishShelvedSuccess()
            return

        }

        TipSound.getInstance().playScanSucceedSound(this)
        isHandleScanResult = false

        conversion_move_in_detail_txt_num.text = shelvedResultData.taskCount.toString()

        ImageUtils.loadImage(conversion_move_in_detail_item_img, "", R.color.color_f2f2f2)
        conversion_move_in_detail_detail_edt_location.setText("")
        conversion_move_in_detail_detail_edt_sku.setText("")
        conversion_move_in_detail_detail_edt_sku.requestFocus()

    }

    override fun finishShelvedSuccess() {
        TipSound.getInstance().playScanSucceedSound(this)
        setResult(Activity.RESULT_OK)
        finish()
    }


    override fun requestFailed(message: String?) {
        TipSound.getInstance().playScanFailedSound(this)
//        ToastUtils.show(message)
        isHandleScanResult = false
        customDialog = PromptDialogUtils.getInstance().getPromptDialog(this@ConversionMoveInDetailActivity, message
                ?: resources.getString(R.string.net_error_prompt_one))
        if (customDialog != null && isAlive) {
            customDialog?.show()
        }
    }


    override fun showLoadingView() {
        showLoadingView(conversion_move_in_detail_progress)
    }

    override fun hideLoadingView() {
        hideLoadingView(conversion_move_in_detail_progress)
    }

    override fun onDestroy() {
        super.onDestroy()
        isHandleScanResult = false
        if (customDialog != null && customDialog?.isShowing == true && isAlive) {
            customDialog?.dismiss()
            customDialog = null
        }
    }
}
