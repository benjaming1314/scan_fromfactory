package club.fromfactory.ui.tracking.contract

import club.fromfactory.baselibrary.presenter.IPresenter
import club.fromfactory.baselibrary.view.IBaseMVPView
import club.fromfactory.ui.tracking.model.MawbInfo

class MawbListContract {

    interface View : IBaseMVPView<Presenter> {

        fun refreshMawbList(mList : List<MawbInfo>?)

        fun getPageNo() :Int
        fun getPageSize() :Int
        fun showFailedError(message: String?)


    }

    interface Presenter : IPresenter<View> {
        fun getTrackingList()
    }
}