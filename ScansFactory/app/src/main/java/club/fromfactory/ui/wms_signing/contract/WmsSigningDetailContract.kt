package club.fromfactory.ui.wms_signing.contract

import club.fromfactory.baselibrary.presenter.IPresenter
import club.fromfactory.baselibrary.view.IBaseMVPView
import club.fromfactory.ui.wms_signing.model.SignPackageResult

class WmsSigningDetailContract {

    interface View : IBaseMVPView<Presenter> {

        fun requestSignAwbSucceed(signPackageResult: SignPackageResult)

        fun requestSignAwbFailed(message :String?)

        fun getType() : String
        fun getSignOrderId() : String
        fun requestCompleteTaskSuccess()
        fun requestFailed(message: String?)

    }

    interface Presenter : IPresenter<View> {
        fun signAwb(orderNum :String)
        fun completeTask()
    }
}