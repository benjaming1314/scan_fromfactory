package club.fromfactory.ui.space_transfer.contract

import club.fromfactory.baselibrary.presenter.IPresenter
import club.fromfactory.baselibrary.view.IBaseMVPView

class SpaceTransferContract {
    interface View :IBaseMVPView<Presenter> {

        fun requestFailed(message :String?)

        fun checkSourceLocationSuccess()
        fun shelfOffSourceLocationSuccess()

        fun shelfLocationSuccess()

    }
    interface Presenter :IPresenter<View> {

        fun checkSourceLocation(boxName:String)
        fun shelfOffSourceLocation(boxName:String)

        fun shelfLocation(boxName:String,toBoxName :String)

    }
}