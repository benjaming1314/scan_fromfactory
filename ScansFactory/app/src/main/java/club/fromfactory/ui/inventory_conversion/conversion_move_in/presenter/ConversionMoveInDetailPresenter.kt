package club.fromfactory.ui.inventory_conversion.conversion_move_in.presenter

import club.fromfactory.FFApplication
import club.fromfactory.R
import club.fromfactory.baselibrary.language.LanguageUtils
import club.fromfactory.baselibrary.model.BaseResponse
import club.fromfactory.baselibrary.model.EmptyResponse
import club.fromfactory.baselibrary.net.CookieHelper
import club.fromfactory.baselibrary.net.retrofit.BaseRetrofit
import club.fromfactory.baselibrary.presenter.BasePresenter
import club.fromfactory.baselibrary.rx.loadingView
import club.fromfactory.baselibrary.utils.JsonUtils
import club.fromfactory.baselibrary.utils.PreferenceStorageUtils
import club.fromfactory.baselibrary.view.RxAppCompatActivity
import club.fromfactory.constant.Constants
import club.fromfactory.http.NetUtils
import club.fromfactory.http.model.HttpJsonErrorResult
import club.fromfactory.ui.inventory_conversion.conversion_move_in.IConversionMoveInApi
import club.fromfactory.ui.inventory_conversion.conversion_move_in.contract.ConversionMoveInDetailContract
import club.fromfactory.ui.inventory_conversion.conversion_move_in.model.ShelvedResultData
import club.fromfactory.ui.inventory_conversion.conversion_move_out.model.ConversionTaskItemInfo
import com.trello.rxlifecycle2.kotlin.bindToLifecycle
import io.reactivex.observers.DefaultObserver
import retrofit2.HttpException
import java.util.*

class ConversionMoveInDetailPresenter(view: ConversionMoveInDetailContract.View) : BasePresenter<ConversionMoveInDetailContract.View>(view), ConversionMoveInDetailContract.Presenter {


    private var cvOrderBarcodeItemDetailUrl = NetUtils.APP_MAIN_URL + "wms-center/cv_item/barcode"
    private var cvOrderShelvedUrl = NetUtils.APP_MAIN_URL + "wms-center/cv_item/shelved"
    private var cvOrderFinishShelvedUrl = NetUtils.APP_MAIN_URL + "wms-center/cv_order/finish/"

    override fun getItemDetailFromBarcode(barcode: String) {


        val token = PreferenceStorageUtils.getInstance().token

        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        //设置参数，访问url获取json
        val params = HashMap<String, Any>()
        params["cvId"] = view.getCvId()
        params["barcode"] = barcode


        BaseRetrofit
                .createService(IConversionMoveInApi::class.java)
                .getItemDetailFromBarcode(cvOrderBarcodeItemDetailUrl, headerMap, params)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : DefaultObserver<BaseResponse<ConversionTaskItemInfo>>() {
                    override fun onComplete() {
                    }

                    override fun onNext(baseResponse: BaseResponse<ConversionTaskItemInfo>) {
                        val success = baseResponse.isSuccess

                        if (success) {
                            view.requestItemInfoSuccess(baseResponse.model)
                        } else {
                            view.requestItemInfoFailed()
                            view.requestFailed(baseResponse.firstErrorMessage.displayMessage)
                        }
                    }

                    override fun onError(e: Throwable) {
                        if (e is HttpException && e.code() in 400..403) {
                            val string = e.response()?.errorBody()?.string()
                            if (string != null) {
                                val parseJson = JsonUtils.parseJson<HttpJsonErrorResult>(string)
                                view.requestItemInfoFailed()
                                view.requestFailed(parseJson?.message)
                            }
                        } else {
                            view.requestItemInfoFailed()
                            view.requestFailed(FFApplication.getInstance().resources.getString(R.string.net_error))
                        }
                    }

                })

    }

    override fun shelvedItem(barcode: String, boxName: String) {


        val token = PreferenceStorageUtils.getInstance().token

        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        //设置参数，访问url获取json
        val params = HashMap<String, Any>()
        params["cvId"] = view.getCvId()
        params["cvItemId"] = view.getCvItemId()
        params["barcode"] = barcode
        params["boxName"] = boxName
        params["containerCode"] = view.getContainerCode()

        BaseRetrofit
                .createService(IConversionMoveInApi::class.java)
                .shelvedItem(cvOrderShelvedUrl, headerMap, params)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : DefaultObserver<BaseResponse<ShelvedResultData>>() {
                    override fun onComplete() {
                    }

                    override fun onNext(baseResponse: BaseResponse<ShelvedResultData>) {
                        val success = baseResponse.isSuccess

                        if (success) {
                            view.shelvedSuccess(baseResponse.model)
                        } else {
                            view.requestFailed(baseResponse.firstErrorMessage.displayMessage)
                        }
                    }

                    override fun onError(e: Throwable) {
                        if (e is HttpException && e.code() in 400..403) {
                            val string = e.response()?.errorBody()?.string()
                            if (string != null) {
                                val parseJson = JsonUtils.parseJson<HttpJsonErrorResult>(string)
                                view.requestFailed(parseJson?.message)
                            }
                        } else {
                            view.requestFailed(FFApplication.getInstance().resources.getString(R.string.net_error))
                        }
                    }

                })
    }

    override fun finishShelved() {

        val token = PreferenceStorageUtils.getInstance().token

        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)
        BaseRetrofit
                .createService(IConversionMoveInApi::class.java)
                .finishShelved(cvOrderFinishShelvedUrl + view.getCvId(), headerMap)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : DefaultObserver<BaseResponse<EmptyResponse>>() {
                    override fun onComplete() {
                    }

                    override fun onNext(baseResponse: BaseResponse<EmptyResponse>) {
                        val success = baseResponse.isSuccess

                        if (success) {
                            view.finishShelvedSuccess()
                        } else {
                            view.requestFailed(baseResponse.firstErrorMessage.displayMessage)
                        }
                    }

                    override fun onError(e: Throwable) {
                        if (e is HttpException && e.code() in 400..403) {
                            val string = e.response()?.errorBody()?.string()
                            if (string != null) {
                                val parseJson = JsonUtils.parseJson<HttpJsonErrorResult>(string)
                                view.requestFailed(parseJson?.message)
                            }
                        } else {
                            view.requestFailed(FFApplication.getInstance().resources.getString(R.string.net_error))
                        }
                    }

                })

    }
}