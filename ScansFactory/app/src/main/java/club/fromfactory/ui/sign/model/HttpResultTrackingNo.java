package club.fromfactory.ui.sign.model;

import club.fromfactory.baselibrary.model.NoProguard;

/**
 * Created by lxm on 2017/7/26.
 * "is_success": true,
 "data": {
 "signed_date": "2017-06-02T09:04:39.258",
 "qty_po": 0,
 "signed_user": "开发者",
 "signed_state": "未签收",
 "voice_type": "voice_already_scan",
 "tracking_no": 123
 }
 */

public class HttpResultTrackingNo implements NoProguard {

    private boolean is_success ;
    private String msg ;
    private TrackingsNoDetail data ;


    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean is_success() {
        return is_success;
    }

    public void setIs_success(boolean is_success) {
        this.is_success = is_success;
    }

    public TrackingsNoDetail getData() {
        return data;
    }

    public void setData(TrackingsNoDetail data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "HttpResultTrackingNo{" +
                "is_success=" + is_success +
                ", data=" + data +
                '}';
    }
}
