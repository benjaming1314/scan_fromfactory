package club.fromfactory.ui.inventory;

import android.content.Intent;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import club.fromfactory.R;
import club.fromfactory.baselibrary.router.RouterConstants;
import club.fromfactory.routerannotaions.Router;
import club.fromfactory.constant.Constants;
import club.fromfactory.ui.main.ScanBaseActivity;
import club.fromfactory.widget.CustomTitleLinearLayout;

/**
 * 盘点->库存报废
 */
@Router(RouterConstants.INVENTORY)
public class InventoryActivity extends ScanBaseActivity {

    @BindView(R.id.inventory_ctl_title)
    CustomTitleLinearLayout mCtlTitle;
    @BindView(R.id.txt_prompt)
    TextView txtPrompt;
    @BindView(R.id.inventory_scan)
    LinearLayout inventoryScan;

    @Override
    public void requestScanStart() {
//
//        if (Zlog.isDebug) {
//            requestScanResult("X01-01-02");
//        }

    }

    @Override
    public int getLayoutResId() {
        return R.layout.activity_inventory;
    }

    @Override
    public void initView() {
        super.initView();
        mCtlTitle.setListener(new CustomTitleLinearLayout.CustomTitleListener() {
            @Override
            public void clickLeft() {
                super.clickLeft();
                finish();
            }
        });
        setScanListener(inventoryScan);
    }

    @Override
    public void clickKeyCodeBack() {
        finish();
    }

    @Override
    public void requestScanResult(String result) {
        handleResult(result);
    }


    private void handleResult(String result) {

        Intent intent = new Intent(InventoryActivity.this, InventoryGoodsListActivity.class);
        intent.putExtra(Constants.BOX_NAME, result);
        startActivity(intent);

    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
