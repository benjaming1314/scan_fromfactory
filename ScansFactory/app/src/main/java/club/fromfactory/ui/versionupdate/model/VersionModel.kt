package club.fromfactory.ui.versionupdate.model

import com.google.gson.annotations.SerializedName

class VersionModel {

    companion object {

        const val UPDATE_TYPE_NO_UPDATE = 0
        const val UPDATE_TYPE_UPDATE = 1
        const val UPDATE_TYPE_FORCE_UPDATE = 2
    }

    @SerializedName("update_type")
    var updateType: Int? = UPDATE_TYPE_NO_UPDATE
    @SerializedName("note")
    var description: String? = null
    @SerializedName("package_url")
    var link_url: String? = null
}