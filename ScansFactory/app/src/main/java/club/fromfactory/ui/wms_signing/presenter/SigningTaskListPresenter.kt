package club.fromfactory.ui.wms_signing.presenter

import club.fromfactory.baselibrary.language.LanguageUtils
import club.fromfactory.baselibrary.net.CookieHelper
import club.fromfactory.baselibrary.net.retrofit.BaseRetrofit
import club.fromfactory.baselibrary.presenter.BasePresenter
import club.fromfactory.baselibrary.rx.NetObserver
import club.fromfactory.baselibrary.rx.loadingView
import club.fromfactory.baselibrary.utils.PreferenceStorageUtils
import club.fromfactory.baselibrary.view.RxAppCompatActivity
import club.fromfactory.constant.Constants
import club.fromfactory.http.NetUtils
import club.fromfactory.ui.wms_signing.IWmsSigningApi
import club.fromfactory.ui.wms_signing.contract.SigningTaskListContract
import club.fromfactory.ui.wms_signing.model.OrderDetailInfo
import com.trello.rxlifecycle2.kotlin.bindToLifecycle
import java.util.HashMap

class SigningTaskListPresenter(view : SigningTaskListContract.View) :BasePresenter<SigningTaskListContract.View>(view),SigningTaskListContract.Presenter {

    private var SIGNED_ORDER_LIST_URL =  NetUtils.APP_MAIN_URL + "wms-center/pda/sign-orders"

    private var CREATE_SIGN_ORDER_URL = NetUtils.APP_MAIN_URL + "wms-center/create_sign_order"

    override fun getSignedOrderList(type: String) {
        val token = PreferenceStorageUtils.getInstance().token

        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        val params = HashMap<String, Any>()
        params["type"] = type

        BaseRetrofit
                .createService(IWmsSigningApi::class.java)
                .getSignedOrderList(SIGNED_ORDER_LIST_URL,headerMap,params)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : NetObserver<List<OrderDetailInfo>>() {
                    override fun onSuccess(t: List<OrderDetailInfo>?) {
                        if (t != null) {
                            view.requestSignOrderListSucceed(t)
                        }else {
                            view.requestFailed("no data")
                        }
                    }

                    override fun onFailure(message: String) {

                        view.requestFailed(message)
                    }
                })

    }

    override fun createSignOrder(type: String) {
        val token = PreferenceStorageUtils.getInstance().token

        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        val params = HashMap<String, Any>()
        params["type"] = type

        BaseRetrofit
                .createService(IWmsSigningApi::class.java)
                .createSignOrder(CREATE_SIGN_ORDER_URL,headerMap,params)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : NetObserver<OrderDetailInfo>() {
                    override fun onSuccess(t: OrderDetailInfo?) {
                        if (t != null) {
                            view.requestCreateSignOrderSucceed(t)
                        }else {
                            view.requestFailed("no data")
                        }
                    }

                    override fun onFailure(message: String) {
                        view.requestFailed(message)
                    }
                })

    }

}