package club.fromfactory.ui.movein

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import club.fromfactory.R
import club.fromfactory.baselibrary.router.RouterConstants
import club.fromfactory.baselibrary.utils.StringUtils
import club.fromfactory.baselibrary.utils.ToastUtils
import club.fromfactory.baselibrary.widget.CustomDialog
import club.fromfactory.fresco.view.FrescoImageView
import club.fromfactory.routerannotaions.Router
import club.fromfactory.ui.common.model.EditTextModel
import club.fromfactory.ui.common.model.Goods
import club.fromfactory.ui.main.ScanBaseMVPActivity
import club.fromfactory.ui.movein.contract.MoveInContract
import club.fromfactory.ui.movein.presenter.MoveInConfirmPresenter
import club.fromfactory.utils.*
import club.fromfactory.widget.CustomTitleLinearLayout
import kotlinx.android.synthetic.main.activity_move_in_confirm_new.*
import kotlinx.android.synthetic.main.goods_item.*

@Router(RouterConstants.MOVE_IN_CONFIRM)
class MoveInConfirmActivity : ScanBaseMVPActivity<MoveInContract.Presenter>(), MoveInContract.View {
    private var isHandleScanResult = false


    private var customDialog: CustomDialog? = null


    private var mEdtMaps = HashMap<EditText, EditTextModel>()

    //    var click = 0
    override fun requestScanStart() {

//        if (Zlog.isDebug) {
//            if (click == 0) {
//                requestScanResult("X06-10-12")
//            }else {
//                requestScanResult("0002530213")
//            }
//            click++
//        }

    }

    override fun getLayoutResId(): Int = R.layout.activity_move_in_confirm_new

    override fun initData() {
        super.initData()
    }

    override fun initView() {
        super.initView()

        move_in_new_new_navigation.visibility = View.GONE
        move_in_new_sku.visibility = View.GONE

        move_in_new_ctl_title.setListener(object : CustomTitleLinearLayout.CustomTitleListener() {
            override fun clickLeft() {
                super.clickLeft()
                finish()
            }

        })
        setScanListener(move_in_new_scan_btn)

        move_in_new_edt_location.setOnFocusChangeListener { v, hasFocus ->
            mEdtMaps[move_in_new_edt_location]?.isFoucus = hasFocus
        }
        move_in_new_edt_sku.setOnFocusChangeListener { v, hasFocus ->
            mEdtMaps[move_in_new_edt_sku]?.isFoucus = hasFocus
        }
        EditTextUtils.addEditTextListener(move_in_new_edt_location)
        EditTextUtils.addEditTextListener(move_in_new_edt_sku)
        move_in_new_edt_location.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                move_in_new_txt_location.text = s?.toString() ?: ""
            }

            override fun afterTextChanged(s: Editable?) {
            }
        })
        move_in_new_edt_sku.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                move_in_new_txt_sku.text = s?.toString() ?: ""
            }

            override fun afterTextChanged(s: Editable?) {
            }
        })

        move_in_new_new_ok.setOnClickListener {
            operateShelve()
        }

        mEdtMaps[move_in_new_edt_sku] = EditTextModel(move_in_new_edt_sku, move_in_new_edt_location, true)
        mEdtMaps[move_in_new_edt_location] = EditTextModel(move_in_new_edt_location, null, false)
    }

    private fun operateShelve() {
        val boxName = move_in_new_edt_location.text.toString()
        val sku = move_in_new_edt_sku.text.toString()

        when {
            StringUtils.isNull(boxName) -> {
                isHandleScanResult = false
                ToastUtils.show(resources.getString(R.string.scan_prompt_1))
                move_in_new_edt_location.requestFocus()
            }
            StringUtils.isNull(sku) -> {
                isHandleScanResult = false
                ToastUtils.show(resources.getString(R.string.scan_prompt_2))

                move_in_new_edt_sku.requestFocus()
            }
            else -> presenter.operateShelve(boxName, sku)
        }


    }

    private fun checkLocationAndSku() {

        val location = move_in_new_edt_location.text.toString()
        val sku = move_in_new_edt_sku.text.toString()

        if (StringUtils.isNull(location) && StringUtils.isNull(sku)) {
            move_in_new_txt_location.text = ""
            move_in_new_txt_sku.text = ""

        }

    }


    override fun requestScanResult(result: String?) {
        if (customDialog != null && customDialog?.isShowing == true) {
            return
        }
        if (isHandleScanResult) {
            return
        } else {
            isHandleScanResult = true
        }

        checkLocationAndSku()

        var isHasResult = false
//        mEdtMaps.keys.filter {
//            val editTextModel = mEdtMaps[it]
//            editTextModel?.isFoucus ?: false
//        }.forEach {
//
//            val editTextModel = mEdtMaps[it]
//            Zlog.ii("lxm sda5:" + mEdtMaps[it].toString())
//            if (editTextModel?.isFoucus == true) {
//
//                it.setText(result ?: "")
//                val nextEditText = editTextModel.nextEditText
//                if (nextEditText == null) {
//                    isHasResult = true
//                    operateShelve()
//                } else {
//                    nextEditText.requestFocus()
//                }
//                return@forEach
//            }
//        }


        val iterator = mEdtMaps.keys.iterator()
        while (iterator.hasNext()) {
            val it = iterator.next()

            val editTextModel = mEdtMaps[it]
            Zlog.ii("lxm sda5:" + mEdtMaps[it].toString())
            if (editTextModel?.isFoucus == true) {

                it.setText(result ?: "")
                val nextEditText = editTextModel.nextEditText
                if (nextEditText == null) {
                    isHasResult = true
                    operateShelve()
                } else {
                    nextEditText.requestFocus()
                    nextEditText.setText("")
                }

                break
            }
        }

        if (!isHasResult) {
            isHandleScanResult = false
        }
    }


    override fun refreshData(goods: Goods) {

        isHandleScanResult = false
        Zlog.ii("lxm ss refreshData:$goods")
        goods.scanState = Goods.SCAN_STATE_SUCCEED
        TipSound.getInstance().playScanSucceedSound(this@MoveInConfirmActivity)
        refresh(goods)
    }


    override fun createPresenter(): MoveInContract.Presenter {

        return MoveInConfirmPresenter(this)
    }

    private fun loadImage(mImageView: FrescoImageView, imageUrl: String?) {
        ImageUtils.loadImageNew(mImageView, imageUrl, R.mipmap.logo)

    }

    private fun refresh(goods: Goods?) {

        if (goods == null) {
            return
        }

        move_in_new_new_navigation.visibility = View.VISIBLE
        move_in_new_sku.visibility = View.VISIBLE

        loadImage(goods_item_img, goods.sku_image_url)
        val barcode = goods.getBarcodeShow()
        val scanState = goods.scanState

        goods_item_txt_sku.text = barcode
        goods_item_state_error.visibility = if (scanState == Goods.SCAN_STATE_FAILED) View.VISIBLE else View.GONE
        goods_item_state_right.visibility = if (scanState == Goods.SCAN_STATE_SUCCEED) View.VISIBLE else View.GONE


        //重置输入框
        move_in_new_edt_location.setText("")
        move_in_new_edt_sku.setText("")
        move_in_new_edt_sku.requestFocus()

    }

    override fun showLoadingView() {
        showLoadingView(move_in_new_progress)
    }

    override fun hideLoadingView() {
        hideLoadingView(move_in_new_progress)
    }

    override fun showFailedError(message: String?) {

        isHandleScanResult = false
        promptError(message)

        TipSound.getInstance().playScanFailedSound(this@MoveInConfirmActivity)
        val goods = Goods()
        goods.scanState = Goods.SCAN_STATE_FAILED
        goods.barcode = move_in_new_edt_sku.text.toString()
        refresh(goods)
    }

    private fun promptError(message: String?) {

        customDialog = PromptDialogUtils.getInstance().getPromptDialog(this@MoveInConfirmActivity, message
                ?: resources.getString(R.string.net_error_prompt_one))
        if (customDialog != null && isAlive) {
            customDialog?.show()
        }
    }

    override fun clickKeyCodeBack() {
        finish()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (customDialog != null && customDialog?.isShowing == true && isAlive) {
            customDialog?.dismiss()
            customDialog = null
        }
    }

}