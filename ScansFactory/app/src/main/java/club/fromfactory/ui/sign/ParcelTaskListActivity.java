package club.fromfactory.ui.sign;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import club.fromfactory.R;
import club.fromfactory.baselibrary.router.RouterConstants;
import club.fromfactory.baselibrary.utils.ToastUtils;
import club.fromfactory.baselibrary.view.BaseMVPActivity;
import club.fromfactory.baselibrary.widget.CustomDialog;
import club.fromfactory.routerannotaions.Router;
import club.fromfactory.ui.sign.adapter.TaskListRecyclerAdapter;
import club.fromfactory.ui.sign.contract.ParcelTaskContract;
import club.fromfactory.ui.sign.model.SignedTask;
import club.fromfactory.ui.sign.presenter.ParcelTaskListPresenter;
import club.fromfactory.utils.PromptDialogUtils;
import club.fromfactory.utils.Zlog;
import club.fromfactory.widget.CustomTitleLinearLayout;

@Router(RouterConstants.PARCEL_TASK_LIST)
public class ParcelTaskListActivity extends BaseMVPActivity<ParcelTaskContract.Presenter> implements ParcelTaskContract.View {

    private final int REQUEST_CODE_TAST_DETAIL = 100;
    @BindView(R.id.txt_new_task)
    TextView txtNewTask;
    @BindView(R.id.rcl_tast_list)
    RecyclerView rclTastList;
    @BindView(R.id.txt_task_num)
    TextView txtTaskNum;
    @BindView(R.id.parcel_task_list_ctl_title)
    CustomTitleLinearLayout mCtlTitle;
    @BindView(R.id.parcel_task_list_progress)
    View mProgress;


    private TaskListRecyclerAdapter mTaskListRecyclerAdapter;
    private List<SignedTask> signedTaskList;

    public static void launchActivity(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, ParcelTaskListActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        fetchData();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public int getLayoutResId() {
        return R.layout.activity_parcel_task_list;
    }

    @Override
    public void clickKeyCodeBack() {
        finish();
    }

    @Override
    public void initData() {
        super.initData();
    }

    @Override
    public void initView() {
        super.initView();

        mCtlTitle.setListener(new CustomTitleLinearLayout.CustomTitleListener() {
            @Override
            public void clickLeft() {
                super.clickLeft();
                finish();
            }
        });
        LinearLayoutManager mainFilterLLM = new LinearLayoutManager(this);
        mainFilterLLM.setOrientation(LinearLayoutManager.VERTICAL);
        rclTastList.setLayoutManager(mainFilterLLM);
        signedTaskList = new ArrayList<>();
        mTaskListRecyclerAdapter = new TaskListRecyclerAdapter(ParcelTaskListActivity.this, signedTaskList);
        rclTastList.setAdapter(mTaskListRecyclerAdapter);
        mTaskListRecyclerAdapter.setOnItemClickLitener(new TaskListRecyclerAdapter.OnItemClickLitener() {
            @Override
            public void onItemClick(int position) {
                SignedTask signedTask = signedTaskList.get(position);
                initTaskDetail(signedTask.getId(), signedTask.getTask_name());
            }
        });

        txtTaskNum.setText(
                getResources().getString(R.string.old_task_list_sum_propt1, String.valueOf(0)));
    }

    @Override
    public void fetchData() {
        super.fetchData();
        presenter.getSignTaskList();
    }


    @OnClick({R.id.txt_new_task})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txt_new_task:
                if (signedTaskList.size() >= 20) {
                    String prompt = getResources().getString(R.string.sign_scan_prompt_one);
                    promptError(prompt);
                } else {
                    presenter.createSignedTask();
                }
                break;
        }
    }

    @Override
    public void httpResult(SignedTask signedTask) {
        initTaskDetail(signedTask.getId(), signedTask.getTask_name());
    }

    @Override
    public void showLoadingView() {

        if (mProgress.getVisibility() == View.GONE) {
            mProgress.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideLoadingView() {
        if (mProgress.getVisibility() == View.VISIBLE) {
            mProgress.setVisibility(View.GONE);
        }
    }

    @Override
    public void httpResult(@NotNull List<? extends SignedTask> signedTask) {
        signedTaskList.clear();
        signedTaskList.addAll(signedTask);
        mTaskListRecyclerAdapter.notifyDataSetChanged();

        txtTaskNum.setText(
                getResources().getString(R.string.old_task_list_sum_propt1, String.valueOf(signedTaskList.size())));


    }

    @Override
    public void showFailedError(String message) {
        if (message != null) {
            ToastUtils.show(message);
        } else {
            ToastUtils.show(getResources().getString(R.string.net_error_prompt_one));
        }

    }

    private CustomDialog customDialog;

    private void promptError(String message) {

        customDialog = PromptDialogUtils.getInstance().getPromptDialog(ParcelTaskListActivity.this, message);
        if (customDialog != null && isAlive()) {
            customDialog.show();
        }
    }


    private void initTaskDetail(int taskId, String taskName) {
        TaskDetailActivity.launchActivity(ParcelTaskListActivity.this, taskId, taskName, REQUEST_CODE_TAST_DETAIL);
    }

    @Override
    public void finish() {
        super.finish();

        try {
            if (customDialog != null && customDialog.isShowing()) {
                customDialog.dismiss();
            }
        } catch (IllegalArgumentException e) {
            // Handle or log or ignore
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            customDialog = null;
        }
    }

    @NonNull
    @Override
    public ParcelTaskContract.Presenter createPresenter() {
        return new ParcelTaskListPresenter(this);
    }
}
