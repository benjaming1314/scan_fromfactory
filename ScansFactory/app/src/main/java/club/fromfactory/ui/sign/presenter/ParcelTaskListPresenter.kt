package club.fromfactory.ui.sign.presenter

import club.fromfactory.FFApplication
import club.fromfactory.R
import club.fromfactory.baselibrary.language.LanguageUtils
import club.fromfactory.baselibrary.net.CookieHelper
import club.fromfactory.baselibrary.net.retrofit.BaseRetrofit
import club.fromfactory.baselibrary.presenter.BasePresenter
import club.fromfactory.baselibrary.rx.loadingView
import club.fromfactory.baselibrary.utils.JsonUtils
import club.fromfactory.baselibrary.utils.PreferenceStorageUtils
import club.fromfactory.baselibrary.view.RxAppCompatActivity
import club.fromfactory.constant.Constants
import club.fromfactory.http.NetUtils
import club.fromfactory.http.model.HttpJsonErrorResult
import club.fromfactory.ui.sign.ISigningApi
import club.fromfactory.ui.sign.contract.ParcelTaskContract
import club.fromfactory.ui.sign.model.CreateNewTaskModel
import club.fromfactory.ui.sign.model.SignedTaskModel
import com.trello.rxlifecycle2.kotlin.bindToLifecycle
import io.reactivex.observers.DefaultObserver
import retrofit2.HttpException
import java.util.*

class ParcelTaskListPresenter(view: ParcelTaskContract.View) : BasePresenter<ParcelTaskContract.View>(view), ParcelTaskContract.Presenter {
    /**
     * 创建任务
     */
    private var createSignedTaskUrl = NetUtils.APP_MAIN_URL_PROCUREMENT + "purchase_order/package/signed_task/create"
    /**
     * 获取未完成的任务清单
     */
    private var getSignedTaskList = NetUtils.APP_MAIN_URL_PROCUREMENT + "purchase_order/package/signed_task/list/?offset=0&limit=20&no_end_time=1"


    override fun createSignedTask() {

        val token = PreferenceStorageUtils.getInstance().token

        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        BaseRetrofit
                .createService(ISigningApi::class.java)
                .createSignedTask(createSignedTaskUrl, headerMap)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : DefaultObserver<CreateNewTaskModel>() {
                    override fun onComplete() {
                    }

                    override fun onNext(createNewTaskModel: CreateNewTaskModel) {
                        if (createNewTaskModel.is_success) {
                            val signedTask = createNewTaskModel.data
                            if (signedTask != null) {
                                view.httpResult(signedTask)
                            } else {
                                view.showFailedError("failed")
                            }
                        } else {
                            view.showFailedError(createNewTaskModel.getMsg())
                        }
                    }

                    override fun onError(e: Throwable) {
                        if (e is HttpException && e.code() in 400..403) {
                            val string = e.response()?.errorBody()?.string()
                            if (string != null) {
                                val parseJson = JsonUtils.parseJson<HttpJsonErrorResult>(string)
                                view.showFailedError(parseJson?.message)
                            }
                        } else {
                            view.showFailedError(FFApplication.getInstance().resources.getString(R.string.net_error))
                        }
                    }

                })
    }

    override fun getSignTaskList() {

        val token = PreferenceStorageUtils.getInstance().token

        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        BaseRetrofit
                .createService(ISigningApi::class.java)
                .getSignTaskList(getSignedTaskList, headerMap)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : DefaultObserver<SignedTaskModel>() {
                    override fun onComplete() {
                    }

                    override fun onNext(signedTaskModel: SignedTaskModel) {
                        val signedTaskList = signedTaskModel.results
                        if (signedTaskList != null) {
                            view.httpResult(signedTaskList)
                        } else {
                            view.showFailedError("failed")
                        }
                    }

                    override fun onError(e: Throwable) {
                        if (e is HttpException && e.code() in 400..403) {
                            val string = e.response()?.errorBody()?.string()
                            if (string != null) {
                                val parseJson = JsonUtils.parseJson<HttpJsonErrorResult>(string)
                                view.showFailedError(parseJson?.message)
                            }
                        } else {
                            view.showFailedError(FFApplication.getInstance().resources.getString(R.string.net_error))
                        }
                    }

                })
    }
}