package club.fromfactory.ui.sign.model;

import club.fromfactory.baselibrary.model.NoProguard;

/**
 * Created by lxm on 2017/7/26.
 *  "id": 68,
 "task_name": "12317072105383982",
 "start_time": "2017-07-21 13:38:39",
 "end_time": null,
 "package_num": 0,
 "create_user": "开发者"
 */

public class SignedTask implements NoProguard {

    private int id ;
    private String task_name ;
    private String start_time ;
    private String end_time ;
    private int package_num ;
    private String create_user ;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTask_name() {
        return task_name;
    }

    public void setTask_name(String task_name) {
        this.task_name = task_name;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public int getPackage_num() {
        return package_num;
    }

    public void setPackage_num(int package_num) {
        this.package_num = package_num;
    }

    public String getCreate_user() {
        return create_user;
    }

    public void setCreate_user(String create_user) {
        this.create_user = create_user;
    }

    @Override
    public String toString() {
        return "SignedTask{" +
                "id=" + id +
                ", task_name='" + task_name + '\'' +
                ", start_time='" + start_time + '\'' +
                ", end_time='" + end_time + '\'' +
                ", package_num='" + package_num + '\'' +
                ", create_user='" + create_user + '\'' +
                '}';
    }
}
