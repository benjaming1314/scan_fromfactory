package club.fromfactory.ui.viewphotos

/**
 * 查看图片的协议
 *
 * @author nichenjian
 * @date 2018/9/4
 */
interface ViewPhotosContract {
    interface View {
        /**
         * 更新图片的状态
         *
         * @param position 位置
         * @param downloaded 是否已经下载
         */
        fun updateImageStatus(position: Int, downloaded: Boolean)
    }
}
