package club.fromfactory.ui.inventory.adapter

import club.fromfactory.baselibrary.view.recyclerview.BaseMultiTypeAdapter
import club.fromfactory.ui.common.viewholder.GoodsForAlterCreator
import club.fromfactory.ui.common.viewholder.GoodListTitleCreator

class InventoryGoodsListAdapter : BaseMultiTypeAdapter(){

    companion object {
        const val TYPE_TITLE = 1
    }

    init {
        manager.addCreator(GoodListTitleCreator())
                .addCreator(GoodsForAlterCreator())
    }


}