//package club.fromfactory.ui.sign.biz;
//
//import com.google.gson.reflect.TypeToken;
//
//import java.util.HashMap;
//import java.util.Map;
//
//import club.fromfactory.FFApplication;
//import club.fromfactory.constant.Constants;
//import club.fromfactory.http.ClubHttpPost;
//import club.fromfactory.http.HttpRequestResultListner;
//import club.fromfactory.http.NetUtils;
//import club.fromfactory.http.RequestTypeConstant;
//import club.fromfactory.ui.sign.model.CreateNewTaskModel;
//import club.fromfactory.ui.sign.model.SignedTaskModel;
//
///**
// * Created by lxm on 2017/7/25.
// */
//
//public class ParceTaskListBiz implements IParceTaskListBiz {
//    /**
//     * 创建任务
//     */
//    public String CLUB_CREATE_SIGNED_TASK = NetUtils.APP_MAIN_URL_PROCUREMENT + "purchase_order/package/signed_task/create";
//    /**
//     * 获取未完成的任务清单
//     */
//    public String CLUB_GET_SIGNED_TASK_LIST = NetUtils.APP_MAIN_URL_PROCUREMENT + "purchase_order/package/signed_task/list/?offset=0&limit=20&no_end_time=1";
//
//    @Override
//    public void createSignedTask(final HttpRequestResultListner httpRequestResultListner) {
//
//        FFApplication.getFixThreadExecutor().execute(new Runnable() {
//            @Override
//            public void run() {
//                TypeToken<CreateNewTaskModel> typeToken = new TypeToken<CreateNewTaskModel>() {
//                };
//                //设置参数，访问url获取json
//                Map<String, String> mapHeader = new HashMap<String, String>();
//                mapHeader.put("Cookie", "warehouse=" + Constants.SERVER_LOCATION);
//                ClubHttpPost.getInstance().doGetHttp(
//                        CLUB_CREATE_SIGNED_TASK, mapHeader, RequestTypeConstant.RETURN_INITJSON_DATA, typeToken, httpRequestResultListner);
//
//
//            }
//        });
//    }
//
//    @Override
//    public void getSignTaskList(final HttpRequestResultListner httpRequestResultListner) {
//
//        FFApplication.getFixThreadExecutor().execute(new Runnable() {
//            @Override
//            public void run() {
//                TypeToken<SignedTaskModel> typeToken = new TypeToken<SignedTaskModel>() {
//                };
//                //设置参数，访问url获取json
//                Map<String, String> mapHeader = new HashMap<String, String>();
//                mapHeader.put("Cookie", "warehouse=" + Constants.SERVER_LOCATION);
//                ClubHttpPost.getInstance().doGetHttp(
//                        CLUB_GET_SIGNED_TASK_LIST, mapHeader, RequestTypeConstant.RETURN_INITJSON_DATA, typeToken, httpRequestResultListner);
//
//
//            }
//        });
//    }
//
//}
