package club.fromfactory.ui.main

import club.fromfactory.FFApplication
import club.fromfactory.R
import club.fromfactory.baselibrary.router.RouterUrlProvider
import club.fromfactory.pojo.QueryGoodsState
import club.fromfactory.ui.main.model.*
import club.fromfactory.ui.wms_signing.SigningTypeListActivity.Companion.TYPE_SIGNING_TYPE_DTO
import club.fromfactory.ui.wms_signing.SigningTypeListActivity.Companion.TYPE_SIGNING_TYPE_PURCHASE
import club.fromfactory.ui.wms_signing.SigningTypeListActivity.Companion.TYPE_SIGNING_TYPE_RTO
import club.fromfactory.ui.wms_signing.SigningTypeListActivity.Companion.TYPE_SIGNING_TYPE_TRANSFER
import java.util.*

var mPermissionResponseData: PermissionResponseData? = null

class PermissionUtils private constructor() {

    companion object {
        val instance = PermissionUtils()
    }

    fun getSigningTypeModelFromPermission(): List<PermissionModel> {

        if (mPermissionResponseData == null) {
            return ArrayList()
        }

        val perms = mPermissionResponseData?.perms
        val permsList = Arrays.asList<String>(*perms)
        val permissionModelList = ArrayList<PermissionModel>()

        for (permissionModel in getSigningTypeModel()) {

            if (permsList.contains(permissionModel.permissionName)) {
                permissionModelList.add(permissionModel)
            }

        }
        return permissionModelList
    }

    fun getInventoryConversionModelFromPermission(): List<PermissionModel> {

        if (mPermissionResponseData == null) {
            return ArrayList()
        }

        val perms = mPermissionResponseData?.perms
        val permsList = Arrays.asList<String>(*perms)
        val permissionModelList = ArrayList<PermissionModel>()

        for (permissionModel in getInventoryConversionModel()) {

            if (permsList.contains(permissionModel.permissionName)) {
                permissionModelList.add(permissionModel)
            }

        }
        return permissionModelList
    }

    fun getGoodsStatusModelFromPermission(queryGoodsStatesListSort: List<QueryGoodsState>): List<QueryGoodsState> {

        val newQueryGoodsStatesListSort = ArrayList<QueryGoodsState>()

        val perms = mPermissionResponseData?.perms
        val permsList = Arrays.asList<String>(*perms)
        val permissionModelList = ArrayList<PermissionModel>()

        for (permissionModel in getAllGoodsStatusModel()) {

            if (permsList.contains(permissionModel.permissionName)) {
                permissionModelList.add(permissionModel)
            }
        }

        queryGoodsStatesListSort.forEach {

            val name = it.name
            val nameValue = it.nameValue


            for (i in 0 until permissionModelList.size) {

                val permissionModel = permissionModelList[i]

                if (name == permissionModel.modelKey || nameValue == permissionModel.modelKey) {
                    newQueryGoodsStatesListSort.add(it)
                }
            }

        }
        return newQueryGoodsStatesListSort
    }

    fun getMainModelFromPermission(): List<PermissionModel> {
        if (mPermissionResponseData == null) {
            return ArrayList()
        }

        val perms = mPermissionResponseData?.perms
        val permsList = Arrays.asList<String>(*perms)
        val permissionModelList = ArrayList<PermissionModel>()

        for (permissionModel in getAllMainModel()) {

            if (permsList.contains(permissionModel.permissionName)) {
                permissionModelList.add(permissionModel)
            }

        }
        return permissionModelList
    }

    //pda.movein pda.moveout pda.signed pda.check  pda.mulmovein
    private fun getAllMainModel(): List<PermissionModel> {
        val permissionModelList = ArrayList<PermissionModel>()

        permissionModelList.add(getPermissionModel(RouterUrlProvider.getMoveInConfirm()
                , FFApplication.getInstance().resources.getString(R.string.move_in)
                , PermissionName.MOVE_IN.permissionName))

        permissionModelList.add(getPermissionModel(RouterUrlProvider.getMoveOut()
                , FFApplication.getInstance().resources.getString(R.string.move_out)
                , PermissionName.MOVE_OUT.permissionName))

        permissionModelList.add(getPermissionModel(RouterUrlProvider.getInventoryUrl()
                , FFApplication.getInstance().resources.getString(R.string.inventory)
                , PermissionName.CHECK.permissionName))

        permissionModelList.add(getPermissionModel(RouterUrlProvider.getParcelTaskList()
                , FFApplication.getInstance().resources.getString(R.string.sign_parcel)
                , PermissionName.SIGNED.permissionName))

//        permissionModel = PermissionModel()
//        permissionModel.modelType = PermissionModel.MODEL_TYPE_MUL_MOVE_IN
//        permissionModel.modelName = FFApplication.getInstance().resources.getString(R.string.mul_move_in)
//        PermissionName = PermissionName.MUL_MOVE_IN.permissionName
//        permissionModelList.add(permissionModel)

        permissionModelList.add(getPermissionModel(RouterUrlProvider.getMoveLocationSelectLocationUrl()
                , FFApplication.getInstance().resources.getString(R.string.move_location)
                , PermissionName.MOVE_LOCATION.permissionName))

        //新增tracking
        permissionModelList.add(getPermissionModel(RouterUrlProvider.getMawbListUrl()
                , FFApplication.getInstance().resources.getString(R.string.processing_tracking)
                , PermissionName.PROCESSING_TRACKING.permissionName))

        //新增库存转换
        permissionModelList.add(getPermissionModel(RouterUrlProvider.getInventoryConversionUrl()
                , FFApplication.getInstance().resources.getString(R.string.inventory_conversion)
                , PermissionName.INVENTORY_CONVERSION.permissionName))


        permissionModelList.add(getPermissionModel(RouterUrlProvider.getWmsSigningTypeListUrl()
                , FFApplication.getInstance().resources.getString(R.string.signing_type_list)
                , PermissionName.WMS_SIGNING.permissionName))

        permissionModelList.add(getPermissionModel(RouterUrlProvider.getInventoryInquire()
                , FFApplication.getInstance().resources.getString(R.string.inventory_inquire)
                , PermissionName.INVENTORY_INQUIRE.permissionName))

        permissionModelList.add(getPermissionModel(RouterUrlProvider.getHawbBindingUrl()
                , FFApplication.getInstance().resources.getString(R.string.hawb_binding)
                , PermissionName.HAWB_BINDING.permissionName))

        permissionModelList.add(getPermissionModel(RouterUrlProvider.getContainerShelf()
                , FFApplication.getInstance().resources.getString(R.string.container_shelf)
                , PermissionName.COMTAINER_SHELF.permissionName))

        permissionModelList.add(getPermissionModel(RouterUrlProvider.getSpaceTransefer()
                , FFApplication.getInstance().resources.getString(R.string.space_transfer)
                , PermissionName.SPACE_TRANSFER.permissionName))
        permissionModelList.add(getPermissionModel(RouterUrlProvider.getSparePartsMoveOutUrl()
                , FFApplication.getInstance().resources.getString(R.string.spare_parts)
                , PermissionName.SPARE_PARTS.permissionName))

//        permissionModelList.add(getPermissionModel(RouterUrlProvider.getRFInventoryList()
//                , FFApplication.getInstance().resources.getString(R.string.cycle_inventory)
//                , PermissionName.RF_INVENTORY.permissionName))

        return permissionModelList
    }

    private fun getAllGoodsStatusModel(): List<PermissionModel> {
        val permissionModelList = ArrayList<PermissionModel>()

        var permissionModel = PermissionModel()
        permissionModel.modelName = FFApplication.getInstance().resources.getString(R.string.pending_for_putaway)
        permissionModel.modelKey = "IDLE_TO_SHELVE"
        permissionModel.permissionName = PermissionName.CHECK_PENDING_FOR_PUTAWAY.permissionName
        permissionModelList.add(permissionModel)

        permissionModel = PermissionModel()
        permissionModel.modelName = FFApplication.getInstance().resources.getString(R.string.shelved)
        permissionModel.modelKey = "IDLE_SHELVED"
        permissionModel.permissionName = PermissionName.CHECK_SHELVED.permissionName
        permissionModelList.add(permissionModel)

        permissionModel = PermissionModel()
        permissionModel.modelName = FFApplication.getInstance().resources.getString(R.string.pending_fo_moveout)
        permissionModel.modelKey = "IDLE_TO_OFF_SHELF"
        permissionModel.permissionName = PermissionName.CHECK_PENDING_FO_MOVEOUT.permissionName
        permissionModelList.add(permissionModel)

        permissionModel = PermissionModel()
        permissionModel.modelName = FFApplication.getInstance().resources.getString(R.string.no_box)
        permissionModel.modelKey = "IDLE_UNALLOCATED"
        permissionModel.permissionName = PermissionName.CHECK_NO_BOX.permissionName
        permissionModelList.add(permissionModel)

        permissionModel = PermissionModel()
        permissionModel.modelName = FFApplication.getInstance().resources.getString(R.string.trash_inventory)
        permissionModel.modelKey = "IDLE_TRASH"
        permissionModel.permissionName = PermissionName.CHECK_TRASH_INVENTORY.permissionName
        permissionModelList.add(permissionModel)

        return permissionModelList
    }

    private fun getInventoryConversionModel(): List<PermissionModel> {
        val permissionModelList = ArrayList<PermissionModel>()

        permissionModelList.add(getPermissionModel(RouterUrlProvider.getConversionTaskUrl()
                , FFApplication.getInstance().resources.getString(R.string.inventory_conversion_move_out)
                , PermissionName.INVENTORY_CONVERSION_MOVE_OUT.permissionName))

        permissionModelList.add(getPermissionModel(RouterUrlProvider.getConversionMoveInContainerUrl()
                , FFApplication.getInstance().resources.getString(R.string.inventory_conversion_move_in)
                , PermissionName.INVENTORY_CONVERSION_MOVE_IN.permissionName))

        return permissionModelList
    }

    private fun getSigningTypeModel(): List<PermissionModel> {
        val permissionModelList = ArrayList<PermissionModel>()

        var modelName = FFApplication.getInstance().resources.getString(R.string.signing_type_rto)
        permissionModelList.add(getPermissionModel(RouterUrlProvider.getWmsSigningTaskListUrl(modelName, TYPE_SIGNING_TYPE_RTO)
                , modelName
                , PermissionName.WMS_SIGNING_RTO.permissionName))

        modelName = FFApplication.getInstance().resources.getString(R.string.signing_type_dto)
        permissionModelList.add(getPermissionModel(RouterUrlProvider.getWmsSigningTaskListUrl(modelName, TYPE_SIGNING_TYPE_DTO)
                , modelName
                , PermissionName.WMS_SIGNING_DTO.permissionName))

        modelName = FFApplication.getInstance().resources.getString(R.string.signing_type_purchase)
        permissionModelList.add(getPermissionModel(RouterUrlProvider.getWmsSigningTaskListUrl(modelName, TYPE_SIGNING_TYPE_PURCHASE)
                , modelName
                , PermissionName.WMS_SIGNING_PURCHASE.permissionName))

        modelName = FFApplication.getInstance().resources.getString(R.string.signing_type_transfer)
        permissionModelList.add(getPermissionModel(RouterUrlProvider.getWmsSigningTaskListUrl(modelName, TYPE_SIGNING_TYPE_TRANSFER)
                , modelName
                , PermissionName.WMS_SIGNING_TRANSFER.permissionName))

        return permissionModelList
    }

    private fun getPermissionModel(routerUrl: String, modelName: String, permissionName: String): PermissionModel {
        var permissionModel = PermissionModel()
        permissionModel.routerUrl = routerUrl
        permissionModel.modelName = modelName
        permissionModel.permissionName = permissionName

        return permissionModel
    }
    private fun getPermissionModel(modelType: Int, modelName: String, permissionName: String): PermissionModel {
        var permissionModel = PermissionModel()
        permissionModel.modelType = modelType
        permissionModel.modelName = modelName
        permissionModel.permissionName = permissionName

        return permissionModel
    }
    private fun getPermissionModel(modelName: String, permissionName: String): PermissionModel {
        var permissionModel = PermissionModel()
        permissionModel.modelName = modelName
        permissionModel.permissionName = permissionName

        return permissionModel
    }

}