package club.fromfactory.ui.setting.adapter

import android.view.ViewGroup
import club.fromfactory.app_config.model.WareHouseInfor
import club.fromfactory.baselibrary.view.recyclerview.BaseRecyclerAdapter
import club.fromfactory.baselibrary.view.recyclerview.BaseViewHolder
import club.fromfactory.ui.setting.viewholder.WareHouseViewHolder

class WareHouseAdapter :BaseRecyclerAdapter<WareHouseInfor>(){
    override fun onCreateBaseViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<WareHouseInfor> = WareHouseViewHolder(parent)


}