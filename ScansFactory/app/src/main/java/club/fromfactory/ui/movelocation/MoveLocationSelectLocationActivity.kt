package club.fromfactory.ui.movelocation

import android.app.Activity
import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import club.fromfactory.R
import club.fromfactory.baselibrary.router.RouterConstants
import club.fromfactory.baselibrary.utils.StringUtils
import club.fromfactory.baselibrary.utils.ToastUtils
import club.fromfactory.routerannotaions.Router
import club.fromfactory.ui.common.model.EditTextModel
import club.fromfactory.ui.common.model.Goods
import club.fromfactory.ui.main.ScanBaseMVPActivity
import club.fromfactory.ui.movelocation.contract.MoveLocationSelectLocationContract
import club.fromfactory.ui.movelocation.presenter.MoveLocationSelectLocationPresenter
import club.fromfactory.utils.EditTextUtils
import club.fromfactory.utils.TipSound
import club.fromfactory.utils.Zlog
import club.fromfactory.widget.CustomTitleLinearLayout
import kotlinx.android.synthetic.main.activity_move_location_select_location.*

@Router(RouterConstants.MOVE_LOCATION_SELECT_LOCATION)
class MoveLocationSelectLocationActivity : ScanBaseMVPActivity<MoveLocationSelectLocationContract.Presenter>(), MoveLocationSelectLocationContract.View {

    override fun createPresenter(): MoveLocationSelectLocationContract.Presenter = MoveLocationSelectLocationPresenter(this)

    private var mSkuInfo: Goods? = null

    private var mSkuOutNum = 0

    private var isFromScan = false


    private var mEdtMaps = HashMap<EditText, EditTextModel>()

    companion object {

        private const val REQUEST_CODE_MOVE_IN = 1000

    }

//        private var click = 0
    override fun requestScanStart() {
        //test
//        if (Zlog.isDebug) {
//            if (click == 0) {
//                requestScanResult("X13-02-41")
//            } else {
//                requestScanResult("0005405545")
//            }
//            click++
//        }
    }

    override fun getLayoutResId(): Int = R.layout.activity_move_location_select_location

    override fun initView() {
        super.initView()
        select_location_ctl_title.setListener(object : CustomTitleLinearLayout.CustomTitleListener() {
            override fun clickLeft() {
                super.clickLeft()
                finish()
            }
        })
        setScanListener(select_location_scan)
        select_location_ok.setOnClickListener {
            var location = select_location_edt_location.text.toString()
            var sku = select_location_edt_sku.text.toString()
            var num = 0
            try {
                num = select_location_edt_num.text.toString().toInt()
            } catch (e: Exception) {
            }
            if (StringUtils.isNull(location)) {
                ToastUtils.show(resources.getString(R.string.scann_prompt_location))
            } else if (StringUtils.isNull(sku)) {
                ToastUtils.show(resources.getString(R.string.scan_prompt_6))
            } else if (num == 0) {
                ToastUtils.show(resources.getString(R.string.sku_qty_error))
            } else if (num > mSkuInfo?.qty ?: 0) {
                ToastUtils.show(resources.getString(R.string.num_error))
            } else {
                MoveLocationMoveInActivity.launchActivity(this, location, sku, num.toInt(), REQUEST_CODE_MOVE_IN)
            }
        }

        select_location_edt_location.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->

            Zlog.ii("lxm sda3:" + mEdtMaps[select_location_edt_location].toString())
            mEdtMaps[select_location_edt_location]?.isFoucus = hasFocus
            Zlog.ii("lxm sda4:" + mEdtMaps[select_location_edt_location].toString())

            if (!hasFocus) {
                if (!isFromScan) {
                    val s = select_location_edt_location.text.toString()
                    if (s.isNotEmpty()) {
                        charmLocation()
                    }
                }
            } else {

                isFromScan = false

                mSkuOutNum = 0
                select_location_edt_sku.setText("")
                select_location_edt_num.setText(mSkuOutNum.toString())
            }
        }
        select_location_edt_sku.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->

            Zlog.ii("lxm sda3:" + mEdtMaps[select_location_edt_sku].toString())
            mEdtMaps[select_location_edt_sku]?.isFoucus = hasFocus
            Zlog.ii("lxm sda4:" + mEdtMaps[select_location_edt_sku].toString())

            if (!hasFocus) {

                if (!isFromScan) {

                    val s = select_location_edt_sku.text.toString()
                    if (s.isNotEmpty()) {
                        when {
                            StringUtils.isNull(select_location_edt_location.text.toString()) -> {
                                ToastUtils.show(resources.getString(R.string.scann_prompt_location))
                                select_location_edt_sku.setText("")
                            }
                            else -> {

                                val sku = select_location_edt_sku.text.toString()
                                if (StringUtils.isNotBlank(sku)) {
                                    select_location_edt_num.setText("")
                                    charmSku()
                                }

                            }
                        }

                    }

                }


            } else {
                isFromScan = false
                mSkuOutNum = 0
                select_location_edt_num.setText(mSkuOutNum.toString())
            }
        }

        select_location_edt_num.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            Zlog.ii("lxm sda3:" + mEdtMaps[select_location_edt_num].toString())
            mEdtMaps[select_location_edt_num]?.isFoucus = hasFocus
            Zlog.ii("lxm sda4:" + mEdtMaps[select_location_edt_num].toString())
        }

        EditTextUtils.addEditTextListener(select_location_edt_location)
        EditTextUtils.addEditTextListener(select_location_edt_sku)

        select_location_edt_num.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

                if (s?.length ?: 0 > 3) {
                    select_location_edt_num.setText(mSkuOutNum.toString())
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                try {
                    if (s?.isNotEmpty() == true && s.length < 4) {
                        mSkuOutNum = s.toString().toInt()
                    }
                } catch (e: Exception) {
                }

            }

        })

        mEdtMaps[select_location_edt_location] = EditTextModel(select_location_edt_location, select_location_edt_sku, true)
        mEdtMaps[select_location_edt_sku] = EditTextModel(select_location_edt_sku, select_location_edt_num, false)
        mEdtMaps[select_location_edt_num] = EditTextModel(select_location_edt_num, null, false)

    }

    private var isClearEditText = false

    override fun getMoveOutLocation(): String = select_location_edt_location.text.toString()

    override fun getMoveOutSku(): String = select_location_edt_sku.text.toString()

    override fun requestScanResult(result: String?) {

        isFromScan = true

        val iterator = mEdtMaps.keys.iterator()
        while (iterator.hasNext()) {
            val it = iterator.next()
            val editTextModel = mEdtMaps[it]
            Zlog.ii("lxm sda5:" + mEdtMaps[it].toString())

            if (editTextModel?.isFoucus == true) {

                if (it == select_location_edt_num) {

                    val sku = select_location_edt_sku.text.toString()
                    if (StringUtils.isNull(sku)) {
                        select_location_edt_sku.setText(result)

                        select_location_edt_num.isFocusable = true
                        select_location_edt_num.requestFocus()

                        charmSku()

                    } else if (!result.equals(sku)) {
                        TipSound.getInstance().playScanFailedSound(this@MoveLocationSelectLocationActivity)
                        ToastUtils.show(resources.getString(R.string.sku_error))
                    } else {
                        updateSkuNum()
                    }

                } else {

                    it.setText(result ?: "")
                    val nextEditText = editTextModel.nextEditText
                    nextEditText?.requestFocus()

                    if (it == select_location_edt_sku) {
                        charmSku()
                    } else if (it == select_location_edt_location) {
                        charmLocation()
                    }
                }
                break
            }
        }

    }

    private fun updateSkuNum() {
        mSkuOutNum++
        select_location_edt_num.setText(mSkuOutNum.toString())
    }

    private fun charmLocation() {
        select_location_edt_sku.setText("")
        select_location_edt_num.setText("")
        presenter.charmLocation()
    }

    private fun charmSku() {
        presenter.charmLocationAndSku()
    }

    override fun charmLocationResult(isEffective: Boolean, message: String?) {
        if (!isEffective) {
            TipSound.getInstance().playScanFailedSound(this@MoveLocationSelectLocationActivity)
        }
        showFailedError(message)
    }

    override fun charmSkuSuccess(goods: Goods) {
        updateSkuNum()
        TipSound.getInstance().playScanSucceedSound(this@MoveLocationSelectLocationActivity)
        mSkuInfo = goods

        select_location_txt_error.text = ""

        select_location_edt_sku.isFocusable = true
        select_location_edt_num.isFocusable = true
        select_location_edt_num.requestFocus()
    }

    override fun showLoadingView() {
        showLoadingView(select_location_progress)
    }

    override fun hideLoadingView() {
        hideLoadingView(select_location_progress)
    }

    override fun showFailedError(message: String?) {

        select_location_txt_error.text = message ?: ""

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {

            when (requestCode) {
                REQUEST_CODE_MOVE_IN -> {
                    startActivity(intent)
                    finish()
                }
            }

        }
    }

    override fun clickKeyCodeBack() {
        finish()
    }

}
