package club.fromfactory.ui.inventory_inquire

import android.widget.EditText
import club.fromfactory.R
import club.fromfactory.baselibrary.router.RouterConstants
import club.fromfactory.baselibrary.router.RouterManager
import club.fromfactory.baselibrary.router.RouterUrlProvider
import club.fromfactory.baselibrary.utils.StringUtils
import club.fromfactory.baselibrary.widget.CustomDialog
import club.fromfactory.routerannotaions.Router
import club.fromfactory.ui.common.model.EditTextModel
import club.fromfactory.ui.inventory_inquire.contract.InquireContract
import club.fromfactory.ui.inventory_inquire.presenter.InquirePresenter
import club.fromfactory.ui.main.ScanBaseMVPActivity
import club.fromfactory.utils.EditTextUtils
import club.fromfactory.utils.PromptDialogUtils
import club.fromfactory.utils.TipSound
import club.fromfactory.utils.Zlog
import club.fromfactory.widget.CustomTitleLinearLayout
import kotlinx.android.synthetic.main.activity_inquire.*

@Router(RouterConstants.INVENTORY_INQUIRE)
class InquireActivity : ScanBaseMVPActivity<InquireContract.Presenter>(), InquireContract.View {

    private var customDialog: CustomDialog? = null


    private var mEdtMaps = HashMap<EditText, EditTextModel>()

    //    var click = 0
    override fun requestScanStart() {

//        if (Zlog.isDebug) {
//            when (click) {
//                0 -> requestScanResult("X14-01-11")
//                1 -> requestScanResult("743562")
//                2 -> requestScanResult("YH2530213")
//            }
//            click++
//        }
    }

    override fun clickKeyCodeBack() {
        finish()
    }

    override fun getLayoutResId(): Int = R.layout.activity_inquire

    override fun initData() {
        super.initData()
    }

    override fun initView() {
        super.initView()

        setListener()
        setEditTextListener()

    }

    private fun setListener() {
        inquire_ctl_title.setListener(object : CustomTitleLinearLayout.CustomTitleListener() {
            override fun clickLeft() {
                super.clickLeft()
                finish()
            }
        })


        setScanListener(inquire_container_scan)

        inquire_reset.setOnClickListener {
            inquire_edt_location.setText("")
            inquire_edt_item_num.setText("")
            inquire_edt_barcode.setText("")
            inquire_edt_sku.setText("")

            inquire_edt_location.requestFocus()
        }
        inquire_inquire.setOnClickListener {

            val box_name = inquire_edt_location.text.toString()
            val sku_id = inquire_edt_sku.text.toString()
            val barcode = inquire_edt_barcode.text.toString()
            val item_no = inquire_edt_item_num.text.toString()
            val containerCode = inquire_edt_container_num.text.toString()


            if (StringUtils.isNull(box_name) && StringUtils.isNull(sku_id) && StringUtils.isNull(barcode) && StringUtils.isNull(item_no) && StringUtils.isNull(containerCode)) {
                requestFailed(resources.getString(R.string.inquire_prompt))
            }else {
                val map = HashMap<String, String>()
                map["box_name"] = box_name
                map["sku_id"] = sku_id
                map["barcode"] = barcode
                map["item_no"] = item_no
                map["container_code"] = containerCode
                RouterManager.open(this@InquireActivity, RouterUrlProvider.getInquireResultUrl(map))
            }
        }
    }

    private fun setEditTextListener() {

        inquire_edt_location.setOnFocusChangeListener { _, hasFocus ->
            mEdtMaps[inquire_edt_location]?.isFoucus = hasFocus
        }
        inquire_edt_item_num.setOnFocusChangeListener { _, hasFocus ->
            mEdtMaps[inquire_edt_item_num]?.isFoucus = hasFocus
        }
        inquire_edt_barcode.setOnFocusChangeListener { _, hasFocus ->
            mEdtMaps[inquire_edt_barcode]?.isFoucus = hasFocus
        }
        inquire_edt_sku.setOnFocusChangeListener { _, hasFocus ->
            mEdtMaps[inquire_edt_sku]?.isFoucus = hasFocus
        }
        inquire_edt_container_num.setOnFocusChangeListener { _, hasFocus ->
            mEdtMaps[inquire_edt_container_num]?.isFoucus = hasFocus
        }
        EditTextUtils.addEditTextListener(inquire_edt_location)
        EditTextUtils.addEditTextListener(inquire_edt_item_num)
        EditTextUtils.addEditTextListener(inquire_edt_barcode)
        EditTextUtils.addEditTextListener(inquire_edt_sku)
        EditTextUtils.addEditTextListener(inquire_edt_container_num)

        mEdtMaps[inquire_edt_location] = EditTextModel(inquire_edt_location, inquire_edt_item_num, true)
        mEdtMaps[inquire_edt_item_num] = EditTextModel(inquire_edt_item_num, inquire_edt_barcode, false)
        mEdtMaps[inquire_edt_barcode] = EditTextModel(inquire_edt_barcode, inquire_edt_sku, false)
        mEdtMaps[inquire_edt_sku] = EditTextModel(inquire_edt_sku, inquire_edt_container_num, false)
        mEdtMaps[inquire_edt_container_num] = EditTextModel(inquire_edt_container_num, null, false)

    }

    override fun requestScanResult(result: String?) {

        if (customDialog != null && customDialog?.isShowing == true) {
            return
        }

        val iterator = mEdtMaps.keys.iterator()
        while (iterator.hasNext()) {

            val it = iterator.next()

            val editTextModel = mEdtMaps[it]
            if (editTextModel?.isFoucus == true) {

                it.setText(result ?: "")
                val nextEditText = editTextModel.nextEditText
                if (nextEditText == null) {
                } else {
                    nextEditText.requestFocus()
                }
                break
            }

        }


    }

    override fun requestFailed(message: String?) {

        TipSound.getInstance().playScanFailedSound(this)

        customDialog = PromptDialogUtils.getInstance().getPromptDialog(this@InquireActivity, message
                ?: resources.getString(R.string.net_error_prompt_one))
        if (customDialog != null && isAlive) {
            customDialog?.show()
        }
    }

    override fun showLoadingView() {
        super.showLoadingView()
        showLoadingView(inquire_progress)
    }

    override fun hideLoadingView() {
        super.hideLoadingView()
        hideLoadingView(inquire_progress)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (customDialog != null && customDialog?.isShowing == true && isAlive) {
            customDialog?.dismiss()
            customDialog = null
        }
    }


    override fun createPresenter(): InquireContract.Presenter = InquirePresenter(this)

}
