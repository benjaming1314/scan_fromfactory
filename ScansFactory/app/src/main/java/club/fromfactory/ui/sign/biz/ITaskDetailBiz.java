//package club.fromfactory.ui.sign.biz;
//
//import android.content.Context;
//
//import club.fromfactory.base.BaseBiz;
//import club.fromfactory.http.HttpRequestResultListner;
//
///**
// * Created by lxm on 2017/7/25.
// */
//
//public interface ITaskDetailBiz extends BaseBiz {
//
//
//    void updateSignedTask( int task_id,HttpRequestResultListner httpRequestResultListner);
//
//    void trackingNo(int task_id,String tracking_no,HttpRequestResultListner httpRequestResultListner) ;
//
//    void getTaskDetail(int task_id,HttpRequestResultListner httpRequestResultListner) ;
//}
