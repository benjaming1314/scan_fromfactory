package club.fromfactory.ui.inventory

import club.fromfactory.ui.common.model.Goods
import io.reactivex.Observable
import retrofit2.http.*

interface IInventoryGoodsListApi {


    /**
     */
    @GET
    fun checkBoxList(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>, @QueryMap queryParams: Map<String, @JvmSuppressWildcards Any>): Observable<List<Goods>>

    /**
     */
    @GET
    fun getQueryStatus(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>, @QueryMap queryParams: Map<String, @JvmSuppressWildcards Any>): Observable<HashMap<String, Any>>

    /**
     */
    @GET
    fun checkSkuNo(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>, @QueryMap queryParams: Map<String, @JvmSuppressWildcards Any>): Observable<Goods>

    /**
     */
    @PATCH
    fun checkSkuAlterState(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>, @Body bodyParams: Map<String, @JvmSuppressWildcards Any>): Observable<Goods>

}