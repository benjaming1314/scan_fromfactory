package club.fromfactory.ui.inventory_conversion.conversion_move_in.contract

import club.fromfactory.baselibrary.presenter.IPresenter
import club.fromfactory.baselibrary.view.IBaseMVPView
import club.fromfactory.ui.inventory_conversion.conversion_move_in.model.ShelvedResultData
import club.fromfactory.ui.inventory_conversion.conversion_move_out.model.ConversionTaskInfo
import club.fromfactory.ui.inventory_conversion.conversion_move_out.model.ConversionTaskItemInfo

class ConversionMoveInDetailContract {

    interface View :IBaseMVPView<Presenter> {

        fun getCvId() :Int

        fun getCvItemId() :Int

        fun getContainerCode() :String

        //    fun requestTaskSuccess(conversionTaskInfo : ConversionTaskInfo)
        fun requestItemInfoSuccess(conversionTaskItemInfo : ConversionTaskItemInfo)
        fun requestItemInfoFailed()

        fun shelvedSuccess(shelvedResultData : ShelvedResultData)

        fun requestFailed(message :String?)

        fun finishShelvedSuccess()

    }
    interface Presenter :IPresenter<View> {
        fun getItemDetailFromBarcode(barcode: String)
        fun shelvedItem(barcode: String,boxName:String)
        fun finishShelved()

    }
}