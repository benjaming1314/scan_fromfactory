package club.fromfactory.ui.hawb_binding

import android.content.Intent
import club.fromfactory.R
import club.fromfactory.baselibrary.router.RouterConstants
import club.fromfactory.baselibrary.utils.StringUtils
import club.fromfactory.baselibrary.view.BaseActivity
import club.fromfactory.routerannotaions.Router
import club.fromfactory.ui.hawb_binding.contract.HawbBindingContract
import club.fromfactory.ui.hawb_binding.model.HawbDetailInfo
import club.fromfactory.ui.hawb_binding.presenter.HawbBindingPresenter
import club.fromfactory.ui.main.ScanBaseMVPActivity
import club.fromfactory.utils.Zlog
import club.fromfactory.widget.CustomTitleLinearLayout
import com.blankj.utilcode.util.ToastUtils
import kotlinx.android.synthetic.main.activity_hawb_binding.*

@Router(RouterConstants.HAWB_BINDING)
class HawbBindingActivity : ScanBaseMVPActivity<HawbBindingContract.Presenter>(),HawbBindingContract.View {

    private var isHandleScanResult = false

    override fun requestScanStart() {

//        if (Zlog.isDebug) {
//            requestScanResult("D291016000")
//        }
    }

    override fun clickKeyCodeBack() {
        finish()
    }

    override fun getLayoutResId(): Int = R.layout.activity_hawb_binding

    override fun initView() {
        super.initView()

        hawb_binding_ctl_title.setListener(object :CustomTitleLinearLayout.CustomTitleListener(){

            override fun clickLeft() {
                super.clickLeft()
                finish()
            }
        })

        hawb_binding_btn_ok.setOnClickListener {
            val hawb = hawb_binding_edt_hawb.text.toString()
            requestHawbDetail(hawb)

        }

        setScanListener(hawb_binding_scan)

    }

    override fun requestScanResult(result: String?) {
        if (isHandleScanResult) {
            return
        } else {
            isHandleScanResult = true
        }

        if (StringUtils.isNotBlank(result)) {
            hawb_binding_edt_hawb.setText(result)
        }
        requestHawbDetail(result?:"")
    }

    private fun requestHawbDetail(hawb :String?) {
        if (StringUtils.isNotBlank(hawb)) {
            presenter.getHawbDetail(hawb?:"")
        }else {
            requestFailed(resources.getString(R.string.hawb_binding_input_hawb_hint))
        }

    }

    override fun requestHawbDetail(hawbDetailInfo: HawbDetailInfo) {
        isHandleScanResult = false
        HawbBindingDetailActivity.launchActivity(this,hawbDetailInfo)
    }

    override fun requestFailed(message: String?) {
        isHandleScanResult = false
        ToastUtils.showLong(message)
    }


    override fun showLoadingView() {
        showLoadingView(hawb_binding_progress)
    }

    override fun hideLoadingView() {
        hideLoadingView(hawb_binding_progress)
    }

    override fun createPresenter(): HawbBindingContract.Presenter = HawbBindingPresenter(this)

}
