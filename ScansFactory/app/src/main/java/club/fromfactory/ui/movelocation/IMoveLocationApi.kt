package club.fromfactory.ui.movelocation

import club.fromfactory.baselibrary.model.EmptyResponse
import club.fromfactory.pojo.HttpJsonResult
import club.fromfactory.ui.common.model.Goods
import io.reactivex.Observable
import retrofit2.http.*

interface IMoveLocationApi {

    /**
     */
    @GET
    fun charmLocation(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>, @QueryMap queryParams: Map<String, @JvmSuppressWildcards Any>): Observable<HttpJsonResult<EmptyResponse>>

    /**
     */
    @GET
    fun charmLocationAndSku(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>, @QueryMap queryParams: Map<String, @JvmSuppressWildcards Any>): Observable<Goods>

    /**
     */
    @POST
    fun moveSku(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>, @Body bodyParams: Map<String, @JvmSuppressWildcards Any>): Observable<HttpJsonResult<EmptyResponse>>


}