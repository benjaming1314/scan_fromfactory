package club.fromfactory.ui.sign.presenter

import club.fromfactory.FFApplication
import club.fromfactory.R
import club.fromfactory.baselibrary.language.LanguageUtils
import club.fromfactory.baselibrary.model.EmptyResponse
import club.fromfactory.baselibrary.net.CookieHelper
import club.fromfactory.baselibrary.net.retrofit.BaseRetrofit
import club.fromfactory.baselibrary.presenter.BasePresenter
import club.fromfactory.baselibrary.rx.loadingView
import club.fromfactory.baselibrary.utils.JsonUtils
import club.fromfactory.baselibrary.utils.PreferenceStorageUtils
import club.fromfactory.baselibrary.view.RxAppCompatActivity
import club.fromfactory.constant.Constants
import club.fromfactory.http.NetUtils
import club.fromfactory.http.model.HttpJsonErrorResult
import club.fromfactory.pojo.HttpJsonResult
import club.fromfactory.ui.sign.ISigningApi
import club.fromfactory.ui.sign.contract.TaskDetailContract
import club.fromfactory.ui.sign.model.HttpResultTrackingNo
import club.fromfactory.ui.sign.model.TrackingsNoDetailModel
import com.trello.rxlifecycle2.kotlin.bindToLifecycle
import io.reactivex.observers.DefaultObserver
import retrofit2.HttpException
import java.util.*

class TaskDetailPresenter(view: TaskDetailContract.View) : BasePresenter<TaskDetailContract.View>(view), TaskDetailContract.Presenter {

    /**
     * 结束签收任务
     */

    private var updateSignedTaskUrl = NetUtils.APP_MAIN_URL_PROCUREMENT + "purchase_order/package/signed_task/update/"

    /**
     * 扫描快递单号
     */
    private var trackingNoUrl = NetUtils.APP_MAIN_URL_PROCUREMENT + "purchase_order/signed/search/tracking_no/"
    /**
     * 获取某个具体任务的物流单号列表信息
     */
    private var getTaskDetailUrl = NetUtils.APP_MAIN_URL_PROCUREMENT + "purchase_order/package/task_detail/list/"


    override fun updateSignedTask() {
        val token = PreferenceStorageUtils.getInstance().token

        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        //设置参数，访问url获取json
        val params = HashMap<String, Any>()
        params["task_id"] = view.getTaskId()

        BaseRetrofit
                .createService(ISigningApi::class.java)
                .updateSignedTask(updateSignedTaskUrl, headerMap, params)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : DefaultObserver<HttpJsonResult<EmptyResponse>>() {
                    override fun onComplete() {
                    }

                    override fun onNext(httpJsonResult: HttpJsonResult<EmptyResponse>) {
                        if (httpJsonResult.is_success()) {
                            view.updateTaskSuccess()
                        } else {
                            view.showFailedError(httpJsonResult.getMsg())
                        }
                    }

                    override fun onError(e: Throwable) {
                        if (e is HttpException && e.code() in 400..403) {
                            val string = e.response()?.errorBody()?.string()
                            if (string != null) {
                                val parseJson = JsonUtils.parseJson<HttpJsonErrorResult>(string)
                                view.showFailedError(parseJson?.message)
                            }
                        } else {
                            view.showFailedError(FFApplication.getInstance().resources.getString(R.string.net_error))
                        }
                    }

                })
    }

    override fun trackingNo(tracking_no: String) {
        val token = PreferenceStorageUtils.getInstance().token

        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        //设置参数，访问url获取json
        val params = HashMap<String, Any>()
        params["task_id"] = view.getTaskId()
        params["tracking_no"] = tracking_no

        BaseRetrofit
                .createService(ISigningApi::class.java)
                .trackingNo(trackingNoUrl, headerMap, params)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : DefaultObserver<HttpResultTrackingNo>() {
                    override fun onComplete() {
                    }

                    override fun onNext(httpResultTrackingNo: HttpResultTrackingNo) {
                        if (httpResultTrackingNo.is_success) {
                            val trackingsNo = httpResultTrackingNo.data
                            view.trackingsNo(trackingsNo)
                        } else {
                            view.showFailedError(httpResultTrackingNo.msg)
                        }
                    }

                    override fun onError(e: Throwable) {
                        if (e is HttpException && e.code() in 400..403) {
                            val string = e.response()?.errorBody()?.string()
                            if (string != null) {
                                val parseJson = JsonUtils.parseJson<HttpJsonErrorResult>(string)
                                view.showFailedError(parseJson?.message)
                            }
                        } else {
                            view.showFailedError(FFApplication.getInstance().resources.getString(R.string.net_error))
                        }
                    }

                })
    }

    override fun getTaskDetail() {
        val token = PreferenceStorageUtils.getInstance().token

        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        //设置参数，访问url获取json
        val params = HashMap<String, Any>()
        params["task_id"] = view.getTaskId()

        BaseRetrofit
                .createService(ISigningApi::class.java)
                .getTaskDetail(getTaskDetailUrl, headerMap, params)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : DefaultObserver<TrackingsNoDetailModel>() {
                    override fun onComplete() {
                    }

                    override fun onNext(trackingsNoModel: TrackingsNoDetailModel) {
                        val trackingsNoList = trackingsNoModel.results
                        if (trackingsNoList != null) {
                            view.httpResult(trackingsNoList)
                        } else {
                            view.showFailedError("failed")
                        }
                    }

                    override fun onError(e: Throwable) {
                        if (e is HttpException && e.code() in 400..403) {
                            val string = e.response()?.errorBody()?.string()
                            if (string != null) {
                                val parseJson = JsonUtils.parseJson<HttpJsonErrorResult>(string)
                                view.showFailedError(parseJson?.message)
                            }
                        } else {
                            view.showFailedError(FFApplication.getInstance().resources.getString(R.string.net_error))
                        }
                    }

                })
    }
}