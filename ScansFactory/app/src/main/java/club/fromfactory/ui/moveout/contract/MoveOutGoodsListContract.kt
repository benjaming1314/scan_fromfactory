package club.fromfactory.ui.moveout.contract

import club.fromfactory.baselibrary.presenter.IPresenter
import club.fromfactory.baselibrary.view.IBaseMVPView
import club.fromfactory.ui.common.model.Goods

class MoveOutGoodsListContract {

    interface View : IBaseMVPView<Presenter> {

        fun getBoxName(): String
        fun startMunGoodsPage(count:Int)
        fun refreshData(goodsList: List<Goods>?)

        fun moveoutConfirm(goods: Goods?)
        fun moveoutConfirmFailed(message: String?)

        fun showFailedError(message: String?)
    }

    interface Presenter : IPresenter<View> {

        fun getSkuList()
        fun operateOffShelve(barcode: String)
    }
}