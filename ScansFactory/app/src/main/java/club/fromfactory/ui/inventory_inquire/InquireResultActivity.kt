package club.fromfactory.ui.inventory_inquire

import club.fromfactory.R
import club.fromfactory.baselibrary.router.RouterConstants
import club.fromfactory.baselibrary.utils.DensityUtils
import club.fromfactory.baselibrary.view.BaseMVPActivity
import club.fromfactory.baselibrary.widget.CustomDialog
import club.fromfactory.baselibrary.widget.recyclerview.BaseSpaceItemDecoration
import club.fromfactory.routerannotaions.Router
import club.fromfactory.routerannotaions.RouterParam
import club.fromfactory.ui.inventory_inquire.adapter.InquireResultAdapter
import club.fromfactory.ui.inventory_inquire.contract.InquireResultContract
import club.fromfactory.ui.inventory_inquire.model.InquireResultData
import club.fromfactory.ui.inventory_inquire.presenter.InquireResultPresenter
import club.fromfactory.utils.PromptDialogUtils
import club.fromfactory.widget.CustomTitleLinearLayout
import com.blankj.utilcode.util.ToastUtils
import kotlinx.android.synthetic.main.activity_inquire_result.*

@Router(RouterConstants.INVENTORY_INQUIRE_RESULT)
class InquireResultActivity : BaseMVPActivity<InquireResultContract.Presenter>(),InquireResultContract.View {


    @RouterParam("box_name")
    @JvmField
    var boxName :String ?= null
    @RouterParam("sku_id")
    @JvmField
    var skuId :String ?= null
    @RouterParam("barcode")
    @JvmField
    var barcode :String ?= null
    @RouterParam("item_no")
    @JvmField
    var itemNo :String ?= null
    @RouterParam("container_code")
    @JvmField
    var containerCode :String ?= null

    private var mInquireResultAdapter : InquireResultAdapter ?= null

    private var customDialog: CustomDialog? = null

    override fun clickKeyCodeBack() {
        finish()
    }
    override fun getSkuid(): String  = skuId?:""

    override fun getBoxName(): String = boxName?:""

    override fun getItemNo(): String = itemNo?:""

    override fun getBarcode(): String = barcode?:""

    override fun getContainerCode(): String = containerCode?:""

    override fun getLayoutResId(): Int = R.layout.activity_inquire_result

    override fun initData() {
        super.initData()
    }

    override fun initView() {
        super.initView()

        inquire_result_ctl_title.setListener(object :CustomTitleLinearLayout.CustomTitleListener() {
            override fun clickLeft() {
                super.clickLeft()
                finish()
            }
        })

        mInquireResultAdapter = InquireResultAdapter()
        inquire_result_recycler_view.addItemDecoration(BaseSpaceItemDecoration(0,0,0,DensityUtils.dp2px(this,5)))
        inquire_result_recycler_view.adapter = mInquireResultAdapter

        mInquireResultAdapter?.setOnItemViewClickListener { data, _, _ ->
            InquireResultDetailActivity.launchActivity(this@InquireResultActivity,data!!)
        }


    }

    override fun fetchData() {
        super.fetchData()
        presenter.getInquireResult()
    }

    override fun requestInquireResult(inquireResultData: InquireResultData) {

        if (inquireResultData.rows?.size == 0) {
            ToastUtils.showLong(resources.getString(R.string.no_inquire_result_prompt))
        }else {
            inquire_result_txt_num.text = inquireResultData.qty_sum.toString()
            mInquireResultAdapter?.addAll(inquireResultData.rows)
        }

    }

    override fun requestFailed(message: String?) {

        customDialog = PromptDialogUtils.getInstance().getPromptDialog(this@InquireResultActivity, message
                ?: resources.getString(R.string.net_error_prompt_one))
        if (customDialog != null && isAlive) {
            customDialog?.show()
        }

    }

    override fun createPresenter(): InquireResultContract.Presenter = InquireResultPresenter(this)
    override fun onDestroy() {
        super.onDestroy()
        if (customDialog != null && customDialog?.isShowing == true && isAlive) {
            customDialog?.dismiss()
            customDialog = null
        }
    }
}
