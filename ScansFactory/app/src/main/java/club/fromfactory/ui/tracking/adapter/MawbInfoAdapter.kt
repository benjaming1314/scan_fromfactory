package club.fromfactory.ui.tracking.adapter

import android.view.ViewGroup
import club.fromfactory.baselibrary.view.recyclerview.BaseRecyclerAdapter
import club.fromfactory.baselibrary.view.recyclerview.BaseViewHolder
import club.fromfactory.ui.tracking.model.MawbInfo
import club.fromfactory.ui.tracking.viewholder.MawbInfoViewHolder

class MawbInfoAdapter : BaseRecyclerAdapter<MawbInfo>() {
    override fun onCreateBaseViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<MawbInfo> = MawbInfoViewHolder(parent)
}