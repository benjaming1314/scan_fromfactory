package club.fromfactory.ui.inventory_conversion.conversion_move_out.presenter

import club.fromfactory.FFApplication
import club.fromfactory.R
import club.fromfactory.baselibrary.language.LanguageUtils
import club.fromfactory.baselibrary.model.BaseResponse
import club.fromfactory.baselibrary.model.EmptyResponse
import club.fromfactory.baselibrary.net.CookieHelper
import club.fromfactory.baselibrary.net.retrofit.BaseRetrofit
import club.fromfactory.baselibrary.presenter.BasePresenter
import club.fromfactory.baselibrary.rx.loadingView
import club.fromfactory.baselibrary.utils.JsonUtils
import club.fromfactory.baselibrary.utils.PreferenceStorageUtils
import club.fromfactory.baselibrary.view.RxAppCompatActivity
import club.fromfactory.constant.Constants
import club.fromfactory.http.NetUtils
import club.fromfactory.http.model.HttpJsonErrorResult
import club.fromfactory.ui.inventory_conversion.conversion_move_out.IConversionMoveOutApi
import club.fromfactory.ui.inventory_conversion.conversion_move_out.contract.ConversionTaskContract
import club.fromfactory.ui.inventory_conversion.conversion_move_out.model.ConversionTaskInfo
import com.trello.rxlifecycle2.kotlin.bindToLifecycle
import io.reactivex.observers.DefaultObserver
import retrofit2.HttpException
import java.util.*

class ConversionTaskPresenter(view: ConversionTaskContract.View) : BasePresenter<ConversionTaskContract.View>(view), ConversionTaskContract.Presenter {

    private var cvOrderInfoUrl = NetUtils.APP_MAIN_URL + "wms-center/cv_order/head_info"
    private var cvOrderBingContainerUrl = NetUtils.APP_MAIN_URL + "wms-center/cv_order/container"

    override fun getConversionTask(type: String) {

        val token = PreferenceStorageUtils.getInstance().token

        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        //设置参数，访问url获取json
        val params = HashMap<String, String>()
        params["type"] = type

        BaseRetrofit
                .createService(IConversionMoveOutApi::class.java)
                .getCvOrderInfo(cvOrderInfoUrl, headerMap, params)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : DefaultObserver<BaseResponse<ConversionTaskInfo>>() {
                    override fun onComplete() {
                    }

                    override fun onNext(baseResponse: BaseResponse<ConversionTaskInfo>) {
                        val success = baseResponse.isSuccess

                        val model = baseResponse.model

                        if (success && model != null) {
                            view.requestSuccess(model)
                        } else if (success) {
                            view.requestSuccess()
                        } else {
                            view.requestFailed(baseResponse.firstErrorMessage.displayMessage)
                        }
                    }

                    override fun onError(e: Throwable) {
                        if (e is HttpException && e.code() in 400..403) {
                            val string = e.response()?.errorBody()?.string()
                            if (string != null) {
                                val parseJson = JsonUtils.parseJson<HttpJsonErrorResult>(string)
                                view.requestFailed(parseJson?.message)
                            }
                        } else {
                            view.requestFailed(FFApplication.getInstance().resources.getString(R.string.net_error))
                        }
                    }

                })
    }

    override fun bindContainer(cvId: Int, containerCode: String) {

        val token = PreferenceStorageUtils.getInstance().token

        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        //设置参数，访问url获取json
        val params = HashMap<String, Any>()
        params["cvId"] = cvId
        params["containerCode"] = containerCode

        BaseRetrofit
                .createService(IConversionMoveOutApi::class.java)
                .bindContainer(cvOrderBingContainerUrl, headerMap, params)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : DefaultObserver<BaseResponse<EmptyResponse>>() {
                    override fun onComplete() {
                    }

                    override fun onNext(baseResponse: BaseResponse<EmptyResponse>) {
                        val success = baseResponse.isSuccess

                        if (success) {
                            view.bindContainerSucceed()
                        } else {
                            view.requestFailed(baseResponse.firstErrorMessage.displayMessage)
                        }
                    }

                    override fun onError(e: Throwable) {
                        if (e is HttpException && e.code() in 400..403) {
                            val string = e.response()?.errorBody()?.string()
                            if (string != null) {
                                val parseJson = JsonUtils.parseJson<HttpJsonErrorResult>(string)
                                view.requestFailed(parseJson?.message)
                            }
                        } else {
                            view.requestFailed(FFApplication.getInstance().resources.getString(R.string.net_error))
                        }
                    }

                })
    }

}