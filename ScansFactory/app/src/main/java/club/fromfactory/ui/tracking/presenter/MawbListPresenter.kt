package club.fromfactory.ui.tracking.presenter

import club.fromfactory.FFApplication
import club.fromfactory.R
import club.fromfactory.baselibrary.language.LanguageUtils
import club.fromfactory.baselibrary.model.BaseResponse
import club.fromfactory.baselibrary.net.CookieHelper
import club.fromfactory.baselibrary.net.retrofit.BaseRetrofit
import club.fromfactory.baselibrary.presenter.BasePresenter
import club.fromfactory.baselibrary.rx.loadingView
import club.fromfactory.baselibrary.utils.JsonUtils
import club.fromfactory.baselibrary.utils.PreferenceStorageUtils
import club.fromfactory.baselibrary.view.RxAppCompatActivity
import club.fromfactory.constant.Constants
import club.fromfactory.http.NetUtils
import club.fromfactory.http.model.HttpJsonErrorResult
import club.fromfactory.ui.tracking.ITrackingApi
import club.fromfactory.ui.tracking.contract.MawbListContract
import club.fromfactory.ui.tracking.model.MawbListResponse
import com.trello.rxlifecycle2.kotlin.bindToLifecycle
import io.reactivex.observers.DefaultObserver
import retrofit2.HttpException
import java.util.*

class MawbListPresenter(view: MawbListContract.View) : BasePresenter<MawbListContract.View>(view), MawbListContract.Presenter {

//    private var trackingListUrl = NetUtils.APP_MAIN_URL_TRACKING + "mawbs/pda/list"

    override fun getTrackingList() {

        val pageNo = view.getPageNo()
        val pageSize = view.getPageSize()

        val token = PreferenceStorageUtils.getInstance().token

        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        //设置参数，访问url获取json
        val params = HashMap<String, String>()
        params["pageNo"] = pageNo.toString()
        params["pageSize"] = pageSize.toString()
        params["dbName"] = Constants.SERVER_LOCATION


        var url = NetUtils.APP_MAIN_URL_TRACKING + "mawbs/pda/list"

        if (PreferenceStorageUtils.getInstance().developentState) {
            url = NetUtils.APP_MAIN_URL_TRACKING_TEST + "mawbs/pda/list"
        }

        BaseRetrofit
                .createService(ITrackingApi::class.java)
                .getTrackingList(url, headerMap, params)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : DefaultObserver<BaseResponse<MawbListResponse>>() {
                    override fun onComplete() {
                    }

                    override fun onNext(response: BaseResponse<MawbListResponse>) {
                        if (response.isSuccess) {
                            view.refreshMawbList(response.model?.mawbs)
                        } else {
                            view.showFailedError(response.firstErrorMessage.message)
                        }
                    }

                    override fun onError(e: Throwable) {
                        if (e is HttpException && e.code() in 400..403) {
                            val string = e.response()?.errorBody()?.string()
                            if (string != null) {
                                val parseJson = JsonUtils.parseJson<HttpJsonErrorResult>(string)
                                view.showFailedError(parseJson?.message)
                            }
                        } else {
                            view.showFailedError(FFApplication.getInstance().resources.getString(R.string.net_error))
                        }
                    }

                })


    }


}