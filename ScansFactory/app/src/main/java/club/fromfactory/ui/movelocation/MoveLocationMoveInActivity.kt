package club.fromfactory.ui.movelocation

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import club.fromfactory.R
import club.fromfactory.baselibrary.utils.StringUtils
import club.fromfactory.baselibrary.utils.ToastUtils
import club.fromfactory.baselibrary.widget.CustomDialog
import club.fromfactory.ui.common.model.EditTextModel
import club.fromfactory.ui.main.ScanBaseMVPActivity
import club.fromfactory.ui.movelocation.contract.MoveLocationMoveInContract
import club.fromfactory.ui.movelocation.presenter.MoveLocationMoveInPresenter
import club.fromfactory.utils.EditTextUtils
import club.fromfactory.utils.PromptDialogUtils
import club.fromfactory.utils.TipSound
import club.fromfactory.utils.Zlog
import club.fromfactory.widget.CustomTitleLinearLayout
import kotlinx.android.synthetic.main.activity_move_location_move_in.*

class MoveLocationMoveInActivity : ScanBaseMVPActivity<MoveLocationMoveInContract.Presenter>(), MoveLocationMoveInContract.View {
    override fun createPresenter(): MoveLocationMoveInContract.Presenter = MoveLocationMoveInPresenter(this)

    private var mSkuNum: Int = 0
    private var mFromBox: String? = null
    private var mFromSku: String? = null

    private var mMoveInSkuNum = 0

    private var isFromScan = false

    private var customDialog: CustomDialog? = null

    private var mEdtMaps = HashMap<EditText, EditTextModel>()

    companion object {

        private const val LOCATION = "location"
        private const val SKU = "sku"
        private const val SKU_NUM = "num"

        fun launchActivity(context: Context, locaiton: String, sku: String, num: Int, resultCode: Int) {
            val intent = Intent()
            intent.setClass(context, MoveLocationMoveInActivity::class.java)
            intent.putExtra(LOCATION, locaiton)
            intent.putExtra(SKU, sku)
            intent.putExtra(SKU_NUM, num)
            (context as Activity).startActivityForResult(intent, resultCode)
        }

    }

        private var click = 0
    override fun requestScanStart() {
        //test
        if (Zlog.isDebug) {
            if (click == 0) {
                requestScanResult("X13-01-13")
            } else {
                requestScanResult("0005405545")
            }
            click++
        }
    }

    override fun getLayoutResId(): Int = R.layout.activity_move_location_move_in

    override fun initData() {
        super.initData()
        mFromBox = intent.getStringExtra(LOCATION)
        mFromSku = intent.getStringExtra(SKU)
        mSkuNum = intent.getIntExtra(SKU_NUM, 0)

        move_in_loacation_txt_location.text = mFromBox
        move_in_loacation_txt_sku.text = mFromSku
        move_in_loacation_txt_num.text = mSkuNum.toString()

    }

    override fun initView() {
        super.initView()
        move_in_loacation_ctl_title.setListener(object : CustomTitleLinearLayout.CustomTitleListener() {
            override fun clickLeft() {
                super.clickLeft()
                finish()
            }
        })
        setScanListener(move_in_location_scan)

        setTextChangedListener()
        setListener()

        mEdtMaps[move_in_loacation_edt_location] = EditTextModel(move_in_loacation_edt_location, move_in_loacation_edt_sku, true)
        mEdtMaps[move_in_loacation_edt_sku] = EditTextModel(move_in_loacation_edt_sku, move_in_loacation_edt_num, false)
        mEdtMaps[move_in_loacation_edt_num] = EditTextModel(move_in_loacation_edt_num, null, false)

    }

    private fun setListener() {

        move_in_loacation_cancel.setOnClickListener {
            setResult(Activity.RESULT_OK)
            finish()
        }

        move_in_loacation_ok.setOnClickListener {

            if (StringUtils.isNull(move_in_loacation_edt_location.text.toString())) {
                ToastUtils.show(resources.getString(R.string.scann_prompt_location))
            } else if (StringUtils.isNull(move_in_loacation_edt_location.text.toString())) {
                ToastUtils.show(resources.getString(R.string.scan_prompt_6))
            } else if (mMoveInSkuNum == 0) {
                ToastUtils.show(resources.getString(R.string.sku_qty_error))
            } else if (mMoveInSkuNum > mSkuNum) {
                ToastUtils.show(resources.getString(R.string.num_error))
            } else {
                presenter.moveSku()
            }
        }

    }

    private fun setTextChangedListener() {

        move_in_loacation_edt_location.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->

            Zlog.ii("lxm sda3:" + mEdtMaps[move_in_loacation_edt_location].toString())
            mEdtMaps[move_in_loacation_edt_location]?.isFoucus = hasFocus
            Zlog.ii("lxm sda4:" + mEdtMaps[move_in_loacation_edt_location].toString())

            if (!hasFocus) {
                if (!isFromScan) {
                    val s = move_in_loacation_edt_location.text.toString()
                    if (s.isNotEmpty()) {
                        charmLocation()
                    }

                }
            } else {
                isFromScan = false

            }
        }
        move_in_loacation_edt_sku.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->

            Zlog.ii("lxm sda3:" + mEdtMaps[move_in_loacation_edt_sku].toString())
            mEdtMaps[move_in_loacation_edt_sku]?.isFoucus = hasFocus
            Zlog.ii("lxm sda4:" + mEdtMaps[move_in_loacation_edt_sku].toString())

            if (!hasFocus) {

                if (!isFromScan) {
                    val sku = move_in_loacation_edt_sku.text.toString()
                    if (sku.isNotEmpty()) {
                        when {
                            StringUtils.isNull(move_in_loacation_edt_location.text.toString()) -> {
                                ToastUtils.show(resources.getString(R.string.scann_prompt_location))
                            }
                            else -> {
                                if (sku != mFromSku) {
                                    TipSound.getInstance().playScanFailedSound(this@MoveLocationMoveInActivity)
                                    ToastUtils.show(resources.getString(R.string.sku_error))
                                } else {
                                    updateSkuNum()
                                }

                            }
                        }
                    }
                }
            } else {
                isFromScan = false

                mMoveInSkuNum = 0
                move_in_loacation_edt_num.setText(mMoveInSkuNum.toString())
            }
        }

        move_in_loacation_edt_num.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            Zlog.ii("lxm sda3:" + mEdtMaps[move_in_loacation_edt_num].toString())
            mEdtMaps[move_in_loacation_edt_num]?.isFoucus = hasFocus
            Zlog.ii("lxm sda4:" + mEdtMaps[move_in_loacation_edt_num].toString())
        }

        EditTextUtils.addEditTextListener(move_in_loacation_edt_location)
        EditTextUtils.addEditTextListener(move_in_loacation_edt_sku)

        move_in_loacation_edt_num.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s?.length ?: 0 > 3) {
                    move_in_loacation_edt_num.setText(mMoveInSkuNum.toString())
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                try {
                    if (s?.isNotEmpty() == true && s.length < 4) {
                        mMoveInSkuNum = s.toString().toInt()
                    }
                } catch (e: Exception) {
                }

            }

        })


    }

    private var isClearEditText = false

    override fun requestScanResult(result: String?) {
        if (customDialog?.isShowing == true) {
            return
        }
        isFromScan = true

        val iterator = mEdtMaps.keys.iterator()
        while (iterator.hasNext()) {
            val it = iterator.next()

            val editTextModel = mEdtMaps[it]
            Zlog.ii("lxm sda5:" + mEdtMaps[it].toString())

            if (editTextModel?.isFoucus == true) {

                if (it == move_in_loacation_edt_num) {

                    val sku = move_in_loacation_edt_sku.text.toString()
                    if (StringUtils.isNull(sku)) {
                        if (result == mFromSku) {
                            move_in_loacation_edt_sku.setText(result)
                            updateSkuNum()
                            move_in_loacation_edt_num.requestFocus()

                        } else {
                            ToastUtils.show(resources.getString(R.string.sku_error))
                        }
                    } else {
                        if (result == mFromSku) {

                            move_in_loacation_edt_num.requestFocus()
                            updateSkuNum()

                        } else {
                            TipSound.getInstance().playScanFailedSound(this@MoveLocationMoveInActivity)
                            ToastUtils.show(resources.getString(R.string.sku_error))
                        }

                    }

                } else {

                    it.setText(result ?: "")
                    val nextEditText = editTextModel.nextEditText
                    nextEditText?.requestFocus()

                    if (it == move_in_loacation_edt_sku) {

                        if (result == mFromSku) {
                            move_in_loacation_edt_sku.setText(result)
                            updateSkuNum()
                            move_in_loacation_edt_num.isFocusable = true
                            move_in_loacation_edt_num.requestFocus()

                        } else {
                            ToastUtils.show(resources.getString(R.string.sku_error))
                        }

                    } else if (it == move_in_loacation_edt_location) {
                        charmLocation()
                    }
                }

                break
            }
        }
    }

    private fun updateSkuNum() {
        mMoveInSkuNum++
        move_in_loacation_edt_num.setText(mMoveInSkuNum.toString())
    }

    private fun charmLocation() {
        move_in_loacation_edt_sku.setText("")
        move_in_loacation_edt_num.setText("")
        presenter.charmLocation()
    }

    override fun getFromBox(): String = mFromBox ?: ""

    override fun getMoveInSku(): String = move_in_loacation_edt_sku.text.toString()

    override fun getToBox(): String = move_in_loacation_edt_location.text.toString()

    override fun getMoveQty(): String = move_in_loacation_edt_num.text.toString()

    override fun showLoadingView() {
        showLoadingView(move_in_loacation_progress)
    }

    override fun hideLoadingView() {
        hideLoadingView(move_in_loacation_progress)
    }

    override fun moveInSuccess() {
        TipSound.getInstance().playScanSucceedSound(this@MoveLocationMoveInActivity)
        setResult(Activity.RESULT_OK)
        finish()

    }

    override fun showFailedError(message: String?) {

        customDialog = PromptDialogUtils.getInstance().getPromptDialog(this@MoveLocationMoveInActivity, message)
        if (customDialog != null && isAlive) {
            customDialog?.show()
        }
    }

    override fun charmLocationResult(isEffective: Boolean, message: String?) {
        if (!isEffective) {
            TipSound.getInstance().playScanFailedSound(this@MoveLocationMoveInActivity)
            showFailedError(message)
        }
    }

    override fun clickKeyCodeBack() {
        finish()
    }

    override fun onDestroy() {
        super.onDestroy()

        if (customDialog != null) {
            if (customDialog?.isShowing == true) {
                customDialog?.dismiss()
            }
            customDialog = null
        }
    }

}
