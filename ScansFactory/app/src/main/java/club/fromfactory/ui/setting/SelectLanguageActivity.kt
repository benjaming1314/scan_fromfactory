package club.fromfactory.ui.setting

import android.app.Activity
import android.content.Intent
import club.fromfactory.R
import club.fromfactory.utils.Zlog
import club.fromfactory.baselibrary.language.LanguageUtils
import club.fromfactory.baselibrary.utils.PreferenceStorageUtils
import club.fromfactory.baselibrary.view.BaseActivity
import club.fromfactory.ui.setting.adapter.LanguageAdapter
import club.fromfactory.widget.CustomTitleLinearLayout
import kotlinx.android.synthetic.main.activity_select_language.*

class SelectLanguageActivity : BaseActivity() {

    companion object {
        const val IS_SHOW_BACK = "is_show_back"

        fun launchActivity(activity: BaseActivity, isShowBack: Boolean, requestCode: Int) {

            val intent = Intent(activity, SelectLanguageActivity::class.java)
            intent.putExtra(IS_SHOW_BACK, isShowBack)
            activity.startActivityForResult(intent, requestCode)
        }
    }

    private var isShowBack = false

    private var mLanguageAdapter: LanguageAdapter? = null

    override fun getLayoutResId(): Int = R.layout.activity_select_language

    override fun initData() {
        super.initData()
        isShowBack = intent.getBooleanExtra(IS_SHOW_BACK, false)

    }


    override fun initView() {
        super.initView()
        select_language_ctl_title.setListener(object : CustomTitleLinearLayout.CustomTitleListener() {

            override fun clickLeft() {
                super.clickLeft()
                finish()
            }
        })

        select_language_txt_current_language.text = LanguageUtils.getCurrentLanguageName()

        select_language_current_device.setOnClickListener {

            if (isShowBack) {
                finish()
            } else {


            }

        }

        mLanguageAdapter = LanguageAdapter()
        select_language_recycler_view.adapter = mLanguageAdapter

        mLanguageAdapter?.setOnItemViewClickListener { data, clickedView, position ->

            Zlog.ii("loginscanactivity initData:$data")
            if (LanguageUtils.isNeedchangeLanguage(data?.languageCode)) {
                PreferenceStorageUtils.getInstance().saveAppSelectLanguage(data?.languageCode)
                LanguageUtils.changeAppLanguage(data?.languageCode)
                setResult(Activity.RESULT_OK)
            }
            finish()

        }

        mLanguageAdapter?.addAll(LanguageUtils.getLanguageInfoList())
    }


    override fun clickKeyCodeBack() {
        select_language_current_device.callOnClick()
    }
}
