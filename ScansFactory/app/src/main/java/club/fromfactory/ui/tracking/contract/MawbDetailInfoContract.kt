package club.fromfactory.ui.tracking.contract

import club.fromfactory.baselibrary.presenter.IPresenter
import club.fromfactory.baselibrary.view.IBaseMVPView
import club.fromfactory.ui.tracking.model.MawbInfo
import club.fromfactory.ui.tracking.model.PackageInfo

class MawbDetailInfoContract {

    interface View : IBaseMVPView<Presenter> {
        fun getMawbId() :Long

        fun refreshMawbDetailInfo(mMawbInfo: MawbInfo?)
        fun mawbBindPackageSuccess(mPackageInfo: PackageInfo?)
        fun transferConfirmSuccess()
        fun showFailedErrorWindow(message: String?)

        fun transferConfirmIgnoreOption()

        fun mawbUnBindPackageSuccess()
        fun showFailedError(message: String?)
    }

    interface Presenter : IPresenter<View> {
        fun getMawbDetailInfo()
        fun mawbBindPackage(packageNo : String)
        fun transferConfirm(ignoreOption : Int ? = 0)

        fun mawbUnBindPackage(packageNo : String)

    }
}