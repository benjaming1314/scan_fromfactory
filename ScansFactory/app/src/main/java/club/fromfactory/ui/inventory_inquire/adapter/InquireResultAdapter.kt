package club.fromfactory.ui.inventory_inquire.adapter

import android.view.ViewGroup
import club.fromfactory.baselibrary.view.recyclerview.BaseRecyclerAdapter
import club.fromfactory.baselibrary.view.recyclerview.BaseViewHolder
import club.fromfactory.ui.common.model.Goods
import club.fromfactory.ui.inventory_inquire.viewholder.InquireResultViewHolder

class InquireResultAdapter: BaseRecyclerAdapter<Goods>() {

    override fun onCreateBaseViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Goods> = InquireResultViewHolder(parent)
}