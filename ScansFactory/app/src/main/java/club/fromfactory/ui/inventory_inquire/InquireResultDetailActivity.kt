package club.fromfactory.ui.inventory_inquire

import android.content.Intent
import club.fromfactory.R
import club.fromfactory.baselibrary.router.RouterConstants
import club.fromfactory.baselibrary.view.BaseActivity
import club.fromfactory.routerannotaions.Router
import club.fromfactory.routerannotaions.RouterParam
import club.fromfactory.ui.common.model.Goods
import club.fromfactory.ui.viewphotos.ViewPhotosActivity
import club.fromfactory.utils.ImageUtils
import club.fromfactory.widget.CustomTitleLinearLayout
import kotlinx.android.synthetic.main.activity_inquire_reslut_detail.*
@Router(RouterConstants.INVENTORY_INQUIRE_RESULT_DETAIL)
class InquireResultDetailActivity : BaseActivity() {

    @RouterParam("goods")
    @JvmField
    var goods: Goods? = null

    companion object {

        fun launchActivity(activity: BaseActivity, goods: Goods) {
            val intent = Intent(activity, InquireResultDetailActivity::class.java)
            intent.putExtra("goods", goods)
            activity.startActivity(intent)
        }
    }


    override fun clickKeyCodeBack() {
        finish()
    }

    override fun getLayoutResId(): Int = R.layout.activity_inquire_reslut_detail

    override fun initData() {
        super.initData()

        if (goods == null) {
            finish()
        }
    }

    override fun initView() {
        super.initView()

        inquire_result_detail_ctl_title.setListener(object : CustomTitleLinearLayout.CustomTitleListener() {

            override fun clickLeft() {
                super.clickLeft()
                finish()
            }
        })
        setData()


        inquire_result_detail_img.setOnClickListener {
            val intent = Intent(this, ViewPhotosActivity::class.java)
            intent.putExtra("images", goods?.sku_image_url)
            startActivity(intent)
        }

    }

    private fun setData() {

        if (goods == null) {
            return
        }
        inquire_result_detail_txt_box_name.text = goods?.box_name
        inquire_result_detail_txt_type.text = goods?.getBoxTypeShow()
        inquire_result_detail_txt_goods_no.text = goods?.barcode
        inquire_result_detail_txt_num.text = goods?.qty.toString()
        inquire_result_detail_txt_state.text = goods?.getStockState()
        inquire_result_detail_txt_sku.text = goods?.skuNo
        inquire_result_detail_txt_item_no.text = goods?.itemNo
        inquire_result_detail_txt_sales_attribute.text = goods?.goodsAttribute?.show()
        inquire_result_detail_txt_product_name.text = goods?.skuTitle

        ImageUtils.loadImageNew(inquire_result_detail_img, goods?.sku_image_url, R.mipmap.logo)
    }


}
