package club.fromfactory.ui.wms_signing.viewholder

import android.view.ViewGroup
import club.fromfactory.R
import club.fromfactory.baselibrary.utils.DateUtil
import club.fromfactory.baselibrary.utils.StringUtils
import club.fromfactory.baselibrary.view.recyclerview.BaseViewHolder
import club.fromfactory.ui.wms_signing.model.OrderDetailInfo
import kotlinx.android.synthetic.main.signing_task_item.*

class SigningTaskViewHolder(parent:ViewGroup) :BaseViewHolder<OrderDetailInfo>(parent, R.layout.signing_task_item) {

    override fun bindData(data: OrderDetailInfo) {
        super.bindData(data)

        signing_task_item_txt_task_name.text = data.name
        signing_task_item_txt_signed_num.text = data.signCount.toString()
        signing_task_item_txt_create_user.text = data.creatorName
//        signing_task_item_txt_start_time.text = data.createTime

        val startTime = data.startTime

        val dateMills = DateUtil.utc2Local( startTime?: "", DateUtil.YYYY_MM_DD_T_Z)

        signing_task_item_txt_start_time.text = if (StringUtils.isNull(startTime)) "" else dateMills

    }
}