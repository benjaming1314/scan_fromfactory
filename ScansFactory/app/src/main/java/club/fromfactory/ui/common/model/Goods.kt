package club.fromfactory.ui.common.model

import club.fromfactory.FFApplication
import club.fromfactory.R
import club.fromfactory.baselibrary.model.NoProguard
import club.fromfactory.baselibrary.utils.StringUtils
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Goods : Serializable, NoProguard {

    companion object {
        const val SCAN_STATE_SUCCEED = 1
        const val SCAN_STATE_FAILED = 2
        const val SCAN_STATE_DEFAULT = 0

        fun getBarcodeList(barcode: String) : List<String>{
            val arrayList = ArrayList<String>()
            arrayList.add(barcode)
            return arrayList
        }

    }

    @SerializedName("sku_attribute")
    var goodsAttribute : GoodsAttribute ? = null

    @SerializedName("sku_name")
    var skuName: String? = null
    @SerializedName("sku_id")
    var sku_id: Int? = null

    @SerializedName("id")
    var id: Int = -1

    @SerializedName("box_name")
    var box_name: String? = null

    @SerializedName("barcode")
    var barcode: String? = null
    @SerializedName("sku_image_url")
    var sku_image_url: String? = null

    @SerializedName("status")
    var status: String? = null

    @SerializedName("qty")
    var qty: Int = 0

    @SerializedName("barcode_list")
    var barcodeList: List<String> ? = null

    var scanState: Int = SCAN_STATE_DEFAULT

    @SerializedName("can_alter")
    var isAlter: Boolean ?= true // 状态知否可修改

    //新增
    @SerializedName("box_type")
    var boxType: Int? = null
    @SerializedName("stock_status")
    var stockStatus: String? = null
    @SerializedName("item_no")
    var itemNo: String? = null
    @SerializedName("sku_no")
    var skuNo: String? = null
    @SerializedName("sku_title")
    var skuTitle: String? = null

    @SerializedName("container_code")
    var containerCode: String? = null


    fun getBarcodeShow() :String {
        
        if (barcode != null) {
            return barcode!!
        }
        if (barcodeList != null && barcodeList?.isNotEmpty()!!) {
            return barcodeList!![0]
        }

        return ""


    }



    fun getBarCodeShowStr() : String{

        var barcodeListStr = ""

        if (barcodeList != null) {
            for (i in 0 until barcodeList!!.size) {
                barcodeListStr += barcodeList!![i]
                if (i != barcodeList!!.size - 1) {
                    barcodeListStr += "\n\n"
                }
            }
        }
        if (StringUtils.isNull(barcodeListStr)) {
            barcodeListStr = barcode?:""
        }

        return barcodeListStr
    }
    fun getBarCodeList() : List<String>{

        if (barcodeList != null) {
            return barcodeList!!
        }

        var barcodeListResult = ArrayList<String>()
        barcodeListResult.add(barcode?:"")

        return barcodeListResult
    }
    //  TO_SHELVE: '待上架',
    //        SHELVED: '已在架',
    //        TO_OFF_SHELF: '待下架',
    //        OCCUPIED: '已占用',  # 闲置库存专有的状态
    //        UNALLOCATED: '无库位',   # 闲置库存专有的状态
    fun getStockState() :String {

        var str = ""

        when(stockStatus){

            "TO_SHELVE" ->{
                str = FFApplication.getInstance().resources.getString(R.string.to_shelve)
            }
            "SHELVED" ->{
                str = FFApplication.getInstance().resources.getString(R.string.inquire_shelved)
            }
            "TO_OFF_SHELF" ->{
                str = FFApplication.getInstance().resources.getString(R.string.to_off_shelf)
            }
            "OCCUPIED" ->{
                str = FFApplication.getInstance().resources.getString(R.string.occupied)
            }
            "UNALLOCATED" ->{
                str = FFApplication.getInstance().resources.getString(R.string.unallocated)
            }

        }

        return str

    }

    //    BOX_ORDER = 0 # 订单库位
    //    BOX_SKU = 1 # 闲置库位
    //    BOX_FROZEN = 2 # 冻结库位
    //    BOX_DAMAGE = 3 # 残次品库位
    fun getBoxTypeShow() : String{
        var str = ""

        when(boxType){

            0->{
                str = FFApplication.getInstance().resources.getString(R.string.box_order)
            }
            1->{
                str = FFApplication.getInstance().resources.getString(R.string.box_sku)
            }
            2->{
                str = FFApplication.getInstance().resources.getString(R.string.box_frozen)
            }
            3->{
                str = FFApplication.getInstance().resources.getString(R.string.box_damage)
            }


        }

        return str
    }

    override fun toString(): String {
        return "Goods(goodsAttribute=$goodsAttribute, skuName=$skuName, sku_id=$sku_id, id=$id, box_name=$box_name, barcode=$barcode, sku_image_url=$sku_image_url, status=$status, qty=$qty, barcodeList=$barcodeList, scanState=$scanState, isAlter=$isAlter)"
    }

}