package club.fromfactory.ui.wms_signing.presenter

import club.fromfactory.baselibrary.language.LanguageUtils
import club.fromfactory.baselibrary.model.EmptyResponse
import club.fromfactory.baselibrary.net.CookieHelper
import club.fromfactory.baselibrary.net.retrofit.BaseRetrofit
import club.fromfactory.baselibrary.presenter.BasePresenter
import club.fromfactory.baselibrary.rx.NetObserver
import club.fromfactory.baselibrary.rx.loadingView
import club.fromfactory.baselibrary.utils.PreferenceStorageUtils
import club.fromfactory.baselibrary.view.RxAppCompatActivity
import club.fromfactory.constant.Constants
import club.fromfactory.http.NetUtils
import club.fromfactory.ui.wms_signing.IWmsSigningApi
import club.fromfactory.ui.wms_signing.contract.WmsSigningDetailContract
import club.fromfactory.ui.wms_signing.model.SignPackageResult
import com.trello.rxlifecycle2.kotlin.bindToLifecycle
import java.util.HashMap

class WmsSigningDetailPreseter(view : WmsSigningDetailContract.View) :BasePresenter<WmsSigningDetailContract.View>(view) ,WmsSigningDetailContract.Presenter {

    private var SIGN_AWB_ORDER_URL = NetUtils.APP_MAIN_URL + "wms-center/pda/sign-order/"
    private var COMPLETE_SIGN_ORDER_URL = NetUtils.APP_MAIN_URL + "wms-center/pda/sign-order/"

    override fun signAwb(awb: String) {
        val token = PreferenceStorageUtils.getInstance().token

        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        val params = HashMap<String, Any>()
        params["awb"] = awb

        BaseRetrofit
                .createService(IWmsSigningApi::class.java)
                .signAwb(SIGN_AWB_ORDER_URL+view.getSignOrderId()+"/line",headerMap,params)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : NetObserver<SignPackageResult>() {
                    override fun onSuccess(t: SignPackageResult?) {
                        if (t!= null) {
                            view.requestSignAwbSucceed(t)

                        }else {
                            view.requestSignAwbFailed("no data")
                        }
                    }

                    override fun onFailure(message: String) {
                        view.requestSignAwbFailed(message)
                    }
                })

    }


    override fun completeTask() {

        val token = PreferenceStorageUtils.getInstance().token

        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)


        BaseRetrofit
                .createService(IWmsSigningApi::class.java)
                .completeSignOrder(COMPLETE_SIGN_ORDER_URL+view.getSignOrderId()+"/complete",headerMap)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : NetObserver<EmptyResponse>() {
                    override fun onSuccess(t: EmptyResponse?) {
                        view.requestCompleteTaskSuccess()
                    }

                    override fun onFailure(message: String) {
                        view.requestFailed(message)
                    }
                })
    }
}