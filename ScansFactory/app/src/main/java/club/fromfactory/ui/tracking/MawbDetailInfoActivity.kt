package club.fromfactory.ui.tracking

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.View
import club.fromfactory.FFApplication
import club.fromfactory.R
import club.fromfactory.baselibrary.utils.DensityUtils
import club.fromfactory.baselibrary.utils.DoubleUtils
import club.fromfactory.baselibrary.utils.StringUtils
import club.fromfactory.baselibrary.utils.ToastUtils
import club.fromfactory.baselibrary.view.recyclerview.BaseSpaceItemDecoration
import club.fromfactory.baselibrary.widget.CustomDialog
import club.fromfactory.ui.main.ScanBaseMVPActivity
import club.fromfactory.ui.tracking.adapter.PackageInfoAdapter
import club.fromfactory.ui.tracking.contract.MawbDetailInfoContract
import club.fromfactory.ui.tracking.model.MawbInfo
import club.fromfactory.ui.tracking.model.PackageInfo
import club.fromfactory.ui.tracking.presenter.MawbDetailInfoPresenter
import club.fromfactory.ui.tracking.view.PackageInfoInterface
import club.fromfactory.utils.DialogUtils
import club.fromfactory.utils.PromptDialogUtils
import club.fromfactory.utils.TipSound
import club.fromfactory.widget.CustomTitleLinearLayout
import kotlinx.android.synthetic.main.activity_tracking_info.*
import kotlinx.android.synthetic.main.tracking_info.*

class MawbDetailInfoActivity : ScanBaseMVPActivity<MawbDetailInfoContract.Presenter>(), MawbDetailInfoContract.View {
    override fun createPresenter(): MawbDetailInfoContract.Presenter = MawbDetailInfoPresenter(this)

    private var mTrackingParcelInfoAdapter: PackageInfoAdapter? = null

    private var customDialog: CustomDialog? = null
    private var mMawbId: Long? = -1

    private var isStatusComplete: Boolean = false

    private var mMawbInfo: MawbInfo? = null

    private var mDeletePosition = -1

    private var isChange = false

    private var mTitle: String? = null

    private var mCurrentBag = 0

    companion object {
        const val INTENT_DATA = "intent_data"

        fun launchActivity(context: Context, trackingInfo: MawbInfo, requestCode: Int) {
            val intent = Intent()
            intent.setClass(context, MawbDetailInfoActivity::class.java)
            intent.putExtra(INTENT_DATA, trackingInfo)
            (context as Activity).startActivityForResult(intent, requestCode)
        }
    }

    override fun getLayoutResId(): Int = R.layout.activity_tracking_info

    override fun initData() {
        super.initData()
        var mTrackingInfo = intent.getSerializableExtra(INTENT_DATA) as MawbInfo?
        mMawbId = mTrackingInfo?.mawbId
        mTitle = mTrackingInfo?.mawbNo
        isStatusComplete = mTrackingInfo?.isStatusComplete() ?: false
        if (mMawbId ?: -1 < 0) {
            ToastUtils.show("data error")
            finish()
        }

    }

    override fun initView() {
        super.initView()

        tracking_info_ctl_title.setTitleCenter(mTitle)
        tracking_info_ctl_title.setListener(object : CustomTitleLinearLayout.CustomTitleListener() {

            override fun clickLeft() {
                super.clickLeft()
                finishActivity(false)
            }
        })

        mTrackingParcelInfoAdapter = PackageInfoAdapter()
        tracking_info_recycler_view.addItemDecoration(BaseSpaceItemDecoration(0, 0, 0, DensityUtils.dp2px(this, 1)))
        tracking_info_recycler_view.adapter = mTrackingParcelInfoAdapter
        tracking_info_btn_status.isClickable = false


        mTrackingParcelInfoAdapter?.setOnItemViewClickListener(object : PackageInfoInterface {
            override fun onItemViewClick(data: PackageInfo?, clickedView: View?, position: Int) {
            }

            override fun deletePackageInfo(position: Int, data: PackageInfo) {
                mDeletePosition = position
                PromptDialogUtils.getInstance().showDialogDefine(this@MawbDetailInfoActivity, resources.getString(R.string.unbind_package_mawb), object : PromptDialogUtils.DialogCallback {
                    override fun onClickPositiveBtn() {
                        presenter.mawbUnBindPackage(data.packageNo?:"")
                    }

                    override fun onClickNegativeBtn() {
                    }

                })

            }
        })

        setListener()
    }

    override fun fetchData() {
        super.fetchData()
        presenter.getMawbDetailInfo()
    }

    private var handoverComfirmDialog :CustomDialog ? = null


    private fun setListener() {

        setScanListener(tracking_info_scan)

        tracking_info_btn_status.setOnClickListener {

            if (tracking_info_btn_status.isSelected) {

                handoverComfirmDialog = DialogUtils.handoverComfirmDialog(this,mCurrentBag,object :DialogUtils.DialogUtilsCallback{
                    override fun onClickPositiveBtn() {
                        PromptDialogUtils.getInstance().showDialogDefine(this@MawbDetailInfoActivity, resources.getString(R.string.complete_handover_prompt), object : PromptDialogUtils.DialogCallback {
                            override fun onClickPositiveBtn() {
                                presenter.transferConfirm()
                            }

                            override fun onClickNegativeBtn() {
                            }

                        })
                    }

                    override fun onClickNegativeBtn() {
                    }

                })
            }

        }
        tracking_info_txt_ok.setOnClickListener {
            val parcelNo = tracking_info_et_tracking_no.text.toString()

            if (StringUtils.isNotBlank(parcelNo)) {
                requestScanResult(parcelNo)
            } else {

            }
        }
    }

    @SuppressLint("SetTextI18n")
    override fun refreshMawbDetailInfo(mawbInfo: MawbInfo?) {
        if (mawbInfo == null) {
            return
        }

        mMawbInfo = mawbInfo

        val airlineName = mawbInfo.airlineName

        val freightForwardingCompanyName = mawbInfo.freightForwardingCompanyName
        val destinationPortName = mawbInfo.destinationPortName

        val clearanceModelName = mawbInfo.clearanceModelName

        if (StringUtils.isNotBlank(freightForwardingCompanyName)) {
            tracking_info_txt_freightForwardingCompany.text = freightForwardingCompanyName
        } else {
            tracking_info_txt_freightForwardingCompany.text = FFApplication.getInstance().resources.getString(R.string.no_valus)
        }
        if (StringUtils.isNotBlank(airlineName)) {
            tracking_info_txt_airline.text = airlineName
        } else {
            tracking_info_txt_airline.text = FFApplication.getInstance().resources.getString(R.string.no_valus)
        }
        if (StringUtils.isNotBlank(destinationPortName)) {
            tracking_info_txt_destinationPort.text = destinationPortName
        } else {
            tracking_info_txt_destinationPort.text = FFApplication.getInstance().resources.getString(R.string.no_valus)
        }

        tracking_info_txt_plan_weight.text = mawbInfo.planWeight.toString() + "kg"
        tracking_info_txt_current_wight.text = mawbInfo.totalWeight.toString() + "kg"
        tracking_info_txt_type.text = if (mawbInfo.isHasSpaciesSku()) resources.getString(R.string.sku_type_has_special) else resources.getString(R.string.sku_type_prompt_costom)
        tracking_info_txt_mode.text = if (StringUtils.isNotBlank(clearanceModelName)) clearanceModelName else ""

        val status: Boolean = if (mawbInfo.status == null) {
            isStatusComplete
        } else {
            mawbInfo.isStatusComplete()
        }

        tracking_info_btn_status.text = if (status) FFApplication.getInstance().resources.getString(R.string.processing_tracking_complete) else FFApplication.getInstance().resources.getString(R.string.processing_tracking_complete_handover)
        tracking_info_btn_status.isSelected = !status
        tracking_info_btn_status.isClickable = !status

        tracking_info_scan.visibility = if (status) View.GONE else View.VISIBLE

        val packageList = mawbInfo.packageList

        mCurrentBag = packageList?.size?:0
        updateCurrentPackageNum()

        mTrackingParcelInfoAdapter?.clear()
        if (packageList != null) {
            mTrackingParcelInfoAdapter?.addAll(packageList)
        }

    }


    @SuppressLint("SetTextI18n", "StringFormatInvalid")
    private fun updateCurrentPackageNum() {
        tracking_info_txt_num.text = mCurrentBag.toString()
    }

    @SuppressLint("SetTextI18n")
    private fun updateCurrentWright(weight: Double, isBindPackage: Boolean) {

        try {

            if (isBindPackage) {
                mCurrentBag++
                updateCurrentPackageNum()
            }else {
                mCurrentBag--
                updateCurrentPackageNum()
            }

            val planWeight = mMawbInfo?.planWeight ?: 0.0
            val totalWeight = if (isBindPackage) {
                DoubleUtils.sum((mMawbInfo?.totalWeight ?: 0.0), weight)
            } else {
                DoubleUtils.sub((mMawbInfo?.totalWeight ?: 0.0), weight)
            }

            mMawbInfo?.totalWeight = totalWeight
            tracking_info_txt_current_wight.text = "$totalWeight kg"

            if (planWeight < totalWeight) {
                TipSound.getInstance().playJinglingdSound(this@MawbDetailInfoActivity)
                showFailedErrorWindow(resources.getString(R.string.plan_weight_out))
            }

        } catch (e: Exception) {
        }

    }


    override fun clickKeyCodeBack() {
        finishActivity(false)
    }

    override fun requestScanStart() {

    }

    override fun requestScanResult(result: String?) {

        if (!tracking_info_btn_status.isSelected || customDialog?.isShowing == true) {
            return
        }

        if (StringUtils.isNotBlank(result)) {
            presenter.mawbBindPackage(result ?: "")
        } else {
            ToastUtils.show(resources.getString(R.string.scan_again))
        }
    }

    override fun getMawbId(): Long = mMawbId ?: 0

    override fun mawbBindPackageSuccess(mPackageInfo: PackageInfo?) {
        isChange = true
        if (mPackageInfo != null) {
            updateCurrentWright(mPackageInfo.packageWeight?:0.0, true)
            mTrackingParcelInfoAdapter?.insert(mPackageInfo,0)
            TipSound.getInstance().playScanSucceedSound(this@MawbDetailInfoActivity)
        } else {
            ToastUtils.show(FFApplication.getInstance().resources.getString(R.string.scan_again))
        }

    }

    override fun mawbUnBindPackageSuccess() {
        isChange = true
        val data = mTrackingParcelInfoAdapter?.data
        if (data != null) {
            val packageInfo = data[mDeletePosition]
            if (packageInfo != null) {
                updateCurrentWright(packageInfo.packageWeight?:0.0, false)
            }
        }

        TipSound.getInstance().playScanSucceedSound(this@MawbDetailInfoActivity)
        mTrackingParcelInfoAdapter?.remove(mDeletePosition)
        tracking_info_recycler_view.closeMenu()
    }

    override fun transferConfirmSuccess() {
        isChange = true
        finishActivity(true)
    }

    override fun transferConfirmIgnoreOption() {

        PromptDialogUtils.getInstance().showDialogDefine(this, resources.getString(R.string.complete_handover_prompt_ignore), resources.getString(R.string.btn_yes), null, null)
    }

    override fun showFailedError(message: String?) {
        ToastUtils.show(message?:"")
    }

    override fun showFailedErrorWindow(message: String?) {
        TipSound.getInstance().playScanFailedSound(this@MawbDetailInfoActivity)
        customDialog = PromptDialogUtils.getInstance().getPromptDialog(this@MawbDetailInfoActivity, message)
        if (customDialog != null && isAlive) {
            customDialog?.show()
        }
    }

    override fun showLoadingView() {
        showLoadingView(tracking_info_progress)
    }

    override fun hideLoadingView() {
        hideLoadingView(tracking_info_progress)
    }

    private fun finishActivity(isComplete: Boolean) {

        if (isChange) {
            if (!isComplete && mMawbInfo != null) {
                val intent = Intent()
                intent.putExtra(INTENT_DATA, mMawbInfo)
                setResult(Activity.RESULT_OK, intent)
            } else {
                setResult(Activity.RESULT_OK)
            }
        }
        finish()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (handoverComfirmDialog != null) {
            if (handoverComfirmDialog?.isShowing == true) {
                handoverComfirmDialog?.dismiss()
            }

            handoverComfirmDialog = null
        }
    }

}
