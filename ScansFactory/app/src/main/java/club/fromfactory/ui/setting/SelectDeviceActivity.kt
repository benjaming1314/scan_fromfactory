package club.fromfactory.ui.setting

import android.app.Activity
import android.content.Intent
import club.fromfactory.R
import club.fromfactory.app_config.DeviceManager
import club.fromfactory.baselibrary.utils.DeviceUtils
import club.fromfactory.baselibrary.utils.StringUtils
import club.fromfactory.baselibrary.view.BaseActivity
import club.fromfactory.ui.setting.adapter.DeviceAdapter
import club.fromfactory.widget.CustomTitleLinearLayout
import kotlinx.android.synthetic.main.activity_select_device.*

class SelectDeviceActivity : BaseActivity() {


    companion object {
        const val IS_SHOW_BACK = "is_show_back"

        fun launchActivity(activity: BaseActivity ,isShowBack : Boolean,requestCode :Int) {

            val intent = Intent(activity,SelectDeviceActivity::class.java)
            intent.putExtra(IS_SHOW_BACK,isShowBack)
            activity.startActivityForResult(intent,requestCode)
        }
    }

    private var isShowBack = false

    private var mDeviceAdapter : DeviceAdapter? =null

    override fun getLayoutResId(): Int = R.layout.activity_select_device


    override fun initData() {
        super.initData()
        isShowBack = intent.getBooleanExtra(IS_SHOW_BACK,false)

    }

    override fun initView() {
        super.initView()

        select_device_ctl_title.setLeftTextVisible(isShowBack)
        select_device_ctl_title.setListener(object : CustomTitleLinearLayout.CustomTitleListener() {

            override fun clickLeft() {
                super.clickLeft()
                finish()
            }
        })


        select_device_current_device.setOnClickListener {


            if (isShowBack) {
                finish()
            }else {


            }

        }
        val phoneModel = DeviceUtils.getPhoneModel().replace(" ", "").toLowerCase()
        val currentTypeName = DeviceManager.getInstance().getCurrentTypeName(phoneModel)


        var tips = resources.getString(R.string.select_device_tips_1, phoneModel) + "  "


        tips += if (StringUtils.isNull(currentTypeName)) {
            resources.getString(R.string.select_device_tips_2)
        }else {
            resources.getString(R.string.select_device_tips_3,currentTypeName)
        }

        select_device_txt_tips.text = tips

        select_device_txt_current_device.text = DeviceManager.getInstance().currentTypeName

        mDeviceAdapter = DeviceAdapter()

        select_device_recycler_view.adapter = mDeviceAdapter

        mDeviceAdapter?.setOnItemViewClickListener { data, clickedView, position ->

            DeviceManager.getInstance().checkDeviceType(this@SelectDeviceActivity,data?.type,object : DeviceManager.CheckPdaTypeListener{
                override fun checkResult(isGo: Boolean) {
                    if (isGo) {

                        val changeDevice = DeviceManager.getInstance().changeDevice(data)
                        if (changeDevice) {
                            setResult(Activity.RESULT_OK)
                        }
                        finish()
                    }
                }

            })

        }

        mDeviceAdapter?.addAll(DeviceManager.getInstance().deviceInfoList)

    }


    override fun clickKeyCodeBack() {
        select_device_current_device.callOnClick()
    }
}
