package club.fromfactory.ui.common.viewholder

import android.view.View
import android.view.ViewGroup
import club.fromfactory.R
import club.fromfactory.baselibrary.view.recyclerview.BaseViewHolder
import club.fromfactory.ui.main.model.PermissionModel
import kotlinx.android.synthetic.main.main_item.*

class PermissionModelViewHolder(parent :ViewGroup) :BaseViewHolder<PermissionModel>(parent, R.layout.main_item) {

    override fun bindData(data: PermissionModel) {
        super.bindData(data)
        val unread = data.unread
        val modelName = data.modelName

        if (unread > 0) {
            main_item_number.visibility = View.VISIBLE
            main_item_txt_number.text = unread.toString()
        } else {
            main_item_number.visibility = View.GONE
        }
        main_item_content.text = modelName
    }
}