package club.fromfactory.ui.moveout.contract

import club.fromfactory.baselibrary.presenter.IPresenter
import club.fromfactory.baselibrary.view.IBaseMVPView
import club.fromfactory.ui.moveout.model.OffShelfTaskResponse

class MoveOutConfirmContract {

    interface View :IBaseMVPView<Presenter> {

        fun refreshData(offShelfTaskResponse: OffShelfTaskResponse)

        fun showFailedError(message: String?)

    }
    interface Presenter :IPresenter<View> {

        fun getOffShelfTask()
    }
}