package club.fromfactory.ui.spare_parts.viewholder

import android.view.ViewGroup
import club.fromfactory.R
import club.fromfactory.baselibrary.view.recyclerview.BaseViewHolder
import kotlinx.android.synthetic.main.spare_parts_goods_item.*

class SparePartsGoodsListViewHolder(parent:ViewGroup):BaseViewHolder<String>(parent, R.layout.spare_parts_goods_item) {

    override fun bindData(data: String, position: Int) {
        super.bindData(data, position)

        if (position == 0) {
            spare_parts_goods_item_name.paint.isFakeBoldText = true
            spare_parts_goods_item_name.textSize = 20F
        }else {
            spare_parts_goods_item_name.paint.isFakeBoldText = false
            spare_parts_goods_item_name.textSize = 16F
        }
        spare_parts_goods_item_name.text = data

    }
}