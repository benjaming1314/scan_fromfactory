package club.fromfactory.ui.inventory_conversion.conversion_move_in

import android.app.Activity
import android.content.Intent
import club.fromfactory.R
import club.fromfactory.baselibrary.router.RouterConstants
import club.fromfactory.baselibrary.utils.StringUtils
import club.fromfactory.baselibrary.utils.ToastUtils
import club.fromfactory.baselibrary.view.BaseActivity
import club.fromfactory.baselibrary.widget.CustomDialog
import club.fromfactory.routerannotaions.Router
import club.fromfactory.ui.inventory_conversion.conversion_move_in.contract.ConversionMoveInContainerContract
import club.fromfactory.ui.inventory_conversion.conversion_move_in.presenter.ConversionMoveInContainerPresenter
import club.fromfactory.ui.inventory_conversion.conversion_move_out.model.ConversionTaskInfo
import club.fromfactory.ui.main.ScanBaseMVPActivity
import club.fromfactory.utils.EditTextUtils
import club.fromfactory.utils.PromptDialogUtils
import club.fromfactory.utils.TipSound
import club.fromfactory.widget.CustomTitleLinearLayout
import kotlinx.android.synthetic.main.activity_conversion_move_in_container.*

/**
 * 转换上架 容器确认页面
 */
@Router(RouterConstants.CONVERSION_MOVE_IN_CONTAINER)
class ConversionMoveInContainerActivity : ScanBaseMVPActivity<ConversionMoveInContainerContract.Presenter>(), ConversionMoveInContainerContract.View {
    override fun createPresenter(): ConversionMoveInContainerContract.Presenter = ConversionMoveInContainerPresenter(this)

    companion object {
        private const val TYPE_WAITING_PUT = "waiting_put"

        private const val REQUEST_CODE_DETAIL = 100
    }

    private var customDialog: CustomDialog? = null
    private var isHandleScanResult = false

    override fun requestScanStart() {

//        if (Zlog.isDebug) {
//            requestScanResult("M0001")
//
//        }
    }

    override fun clickKeyCodeBack() {
        finish()
    }

    override fun getLayoutResId(): Int = R.layout.activity_conversion_move_in_container


    override fun initData() {
        super.initData()
    }

    override fun initView() {
        super.initView()

        val title = resources.getString(R.string.inventory_conversion_move_in) + ":" + resources.getString(R.string.container_judge)
        conversion_move_in_container_ctl_title.setTitleCenter(title)
        conversion_move_in_container_ctl_title.setListener(object : CustomTitleLinearLayout.CustomTitleListener() {

            override fun clickLeft() {
                super.clickLeft()
                finish()
            }
        })

        setScanListener(conversion_move_in_container_scan)

        conversion_move_in_container_btn_ok.setOnClickListener {
            jumgeContainer()
        }
        EditTextUtils.addEditTextListener(conversion_move_in_container_edt_bind_container)
    }


    override fun requestScanResult(result: String?) {

        if (isHandleScanResult) {
            return
        } else {
            isHandleScanResult = true
        }

        if (StringUtils.isNotBlank(result)) {
            conversion_move_in_container_edt_bind_container.setText(result)
        }

        jumgeContainer()
    }

    private fun jumgeContainer() {

        val toString = conversion_move_in_container_edt_bind_container.text.toString()

        if (StringUtils.isNotBlank(toString)) {
            presenter.getConversionTask(TYPE_WAITING_PUT, toString)
        } else {
            isHandleScanResult = false
            ToastUtils.show(resources.getString(R.string.scan_prompt_no_container))
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {

            conversion_move_in_container_edt_bind_container.setText("")
        }

    }

    override fun requestTaskSuccess(conversionTaskInfo: ConversionTaskInfo) {
        isHandleScanResult = false
        TipSound.getInstance().playScanSucceedSound(this)
        ConversionMoveInDetailActivity.launchActivity(this, conversionTaskInfo, REQUEST_CODE_DETAIL)

    }

    override fun requestFailed(message: String?) {
        TipSound.getInstance().playScanFailedSound(this)
        isHandleScanResult = false
        customDialog = PromptDialogUtils.getInstance().getPromptDialog(this@ConversionMoveInContainerActivity, message
                ?: resources.getString(R.string.net_error_prompt_one))
        if (customDialog != null && isAlive) {
            customDialog?.show()
        }
    }

    override fun showLoadingView() {
        showLoadingView(conversion_move_in_container_progress)
    }

    override fun hideLoadingView() {
        hideLoadingView(conversion_move_in_container_progress)
    }


    override fun onDestroy() {
        super.onDestroy()
        if (customDialog != null && customDialog?.isShowing == true && isAlive) {
            customDialog?.dismiss()
            customDialog = null
        }
    }

}
