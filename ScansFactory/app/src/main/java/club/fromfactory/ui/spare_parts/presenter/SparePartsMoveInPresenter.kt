package club.fromfactory.ui.spare_parts.presenter

import club.fromfactory.baselibrary.language.LanguageUtils
import club.fromfactory.baselibrary.model.EmptyResponse
import club.fromfactory.baselibrary.net.CookieHelper
import club.fromfactory.baselibrary.net.retrofit.BaseRetrofit
import club.fromfactory.baselibrary.presenter.BasePresenter
import club.fromfactory.baselibrary.rx.NetObserver
import club.fromfactory.baselibrary.rx.loadingView
import club.fromfactory.baselibrary.utils.PreferenceStorageUtils
import club.fromfactory.baselibrary.view.RxAppCompatActivity
import club.fromfactory.constant.Constants
import club.fromfactory.http.NetUtils
import club.fromfactory.ui.spare_parts.SparePartsApi
import club.fromfactory.ui.spare_parts.contract.SparePartsMoveInContract
import com.trello.rxlifecycle2.kotlin.bindToLifecycle
import java.util.HashMap

class SparePartsMoveInPresenter(view: SparePartsMoveInContract.View):BasePresenter<SparePartsMoveInContract.View>(view),SparePartsMoveInContract.Presenter {


    private var shelfBoxUrl = NetUtils.APP_MAIN_URL + "wms-center/pda/inventory-move/shelve/box"
    private var shelfBarcodeUrl = NetUtils.APP_MAIN_URL + "wms-center/pda/inventory-move/shelve/barcode"
    private var shelfAllUrl = NetUtils.APP_MAIN_URL + "wms-center/pda/inventory-move/shelve/all"

    override fun shelfBox(boxName:String) {

        val token = PreferenceStorageUtils.getInstance().token
        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        val params = HashMap<String, Any>()
        params["boxName"] = boxName

        BaseRetrofit
                .createService(SparePartsApi::class.java)
                .shelfBox(shelfBoxUrl, headerMap,params)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : NetObserver<EmptyResponse>() {
                    override fun onSuccess(t: EmptyResponse?) {
                        view?.shelfBoxSuccess()
                    }

                    override fun onFailure(message: String) {
                        view.requestFailed(message)
                    }
                })
    }

    override fun shelfBarcode(boxName:String,barcode: String) {

        val token = PreferenceStorageUtils.getInstance().token
        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        val params = HashMap<String, Any>()
        params["boxName"] = boxName
        params["barcode"] = barcode

        BaseRetrofit
                .createService(SparePartsApi::class.java)
                .shelfBarcode(shelfBarcodeUrl, headerMap,params)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : NetObserver<EmptyResponse>() {
                    override fun onSuccess(t: EmptyResponse?) {
                        view.shelfBarcodeSuccess()
                    }

                    override fun onFailure(message: String) {
                        view.requestFailed(message)
                    }
                })
    }

    override fun shelfAll(boxName:String, barcodeList: List<String>) {
        val token = PreferenceStorageUtils.getInstance().token
        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        val params = HashMap<String, Any>()
        params["boxName"] = boxName
        params["barcodeList"] = barcodeList

        BaseRetrofit
                .createService(SparePartsApi::class.java)
                .shelfAll(shelfAllUrl, headerMap,params)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : NetObserver<EmptyResponse>() {
                    override fun onSuccess(t: EmptyResponse?) {
                        view.shelfAllSuccess()
                    }

                    override fun onFailure(message: String) {
                        view.requestFailed(message)
                    }
                })
    }
}