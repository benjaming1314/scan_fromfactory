package club.fromfactory.ui.inventory_inquire.presenter

import club.fromfactory.baselibrary.presenter.BasePresenter
import club.fromfactory.ui.inventory_inquire.contract.InquireContract

class InquirePresenter(view : InquireContract.View) :BasePresenter<InquireContract.View>(view),InquireContract.Presenter {
}