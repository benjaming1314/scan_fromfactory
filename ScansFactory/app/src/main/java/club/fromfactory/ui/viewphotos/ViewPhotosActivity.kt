package club.fromfactory.ui.viewphotos

import android.support.v4.view.ViewPager
import android.text.TextUtils
import android.util.SparseArray
import android.view.View
import club.fromfactory.R
import club.fromfactory.baselibrary.utils.decode
import club.fromfactory.baselibrary.view.BaseActivity
import club.fromfactory.ui.viewphotos.adapters.PhotoPageAdapter
import club.fromfactory.widget.CustomTitleLinearLayout
import kotlinx.android.synthetic.main.activity_view_photos.*

/**
 * 查看图片界面
 */
class ViewPhotosActivity : BaseActivity(), ViewPhotosContract.View {

    var images: String? = null
    var index: Int = 0
    private val imageList = ArrayList<String>()
    /**
     * 图片状态列表
     */
    private val imageStatusList = SparseArray<Boolean>()

    override fun clickKeyCodeBack() {
        finish()
    }

    override fun getLayoutResId(): Int {
        return R.layout.activity_view_photos
    }

    override fun initData() {
        super.initData()
        images = intent.getStringExtra("images")
        index = intent.getIntExtra("index",0)

        parseImagesParam()
    }


    // 解析images参数
    private fun parseImagesParam() {
        if (!TextUtils.isEmpty(images)) {
            decode(images)
                    .split(",")
                    .filter { !TextUtils.isEmpty(it) }
                    .forEach {
                        imageList.add(it)
                    }
        }

        // 如果当前没有可以展示的图片，直接关闭当前页面
        if (imageList.isEmpty()) {
            finish()
        }

    }

    override fun initView() {
        val photoPageAdapter = PhotoPageAdapter(this)

        photoPageAdapter.addAll(imageList)
        view_photes_view_pager.adapter = photoPageAdapter
        photoPageAdapter.notifyDataSetChanged()


        if (index > 0 && index < imageList.size) {
            view_photes_view_pager.currentItem = index
        }
        initTitleLayout()
        showLoading()
        view_photes_view_pager.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
                index = position
                updateTitle((position + 1).toString() + "/" + imageList.size)
                if (imageStatusList.indexOfKey(position) > -1
                        && imageStatusList.get(position)) {
                    hideLoading()
                } else {
                    showLoading()
                }
            }
        })

    }

    private fun initTitleLayout() {
        view_photes_title_layout.setListener(object : CustomTitleLinearLayout.CustomTitleListener() {
            override fun clickLeft() {
                finish()
            }
        })
        updateTitle((index + 1).toString() + "/" + imageList.size)
    }

    private fun updateTitle(title: String) {
        view_photes_title_layout.setTitleCenter(title)
    }


    private fun showLoading() {
        view_photes_progress.visibility = View.VISIBLE
        view_photes_progress.animate()
    }

    private fun hideLoading() {
        view_photes_progress.visibility = View.GONE
    }

    override fun updateImageStatus(position: Int, downloaded: Boolean) {
        imageStatusList.put(position, downloaded)
        // 当前展示的元素和展示的位置相同，关闭loading效果
        if (view_photes_view_pager.currentItem == position) {
            hideLoading()
        }
    }

}
