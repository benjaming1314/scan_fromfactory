package club.fromfactory.ui.setting.viewholder

import android.annotation.SuppressLint
import android.view.ViewGroup
import club.fromfactory.R
import club.fromfactory.app_config.model.DeviceInfo
import club.fromfactory.baselibrary.view.recyclerview.BaseViewHolder
import kotlinx.android.synthetic.main.device_item.*

class DeviceViewHolder(parent: ViewGroup) : BaseViewHolder<DeviceInfo>(parent, R.layout.device_item) {


    @SuppressLint("SetTextI18n")
    override fun bindData(data: DeviceInfo) {
        super.bindData(data)

        var deviceType = ""

        val deviceTypeList = data.deviceTypeList
        val size = deviceTypeList.size


        for (i in 0 until size) {

            deviceType += deviceTypeList[i]

            if (i != size - 1) {
                deviceType += "/"
            }

        }

        device_txt_device.text = data.name + "(" + deviceType + ")"

    }
}