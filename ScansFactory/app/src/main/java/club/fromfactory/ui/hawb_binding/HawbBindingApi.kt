package club.fromfactory.ui.hawb_binding

import club.fromfactory.baselibrary.model.EmptyResponse
import club.fromfactory.pojo.HttpJsonResult
import club.fromfactory.ui.hawb_binding.model.HawbBindingResult
import club.fromfactory.ui.hawb_binding.model.HawbDetailInfo
import io.reactivex.Observable
import retrofit2.http.*

interface HawbBindingApi {

    /**
     */
    @GET
    fun getHawbDetail(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>): Observable<HawbDetailInfo>

    /**
     */
    @POST
    fun bindPackage(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>, @Body bodyParams: Map<String, @JvmSuppressWildcards Any>): Observable<HttpJsonResult<HawbBindingResult>>

    /**
     */
    @HTTP(method = "DELETE", hasBody = true)
    fun unBindPackage(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>, @Body bodyParams: Map<String, @JvmSuppressWildcards Any>): Observable<HttpJsonResult<EmptyResponse>>

}