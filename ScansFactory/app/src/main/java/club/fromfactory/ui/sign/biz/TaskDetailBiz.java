//package club.fromfactory.ui.sign.biz;
//
//import com.google.gson.reflect.TypeToken;
//
//import java.util.HashMap;
//import java.util.Map;
//
//import club.fromfactory.FFApplication;
//import club.fromfactory.constant.Constants;
//import club.fromfactory.http.ClubHttpPost;
//import club.fromfactory.http.HttpRequestResultListner;
//import club.fromfactory.http.NetUtils;
//import club.fromfactory.http.RequestTypeConstant;
//import club.fromfactory.pojo.HttpJsonResult;
//import club.fromfactory.ui.sign.model.HttpResultTrackingNo;
//import club.fromfactory.ui.sign.model.TrackingsNoDetailModel;
//
///**
// * Created by lxm on 2017/7/25.
// */
//
//public class TaskDetailBiz implements ITaskDetailBiz {
//    /**
//     * 结束签收任务
//     */
//
//    public String CLUB_UPDATE_SIGNED_TASK = NetUtils.APP_MAIN_URL_PROCUREMENT + "purchase_order/package/signed_task/update/";
//
//    /**
//     * 扫描快递单号
//     */
//    public String CLUB_TRACKING_NO = NetUtils.APP_MAIN_URL_PROCUREMENT + "purchase_order/signed/search/tracking_no/";
//    /**
//     * 获取某个具体任务的物流单号列表信息
//     */
//    public String CLUB_GET_TASK_DETAIL = NetUtils.APP_MAIN_URL_PROCUREMENT + "purchase_order/package/task_detail/list/?task_id=";
//
//    @Override
//    public void updateSignedTask(final int task_id, final HttpRequestResultListner httpRequestResultListner) {
//
//        FFApplication.getFixThreadExecutor().execute(new Runnable() {
//            @Override
//            public void run() {
//                TypeToken<HttpJsonResult> typeToken = new TypeToken<HttpJsonResult>() {
//                };
//
//                Map<String, Object> params = new HashMap<String, Object>();
//                params.put("task_id", task_id);
//
//                ClubHttpPost.getInstance().doPostHttpPost(
//                        CLUB_UPDATE_SIGNED_TASK, params, RequestTypeConstant.RETURN_INITJSON_DATA, typeToken, httpRequestResultListner);
//
//            }
//        });
////
//    }
//
//    @Override
//    public void trackingNo(final int task_id, final String tracking_no, final HttpRequestResultListner httpRequestResultListner) {
//
//        FFApplication.getFixThreadExecutor().execute(new Runnable() {
//            @Override
//            public void run() {
//                TypeToken<HttpResultTrackingNo> typeToken = new TypeToken<HttpResultTrackingNo>() {
//                };
//
//                Map<String, Object> params = new HashMap<String, Object>();
//                params.put("task_id", task_id);
//                params.put("tracking_no", tracking_no);
//
//                ClubHttpPost.getInstance().doPostHttpPost(
//                        CLUB_TRACKING_NO, params, RequestTypeConstant.RETURN_INITJSON_DATA, typeToken, httpRequestResultListner);
//
//            }
//        });
//    }
//
//    @Override
//    public void getTaskDetail(final int task_id, final HttpRequestResultListner httpRequestResultListner) {
//
//        FFApplication.getFixThreadExecutor().execute(new Runnable() {
//            @Override
//            public void run() {
//                TypeToken<TrackingsNoDetailModel> typeToken = new TypeToken<TrackingsNoDetailModel>() {
//                };
//                //设置参数，访问url获取json
//                ClubHttpPost.getInstance().doGetHttp(
//                        CLUB_GET_TASK_DETAIL + task_id, null, RequestTypeConstant.RETURN_INITJSON_DATA, typeToken, httpRequestResultListner);
//
//
//            }
//        });
//    }
//
//}
