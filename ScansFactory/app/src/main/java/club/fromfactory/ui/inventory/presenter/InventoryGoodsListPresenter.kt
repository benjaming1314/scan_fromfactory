package club.fromfactory.ui.inventory.presenter

import club.fromfactory.FFApplication
import club.fromfactory.R
import club.fromfactory.baselibrary.language.LanguageUtils
import club.fromfactory.baselibrary.net.CookieHelper
import club.fromfactory.baselibrary.net.retrofit.BaseRetrofit
import club.fromfactory.baselibrary.presenter.BasePresenter
import club.fromfactory.baselibrary.rx.loadingView
import club.fromfactory.baselibrary.utils.JsonUtils
import club.fromfactory.baselibrary.utils.PreferenceStorageUtils
import club.fromfactory.baselibrary.utils.StringUtils
import club.fromfactory.baselibrary.view.RxAppCompatActivity
import club.fromfactory.constant.Constants
import club.fromfactory.http.NetUtils
import club.fromfactory.http.model.HttpJsonErrorResult
import club.fromfactory.pojo.QueryGoodsState
import club.fromfactory.ui.common.model.Goods
import club.fromfactory.ui.inventory.IInventoryGoodsListApi
import club.fromfactory.ui.inventory.contract.InventoryGoodsListContract
import club.fromfactory.ui.main.PermissionUtils
import club.fromfactory.utils.Zlog
import com.trello.rxlifecycle2.kotlin.bindToLifecycle
import io.reactivex.observers.DefaultObserver
import org.json.JSONException
import org.json.JSONObject
import retrofit2.HttpException
import java.util.*

class InventoryGoodsListPresenter(view: InventoryGoodsListContract.View) : BasePresenter<InventoryGoodsListContract.View>(view), InventoryGoodsListContract.Presenter {

    /**
     * 盘点 - 列表
     */
    var checkBoxListUrl = NetUtils.APP_MAIN_URL + "wms/pda/check"
    /**
     * 盘点 - 扫描sku_no
     */
    var checkSkuNoUrl = NetUtils.APP_MAIN_URL + "wms/pda/check/sku"
    /**
     * 盘点修改sku状态
     */
    var checkSkuAlterStateUrl = NetUtils.APP_MAIN_URL + "wms/pda/check/sku"

    /**
     * 订单/仓库 - 盘点获取可更改的状态选项
     * {{ip_bl}}/wms/static?query=status&box_name=X01-1-01
     */

    var queryStatusUrl = NetUtils.APP_MAIN_URL + "wms/static?query=status"

    override fun checkBoxList() {

        val token = PreferenceStorageUtils.getInstance().token

        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        //设置参数，访问url获取json
        val params = HashMap<String, Any>()
        params["box_name"] = view.getBoxName()

        BaseRetrofit
                .createService(IInventoryGoodsListApi::class.java)
                .checkBoxList(checkBoxListUrl, headerMap, params)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : DefaultObserver<List<Goods>>() {

                    override fun onComplete() {
                    }

                    override fun onNext(goodsList: List<Goods>) {

                        val goodsesListShow = ArrayList<Goods>()
                        val isOrderLocation = view.isOrderLocation()

                        for (i in goodsList.indices) {
                            val goods = goodsList.get(i)
                            Zlog.ii("lxm ss refreshData:$goods")
                            val qty = goods.qty
                            val orderTakeUp = FFApplication.getInstance().resources.getString(R.string.order_take_up)

                            if (qty > 0) {
                                for (j in 0 until qty) {
                                    if (isOrderLocation) {
                                        goods.status = orderTakeUp
                                    }
                                    goodsesListShow.add(goods)
                                }
                            }
                        }
                        view.refreshData(goodsesListShow)
                    }

                    override fun onError(e: Throwable) {
                        if (e is HttpException && e.code() in 400..403) {
                            val string = e.response()?.errorBody()?.string()
                            if (string != null) {
                                val parseJson = JsonUtils.parseJson<HttpJsonErrorResult>(string)
                                view.showFailedError(parseJson?.message)
                            }
                        } else {
                            view.showFailedError(FFApplication.getInstance().resources.getString(R.string.net_error))
                        }
                    }

                })
    }

    override fun getQueryStatus() {

        val token = PreferenceStorageUtils.getInstance().token

        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        //设置参数，访问url获取json
        val params = HashMap<String, Any>()
        params["box_name"] = view.getBoxName()

        BaseRetrofit
                .createService(IInventoryGoodsListApi::class.java)
                .getQueryStatus(queryStatusUrl, headerMap, params)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : DefaultObserver<HashMap<String, Any>>() {
                    override fun onComplete() {
                    }

                    override fun onNext(queryStatus: HashMap<String, Any>) {

                        val keysIterator = queryStatus.keys.iterator()

                        Zlog.ii("lxm ss requestSucceed: getQueryStatus  $queryStatus")
                        val queryGoodsStatesList = ArrayList<QueryGoodsState>()
                        val queryGoodsStatesListSort = ArrayList<QueryGoodsState>()

                        var strSort = ArrayList<String>()

                        while (keysIterator.hasNext()) {

                            val queryGoodsState = QueryGoodsState()
                            val key = keysIterator.next()
                            if (key == "content") {

                                val strArray = queryStatus[key] as ArrayList<String>

                                for (i in 0 until strArray.size) {
                                    val str = strArray[i]
                                    strSort.add(str)
                                }
                            } else {
                                val valus = queryStatus[key]
                                queryGoodsState.name = key
                                queryGoodsState.nameValue = valus as String
                                queryGoodsStatesList.add(queryGoodsState)
                            }
                        }
                        for (i in strSort.indices) {
                            for (j in queryGoodsStatesList.indices) {
                                val queryGoodsState = queryGoodsStatesList[j]
                                if (queryGoodsState.nameValue == strSort[i]) {
                                    queryGoodsStatesListSort.add(queryGoodsState)
                                    break
                                }
                            }
                        }

                        for (i in queryGoodsStatesListSort.indices) {
                            Zlog.ii("lxm ss sort: " + queryGoodsStatesListSort[i])
                        }

                        view.goodsStateList(PermissionUtils.instance.getGoodsStatusModelFromPermission(queryGoodsStatesListSort))

                    }

                    override fun onError(e: Throwable) {
                        if (e is HttpException && e.code() in 400..403) {
                            val string = e.response()?.errorBody()?.string()
                            if (string != null) {
                                val parseJson = JsonUtils.parseJson<HttpJsonErrorResult>(string)
                                view.goodsStateListFailed()
                            }
                        } else {
                            view.goodsStateListFailed()
                        }
                    }

                })
    }

    override fun checkSkuNo(barcode: String) {

        val token = PreferenceStorageUtils.getInstance().token

        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        //设置参数，访问url获取json
        val params = HashMap<String, Any>()
        params["box_name"] = view.getBoxName()
        params["barcode"] = barcode

        BaseRetrofit
                .createService(IInventoryGoodsListApi::class.java)
                .checkSkuNo(checkSkuNoUrl, headerMap, params)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : DefaultObserver<Goods>() {
                    override fun onComplete() {
                    }

                    override fun onNext(goods: Goods) {
                        Zlog.ii("lxm ss requestSucceed:$goods")
                        view.scanConfirmSucceed(goods)

                    }

                    override fun onError(e: Throwable) {
                        if (e is HttpException && e.code() in 400..403) {
                            val string = e.response()?.errorBody()?.string()
                            if (string != null) {
                                val parseJson = JsonUtils.parseJson<HttpJsonErrorResult>(string)
                                view.scanConfirmFailed(parseJson?.message)
                            }
                        } else {
                            view.scanConfirmFailed(FFApplication.getInstance().resources.getString(R.string.net_error))
                        }
                    }

                })
    }

    override fun checkSkuAlterState(id: Int, barcode: String, to_status: String, from_status: String) {


        val token = PreferenceStorageUtils.getInstance().token

        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        //设置参数，访问url获取json
        val params = HashMap<String, Any>()

        params["box_name"] = view.getBoxName()
        params["id"] = id
        params["barcode"] = barcode
        params["to_status"] = to_status
        if (StringUtils.isNotBlank(from_status)) {
            params["from_status"] = from_status
        }

        BaseRetrofit
                .createService(IInventoryGoodsListApi::class.java)
                .checkSkuAlterState(checkSkuAlterStateUrl, headerMap, params)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : DefaultObserver<Goods>() {
                    override fun onComplete() {
                    }

                    override fun onNext(goods: Goods) {
                        view.alterGoodStateSucceed(goods)
                    }

                    override fun onError(e: Throwable) {
                        if (e is HttpException && e.code() in 400..403) {
                            val string = e.response()?.errorBody()?.string()
                            if (string != null) {
                                val parseJson = JsonUtils.parseJson<HttpJsonErrorResult>(string)
                                view.alterGoodStateFailed(parseJson?.message)
                            }
                        } else {
                            view.alterGoodStateFailed(FFApplication.getInstance().resources.getString(R.string.net_error))
                        }
                    }

                })
    }
}