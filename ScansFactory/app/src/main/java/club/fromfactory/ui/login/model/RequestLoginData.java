package club.fromfactory.ui.login.model;

import club.fromfactory.baselibrary.model.NoProguard;

/**
 * @author lxm
 * @date 2018/7/11/011
 */
public class RequestLoginData implements NoProguard {

    private String message ;

    private RequestLoginErrorMsg msg ;

    private String status ;

    private LoginData data ;

    private boolean is_success ;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public RequestLoginErrorMsg getMsg() {
        return msg;
    }

    public void setMsg(RequestLoginErrorMsg msg) {
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LoginData getData() {
        return data;
    }

    public void setData(LoginData data) {
        this.data = data;
    }

    public boolean isIs_success() {
        return is_success;
    }

    public void setIs_success(boolean is_success) {
        this.is_success = is_success;
    }
}
