package club.fromfactory.ui.moveout.model

import club.fromfactory.baselibrary.model.NoProguard

data class TitleModel (var title : String  = "",
                       var num :Int = 0

): NoProguard