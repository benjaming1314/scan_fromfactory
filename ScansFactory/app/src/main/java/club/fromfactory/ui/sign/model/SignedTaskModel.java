package club.fromfactory.ui.sign.model;

import java.util.List;

import club.fromfactory.baselibrary.model.NoProguard;

/**
 * Created by lxm on 2017/7/26.
 * "count": 33,
 "next": "http://dev.yuceyi.com:5677/purchase_order/package/signed_task/list/?limit=20&no_end_time=1&offset=20",
 "previous": null,
 "results":
 */

public class SignedTaskModel implements NoProguard {

    private int count ;
    private String next ;
    private String previous ;
    private List<SignedTask> results ;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public String getPrevious() {
        return previous;
    }

    public void setPrevious(String previous) {
        this.previous = previous;
    }

    public List<SignedTask> getResults() {
        return results;
    }

    public void setResults(List<SignedTask> results) {
        this.results = results;
    }

    @Override
    public String toString() {
        return "SignedTaskModel{" +
                "count=" + count +
                ", next='" + next + '\'' +
                ", previous='" + previous + '\'' +
                ", results=" + results +
                '}';
    }
}
