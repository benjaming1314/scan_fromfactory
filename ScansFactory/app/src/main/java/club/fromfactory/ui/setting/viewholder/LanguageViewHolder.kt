package club.fromfactory.ui.setting.viewholder

import android.view.ViewGroup
import club.fromfactory.R
import club.fromfactory.baselibrary.language.LanguageInfo
import club.fromfactory.baselibrary.view.recyclerview.BaseViewHolder
import kotlinx.android.synthetic.main.language_item.*

class LanguageViewHolder(parent: ViewGroup) : BaseViewHolder<LanguageInfo>(parent, R.layout.language_item) {


    override fun bindData(data: LanguageInfo) {
        super.bindData(data)

        language_txt_language.text = data.languageStr
    }
}