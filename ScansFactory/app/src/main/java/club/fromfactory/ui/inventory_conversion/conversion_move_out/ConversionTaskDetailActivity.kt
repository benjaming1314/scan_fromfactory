package club.fromfactory.ui.inventory_conversion.conversion_move_out

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.widget.EditText
import club.fromfactory.R
import club.fromfactory.baselibrary.utils.StringUtils
import club.fromfactory.baselibrary.utils.ToastUtils
import club.fromfactory.baselibrary.widget.CustomDialog
import club.fromfactory.ui.common.model.EditTextModel
import club.fromfactory.ui.inventory_conversion.conversion_move_out.contract.ConversionTaskDetailContract
import club.fromfactory.ui.inventory_conversion.conversion_move_out.model.ConversionTaskItemInfo
import club.fromfactory.ui.inventory_conversion.conversion_move_out.presenter.ConversionTaskDetailPresenter
import club.fromfactory.ui.main.ScanBaseMVPActivity
import club.fromfactory.ui.viewphotos.ViewPhotosActivity
import club.fromfactory.utils.*
import club.fromfactory.widget.CustomTitleLinearLayout
import kotlinx.android.synthetic.main.activity_conversion_task_detail.*
import kotlinx.android.synthetic.main.conversion_task_item.*

/**
 * 转换下架 详情页面
 */
class ConversionTaskDetailActivity : ScanBaseMVPActivity<ConversionTaskDetailContract.Presenter>(), ConversionTaskDetailContract.View {
    override fun createPresenter(): ConversionTaskDetailContract.Presenter = ConversionTaskDetailPresenter(this)

    companion object {

        private const val INTENT_CVID = "cvid"

        fun launchActivity(context: Context, cvId: Int, requestCode: Int) {
            val intent = Intent()
            intent.setClass(context, ConversionTaskDetailActivity::class.java)
            intent.putExtra(INTENT_CVID, cvId)
            (context as Activity).startActivityForResult(intent, requestCode)
        }
    }

    private var cvId: Int? = 0


    private var mSize = 15

    private var mIsRequest = true

    private var mConversionTaskItemInfoList = ArrayList<ConversionTaskItemInfo>()

    private var mCurrentIndex = 0

    private var mCurrentConversionTaskItemInfo: ConversionTaskItemInfo? = null

    private var customDialog: CustomDialog? = null


    private var mEdtMaps = HashMap<EditText, EditTextModel>()


    private var isHandleScanResult = false


    //    var click = 0
    override fun requestScanStart() {

//        if (Zlog.isDebug) {
//
//            if (click == 0) {
//
//                requestScanResult("X02-01-23")
//
//            }else if (click == 1){
//                requestScanResult("0000743261")
//
//            }
//            click++
//        }
    }

    override fun clickKeyCodeBack() {
        finish()
    }

    override fun getLayoutResId(): Int = R.layout.activity_conversion_task_detail


    override fun initData() {
        super.initData()

        cvId = intent.getIntExtra(INTENT_CVID, 0)

    }

    override fun initView() {
        super.initView()

        conversion_task_detail_ctl_title.setListener(object : CustomTitleLinearLayout.CustomTitleListener() {

            override fun clickLeft() {
                super.clickLeft()
                finish()
            }
        })

        setScanListener(conversion_task_detail_scan)

        conversion_task_item_img.setOnClickListener {
            val intent = Intent(this, ViewPhotosActivity::class.java)
            intent.putExtra("images", mCurrentConversionTaskItemInfo?.imageUrl)
            startActivity(intent)
        }
        conversion_task_detail_btn_ok.setOnClickListener {

            takeDown()
        }

        conversion_task_detail_out_stock.setOnClickListener {

            PromptDialogUtils.getInstance().showDialog(this, resources.getString(R.string.out_stock_prompt), object : PromptDialogUtils.DialogCallback {
                override fun onClickPositiveBtn() {
                    presenter.misssing(mCurrentConversionTaskItemInfo?.cvItemId ?: 0)
                }

                override fun onClickNegativeBtn() {
                }
            })

        }

        conversion_task_detail_edt_location.setOnFocusChangeListener { v, hasFocus ->

            Zlog.ii("lxm sda1:" + mEdtMaps[conversion_task_detail_edt_location].toString())
            mEdtMaps[conversion_task_detail_edt_location]?.isFoucus = hasFocus
            Zlog.ii("lxm sda2:" + mEdtMaps[conversion_task_detail_edt_location].toString())

        }

        conversion_task_detail_edt_sku.setOnFocusChangeListener { v, hasFocus ->
            Zlog.ii("lxm sda3:" + mEdtMaps[conversion_task_detail_edt_sku].toString())
            mEdtMaps[conversion_task_detail_edt_sku]?.isFoucus = hasFocus
            Zlog.ii("lxm sda4:" + mEdtMaps[conversion_task_detail_edt_sku].toString())
        }
        EditTextUtils.addEditTextListener(conversion_task_detail_edt_location)
        EditTextUtils.addEditTextListener(conversion_task_detail_edt_sku)


        mEdtMaps[conversion_task_detail_edt_location] = EditTextModel(conversion_task_detail_edt_location, conversion_task_detail_edt_sku, true)
        mEdtMaps[conversion_task_detail_edt_sku] = EditTextModel(conversion_task_detail_edt_sku, null, false)

    }

    override fun fetchData() {
        super.fetchData()
        presenter.getCvItemList()

    }

    override fun requestScanResult(result: String?) {
        if (customDialog != null && customDialog?.isShowing == true) {
            return
        }

        if (isHandleScanResult) {
            return
        } else {
            isHandleScanResult = true
        }

        var isHasResult = false

        val iterator = mEdtMaps.keys.iterator()
        while (iterator.hasNext()) {

            val it = iterator.next()

            val editTextModel = mEdtMaps[it]
            Zlog.ii("lxm sda5:" + mEdtMaps[it].toString())
            if (editTextModel?.isFoucus == true) {

                it.setText(result ?: "")
                val nextEditText = editTextModel.nextEditText
                if (nextEditText == null) {
                    isHasResult = true
                    takeDown()
                } else {
                    //判断库位是否正确
                    if (checkLocation()) {
                        nextEditText.requestFocus()
                    } else {

                    }
                }
                break
            }

        }


        if (!isHasResult) {
            isHandleScanResult = false
        }

    }

    private fun checkLocation(): Boolean {


        val location = conversion_task_detail_edt_location.text.toString()

        val boxName = mCurrentConversionTaskItemInfo?.boxName

        return if (StringUtils.isNotBlank(location) && location == boxName) {
            true

        } else if (StringUtils.isNotBlank(location)) {
            TipSound.getInstance().playScanFailedSound(this)
            ToastUtils.show(resources.getString(R.string.scan_prompt_not_match_location))
            false
        } else {

            ToastUtils.show(resources.getString(R.string.scann_prompt_location))
            false
        }

    }

    private fun checkSku(): Boolean {

        val sku = conversion_task_detail_edt_sku.text.toString()

        val barcode = mCurrentConversionTaskItemInfo?.barcode

        return if (StringUtils.isNotBlank(sku) && sku == barcode) {
            true

        } else if (StringUtils.isNotBlank(sku)) {
            TipSound.getInstance().playScanFailedSound(this)
            ToastUtils.show(resources.getString(R.string.scan_prompt_not_match_sku))
            false
        } else {
            ToastUtils.show(resources.getString(R.string.scan_prompt_6))
            false
        }

    }

    private fun takeDown() {

        val checkLocation = checkLocation()
        if (checkLocation && checkSku()) {
            presenter.takeDown(mCurrentConversionTaskItemInfo?.cvItemId
                    ?: 0, mCurrentConversionTaskItemInfo?.boxName
                    ?: "", mCurrentConversionTaskItemInfo?.barcode
                    ?: "")
        } else if (!checkLocation) {
            isHandleScanResult = false
            conversion_task_detail_edt_location.requestFocus()
        } else if (!checkSku()) {
            isHandleScanResult = false
            conversion_task_detail_edt_sku.requestFocus()
        }

    }

    override fun getSize(): Int = mSize
    override fun getCvId(): Int = cvId ?: 0

    override fun requestFailed(message: String?) {
        isHandleScanResult = false
        TipSound.getInstance().playScanFailedSound(this)
//        ToastUtils.show(message)

        customDialog = PromptDialogUtils.getInstance().getPromptDialog(this@ConversionTaskDetailActivity, message
                ?: resources.getString(R.string.net_error_prompt_one))
        if (customDialog != null && isAlive) {
            customDialog?.show()
        }
    }

    override fun requestItemListSuccess(conversionTaskItemInfoList: List<ConversionTaskItemInfo>) {
        isHandleScanResult = false
        mIsRequest = conversionTaskItemInfoList.size == mSize

        mConversionTaskItemInfoList.clear()
        mConversionTaskItemInfoList.addAll(conversionTaskItemInfoList)

        mCurrentIndex = 0

        refreshCurrentItem()

    }

    override fun takeDownSuccess() {
        isHandleScanResult = false
        TipSound.getInstance().playScanSucceedSound(this)
        mCurrentIndex++
        refreshCurrentItem()
    }

    override fun misssingSuccess() {
        isHandleScanResult = false
        TipSound.getInstance().playScanSucceedSound(this)
        mCurrentIndex++
        refreshCurrentItem()
    }

    private fun refreshCurrentItem() {
        isHandleScanResult = false
        if (mCurrentIndex > mConversionTaskItemInfoList.size - 1) {

            if (mIsRequest) {
                fetchData()
            } else {
                ToastUtils.show(resources.getString(R.string.scan_prompt_no_task))
                setResult(Activity.RESULT_OK)
                finish()
            }
            return
        }
        mCurrentConversionTaskItemInfo = mConversionTaskItemInfoList[mCurrentIndex]

        ImageUtils.loadImageNew(conversion_task_item_img, mCurrentConversionTaskItemInfo?.imageUrl, R.color.color_f2f2f2)

        conversion_task_item_txt_location.text = mCurrentConversionTaskItemInfo?.boxName
        conversion_task_item_txt_sku.text = mCurrentConversionTaskItemInfo?.barcode
        conversion_task_item_txt_category.text = mCurrentConversionTaskItemInfo?.category
        conversion_task_item_txt_color.text = mCurrentConversionTaskItemInfo?.color

        conversion_task_detail_edt_location.setText("")
        conversion_task_detail_edt_sku.setText("")

        conversion_task_detail_edt_location.requestFocus()
    }

    override fun showLoadingView() {
        showLoadingView(conversion_task_detail_progress)
    }

    override fun hideLoadingView() {
        hideLoadingView(conversion_task_detail_progress)
    }

    override fun onDestroy() {
        super.onDestroy()
        isHandleScanResult = false
        if (customDialog != null && customDialog?.isShowing == true && isAlive) {
            customDialog?.dismiss()
            customDialog = null
        }
    }

}
