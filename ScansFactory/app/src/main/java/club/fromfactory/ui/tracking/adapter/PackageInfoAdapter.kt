package club.fromfactory.ui.tracking.adapter

import android.view.ViewGroup
import club.fromfactory.baselibrary.view.recyclerview.BaseRecyclerAdapter
import club.fromfactory.baselibrary.view.recyclerview.BaseViewHolder
import club.fromfactory.ui.tracking.model.PackageInfo
import club.fromfactory.ui.tracking.viewholder.PackageInfoViewHolder

class PackageInfoAdapter : BaseRecyclerAdapter<PackageInfo>() {
    override fun onCreateBaseViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<PackageInfo> = PackageInfoViewHolder(parent)
}