package club.fromfactory.ui.moveout.presenter

import club.fromfactory.FFApplication
import club.fromfactory.R
import club.fromfactory.baselibrary.language.LanguageUtils
import club.fromfactory.baselibrary.net.CookieHelper
import club.fromfactory.baselibrary.net.retrofit.BaseRetrofit
import club.fromfactory.baselibrary.presenter.BasePresenter
import club.fromfactory.baselibrary.rx.loadingView
import club.fromfactory.baselibrary.utils.JsonUtils
import club.fromfactory.baselibrary.utils.PreferenceStorageUtils
import club.fromfactory.baselibrary.view.RxAppCompatActivity
import club.fromfactory.constant.Constants
import club.fromfactory.http.NetUtils
import club.fromfactory.http.model.HttpJsonErrorResult
import club.fromfactory.ui.moveout.IMoveOutApi
import club.fromfactory.ui.moveout.contract.MoveOutConfirmContract
import club.fromfactory.ui.moveout.model.Locations
import club.fromfactory.ui.moveout.model.OffShelfTaskResponse
import com.trello.rxlifecycle2.kotlin.bindToLifecycle
import io.reactivex.observers.DefaultObserver
import retrofit2.HttpException
import java.util.*

class MoveOutPresenter(view: MoveOutConfirmContract.View) : BasePresenter<MoveOutConfirmContract.View>(view), MoveOutConfirmContract.Presenter {
    private var offshelfTaskUrl = NetUtils.APP_MAIN_URL + "wms/pda/off_shelf/task"

    override fun getOffShelfTask() {

        val token = PreferenceStorageUtils.getInstance().token

        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        BaseRetrofit
                .createService(IMoveOutApi::class.java)
                .getOffshelfTask(offshelfTaskUrl, headerMap)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : DefaultObserver<OffShelfTaskResponse>() {
                    override fun onComplete() {
                    }

                    override fun onNext(t: OffShelfTaskResponse) {
                        view.refreshData(t)
                    }

                    override fun onError(e: Throwable) {
                        if (e is HttpException && e.code() in 400..404) {
                            val string = e.response()?.errorBody()?.string()
                            if (string != null) {
                                val parseJson = JsonUtils.parseJson<HttpJsonErrorResult>(string)
                                view.showFailedError(parseJson?.message)
                            }
                        } else {
                            view.showFailedError(FFApplication.getInstance().resources.getString(R.string.net_error))
                        }
                    }

                })
    }
}