package club.fromfactory.ui.sign

import club.fromfactory.baselibrary.model.BaseResponse
import club.fromfactory.baselibrary.model.EmptyResponse
import club.fromfactory.pojo.HttpJsonResult
import club.fromfactory.ui.sign.model.CreateNewTaskModel
import club.fromfactory.ui.sign.model.HttpResultTrackingNo
import club.fromfactory.ui.sign.model.SignedTaskModel
import club.fromfactory.ui.sign.model.TrackingsNoDetailModel
import io.reactivex.Observable
import retrofit2.http.*

interface ISigningApi {

    /**
     * 创建任务
     */
    @GET
    fun createSignedTask(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>): Observable<CreateNewTaskModel>

    /**
     * 获取未完成的任务清单
     */
    @GET
    fun getSignTaskList(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>): Observable<SignedTaskModel>

    /**
     * 结束签收任务
     */
    @POST
    fun updateSignedTask(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>, @Body bodyParams: Map<String, @JvmSuppressWildcards Any>): Observable<HttpJsonResult<EmptyResponse>>

    /**
     * 扫描快递单号
     */
    @POST
    fun trackingNo(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>, @Body bodyParams: Map<String, @JvmSuppressWildcards Any>): Observable<HttpResultTrackingNo>

    /**
     * 获取某个具体任务的物流单号列表信息
     */
    @GET
    fun getTaskDetail(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>, @QueryMap queryParams: Map<String, @JvmSuppressWildcards Any>): Observable<TrackingsNoDetailModel>

}