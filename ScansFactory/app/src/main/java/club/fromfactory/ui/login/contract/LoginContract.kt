package club.fromfactory.ui.login.contract

import club.fromfactory.baselibrary.presenter.IPresenter
import club.fromfactory.baselibrary.view.BaseActivity
import club.fromfactory.baselibrary.view.IBaseMVPView
import club.fromfactory.ui.login.model.RequestLoginData
import club.fromfactory.ui.versionupdate.model.VersionModel

class LoginContract {

    interface View : IBaseMVPView<Presenter> {
        fun getUserName(): String

        fun getPassword(): String

        fun clearUserName()

        fun clearPassword()

        fun showFailedError(message: String?)

        fun loginSucceed(httpJsonResult: RequestLoginData?)

        fun requestUpdateVersionSuccess(versionModel: VersionModel?)
        fun requestUpdateVersionFailed(message: String?)

        fun enterDebugPanel()

    }

    interface Presenter : IPresenter<View> {

        fun debugPanel()

        fun onDestroy(activity: BaseActivity)

        fun login()

        fun getVersionUpdate()


    }

}