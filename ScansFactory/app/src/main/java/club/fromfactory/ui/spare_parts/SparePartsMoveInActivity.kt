package club.fromfactory.ui.spare_parts

import android.app.Activity
import android.widget.EditText
import club.fromfactory.R
import club.fromfactory.baselibrary.router.RouterConstants
import club.fromfactory.baselibrary.utils.JsonUtils
import club.fromfactory.baselibrary.utils.StringUtils
import club.fromfactory.baselibrary.utils.ToastUtils
import club.fromfactory.baselibrary.widget.CustomDialog
import club.fromfactory.routerannotaions.Router
import club.fromfactory.routerannotaions.RouterParam
import club.fromfactory.ui.common.model.EditTextModel
import club.fromfactory.ui.main.ScanBaseMVPActivity
import club.fromfactory.ui.spare_parts.adapter.SparePartsGoodsListAdapter
import club.fromfactory.ui.spare_parts.contract.SparePartsMoveInContract
import club.fromfactory.ui.spare_parts.presenter.SparePartsMoveInPresenter
import club.fromfactory.utils.EditTextUtils
import club.fromfactory.utils.PromptDialogUtils
import club.fromfactory.utils.TipSound
import club.fromfactory.utils.Zlog
import club.fromfactory.widget.CustomTitleLinearLayout
import kotlinx.android.synthetic.main.activity_spare_parts_move_in.*

/**
 * 散件移库上架
 */
@Router(RouterConstants.SPARE_PARTS_MOVE_IN)
class SparePartsMoveInActivity : ScanBaseMVPActivity<SparePartsMoveInContract.Presenter>(), SparePartsMoveInContract.View {

    private var isHandleScanResult = false
    private var customDialog: CustomDialog? = null
    private var mSparePartsGoodsListAdapter: SparePartsGoodsListAdapter? = null

    private var mEdtMaps = HashMap<EditText, EditTextModel>()
    @RouterParam("barcode_list_str")
    @JvmField
    var barcodeListStr: String? = null

//    var click = 0
    override fun requestScanStart() {
//        if (Zlog.isDebug) {
//
//            if (click == 0) {
//                requestScanResult("X06-06-10")
//            } else if (click == 1) {
//                requestScanResult("H20002LGNC")
//            } else if (click == 2) {
//                requestScanResult("I3dgh65478")
//            }
//            click++
//        }
    }

    override fun getLayoutResId(): Int = R.layout.activity_spare_parts_move_in

    override fun initData() {
        super.initData()
    }

    override fun initView() {
        super.initView()

        spare_parts_move_in_ctl_title.setListener(object : CustomTitleLinearLayout.CustomTitleListener() {

            override fun clickLeft() {
                super.clickLeft()
                finish()
            }
        })
        setListener()
        mSparePartsGoodsListAdapter = SparePartsGoodsListAdapter()
        spare_parts_move_in_recycler_view.adapter = mSparePartsGoodsListAdapter

        val barcodeList = JsonUtils.parseJson<List<String>>(barcodeListStr ?: "")
        mSparePartsGoodsListAdapter?.addAll(barcodeList?:ArrayList())

        refreshTotalNum()

    }


    private fun setListener() {
        spare_parts_move_in_all_move_in.isEnabled = false
        spare_parts_move_in_all_move_in.setOnClickListener {
            if (it.isEnabled) {
                finishMoveIn()
            }
        }
        spare_parts_move_in_change_location.isEnabled = false
        spare_parts_move_in_change_location.setOnClickListener {
            if (it.isEnabled) {
                changeLocation()
            }
        }
        setScanListener(spare_parts_move_in_scan)
        spare_parts_move_in_btn_shelves_boxname_ok.setOnClickListener {
            if (spare_parts_move_in_edt_shelves_boxname.isEnabled) {
                checkLocation(spare_parts_move_in_edt_shelves_boxname.text.toString())
            }
        }
        spare_parts_move_in_btn_barcode_ok.setOnClickListener {
            if (spare_parts_move_in_edt_barcode.isEnabled) {
                shelfBarcode(spare_parts_move_in_edt_barcode.text.toString())
            }
        }

        spare_parts_move_in_edt_shelves_boxname.setOnFocusChangeListener { _, hasFocus ->
            mEdtMaps[spare_parts_move_in_edt_shelves_boxname]?.isFoucus = hasFocus
        }
        spare_parts_move_in_edt_barcode.setOnFocusChangeListener { _, hasFocus ->
            mEdtMaps[spare_parts_move_in_edt_barcode]?.isFoucus = hasFocus
        }
        EditTextUtils.addEditTextListener(spare_parts_move_in_edt_shelves_boxname)
        EditTextUtils.addEditTextListener(spare_parts_move_in_edt_barcode)

        mEdtMaps[spare_parts_move_in_edt_shelves_boxname] = EditTextModel(spare_parts_move_in_edt_shelves_boxname, null, true)
        mEdtMaps[spare_parts_move_in_edt_barcode] = EditTextModel(spare_parts_move_in_edt_barcode, null, false)

        spare_parts_move_in_edt_barcode.isEnabled = false
    }


    override fun requestScanResult(result: String?) {
        if (customDialog != null && customDialog?.isShowing == true) {
            return
        }
        if (isHandleScanResult) {
            return
        } else {
            isHandleScanResult = true
        }
        var isHasResult = false

        val iterator = mEdtMaps.keys.iterator()
        while (iterator.hasNext()) {
            val it = iterator.next()

            val editTextModel = mEdtMaps[it]
            if (editTextModel?.isFoucus == true) {

                it.setText(result ?: "")
                when {
                    editTextModel.editText == spare_parts_move_in_edt_shelves_boxname -> {
                        isHasResult = true
                        checkLocation(result)
                    }
                    editTextModel.editText == spare_parts_move_in_edt_barcode -> {
                        isHasResult = true
                        shelfBarcode(result)
                    }
                }
                break
            }
        }

        if (!isHasResult) {
            isHandleScanResult = false
        }
    }

    private fun checkLocation(boxName: String?) {
        if (StringUtils.isNotBlank(boxName)) {
            presenter.shelfBox(boxName ?: "")
        } else {
            isHandleScanResult = false
            requestFailed(resources.getString(R.string.shelf_location_scan_prompt))
        }
    }

    private fun shelfBarcode(barcode: String?) {
        if (StringUtils.isNotBlank(barcode)) {
            val boxName = spare_parts_move_in_edt_shelves_boxname.text.toString()
            presenter.shelfBarcode(boxName, barcode ?: "")
        } else {
            isHandleScanResult = false
            requestFailed(resources.getString(R.string.barcode_scan_prompt))
        }
    }

    private fun refreshTotalNum() {
        val size = mSparePartsGoodsListAdapter?.data?.size ?: 0
        spare_parts_move_in_total_num.text = size.toString()
    }


    override fun shelfBoxSuccess() {
        isHandleScanResult = false
        TipSound.getInstance().playScanSucceedSound(this)

        spare_parts_move_in_edt_shelves_boxname.isEnabled = false
        spare_parts_move_in_edt_barcode.isEnabled = true
        spare_parts_move_in_edt_barcode.requestFocus()

        spare_parts_move_in_all_move_in.isEnabled = true
        spare_parts_move_in_change_location.isEnabled = true

    }

    override fun shelfBarcodeSuccess() {
        isHandleScanResult = false
        ToastUtils.show(resources.getString(R.string.shelf_success))
        TipSound.getInstance().playScanSucceedSound(this)
        val barcode = spare_parts_move_in_edt_barcode.text.toString()

        mSparePartsGoodsListAdapter?.remove(barcode)
        mSparePartsGoodsListAdapter?.notifyDataSetChanged()
        refreshTotalNum()
        spare_parts_move_in_edt_barcode.setText("")
        spare_parts_move_in_edt_barcode.requestFocus()

        if (mSparePartsGoodsListAdapter?.data?.size == 0) {
            setResult(Activity.RESULT_OK)
            finish()
        }
    }

    override fun shelfAllSuccess() {
        isHandleScanResult = false
        TipSound.getInstance().playScanSucceedSound(this)
        ToastUtils.show(resources.getString(R.string.shelf_success))
        setResult(Activity.RESULT_OK)
        finish()
    }

    private fun changeLocation() {
        val toJson = JsonUtils.toJson(mSparePartsGoodsListAdapter?.data ?: ArrayList<String>())
        intent.putExtra("barcode_list_str",toJson)
        startActivity(intent)
        finish()
    }

    private fun finishMoveIn() {
        var msg = resources.getString(R.string.all_move_in_prompt) + "  " + spare_parts_move_in_edt_shelves_boxname.text.toString()
        PromptDialogUtils.getInstance().showDialogDefine(this, msg, object : PromptDialogUtils.DialogCallback {
            override fun onClickPositiveBtn() {
                val boxName = spare_parts_move_in_edt_shelves_boxname.text.toString()
                val data = mSparePartsGoodsListAdapter?.data
                presenter.shelfAll(boxName, data ?: ArrayList())
            }

            override fun onClickNegativeBtn() {
            }

        })
    }

    override fun requestFailed(message: String?) {
        isHandleScanResult = false
//        ToastUtils.showLong(message)
        TipSound.getInstance().playScanFailedSound(this)
        customDialog = PromptDialogUtils.getInstance().getPromptDialog(this, message)
        if (customDialog != null && isAlive) {
            customDialog?.show()
        }
    }

    override fun showLoadingView() {
        showLoadingView(spare_parts_move_in_progress)
    }

    override fun hideLoadingView() {
        hideLoadingView(spare_parts_move_in_progress)
    }

    override fun onDestroy() {
        super.onDestroy()

        if (customDialog != null) {
            if (customDialog?.isShowing == null) {
                customDialog?.dismiss()
            }
            customDialog = null
        }
    }

    override fun createPresenter(): SparePartsMoveInContract.Presenter = SparePartsMoveInPresenter(this)

}
