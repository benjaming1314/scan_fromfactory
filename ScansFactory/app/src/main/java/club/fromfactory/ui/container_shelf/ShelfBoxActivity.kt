//package club.fromfactory.ui.container_shelf
//
//import android.app.Activity
//import android.content.Intent
//import club.fromfactory.FFApplication
//import club.fromfactory.R
//import club.fromfactory.baselibrary.router.RouterConstants
//import club.fromfactory.baselibrary.utils.StringUtils
//import club.fromfactory.baselibrary.view.BaseActivity
//import club.fromfactory.baselibrary.widget.CustomDialog
//import club.fromfactory.routerannotaions.Router
//import club.fromfactory.routerannotaions.RouterParam
//import club.fromfactory.ui.container_shelf.contract.ShelfBoxContract
//import club.fromfactory.ui.container_shelf.presenter.ShelfBoxPresenter
//import club.fromfactory.ui.main.ScanBaseMVPActivity
//import club.fromfactory.utils.PromptDialogUtils
//import club.fromfactory.utils.TipSound
//import club.fromfactory.widget.CustomTitleLinearLayout
//import com.blankj.utilcode.util.ToastUtils
//import kotlinx.android.synthetic.main.activity_shelf_box.*
//
//@Router(RouterConstants.SHELF_BOX)
//class ShelfBoxActivity : ScanBaseMVPActivity<ShelfBoxContract.Presenter>(),ShelfBoxContract.View {
//
//    @RouterParam("container_num")
//    @JvmField
//    var containerNum :String ?= null
//
//    @RouterParam("boxName")
//    @JvmField
//    var boxName :String ?= null
//
//    @RouterParam("total_to_shelve")
//    @JvmField
//    var totalSheve :String ?= null
//
//    private var isHandleScanResult = false
//
//    companion object {
//        fun launchActivity(activity: BaseActivity) {
//
//            val intent = Intent(activity, ContainerConfirmActivity::class.java)
//            activity.startActivity(intent)
//        }
//    }
//
//    override fun requestScanStart() {
//
//    }
//
//    override fun getLayoutResId(): Int = R.layout.activity_shelf_box
//
//    override fun initData() {
//        super.initData()
//    }
//
//    override fun initView() {
//        super.initView()
//
//        shelf_box_ctl_title.setListener(object : CustomTitleLinearLayout.CustomTitleListener(){
//
//            override fun clickLeft() {
//                super.clickLeft()
//                finish()
//            }
//        })
//
//        shelf_box_txt_container.text = containerNum
//        shelf_box_txt_boxname.text = boxName
//        shelf_box_txt_num.text = totalSheve
//
//        shelf_box_btn_ok.setOnClickListener {
//            val str = shelf_box_edt_hawb.text.toString()
//            shelfBox(str)
//        }
//
//        setScanListener(shelf_box_scan)
//    }
//
//
//    override fun requestScanResult(result: String?) {
//
//        if (isHandleScanResult) {
//            return
//        } else {
//            isHandleScanResult = true
//        }
//
//        if (StringUtils.isNotBlank(result)) {
//            shelf_box_edt_hawb.setText(result)
//        }
//        shelfBox(result?:"")
//    }
//
//    override fun getContainerBox(): String = containerNum?:""
//
//    private fun shelfBox(str :String?) {
//        if (StringUtils.isNotBlank(str)) {
//            presenter.shelfBox(str?:"")
//        }else {
//            requestFailed(resources.getString(R.string.container_num_scan_prompt))
//        }
//    }
//
//    override fun shelfSuccess() {
//        TipSound.getInstance().playScanSucceedSound(this)
////        goBack()
//    }
//
////    private fun goBack() {
////        val message = resources.getString(R.string.shelf_success_prompt)
////        PromptDialogUtils.getInstance().showDialogDefine(this@ShelfBoxActivity, message, object : PromptDialogUtils.DialogCallback {
////            override fun onClickPositiveBtn() {
////                val intent = Intent()
////                intent.putExtra("is_reset",true)
////                setResult(Activity.RESULT_OK,intent)
////            }
////
////            override fun onClickNegativeBtn() {
////                val intent = Intent()
////                intent.putExtra("is_reset",false)
////                setResult(Activity.RESULT_OK,intent)
////            }
////
////        })
////    }
//
//    private var customDialog : CustomDialog?= null
//    override fun requestFailed(message: String?) {
//        isHandleScanResult = false
////        ToastUtils.showLong(message)
//
//        TipSound.getInstance().playScanFailedSound(this)
//        customDialog = PromptDialogUtils.getInstance().getPromptDialog(this, message)
//        if (customDialog != null && isAlive) {
//            customDialog?.show()
//        }
//    }
//
//
//    override fun showLoadingView() {
//        showLoadingView(shelf_box_progress)
//    }
//
//    override fun hideLoadingView() {
//        hideLoadingView(shelf_box_progress)
//    }
//
//    override fun onDestroy() {
//        super.onDestroy()
//        if (customDialog != null) {
//            if (customDialog?.isShowing == null) {
//                customDialog?.dismiss()
//            }
//            customDialog = null
//        }
//    }
//
//    override fun createPresenter(): ShelfBoxContract.Presenter = ShelfBoxPresenter(this)
//
//}
