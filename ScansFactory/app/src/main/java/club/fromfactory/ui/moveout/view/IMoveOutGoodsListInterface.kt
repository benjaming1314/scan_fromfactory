package club.fromfactory.ui.moveout.view

import android.util.SparseArray
import club.fromfactory.baselibrary.view.recyclerview.BaseRecyclerItemViewClickListener

interface IMoveOutGoodsListInterface : BaseRecyclerItemViewClickListener<Any> {


    fun getTextContentState(): SparseArray<Int>

}