package club.fromfactory.ui.inventory_inquire.model

import club.fromfactory.ui.common.model.Goods

data class InquireResultData (
        var rows :List<Goods> ?= null,
        var qty_sum :Int ?= null

)