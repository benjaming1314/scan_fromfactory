package club.fromfactory.ui.setting.viewholder

import android.view.ViewGroup
import club.fromfactory.R
import club.fromfactory.app_config.model.WareHouseInfor
import club.fromfactory.baselibrary.view.recyclerview.BaseViewHolder
import kotlinx.android.synthetic.main.warehouse_item.*

class WareHouseViewHolder(parent: ViewGroup) :BaseViewHolder<WareHouseInfor>(parent, R.layout.warehouse_item) {


    override fun bindData(data: WareHouseInfor) {
        super.bindData(data)

        warehouse_txt_current_warehouse.text = data.house_name
    }
}