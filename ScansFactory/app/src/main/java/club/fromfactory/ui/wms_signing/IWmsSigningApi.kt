package club.fromfactory.ui.wms_signing

import club.fromfactory.baselibrary.model.BaseResponse
import club.fromfactory.baselibrary.model.EmptyResponse
import club.fromfactory.ui.wms_signing.model.OrderDetailInfo
import club.fromfactory.ui.wms_signing.model.SignPackageResult
import io.reactivex.Observable
import retrofit2.http.*

interface IWmsSigningApi {

    /**
     * 创建签收单
     */
    @POST
    fun createSignOrder(@Url url :String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any> ,@Body bodyParams: Map<String, @JvmSuppressWildcards Any>): Observable<BaseResponse<OrderDetailInfo>>

    /**
     * 签收包裹
     */
    @POST
    fun signAwb(@Url url :String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any> ,@Body bodyParams: Map<String, @JvmSuppressWildcards Any>): Observable<BaseResponse<SignPackageResult>>

    /**
     * 获取签收订单列表
     */
    @GET
    fun getSignedOrderList(@Url url :String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any> ,@QueryMap queryParams: Map<String, @JvmSuppressWildcards Any>): Observable<BaseResponse<List<OrderDetailInfo>>>

    /**
     * 完成签收
     */
    @PATCH
    fun completeSignOrder(@Url url :String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any> ): Observable<BaseResponse<EmptyResponse>>



}