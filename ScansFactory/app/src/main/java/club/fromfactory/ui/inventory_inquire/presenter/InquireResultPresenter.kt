package club.fromfactory.ui.inventory_inquire.presenter

import club.fromfactory.baselibrary.language.LanguageUtils
import club.fromfactory.baselibrary.net.CookieHelper
import club.fromfactory.baselibrary.net.retrofit.BaseRetrofit
import club.fromfactory.baselibrary.presenter.BasePresenter
import club.fromfactory.baselibrary.rx.loadingView
import club.fromfactory.baselibrary.utils.JsonUtils
import club.fromfactory.baselibrary.utils.PreferenceStorageUtils
import club.fromfactory.baselibrary.utils.StringUtils
import club.fromfactory.baselibrary.view.RxAppCompatActivity
import club.fromfactory.constant.Constants
import club.fromfactory.http.NetUtils
import club.fromfactory.http.model.HttpJsonErrorResult
import club.fromfactory.ui.inventory_inquire.IInquireApi
import club.fromfactory.ui.inventory_inquire.contract.InquireResultContract
import club.fromfactory.ui.inventory_inquire.model.InquireResultData
import com.trello.rxlifecycle2.kotlin.bindToLifecycle
import io.reactivex.observers.DefaultObserver
import retrofit2.HttpException
import java.util.HashMap

class InquireResultPresenter(view: InquireResultContract.View) : BasePresenter<InquireResultContract.View>(view), InquireResultContract.Presenter {

    private var CREATE_SIGN_ORDER_URL = NetUtils.APP_MAIN_URL + "wms/api/stocks/"

    override fun getInquireResult() {

        val token = PreferenceStorageUtils.getInstance().token

        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        val params = HashMap<String, Any>()
        val boxName = view.getBoxName()

        val itemNo = view.getItemNo()
        val skuid = view.getSkuid()
        val barcode = view.getBarcode()
        val containerCode = view.getContainerCode()

        if (StringUtils.isNotBlank(boxName)) {
            params["box_name"] = boxName
        }
        if (StringUtils.isNotBlank(skuid)) {
            params["sku_no"] = skuid
        }
        if (StringUtils.isNotBlank(barcode)) {
            params["barcode"] = barcode
        }
        if (StringUtils.isNotBlank(itemNo)) {
            params["item_no"] = itemNo
        }
        if (StringUtils.isNotBlank(containerCode)) {
            params["container_code"] = containerCode
        }
        params["pda"] = true

        BaseRetrofit
                .createService(IInquireApi::class.java)
                .getInquireResult(CREATE_SIGN_ORDER_URL,headerMap,params)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : DefaultObserver<InquireResultData>() {
                    override fun onComplete() {
                    }

                    override fun onNext(t: InquireResultData) {
                        view.requestInquireResult(t)
                    }

                    override fun onError(e: Throwable) {
                        if (e is HttpException && e.code() in 400..403) {
                            val string = e.response()?.errorBody()?.string()
                            if (string != null) {
                                val parseJson = JsonUtils.parseJson<HttpJsonErrorResult>(string)
                                view.requestFailed(parseJson?.message)
                            }
                        }
                    }

                })

    }
}