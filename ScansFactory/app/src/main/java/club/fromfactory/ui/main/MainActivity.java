package club.fromfactory.ui.main;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import butterknife.BindView;
import club.fromfactory.R;
import club.fromfactory.baselibrary.router.RouterConstants;
import club.fromfactory.baselibrary.router.RouterManager;
import club.fromfactory.baselibrary.utils.PreferenceStorageUtils;
import club.fromfactory.baselibrary.utils.StringUtils;
import club.fromfactory.baselibrary.utils.ToastUtils;
import club.fromfactory.baselibrary.view.BaseMVPActivity;
import club.fromfactory.baselibrary.view.recyclerview.BaseRecyclerItemViewClickListener;
import club.fromfactory.routerannotaions.Router;
import club.fromfactory.scan.ScanViewUtilsKt;
import club.fromfactory.ui.main.adapter.MainAdapter;
import club.fromfactory.ui.main.contract.MainContract;
import club.fromfactory.ui.main.model.PermissionModel;
import club.fromfactory.ui.main.presenter.MainPresenter;
import club.fromfactory.utils.Utils;

@Router(RouterConstants.MAIN)
public class MainActivity extends BaseMVPActivity<MainContract.Presenter> implements MainContract.View {

    private final int REQUEST_TYPE_MOVEOUT_LIST = 1001;
    public final int IS_EXIT = 1000;
    @BindView(R.id.txt_show_account)
    TextView txtShowAccount;

    @BindView(R.id.main_rectcler_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.main_progress)
    View mProgress;

    @BindView(R.id.main_txt_version)
    TextView mTxtVersion;


    private boolean isExit = false;

    private MainAdapter mMainAdapter;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_main;
    }

    @Override
    public void initData() {
        super.initData();

    }

    @Override
    public void initView() {
        super.initView();

        mTxtVersion.setText(getResources().getString(R.string.version_prompt) + Utils.getApplicationVersionName(this));

        String username = PreferenceStorageUtils.getInstance().getUserName();
        txtShowAccount.setText(username);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        mRecyclerView.setLayoutManager(gridLayoutManager);
        mMainAdapter = new MainAdapter();
        mRecyclerView.setAdapter(mMainAdapter);
        mMainAdapter.setOnItemViewClickListener(new BaseRecyclerItemViewClickListener<PermissionModel>() {
            @Override
            public void onItemViewClick(@Nullable PermissionModel data, View clickedView, int position) {
                RouterManager.open(MainActivity.this, data.getRouterUrl());
            }
        });
    }

    @Override
    public void fetchData() {
        super.fetchData();
        presenter.getUserPermission();
        getOffShelfTask();
    }

    @Override
    public void clickKeyCodeBack() {
        ToQuitTheApp();
    }


    private void getOffShelfTask() {
        presenter.getOffShelfTask();
    }

    private Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {

                case IS_EXIT: {
                    isExit = false;
                }
                break;

                default:
                    break;
            }
            return false;
        }
    });

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_TYPE_MOVEOUT_LIST: {
                getOffShelfTask();
            }
            break;
            default:
                break;
        }
    }

    /**
     * 点两次退出
     */
    private void ToQuitTheApp() {
        if (isExit) {
            finish();
        } else {
            isExit = true;
            Toast.makeText(MainActivity.this, "Click again to exit", Toast.LENGTH_SHORT).show();
            mHandler.sendEmptyMessageDelayed(IS_EXIT, 3000);// 3秒后发送消息
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        ScanViewUtilsKt.setMScanView(null);

        PreferenceStorageUtils.getInstance().saveUserName("");
        PreferenceStorageUtils.getInstance().savePassword("");
        PreferenceStorageUtils.getInstance().saveToken("");
    }

    @Override
    public void showFailedError(String message) {
        ToastUtils.show(StringUtils.isNotBlank(message) ? message : "");
    }

    @Override
    public void showLoadingView() {
        mProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadingView() {
        mProgress.setVisibility(View.GONE);
    }

    @Override
    public void refreshModelDada(@NotNull List<PermissionModel> permissionModelList) {
        if (permissionModelList.size() > 0) {
            mMainAdapter.clear();
            mMainAdapter.addAll(permissionModelList);
        } else {
            ToastUtils.show("no permission");
        }

    }

    @Override
    public void refreshModelDada() {
        mMainAdapter.notifyDataSetChanged();
    }

    @Override
    public List<PermissionModel> getModelDadaList() {
        return mMainAdapter.getData();
    }

    @NonNull
    @Override
    public MainContract.Presenter createPresenter() {
        return new MainPresenter(this);
    }


}
