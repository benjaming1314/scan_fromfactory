package club.fromfactory.ui.sign.model;

import java.util.List;

import club.fromfactory.baselibrary.model.NoProguard;

/**
 * Created by lxm on 2017/7/26.
 */

public class TrackingsNoDetailModel implements NoProguard {

    private int count ;
    private String next ;
    private String previous ;
    private List<TrackingsNoDetail> results ;


    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public String getPrevious() {
        return previous;
    }

    public void setPrevious(String previous) {
        this.previous = previous;
    }

    public List<TrackingsNoDetail> getResults() {
        return results;
    }

    public void setResults(List<TrackingsNoDetail> results) {
        this.results = results;
    }

    @Override
    public String toString() {
        return "TrackingsNoModel{" +
                "count=" + count +
                ", next='" + next + '\'' +
                ", previous='" + previous + '\'' +
                ", results=" + results +
                '}';
    }
}
