package club.fromfactory.ui.main.adapter;

import android.support.annotation.NonNull;
import android.view.ViewGroup;

import club.fromfactory.baselibrary.view.recyclerview.BaseRecyclerAdapter;
import club.fromfactory.baselibrary.view.recyclerview.BaseViewHolder;
import club.fromfactory.ui.main.model.PermissionModel;
import club.fromfactory.ui.main.viewholers.MainViewHolders;

public class MainAdapter extends BaseRecyclerAdapter<PermissionModel> {

    @Override
    public BaseViewHolder<PermissionModel> onCreateBaseViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MainViewHolders(parent);
    }
}
