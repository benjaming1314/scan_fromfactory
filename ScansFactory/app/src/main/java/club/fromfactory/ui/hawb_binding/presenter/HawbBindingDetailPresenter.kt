package club.fromfactory.ui.hawb_binding.presenter

import club.fromfactory.FFApplication
import club.fromfactory.R
import club.fromfactory.baselibrary.language.LanguageUtils
import club.fromfactory.baselibrary.model.EmptyResponse
import club.fromfactory.baselibrary.net.CookieHelper
import club.fromfactory.baselibrary.net.retrofit.BaseRetrofit
import club.fromfactory.baselibrary.presenter.BasePresenter
import club.fromfactory.baselibrary.rx.loadingView
import club.fromfactory.baselibrary.utils.JsonUtils
import club.fromfactory.baselibrary.utils.PreferenceStorageUtils
import club.fromfactory.baselibrary.view.RxAppCompatActivity
import club.fromfactory.constant.Constants
import club.fromfactory.http.NetUtils
import club.fromfactory.http.model.HttpJsonErrorResult
import club.fromfactory.pojo.HttpJsonResult
import club.fromfactory.ui.hawb_binding.HawbBindingApi
import club.fromfactory.ui.hawb_binding.contract.HawbBindingDetailContract
import club.fromfactory.ui.hawb_binding.model.HawbBindingResult
import club.fromfactory.ui.tracking.model.PackageInfo
import com.trello.rxlifecycle2.kotlin.bindToLifecycle
import io.reactivex.observers.DefaultObserver
import retrofit2.HttpException
import java.util.*

class HawbBindingDetailPresenter(view: HawbBindingDetailContract.View) : BasePresenter<HawbBindingDetailContract.View>(view), HawbBindingDetailContract.Presenter {

    private var HAWB_BINDING_URL = NetUtils.APP_MAIN_URL + "wms/pda/hawb/"
    private var HAWB_UNBINDING_URL = NetUtils.APP_MAIN_URL + "wms/pda/hawb/"

    override fun hawbBindPackage(packageNo: String, is_confirmed: Boolean) {
        val token = PreferenceStorageUtils.getInstance().token

        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        //设置参数，访问url获取json
        val params = HashMap<String, Any>()
        params["large_parcel"] = packageNo
        params["is_confirmed"] = is_confirmed

        BaseRetrofit
                .createService(HawbBindingApi::class.java)
                .bindPackage(HAWB_BINDING_URL + view.getHawbId()+"/", headerMap, params)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : DefaultObserver<HttpJsonResult<HawbBindingResult>>() {
                    override fun onComplete() {
                    }

                    override fun onNext(baseResponse: HttpJsonResult<HawbBindingResult>) {

                        if (baseResponse.is_success) {
                            val packageInfo = PackageInfo()
                            packageInfo.largeParcelName = packageNo
                            view.hawbBindPackageSuccess(packageInfo)
                        }else if (baseResponse.data?.needConfirmed == true){
                            view.hawbBindPackageNeedComfirm(baseResponse.message)
                        }else {
                            view.showFailedErrorWindow(baseResponse.message)
                        }

                    }

                    override fun onError(e: Throwable) {
                        if (e is HttpException && e.code() in 400..403) {
                            val string = e.response()?.errorBody()?.string()
                            if (string != null) {
                                val parseJson = JsonUtils.parseJson<HttpJsonErrorResult>(string)
                                view.showFailedErrorWindow(parseJson?.message)
                            }
                        } else {
                            view.showFailedErrorWindow(FFApplication.getInstance().resources.getString(R.string.net_error))
                        }
                    }

                })
    }

    override fun hawbUnBindPackage(packageNo: String) {
        val token = PreferenceStorageUtils.getInstance().token

        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        //设置参数，访问url获取json
        val params = HashMap<String, String>()
        params["large_parcel"] = packageNo

        BaseRetrofit
                .createService(HawbBindingApi::class.java)
                .unBindPackage(HAWB_UNBINDING_URL + view.getHawbId()+"/", headerMap, params)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : DefaultObserver<HttpJsonResult<EmptyResponse>>() {
                    override fun onComplete() {
                    }

                    override fun onNext(baseResponse: HttpJsonResult<EmptyResponse>) {
                        if (baseResponse.is_success){
                            view.hawbUnBindPackageSuccess()
                        }else {
                            view.showFailedErrorWindow(baseResponse.message)
                        }

                    }

                    override fun onError(e: Throwable) {
                        if (e is HttpException && e.code() in 400..403) {
                            val string = e.response()?.errorBody()?.string()
                            if (string != null) {
                                val parseJson = JsonUtils.parseJson<HttpJsonErrorResult>(string)
                                view.showFailedErrorWindow(parseJson?.message)
                            }
                        } else {
                            view.showFailedErrorWindow(FFApplication.getInstance().resources.getString(R.string.net_error))
                        }
                    }

                })
    }
}