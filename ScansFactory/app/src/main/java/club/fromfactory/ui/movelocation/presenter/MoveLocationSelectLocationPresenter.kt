package club.fromfactory.ui.movelocation.presenter

import club.fromfactory.FFApplication
import club.fromfactory.R
import club.fromfactory.baselibrary.language.LanguageUtils
import club.fromfactory.baselibrary.model.EmptyResponse
import club.fromfactory.baselibrary.net.CookieHelper
import club.fromfactory.baselibrary.net.retrofit.BaseRetrofit
import club.fromfactory.baselibrary.presenter.BasePresenter
import club.fromfactory.baselibrary.rx.loadingView
import club.fromfactory.baselibrary.utils.JsonUtils
import club.fromfactory.baselibrary.utils.PreferenceStorageUtils
import club.fromfactory.baselibrary.view.RxAppCompatActivity
import club.fromfactory.constant.Constants
import club.fromfactory.http.NetUtils
import club.fromfactory.http.model.HttpJsonErrorResult
import club.fromfactory.pojo.HttpJsonResult
import club.fromfactory.ui.common.model.Goods
import club.fromfactory.ui.movelocation.IMoveLocationApi
import club.fromfactory.ui.movelocation.contract.MoveLocationSelectLocationContract
import com.trello.rxlifecycle2.kotlin.bindToLifecycle
import io.reactivex.observers.DefaultObserver
import retrofit2.HttpException
import java.util.*

class MoveLocationSelectLocationPresenter(view: MoveLocationSelectLocationContract.View) : BasePresenter<MoveLocationSelectLocationContract.View>(view), MoveLocationSelectLocationContract.Presenter {

    private var skuMoveUrl = NetUtils.APP_MAIN_URL + "wms/pda/sku_move"
    private var charmLocaitonUrl = NetUtils.APP_MAIN_URL + "wms/pda/check_box"


    override fun charmLocationAndSku() {

        val token = PreferenceStorageUtils.getInstance().token

        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        //设置参数，访问url获取json
        val params = HashMap<String, Any>()
        params["box_name"] = view.getMoveOutLocation()
        params["barcode"] = view.getMoveOutSku()

        BaseRetrofit
                .createService(IMoveLocationApi::class.java)
                .charmLocationAndSku(skuMoveUrl, headerMap, params)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : DefaultObserver<Goods>() {
                    override fun onComplete() {
                    }

                    override fun onNext(goods: Goods) {
                        view.charmSkuSuccess(goods)
                    }

                    override fun onError(e: Throwable) {
                        if (e is HttpException && e.code() in 400..403) {
                            val string = e.response()?.errorBody()?.string()
                            if (string != null) {
                                val parseJson = JsonUtils.parseJson<HttpJsonErrorResult>(string)
                                view.showFailedError(parseJson?.message)
                            }
                        } else {
                            view.showFailedError(FFApplication.getInstance().resources.getString(R.string.net_error))
                        }
                    }

                })
    }

    override fun charmLocation() {

        val token = PreferenceStorageUtils.getInstance().token

        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        //设置参数，访问url获取json
        val params = HashMap<String, Any>()
        params["box_name"] = view.getMoveOutLocation()

        BaseRetrofit
                .createService(IMoveLocationApi::class.java)
                .charmLocation(charmLocaitonUrl, headerMap, params)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : DefaultObserver<HttpJsonResult<EmptyResponse>>() {
                    override fun onComplete() {
                    }

                    override fun onNext(baseResponse: HttpJsonResult<EmptyResponse>) {
                        view.charmLocationResult(true)
                    }

                    override fun onError(e: Throwable) {
                        if (e is HttpException && e.code() in 400..403) {
                            val string = e.response()?.errorBody()?.string()
                            if (string != null) {
                                val parseJson = JsonUtils.parseJson<HttpJsonErrorResult>(string)
                                view.charmLocationResult(false, parseJson?.message)
                            }
                        } else {
                            view.charmLocationResult(false, FFApplication.getInstance().resources.getString(R.string.net_error))
                        }
                    }

                })


    }

}