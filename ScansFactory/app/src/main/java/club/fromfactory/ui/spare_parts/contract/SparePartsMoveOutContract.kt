package club.fromfactory.ui.spare_parts.contract

import club.fromfactory.baselibrary.presenter.IPresenter
import club.fromfactory.baselibrary.view.IBaseMVPView
import club.fromfactory.ui.spare_parts.model.SparePartsMoveOutData

class SparePartsMoveOutContract {

    interface View :IBaseMVPView<Presenter> {
        fun requestFailed(message: String?)

        fun takeOffBarcodeFailed(message: String?)
        fun initCheckResponce(sparePartsMoveOutData: SparePartsMoveOutData?)
        fun takeOffBarcodeSuccess()

        fun takeOffBarcodeRequestBoxName(message: String?)

        fun takeOffOverSuccess()
    }
    interface Presenter:IPresenter<View> {

        fun initCheck()
        fun takeOffBarcode(barcode:String,boxName :String?)
        fun takeOffOver()
    }
}