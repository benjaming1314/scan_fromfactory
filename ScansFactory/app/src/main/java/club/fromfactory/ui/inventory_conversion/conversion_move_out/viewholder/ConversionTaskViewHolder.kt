package club.fromfactory.ui.inventory_conversion.conversion_move_out.viewholder

import android.view.ViewGroup
import club.fromfactory.R
import club.fromfactory.baselibrary.view.recyclerview.BaseViewHolder
import club.fromfactory.ui.inventory_conversion.conversion_move_out.model.ConversionTaskInfo

class ConversionTaskViewHolder(parent :ViewGroup) :BaseViewHolder<ConversionTaskInfo>(parent, R.layout.conversion_task_item) {


    override fun bindData(data: ConversionTaskInfo) {
        super.bindData(data)
    }
}