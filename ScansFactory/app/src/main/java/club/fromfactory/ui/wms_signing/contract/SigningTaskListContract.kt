package club.fromfactory.ui.wms_signing.contract

import club.fromfactory.baselibrary.presenter.IPresenter
import club.fromfactory.baselibrary.view.IBaseMVPView
import club.fromfactory.ui.wms_signing.model.OrderDetailInfo

class SigningTaskListContract {

    interface View : IBaseMVPView<Presenter> {
        fun requestCreateSignOrderSucceed( orderDetailInfo: OrderDetailInfo)
        fun requestFailed(message :String?)

        fun requestSignOrderListSucceed(orderDetailInfoList : List<OrderDetailInfo>)
    }

    interface Presenter : IPresenter<View> {

        fun getSignedOrderList(type: String)

        fun createSignOrder(type: String)
    }
}