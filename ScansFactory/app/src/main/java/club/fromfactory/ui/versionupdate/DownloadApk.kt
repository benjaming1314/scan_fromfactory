package club.fromfactory.ui.versionupdate

import android.annotation.SuppressLint
import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.support.v4.content.FileProvider
import android.view.Window
import android.widget.ProgressBar
import android.widget.TextView
import club.fromfactory.BuildConfig
import club.fromfactory.FFApplication
import club.fromfactory.R
import club.fromfactory.baselibrary.utils.ScreenUtils
import club.fromfactory.baselibrary.utils.StorageUtils
import club.fromfactory.baselibrary.utils.ThreadUtils
import club.fromfactory.baselibrary.view.IBaseView
import club.fromfactory.baselibrary.widget.CustomDialog
import club.fromfactory.http.FileDownLoaderListener
import club.fromfactory.http.download_file.OkHttpFileDownLoader
import club.fromfactory.utils.MDFive
import club.fromfactory.utils.Zlog
import java.io.File

@SuppressLint("StaticFieldLeak")
object DownloadApk {

    private var mDownloadDialogCallback: DownloadDialogCallback? = null

    private var mDownloadBroadcast: DownloadBroadcast? = null


    private var breakPoint :Long ?= 0


    private var mDialog: CustomDialog? = null
    private var mTxtOk: TextView? = null
    private var mTxtProgerss: TextView? = null

    private var mProgressStyle: ProgressBar? = null

    private var okHttpFileDownLoader: OkHttpFileDownLoader? = null


    fun isDownloadComplete(url: String): Boolean {

        var isDownloadComplete = false
        // 设置下载文件的保存位置
        val file = File(StorageUtils.getPath() + File.separator + getFileName(url))
        if (file.exists() && getUninatllApkInfo(file.path)) {
            isDownloadComplete = true
        }
        return isDownloadComplete
    }

    private fun getUninatllApkInfo(filePath: String): Boolean {
        var result = false
        try {
            val pm = FFApplication.getInstance().packageManager
            val info = pm.getPackageArchiveInfo(filePath, PackageManager.GET_ACTIVITIES)
            if (info != null) {
                result = true//完整
            }
        } catch (e: Exception) {
            result = false//不完整
        }
        return result
    }

//    private fun getDownloadLength(url: String): Long {
//        // 设置下载文件的保存位置
//        val file = File(StorageUtils.getPath() + File.separator + getFileName(url))
//
//        if (file.exists()) {
//            return file.length()
//        }
//        return 0
//
//    }

    fun download(iBaseView: IBaseView, url: String, downloadDialogCallback: DownloadDialogCallback?) {

        mDownloadDialogCallback = downloadDialogCallback

        val fileName = getFileName(url)

        // 设置下载文件的保存位置

        if (isDownloadComplete(url)) {
            installApk(iBaseView, url)
            return
        }
        showDownload(iBaseView)

        okHttpFileDownLoader = OkHttpFileDownLoader(iBaseView, StorageUtils.getPath(), fileName,
                object : FileDownLoaderListener {

                    override fun onDownloading(progress: Int, currentLength: Long) {
                        breakPoint = currentLength
                        ThreadUtils.runOnUi {
                            updateProgress(iBaseView, progress)
                        }
                    }

                    override fun onSuccess() {
                        finishDialog()
                        installApk(iBaseView, url)
                        mDownloadDialogCallback?.downloadSuccess()
                    }

                    override fun onFail(code: Int, msg: String?) {
                        mDownloadDialogCallback?.downloadFailed(msg)

                    }
                })




        okHttpFileDownLoader?.startDownload(url, breakPoint?:0)
    }

    private fun showDownload(iBaseView: IBaseView) {

        mDialog = CustomDialog(iBaseView.context)
        mDialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        mDialog?.setContentView(R.layout.progress_dialog)
        val window = mDialog?.window
        val lp = window?.attributes
        val screenW = ScreenUtils.getScreenWidth()
        lp?.width = (0.8 * screenW).toInt()
        mDialog?.setCanceledOnTouchOutside(false)

        mProgressStyle = mDialog?.findViewById(R.id.progress_dialog_progressbar)
        mTxtProgerss = mDialog?.findViewById(R.id.progress_dialog_txt_progress)
        mTxtOk = mDialog?.findViewById(R.id.progress_dialog_ok)

        mTxtOk?.setOnClickListener {

        }

        mDialog?.setOnDismissListener {
            finishDialog()
            okHttpFileDownLoader?.cancelDownload()
            mDownloadDialogCallback?.cancelSuccess()
        }
        mProgressStyle?.progress = 0
        mTxtProgerss?.text = "0%"

        mDialog?.show()
    }

    @SuppressLint("SetTextI18n")
    fun updateProgress(iBaseView: IBaseView, progress: Int) {

        if (mDialog == null) {
            showDownload(iBaseView)
        }
        mProgressStyle?.progress = progress
        mTxtProgerss?.text = "$progress%"

    }

    private fun finishDialog() {

        if (mDialog != null) {
            if (mDialog?.isShowing == true) {
                mDialog?.dismiss()
            }
            mDialog = null
        }

    }

    private fun getFileName(url: String): String {
        return MDFive.getMD5(url) + ".apk"
    }

    fun installApk(iBaseView: IBaseView, url: String) {
        // 设置下载文件的保存位置
        val file = File(StorageUtils.getPath() + File.separator + getFileName(url))

        val intent1 = Intent(Intent.ACTION_VIEW);
        intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            intent1.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            val uri1 = FileProvider.getUriForFile(iBaseView.context, BuildConfig.APPLICATION_ID + ".fileProvider", file)
            intent1.setDataAndType(uri1, "application/vnd.android.package-archive");
        } else {
            intent1.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive")
        }
        try {
            iBaseView.context.startActivity(intent1)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * 通过浏览器
     */
    fun downloadApkFromBrow(iBaseView: IBaseView, url: String) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.addCategory(Intent.CATEGORY_BROWSABLE)
        intent.data = Uri.parse(url)
        iBaseView.context.startActivity(intent)
    }

    /**
     * 使用系统下载器
     */
    fun downloadApk(iBaseView: IBaseView, url: String) {

        val fileName = MDFive.getMD5(url) + ".apk"

        val request = DownloadManager.Request(Uri.parse(url))
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)

        request.setMimeType("application/vnd.android.package-archive")

        // 设置通知的标题和描述
        request.setTitle(FFApplication.getInstance().resources.getString(R.string.app_name))
        request.setVisibleInDownloadsUi(true)
        // 设置下载文件的保存位置
        val saveFile = File(Environment.getExternalStorageDirectory(), "scan/$fileName")
        request.setDestinationUri(Uri.fromFile(saveFile))

        val manager = iBaseView.context.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager

        val downloadId = manager.enqueue(request)

        Zlog.ii("lxm downloadApk:$downloadId")

        //文件下载完成会发送完成广播，可注册广播进行监听
        val intentFilter = IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        intentFilter.addAction(DownloadManager.ACTION_NOTIFICATION_CLICKED);
        intentFilter.addAction(DownloadManager.ACTION_VIEW_DOWNLOADS);

        mDownloadBroadcast = DownloadBroadcast(saveFile)
        iBaseView.context.registerReceiver(mDownloadBroadcast, intentFilter)

    }

    fun onDestroy(iBaseView: IBaseView) {
        if (mDownloadBroadcast != null) {
            iBaseView.context.unregisterReceiver(mDownloadBroadcast);
        }
    }
}


interface DownloadDialogCallback {
    fun downloadSuccess()
    fun cancelSuccess()
    fun downloadFailed(msg: String?)
}

class DownloadBroadcast(var file: File) : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {

        val action = intent?.action
        if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
            val intent1 = Intent(Intent.ACTION_VIEW);
            intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                intent1.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                val uri1 = FileProvider.getUriForFile(context!!, BuildConfig.APPLICATION_ID + ".fileProvider", file)
                intent1.setDataAndType(uri1, "application/vnd.android.package-archive");
            } else {
                intent1.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive")
            }
            try {
                context?.startActivity(intent1)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }


}