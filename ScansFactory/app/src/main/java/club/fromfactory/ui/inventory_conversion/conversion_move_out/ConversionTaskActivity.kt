package club.fromfactory.ui.inventory_conversion.conversion_move_out

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.View
import club.fromfactory.R
import club.fromfactory.baselibrary.router.RouterConstants
import club.fromfactory.baselibrary.utils.StringUtils
import club.fromfactory.baselibrary.utils.ToastUtils
import club.fromfactory.routerannotaions.Router
import club.fromfactory.ui.inventory_conversion.conversion_move_out.contract.ConversionTaskContract
import club.fromfactory.ui.inventory_conversion.conversion_move_out.model.ConversionTaskInfo
import club.fromfactory.ui.inventory_conversion.conversion_move_out.presenter.ConversionTaskPresenter
import club.fromfactory.ui.main.ScanBaseMVPActivity
import club.fromfactory.utils.EditTextUtils
import club.fromfactory.widget.CustomTitleLinearLayout
import kotlinx.android.synthetic.main.activity_conversion_task.*

/**
 * 转换下架任务页面
 */
@Router(RouterConstants.CONVERSION_TASK)
class ConversionTaskActivity : ScanBaseMVPActivity<ConversionTaskContract.Presenter>(), ConversionTaskContract.View {
    override fun createPresenter(): ConversionTaskContract.Presenter = ConversionTaskPresenter(this)

    companion object {

        private const val TYPE_WAITING_REMOVE = "waiting_remove"
        private const val TYPE_ASK_WAITING_REMOVE = "ask_waiting_remove"


        private const val REQUEST_CODE_DETAIL = 100
    }


    private var mConversionTaskInfo: ConversionTaskInfo? = null

    private var isHandleScanResult = false

    override fun requestScanStart() {
//        if (Zlog.isDebug) {
//
//            requestScanResult("M0001")
//        }
    }

    override fun clickKeyCodeBack() {
        finish()
    }

    override fun getLayoutResId(): Int = R.layout.activity_conversion_task

    override fun onResume() {
        super.onResume()
    }


    override fun initData() {
        super.initData()
    }

    override fun initView() {
        super.initView()


        conversion_task_ctl_title.setListener(object : CustomTitleLinearLayout.CustomTitleListener() {

            override fun clickLeft() {
                super.clickLeft()
                finish()
            }
        })

        setScanListener(conversion_task_scan)


//        showTaskDialog()

        conversion_task_btn_ok.setOnClickListener {
            bindContainer()
        }
        conversion_task_request.setOnClickListener {
            getConversionTaskNum(TYPE_ASK_WAITING_REMOVE)
        }
        EditTextUtils.addEditTextListener(conversion_task_edt_bind_container)

    }

    override fun fetchData() {
        super.fetchData()
        getConversionTaskNum(TYPE_WAITING_REMOVE)
    }


//    private fun showTaskDialog() {
//
//        val msg = resources.getString(R.string.get_conversion_out_task)
//        PromptDialogUtils.getInstance().showDialog(this@ConversionTaskActivity, msg, object : PromptDialogUtils.DialogCallback {
//            override fun onClickPositiveBtn() {
//                getConversionTaskNum(TYPE_WAITING_REMOVE)
//
//            }
//
//            override fun onClickNegativeBtn() {
//                finish()
//            }
//
//        })
//    }

    override fun requestScanResult(result: String?) {

        //当没有任务时，不支持扫描容器
        if (conversion_task_scrollview.visibility == View.GONE) {
            return
        }

        if (isHandleScanResult) {
            return
        } else {
            isHandleScanResult = true
        }

        if (StringUtils.isNotBlank(result)) {
            conversion_task_edt_bind_container.setText(result)
            bindContainer()
        } else {
            isHandleScanResult = false
            ToastUtils.show(resources.getString(R.string.scan_prompt_10))
        }

    }

    private fun getConversionTaskNum(type: String) {
        presenter.getConversionTask(type)
    }

    private fun bindContainer() {
        val string = conversion_task_edt_bind_container.text.toString()

        val containerCode = mConversionTaskInfo?.containerCode

        if (StringUtils.isNotBlank(string) && StringUtils.isNotBlank(containerCode)) {
            if (containerCode == string) {
                bindContainerSucceed()
            } else {
                isHandleScanResult = false
                ToastUtils.show(resources.getString(R.string.scan_prompt_not_match_container))
            }

        } else if (StringUtils.isNotBlank(string)) {
            presenter.bindContainer(mConversionTaskInfo?.cvId ?: 0, string)
        } else {
            isHandleScanResult = false
            ToastUtils.show(resources.getString(R.string.scan_prompt_no_container))
        }


    }

    override fun bindContainerSucceed() {

        isHandleScanResult = false
        ConversionTaskDetailActivity.launchActivity(this, mConversionTaskInfo?.cvId
                ?: 0, REQUEST_CODE_DETAIL)

    }

    override fun requestSuccess(conversionTaskInfo: ConversionTaskInfo) {

        conversion_task_scrollview.visibility = View.VISIBLE

        mConversionTaskInfo = conversionTaskInfo

        val cvName = conversionTaskInfo.cvName
        val containerCode = conversionTaskInfo.containerCode

        conversion_task_txt_conversion_num.text = if (StringUtils.isNotBlank(cvName)) cvName else ""
        conversion_task_txt_task_num.text = conversionTaskInfo.taskCount.toString()
        conversion_task_txt_binded_container.text = if (StringUtils.isNotBlank(containerCode)) containerCode else ""

    }

    override fun requestSuccess() {

        conversion_task_scrollview.visibility = View.GONE
    }

    override fun requestFailed(message: String?) {
        isHandleScanResult = false
        ToastUtils.show(message ?: "")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)


        if (resultCode == Activity.RESULT_OK) {

            when (requestCode) {

                REQUEST_CODE_DETAIL -> {
                    val intent = intent
                    finish()
                    startActivity(intent)
                }

            }

        } else {

            fetchData()

        }
    }

    override fun showLoadingView() {
        showLoadingView(conversion_task_progress)
    }

    override fun hideLoadingView() {
        hideLoadingView(conversion_task_progress)
    }
}
