package club.fromfactory.ui.inventory.contract

import club.fromfactory.baselibrary.presenter.IPresenter
import club.fromfactory.baselibrary.view.IBaseMVPView
import club.fromfactory.pojo.QueryGoodsState
import club.fromfactory.ui.common.model.Goods

class InventoryGoodsListContract {

    interface View : IBaseMVPView<Presenter> {

        fun isOrderLocation(): Boolean

        fun getBoxName(): String
        fun refreshData(goodsList: List<Goods>)

        fun goodsStateList(queryGoodsStatesList: List<QueryGoodsState>)
        fun goodsStateListFailed()

        fun scanConfirmSucceed(goods: Goods)
        fun scanConfirmFailed(message: String?)

        fun alterGoodStateSucceed(goods: Goods)
        fun alterGoodStateFailed(message: String?)

        fun showFailedError(message: String?)

    }

    interface Presenter : IPresenter<View> {

        fun checkBoxList()
        fun getQueryStatus()
        fun checkSkuNo(barcode: String)
        fun checkSkuAlterState(id: Int, barcode: String, to_status: String, from_status: String)

    }
}