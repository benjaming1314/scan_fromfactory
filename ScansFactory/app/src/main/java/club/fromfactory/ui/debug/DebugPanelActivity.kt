package club.fromfactory.ui.debug

import club.fromfactory.R
import club.fromfactory.baselibrary.utils.PreferenceStorageUtils
import club.fromfactory.baselibrary.view.BaseMVPActivity
import club.fromfactory.ui.debug.contract.DebugPannelContract
import club.fromfactory.ui.debug.presenter.DebugPannelPresenter
import club.fromfactory.utils.Zlog
import club.fromfactory.widget.CustomTitleLinearLayout
import kotlinx.android.synthetic.main.activity_debug_pannel.*

/**
 *@author lxm
 *@date 2018/11/7
 */
class DebugPanelActivity : BaseMVPActivity<DebugPannelContract.Presenter>(), DebugPannelContract.View {

    private var isDevelopmentCheck = false

    override fun getLayoutResId(): Int = R.layout.activity_debug_pannel

    override fun initData() {
        super.initData()
    }

    override fun initView() {
        super.initView()
        ctl_title.setRightView(R.layout.edit_introduce_save)
        ctl_title.setRightVisible(true)
        ctl_title.setListener(object : CustomTitleLinearLayout.CustomTitleListener() {
            override fun clickLeft() {
                super.clickLeft()
                finish()
            }

            override fun clickRight() {
                super.clickRight()
                save()
            }
        })

        //开关
        debug_panel_mode.setOnCheckedChangeListener { buttonView, isChecked ->
            Zlog.isDebug = isChecked
            PreferenceStorageUtils.getInstance().saveDebugState(isChecked)

        }
        development_panel_mode.setOnCheckedChangeListener { buttonView, isChecked ->
            isDevelopmentCheck = isChecked
        }
        afterInitViews()
    }

    private fun afterInitViews() {
        debug_panel_mode.isChecked = Zlog.isDebug
        development_panel_mode.isChecked = PreferenceStorageUtils.getInstance().developentState
    }

    private fun save() {

        if (isDevelopmentCheck == PreferenceStorageUtils.getInstance().developentState) {
            finish()
        }else {
            PreferenceStorageUtils.getInstance().warehousePdaInfo = ""
            PreferenceStorageUtils.getInstance().saveDevelopentState(isDevelopmentCheck)
            PreferenceStorageUtils.getInstance().saveIsFirstInstall(true)
//            PreferenceStorageUtils.getInstance().clearIsFirstData()

            // 等待500ms等数据刷新完成
            Thread.sleep(500)

            relaunchApp()
            finish()
        }
    }


    private fun relaunchApp() {
        com.blankj.utilcode.util.AppUtils.relaunchApp()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun createPresenter(): DebugPannelContract.Presenter = DebugPannelPresenter(this)
}