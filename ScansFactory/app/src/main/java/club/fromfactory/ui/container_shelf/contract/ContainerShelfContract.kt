package club.fromfactory.ui.container_shelf.contract

import club.fromfactory.baselibrary.presenter.IPresenter
import club.fromfactory.baselibrary.view.IBaseMVPView
import club.fromfactory.ui.container_shelf.model.ContainerResponseData

class ContainerShelfContract {

    interface View: IBaseMVPView<Presenter> {

        fun requestContainerConfirmSuccess(containerResponseData: ContainerResponseData)

        fun shelfSuccess()

        fun getContainerCode():String

        fun requestFailed(message :String?)

    }
    interface Presenter: IPresenter<View> {

        fun checkContainer(str:String)

        fun shelfBox(boxName:String)

    }
}