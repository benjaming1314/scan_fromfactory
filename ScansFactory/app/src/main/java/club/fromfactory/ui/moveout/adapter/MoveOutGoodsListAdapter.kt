package club.fromfactory.ui.moveout.adapter

import club.fromfactory.baselibrary.view.recyclerview.BaseMultiTypeAdapter
import club.fromfactory.ui.common.viewholder.GoodListTitleCreator
import club.fromfactory.ui.moveout.viewholder.GoodsForMoveOutCreator

class MoveOutGoodsListAdapter : BaseMultiTypeAdapter() {

    companion object {
        const val TYPE_TITLE = 1
    }

    init {
        manager.addCreator(GoodListTitleCreator())
                .addCreator(GoodsForMoveOutCreator())
    }


}