package club.fromfactory.ui.wms_signing

import android.app.Activity
import android.content.Intent
import android.view.View
import club.fromfactory.R
import club.fromfactory.baselibrary.router.RouterConstants
import club.fromfactory.baselibrary.utils.StringUtils
import club.fromfactory.baselibrary.utils.ToastUtils
import club.fromfactory.baselibrary.view.BaseActivity
import club.fromfactory.routerannotaions.Router
import club.fromfactory.ui.main.ScanBaseMVPActivity
import club.fromfactory.ui.wms_signing.contract.WmsSigningDetailContract
import club.fromfactory.ui.wms_signing.model.OrderDetailInfo
import club.fromfactory.ui.wms_signing.model.SignPackageResult
import club.fromfactory.ui.wms_signing.presenter.WmsSigningDetailPreseter
import club.fromfactory.utils.PromptDialogUtils
import club.fromfactory.utils.TipSound
import club.fromfactory.widget.CustomTitleLinearLayout
import kotlinx.android.synthetic.main.activity_wms_signing_detail.*

@Router(RouterConstants.WMS_SIGNING_DETAIL)
class WmsSigningDetailActivity : ScanBaseMVPActivity<WmsSigningDetailContract.Presenter>(),WmsSigningDetailContract.View {

    private var mTitle :String ?= null
    private var isShowComplete :Boolean = false

    private var type :String ?= null

    var orderDetailInfo: OrderDetailInfo ?= null

    private var singCount = 0
    private var isHandleScanResult = false

    companion object {

        fun launchActivity(activity: BaseActivity,title :String , isShowComplete :Boolean ,orderDetailInfo: OrderDetailInfo) {

            val intent = Intent(activity, WmsSigningDetailActivity::class.java)
            intent.putExtra("title",title)
            intent.putExtra("is_show_complete",isShowComplete)
            intent.putExtra("order_detail_info",orderDetailInfo)
            activity.startActivity(intent)
        }
        fun launchActivity(activity: BaseActivity,title :String , isShowComplete :Boolean ,orderDetailInfo: OrderDetailInfo,requestCode :Int) {

            val intent = Intent(activity, WmsSigningDetailActivity::class.java)
            intent.putExtra("title",title)
            intent.putExtra("is_show_complete",isShowComplete)
            intent.putExtra("order_detail_info",orderDetailInfo)
            activity.startActivityForResult(intent,requestCode)
        }

    }


    override fun clickKeyCodeBack() {
        super.clickKeyCodeBack()
        setResult(Activity.RESULT_OK)
        finish()
    }
//    var click = 0
    override fun requestScanStart() {

//        if (Zlog.isDebug) {
//            if (click == 0) {
//                requestScanResult("455788")
//            }else {
//                requestScanResult("ooomoj")
//            }
//        }
//        click++
    }

    override fun getLayoutResId(): Int = R.layout.activity_wms_signing_detail

    override fun initData() {
        super.initData()
        mTitle = intent.getStringExtra("title")
        isShowComplete = intent.getBooleanExtra("is_show_complete",false)
        orderDetailInfo = intent.getSerializableExtra("order_detail_info") as OrderDetailInfo?

        type= orderDetailInfo?.type.toString()

        singCount = orderDetailInfo?.signCount?:0
    }

    override fun initView() {
        super.initView()
        signing_type_detail_ctl_title.setTitleCenter(mTitle?:"")
        signing_type_detail_ctl_title.setListener(object :CustomTitleLinearLayout.CustomTitleListener(){
            override fun clickLeft() {
                super.clickLeft()
                setResult(Activity.RESULT_OK)
                finish()
            }
        })

        setScanListener(signing_type_detail_scan)

        signing_type_detail_txt_sign_num.text = orderDetailInfo?.name

        signing_type_detail_txt_count.text = singCount.toString()


        signing_type_detail_btn_ok.setOnClickListener {
            signAwn()
        }
        signing_type_detail_complete_task.setOnClickListener {

            PromptDialogUtils.getInstance().showDialog(this@WmsSigningDetailActivity,resources.getString(R.string.signing_task_complete_task_prompt),object : PromptDialogUtils.DialogCallback{
                override fun onClickPositiveBtn() {
                    presenter.completeTask()
                }

                override fun onClickNegativeBtn() {
                }
            })


        }

        if (isShowComplete) {
            signing_type_detail_complete_task.visibility = View.VISIBLE
        }else {
            signing_type_detail_complete_task.visibility = View.GONE
        }

    }


    override fun createPresenter(): WmsSigningDetailContract.Presenter = WmsSigningDetailPreseter(this)

    override fun requestScanResult(result: String?) {


        if (isHandleScanResult) {
            return
        } else {
            isHandleScanResult = true
        }

        if (StringUtils.isNotBlank(result)) {
            signing_type_detail_edt_parcel_num.setText(result)
            signAwn()
        }else {
            isHandleScanResult = false
            ToastUtils.show(resources.getString(R.string.scan_sign_num_prompt))
        }
    }

    private fun signAwn() {
        val toString = signing_type_detail_edt_parcel_num.text.toString()

        if (StringUtils.isNull(toString)) {
            isHandleScanResult = false
            ToastUtils.show(resources.getString(R.string.scan_sign_num_prompt))

            return
        }
        presenter.signAwb(toString)
    }

    override fun requestSignAwbSucceed(signPackageResult: SignPackageResult) {

        singCount = signPackageResult.count?:0

        isHandleScanResult = false
        TipSound.getInstance().playScanSucceedSound(this)

        signing_type_detail_txt_parcel_num.text = signing_type_detail_edt_parcel_num.text.toString()
        signing_type_detail_edt_parcel_num.setText("")

        signing_type_detail_txt_count.text = singCount.toString()

        signing_type_detail_state_right.visibility = View.VISIBLE
        signing_type_detail_txt_parcel_num_state_error.visibility = View.GONE
        signing_type_detail_txt_error.visibility = View.GONE

        signing_type_detail_txt_tagView.visibility = View.VISIBLE
        signing_type_detail_txt_tagView.text = signPackageResult.tagView

    }

    override fun requestSignAwbFailed(message: String?) {
        isHandleScanResult =false
        TipSound.getInstance().playScanFailedSound(this)

        signing_type_detail_txt_parcel_num.text = signing_type_detail_edt_parcel_num.text.toString()
        signing_type_detail_edt_parcel_num.setText("")

        signing_type_detail_txt_count.text = singCount.toString()

        signing_type_detail_state_right.visibility = View.GONE
        signing_type_detail_txt_parcel_num_state_error.visibility = View.VISIBLE

        signing_type_detail_txt_error.visibility = View.VISIBLE
        signing_type_detail_txt_error.text = message

        signing_type_detail_txt_tagView.visibility = View.GONE
    }

    override fun requestCompleteTaskSuccess() {
        setResult(Activity.RESULT_OK)
        finish()
    }

    override fun requestFailed(message: String?) {
        TipSound.getInstance().playScanFailedSound(this)
        ToastUtils.show(message?:"")
    }

    override fun getType(): String = type?:""

    override fun getSignOrderId(): String = orderDetailInfo?.id.toString()

    override fun showLoadingView() {
        super.showLoadingView()
        showLoadingView(signing_type_detail_progress)
    }

    override fun hideLoadingView() {
        super.hideLoadingView()
        hideLoadingView(signing_type_detail_progress)
    }

}
