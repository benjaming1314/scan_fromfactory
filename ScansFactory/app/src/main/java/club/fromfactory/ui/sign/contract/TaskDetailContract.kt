package club.fromfactory.ui.sign.contract

import club.fromfactory.baselibrary.presenter.IPresenter
import club.fromfactory.baselibrary.view.IBaseMVPView
import club.fromfactory.ui.sign.model.TrackingsNoDetail

class TaskDetailContract {

    interface View : IBaseMVPView<Presenter> {

        fun getTaskId(): Int
        fun updateTaskSuccess()
        fun trackingsNo(trackingsNo: TrackingsNoDetail)

        fun httpResult(trackingsNoList: List<TrackingsNoDetail>)
        fun showFailedError(message: String?)

    }

    interface Presenter : IPresenter<View> {

        fun updateSignedTask()
        fun trackingNo(tracking_no: String)
        fun getTaskDetail()
    }
}