package club.fromfactory.ui.wms_signing

import android.content.Intent
import android.view.View
import club.fromfactory.R
import club.fromfactory.baselibrary.router.RouterConstants
import club.fromfactory.baselibrary.router.RouterManager
import club.fromfactory.baselibrary.router.RouterUrlProvider
import club.fromfactory.baselibrary.utils.DensityUtils
import club.fromfactory.baselibrary.utils.ToastUtils
import club.fromfactory.baselibrary.view.BaseMVPActivity
import club.fromfactory.baselibrary.view.recyclerview.BaseSpaceItemDecoration
import club.fromfactory.routerannotaions.Router
import club.fromfactory.routerannotaions.RouterParam
import club.fromfactory.ui.wms_signing.adapter.SigningTaskListAdapter
import club.fromfactory.ui.wms_signing.contract.SigningTaskListContract
import club.fromfactory.ui.wms_signing.model.OrderDetailInfo
import club.fromfactory.ui.wms_signing.presenter.SigningTaskListPresenter
import club.fromfactory.widget.CustomTitleLinearLayout
import kotlinx.android.synthetic.main.activity_signing_task_list.*

@Router(RouterConstants.SIGNING_TASK_LIST)
class SigningTaskListActivity : BaseMVPActivity<SigningTaskListContract.Presenter>(),SigningTaskListContract.View {

    companion object {

        private const val REQUEST_CODE_DETAIL = 100
    }

    @RouterParam("title")
    @JvmField
    var mTitle :String ?= null
    @RouterParam("type")
    @JvmField
    var type :String ?= null

    override fun getLayoutResId(): Int = R.layout.activity_signing_task_list

    private var mSigningTaskListAdapter :SigningTaskListAdapter?=null


    override fun initView() {
        super.initView()
        signing_task_list_ctl_title.setTitleCenter(mTitle)
        signing_task_list_ctl_title.setListener(object : CustomTitleLinearLayout.CustomTitleListener() {
            override fun clickLeft() {
                super.clickLeft()
                finish()
            }
        })

        mSigningTaskListAdapter = SigningTaskListAdapter()
        signing_task_list_recycler_view.addItemDecoration(BaseSpaceItemDecoration(0,0,0, DensityUtils.dp2px(this,5)))
        signing_task_list_recycler_view.adapter = mSigningTaskListAdapter

        mSigningTaskListAdapter?.setOnItemViewClickListener { data, _, _ ->

            WmsSigningDetailActivity.launchActivity(this@SigningTaskListActivity,mTitle?:"",true,data!!,REQUEST_CODE_DETAIL)
        }

        signing_task_list_new_task.setOnClickListener {
            presenter.createSignOrder(type?:"")
        }
    }

    override fun fetchData() {
        super.fetchData()
        presenter.getSignedOrderList(type?:"")
    }

    override fun requestCreateSignOrderSucceed(orderDetailInfo: OrderDetailInfo) {
        WmsSigningDetailActivity.launchActivity(this@SigningTaskListActivity,mTitle?:"",true,orderDetailInfo,REQUEST_CODE_DETAIL)
    }

    override fun requestFailed(message: String?) {
        ToastUtils.show(message?:"")
    }

    override fun requestSignOrderListSucceed(orderDetailInfoList: List<OrderDetailInfo>) {
        mSigningTaskListAdapter?.clear()
        mSigningTaskListAdapter?.addAll(orderDetailInfoList)

        signing_task_list_txt_not_complete_num.text = resources.getString(R.string.old_task_list_sum_propt1,orderDetailInfoList.size.toString())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        fetchData()
    }

    override fun showLoadingView() {
        super.showLoadingView()
        if (signing_task_list_progress.visibility == View.GONE) {
            signing_task_list_progress.visibility = View.VISIBLE
        }
    }

    override fun hideLoadingView() {
        super.hideLoadingView()
        if (signing_task_list_progress.visibility == View.VISIBLE) {
            signing_task_list_progress.visibility = View.GONE
        }
    }

    override fun createPresenter(): SigningTaskListContract.Presenter = SigningTaskListPresenter(this)
}
