package club.fromfactory.ui.inventory

import android.app.AlertDialog
import android.view.View
import club.fromfactory.FFApplication
import club.fromfactory.R
import club.fromfactory.baselibrary.utils.DensityUtils
import club.fromfactory.baselibrary.utils.StringUtils
import club.fromfactory.baselibrary.utils.ToastUtils
import club.fromfactory.baselibrary.view.recyclerview.ViewHolderCreatorModel
import club.fromfactory.baselibrary.view.recyclerview.decoration.LinearDecoration
import club.fromfactory.baselibrary.widget.CustomDialog
import club.fromfactory.constant.Constants
import club.fromfactory.pojo.QueryGoodsState
import club.fromfactory.ui.common.model.Goods
import club.fromfactory.ui.common.model.Goods.Companion.SCAN_STATE_DEFAULT
import club.fromfactory.ui.common.view.IGoodsForAlterInterface
import club.fromfactory.ui.inventory.adapter.InventoryGoodsListAdapter
import club.fromfactory.ui.inventory.contract.InventoryGoodsListContract
import club.fromfactory.ui.inventory.presenter.InventoryGoodsListPresenter
import club.fromfactory.ui.main.ScanBaseMVPActivity
import club.fromfactory.ui.moveout.model.TitleModel
import club.fromfactory.utils.AlterSkuSateDialog
import club.fromfactory.utils.PromptDialogUtils
import club.fromfactory.utils.TipSound
import club.fromfactory.utils.Zlog
import club.fromfactory.view.SelectOnlickListner
import club.fromfactory.widget.CustomTitleLinearLayout
import kotlinx.android.synthetic.main.activity_inventory_goods_list_new.*

/**
 * 闲置库位 / 订单库位  X x 开头是空闲库位
 *
 */
class InventoryGoodsListActivity : ScanBaseMVPActivity<InventoryGoodsListContract.Presenter>(), InventoryGoodsListContract.View, SelectOnlickListner, IGoodsForAlterInterface {

    override fun createPresenter(): InventoryGoodsListContract.Presenter = InventoryGoodsListPresenter(this)

    private var mInventoryGoodsListAdapter: InventoryGoodsListAdapter? = null

    private var mBoxName: String? = null
    private var isOrderLocation = true  //是否是订单库位

    private var mCurrentScanGoods: Goods? = null //当前扫描商品

    private var mCurrentSkuNo: String? = null

    private var scanGoodsList = ArrayList<Goods>() //待盘点
    private var confirmGoodsList = ArrayList<Goods>()  //已经盘点


    private var mQueryGoodsStatesList = ArrayList<QueryGoodsState>()

    private var isHandleScanResult = false

    private var alterPosition: Int = -1

    private var alterGoods: Goods? = null

    private var isCanAlterStatus = true

    private var mAlterStatusName = ""

    private var alterSkuSateDialog: CustomDialog? = null
    private var isShowAlterDialog = false

    private var mCustomDialog: CustomDialog? = null

    //    var click = 0
    override fun requestScanStart() {

//        if (Zlog.isDebug) {
//
//            if (click %2 == 0) {
//                requestScanResult("0000743261")
//            }else {
//                requestScanResult("")
//            }
//            click++
//        }
    }

    override fun getLayoutResId(): Int = R.layout.activity_inventory_goods_list_new

    override fun initData() {
        super.initData()
        mBoxName = intent.getStringExtra(Constants.BOX_NAME)

        if (mBoxName?.startsWith("X") == true || mBoxName?.startsWith("x") == true) {
            isOrderLocation = false
        }
    }

    override fun initView() {
        super.initView()

        inventory_goods_list_new_ctl_title.setRightVisible(true)
        inventory_goods_list_new_ctl_title.setRightView(R.layout.right_close_layout)
        inventory_goods_list_new_ctl_title.setTitleCenter(mBoxName ?: "")
        inventory_goods_list_new_ctl_title.setListener(object : CustomTitleLinearLayout.CustomTitleListener() {

            override fun clickRight() {
                super.clickRight()
                goback()
            }
        })

        mInventoryGoodsListAdapter = InventoryGoodsListAdapter()
        inventory_goods_list_new_scan_recycler_view.addItemDecoration(LinearDecoration(DensityUtils.dp2px(this, 5), R.color.color_333333))
        inventory_goods_list_new_scan_recycler_view.adapter = mInventoryGoodsListAdapter

        setScanListener(inventory_goods_list_new_scan)

        mInventoryGoodsListAdapter?.setOnItemViewClickListener(this)
    }

    override fun fetchData() {
        super.fetchData()

        presenter.checkBoxList()
        presenter.getQueryStatus()

    }

    override fun clickKeyCodeBack() {
        goback()
    }

    override fun requestScanResult(result: String?) {

        if (scanGoodsList.size == 0) {
            ToastUtils.show(resources.getString(R.string.scan_prompt_7))
            return
        }

        if (isHandleScanResult) {
            return
        } else {
            isHandleScanResult = true
        }
        Zlog.ii("lxm ss handleResult:inventory $result  ")

        var isHasGoods = false

        for (i in 0 until scanGoodsList.size) {
            val goods = scanGoodsList[i]
            val barcodeList = goods.getBarCodeList()

            if (barcodeList.contains(result)) {
                mCurrentSkuNo = result
                isHasGoods = true
                presenter.checkSkuNo(result ?: "")
                break
            }

        }

        //，没有对应商品时
        if (!isHasGoods) {
            ToastUtils.show(resources.getString(R.string.scan_prompt_8))
            val goods = Goods()
            goods.barcodeList = Goods.getBarcodeList(result ?: "")

            goods.scanState = Goods.SCAN_STATE_FAILED
            scanResult(goods, false)

            isHandleScanResult = false
        }

    }

    private fun scanResult(resultGoods: Goods, isSucceed: Boolean) {


        Zlog.ii("lxm ss scanResult:$resultGoods $isSucceed")

        //当前的扫描一行不支持修改
        resultGoods.isAlter = false
        //处理当前扫描商品
        if (mCurrentScanGoods == null) {
            mInventoryGoodsListAdapter?.insert(resultGoods, 0)
            val tilteViewHolderCreatorModel = ViewHolderCreatorModel(InventoryGoodsListAdapter.TYPE_TITLE, TitleModel(resources.getString(R.string.current_scan_goods)))
            mInventoryGoodsListAdapter?.insert(tilteViewHolderCreatorModel, 0)
        } else {
            mInventoryGoodsListAdapter?.replace(resultGoods, 1)
        }
        mCurrentScanGoods = resultGoods


        if (isSucceed) {
            TipSound.getInstance().playScanSucceedSound(this@InventoryGoodsListActivity)

            //从位置判断
            for (i in 0 until scanGoodsList.size) {
                val goods = scanGoodsList[i]
                val barcodeList = goods.getBarCodeList()

                val barcode = resultGoods.barcode
                if (barcodeList.contains(barcode)) {
                    if (confirmGoodsList.size == 0) {
                        val tilteViewHolderCreatorModel = ViewHolderCreatorModel(InventoryGoodsListAdapter.TYPE_TITLE, TitleModel(resources.getString(R.string.good_scan_list_prompt)))
                        mInventoryGoodsListAdapter?.add(tilteViewHolderCreatorModel)
                    }

                    goods.scanState = Goods.SCAN_STATE_SUCCEED
                    confirmGoodsList.add(goods)
                    scanGoodsList.removeAt(i)

                    mInventoryGoodsListAdapter?.add(goods)
                    mInventoryGoodsListAdapter?.remove(i + 3)//只会移除一个
                    break
                }
            }
        } else {
            TipSound.getInstance().playScanFailedSound(this@InventoryGoodsListActivity)
            scanGoodsList.forEach {
                val barcodeList = it.getBarCodeList()
                val barcode = resultGoods.barcode
                if (barcodeList.contains(barcode)) {
                    showPromptDialog(resources.getString(R.string.scan_failed_prompt))
                }
            }

        }

    }

    override fun clickAlter(positon: Int, goods: Goods) {
        showAlterDialog(positon, goods)
    }

    override fun onItemViewClick(data: Any?, clickedView: View?, position: Int) {
    }

    private fun showAlterDialog(position: Int, goods: Goods) {

        alterGoods = goods

        this.alterPosition = position

        val skuState = goods.status

        if (alterSkuSateDialog == null) {
            isShowAlterDialog = true
            presenter.getQueryStatus()
        } else {
            val instance = AlterSkuSateDialog.getInstance()
            instance.setSkuState(skuState)
            alterSkuSateDialog?.show()
        }
    }

    override fun onSelectItem(position: Int) {

        val queryGoodsState = mQueryGoodsStatesList.get(position)
//            Toast.makeText(InventoryGoodsListActivity.this,queryGoodsState.getName(),Toast.LENGTH_SHORT).show();
        Zlog.ii("lxm ss onSelectItem:$queryGoodsState")

        if (isOrderLocation) {
            val status = alterGoods?.status
            var from_status: String? = null
            mAlterStatusName = queryGoodsState.nameValue
            val name = queryGoodsState.name

            isCanAlterStatus = !("IDLE_UNALLOCATED" == name || "IDLE_TRASH" == name)

            val orderTakeUp = FFApplication.getInstance().resources.getString(R.string.order_take_up)

            if (orderTakeUp == status) {
                from_status = "shelved"
            }

            Zlog.ii("lxm ss selectOnlickListner;$name $from_status")
            presenter.checkSkuAlterState(alterGoods?.id
                    ?: -1, alterGoods?.getBarcodeShow() ?: "", queryGoodsState.name, from_status
                    ?: "")

        } else {
            presenter.checkSkuAlterState(alterGoods?.id
                    ?: -1, alterGoods?.getBarcodeShow() ?: "", queryGoodsState.name, "")
        }

    }

    override fun refreshData(goodsList: List<Goods>) {

        if (goodsList.isNotEmpty()) {
            scanGoodsList.clear()
            scanGoodsList.addAll(goodsList)

            val tilteViewHolderCreatorModel = ViewHolderCreatorModel(InventoryGoodsListAdapter.TYPE_TITLE, TitleModel(resources.getString(R.string.inventory_list)))
            mInventoryGoodsListAdapter?.add(tilteViewHolderCreatorModel)
            mInventoryGoodsListAdapter?.addAll(scanGoodsList)

        } else {
            ToastUtils.show("no list data")
        }
    }

    override fun scanConfirmFailed(message: String?) {

        if (StringUtils.isNotBlank(message)) {
            ToastUtils.show(message ?: "")
        }
        isHandleScanResult = false

        val goods = Goods()
        goods.barcodeList = Goods.getBarcodeList(mCurrentSkuNo ?: "")

        goods.scanState = Goods.SCAN_STATE_FAILED
        scanResult(goods, false)
    }

    override fun goodsStateList(queryGoodsStatesList: List<QueryGoodsState>) {
        if (queryGoodsStatesList != null) {
            mQueryGoodsStatesList.addAll(queryGoodsStatesList)
            Zlog.ii("lxm ss goodsStateList: " + queryGoodsStatesList.size)

            if (queryGoodsStatesList.isEmpty()) {

                val data = mInventoryGoodsListAdapter?.data
                if (data != null) {
                    data.forEach {
                        if (it is Goods) {
                            it.isAlter = false
                        }
                    }
                    mInventoryGoodsListAdapter?.notifyDataSetChanged()
                }

            } else {
                alterSkuSateDialog = AlterSkuSateDialog.getInstance().getAlterSkuSateDialog(this@InventoryGoodsListActivity, queryGoodsStatesList, this)
                if (isShowAlterDialog) {
                    isShowAlterDialog = false
                    alterSkuSateDialog?.show()
                }
            }

        }
    }

    override fun goodsStateListFailed() {
    }

    override fun scanConfirmSucceed(goods: Goods) {

        isHandleScanResult = false
        if (goods == null) {
            return
        }
        goods.scanState = Goods.SCAN_STATE_SUCCEED
        scanResult(goods, true)

    }

    override fun alterGoodStateSucceed(goods: Goods) {

        if (alterPosition < 0) {
            return
        }
        if (alterGoods == null) {
            alterGoods = Goods()
        }

        if (isOrderLocation) {
            Zlog.ii("lxm ss alterGoodStateSucceed:$mAlterStatusName   $goods")
            alterGoods?.isAlter = isCanAlterStatus
            alterGoods?.status = mAlterStatusName
        } else {
            alterGoods?.status = goods.status
        }

        mInventoryGoodsListAdapter?.replace(alterGoods, alterPosition)

    }

    override fun alterGoodStateFailed(message: String?) {
        if (StringUtils.isNull(message)) {
            ToastUtils.show(resources.getString(R.string.scan_prompt_result_1))
        } else {
            ToastUtils.show(message ?: "")
        }
    }

    override fun showFailedError(message: String?) {
        if (message != null) {
            ToastUtils.show(message)
        } else {
            ToastUtils.show(resources.getString(R.string.net_error_prompt_one))
        }
    }

    override fun isOrderLocation(): Boolean = isOrderLocation

    override fun getBoxName(): String = mBoxName ?: ""
    override fun showLoadingView() {
        showLoadingView(inventory_goods_list_progress)
    }

    override fun hideLoadingView() {
        hideLoadingView(inventory_goods_list_progress)
    }

    private fun goback() {

        if (scanGoodsList.size > 0) {

            val builder = AlertDialog.Builder(this@InventoryGoodsListActivity)
            builder.setCancelable(true)
            builder.setTitle(resources.getString(R.string.inventory_prompt_one))
            builder.setMessage(resources.getString(R.string.inventory_prompt_two))
            builder.setPositiveButton(resources.getString(R.string.btn_yes)) { dialog, which ->
                dialog.dismiss()
                finish()
            }
            builder.setNegativeButton(resources.getString(R.string.btn_no)) { dialog, which -> dialog.dismiss() }
            builder.show()
        } else {
            finish()
        }

    }

    private fun showPromptDialog(message: String) {
        mCustomDialog = PromptDialogUtils.getInstance().getPromptDialog(this@InventoryGoodsListActivity, message)
        mCustomDialog?.show()
    }


    override fun onDestroy() {
        super.onDestroy()

        if (mCustomDialog != null) {
            if (mCustomDialog?.isShowing == true) {
                mCustomDialog?.dismiss()
            }
            mCustomDialog = null
        }
        if (alterSkuSateDialog != null) {
            if (alterSkuSateDialog?.isShowing == true) {
                alterSkuSateDialog?.dismiss()
            }
            alterSkuSateDialog = null
        }

    }
}