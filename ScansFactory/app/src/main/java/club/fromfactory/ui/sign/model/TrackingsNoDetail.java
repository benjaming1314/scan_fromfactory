package club.fromfactory.ui.sign.model;

import club.fromfactory.FFApplication;
import club.fromfactory.baselibrary.model.NoProguard;
import club.fromfactory.baselibrary.utils.StringUtils;
import club.fromfactory.R;

/**
 * Created by lxm on 2017/7/26.
 * "signed_date": "2017-06-02T09:04:39.258",
 * "qty_po": 0,
 * "signed_user": "开发者",
 * "signed_state": "未签收",
 * "voice_type": "voice_already_scan",
 * "tracking_no": 123
 */

public class TrackingsNoDetail implements NoProguard {

    public static final String QUALITY_INSPECTIO_FULL = "full_inspection"; //全
    public static final String QUALITY_INSPECTIO_SAMPLING = "sampling_inspection";  //抽

    public static final int SUGGEST_STOCK_YES = 1;  //1备货
    public static final int SUGGEST_STOCK_NO = 0;  //0非

    public static final String VOICE_TYPE_AREADY_SCAN = "voice_already_scan";//jinglingVoice warning("该包裹已经被扫描");
    public static final String VOICE_TYPE_BIG = "voice_big";//bigVoice warning("该包裹是大包裹");
    public static final String VOICE_TYPE_EXCEPTION = "voice_exception";//jinglingVoice
    public static final String VOICE_TYPE_NO_TRACKING = "voice_no_tracking";//jinglingVoice warning("该物流单号不存在");
    private String tracking_no;
    private String voice_type; //null 表示成功
    private String signed_date;
    private int qty_po;
    private String signed_user;
    private String suggest_category_name; //包裹品类
    private String signed_state;

    private String suggest_quality_inspection_method; //质检类型
    private int suggest_is_stock; //1备货 0非

    private int signed_status = -1;// 签收状态，1 表示错误0成功 -1 表示默认状态

    private int id;
    private int task_id;

    public String getSuggest_category_name() {
        return suggest_category_name;
    }
    public String getSuggest_category_name_show() {
        return StringUtils.isNotBlank(suggest_category_name) ? suggest_category_name : "" ;
    }

    public void setSuggest_category_name(String suggest_category_name) {
        this.suggest_category_name = suggest_category_name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTask_id() {
        return task_id;
    }

    public void setTask_id(int task_id) {
        this.task_id = task_id;
    }

    public String getTracking_no() {
        return tracking_no;
    }

    public void setTracking_no(String tracking_no) {
        this.tracking_no = tracking_no;
    }

    public String getVoice_type() {
        return voice_type;
    }

    public void setVoice_type(String voice_type) {
        this.voice_type = voice_type;
    }

    public String getSigned_date() {
        return signed_date;
    }

    public void setSigned_date(String signed_date) {
        this.signed_date = signed_date;
    }

    public int getQty_po() {
        return qty_po;
    }

    public void setQty_po(int qty_po) {
        this.qty_po = qty_po;
    }

    public String getSigned_user() {
        return signed_user;
    }

    public void setSigned_user(String signed_user) {
        this.signed_user = signed_user;
    }

    public String getSigned_state() {
        return signed_state;
    }

    public void setSigned_state(String signed_state) {
        this.signed_state = signed_state;
    }

    public int getSigned_status() {
        return signed_status;
    }

    public void setSigned_status(int signed_status) {
        this.signed_status = signed_status;
    }

    public String getSuggest_quality_inspection_method() {
        return suggest_quality_inspection_method;
    }
    public String getSuggest_quality_inspection_method_show() {

        if (QUALITY_INSPECTIO_FULL.equals(suggest_quality_inspection_method)) {
            return  FFApplication.getInstance().getResources().getString(R.string.parcel_check_type_one);
        }else if (QUALITY_INSPECTIO_SAMPLING.equals(suggest_quality_inspection_method)) {
            return  FFApplication.getInstance().getResources().getString(R.string.parcel_check_type_two);
        }else {
            return "" ;
        }
    }

    public void setSuggest_quality_inspection_method(String suggest_quality_inspection_method) {
        this.suggest_quality_inspection_method = suggest_quality_inspection_method;
    }

    public int getSuggest_is_stock() {
        return suggest_is_stock;
    }
    public String getSuggest_is_stock_show() {

        if (suggest_is_stock == SUGGEST_STOCK_YES) {
          return  FFApplication.getInstance().getResources().getString(R.string.parcel_shopping_type_one);
        }else if (suggest_is_stock == SUGGEST_STOCK_NO){
            return FFApplication.getInstance().getResources().getString(R.string.parcel_shopping_type_two);
        }else {
            return "";
        }

    }

    public void setSuggest_is_stock(int suggest_is_stock) {
        this.suggest_is_stock = suggest_is_stock;
    }

    @Override
    public String toString() {
        return "TrackingsNoModel{" +
                "tracking_no=" + tracking_no +
                ", voice_type='" + voice_type + '\'' +
                ", signed_date='" + signed_date + '\'' +
                ", qty_po=" + qty_po +
                ", signed_user='" + signed_user + '\'' +
                ", signed_state='" + signed_state + '\'' +
                '}';
    }
}
