package club.fromfactory.ui.tracking

import club.fromfactory.baselibrary.model.BaseResponse
import club.fromfactory.baselibrary.model.EmptyResponse
import club.fromfactory.ui.tracking.model.MawbInfo
import club.fromfactory.ui.tracking.model.MawbListResponse
import club.fromfactory.ui.tracking.model.PackageInfo
import io.reactivex.Observable
import retrofit2.http.*

interface ITrackingApi {


    /**
     */
    @GET
    fun getTrackingList(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>, @QueryMap queryParams: Map<String, @JvmSuppressWildcards Any>): Observable<BaseResponse<MawbListResponse>>

    /**
     */
    @GET
    fun getMawbDetailInfo(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>, @QueryMap queryParams: Map<String, @JvmSuppressWildcards Any>): Observable<BaseResponse<MawbInfo>>


    /**
     */
    @PATCH
    fun mawbBindPackage(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>): Observable<BaseResponse<PackageInfo>>

    /**
     */
    @PATCH
    fun transferConfirm(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>, @Body bodyParams: Map<String, @JvmSuppressWildcards Any>): Observable<BaseResponse<EmptyResponse>>

    /**
     */
    @DELETE
    fun mawbUnbindPackage(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>): Observable<BaseResponse<EmptyResponse>>

}