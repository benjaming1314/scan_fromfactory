package club.fromfactory.ui.hawb_binding

import android.content.Intent
import android.view.View
import club.fromfactory.R
import club.fromfactory.baselibrary.extention.clear
import club.fromfactory.baselibrary.utils.DensityUtils
import club.fromfactory.baselibrary.utils.StringUtils
import club.fromfactory.baselibrary.utils.ToastUtils
import club.fromfactory.baselibrary.view.BaseActivity
import club.fromfactory.baselibrary.view.recyclerview.BaseSpaceItemDecoration
import club.fromfactory.baselibrary.widget.CustomDialog
import club.fromfactory.ui.hawb_binding.contract.HawbBindingDetailContract
import club.fromfactory.ui.hawb_binding.contract.HawbPackageInfoAdapter
import club.fromfactory.ui.hawb_binding.model.HawbDetailInfo
import club.fromfactory.ui.hawb_binding.presenter.HawbBindingDetailPresenter
import club.fromfactory.ui.main.ScanBaseMVPActivity
import club.fromfactory.ui.tracking.model.PackageInfo
import club.fromfactory.ui.tracking.view.PackageInfoInterface
import club.fromfactory.utils.PromptDialogUtils
import club.fromfactory.utils.TipSound
import club.fromfactory.utils.Zlog
import club.fromfactory.widget.CustomTitleLinearLayout
import kotlinx.android.synthetic.main.activity_hawb_binding_detail.*
import kotlinx.android.synthetic.main.hawb_info.*

class HawbBindingDetailActivity : ScanBaseMVPActivity<HawbBindingDetailContract.Presenter>(), HawbBindingDetailContract.View {

    private var hawbDetailInfo: HawbDetailInfo? = null

    private var largeParcelList: List<PackageInfo>? = null

    private var mHawbPackageInfoAdapter: HawbPackageInfoAdapter? = null

    private var mDeletePosition = -1


    private var mCurrentBagNum = 0

    private var isHandleScanResult = false

    private var customDialog: CustomDialog? = null

    companion object {
        fun launchActivity(activity: BaseActivity, hawbDetailInfo: HawbDetailInfo) {

            val intent = Intent(activity, HawbBindingDetailActivity::class.java)
            intent.putExtra("hawb_detail", hawbDetailInfo)
            activity.startActivity(intent)
        }
    }
//    var click = 0

    override fun requestScanStart() {

//        if (Zlog.isDebug) {
//
//            when(click){
//                0 ->{
//                  requestScanResult("D2191015010R")
//                }
//                1 ->{
//                    requestScanResult("D2191015011R")
//                }
//                2 ->{
//                    requestScanResult("D2191015016B")
//                }
//
//            }
//            click++
//        }
    }

    override fun clickKeyCodeBack() {

        finish()
    }

    override fun getLayoutResId(): Int = R.layout.activity_hawb_binding_detail

    override fun initData() {
        super.initData()
        hawbDetailInfo = intent.getSerializableExtra("hawb_detail") as HawbDetailInfo?
        largeParcelList = hawbDetailInfo?.large_parcel_list
        mCurrentBagNum = largeParcelList?.size ?: 0

    }

    override fun initView() {
        super.initView()

        hawb_binding_detail_ctl_title.setListener(object : CustomTitleLinearLayout.CustomTitleListener() {

            override fun clickLeft() {
                super.clickLeft()
                finish()
            }
        })
        setScanListener(hawb_binding_detail_bottom)

        hawb_info_txt_ior.text = hawbDetailInfo?.ior_name
        hawb_info_txt_del.text = hawbDetailInfo?.dest_port
        hawb_info_txt_num.text = mCurrentBagNum.toString()
        hawb_info_txt_hawb.text = hawbDetailInfo?.name
        hawb_info_txt_plan_time.text = hawbDetailInfo?.planning_out_time

        hawb_info_txt_ok.setOnClickListener {

            bindPackage(hawb_info_edt_package_num.text.toString(),false)
        }

        mHawbPackageInfoAdapter = HawbPackageInfoAdapter()
        hawb_binding_detail_recycler_view.addItemDecoration(BaseSpaceItemDecoration(0, 0, 0, DensityUtils.dp2px(this, 1)))
        hawb_binding_detail_recycler_view.adapter = mHawbPackageInfoAdapter


        mHawbPackageInfoAdapter?.setOnItemViewClickListener(object : PackageInfoInterface {
            override fun onItemViewClick(data: PackageInfo?, clickedView: View?, position: Int) {
            }

            override fun deletePackageInfo(position: Int, data: PackageInfo) {
                mDeletePosition = position
                PromptDialogUtils.getInstance().showDialogDefine(this@HawbBindingDetailActivity, resources.getString(R.string.unbind_package_hawb), object : PromptDialogUtils.DialogCallback {
                    override fun onClickPositiveBtn() {
                        presenter.hawbUnBindPackage(data.largeParcelName ?: "")
                    }

                    override fun onClickNegativeBtn() {
                    }

                })

            }
        })

        if (largeParcelList?.isNotEmpty() == true) {
            mHawbPackageInfoAdapter?.addAll(largeParcelList)
        }
    }

    override fun requestScanResult(result: String?) {
        if (isHandleScanResult || customDialog?.isShowing == true) {
            return
        } else {
            isHandleScanResult = true
        }

        if (StringUtils.isNotBlank(result)) {
            hawb_info_edt_package_num.setText(result)
        }
        bindPackage(result,false)
    }

    private fun bindPackage(result: String?,isComfirm:Boolean) {

        if (StringUtils.isNotBlank(result)) {
            presenter.hawbBindPackage(result ?: "",isComfirm)
        } else {
            ToastUtils.show(resources.getString(R.string.hawb_binding_input_package_hint))
        }

    }

    private fun updateCurrentPackageNum() {
        hawb_info_txt_num.text = mCurrentBagNum.toString()
    }

    override fun hawbBindPackageSuccess(mPackageInfo: PackageInfo?) {
        hawb_info_edt_package_num.clear()
        isHandleScanResult = false
        mCurrentBagNum++
        TipSound.getInstance().playScanSucceedSound(this@HawbBindingDetailActivity)
        mHawbPackageInfoAdapter?.insert(mPackageInfo, 0)
        updateCurrentPackageNum()
    }

    override fun showFailedErrorWindow(message: String?) {
        isHandleScanResult = false
        TipSound.getInstance().playScanFailedSound(this@HawbBindingDetailActivity)
        customDialog = PromptDialogUtils.getInstance().getPromptDialog(this@HawbBindingDetailActivity, message)
        if (customDialog != null && isAlive) {
            customDialog?.show()
        }
    }

    override fun hawbUnBindPackageSuccess() {
        isHandleScanResult = false
        mCurrentBagNum--
        updateCurrentPackageNum()

        TipSound.getInstance().playScanSucceedSound(this@HawbBindingDetailActivity)
        mHawbPackageInfoAdapter?.remove(mDeletePosition)
        hawb_binding_detail_recycler_view.closeMenu()

    }

    override fun hawbBindPackageNeedComfirm(message: String?) {
        PromptDialogUtils.getInstance().showDialogDefine(this@HawbBindingDetailActivity, message, object : PromptDialogUtils.DialogCallback {
            override fun onClickPositiveBtn() {
                bindPackage(hawb_info_edt_package_num.text.toString(),true)
            }

            override fun onClickNegativeBtn() {
            }

        })
    }

    override fun showFailedError(message: String?) {
        isHandleScanResult = false
        ToastUtils.show(message ?: "")
    }

    override fun getHawbId(): Int = hawbDetailInfo?.id ?: 0

    override fun showLoadingView() {
        showLoadingView(hawb_binding_detail_progress)
    }

    override fun hideLoadingView() {
        hideLoadingView(hawb_binding_detail_progress)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (customDialog != null) {
            if (customDialog?.isShowing == true) {
                customDialog?.dismiss()
            }

            customDialog = null
        }
    }

    override fun createPresenter(): HawbBindingDetailContract.Presenter = HawbBindingDetailPresenter(this)


}
