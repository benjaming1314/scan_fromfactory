package club.fromfactory.ui.login

import club.fromfactory.ui.login.model.RequestLoginData
import club.fromfactory.ui.versionupdate.model.VersionModelResponse
import io.reactivex.Observable
import retrofit2.http.*

interface ILoginApi {

    /**
     * 版本更新
     */
    @GET
    fun getVersionUpdate(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>,@QueryMap queryParams: Map<String, @JvmSuppressWildcards Any>): Observable<VersionModelResponse>

    /**
     * 登录
     */
    @POST
    fun login(@Url url: String, @HeaderMap params: Map<String, @JvmSuppressWildcards Any>, @Body bodyParams: Map<String, @JvmSuppressWildcards Any>): Observable<RequestLoginData>

}