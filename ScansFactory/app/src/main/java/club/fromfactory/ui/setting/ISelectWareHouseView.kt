package club.fromfactory.ui.setting

import club.fromfactory.app_config.model.WareHouseInfor

interface ISelectWareHouseView {

    fun refreshWareHouse(wareHouseInfoList: List<WareHouseInfor>)

    fun showLoadingView()

    fun hideLoadingView()

    fun showFailedError(message: String?)

}