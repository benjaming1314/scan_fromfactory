package club.fromfactory.ui.sign;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import club.fromfactory.R;
import club.fromfactory.baselibrary.utils.StringUtils;
import club.fromfactory.baselibrary.utils.ToastUtils;
import club.fromfactory.baselibrary.widget.CustomDialog;
import club.fromfactory.ui.main.ScanBaseMVPActivity;
import club.fromfactory.ui.sign.adapter.TaskDetatilRecyclerAdapter;
import club.fromfactory.ui.sign.contract.TaskDetailContract;
import club.fromfactory.ui.sign.model.TrackingsNoDetail;
import club.fromfactory.ui.sign.presenter.TaskDetailPresenter;
import club.fromfactory.utils.PromptDialogUtils;
import club.fromfactory.utils.TipSound;
import club.fromfactory.utils.Utils;
import club.fromfactory.utils.Zlog;
import club.fromfactory.widget.CustomTitleLinearLayout;

public class TaskDetailActivity extends ScanBaseMVPActivity<TaskDetailContract.Presenter> implements TaskDetailContract.View {

    private static final String TASK_ID = "task_id";
    private static final String TASK_NAME = "task_name";
    @BindView(R.id.task_detail_scan)
    LinearLayout mScan;
    @BindView(R.id.txt_commit_task)
    TextView txtCommitTask;
    @BindView(R.id.rcl_tast_list)
    RecyclerView rclTastList;
    @BindView(R.id.task_detail_ctl_title)
    CustomTitleLinearLayout mCtlTitle;
    @BindView(R.id.txt_task_name)
    TextView txtTaskName;
    @BindView(R.id.task_detail_et_tracking_no)
    EditText taskDetailEtTrackingNo;
    @BindView(R.id.task_detail_txt_num)
    TextView taskDetailTxtNum;
    @BindView(R.id.task_detail_txt_error_num)
    TextView taskDetailTxtErrorNum;

    @BindView(R.id.parcel_info_txt_product_type)
    TextView mTxtProductType;
    @BindView(R.id.parcel_info_txt_check_type)
    TextView mTxtCheckType;
    @BindView(R.id.parcel_info_txt_stock_type)
    TextView mTxtStockType;

    @BindView(R.id.task_detail_progress)
    View mProgress;

    private TaskDetatilRecyclerAdapter mTaskDetatilRecyclerAdapter;
    private List<TrackingsNoDetail> trackingsNoList;

    private int taskID;
    private String taskName;

    private boolean isScan = false; //防止两次请求

    public static void launchActivity(Context context, int taskId, String taskName, int requestCode) {
        Intent intent = new Intent();
        intent.setClass(context, TaskDetailActivity.class);
        intent.putExtra(TASK_ID, taskId);
        intent.putExtra(TASK_NAME, taskName);
        ((Activity) context).startActivityForResult(intent, requestCode);
    }

    @Override
    public int getLayoutResId() {
        return R.layout.activity_task_detail;
    }

    @Override
    public void initData() {
        super.initData();
        Intent intent = getIntent();
        taskID = intent.getIntExtra(TASK_ID, -1);
        taskName = intent.getStringExtra(TASK_NAME);
        Zlog.ii("lxm ss onCreate:" + taskID + "  " + taskName);
        if (taskID == -1) {
            finish();
        }
    }

    @Override
    public void clickKeyCodeBack() {
        finish();
    }

    @Override
    public void requestScanStart() {
    }

    @Override
    public void requestScanResult(String result) {
        handleResult(result);
    }


    @Override
    public void initView() {
        super.initView();
        mCtlTitle.setListener(new CustomTitleLinearLayout.CustomTitleListener() {
            @Override
            public void clickLeft() {
                super.clickLeft();
                finishActivity();
            }
        });

        LinearLayoutManager mainFilterLLM = new LinearLayoutManager(this);
        mainFilterLLM.setOrientation(LinearLayoutManager.VERTICAL);
        rclTastList.setLayoutManager(mainFilterLLM);
        trackingsNoList = new ArrayList<>();
        mTaskDetatilRecyclerAdapter = new TaskDetatilRecyclerAdapter(TaskDetailActivity.this, trackingsNoList);
        rclTastList.setAdapter(mTaskDetatilRecyclerAdapter);

        setScanListener(mScan);
        if (!TextUtils.isEmpty(taskName)) {
            txtTaskName.setText(taskName);
        }
        presenter.getTaskDetail();
    }


    @OnClick({R.id.txt_commit_task, R.id.task_detail_txt_ok})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txt_commit_task:
                presenter.updateSignedTask();
                break;
            case R.id.task_detail_txt_ok:
                handleInputTrackingNo();
                break;
        }
    }

    private void handleInputTrackingNo() {
        String trackingNo = taskDetailEtTrackingNo.getText().toString().trim();

        if (TextUtils.isEmpty(trackingNo)) {
            ToastUtils.show(getResources().getString(R.string.scan_prompt_sku));
        } else {
            handleResult(trackingNo);
            taskDetailEtTrackingNo.getText().clear();
        }
    }

    private synchronized void handleResult(String result) {
        Zlog.ii("lxm ss handleResult:" + result);

        if (isScan) {
            return;
        }
        if (customDialog != null && customDialog.isShowing()) {
            return;
        }
        //判断是否库位码
        if (!StringUtils.isTrackingNo(result)) {
            ToastUtils.show(getResources().getString(R.string.scan_prompt_sku));
            return;
        }
        try {
            isScan = true;
            presenter.trackingNo(result);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            isScan = false;
        }
    }

    @Override
    public int getTaskId() {
        return taskID;
    }

    @Override
    public void updateTaskSuccess() {
        finishActivity();
    }

    //
    @Override
    public void trackingsNo(TrackingsNoDetail trackingsNo) {

        isScan = false;
        Zlog.ii("lxm ss trackingsNo:" + trackingsNo);

        mTxtProductType.setText(trackingsNo.getSuggest_category_name_show());
        mTxtCheckType.setText(trackingsNo.getSuggest_quality_inspection_method_show());
        mTxtStockType.setText(trackingsNo.getSuggest_is_stock_show());

        String voideType = trackingsNo.getVoice_type();
        String tracking_no = trackingsNo.getTracking_no();
        if (voideType == null || voideType.equals("null")) {
            TipSound.getInstance().playScanSucceedSound(TaskDetailActivity.this);
            TaskDetailActivity.this.trackingsNoList.add(0, trackingsNo);
            refreshData();
        } else if (TrackingsNoDetail.VOICE_TYPE_AREADY_SCAN.equals(voideType)) {
            TipSound.getInstance().playJinglingdSound(TaskDetailActivity.this);
            promptError(tracking_no + getResources().getString(R.string.sign_scan_prompt_result_1));
        } else if (TrackingsNoDetail.VOICE_TYPE_BIG.equals(voideType)) {
            promptError(tracking_no + getResources().getString(R.string.sign_scan_prompt_result_2));
            TipSound.getInstance().playBigSound(TaskDetailActivity.this);
            TaskDetailActivity.this.trackingsNoList.add(0, trackingsNo);
            refreshData();
        } else if (TrackingsNoDetail.VOICE_TYPE_EXCEPTION.equals(voideType)) {
            TipSound.getInstance().playJinglingdSound(TaskDetailActivity.this);
        } else if (TrackingsNoDetail.VOICE_TYPE_NO_TRACKING.equals(voideType)) {
            promptError(tracking_no + getResources().getString(R.string.sign_scan_prompt_result_3));
            TipSound.getInstance().playJinglingdSound(TaskDetailActivity.this);
            TaskDetailActivity.this.trackingsNoList.add(0, trackingsNo);
            refreshData();
        } else {

        }


    }

    @Override
    public void httpResult(@NotNull List<? extends TrackingsNoDetail> trackingsNoList) {
        if (trackingsNoList == null) return;
        TaskDetailActivity.this.trackingsNoList.clear();
        TaskDetailActivity.this.trackingsNoList.addAll(trackingsNoList);
        refreshData();
    }

    @Override
    public void showLoadingView() {

        TaskDetailActivity.this.showLoadingView(mProgress);
    }

    @Override
    public void hideLoadingView() {
        TaskDetailActivity.this.hideLoadingView(mProgress);
    }


    @Override
    public void showFailedError(String message) {
        isScan = false;
        if (message != null) {
            ToastUtils.show(message);
        } else {
            ToastUtils.show(getResources().getString(R.string.net_error_prompt_one));
        }
    }
    @SuppressLint("StringFormatInvalid")
    private void refreshData() {

        String signedrNum = getResources().getString(R.string.sign_prompt_one, String.valueOf(trackingsNoList.size()));

        taskDetailTxtNum.setText(signedrNum);

        String errorNum = getResources().getString(R.string.sign_prompt_three, String.valueOf(Utils.getErrorTrackingNo(trackingsNoList)));

        taskDetailTxtErrorNum.setText(errorNum);
        mTaskDetatilRecyclerAdapter.notifyDataSetChanged();
    }

    private CustomDialog customDialog;

    private void promptError(String message) {
        customDialog = PromptDialogUtils.getInstance().getPromptDialog(TaskDetailActivity.this, message);
        if (customDialog != null && isAlive()) {
            customDialog.show();
        }
    }

    @Override
    public void finish() {
        super.finish();
        try {
            if (customDialog != null && customDialog.isShowing()) {
                customDialog.dismiss();
            }
        } catch (IllegalArgumentException e) {
            // Handle or log or ignore
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            customDialog = null;
        }
    }

    private void finishActivity() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @NonNull
    @Override
    public TaskDetailContract.Presenter createPresenter() {
        return new TaskDetailPresenter(this);
    }
}
