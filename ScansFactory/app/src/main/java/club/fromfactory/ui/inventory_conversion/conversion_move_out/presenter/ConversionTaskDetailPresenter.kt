package club.fromfactory.ui.inventory_conversion.conversion_move_out.presenter

import club.fromfactory.FFApplication
import club.fromfactory.R
import club.fromfactory.baselibrary.language.LanguageUtils
import club.fromfactory.baselibrary.model.BaseResponse
import club.fromfactory.baselibrary.model.EmptyResponse
import club.fromfactory.baselibrary.net.CookieHelper
import club.fromfactory.baselibrary.net.retrofit.BaseRetrofit
import club.fromfactory.baselibrary.presenter.BasePresenter
import club.fromfactory.baselibrary.rx.loadingView
import club.fromfactory.baselibrary.utils.JsonUtils
import club.fromfactory.baselibrary.utils.PreferenceStorageUtils
import club.fromfactory.baselibrary.view.RxAppCompatActivity
import club.fromfactory.constant.Constants
import club.fromfactory.http.NetUtils
import club.fromfactory.http.model.HttpJsonErrorResult
import club.fromfactory.ui.inventory_conversion.conversion_move_out.IConversionMoveOutApi
import club.fromfactory.ui.inventory_conversion.conversion_move_out.contract.ConversionTaskDetailContract
import club.fromfactory.ui.inventory_conversion.conversion_move_out.model.ConversionTaskItemInfo
import com.trello.rxlifecycle2.kotlin.bindToLifecycle
import io.reactivex.observers.DefaultObserver
import retrofit2.HttpException
import java.util.*

class ConversionTaskDetailPresenter(view: ConversionTaskDetailContract.View) : BasePresenter<ConversionTaskDetailContract.View>(view), ConversionTaskDetailContract.Presenter {

    private var cvOrderItemListUrl = NetUtils.APP_MAIN_URL + "wms-center/cv_item/info"
    private var cvOrderTakeDownUrl = NetUtils.APP_MAIN_URL + "wms-center/cv_item/take_down"
    private var cvOrderMissingUrl = NetUtils.APP_MAIN_URL + "wms-center/cv_item/missing"

    override fun getCvItemList() {


        val token = PreferenceStorageUtils.getInstance().token

        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        //设置参数，访问url获取json
        val params = HashMap<String, Any>()
        params["cvId"] = view.getCvId()
        params["size"] = view.getSize()

        BaseRetrofit
                .createService(IConversionMoveOutApi::class.java)
                .getCvItemList(cvOrderItemListUrl, headerMap, params)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : DefaultObserver<BaseResponse<List<ConversionTaskItemInfo>>>() {
                    override fun onComplete() {
                    }

                    override fun onNext(baseResponse: BaseResponse<List<ConversionTaskItemInfo>>) {
                        val success = baseResponse.isSuccess

                        if (success) {
                            view.requestItemListSuccess(baseResponse.model)
                        } else {
                            view.requestFailed(baseResponse.firstErrorMessage.displayMessage)
                        }
                    }

                    override fun onError(e: Throwable) {
                        if (e is HttpException && e.code() in 400..403) {
                            val string = e.response()?.errorBody()?.string()
                            if (string != null) {
                                val parseJson = JsonUtils.parseJson<HttpJsonErrorResult>(string)
                                view.requestFailed(parseJson?.message)
                            }
                        } else {
                            view.requestFailed(FFApplication.getInstance().resources.getString(R.string.net_error))
                        }
                    }

                })

    }

    override fun takeDown(cvItemId: Int, boxName: String, barcode: String) {

        val token = PreferenceStorageUtils.getInstance().token

        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        //设置参数，访问url获取json
        val params = HashMap<String, Any>()
        params["cvId"] = view.getCvId().toString()
        params["cvItemId"] = cvItemId
        params["boxName"] = boxName
        params["barcode"] = barcode

        BaseRetrofit
                .createService(IConversionMoveOutApi::class.java)
                .takeDown(cvOrderTakeDownUrl, headerMap, params)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : DefaultObserver<BaseResponse<EmptyResponse>>() {
                    override fun onComplete() {
                    }

                    override fun onNext(baseResponse: BaseResponse<EmptyResponse>) {
                        val success = baseResponse.isSuccess

                        if (success) {
                            view.takeDownSuccess()
                        } else {
                            view.requestFailed(baseResponse.firstErrorMessage.displayMessage)
                        }
                    }

                    override fun onError(e: Throwable) {
                        if (e is HttpException && e.code() in 400..403) {
                            val string = e.response()?.errorBody()?.string()
                            if (string != null) {
                                val parseJson = JsonUtils.parseJson<HttpJsonErrorResult>(string)
                                view.requestFailed(parseJson?.message)
                            }
                        } else {
                            view.requestFailed(FFApplication.getInstance().resources.getString(R.string.net_error))
                        }
                    }

                })

    }

    override fun misssing(cvItemId: Int) {

        val token = PreferenceStorageUtils.getInstance().token

        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        //设置参数，访问url获取json
        val params = HashMap<String, Any>()
        params["cvId"] = view.getCvId()
        params["cvItemId"] = cvItemId

        BaseRetrofit
                .createService(IConversionMoveOutApi::class.java)
                .misssing(cvOrderMissingUrl, headerMap, params)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : DefaultObserver<BaseResponse<EmptyResponse>>() {
                    override fun onComplete() {
                    }

                    override fun onNext(baseResponse: BaseResponse<EmptyResponse>) {
                        val success = baseResponse.isSuccess

                        if (success) {
                            view.misssingSuccess()
                        } else {
                            view.requestFailed(baseResponse.firstErrorMessage.displayMessage)
                        }
                    }

                    override fun onError(e: Throwable) {
                        if (e is HttpException && e.code() in 400..403) {
                            val string = e.response()?.errorBody()?.string()
                            if (string != null) {
                                val parseJson = JsonUtils.parseJson<HttpJsonErrorResult>(string)
                                view.requestFailed(parseJson?.message)
                            }
                        } else {
                            view.requestFailed(FFApplication.getInstance().resources.getString(R.string.net_error))
                        }
                    }

                })

    }


}