package club.fromfactory.ui.viewphotos.adapters

import android.graphics.drawable.Animatable
import android.support.v4.view.PagerAdapter
import android.view.View
import android.view.ViewGroup
import club.fromfactory.ui.viewphotos.ViewPhotosContract
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.controller.BaseControllerListener
import com.facebook.imagepipeline.image.ImageInfo
import me.relex.photodraweeview.PhotoDraweeView

/**
 * Created by nichenjian on 2018/5/14.
 */

class PhotoPageAdapter(val view: ViewPhotosContract.View) : PagerAdapter() {
    private val mList = ArrayList<String>()
    override fun isViewFromObject(view: View, obj: Any): Boolean {
        return view == obj
    }

    override fun destroyItem(container: ViewGroup, position: Int, obj: Any) {
        container.removeView(obj as View)
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val photoDraweeView = PhotoDraweeView(container.context)
        val controller = Fresco.newDraweeControllerBuilder()
        controller.setUri(mList[position])
        controller.oldController = photoDraweeView.controller

        controller.controllerListener = object : BaseControllerListener<ImageInfo>() {
            override fun onFinalImageSet(id: String?, imageInfo: ImageInfo?, animatable: Animatable?) {
                super.onFinalImageSet(id, imageInfo, animatable)
                view.updateImageStatus(position, true)
                if (imageInfo == null) {
                    return
                }
                photoDraweeView.update(imageInfo.width, imageInfo.height)
            }
        }

        photoDraweeView.controller = controller?.build()
        try {
            container.addView(photoDraweeView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return photoDraweeView
    }

    fun getCurrentImage() {

    }

    override fun getCount(): Int {
        return mList.size
    }

    fun addAll(data: List<String>) {
        if (data.isNotEmpty()) {
            mList.clear()
            mList.addAll(data)
        }
    }
}
