package club.fromfactory.ui.wms_signing.model

import club.fromfactory.baselibrary.model.NoProguard
import java.io.Serializable

data class OrderDetailInfo(

        var name: String? = null,
        var createTime: String? = null,
        var updateTime: String? = null,
        var creatorName: String? = null,
        var startTime: String? = null,
        var typeName: String? = null,
        var type: Int? = null,
        var id: Int? = null,
        var taskId: Int? = null,
        var status: Int? = null,
        var signCount: Int? = null,
        var finishCount: Int? = null

) : NoProguard,Serializable