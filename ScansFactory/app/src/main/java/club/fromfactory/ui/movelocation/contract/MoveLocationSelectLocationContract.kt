package club.fromfactory.ui.movelocation.contract

import club.fromfactory.baselibrary.presenter.IPresenter
import club.fromfactory.baselibrary.view.IBaseMVPView
import club.fromfactory.ui.common.model.Goods

class MoveLocationSelectLocationContract {

    interface View : IBaseMVPView<Presenter> {
        fun getMoveOutLocation(): String
        fun getMoveOutSku(): String

        fun charmLocationResult(isEffective: Boolean, message: String? = "")

        fun charmSkuSuccess(goods: Goods)
        fun showFailedError(message: String?)

    }

    interface Presenter : IPresenter<View> {

        fun charmLocationAndSku()
        fun charmLocation()

    }
}