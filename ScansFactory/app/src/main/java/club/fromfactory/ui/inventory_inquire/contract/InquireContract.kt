package club.fromfactory.ui.inventory_inquire.contract

import club.fromfactory.baselibrary.presenter.IPresenter
import club.fromfactory.baselibrary.view.IBaseMVPView

class InquireContract {

    interface View : IBaseMVPView<Presenter> {
        fun requestFailed(message: String?)
    }

    interface Presenter : IPresenter<View> {
    }
}