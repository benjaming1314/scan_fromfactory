package club.fromfactory.ui.login.model;

import club.fromfactory.baselibrary.model.NoProguard;

/**
 * Created by lxm on 2017/5/23.
 */

public class LoginData implements NoProguard {

    private String token ;
    private String redirect_url ;


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getRedirect_url() {
        return redirect_url;
    }

    public void setRedirect_url(String redirect_url) {
        this.redirect_url = redirect_url;
    }

    @Override
    public String toString() {
        return "LoginData{" +
                "token='" + token + '\'' +
                ", redirect_url='" + redirect_url + '\'' +
                '}';
    }
}
