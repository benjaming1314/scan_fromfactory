package club.fromfactory.ui.inventory_conversion.conversion_move_out.model

import java.io.Serializable

class ConversionTaskInfo:Serializable {

    var cvId : Int ?= null

    var cvName :String ?= null

    var taskCount : Int = 0

    var containerCode :String ?= null

}