package club.fromfactory.ui.moveout.model;

import club.fromfactory.baselibrary.model.NoProguard;

/**
 * Created by lxm on 2017/2/24.
 *
 * 库位码
 */

public class Locations implements NoProguard {

    private String box_name ;

    private int qty ;

    public String getBox_name() {
        return box_name;
    }

    public void setBox_name(String box_name) {
        this.box_name = box_name;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    @Override
    public String toString() {
        return "Locations{" +
                "box_name='" + box_name + '\'' +
                ", qty=" + qty +
                '}';
    }
}
