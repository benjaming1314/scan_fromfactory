package club.fromfactory.ui.movein.contract

import club.fromfactory.baselibrary.presenter.IPresenter
import club.fromfactory.baselibrary.view.IBaseMVPView
import club.fromfactory.ui.common.model.Goods

class MoveInContract {

    interface View : IBaseMVPView<Presenter> {
        fun refreshData(goods: Goods)

        fun showFailedError(message: String?)
    }

    interface Presenter : IPresenter<View> {
        fun operateShelve(boxName: String, barcode: String)

    }
}