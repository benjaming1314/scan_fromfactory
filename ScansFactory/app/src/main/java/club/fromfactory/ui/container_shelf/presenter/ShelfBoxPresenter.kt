package club.fromfactory.ui.container_shelf.presenter

import club.fromfactory.FFApplication
import club.fromfactory.R
import club.fromfactory.baselibrary.language.LanguageUtils
import club.fromfactory.baselibrary.model.EmptyResponse
import club.fromfactory.baselibrary.net.CookieHelper
import club.fromfactory.baselibrary.net.retrofit.BaseRetrofit
import club.fromfactory.baselibrary.presenter.BasePresenter
import club.fromfactory.baselibrary.rx.loadingView
import club.fromfactory.baselibrary.utils.JsonUtils
import club.fromfactory.baselibrary.utils.PreferenceStorageUtils
import club.fromfactory.baselibrary.view.RxAppCompatActivity
import club.fromfactory.constant.Constants
import club.fromfactory.http.NetUtils
import club.fromfactory.http.model.HttpJsonErrorResult
import club.fromfactory.pojo.HttpJsonResult
import club.fromfactory.ui.container_shelf.ContainerShelfApi
import club.fromfactory.ui.container_shelf.contract.ShelfBoxContract
import com.trello.rxlifecycle2.kotlin.bindToLifecycle
import io.reactivex.observers.DefaultObserver
import retrofit2.HttpException
import java.util.HashMap

class ShelfBoxPresenter(view:ShelfBoxContract.View):BasePresenter<ShelfBoxContract.View>(view),ShelfBoxContract.Presenter {

    private var containerShelfUrl = NetUtils.APP_MAIN_URL + "wms/pda/batch_shelve/container"

    override fun shelfBox(str: String) {

        val token = PreferenceStorageUtils.getInstance().token

        val containerBox = view.getContainerBox()

        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        val params = HashMap<String, Any>()
        params["container_code"] = containerBox
        params["box_name"] = str

        BaseRetrofit
                .createService(ContainerShelfApi::class.java)
                .batchShelveContainerShelf(containerShelfUrl, headerMap,params)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : DefaultObserver<HttpJsonResult<EmptyResponse>>() {
                    override fun onComplete() {
                    }

                    override fun onNext(t: HttpJsonResult<EmptyResponse>) {
                        if (t.is_success){
                            view.shelfSuccess()
                        }else {
                            view.requestFailed(FFApplication.getInstance().resources.getString(R.string.net_error))
                        }
                    }

                    override fun onError(e: Throwable) {
                        if (e is HttpException && e.code() in 400..403) {
                            val string = e.response()?.errorBody()?.string()
                            if (string != null) {
                                val parseJson = JsonUtils.parseJson<HttpJsonErrorResult>(string)
                                view.requestFailed(parseJson?.message)
                            }
                        } else {
                            view.requestFailed(FFApplication.getInstance().resources.getString(R.string.net_error))
                        }
                    }

                })
    }
}