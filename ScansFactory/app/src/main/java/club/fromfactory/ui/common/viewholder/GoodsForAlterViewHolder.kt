package club.fromfactory.ui.common.viewholder

import android.view.View
import android.view.ViewGroup
import club.fromfactory.R
import club.fromfactory.utils.ImageUtils
import club.fromfactory.baselibrary.utils.StringUtils
import club.fromfactory.baselibrary.view.recyclerview.BaseViewHolder
import club.fromfactory.baselibrary.view.recyclerview.ViewHolderCreator
import club.fromfactory.ui.common.model.Goods
import club.fromfactory.ui.common.view.IGoodsForAlterInterface
import kotlinx.android.synthetic.main.goods_item_three_columns.*

class GoodsForAlterViewHolder(parent: ViewGroup?) : BaseViewHolder<Goods>(parent, R.layout.goods_item_three_columns)  {

    override fun bindData(goods: Goods) {
        super.bindData(goods)

        ImageUtils.loadImageNew(goods_item_three_columns_img, goods.sku_image_url, R.color.white)

        goods_item_three_columns_txt_sku.text = goods.getBarCodeShowStr()

        val scanState = goods.scanState

        val skuState = goods.status
        goods_item_three_columns_alter_skustate.text = if (StringUtils.isNotBlank(skuState)) skuState else ""

        val alter = goods.isAlter

        if (alter == false) {
            goods_item_three_columns_alter.setBackgroundResource(R.color.color_f2f2f2)
        }else {
            goods_item_three_columns_alter.setBackgroundResource(R.color.color_00A7F7)
        }

        goods_item_three_columns_alter.setOnClickListener {
            if (mRecyclerItemViewClickListener != null && mRecyclerItemViewClickListener is IGoodsForAlterInterface && alter ==true) {
                (mRecyclerItemViewClickListener as IGoodsForAlterInterface).clickAlter(layoutPosition,goods)
            }
        }

        when (scanState) {
            Goods.SCAN_STATE_DEFAULT -> {
                goods_item_three_columns_state_right.visibility = View.GONE
                goods_item_three_columns_state_error.visibility = View.GONE
            }
            Goods.SCAN_STATE_FAILED -> {
                goods_item_three_columns_state_right.visibility = View.GONE
                goods_item_three_columns_state_error.visibility = View.VISIBLE
            }
            Goods.SCAN_STATE_SUCCEED -> {
                goods_item_three_columns_state_right.visibility = View.VISIBLE
                goods_item_three_columns_state_error.visibility = View.GONE
            }

            else -> {
                goods_item_three_columns_state_right.visibility = View.GONE
                goods_item_three_columns_state_error.visibility = View.GONE
            }
        }

    }

}

class GoodsForAlterCreator : ViewHolderCreator<Goods> {

    override fun isForViewType(item: Any, position: Int): Boolean {
        return item is Goods
    }

    override fun onCreateBaseViewHolder(parent: ViewGroup): BaseViewHolder<Goods> {
        return GoodsForAlterViewHolder(parent)
    }

}