package club.fromfactory.ui.movein.presenter

import club.fromfactory.FFApplication
import club.fromfactory.R
import club.fromfactory.baselibrary.language.LanguageUtils
import club.fromfactory.baselibrary.net.CookieHelper
import club.fromfactory.baselibrary.net.retrofit.BaseRetrofit
import club.fromfactory.baselibrary.presenter.BasePresenter
import club.fromfactory.baselibrary.rx.loadingView
import club.fromfactory.baselibrary.utils.JsonUtils
import club.fromfactory.baselibrary.utils.PreferenceStorageUtils
import club.fromfactory.baselibrary.view.RxAppCompatActivity
import club.fromfactory.constant.Constants
import club.fromfactory.http.NetUtils
import club.fromfactory.http.model.HttpJsonErrorResult
import club.fromfactory.ui.common.model.Goods
import club.fromfactory.ui.movein.IMoveInApi
import club.fromfactory.ui.movein.contract.MoveInContract
import com.trello.rxlifecycle2.kotlin.bindToLifecycle
import io.reactivex.observers.DefaultObserver
import retrofit2.HttpException
import java.util.*

class MoveInConfirmPresenter(view: MoveInContract.View) : BasePresenter<MoveInContract.View>(view), MoveInContract.Presenter {

    /**
     * 移入确认 - 扫描sku_id - 出库单
     */
    private var operateShelveUrl = NetUtils.APP_MAIN_URL + "wms/pda/operate/shelve"

    override fun operateShelve(boxName: String, barcode: String) {
        val token = PreferenceStorageUtils.getInstance().token

        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Authorization"] = token
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        val params = HashMap<String, Any>()
        params["box_name"] = boxName
        params["barcode"] = barcode

        BaseRetrofit
                .createService(IMoveInApi::class.java)
                .operateShelve(operateShelveUrl, headerMap, params)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : DefaultObserver<Goods>() {
                    override fun onComplete() {
                    }

                    override fun onNext(t: Goods) {
                        view.refreshData(t)
                    }

                    override fun onError(e: Throwable) {
                        if (e is HttpException && e.code() in 400..403) {
                            val string = e.response()?.errorBody()?.string()
                            if (string != null) {
                                val parseJson = JsonUtils.parseJson<HttpJsonErrorResult>(string)
                                view.showFailedError(parseJson?.message)
                            }
                        } else {
                            view.showFailedError(FFApplication.getInstance().resources.getString(R.string.net_error))
                        }
                    }

                })
    }
}