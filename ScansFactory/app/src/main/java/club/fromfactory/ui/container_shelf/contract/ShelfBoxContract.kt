package club.fromfactory.ui.container_shelf.contract

import club.fromfactory.baselibrary.presenter.IPresenter
import club.fromfactory.baselibrary.view.IBaseMVPView

class ShelfBoxContract {

    interface View: IBaseMVPView<Presenter> {

        fun shelfSuccess()

        fun requestFailed(message :String?)

        fun getContainerBox():String
    }
    interface Presenter: IPresenter<View> {

        fun shelfBox(boxName:String)

    }
}