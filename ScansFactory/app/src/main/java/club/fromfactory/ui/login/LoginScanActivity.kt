package club.fromfactory.ui.login

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import club.fromfactory.BuildConfig
import club.fromfactory.R
import club.fromfactory.app_config.DeviceManager
import club.fromfactory.app_config.WareHouseManager
import club.fromfactory.baselibrary.language.LanguageUtils
import club.fromfactory.baselibrary.utils.DeviceUtils
import club.fromfactory.baselibrary.utils.PreferenceStorageUtils
import club.fromfactory.baselibrary.utils.StringUtils
import club.fromfactory.baselibrary.utils.ToastUtils
import club.fromfactory.baselibrary.view.BaseMVPActivity
import club.fromfactory.constant.Constants
import club.fromfactory.http.NetUtils
import club.fromfactory.ui.debug.DebugPanelActivity
import club.fromfactory.ui.login.contract.LoginContract
import club.fromfactory.ui.login.model.RequestLoginData
import club.fromfactory.ui.login.presenter.UserLoginPresenter
import club.fromfactory.ui.main.MainActivity
import club.fromfactory.ui.setting.SelectDeviceActivity
import club.fromfactory.ui.setting.SelectLanguageActivity
import club.fromfactory.ui.setting.SelectWareHouseActivity
import club.fromfactory.ui.test.TestScanActivity
import club.fromfactory.ui.versionupdate.UpdateDialogCallback
import club.fromfactory.ui.versionupdate.VersionUpdateUtils
import club.fromfactory.ui.versionupdate.model.VersionModel
import club.fromfactory.utils.Utils
import club.fromfactory.utils.Zlog
import kotlinx.android.synthetic.main.activity_login_scan.*

class LoginScanActivity : BaseMVPActivity<LoginContract.Presenter>(), LoginContract.View {


    companion object {
        private const val REQUEST_CODE_DEVICE = 100
        private const val REQUEST_CODE_WAREHOUSE = 101
        private const val REQUEST_CODE_LANGUAGE = 102
        private const val IS_CLEAR = 100000//是否退出的参数
        fun launchActivity(activity: Activity) {
            val intent = Intent(activity, LoginScanActivity::class.java)
            activity.startActivity(intent)
        }
    }


    override fun getLayoutResId(): Int = R.layout.activity_login_scan


    override fun initData() {
        super.initData()

    }

    @SuppressLint("SetTextI18n")
    override fun initView() {
        super.initView()

        login_txt_current_language.text = LanguageUtils.getCurrentLanguageName()
        login_txt_current_device.text = DeviceManager.getInstance().currentTypeName
        login_txt_current_warehouse.text = Constants.HOUSE_NAME


        setListener()


        if (Zlog.isDebug) {
            login_scan_et_account.setText("developer")
            login_scan_et_password.setText("123123")
            //            mEdtAccount.setText("xiaoxiaozhang@clubfactory.com");
            //            mEdtPassword.setText("121212");
        }

        login_scan_txt_version.text = resources.getString(R.string.version_prompt) + Utils.getApplicationVersionName(this@LoginScanActivity) + "\n" + DeviceUtils.getPhoneModel()
        login_scan_et_account.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                Zlog.ii("ss onTextChanged:$s")
            }

            override fun afterTextChanged(s: Editable) {
                for (i in s.length downTo 1) {
                    if (s.subSequence(i - 1, i).toString() == "\n")
                        s.replace(i - 1, i, "")

                }
            }
        })

        login_scan_et_password.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                Zlog.ii("ss onTextChanged:$s")

            }

            override fun afterTextChanged(s: Editable) {
                for (i in s.length downTo 1) {
                    if (s.subSequence(i - 1, i).toString() == "\n")
                        s.replace(i - 1, i, "")

                }
            }
        })


    }

    private fun setListener() {

        login_scan_ly_title.setOnClickListener {
            presenter.debugPanel()
//            startActivity(Intent(this@LoginScanActivity,TestScanActivity::class.java))
        }

        login_change_device.setOnClickListener {
            SelectDeviceActivity.launchActivity(this@LoginScanActivity, true, REQUEST_CODE_DEVICE)

        }
        login_change_warehouse.setOnClickListener {

            SelectWareHouseActivity.launchActivity(this@LoginScanActivity, true, REQUEST_CODE_WAREHOUSE)
        }

        login_change_language.setOnClickListener {
            SelectLanguageActivity.launchActivity(this@LoginScanActivity, true, REQUEST_CODE_LANGUAGE)
        }

        login_scan_ly_login.setOnClickListener {
            presenter.getVersionUpdate()
        }

    }

    override fun requestUpdateVersionSuccess(versionModel: VersionModel?) {
        VersionUpdateUtils.resultVersionInfo(versionModel, this, object : UpdateDialogCallback {
            override fun noUpdate() {
                login()
            }

            override fun cancelUpdate() {
                login()
            }
        })

    }

    override fun requestUpdateVersionFailed(message: String?) {
        login()
    }

    private fun login() {

        DeviceManager.getInstance().checkDeviceType(this@LoginScanActivity, Constants.PDA_TYPE) { isGo ->
            if (isGo) {
                presenter.login()
            }
        }
    }

    override fun getUserName(): String = login_scan_et_account.text.toString()
    override fun getPassword(): String = login_scan_et_password.text.toString()
    override fun clearUserName() {
        login_scan_et_account.setText("")
    }

    override fun clearPassword() {
        login_scan_et_password.setText("")
    }

    override fun showLoadingView() {
        if (login_scan_progress.visibility == View.GONE) {
            login_scan_progress.visibility = View.VISIBLE
        }
    }

    override fun hideLoadingView() {
        if (login_scan_progress.visibility == View.VISIBLE) {
            login_scan_progress.visibility = View.GONE
        }
    }

    override fun showFailedError(message: String?) {

        if (StringUtils.isNull(message)) {
            ToastUtils.show(resources.getString(R.string.login_result_failed))
        } else {
            ToastUtils.show(message ?: "")
        }

    }

    override fun createPresenter(): LoginContract.Presenter = UserLoginPresenter(this)

    override fun loginSucceed(httpJsonResult: RequestLoginData?) {

        ToastUtils.show(resources.getString(R.string.login_result_succeed))

        val userName = getUserName()
        val password = getPassword()
        val token = httpJsonResult?.data?.token
        Zlog.ii("loginSucceed';$token")
        PreferenceStorageUtils.getInstance().saveUserName(userName)
        PreferenceStorageUtils.getInstance().savePassword(password)
        PreferenceStorageUtils.getInstance().saveToken(token)

        val intent = Intent(this@LoginScanActivity, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {

            when (requestCode) {

                REQUEST_CODE_LANGUAGE, REQUEST_CODE_DEVICE -> {
                    reStartActivity()
                }
                REQUEST_CODE_WAREHOUSE -> {
                    Utils.restartApp(this@LoginScanActivity)
                    finish()
                }
            }
        }
    }

    override fun enterDebugPanel() {
        startActivity(Intent(this, DebugPanelActivity::class.java))
    }


    private fun reStartActivity() {
        val intent = intent
        finish()
        startActivity(intent)
    }

    override fun clickKeyCodeBack() {

        finish()

    }
}