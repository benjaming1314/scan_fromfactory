package club.fromfactory.ui.spare_parts.model

class SparePartsMoveOutData {

    var barcodeList : List<String> ?=null
    var hasBarcodeList :Boolean ?= false
    var count : Int ?= -1
}