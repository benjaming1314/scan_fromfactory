package club.fromfactory.ui.moveout.viewholder

import android.view.ViewGroup
import club.fromfactory.R
import club.fromfactory.baselibrary.view.recyclerview.BaseViewHolder
import kotlinx.android.synthetic.main.move_out_scan_mul_goods_item.*

class MoveOutMulGoodsViewHolder(parent: ViewGroup?) : BaseViewHolder<String>(parent, R.layout.move_out_scan_mul_goods_item) {

    override fun bindData(data: String) {
        super.bindData(data)
        move_out_mul_goods_item_txt.text = data
    }
}