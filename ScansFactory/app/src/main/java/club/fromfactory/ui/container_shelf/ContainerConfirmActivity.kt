//package club.fromfactory.ui.container_shelf
//
//import android.content.Intent
//import club.fromfactory.R
//import club.fromfactory.baselibrary.router.RouterConstants
//import club.fromfactory.baselibrary.router.RouterManager
//import club.fromfactory.baselibrary.router.RouterUrlProvider
//import club.fromfactory.baselibrary.utils.StringUtils
//import club.fromfactory.baselibrary.view.BaseActivity
//import club.fromfactory.baselibrary.widget.CustomDialog
//import club.fromfactory.routerannotaions.Router
//import club.fromfactory.ui.container_shelf.contract.ContainerConfirmContract
//import club.fromfactory.ui.container_shelf.model.ContainerResponseData
//import club.fromfactory.ui.container_shelf.presenter.ContainerConfirmPresenter
//import club.fromfactory.ui.main.ScanBaseMVPActivity
//import club.fromfactory.utils.PromptDialogUtils
//import club.fromfactory.utils.TipSound
//import club.fromfactory.widget.CustomTitleLinearLayout
//import com.blankj.utilcode.util.ToastUtils
//import kotlinx.android.synthetic.main.activity_container_confirm.*
//
//@Router(RouterConstants.CONTAINER_CONFIRM)
//class ContainerConfirmActivity : ScanBaseMVPActivity<ContainerConfirmContract.Presenter>(),ContainerConfirmContract.View {
//
//    private var isHandleScanResult = false
//
//    companion object {
//
//        private const val REQUEST_CODE_BOX_SHELF = 100
//
//        fun launchActivity(activity: BaseActivity) {
//
//            val intent = Intent(activity, ContainerConfirmActivity::class.java)
//            activity.startActivity(intent)
//        }
//    }
//
//    override fun requestScanStart() {
//
//    }
//
//    override fun getLayoutResId(): Int = R.layout.activity_container_confirm
//
//    override fun initData() {
//        super.initData()
//    }
//
//    override fun initView() {
//        super.initView()
//
//        container_confirm_ctl_title.setListener(object : CustomTitleLinearLayout.CustomTitleListener(){
//
//            override fun clickLeft() {
//                super.clickLeft()
//                finish()
//            }
//        })
//
//        container_confirm_btn_ok.setOnClickListener {
//            val str = container_confirm_edt_hawb.text.toString()
//            checkContainer(str)
//        }
//
//        setScanListener(container_confirm_scan)
//    }
//
//
//
//    override fun requestScanResult(result: String?) {
//
//        if (isHandleScanResult) {
//            return
//        } else {
//            isHandleScanResult = true
//        }
//
//        if (StringUtils.isNotBlank(result)) {
//            container_confirm_edt_hawb.setText(result)
//        }
//        checkContainer(result?:"")
//    }
//
//    private fun checkContainer(str :String?) {
//        if (StringUtils.isNotBlank(str)) {
//            presenter.checkContainer(str?:"")
//        }else {
//            requestFailed(resources.getString(R.string.container_num_scan_prompt))
//        }
//    }
//
//    override fun requestSuccess(containerResponseData: ContainerResponseData) {
//        val str = container_confirm_edt_hawb.text.toString()
//        RouterManager.open(this, RouterUrlProvider.getShelfBoxUrl(str,containerResponseData.box_name?:"",containerResponseData.total_to_shelve?:0),REQUEST_CODE_BOX_SHELF)
//    }
//
//    private var customDialog : CustomDialog ?= null
//    override fun requestFailed(message: String?) {
//        isHandleScanResult = false
////        ToastUtils.showLong(message)
//        TipSound.getInstance().playScanFailedSound(this)
//        customDialog = PromptDialogUtils.getInstance().getPromptDialog(this, message)
//        if (customDialog != null && isAlive) {
//            customDialog?.show()
//        }
//    }
//
//
//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//
//        if(requestCode == REQUEST_CODE_BOX_SHELF) {
//
//            if (resultCode == RESULT_OK) {
//                if (intent.getBooleanExtra("is_reset",false)) {
//                    startActivity(intent)
//                }
//                finish()
//
//            }
//        }
//
//    }
//
//
//    override fun showLoadingView() {
//        showLoadingView(container_confirm_progress)
//    }
//
//    override fun hideLoadingView() {
//        hideLoadingView(container_confirm_progress)
//    }
//
//    override fun onDestroy() {
//        super.onDestroy()
//
//        if (customDialog != null) {
//            if (customDialog?.isShowing == null) {
//                customDialog?.dismiss()
//            }
//            customDialog = null
//        }
//    }
//
//
//    override fun createPresenter(): ContainerConfirmContract.Presenter = ContainerConfirmPresenter(this)
//
//}
