package club.fromfactory.ui.main.viewholers;

import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import club.fromfactory.baselibrary.view.recyclerview.BaseViewHolder;
import club.fromfactory.ui.main.model.PermissionModel;
import club.fromfactory.R;

public class MainViewHolders extends BaseViewHolder<PermissionModel> {

    @BindView(R.id.main_item_content)
    TextView mTxtContent ;
    @BindView(R.id.main_item_number)
    View mNum ;
    @BindView(R.id.main_item_txt_number)
    TextView mTxtNum ;

    public MainViewHolders(ViewGroup parent) {
        super(parent, R.layout.main_item);
        ButterKnife.bind(this,itemView);
    }

    @Override
    public void bindData(@NonNull PermissionModel data) {
        super.bindData(data);

        int unread = data.getUnread();
        String modelName = data.getModelName();

        if (unread > 0 ) {
            mNum.setVisibility(View.VISIBLE);
            mTxtNum.setText(String.valueOf(unread));
        }else {
            mNum.setVisibility(View.GONE);
        }
        mTxtContent.setText(modelName);

    }
}
