package club.fromfactory.ui.setting.adapter

import android.view.ViewGroup
import club.fromfactory.baselibrary.language.LanguageInfo
import club.fromfactory.baselibrary.view.recyclerview.BaseRecyclerAdapter
import club.fromfactory.baselibrary.view.recyclerview.BaseViewHolder
import club.fromfactory.ui.setting.viewholder.LanguageViewHolder

class LanguageAdapter : BaseRecyclerAdapter<LanguageInfo>() {

    override fun onCreateBaseViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<LanguageInfo> = LanguageViewHolder(parent)
}