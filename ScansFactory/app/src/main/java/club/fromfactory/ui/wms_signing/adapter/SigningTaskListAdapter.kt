package club.fromfactory.ui.wms_signing.adapter

import android.view.ViewGroup
import club.fromfactory.baselibrary.view.recyclerview.BaseRecyclerAdapter
import club.fromfactory.baselibrary.view.recyclerview.BaseViewHolder
import club.fromfactory.ui.wms_signing.model.OrderDetailInfo
import club.fromfactory.ui.wms_signing.viewholder.SigningTaskViewHolder

class SigningTaskListAdapter : BaseRecyclerAdapter<OrderDetailInfo>() {
    override fun onCreateBaseViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<OrderDetailInfo> = SigningTaskViewHolder(parent)
}