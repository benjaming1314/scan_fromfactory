package club.fromfactory.ui.sign.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import club.fromfactory.utils.Zlog;
import club.fromfactory.ui.sign.model.SignedTask;
import club.fromfactory.R;

/**
 * Created by lxm on 2017/7/25.
 */

public class TaskListRecyclerAdapter extends RecyclerView.Adapter<TaskListRecyclerAdapter.ViewHolder> {

    private List<SignedTask> signedTaskList;

    private LayoutInflater mLayoutInflater;

    private Context mContext ;

    public TaskListRecyclerAdapter(Context mContext, List<SignedTask> signedTaskList) {
        this.mContext = mContext;
        this.signedTaskList = signedTaskList;
        mLayoutInflater = LayoutInflater.from(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View vv = mLayoutInflater.inflate(R.layout.task_list_item, parent,false);
        return new ViewHolder(vv);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {


        Zlog.ii("lxm ss onBindViewHolder" + signedTaskList.size());
        SignedTask signedTask = signedTaskList.get(position);

        String taskName = signedTask.getTask_name();
        int packageNum = signedTask.getPackage_num();
        String taskStartTime = signedTask.getStart_time();
        if (position == 0) {
            holder.lyTastListItem.setBackgroundColor(mContext.getResources().getColor(R.color.color_CCFFCC));
        }else {
            holder.lyTastListItem.setBackgroundColor(mContext.getResources().getColor(R.color.white));
        }

        if (TextUtils.isEmpty(taskName)) {
            taskName = "" ;
        }
        holder.txtTaskName.setText(taskName);

        holder.txtTaskNum.setText(String.valueOf(packageNum));

        if (TextUtils.isEmpty(taskStartTime)) {
            taskName = "" ;
        }
        holder.txtTaskTime.setText(taskStartTime);

        holder.lyTastListItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickLitener != null) {
                    mOnItemClickLitener.onItemClick(position);
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return signedTaskList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View arg0) {
            super(arg0);
            ButterKnife.bind(this, arg0);
        }

        @BindView(R.id.txt_task_name)
        TextView txtTaskName;
        @BindView(R.id.txt_task_num)
        TextView txtTaskNum;
        @BindView(R.id.txt_task_time)
        TextView txtTaskTime;
        @BindView(R.id.ly_tast_list_item)
        LinearLayout lyTastListItem;
    }

    /**
     * ItemClick的回调接口
     *
     */
    public interface OnItemClickLitener {
        void onItemClick( int position);
    }

    private OnItemClickLitener mOnItemClickLitener;

    public void setOnItemClickLitener(OnItemClickLitener mOnItemClickLitener) {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }
}
