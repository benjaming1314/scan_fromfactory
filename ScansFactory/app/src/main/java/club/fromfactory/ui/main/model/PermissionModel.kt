package club.fromfactory.ui.main.model

class PermissionModel {

    var routerUrl: String? = null
    var modelType: Int = -1
    var permissionName: String? = null
    var modelName: String? = null
    var modelKey: String? = null
    var unread: Int = 0
}

enum class PermissionName(val permissionName: String) {

    //pda | pda | PDA_批量移入
    //pda.movein
    MOVE_IN("pda.movein"),
    //pda | pda | PDA_移出
    //pda.moveout
    MOVE_OUT("pda.moveout"),
    //pda | pda | PDA_盘点
    //pda.check
    CHECK("pda.check"),
    //pda | pda | PDA_签收
    //pda.signed
    SIGNED("pda.signed"),
    //pda | pda | PDA_批量移入
    //pda.mulmovein
    //        MUL_MOVE_IN("pda.mulmovein"),
    //pda | pda | PDA_库存移动
    //pda.movelocation
    MOVE_LOCATION("pda.movelocation"),
    //pda | pda | PDA_Tracking
    //pda.tracking
    PROCESSING_TRACKING("pda.tracking"),
    //pda | pda | PDA_库存转换
    //pda.inventory_conversion
    INVENTORY_CONVERSION("pda.inventory_conversion"),
    //pda | pda | PDA_wms签收
    //pda.wms_signing
    WMS_SIGNING("pda.wms_signing"),
    //pda | pda | PDA_库存查询
    //pda.inventory_inquire
    INVENTORY_INQUIRE("pda.inventory_inquire"),
    //pda | pda | PDA_HAWB绑定
    //pda.hawb_binding
    HAWB_BINDING("pda.hawb_binding"),
    //pda | pda | PDA_整容器上架
    //pda.comtainer_shelf
    COMTAINER_SHELF("pda.comtainer_shelf"),
    //pda | pda | PDA_整货位移库
    //pda.space_transfer
    SPACE_TRANSFER("pda.space_transfer"),
    //pda | pda | PDA_循环盘点
    //pda.rf_inventory
    RF_INVENTORY("pda.rf_inventory"),

    //pda | pda | PDA_散件移库
    //pda.spare_parts
    SPARE_PARTS("pda.spare_parts"),

    //wms签收
    //pda | pda | PDA_rto签收
    //pda.wms_signing_rto
    WMS_SIGNING_RTO("pda.wms_signing_rto"),
    //pda | pda | PDA_dto签收
    //pda.wms_signing_dto
    WMS_SIGNING_DTO("pda.wms_signing_dto"),
    //pda | pda | PDA_采购签收
    //pda.wms_signing_purchase
    WMS_SIGNING_PURCHASE("pda.wms_signing_purchase"),
    //pda | pda | PDA_调拨签收
    //pda.wms_signing_transfer
    WMS_SIGNING_TRANSFER("pda.wms_signing_transfer"),

    //库存转换
    //pda | pda | PDA_转换上架
    //pda.inventory_conversion_move_in
    INVENTORY_CONVERSION_MOVE_IN("pda.inventory_conversion_move_in"),

    //pda | pda | PDA_转换下架
    //pda.inventory_conversion_move_out
    INVENTORY_CONVERSION_MOVE_OUT("pda.inventory_conversion_move_out"),


    //待移入占用：Pending for putaway to Idle
    //pda | pda | PDA_盘点_待移入占用
    //pda.check_pending_for_putaway
    CHECK_PENDING_FOR_PUTAWAY("pda.check_pending_for_putaway"),

    //闲置（有库位）：Shelved
    //pda | pda | PDA_盘点_闲置（有库位）
    //pda.check_shelved
    CHECK_SHELVED("pda.check_shelved"),

    //待移出占用：Pending fo MoveOut
    //pda | pda | PDA_盘点_待移出占用
    //pda.check_pending_fo_moveout
    CHECK_PENDING_FO_MOVEOUT("pda.check_pending_fo_moveout"),

    //闲置（无库位）：No box
    //pda | pda | PDA_盘点_闲置（无库位）
    //pda.check_no_box
    CHECK_NO_BOX("pda.check_no_box"),

    //报废库存：Trash Inventory
    //pda | pda | PDA_盘点_报废库存
    //pda.check_trash_inventory
    CHECK_TRASH_INVENTORY("pda.check_trash_inventory")
}