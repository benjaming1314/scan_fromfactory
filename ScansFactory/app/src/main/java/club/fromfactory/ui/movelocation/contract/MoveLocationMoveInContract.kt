package club.fromfactory.ui.movelocation.contract

import club.fromfactory.baselibrary.presenter.IPresenter
import club.fromfactory.baselibrary.view.IBaseMVPView

class MoveLocationMoveInContract {

    interface View : IBaseMVPView<Presenter> {
        fun getFromBox(): String
        fun getMoveInSku(): String
        fun getToBox(): String
        fun getMoveQty(): String

        fun charmLocationResult(isEffective: Boolean, message: String? = "")

        fun moveInSuccess()
        fun showFailedError(message: String?)


    }

    interface Presenter : IPresenter<View> {

        fun charmLocation()
        fun moveSku()

    }
}