package club.fromfactory.ui.spare_parts.contract

import club.fromfactory.baselibrary.presenter.IPresenter
import club.fromfactory.baselibrary.view.IBaseMVPView

class SparePartsMoveInContract {

    interface View :IBaseMVPView<Presenter> {
        fun requestFailed(message: String?)
        fun shelfBoxSuccess()
        fun shelfBarcodeSuccess()
        fun shelfAllSuccess()
    }
    interface Presenter:IPresenter<View> {
        fun shelfBox(boxName:String)
        fun shelfBarcode(boxName:String,barcode:String)
        fun shelfAll(boxName:String, barcodeList: List<String>)
    }
}