package club.fromfactory.ui.hawb_binding.contract

import club.fromfactory.baselibrary.presenter.IPresenter
import club.fromfactory.baselibrary.view.IBaseMVPView
import club.fromfactory.ui.hawb_binding.model.HawbDetailInfo

class HawbBindingContract {

    interface View : IBaseMVPView<Presenter> {

        fun requestHawbDetail(hawbDetailInfo: HawbDetailInfo)

        fun requestFailed(message :String?)

    }

    interface Presenter : IPresenter<View> {

        fun getHawbDetail(hawbNum: String)

    }
}