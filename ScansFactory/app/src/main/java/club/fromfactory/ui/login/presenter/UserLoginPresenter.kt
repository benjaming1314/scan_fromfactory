package club.fromfactory.ui.login.presenter

import android.os.Handler
import android.text.TextUtils
import club.fromfactory.BuildConfig
import club.fromfactory.FFApplication
import club.fromfactory.R
import club.fromfactory.baselibrary.language.LanguageUtils
import club.fromfactory.baselibrary.net.CookieHelper
import club.fromfactory.baselibrary.net.retrofit.BaseRetrofit
import club.fromfactory.baselibrary.presenter.BasePresenter
import club.fromfactory.baselibrary.rx.loadingView
import club.fromfactory.baselibrary.utils.JsonUtils
import club.fromfactory.baselibrary.utils.PreferenceStorageUtils
import club.fromfactory.baselibrary.view.BaseActivity
import club.fromfactory.baselibrary.view.RxAppCompatActivity
import club.fromfactory.constant.Constants
import club.fromfactory.http.NetUtils
import club.fromfactory.http.model.HttpJsonErrorResult
import club.fromfactory.ui.debug.DebugVerifyDialogUtils
import club.fromfactory.ui.login.ILoginApi
import club.fromfactory.ui.login.contract.LoginContract
import club.fromfactory.ui.login.model.RequestLoginData
import club.fromfactory.ui.versionupdate.model.VersionModelResponse
import club.fromfactory.utils.Zlog
import com.trello.rxlifecycle2.kotlin.bindToLifecycle
import io.reactivex.observers.DefaultObserver
import retrofit2.HttpException
import java.util.*

class UserLoginPresenter(view: LoginContract.View) : BasePresenter<LoginContract.View>(view), LoginContract.Presenter {


    private var packageVersionUrl = NetUtils.APP_MAIN_URL + "wms/pda/package_version"


    override fun getVersionUpdate() {


        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        val params = HashMap<String, Any>()
        params["version_code"] = BuildConfig.VERSION_CODE.toString()
        params["version_name"] = BuildConfig.VERSION_NAME

        BaseRetrofit
                .createService(ILoginApi::class.java)
                .getVersionUpdate(packageVersionUrl, headerMap, params)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : DefaultObserver<VersionModelResponse>() {
                    override fun onComplete() {
                    }

                    override fun onNext(httpJsonResult: VersionModelResponse) {
                        view.requestUpdateVersionSuccess(httpJsonResult.model)
                    }

                    override fun onError(e: Throwable) {

                        if (e is HttpException && e.code() in 400..403) {
                            val string = e.response()?.errorBody()?.string()
                            if (string != null) {
                                val parseJson = JsonUtils.parseJson<HttpJsonErrorResult>(string)
                                view.requestUpdateVersionFailed(parseJson?.message ?: "")
                            }
                        } else {
                            view.requestUpdateVersionFailed(FFApplication.getInstance().resources.getString(R.string.net_error))
                        }
                    }

                })
    }

    override fun login() {

        val username = view.getUserName()
        val password = view.getPassword()

        val ischeck = check(username, password)

        if (!ischeck) {
            return
        }

        val cookieMap = HashMap<String, Any>()
        cookieMap["warehouse"] = Constants.SERVER_LOCATION
        cookieMap["language"] = LanguageUtils.getLanguageCode()

        val headerMap = HashMap<String, Any>()
        headerMap["Cookie"] = CookieHelper.getCookieString(cookieMap)

        val params = HashMap<String, Any>()
        params["username"] = username
        params["password"] = password

        var url = NetUtils.APP_MAIN_URL_LOGIN + "jwt_auth/login"

        if (PreferenceStorageUtils.getInstance().developentState) {
            url = NetUtils.APP_MAIN_URL_LOGIN_TEST + "jwt_auth/login"
        }

        BaseRetrofit
                .createService(ILoginApi::class.java)
                .login(url, headerMap, params)
                .bindToLifecycle(view as RxAppCompatActivity)
                .loadingView(view)
                .subscribe(object : DefaultObserver<RequestLoginData>() {
                    override fun onComplete() {
                    }

                    override fun onNext(httpJsonResult: RequestLoginData) {
                        val loginData = httpJsonResult.data
                        val isSucceed = httpJsonResult.isIs_success

                        if (isSucceed && loginData != null) {
                            view.loginSucceed(httpJsonResult)
                        } else {

                            var message = FFApplication.getInstance().resources.getString(R.string.net_error_prompt_one)

                            try {
                                val messageArray = httpJsonResult.msg.non_field_errors
                                if (messageArray != null && messageArray.isNotEmpty()) {
                                    message = messageArray[0]
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                            view.showFailedError(message)
                        }

                    }

                    override fun onError(e: Throwable) {
                        if (e is HttpException && e.code() in 400..403) {
                            val string = e.response()?.errorBody()?.string()
                            if (string != null) {
                                val parseJson = JsonUtils.parseJson<HttpJsonErrorResult>(string)
                                view.showFailedError(parseJson?.message ?: "")
                            }
                        } else {
                            view.showFailedError(FFApplication.getInstance().resources.getString(R.string.net_error))
                        }
                    }

                })
    }

    private fun check(username: String, password: String): Boolean {
        var isCheck = true

        if (TextUtils.isEmpty(username)) {
            isCheck = false
            view.showFailedError(FFApplication.getInstance().resources.getString(R.string.login_account_prompt_hint))
        } else if (TextUtils.isEmpty(password)) {
            isCheck = false
            view.showFailedError(FFApplication.getInstance().resources.getString(R.string.login_password_prompt_hint))
        }

        return isCheck
    }

    fun clear() {
        view.clearUserName()
        view.clearPassword()
    }



    private var mDebugVerifyDialogUtils: DebugVerifyDialogUtils? = null
    private var mClickNum = 0
    private val IS_CLEAR = 100000//是否退出的参数

    override fun debugPanel() {
        mHandler.removeCallbacksAndMessages(null)
        mClickNum++

        val magicNumber = when {
            Zlog.isDebug || PreferenceStorageUtils.getInstance().debugState -> 1
            else -> 30
        }
        if (mClickNum >= magicNumber) {
            mClickNum = 0
            showVerifyDialog()
        } else {
            mHandler.sendEmptyMessageDelayed(IS_CLEAR, 2000)// 3秒后发送消息
        }

    }


    private fun showVerifyDialog() {

        mDebugVerifyDialogUtils = DebugVerifyDialogUtils().showVerifyDialog(view.context as BaseActivity, object : DebugVerifyDialogUtils.DebugVerifyInterface {
            override fun verifySuccess() {
                view.enterDebugPanel()
            }

            override fun verifyFailed() {
            }

        })

    }

    private val mHandler = Handler(Handler.Callback { msg ->
        when (msg.what) {
            IS_CLEAR -> mClickNum = 0
            else -> {
            }
        }
        false
    })


    override fun onDestroy(activity: BaseActivity) {
        mDebugVerifyDialogUtils?.finish(activity)
    }
}