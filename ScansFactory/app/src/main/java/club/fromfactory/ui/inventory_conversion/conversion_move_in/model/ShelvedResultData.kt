package club.fromfactory.ui.inventory_conversion.conversion_move_in.model

class ShelvedResultData {

    var finish : Boolean ?= null
    var taskCount : Int ?= null
}