package club.fromfactory.ui.hawb_binding.contract

import club.fromfactory.baselibrary.presenter.IPresenter
import club.fromfactory.baselibrary.view.IBaseMVPView
import club.fromfactory.ui.tracking.model.PackageInfo

class HawbBindingDetailContract {

    interface View :IBaseMVPView<Presenter> {

        fun hawbBindPackageSuccess(mPackageInfo: PackageInfo?)
        fun hawbBindPackageNeedComfirm(message: String?)
        fun showFailedErrorWindow(message: String?)

        fun hawbUnBindPackageSuccess()
        fun showFailedError(message: String?)

        fun getHawbId() :Int

    }
    interface Presenter :IPresenter<View> {

        fun hawbBindPackage(packageNo : String,is_confirmed: Boolean)

        fun hawbUnBindPackage(packageNo : String)

    }
}