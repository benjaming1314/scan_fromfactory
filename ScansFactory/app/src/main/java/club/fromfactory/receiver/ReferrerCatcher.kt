package club.fromfactory.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.google.android.gms.analytics.CampaignTrackingReceiver

class ReferrerCatcher : BroadcastReceiver() {


    override fun onReceive(context: Context?, intent: Intent?) {
        // Google Analytics
        CampaignTrackingReceiver().onReceive(context, intent)
    }

}