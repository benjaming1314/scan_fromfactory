//package club.fromfactory.scan;
//
//import android.app.Activity;
//import android.content.Context;
//
//import com.android.Scanner;
//
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
//
//import club.fromfactory.baselibrary.utils.StringUtils;
//
///**
// * @author lxm
// * @date 2018/10/16
// */
//public class IData95wScanUtils extends ScanView {
//
//    private Scanner mScanner;
//
//    private Scanner.DecodeCallback mDataListener;
//
//    public IData95wScanUtils(Context context) {
//
//        initScanner(context);
//
//    }
//
//    private void initScanner(Context context) {
//        mScanner = new Scanner();
//    }
//
//    @Override
//    public void onResume(Activity activity) {
//        //Set Callback
//        setDecodeListener();
//        mScanner.open();
//    }
//
//    @Override
//    public void onPause(Activity activity) {
//        removeListener();
//        //Remove Callback
//        mScanner.close();
//        mScanner.setDecodeCallBack(null);
//
//    }
//
//    @Override
//    public void startScan(boolean isFromKey) {
//        mScanner.StartScan();
//    }
//
//    @Override
//    public void stopScan() {
////        mScanner.StopScan();
//    }
//
//    private void setDecodeListener() {
//        mDataListener = new Scanner.DecodeCallback() {
//            @Override
//            public void onDecodeComplete(String data) {
//                scanShow("setDecodeListener1");
//                scanShow("setDecodeListener3" + data);
//                if (StringUtils.isNull(data)) {
//                    scanFailed("scan failed");
//                    return;
//                }
//                String regEx = "[^a-zA-Z0-9-]";
//                Pattern p = Pattern.compile(regEx);
//                Matcher m = p.matcher(data);
//                data = m.replaceAll("").trim();
//                scanSucceed(data);
//
//                scanShow("setDecodeListener3");
//            }
//
//        };
//        mScanner.setDecodeCallBack(mDataListener);
//        scanShow("setDecodeListener0");
//    }
//
//    @Override
//    public void onDestroy(Activity activity) {
//        super.onDestroy(activity);
//        mScanner = null;
//    }
//}
