package club.fromfactory.scan;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import com.zkc.Service.CaptureService;

import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import club.fromfactory.utils.Zlog;

/**
 * Created by lxm on 2017/2/20.
 * nf3501
 */

public class ScanUtils extends ScanView {
    private static final String SCAN_ACTION = "com.zkc.scancode";
    private Timer timer;

    private ScanBroadcastReceiver scanBroadcastReceiver;

    public ScanUtils() {
    }

    @Override
    public void onResume(Activity activity) {
        try {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(SCAN_ACTION);
            scanBroadcastReceiver = new ScanBroadcastReceiver();
            activity.registerReceiver(scanBroadcastReceiver, intentFilter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause(Activity activity) {
        removeListener();
        if (activity != null && scanBroadcastReceiver != null) {
            try {
                activity.unregisterReceiver(scanBroadcastReceiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void startScan(boolean isFromKey) {
        super.startScan(isFromKey);
        try {
            CaptureService.scanGpio.openScan();
            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    stopScan();
                    scanFailed("");
                }
            }, 3000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void stopScan() {
        super.stopScan();
        try {
            CaptureService.scanGpio.closeScan();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    class ScanBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Bundle extras = intent.getExtras();
            Zlog.ii("lxm ss ScanBroadcastReceiver:" + action + "   " + extras);
            if (action.equals(SCAN_ACTION)) {
                if (timer != null) {
                    timer.cancel();
                    timer = null;
                }
                stopScan();
                Zlog.ii("lxm ss mainactivity:extras " + extras);
                Zlog.ii("lxm ss mainactivity:action " + action);
                String scanResult = intent.getExtras().getString("code");

                String regEx = "[^a-zA-Z0-9-]";
                Pattern p = Pattern.compile(regEx);
                Matcher m = p.matcher(scanResult);
                scanResult = m.replaceAll("").trim();
                Zlog.ii("lxm ss mainactivity:scanresult2 " + scanResult);
                scanSucceed(scanResult);
            }
        }
    }

}
