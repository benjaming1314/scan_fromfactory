package club.fromfactory.scan

import android.content.Context
import club.fromfactory.constant.Constants

var mScanView: ScanView? = null

object ScanViewUtils {

    fun getScanView(context: Context): ScanView {

        if (mScanView != null) {
            return mScanView!!
        }
        return initScanView(context)
    }

    fun initScanView(context: Context): ScanView {
        when (Constants.PDA_TYPE) {
            Constants.PDA_TYPE_NF3501 -> mScanView = ScanUtils()
            Constants.PDA_TYPE_NEWPDA4001 -> mScanView = NewPDA4001SanUtils()
            Constants.PDA_TYPE_ZEBRA_MC36 -> mScanView = Mc36PDAScanUtils()
            Constants.PDA_TYPE_SEUIC_AUTOID9 -> mScanView = SeuicUtils(context)
            Constants.PDA_TYPE_50_SERIES -> mScanView = IData50ScanUtils(context)
        }

        return mScanView!!
    }

}