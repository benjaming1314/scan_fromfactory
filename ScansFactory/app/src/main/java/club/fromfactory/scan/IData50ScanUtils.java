package club.fromfactory.scan;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.idatachina.weystar.barcodeas.ScannerInterface;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author lxm
 * @date 2018/10/16
 */
public class IData50ScanUtils extends ScanView {

    private static final String RES_ACTION = "android.intent.action.SCANRESULT";
    //    private Dialog loadingDialog;
    private boolean isContinue = false;    //连续扫描的标志

    private ScannerInterface scanner;
    private BroadcastReceiver scanReceiver;

    public IData50ScanUtils(Context context) {

        initScanner(context);

    }

    private void initScanner(Context context) {


        scanner = new ScannerInterface(context);
        //scanner.open();//注意：扫描引擎上电，该接口请勿频繁调用，频繁关闭串口会导致程序卡死
        //scanner.resultScan();//注意：恢复iScan默认设置，频繁重置串口会导致程序卡死
        //scanner.close();//注意：恢复关闭扫描引擎电源，频繁重置串口会导致程序卡死

        /**设置扫描结果的输出模式，参数为0和1：
         * 0为模拟输出，同时广播扫描数据（在光标停留的地方输出扫描结果同时广播扫描数据）;
         * 1为广播输出（只广播扫描数据）；
         * 2为模拟按键输出；
         * */
        scanner.setOutputMode(1);

        //		scanner.lockScanKey();
        //		锁定设备的扫描按键,通过iScan定义的扫描键扫描。
        scanner.unlockScanKey();
        //		释放扫描按键的锁定，释放后iScan无法控制扫描按键，用户可自定义按键扫描。

        //		scanner.enablePlayBeep(true);//是否允许蜂鸣反馈
        //		scanner.enableFailurePlayBeep(true);//扫描失败蜂鸣反馈
        //		scanner.enablePlayVibrate(true);//震动开启与关闭
        //		scanner.timeOutSet(5);//设置扫描延时5秒
        //		scanner.intervalSet(0); //设置连续扫描间隔时间为0，单位为毫秒
    }

    @Override
    public void onResume(Activity activity) {

        try {
//            loadingDialog = DialogUtils.getInstance().getProgressDialog(activity, activity.getResources().getString(R.string.scanning_prompt));

            //扫描结果的意图过滤器action一定要使用"android.intent.action.SCANRESULT"
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(RES_ACTION);
            //注册广播接受者
            scanReceiver = new ScannerResultReceiver();
            activity.registerReceiver(scanReceiver, intentFilter);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onPause(Activity activity) {
        isContinue = false;
        removeListener();
        if (activity != null && scanReceiver != null) {
            try {
                activity.unregisterReceiver(scanReceiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void startScan(boolean isFromKey) {
        super.startScan(isFromKey);
        scanner.scan_start();
    }

    @Override
    public void stopScan() {
        super.stopScan();
        scanner.scan_stop();
    }

    @Override
    public void continceScan() {
        super.continceScan();
        isContinue = !isContinue;
//        scanner.continceScan(isContinue);
    }

    /**
     * 扫描结果广播接收
     */
    private class ScannerResultReceiver extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {

            String scanResult = intent.getStringExtra("value");

            /** 如果条码长度>0，解码成功。如果条码长度等于0解码失败。*/
            if (intent.getAction().equals(RES_ACTION)) {
                //获取扫描结果
                if (scanResult.length() > 0) {
                    String regEx = "[^a-zA-Z0-9-]";
                    Pattern p = Pattern.compile(regEx);
                    Matcher m = p.matcher(scanResult);
                    scanResult = m.replaceAll("").trim();
                    scanSucceed(scanResult);

                } else {//else if(scanResult.length()==0)
                    /**扫描失败提示使用有两个条件：
                     1，需要先将扫描失败提示接口打开只能在广播模式下使用，其他模式无法调用。
                     2，通过判断条码长度来判定是否解码成功，当长度等于0时表示解码失败。
                     * */
                    scanFailed("scan failed");
                }
            }
        }
    }


}
