package club.fromfactory.scan;

import android.app.Activity;
import android.content.Context;

import club.fromfactory.view.ScanResultListner;

/**
 * @author lxm
 * @date 2018/9/10
 */
public abstract class ScanView {

    private ScanResultListner scanResultListner ;

    public abstract void onResume(Activity activity);
    public abstract void onPause(Activity activity);
    public void onDestroy(Activity activity) {
    }

    public void startScan(boolean isFromKey) {
//        showLoadingView();

    }
    public void stopScan() {
//        hideLoadingView();
    }

    public void continceScan() {

    }

    public void setScanResultListner(ScanResultListner scanResultListner) {
        this.scanResultListner = scanResultListner ;
    }

    void scanFailed(String message) {
        if (scanResultListner != null) {
            scanResultListner.scanFailed(message);
        }
    }

    void scanSucceed(String scanResult) {
        if (scanResultListner != null) {
            scanResultListner.scanSucceed(scanResult);
        }
    }
    void scanShow(String str) {
        if (scanResultListner != null) {
            scanResultListner.showDialog(str);
        }
    }

    /**
     * 显示加载提示视图
     */
    void showLoadingView() {
        if (scanResultListner != null) {
            scanResultListner.showLoadingView();
        }
    }

    /**
     * 隐藏加载视图
     */
    void hideLoadingView(){
        if (scanResultListner != null) {
            scanResultListner.hideLoadingView();
        }
    }

    void removeListener() {
        if (scanResultListner != null) {
            scanResultListner = null;
        }
    }


}
