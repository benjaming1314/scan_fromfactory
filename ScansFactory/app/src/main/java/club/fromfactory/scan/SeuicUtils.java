package club.fromfactory.scan;

import android.app.Activity;
import android.content.Context;

import com.seuic.scanner.DecodeInfo;
import com.seuic.scanner.DecodeInfoCallBack;
import com.seuic.scanner.Scanner;
import com.seuic.scanner.ScannerFactory;
import com.seuic.scanner.ScannerKey;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import club.fromfactory.baselibrary.utils.StringUtils;
import club.fromfactory.view.ScanResultListner;

/**
 * @author lxm
 * @date 2018/9/7
 * Seuic PDT-90P
 */
public class SeuicUtils extends ScanView{
    private Scanner mScanner;
    private Thread thread ;

    public SeuicUtils(Context context) {

        //Get Scanner
        mScanner = ScannerFactory.getScanner(context);

        thread = new Thread(runnable);
        thread.start();

    }

    @Override
    public void onResume(Activity activity) {
        mScanner.setDecodeInfoCallBack(decodeInfoCallBack);
        mScanner.open();
    }

    @Override
    public void onPause(Activity activity) {
        scanShow("onPause");
        removeListener();
        //Remove Callback
        mScanner.close();
        mScanner.setDecodeCallBack(null);
        scanShow("onPause2");
    }

    @Override
    public void startScan(boolean isFromKey) {
        super.startScan(isFromKey);
        mScanner.startScan();
    }

    @Override
    public void stopScan() {
        super.stopScan();
        mScanner.stopScan();
    }

    @Override
    public void onDestroy(Activity activity) {
        super.onDestroy(activity);

        //Close Scanner
        ScannerKey.close();
        Scanner.startScanService(activity);
    }

    private DecodeInfoCallBack decodeInfoCallBack = new DecodeInfoCallBack() {
        @Override
        public void onDecodeComplete(DecodeInfo decodeInfo) {

            String data = decodeInfo.barcode;

            scanShow("setDecodeListener2");
            if (StringUtils.isNull(data)) {
                scanFailed("scan failed");
                return;
            }
            String regEx = "[^a-zA-Z0-9-]";
            Pattern p = Pattern.compile(regEx);
            Matcher m = p.matcher(data);
            data = m.replaceAll("").trim();
            scanSucceed(data);

            scanShow("setDecodeListener3");

        }
    };
    Runnable runnable = new Runnable() {

        @Override
        public void run() {
            int ret1 = ScannerKey.open();
            if(ret1 > -1){
                while(true){
                    int ret = ScannerKey.getKeyEvent();
                    if(ret > -1){
                        switch (ret) {
                            case ScannerKey.KEY_DOWN:
                                mScanner.startScan();
                                break;
                            case ScannerKey.KEY_UP:
                                mScanner.stopScan();
                                break;
                        }
                    }
                }
            }
        }
    };


}
