package club.fromfactory.scan;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.device.ScanDevice;
import android.os.Bundle;
import android.os.Handler;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import club.fromfactory.utils.Zlog;

/**
 * Created by lxm on 2017/12/14.
 * newpda 4001
 */

public class NewPDA4001SanUtils extends ScanView {
    public static final String SCAN_ACTION = "scan.rcv.message";

    private ScanBroadcastReceiver scanBroadcastReceiver;
    private ScanDevice sm;


    public NewPDA4001SanUtils() {
        try {
            sm = new ScanDevice();
            openScan();
            sm.setOutScanMode(0);//0是广播模式。1是键盘模式
            sm.setScanLaserMode(8);//4是持续模式，8是普通模式
            sm.setScanLaserTime(3000);//
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume(Activity activity) {
        if (!sm.isScanOpened()) {
            sm.openScan();
        }
        try {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(SCAN_ACTION);
            scanBroadcastReceiver = new ScanBroadcastReceiver();
            activity.registerReceiver(scanBroadcastReceiver, intentFilter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause(Activity activity) {
        if (sm.isScanOpened()) {
            sm.closeScan();
        }

        removeListener();
        if (activity != null && scanBroadcastReceiver != null) {
            try {
                activity.unregisterReceiver(scanBroadcastReceiver);
                scanBroadcastReceiver = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    //当不是键盘时需要打开扫描器
    @Override
    public void startScan(boolean isFromKey) {
        Zlog.ii("lxm ss scan:openScan");

        boolean isScanOpen = isScanOpened();
        if (isScanOpen) {
            scan(isFromKey);
        } else {
            boolean isOpen = openScan();
            if (isOpen) {
                scan(isFromKey);
            } else {
                scanFailed("open scan failed");
            }
        }
    }


    private void scan(boolean isKey) {
        if (!isKey) {
            try {
                if (sm != null) {
                    boolean isStart = sm.startScan();
                    Zlog.ii("lxm ss scanutil:startScan " + isStart);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                stopScan();
            }
        }, 3000);
    }

    private boolean isScanOpened() {
        try {
            if (sm != null) {
                boolean isOpen = sm.isScanOpened();
                Zlog.ii("lxm ss scanutil:isScanOpened " + isOpen);
                return isOpen;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    private boolean openScan() {
        try {
            if (sm != null) {
                boolean isOpen = sm.openScan();
                Zlog.ii("lxm ss scanutil:openScan " + isOpen);
                isScanOpened();
                return isOpen;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public void stopScan() {
        try {
            if (sm != null) {
                boolean isStop = sm.stopScan();
                Zlog.ii("lxm ss scanutil:stopScan " + isStop);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    class ScanBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Bundle extras = intent.getExtras();
            Zlog.ii("lxm ss scan: ScanBroadcastReceiver");
            Zlog.ii("lxm ss ScanBroadcastReceiver:" + action + "   " + extras);
            if (action.equals(SCAN_ACTION)) {

                Zlog.ii("lxm ss mainactivity:extras " + extras);
                Zlog.ii("lxm ss mainactivity:action " + action);

                byte[] barocode = intent.getByteArrayExtra("barocode");
                int barocodelen = intent.getIntExtra("length", 0);
                byte temp = intent.getByteExtra("barcodeType", (byte) 0);
                Zlog.ii("lxm ss mainactivity:----codetype--" + temp);
                String scanResult = new String(barocode, 0, barocodelen);
                stopScan();

                Zlog.ii("lxm ss mainactivity:scanresult1 " + scanResult + "  ");
                String regEx = "[^a-zA-Z0-9-]";
                Pattern p = Pattern.compile(regEx);
                Matcher m = p.matcher(scanResult);
                scanResult = m.replaceAll("").trim();
                Zlog.ii("lxm ss mainactivity:scanresult2 " + scanResult);

                scanSucceed(scanResult);

            }
        }
    }
}
