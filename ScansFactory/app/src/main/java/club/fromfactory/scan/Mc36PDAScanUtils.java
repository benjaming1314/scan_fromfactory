package club.fromfactory.scan;

import android.app.Activity;
import com.symbol.scanning.BarcodeManager;
import com.symbol.scanning.ScanDataCollection;
import com.symbol.scanning.Scanner;
import com.symbol.scanning.ScannerException;
import com.symbol.scanning.ScannerInfo;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import club.fromfactory.baselibrary.utils.StringUtils;

/**
 * zebra mc 36
 */
public class Mc36PDAScanUtils extends ScanView {

    private Scanner mScanner;
    private Scanner.DataListener mDataListener;
//    private List<ScannerInfo> scanInfoList;
    private boolean canDecode = true;

    public Mc36PDAScanUtils() {
        ScannerInfo mInfo =
                new ScannerInfo("se4710_cam_builtin", "DECODER_2D");
        BarcodeManager mBarcodeManager = new BarcodeManager();
        mScanner = mBarcodeManager.getDevice(mInfo);
//        scanInfoList = mBarcodeManager.getSupportedDevicesInfo();
    }

    @Override
    public void onResume(Activity activity) {

        try {
            mScanner.enable();
            setDecodeListener();
        } catch (ScannerException se) {
            se.printStackTrace();
        }
    }

    @Override
    public void onPause(Activity activity) {

        removeListener();
        try {
            if (!canDecode)
                mScanner.cancelRead();
            mScanner.disable();
        } catch (ScannerException se) {
            se.printStackTrace();
            Mc36PDAScanUtils.this.mScanner.removeDataListener(Mc36PDAScanUtils.this.mDataListener);
            Mc36PDAScanUtils.this.canDecode = true;
        } finally {
            mScanner.removeDataListener(mDataListener);
            canDecode = true;
        }
    }

    @Override
    public void startScan(boolean isFromKey) {
        super.startScan(isFromKey);
        if (canDecode) {
            canDecode = false;
            try {
                mScanner.read();
//                boolean bool = this.mScanner.getConfig().isParamSupported("mScannerConfig.decoderParams.upca.enabled");
//                Zlog.ii("upcaEnabled isParamSupported - " + bool);
//                Zlog.ii("upcaEnabled = " + this.mScanner.getConfig().decoderParams.upca.enabled);
//                if (!this.scanInfoList.isEmpty()) {
//                    Zlog.ii("scanInfoList is not empty");
//                    Iterator localIterator = this.scanInfoList.iterator();
//                    while (localIterator.hasNext()) {
//                        ScannerInfo localScannerInfo = (ScannerInfo) localIterator.next();
//                        Zlog.ii("scanning supprot " + localScannerInfo.getDeviceType());
//                    }
//                }
            } catch (ScannerException se) {
                se.printStackTrace();
            }
        }
    }

    @Override
    public void stopScan() {
        super.stopScan();
        if (!canDecode) {
            canDecode = true;
            try {
                mScanner.cancelRead();
            } catch (ScannerException se) {
                se.printStackTrace();
            }
        }

    }

    private void setDecodeListener() {
        mDataListener = new Scanner.DataListener() {
            public void onData(ScanDataCollection scanDataCollection) {
                String data = "";
                ArrayList<ScanDataCollection.ScanData> scanDataList = scanDataCollection.getScanData();

                for (ScanDataCollection.ScanData scanData : scanDataList) {
                    data = scanData.getData();
                }
                if (StringUtils.isNull(data)) {
                    scanFailed("scan failed");
                    canDecode = true;
                    return;
                }
                String regEx = "[^a-zA-Z0-9-]";
                Pattern p = Pattern.compile(regEx);
                Matcher m = p.matcher(data);
                data = m.replaceAll("").trim();
                scanSucceed(data);
                canDecode = true;
            }
        };
        mScanner.addDataListener(mDataListener);
    }


}
