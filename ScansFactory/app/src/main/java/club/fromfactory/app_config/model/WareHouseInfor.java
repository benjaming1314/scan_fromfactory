package club.fromfactory.app_config.model;

import club.fromfactory.baselibrary.model.NoProguard;

/**
 * Created by lxm on 2017/12/29.
 *
 */

public class WareHouseInfor implements NoProguard {

    private String house_name = ""; //仓库名字
    private String server_path ;
    private String server_path_login = "http://auth.yuceyi.com:80/" ;
    private String server_path_procurement ;
    private String server_path_tracking = "http://internal.tracking.yuceyi.com/" ;
    private String server_location ;

    public String getHouse_name() {
        return house_name;
    }

    public void setHouse_name(String house_name) {
        this.house_name = house_name;
    }

    public String getServer_path() {
        return server_path;
    }

    public void setServer_path(String server_path) {
        this.server_path = server_path;
    }

    public String getServer_path_login() {
        return server_path_login;
    }

    public void setServer_path_login(String server_path_login) {
        this.server_path_login = server_path_login;
    }

    public String getServer_path_procurement() {
        return server_path_procurement;
    }

    public void setServer_path_procurement(String server_path_procurement) {
        this.server_path_procurement = server_path_procurement;
    }

    public String getServer_location() {
        return server_location;
    }

    public void setServer_location(String server_location) {
        this.server_location = server_location;
    }

    public String getServer_path_tracking() {
        return server_path_tracking;
    }

    public void setServer_path_tracking(String server_path_tracking) {
        this.server_path_tracking = server_path_tracking;
    }

    @Override
    public String toString() {
        return "WareHouseInfor{" +
                "house_name='" + house_name + '\'' +
                ", server_path='" + server_path + '\'' +
                ", server_path_login='" + server_path_login + '\'' +
                ", server_path_procurement='" + server_path_procurement + '\'' +
                ", server_path_tracking='" + server_path_tracking + '\'' +
                ", server_location='" + server_location + '\'' +
                '}';
    }
}

