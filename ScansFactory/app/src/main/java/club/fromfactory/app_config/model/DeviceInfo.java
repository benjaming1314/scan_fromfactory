package club.fromfactory.app_config.model;

import java.util.List;

import club.fromfactory.baselibrary.model.NoProguard;

/**
 * Created by lxm on 2018/1/16.
 */

public class DeviceInfo implements NoProguard {

    private String type ;

    private String name ;

    private List<String> deviceTypeList ;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getDeviceTypeList() {
        return deviceTypeList;
    }

    public void setDeviceTypeList(List<String> deviceTypeList) {
        this.deviceTypeList = deviceTypeList;
    }

    @Override
    public String toString() {
        return "DeviceInfo{" +
                "type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", deviceTypeList=" + deviceTypeList +
                '}';
    }
}
