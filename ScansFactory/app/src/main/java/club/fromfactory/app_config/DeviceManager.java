package club.fromfactory.app_config;

import java.util.ArrayList;
import java.util.List;

import club.fromfactory.R;
import club.fromfactory.baselibrary.BuildConfig;
import club.fromfactory.baselibrary.utils.PreferenceStorageUtils;
import club.fromfactory.baselibrary.view.BaseActivity;
import club.fromfactory.utils.PromptDialogUtils;
import club.fromfactory.utils.Zlog;
import club.fromfactory.app_config.model.DeviceInfo;
import club.fromfactory.baselibrary.utils.DeviceUtils;
import club.fromfactory.baselibrary.utils.StringUtils;
import club.fromfactory.constant.Constants;

/**
 * Created by lxm on 2017/12/29.
 */

public class DeviceManager {

    private static DeviceManager mWareHouseManager;

    private DeviceManager() {
    }

    public static DeviceManager getInstance() {
        if (mWareHouseManager == null) {
            mWareHouseManager = new DeviceManager();
        }
        return mWareHouseManager;
    }

    public void checkDeviceType(BaseActivity baseActivity, String selectPdaType, final CheckPdaTypeListener checkPdaTypeListener) {
        String phoneModel = DeviceUtils.getPhoneModel().replace(" ", "").toLowerCase();

        String devicePdaType = getCurrentType(phoneModel);

        if (StringUtils.isNull(devicePdaType)|| !devicePdaType.equals(selectPdaType)) {

            PromptDialogUtils.getInstance().showDialog(baseActivity, baseActivity.getResources().getString(R.string.select_device_tips_4, phoneModel), new PromptDialogUtils.DialogCallback() {
                @Override
                public void onClickPositiveBtn() {
                    if (checkPdaTypeListener != null) {
                        checkPdaTypeListener.checkResult(true);
                    }
                }

                @Override
                public void onClickNegativeBtn() {
                    if (checkPdaTypeListener != null) {
                        checkPdaTypeListener.checkResult(false);
                    }
                }
            });

        } else {
            if (checkPdaTypeListener != null) {
                checkPdaTypeListener.checkResult(true);
            }
        }

    }
    public void checkDeviceType(final CheckPdaTypeListener checkPdaTypeListener) {
        String phoneModel = DeviceUtils.getPhoneModel().replace(" ", "").toLowerCase();

        String devicePdaType = getCurrentType(phoneModel);

        if (StringUtils.isNull(devicePdaType)|| !devicePdaType.equals(Constants.PDA_TYPE)) {

            if (checkPdaTypeListener != null) {
                checkPdaTypeListener.checkResult(false);
            }

        } else {
            if (checkPdaTypeListener != null) {
                checkPdaTypeListener.checkResult(true);
            }
        }

    }

    public String getPdaType() {
        String pdaType = PreferenceStorageUtils.getInstance().getPdaType();
        if (StringUtils.isNull(pdaType)) {
            pdaType = BuildConfig.PDA_TYPE;
            PreferenceStorageUtils.getInstance().savePdaType(pdaType);
        }
        return pdaType;

    }

    public String getCurrentTypeName() {
        List<DeviceInfo> deviceInfoList = getDeviceInfoList();

        String currentType = "";

        for (DeviceInfo deviceInfo :
                deviceInfoList) {

            if (deviceInfo.getType().equalsIgnoreCase(Constants.PDA_TYPE)) {
                currentType = deviceInfo.getName();
                break;
            }
        }

        return currentType;

    }


    public String getCurrentType(String phoneModel) {
        List<DeviceInfo> deviceInfoList = getDeviceInfoList();

        String currentType = "";

        for (DeviceInfo deviceInfo :
                deviceInfoList) {
            List<String> deviceTypeList = deviceInfo.getDeviceTypeList();

            for (int i = 0; i < deviceTypeList.size(); i++) {
                Zlog.ii("lxmgetCurrentType :" + phoneModel + " " + deviceTypeList.get(i));
                String str = deviceTypeList.get(i).replace(" ", "").toLowerCase();

                if (str.equals(phoneModel)) {
                    currentType = deviceInfo.getType();
                    break;
                }
            }
        }

        return currentType;

    }

    public String getCurrentTypeName(String phoneModel) {
        List<DeviceInfo> deviceInfoList = getDeviceInfoList();

        String currentTypeName = "";

        for (DeviceInfo deviceInfo :
                deviceInfoList) {

            List<String> deviceTypeList = deviceInfo.getDeviceTypeList();

            for (int i = 0; i < deviceTypeList.size(); i++) {
                Zlog.ii("lxmgetCurrentType :" + phoneModel + " " + deviceTypeList.get(i));
                String str = deviceTypeList.get(i).replace(" ", "").toLowerCase();

                if (str.equals(phoneModel)) {
                    currentTypeName = deviceInfo.getName();
                    break;
                }
            }

        }

        return currentTypeName;

    }

    public List<DeviceInfo> getDeviceInfoList() {
        List<DeviceInfo> mDeviceInfoList = new ArrayList<>();

        DeviceInfo deviceInfo = new DeviceInfo();
        List<String> deviceTypeList = new ArrayList<>();
        deviceTypeList.add("pda");
        deviceTypeList.add("U8000S");
        deviceInfo.setDeviceTypeList(deviceTypeList);
        deviceInfo.setName("New Device");
        deviceInfo.setType(Constants.PDA_TYPE_NEWPDA4001);

        mDeviceInfoList.add(deviceInfo);
        deviceInfo = new DeviceInfo();
        deviceTypeList = new ArrayList<>();
        deviceTypeList.add("ww808_emmc");
        deviceTypeList.add("nf3501");
        deviceInfo.setDeviceTypeList(deviceTypeList);
        deviceInfo.setName("Old Device");
        deviceInfo.setType(Constants.PDA_TYPE_NF3501);
        mDeviceInfoList.add(deviceInfo);

        deviceInfo = new DeviceInfo();
        deviceTypeList = new ArrayList<>();
        deviceTypeList.add("mc36");
        deviceInfo.setDeviceTypeList(deviceTypeList);
        deviceInfo.setName("Zebra MC36 Device");
        deviceInfo.setType(Constants.PDA_TYPE_ZEBRA_MC36);
        mDeviceInfoList.add(deviceInfo);

        deviceInfo = new DeviceInfo();
        deviceTypeList = new ArrayList<>();
        deviceTypeList.add("PDT-90P");
        deviceInfo.setDeviceTypeList(deviceTypeList);
        deviceInfo.setName("Seuic Autoid9 Device");
        deviceInfo.setType(Constants.PDA_TYPE_SEUIC_AUTOID9);
        mDeviceInfoList.add(deviceInfo);

        deviceInfo = new DeviceInfo();
        deviceTypeList = new ArrayList<>();
        deviceTypeList.add("50 Series");
        deviceTypeList.add("95W Series");
        deviceInfo.setDeviceTypeList(deviceTypeList);
        deviceInfo.setName("iData 50/95w Device");
        deviceInfo.setType(Constants.PDA_TYPE_50_SERIES);
        mDeviceInfoList.add(deviceInfo);

        return mDeviceInfoList;
    }

    public boolean changeDevice(DeviceInfo deviceInfo) {

        if (Constants.PDA_TYPE.equalsIgnoreCase(deviceInfo.getType())) {
            return false;
        }
        Constants.PDA_TYPE = deviceInfo.getType();

        PreferenceStorageUtils.getInstance().savePdaType(Constants.PDA_TYPE);

        return true;

    }


    public interface CheckPdaTypeListener {

        void checkResult(boolean isGo);
    }


}
