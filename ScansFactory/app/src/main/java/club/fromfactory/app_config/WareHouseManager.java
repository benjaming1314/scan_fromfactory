package club.fromfactory.app_config;

import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import club.fromfactory.baselibrary.BuildConfig;
import club.fromfactory.baselibrary.utils.JsonUtil;
import club.fromfactory.baselibrary.utils.PreferenceStorageUtils;
import club.fromfactory.utils.GsonUtils;
import club.fromfactory.utils.Zlog;
import club.fromfactory.app_config.model.WareHouseInfor;
import club.fromfactory.baselibrary.utils.StringUtils;
import club.fromfactory.constant.Constants;
import club.fromfactory.http.NetUtils;

/**
 * Created by lxm on 2017/12/29.
 */

public class WareHouseManager {


    private static WareHouseManager mWareHouseManager;

    private WareHouseManager() {
    }

    public static WareHouseManager getInstance() {
        if (mWareHouseManager == null) {
            mWareHouseManager = new WareHouseManager();
        }
        return mWareHouseManager;
    }

    public String getHouseName() {
        String houseName = PreferenceStorageUtils.getInstance().getHouseName();

        if (StringUtils.isNull(houseName)) {
            houseName = BuildConfig.HOUSE_NAME;
            PreferenceStorageUtils.getInstance().saveHouseName(houseName);
        }
        return houseName;

    }

    public String getWareHouseInfoServer() {

        String wareHouseInfoServer = PreferenceStorageUtils.getInstance().getWareHouseInfoServer();
        if (StringUtils.isNull(wareHouseInfoServer)) {
            wareHouseInfoServer = BuildConfig.SERVER_HOUSE_PATH;
            PreferenceStorageUtils.getInstance().saveWareHouseInfoServer(wareHouseInfoServer);
        }
        return wareHouseInfoServer;

    }

    public String getServerLotion() {
        String houseServerLocation = PreferenceStorageUtils.getInstance().getHouseServerLocation();
        if (StringUtils.isNull(houseServerLocation)) {
            houseServerLocation = BuildConfig.SERVER_LOCATION;
            PreferenceStorageUtils.getInstance().saveHouseServerLocation(houseServerLocation);
        }
        return houseServerLocation;

    }

    public String getServerPath() {
        String serverPath = PreferenceStorageUtils.getInstance().getHouseServerPath();
        if (StringUtils.isNull(serverPath)) {
            serverPath = BuildConfig.SERVER_PATH;
            PreferenceStorageUtils.getInstance().saveHouseServerPath(serverPath);
        }
        return serverPath;
    }

    public String getServerPathLogin() {
        String serverPathLogin = PreferenceStorageUtils.getInstance().getHouseServerPathLogin();
        if (StringUtils.isNull(serverPathLogin)) {
            serverPathLogin = BuildConfig.SERVER_PATH_LOGIN;
            PreferenceStorageUtils.getInstance().saveHouseServerPathLogin(serverPathLogin);
        }
        return serverPathLogin;
    }

    public String getServerProcurement() {
        String houseServerPathProcurement = PreferenceStorageUtils.getInstance().getHouseServerPathProcurement();
        if (StringUtils.isNull(houseServerPathProcurement)) {
            houseServerPathProcurement = BuildConfig.SERVER_PATH_PROCUREMENT;
            PreferenceStorageUtils.getInstance().saveHouseServerPathProcurement(houseServerPathProcurement);
        }
        return houseServerPathProcurement;
    }

    public String getServerPathTracking() {
        String houseServerPathTracking = PreferenceStorageUtils.getInstance().getHouseServerPathTracking();
        if (StringUtils.isNull(houseServerPathTracking)) {
            houseServerPathTracking = BuildConfig.SERVER_PATH_TRACKING;
            PreferenceStorageUtils.getInstance().saveHouseServerPathTracking(houseServerPathTracking);
        }
        return houseServerPathTracking;
    }

    public List<WareHouseInfor> saveWareHouseInfo(List<WareHouseInfor> wareHouseInfoList) {

        try {
            PreferenceStorageUtils.getInstance().setWarehousePdaInfo(JsonUtil.getInstance().toJson(wareHouseInfoList));
//            if (BuildConfig.DEBUG) {
//                wareHouseInfoList.addAll(0, getDevAllHouse());
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return wareHouseInfoList;

    }

    public List<WareHouseInfor> getWareHouseInfo() {
        List<WareHouseInfor> infoList = null;
        String warehousePdaInfo = PreferenceStorageUtils.getInstance().getWarehousePdaInfo();

        if (StringUtils.isNotBlank(warehousePdaInfo)) {

            TypeToken<List<WareHouseInfor>> typeToken = new TypeToken<List<WareHouseInfor>>() {
            };
            try {
                infoList = (List<WareHouseInfor>) GsonUtils.getInstance().parseJson(warehousePdaInfo, typeToken);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

//        if (infoList != null && infoList.size() > 0) {
//
//            boolean debugState = PreferenceStorageUtils.getInstance().getDebugState();
//
//            if (BuildConfig.DEBUG || debugState) {
//
//                infoList.addAll(0, getDevAllHouse());
//            }
//        }

        return infoList;

    }

//    public boolean changeDevHouse() {
//
//        PreferenceStorageUtils.getInstance().saveDebugState(true);
//
//        return changeHouse(getDevDefaultHouse());
//
//    }



//    private List<WareHouseInfor> getDevAllHouse() {
//        List<WareHouseInfor> devInfoList= new ArrayList<>();
//
//        List<WareHouseInfor> infoList = null;
//        String warehousePdaInfo = PreferenceStorageUtils.getInstance().getWarehousePdaInfo();
//
//        if (StringUtils.isNotBlank(warehousePdaInfo)) {
//
//            TypeToken<List<WareHouseInfor>> typeToken = new TypeToken<List<WareHouseInfor>>() {
//            };
//            try {
//                infoList = (List<WareHouseInfor>) GsonUtils.getInstance().parseJson(warehousePdaInfo, typeToken);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//
////        if (infoList == null || infoList.size() == 0) {
////            devInfoList.add(getDevDefaultHouse());
////        }else  {
////
////            for (WareHouseInfor wareHouseInfor :
////                    infoList) {
////                WareHouseInfor devDefaultHouse = getDevDefaultHouse();
////                devDefaultHouse.setHouse_name("dev(" + wareHouseInfor.getHouse_name()+")");
////                devDefaultHouse.setServer_location(wareHouseInfor.getServer_location());
////                devInfoList.add(devDefaultHouse);
////
////            }
////        }
//
//        return devInfoList ;
//
//    }

//    private WareHouseInfor getDevDefaultHouse() {
//
//        WareHouseInfor wareHouseInfor = new WareHouseInfor();
//        wareHouseInfor.setHouse_name("dev(余杭仓)");
//        wareHouseInfor.setServer_path("http://dev.yuceyi.com:5678/");
//        wareHouseInfor.setServer_path_login("http://dev.yuceyi.com:8899/");
//        wareHouseInfor.setServer_path_procurement("http://test-pms.yuceyi.com/");
//        wareHouseInfor.setServer_path_tracking("http://test-internal.tracking.yuceyi.com/");
//        wareHouseInfor.setServer_location("yuhang");
//
//
//        return wareHouseInfor ;
//    }

    public boolean changeHouse(WareHouseInfor wareHouseInfor) {

        Zlog.ii("lxm changeHouse:" + wareHouseInfor);
        String houseName = wareHouseInfor.getHouse_name();

        if (Constants.HOUSE_NAME.equals(houseName)) {
            return false;
        }

        String server_location = wareHouseInfor.getServer_location();
        String server_path = wareHouseInfor.getServer_path();
        String server_path_login = wareHouseInfor.getServer_path_login();
        String server_path_procurement = wareHouseInfor.getServer_path_procurement();
        String server_path_tracking = wareHouseInfor.getServer_path_tracking();

        PreferenceStorageUtils.getInstance().saveHouseName(houseName);

        PreferenceStorageUtils.getInstance().saveHouseServerPath(server_path);
        PreferenceStorageUtils.getInstance().saveHouseServerPathLogin(server_path_login);
        PreferenceStorageUtils.getInstance().saveHouseServerPathProcurement(server_path_procurement);
        PreferenceStorageUtils.getInstance().saveHouseServerPathTracking(server_path_tracking);
        PreferenceStorageUtils.getInstance().saveHouseServerLocation(server_location);

        NetUtils.APP_MAIN_URL = server_path;
        NetUtils.APP_MAIN_URL_LOGIN = server_path_login;
        NetUtils.APP_MAIN_URL_PROCUREMENT = server_path_procurement;
        NetUtils.APP_MAIN_URL_TRACKING = server_path_tracking;

        Constants.HOUSE_NAME = houseName;
        Constants.SERVER_LOCATION = server_location;

        Zlog.ii("lxm changeHouse:" + NetUtils.APP_MAIN_URL);
        Zlog.ii("lxm changeHouse:" + NetUtils.APP_MAIN_URL_LOGIN);
        Zlog.ii("lxm changeHouse:" + NetUtils.APP_MAIN_URL_PROCUREMENT);

        return true;
    }

}
