//package club.fromfactory.base;
//
//import android.os.Bundle;
//import android.support.v4.content.LocalBroadcastManager;
//import android.support.v7.app.AppCompatActivity;
//import android.view.KeyEvent;
//
//import butterknife.ButterKnife;
//import club.fromfactory.FFApplication;
//import club.fromfactory.baselibrary.widget.CustomDialog;
//import club.fromfactory.utils.DialogUtils;
//import club.fromfactory.utils.Zlog;
//
//public abstract class BaseActivity extends AppCompatActivity {
//
//    public LocalBroadcastManager localBroadcastManager;
//
//    private CustomDialog mCustomDialog;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView();
//        ButterKnife.bind(this);
//        initData();
//        initView();
//        fetchDada();
//    }
//
//    public void initData(){
//        FFApplication.addActivity(this);
//        localBroadcastManager = LocalBroadcastManager.getInstance(this);
//        mCustomDialog = DialogUtils.getInstance().getProgressDialog(this, null);
//    }
//    public void initView(){
//
//    }
//    public void fetchDada(){
//
//    }
//
//    public void getSpData(String key) {
//
//    }
//
//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        Zlog.ii("lxm ss onKeyDown:" + keyCode + "   " + event + "  " + event.getKeyCode());
//        switch (event.getKeyCode()) {
//            case KeyEvent.ACTION_DOWN: {
//            }
//            break;
//            case KeyEvent.KEYCODE_BACK: {
//                clickKeyCodeBack();
//                return true;
//            }
//        }
//        return super.onKeyDown(keyCode, event);
//    }
//
//    public abstract void clickKeyCodeBack();
//
//
//    public abstract int getLayoutResId();
//
//
//
//    /**
//     * 设置视图
//     */
//    protected void setContentView() {
//        setContentView(getLayoutResId());
//    }
//
//    public void showBaseProgressDialog() {
//        if (isAlive() && mCustomDialog != null) {
//            mCustomDialog.show();
//        }
//    }
//
//    public void hideBaseProgressDialog() {
//        if (mCustomDialog != null && mCustomDialog.isShowing()) {
//            mCustomDialog.dismiss();
//        }
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//    }
//
//    @Override
//    protected void onStart() {
//        super.onStart();
//    }
//
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        if (mCustomDialog != null) {
//            if (mCustomDialog.isShowing()) {
//                mCustomDialog.dismiss();
//            }
//            mCustomDialog = null ;
//        }
//        FFApplication.removeActivity(this);
//    }
//
//    public boolean isAlive() {
//        return (!isDestroyed() && !isFinishing());
//    }
//
//}
