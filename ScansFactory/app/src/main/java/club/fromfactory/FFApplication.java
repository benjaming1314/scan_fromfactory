package club.fromfactory;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import com.crashlytics.android.Crashlytics;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipelineConfig;
import com.tencent.bugly.Bugly;
import com.tencent.bugly.crashreport.CrashReport;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import club.fromfactory.baselibrary.BaseApplication;
import club.fromfactory.baselibrary.router.RouterManager;
import club.fromfactory.baselibrary.utils.DeviceUtils;
import club.fromfactory.utils.Zlog;
import io.fabric.sdk.android.Fabric;

/**
 * Created by fengzhe on 15/12/29.
 */
public class FFApplication extends BaseApplication {
    private static FFApplication instance;

    /*
     *
     * mActivityList:Activities for children of BaseActivity.
     */
    private static List<AppCompatActivity> mActivityList = new LinkedList<AppCompatActivity>();

    /**
     * 每次执行限定个数个任务的线程池
     */
    private static ExecutorService mFixedThreadExecutor = null;
    private static ExecutorService mCachedThreadPool = null;


    public static FFApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;
        mFixedThreadExecutor = Executors.newFixedThreadPool(5);
        mCachedThreadPool = Executors.newCachedThreadPool();
        initBugly();

        ImagePipelineConfig config = ImagePipelineConfig.newBuilder(this)
                .setDownsampleEnabled(true)
                .build();
        Fresco.initialize(this, config);

    }

    @Override
    protected void onCheckedProcess() {
        super.onCheckedProcess();
        initCrashLytics(this);
        initRouter();
    }

    /**
     * 初始化 crashLytics
     */
    void initCrashLytics(Context context) {
        try {
            Fabric.with(context, new Crashlytics());

            Crashlytics.setUserIdentifier(DeviceUtils.getAndroidId());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void initRouter() {
        // 初始化路由
        RouterManager.init();

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    private void initBugly() {

//        Bugly.init(getApplicationContext(), "d30a706459", true);

        String packageName = getPackageName();
        String processName = getProcessName(android.os.Process.myPid());
        CrashReport.UserStrategy strategy = new CrashReport.UserStrategy(this);
        strategy.setUploadProcess(processName == null || processName.equals(packageName));
        CrashReport.initCrashReport(this, "d30a706459", Zlog.isDebug, strategy);

    }

    /**
     * 获取进程号对应的进程名
     *
     * @param pid 进程号
     * @return 进程名
     */
    private static String getProcessName(int pid) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader("/proc/" + pid + "/cmdline"));
            String processName = reader.readLine();
            if (!TextUtils.isEmpty(processName)) {
                processName = processName.trim();
            }
            return processName;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }
        return null;
    }

    public static ExecutorService getFixThreadExecutor() {
        return mFixedThreadExecutor;
    }

    public static ExecutorService getCacheThreadExecutor() {
        return mCachedThreadPool;
    }

    public static void addActivity(AppCompatActivity activity) {
        mActivityList.add(activity);
    }

    public static void removeActivity(AppCompatActivity activity) {
        mActivityList.remove(activity);
    }

    public static void finishAllAndExit() {
        try {
            for (AppCompatActivity activity : mActivityList) {
                if (activity != null)
                    activity.finish();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(1);
        }
    }


}
