package com.idatachina.weystar.barcodeas;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 开发注意事项：
 * 	1，该开发包基于iScan开发，需要预装iScan4.2.0以上版本。
 *  2，该Demo使用广播接收数据，其他接口均屏蔽，如需使用其他接口请参考文档并联系技术支持使用。
 *  3，该代码仅作为广播开发参考，切勿直接将工程直接拷入使用。
 */
public class MainActivity extends Activity{

    private boolean isContinue = false;	//连续扫描的标志

    TextView          tvScanResult;
    ScannerInterface  scanner;
    IntentFilter      intentFilter;
    BroadcastReceiver scanReceiver;
    private static final String RES_ACTION = "android.intent.action.SCANRESULT";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvScanResult = (TextView) this.findViewById(R.id.tv_scan_result);
        initScanner();
    }

    private void initScanner(){
        scanner = new ScannerInterface(this);

        //scanner.open();//注意：扫描引擎上电，该接口请勿频繁调用，频繁关闭串口会导致程序卡死
        //scanner.resultScan();//注意：恢复iScan默认设置，频繁重置串口会导致程序卡死
        //scanner.close();//注意：恢复关闭扫描引擎电源，频繁重置串口会导致程序卡死

        /**设置扫描结果的输出模式，参数为0和1：
         * 0为模拟输出，同时广播扫描数据（在光标停留的地方输出扫描结果同时广播扫描数据）;
         * 1为广播输出（只广播扫描数据）；
         * 2为模拟按键输出；
         * */
        scanner.setOutputMode(1);

        //		scanner.lockScanKey();
        //		锁定设备的扫描按键,通过iScan定义的扫描键扫描。
        scanner.unlockScanKey();
        //		释放扫描按键的锁定，释放后iScan无法控制扫描按键，用户可自定义按键扫描。

        //		scanner.enablePlayBeep(true);//是否允许蜂鸣反馈
        //		scanner.enableFailurePlayBeep(true);//扫描失败蜂鸣反馈
        //		scanner.enablePlayVibrate(true);//震动开启与关闭
        //		scanner.timeOutSeintentFiltert(5);//设置扫描延时5秒
        //		scanner.intervalSet(0); //设置连续扫描间隔时间为0，单位为毫秒
        //扫描结果的意图过滤器action一定要使用"android.intent.action.SCANRESULT"
        intentFilter = new IntentFilter();
        intentFilter.addAction(RES_ACTION);
        //注册广播接受者
        scanReceiver = new ScannerResultReceiver();
        registerReceiver(scanReceiver, intentFilter);
    }

    /**---------------------------------------------------------------------*/
    /**以下为客户自定义按键处理方式:
     * 指定只能按键键值为139的物理按键（中间黄色按键）按下来触发扫描*/
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 139&&event.getRepeatCount()==0){
            scanner.scan_start();
        }
        return super.onKeyDown(keyCode, event);
    }
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == 139){	/**按键弹起，停止扫描*/
            scanner.scan_stop();
        }else if (keyCode == 140){
            scanner.scan_stop();
            isContinue=!isContinue;
            if(isContinue){
                scanner.continceScan(true);
            }else{
                scanner.continceScan(false);
            }
        }
        return super.onKeyUp(keyCode, event);
    }
    /**---------------------------------------------------------------------*/

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
    }

    /**
     * 扫描结果广播接收
     */
    private class ScannerResultReceiver extends BroadcastReceiver{
        public void onReceive(Context context, Intent intent) {
            Log.d("111","intent.getAction()-->"+intent.getAction());

            final String scanResult = intent.getStringExtra("value");

            /** 如果条码长度>0，解码成功。如果条码长度等于0解码失败。*/
            if (intent.getAction().equals(RES_ACTION)){
                //获取扫描结果
                if(scanResult.length()>0){
                    Log.d("111","----->扫描成功！");

                    tvScanResult.append("Barcode："+scanResult+"\n");
                }else{//else if(scanResult.length()==0)
                    /**扫描失败提示使用有两个条件：
                     1，需要先将扫描失败提示接口打开只能在广播模式下使用，其他模式无法调用。
                     2，通过判断条码长度来判定是否解码成功，当长度等于0时表示解码失败。
                     * */
                    Log.d("111","----->扫描失败！");
                    Toast.makeText(getApplicationContext(), "解码失败！", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}
