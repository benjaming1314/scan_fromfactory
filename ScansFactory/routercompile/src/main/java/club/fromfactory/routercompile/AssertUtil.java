package club.fromfactory.routercompile;

import javax.annotation.processing.Messager;
import javax.tools.Diagnostic;

/**
 * Router模块的断言工具类
 *
 * @author nichenjian
 * @date 2018/10/14
 */

class AssertUtil {
    private Messager messager;

    AssertUtil(Messager messager) {
        this.messager = messager;
    }

    /**
     * 打印日志
     *
     * @param message 日志
     */
    void log(String message) {
        messager.printMessage(Diagnostic.Kind.NOTE, message);
    }

    /**
     * 打印错误日志（中断编译）
     *
     * @param message 日志
     */
    void error(String message) {
        messager.printMessage(Diagnostic.Kind.ERROR, message);
    }

    /**
     * 打印警告日志
     *
     * @param message 日志
     */
    void warning(String message) {
        messager.printMessage(Diagnostic.Kind.WARNING, message);
    }
}
