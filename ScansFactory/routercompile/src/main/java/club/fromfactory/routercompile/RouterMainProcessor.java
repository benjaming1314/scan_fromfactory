package club.fromfactory.routercompile;

import club.fromfactory.routerannotaions.Router;
import club.fromfactory.routerannotaions.RouterParam;
import com.google.auto.service.AutoService;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Filer;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;

/**
 * 路由主入口
 *
 * @author nichenjian
 * @date 2018/10/14
 */
@AutoService(Processor.class)
public class RouterMainProcessor extends AbstractProcessor {
    private Filer mFiler;
    private Elements mElementUtils;
    private AssertUtil mAssertUtil;
    private Map<TypeElement, RouterParamParser> mRouterParamParserMap;

    @Override
    public synchronized void init(ProcessingEnvironment processingEnvironment) {
        super.init(processingEnvironment);
        mFiler = processingEnvironment.getFiler();
        mElementUtils = processingEnvironment.getElementUtils();
        mAssertUtil = new AssertUtil(processingEnvironment.getMessager());
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        Set<String> annotationTypes = new HashSet<>();
        annotationTypes.add(Router.class.getCanonicalName());
        annotationTypes.add(RouterParam.class.getCanonicalName());
        return annotationTypes;
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latestSupported();
    }

    @Override
    public boolean process(Set<? extends TypeElement> set, RoundEnvironment env) {
        parseRouterParamAnnotation(env);
        parserRouterAnnotation(env);
        return true;
    }

    private void parserRouterAnnotation(RoundEnvironment env) {
        new RouterProcessor(env, mFiler, mRouterParamParserMap).parse();
    }

    private void parseRouterParamAnnotation(RoundEnvironment env) {
        mRouterParamParserMap = new RouterParamProcessor(env, mFiler, mElementUtils, mAssertUtil).parse();
    }
}
