package club.fromfactory.routercompile;

import club.fromfactory.routerannotaions.RouterParam;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeSpec;
import com.squareup.javapoet.TypeVariableName;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.lang.model.element.Element;
import javax.lang.model.element.Modifier;

/**
 * RouterParam注解解析模块 针对每个添加{@link RouterParam}注解的类，生成$$Router类
 *
 * @author nichenjian
 * @date 2018/10/14
 */
class RouterParamParser {

    private final Map<RouterParam, Element> mRouterParamsMap = new LinkedHashMap<>();
    private final String mPackageName;
    private final String mTargetClass;

    RouterParamParser(String packageName,
            String targetClass) {
        mPackageName = packageName;
        mTargetClass = targetClass;
    }

    /**
     * 创建$$Router类
     *
     * @return JavaFile $$Router类
     */
    JavaFile brewJava() {
        TypeSpec.Builder routerHandler = TypeSpec
                .classBuilder(mTargetClass + Constants.ROUTER_SUFFIX)
                .addTypeVariable(TypeVariableName.get("T", ClassName.bestGuess(mTargetClass)))
                .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
                .addSuperinterface(ParameterizedTypeName
                        .get(Constants.ClassNameList.IROUTER_INTERFACE, TypeVariableName.get("T")))
                .addMethod(createBindMethod());

        return JavaFile.builder(mPackageName, routerHandler.build())
                .addFileComment("Generated code from Router. Do not modify!")
                .build();
    }

    void addRouterParam(RouterParam routerParam, Element element) {
        mRouterParamsMap.put(routerParam, element);
    }

    /**
     * 创建bind方法
     *
     * @return MethodSpec bind方法
     */
    private MethodSpec createBindMethod() {
        MethodSpec.Builder builder = MethodSpec.methodBuilder("bind")
                .addAnnotation(Override.class)
                .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
                .addParameter(TypeVariableName.get("T"), "router", Modifier.FINAL)
                .addParameter(Constants.ClassNameList.BUNDLE_CLASS, "bundle", Modifier.FINAL);

        builder.beginControlFlow("if (bundle != null)");
        for (Map.Entry<RouterParam, Element> entry : mRouterParamsMap.entrySet()) {
            String[] routerParamValue = entry.getKey().value();
            for (String routerParam : routerParamValue) {
                builder.beginControlFlow("if (bundle.containsKey($S))", routerParam);
                builder.beginControlFlow("try");
                builder.addStatement("router.$L = $T.castValue(bundle.get($S))",
                        entry.getValue().getSimpleName(),
                        Constants.ClassNameList.ROUTER_MANAGER_CLASS,
                        routerParam);
                builder.nextControlFlow("catch(Exception e)");
                builder.addStatement("e.printStackTrace()");
                builder.endControlFlow();
                builder.endControlFlow();
            }
        }
        builder.endControlFlow();
        return builder.build();
    }

    Map<RouterParam, Element> getRouterParamsMap() {
        return mRouterParamsMap;
    }
}
