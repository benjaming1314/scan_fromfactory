package club.fromfactory.routercompile;

import static javax.lang.model.element.ElementKind.CLASS;
import static javax.lang.model.element.Modifier.PRIVATE;
import static javax.lang.model.element.Modifier.STATIC;

import club.fromfactory.routerannotaions.RouterParam;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import javax.annotation.processing.Filer;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;

/**
 * 路由参数解析
 *
 * @author nichenjian
 * @date 2018/10/14
 */
class RouterParamProcessor {

    private final RoundEnvironment mEnv;
    private final Filer mFiler;
    private final AssertUtil mAssertUtil;
    private Elements mElementUtils;

    RouterParamProcessor(RoundEnvironment env,
                         Filer filer,
                         Elements elementUtils,
                         AssertUtil assertUtil) {
        mEnv = env;
        mFiler = filer;
        mElementUtils = elementUtils;
        mAssertUtil = assertUtil;
    }

    Map<TypeElement, RouterParamParser> parse() {
        Map<TypeElement, RouterParamParser> routerParamMap = new LinkedHashMap<>();
        Set<? extends Element> routerParamSet = mEnv.getElementsAnnotatedWith(RouterParam.class);

        for (Element elem : routerParamSet) {
            RouterParam routerParam = elem.getAnnotation(RouterParam.class);
            TypeElement typeElement = (TypeElement) elem.getEnclosingElement();

            if (!isInaccessibleViaGeneratedCode(elem, typeElement)) {
                RouterParamParser parser = getOrCreateRouterParamClass(routerParamMap, typeElement);
                if (parser != null) {
                    parser.addRouterParam(routerParam, elem);
                }
            }
        }

        for (Map.Entry<TypeElement, RouterParamParser> entry : routerParamMap.entrySet()) {
            try {
                entry.getValue().brewJava().writeTo(mFiler);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return routerParamMap;
    }


    /**
     * 获取RouterParamParser
     *
     * @param routerParamsMap RouterParamsMap
     * @param element 类元素
     */
    private RouterParamParser getOrCreateRouterParamClass(
            Map<TypeElement, RouterParamParser> routerParamsMap,
            TypeElement element) {
        if (routerParamsMap == null || element == null) {
            mAssertUtil.error("routerParamsMap or element not valid");
            return null;
        }

        if (!routerParamsMap.containsKey(element)) {
            String classPackage = getPackageName(element);
            String targetClass = element.getSimpleName().toString();
            RouterParamParser parser = new RouterParamParser(classPackage, targetClass);
            routerParamsMap.put(element, parser);
        }

        return routerParamsMap.get(element);
    }

    /**
     * 获取package名
     *
     * @return string 包名
     */
    private String getPackageName(TypeElement element) {
        return mElementUtils.getPackageOf(element).getQualifiedName().toString();
    }

    /**
     * 检查注解是否符合要求
     *
     * @param element 注解元素
     * @param typeElement 类元素
     * @return boolean
     */
    private boolean isInaccessibleViaGeneratedCode(Element element,
                                                   TypeElement typeElement) {
        boolean hasError = false;

        // 校验字段的属性.
        Set<Modifier> modifiers = element.getModifiers();
        // 不能是私有或者是静态的
        if (modifiers.contains(PRIVATE) || modifiers.contains(STATIC)) {
            error("%s must not be private or static.(%s)",
                    element.getSimpleName(), typeElement.getQualifiedName());
            hasError = true;
        }

        // 校验是否Class.
        if (typeElement.getKind() != CLASS) {
            error("%s may only be contained in classes.(%s)",
                    element.getSimpleName(), typeElement.getQualifiedName());
            hasError = true;
        }

        // 校验Class是否私有.
        if (typeElement.getModifiers().contains(PRIVATE)) {
            error("%s may not be contained in private classes. (%s)",
                    element.getSimpleName(), typeElement.getQualifiedName());
            hasError = true;
        }

        return hasError;
    }

    /**
     * 打印错误日志
     *
     * @param message 错误日志
     * @param args 参数信息
     */
    private void error(String message, Object... args) {
        if (args.length > 0) {
            message = String.format(message, args);
        }
        mAssertUtil.error(message);
    }

}
