package club.fromfactory.routercompile;

import club.fromfactory.routerannotaions.Router;
import club.fromfactory.routerannotaions.RouterParam;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeSpec;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.annotation.processing.Filer;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;

/**
 * ��?????
 *
 * @author nichenjian
 * @date 2018/10/14
 */
class RouterProcessor {

    private final RoundEnvironment mEnv;
    private final Filer mFiler;
    private final Map<TypeElement, RouterParamParser> mParamParserMap;

    RouterProcessor(RoundEnvironment env,
            Filer filer,
            Map<TypeElement, RouterParamParser> map) {
        mEnv = env;
        mFiler = filer;
        mParamParserMap = map;
    }

    /**
     * create routerMap file
     */
    private JavaFile brewJava() {
        TypeSpec.Builder routerHandler = TypeSpec.classBuilder(Constants.ROUTER_MAP)
                .addSuperinterface(Constants.ClassNameList.IROUTER_MAP_INTERFACE)
                .addModifiers(Modifier.PUBLIC, Modifier.FINAL);

        routerHandler.addMethod(createInitMethod());

        return JavaFile
                .builder(Constants.ROUTER_BASE_PACKAGE, routerHandler.build())
                .addFileComment("Generated code from Router. Do not modify!")
                .build();
    }

    private MethodSpec createInitMethod() {
        MethodSpec.Builder builder = MethodSpec.methodBuilder("init")
                .addAnnotation(Override.class)
                .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
                .returns(HashMap.class);

        builder.addStatement("HashMap <String, $T> routerMap = new HashMap<>()",
                Constants.ClassNameList.PAGE_INFO_CLASS);
        builder.addStatement("PageInfo pageInfo = null");
        builder.addCode("\n");

        Set<? extends Element> routers = mEnv.getElementsAnnotatedWith(Router.class);
        for (Element element : routers) {
            Router router = element.getAnnotation(Router.class);
            try {
                String[] values = router.value();
                for (String value : values) {
                    if (value != null) {
                        ClassName className = ClassName.get((TypeElement) element);
                        builder.addStatement("pageInfo = new PageInfo()");
                        builder.addStatement("pageInfo.setActivityClass($T.class)", className);
                        for (Map.Entry<TypeElement, RouterParamParser> entry : mParamParserMap
                                .entrySet()) {
                            if (entry != null
                                    && ClassName.get(entry.getKey()).compareTo(className) == 0) {
                                Map<RouterParam, Element> routerParamElementMap = entry.getValue()
                                        .getRouterParamsMap();
                                for (Map.Entry<RouterParam, Element> routerParamEntry : routerParamElementMap
                                        .entrySet()) {
                                    String[] routerParamValue = routerParamEntry.getKey().value();
                                    for (String routerParam : routerParamValue) {
                                        String typeName = routerParamEntry.getValue().asType()
                                                .toString();
                                        builder.addStatement("pageInfo.addParam($S, $T.class)",
                                                routerParam, getValidClassName(typeName));
                                    }
                                }
                                break;
                            }
                        }
                        builder.addStatement("routerMap.put($S, pageInfo)", value);
                        builder.addCode("\n");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        builder.addStatement("return routerMap");
        return builder.build();
    }

    void parse() {
        try {
            brewJava().writeTo(mFiler);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Class getValidClassName(String type) {
        Class className = null;
        switch (type) {
            case "int":
                className = Integer.class;
                break;
            case "java.lang.Integer":
                className = Integer.class;
                break;
            case "long":
                className = Long.class;
                break;
            case "java.lang.Long":
                className = Long.class;
                break;
            case "float":
                className = Float.class;
                break;
            case "java.lang.Float":
                className = Float.class;
                break;
            case "boolean":
                className = Boolean.class;
                break;
            case "java.lang.Boolean":
                className = Boolean.class;
                break;
            case "short":
                className = Short.class;
                break;
            case "java.lang.Short":
                className = Short.class;
                break;
            case "double":
                className = Double.class;
                break;
            case "java.lang.Double":
                className = Double.class;
                break;
            case "byte":
                className = Byte.class;
                break;
            case "java.lang.Byte":
                className = Byte.class;
                break;
            case "char":
                className = Character.class;
                break;
            case "java.lang.Character":
                className = Character.class;
                break;
            case "java.lang.String":
                className = String.class;
                break;
            default:
                className = Object.class;
                break;
        }

        return className;
    }
}
