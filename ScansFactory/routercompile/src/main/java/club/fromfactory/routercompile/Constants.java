package club.fromfactory.routercompile;

import com.squareup.javapoet.ClassName;

/**
 * Routerģ�鳣��
 *
 * @author nichenjian
 * @date 2018/10/14
 */
class Constants {
    /**
     * router map
     */
    static final String ROUTER_MAP = "RouterMap";

    /**
     * router suffix
     */
    static final String ROUTER_SUFFIX = "$$Router";

    /**
     * router base package
     */
    static final String ROUTER_BASE_PACKAGE = "club.fromfactory.router";

    /**
     * router base library package
     */
    static final String ROUTER_BASE_LIBRARY_PACKAGE = "club.fromfactory.baselibrary.router";

    /**
     * class name list
     */
    static class ClassNameList {
        static final ClassName IROUTER_INTERFACE = ClassName.get(ROUTER_BASE_PACKAGE, "IRouter");
        static final ClassName IROUTER_MAP_INTERFACE = ClassName.get(ROUTER_BASE_PACKAGE, "IRouterMap");
        static final ClassName PAGE_INFO_CLASS = ClassName.get(ROUTER_BASE_PACKAGE, "PageInfo");
        static final ClassName ROUTER_MANAGER_CLASS = ClassName.get(ROUTER_BASE_LIBRARY_PACKAGE, "RouterManager");
        static final ClassName BUNDLE_CLASS = ClassName.get("android.os", "Bundle");
        static final ClassName ACTIVITY_CLASS = ClassName.get("android.app", "Activity");
    }
}
