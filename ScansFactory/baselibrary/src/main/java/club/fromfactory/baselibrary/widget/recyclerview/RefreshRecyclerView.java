package club.fromfactory.baselibrary.widget.recyclerview;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import club.fromfactory.baselibrary.R;

/**
 * Created by lxm on 2017/9/6..
 */
public class RefreshRecyclerView extends FrameLayout {

    private final String TAG = "RefreshRecyclerView";
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;
    private RecyclerAdapter mAdapter;
    private boolean loadMoreAble;

    private int lastVisibleItem;

    public RefreshRecyclerView(Context context) {
        this(context, null);
    }

    public RefreshRecyclerView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RefreshRecyclerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        View view = inflate(context, R.layout.view_refresh_recycler, this);
        mRecyclerView = view.findViewById(R.id.view_refresh_recycler_recycler_view);
        mSwipeRefreshLayout = view.findViewById(R.id.view_refresh_recycler_refresh_layout);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable
                .RefreshRecyclerView);
        boolean refreshAble = typedArray.getBoolean(R.styleable.RefreshRecyclerView_refresh_able,
                true);
        loadMoreAble = typedArray.getBoolean(R.styleable.RefreshRecyclerView_load_more_able, true);
        if (!refreshAble) {
            mSwipeRefreshLayout.setEnabled(false);
        }

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                Log.i(TAG, "lxm onScrollStateChanged1:" + lastVisibleItem + "  " + mAdapter
                        .getItemCount());
                Log.i(TAG, "lxm onScrollStateChanged1:" + newState + " " + loadMoreAble);

                //当newState == RecyclerView.SCROLL_STATE_IDLE
                //lastVisibleItem == 1时表示下拉
                if (newState == RecyclerView.SCROLL_STATE_IDLE && lastVisibleItem + 1 == mAdapter
                        .getItemCount() && loadMoreAble) // TODO: 2017/7/27上拉加载更多。
                {
//                    mAdapter.loadMore();
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager mLayoutManager = (LinearLayoutManager) recyclerView
                        .getLayoutManager();
                lastVisibleItem = mLayoutManager.findLastVisibleItemPosition();
                Log.i(TAG, "lxm onScrollStateChanged2:" + lastVisibleItem + "  " + mAdapter
                        .getItemCount());
            }
        });
    }

    public void setAdapter(RecyclerAdapter adapter) {
        mRecyclerView.setAdapter(adapter);
        mAdapter = adapter;
        mAdapter.loadMoreAble = loadMoreAble;
    }

    public void setLayoutManager(RecyclerView.LayoutManager layoutManager) {
        mRecyclerView.setLayoutManager(layoutManager);
    }

    public void setRefreshAction(final RecyclerAction action) {
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mAdapter.openLoadMore();
                action.onAction();
            }
        });
    }


    public void setLoadMoreEnable(boolean isEnable) {
        loadMoreAble = isEnable;
    }

    public void setLoadMoreAction(final RecyclerAction action) {

        if (mAdapter.isShowNoMore || !loadMoreAble) {
            return;
        }
        mAdapter.loadMoreAble = true;
        mAdapter.setLoadMoreAction(action);
    }

    public void showNoMore(String noMoreStr) {
        mAdapter.showNoMore(noMoreStr);
    }

    public void setItemSpace(int left, int top, int right, int bottom) {
        mRecyclerView.addItemDecoration(new BaseSpaceItemDecoration(left, top, right, bottom));
    }

    public void addItemDecoration(RecyclerView.ItemDecoration itemDecoration) {
        mRecyclerView.addItemDecoration(itemDecoration);
    }


    public RecyclerView getRecyclerView() {
        return mRecyclerView;
    }

    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return mSwipeRefreshLayout;
    }

    public View getNoMoreView() {
        return mAdapter.mNoMoreView;
    }

    public void setRefreshPosition(int start, int end) {
        mSwipeRefreshLayout.setProgressViewOffset(false, start, end);
    }

    public void setSwipeRefreshColorsFromRes(@ColorRes int... colors) {
        mSwipeRefreshLayout.setColorSchemeResources(colors);
    }

    /**
     * 8位16进制数 ARGB
     */
    public void setSwipeRefreshColors(@ColorInt int... colors) {
        mSwipeRefreshLayout.setColorSchemeColors(colors);
    }

    public void setRefreshEnable(boolean isEnable) {
        mSwipeRefreshLayout.setEnabled(false);
    }

    public void showSwipeRefresh() {
        mSwipeRefreshLayout.setRefreshing(true);
    }

    public void dismissSwipeRefresh() {
        mSwipeRefreshLayout.setRefreshing(false);
    }
}
