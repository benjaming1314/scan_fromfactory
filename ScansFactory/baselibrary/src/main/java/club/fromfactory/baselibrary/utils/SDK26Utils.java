package club.fromfactory.baselibrary.utils;

import android.content.Intent;
import android.os.Build;

import java.util.Locale;

import club.fromfactory.baselibrary.BaseApplication;

/**
 * @author lxm
 * @date 2018/7/9/009
 */
public class SDK26Utils {


    public static void startService(Intent intent) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            BaseApplication.instance.startForegroundService(intent);
        } else {
            BaseApplication.instance.startService(intent);
        }
    }

    public static Locale getLocalInstance() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            return Locale.getDefault(Locale.Category.DISPLAY);
        } else {
            return Locale.getDefault();
        }

    }
}
