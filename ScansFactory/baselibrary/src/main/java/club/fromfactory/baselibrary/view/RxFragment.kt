package club.fromfactory.baselibrary.view

import android.content.Context
import android.os.Bundle
import android.support.annotation.CheckResult
import android.support.v4.app.Fragment
import android.view.View
import com.trello.rxlifecycle2.LifecycleProvider
import com.trello.rxlifecycle2.LifecycleTransformer
import com.trello.rxlifecycle2.RxLifecycle
import com.trello.rxlifecycle2.android.FragmentEvent
import com.trello.rxlifecycle2.android.RxLifecycleAndroid
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject

/**
 * 响应式Fragment，将Fragment各生命周期转换为Transformer
 *
 * @author Jellybean
 * @date 2018/5/30
 */
abstract class RxFragment : Fragment(), LifecycleProvider<FragmentEvent> {

    /**
     * 生命周期分发
     */
    private val lifecycleBehaviorSubject = BehaviorSubject.create<FragmentEvent>()

    @CheckResult
    override fun lifecycle(): Observable<FragmentEvent> {
        return lifecycleBehaviorSubject.hide()
    }

    @CheckResult
    override fun <T : Any?> bindUntilEvent(event: FragmentEvent): LifecycleTransformer<T> {
        return RxLifecycle.bindUntilEvent(lifecycleBehaviorSubject, event)

    }

    @CheckResult
    override fun <T : Any?> bindToLifecycle(): LifecycleTransformer<T> {
        return RxLifecycleAndroid.bindFragment(lifecycleBehaviorSubject)
    }

    override fun onAttach(context: Context?) {
        lifecycleBehaviorSubject.onNext(FragmentEvent.ATTACH)
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        lifecycleBehaviorSubject.onNext(FragmentEvent.CREATE)
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        lifecycleBehaviorSubject.onNext(FragmentEvent.CREATE_VIEW)
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onStart() {
        lifecycleBehaviorSubject.onNext(FragmentEvent.START)
        super.onStart()
    }

    override fun onResume() {
        lifecycleBehaviorSubject.onNext(FragmentEvent.RESUME)
        super.onResume()
    }

    override fun onPause() {
        lifecycleBehaviorSubject.onNext(FragmentEvent.PAUSE)
        super.onPause()
    }

    override fun onStop() {
        lifecycleBehaviorSubject.onNext(FragmentEvent.STOP)
        super.onStop()
    }

    override fun onDestroyView() {
        lifecycleBehaviorSubject.onNext(FragmentEvent.DESTROY_VIEW)
        super.onDestroyView()
    }

    override fun onDestroy() {
        lifecycleBehaviorSubject.onNext(FragmentEvent.DESTROY)
        super.onDestroy()
    }

    override fun onDetach() {
        lifecycleBehaviorSubject.onNext(FragmentEvent.DETACH)
        super.onDetach()
    }
}