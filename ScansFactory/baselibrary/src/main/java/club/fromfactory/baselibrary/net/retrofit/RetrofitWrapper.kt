package club.fromfactory.baselibrary.net.retrofit

import club.fromfactory.baselibrary.net.retrofit.cache.RxCache
import club.fromfactory.baselibrary.net.retrofit.cache.core.INVALID_TIME
import club.fromfactory.baselibrary.net.retrofit.cache.model.CacheMode
import club.fromfactory.baselibrary.net.retrofit.cache.model.Reply
import club.fromfactory.baselibrary.net.retrofit.cache.strategy.StrategyManager
import io.reactivex.Observable
import okio.ByteString
import java.lang.reflect.Type
import java.nio.ByteBuffer
import java.util.concurrent.TimeUnit

/**
 * Retrofit 请求封装
 *
 * @author nichenjian
 * @date 2018/6/21
 */
class RetrofitWrapper<T>(builder: RetrofitWrapper.Builder<T>) {
    /**
     * 缓存的模式
     */
    private val cacheMode: CacheMode
    /**
     * 缓存的key
     */
    private val cacheKey: String
    /**
     * 请求的observable
     */
    private val source: Observable<T>?
    /**
     * 缓存有效时长
     */
    private val cacheTime: Long
    /**
     * 类型
     */
    private val type: Type
    /**
     * 缓存的时间类型
     */
    private val timeUnit: TimeUnit
    /**
     * 缓存管理
     */
    private val rxCache: RxCache


    init {
        cacheMode = builder.getCacheMode()
        cacheKey = builder.getCacheKey()
        source = builder.getObservable()
        cacheTime = builder.getCacheTime()
        timeUnit = builder.getTimeUnit()
        type = builder.getType()
        val builder = RxCache.Builder().setCacheTime(cacheTime).setTimeUnit(timeUnit)
        rxCache = builder.build()
    }

    private fun load(): Observable<Reply<T>> {
        val strategy = StrategyManager.map[cacheMode]!!.newInstance()
        val realCacheKey = ByteString.of(ByteBuffer.wrap(cacheKey.toByteArray())).md5().hex()
        return strategy.execute(rxCache, realCacheKey, cacheTime, timeUnit, source, type)
    }

    class Builder<T> {
        private var cacheMode: CacheMode = CacheMode.NO_CACHE
        private var cacheKey: String = ""
        private var observable: Observable<T>? = null
        private var cacheTime: Long = INVALID_TIME
        private var timeUnit: TimeUnit = TimeUnit.SECONDS
        private var type: Type? = null

        fun setCacheMode(cacheMode: CacheMode): Builder<T> {
            this.cacheMode = cacheMode
            return this@Builder
        }


        fun setCacheKey(cacheKey: String): Builder<T> {
            this.cacheKey = cacheKey
            return this@Builder
        }

        fun setObservable(observable: Observable<T>?): Builder<T> {
            this.observable = observable
            return this@Builder
        }

        fun setCacheTime(cacheTime: Long): Builder<T> {
            this.cacheTime = cacheTime
            return this@Builder
        }

        fun setTimeUnit(timeUnit: TimeUnit): Builder<T> {
            this.timeUnit = timeUnit
            return this@Builder
        }

        fun setType(type: Type): Builder<T> {
            this.type = type
            return this@Builder
        }

        fun getCacheMode(): CacheMode {
            return cacheMode
        }

        fun getCacheKey(): String {
            return this.cacheKey
        }

        fun getObservable(): Observable<T>? {
            return this.observable
        }

        fun getCacheTime(): Long {
            return this.cacheTime
        }

        fun getTimeUnit(): TimeUnit {
            return this.timeUnit
        }

        fun getType(): Type {
            return type!!
        }

        fun build(): Observable<Reply<T>> {
            if (type == null) {
                throw IllegalStateException("type must not be null")
            }
            return RetrofitWrapper(this).load()
        }
    }
}
