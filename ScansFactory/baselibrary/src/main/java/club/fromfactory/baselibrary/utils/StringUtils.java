package club.fromfactory.baselibrary.utils;

import android.text.TextUtils;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by lxm on 2017/8/4.
 */

public class StringUtils {

    public static final String TRACKING_NO = "^[0-9a-zA-Z]+$";  // 字符，支持字母、数字 //物流单号

    /**
     * 正则表达式:验证邮箱
     */
    private static final String REGEX_EMAIL = "^[\\w\\.\\-]+@([\\w\\-]+\\.)+[\\w\\-]+$";

    public static boolean isEmail(String email) {
        return match(REGEX_EMAIL, email) ;
    }

    public static boolean isNotBlank(String str) {
        return str != null && str.length() > 0 && (!str.trim().equalsIgnoreCase("null")) ;
    }

    public static boolean isNull(String str) {
        return str == null || str.length() == 0 || str.trim().equalsIgnoreCase("null");
    }

    public static int[] stringArrToIntArr(String[] strArr) {
        int[] intArr = new int[strArr.length];
        for (int i = 0; i < strArr.length; i++) {
            intArr[i] = Integer.parseInt(strArr[i]);
        }
        return intArr;
    }

    public static ArrayList<String> stringToStrArr(String str) {
        ArrayList<String> list = new ArrayList();
        String[] strArr = str.split(",");
        for (int i = 0; i < strArr.length; i++) {
            list.add(strArr[i]);
        }
        return list;
    }

    public static String removeStr(String removeStr, String oldStr) {
        Log.d("oldstr", oldStr + "-------" + removeStr);
        String[] mArr = oldStr.split(",");
        ArrayList<String> mList = new ArrayList();
        for (int i = 0; i < mArr.length; i++) {
            mList.add(mArr[i]);
        }
        for (int i = 0; i < mList.size(); i++) {
            if (mList.get(i).equals(removeStr)) {
                mList.remove(i);
            }
        }
        StringBuffer newStr = new StringBuffer();
        for (int i = 0; i < mList.size(); i++) {
            if (!(i == mList.size() - 1)) {
                newStr.append(mList.get(i) + ",");
            } else {
                newStr.append(mList.get(i));
            }
        }
        Log.d("oldstr", newStr.toString() + "---");
        return newStr.toString();
    }



    public static ArrayList<String> stringToList(String str, boolean isReverse) {
        ArrayList<String> list = new ArrayList();
        if (TextUtils.isEmpty(str)) {
            return list;
        }
        String[] strArr = str.split(",");
        if (isReverse) {
            for (int i = strArr.length - 1; i >= 0; i--) {
                list.add(strArr[i]);
            }
        } else {
            for (int i = 0; i < strArr.length; i++) {
                list.add(strArr[i]);
            }
        }

        return list;
    }

    public static String listToString(ArrayList<String> stringList) {

        StringBuffer newStr = new StringBuffer();
        for (int i = 0; i < stringList.size(); i++) {
            newStr.append(stringList.get(i));
            if (i != stringList.size() - 1) {
                newStr.append(",");
            }
        }
        return newStr.toString();
    }

    /**
     * 获得字符串的长度，中文算2个，英文算1个
     *
     * @param text
     * @return
     */
    public static int getStringLength(String text) {
        int valueLength = 0;
        String chinese = "[\u4e00-\u9fa5]";
        for (int i = 0; i < text.length(); i++) {
            String temp = text.substring(i, i + 1);
            if (temp.matches(chinese)) {
                valueLength += 2;
            } else {
                valueLength += 1;
            }
        }
        return valueLength;
    }

    public static String stringArrayToString(String[] str) {
        if (str == null) return null;
        StringBuffer stringBuffer = new StringBuffer();

        for (int i = 0; i < str.length; i++) {
            stringBuffer.append(str[i]);
            if (i != str.length - 1) stringBuffer.append(",");
        }

        return stringBuffer.toString();
    }

    public static boolean isTrackingNo(String str) {
        if (isNull(str)) return false ;
        return match(TRACKING_NO,str) ;
    }

    public static boolean match(String matcher, String str) {
        Pattern p = Pattern.compile(matcher);
        Matcher m = p.matcher(str);
        return m.find();
    }


    public static String matchStrString(String matcher, String str) {
        StringBuilder stringBuilder = new StringBuilder();
        Pattern p = Pattern.compile(matcher);
        Matcher m = p.matcher(str);

        while(m.find()) {
            Log.i("lxm matchStr:" ,"lxm matchStr:"+m.group(0) +" "+ m.group(1));
            stringBuilder.append(StringUtils.isNotBlank(m.group(1)) ? m.group(1) :m.group(0));
        }

        return stringBuilder.toString();
    }


    public static List<String> matchStrList(String matcher, String str) {
        List<String> stringList = new ArrayList<>();
        Pattern p = Pattern.compile(matcher);
        Matcher m = p.matcher(str);
        while (m.find()){
            Log.i("lxm matchStr:" ,"lxm matchStrList:"+m.group(0) +" "+ m.group(1));
            stringList.add(StringUtils.isNotBlank(m.group(1)) ? m.group(1) :m.group(0));
        }
        return  stringList ;
    }
    public static String matchReplace(String matcher, String str) {
        Pattern p = Pattern.compile(matcher);
        Matcher m = p.matcher(str);
        if (m.find()) {
            return m.replaceAll("");
        } else {
            return str;
        }
    }
    public static String matchReplace(String matcher, String str,String replaceStr) {
        Pattern p = Pattern.compile(matcher);
        Matcher m = p.matcher(str);
        if (m.find()) {
            return m.replaceAll(replaceStr);
        } else {
            return str;
        }
    }

    public static int toInt(String value) {
        try {
            if (isNotBlank(value)) {
                return Integer.parseInt(value);
            }
            return 0;
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public static float toFloat(String value) {
        try {
            if (isNotBlank(value)) {
                return Float.parseFloat(value);
            }
            return 0.0f;
        } catch (NumberFormatException e) {
            return 0.00f;
        }
    }
    public static long toLong(String value) {
        try {
            if (isNotBlank(value)) {
                return Long.parseLong(value);
            }
            return 0;
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public static double toDouble(String value) {
        try {
            if (isNotBlank(value)) {
                return Double.parseDouble(value);
            }
            return 0.0f;
        } catch (NumberFormatException e) {
            return 0.00f;
        }
    }
}
