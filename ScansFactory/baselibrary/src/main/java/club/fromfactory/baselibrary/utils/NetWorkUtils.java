package club.fromfactory.baselibrary.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.telephony.TelephonyManager;

import com.crashlytics.android.Crashlytics;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * 用户网络相关信息工具包
 *
 * @author zhoulei on 2017/6/7.
 */

public class NetWorkUtils {

    public static final String NETWORK_TYPE_DISCONNECT = "Disconnect";
    private static final String NETWORK_TYPE_WIFI = "Wifi";
    private static final String NETWORK_TYPE_2G = "2G";
    private static final String NETWORK_TYPE_3G = "3G";
    private static final String NETWORK_TYPE_4G = "4G";
    private static final String NETWORK_TYPE_UNKNOWN = "Unknown";

    /**
     * 判断当前是否有网络状态权限
     *
     * @param context 上下文
     * @return 是否有网络权限
     */
    private static boolean hasNetworkStatePermission(Context context) {
        // 针对 6.0 以上的手机判断是否有获取手机网络状态的权限
        // fix https://fabric.io/jiayunshuju/android/apps/club
        // .fromfactory/issues/5b12ae4f6007d59fcdb20248
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            return context.checkSelfPermission(android.Manifest.permission.ACCESS_NETWORK_STATE)
                    == android.content.pm.PackageManager.PERMISSION_GRANTED;
        }

        return true;
    }

    /**
     * Get network type
     */
    public static String getNetWorkType(Context context) {
        if (!hasNetworkStatePermission(context)) {
            return NETWORK_TYPE_UNKNOWN;
        }

        // fix crash https://fabric.io/jiayunshuju/android/apps/club
        // .fromfactory/issues/5b12ae4f6007d59fcdb20248
        try {
            ConnectivityManager manager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo;

            String type = NETWORK_TYPE_UNKNOWN;
            if (manager == null || (networkInfo = manager.getActiveNetworkInfo()) == null) {
                return type;
            }

            if (networkInfo.isConnected()) {
                String typeName = networkInfo.getTypeName();
                if ("WIFI".equalsIgnoreCase(typeName)) {
                    type = NETWORK_TYPE_WIFI;
                } else if ("MOBILE".equalsIgnoreCase(typeName)) {
                    int networkType = networkInfo.getSubtype();
                    switch (networkType) {
                        case TelephonyManager.NETWORK_TYPE_GPRS:
                        case TelephonyManager.NETWORK_TYPE_EDGE:
                        case TelephonyManager.NETWORK_TYPE_CDMA:
                        case TelephonyManager.NETWORK_TYPE_1xRTT:
                        case TelephonyManager.NETWORK_TYPE_IDEN:
                            type = NETWORK_TYPE_2G;
                            break;
                        case TelephonyManager.NETWORK_TYPE_UMTS:
                        case TelephonyManager.NETWORK_TYPE_EVDO_0:
                        case TelephonyManager.NETWORK_TYPE_EVDO_A:
                        case TelephonyManager.NETWORK_TYPE_HSDPA:
                        case TelephonyManager.NETWORK_TYPE_HSUPA:
                        case TelephonyManager.NETWORK_TYPE_HSPA:
                        case TelephonyManager.NETWORK_TYPE_EVDO_B:
                        case TelephonyManager.NETWORK_TYPE_EHRPD:
                        case TelephonyManager.NETWORK_TYPE_HSPAP:
                            type = NETWORK_TYPE_3G;
                            break;
                        case TelephonyManager.NETWORK_TYPE_LTE:
                            type = NETWORK_TYPE_4G;
                            break;
                        default:
                            type = NETWORK_TYPE_UNKNOWN;
                            break;
                    }
                } else {
                    type = NETWORK_TYPE_UNKNOWN;
                }
            }

            return type;
        } catch (Exception e) {
            Crashlytics.logException(e);
        }

        return NETWORK_TYPE_UNKNOWN;
    }

    /**
     * 判断当前网络是否可用
     *
     * @return 如果当前网络可用return true;如果当前网络不可用 return false
     */
    public static boolean isNetworkAvailable(Context context) {
        if (hasNetworkStatePermission(context)) {
            ConnectivityManager manager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo info = manager.getActiveNetworkInfo();
            return (info != null && info.isConnected());
        }

        return true;
    }

    /**
     * 判断当前是否wifi网络
     */
    public static boolean isWifi(Context context) {
        return getNetWorkType(context) == NETWORK_TYPE_WIFI;
    }

    /**
     * ping指定url
     *
     * @param url url host
     * @return ping信息
     */
    public static String ping(String url) {
        if (url != null && url.length() > 0) {
            return null;
        }

        String result = "";
        try {
            Runtime runtime = Runtime.getRuntime();
            Process process = runtime.exec("ping -c 4 " + url);
            BufferedReader bufferedreader = new BufferedReader(
                    new InputStreamReader(process.getInputStream()));
            String line = "";
            StringBuilder sb = new StringBuilder(line);
            while ((line = bufferedreader.readLine()) != null) {
                sb.append(line);
                sb.append('\n');
            }
            result = sb.toString();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return result;
    }


    /**
     * 判断当前url是否是club factory域
     *
     * @param url 要判断的url
     */
    public static boolean isFactoryHost(String url) {
        if (url == null || url.length() == 0) {
            return false;
        }

        try {
            Uri uri = Uri.parse(url);
            return uri.getHost().contains("fromfactory.club");
        } catch (Exception e) {
            return false;
        }
    }

}
