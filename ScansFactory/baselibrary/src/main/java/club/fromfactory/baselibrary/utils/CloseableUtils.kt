package club.fromfactory.baselibrary.utils

import java.io.Closeable
import java.io.IOException

/**
 * closeable 辅助类
 *
 * @author nichenjian
 * @date 2018/6/26
 */
object CloseableUtils {
    @JvmStatic
    fun close(closeable: Closeable?) {
        if (closeable != null) {
            try {
                closeable.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }
}
