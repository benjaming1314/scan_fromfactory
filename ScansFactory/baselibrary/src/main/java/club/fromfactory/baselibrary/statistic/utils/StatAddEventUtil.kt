package club.fromfactory.baselibrary.statistic.utils

import android.content.Context
import android.support.v4.app.NotificationManagerCompat
import android.text.TextUtils
import android.view.View
import club.fromfactory.baselibrary.R
import club.fromfactory.baselibrary.statistic.StatEventManager
import club.fromfactory.baselibrary.statistic.constants.*
import club.fromfactory.baselibrary.statistic.exceptions.InvalidStatParamException
import club.fromfactory.baselibrary.utils.throwException
import club.fromfactory.baselibrary.view.IBaseView
import com.crashlytics.android.answers.Answers
import com.crashlytics.android.answers.CustomEvent
import java.util.*

/**
 * 添加统计的辅助方法
 *
 * @author nichenjian
 * @date 2018/7/31
 */
/**
 * 不合法的模块id
 */
const val INVALID_MODULE_ID = -1

object StatAddEventUtil {
    /**
     * 添加页面曝光的日志
     *
     * @param baseView 页面
     */
    @JvmStatic
    fun addPageImpressionEvent(baseView: IBaseView) {
        val pageId = baseView.traceInfo?.pageId ?: StatUtil.getPageId(baseView)

        if (pageId > 0) {
            val params = Hashtable<String, Any>()
            params[MODULE_ID] = "1.$pageId"
            params[EVENT_TYPE] = StatEventType.IMPRESSION
            try {
                // fix https://fabric.io/jiayunshuju/android/apps/club.fromfactory/issues/5c91d266f8b88c2963c32a5a
                StatEventManager.addStatEvent(params, baseView)
            } catch (e: Throwable) {
                e.printStackTrace()
            }
        }
    }

    /**
     * 添加页面停留时长统计
     *
     * @param baseView 页面
     * @param duration 停留时长
     */
    @JvmStatic
    fun addPageStayTimeEvent(baseView: IBaseView) {
        baseView.traceInfo?.showTime ?: return
        val pageId = baseView.traceInfo?.pageId ?: StatUtil.getPageId(baseView)
        val duration = if (baseView.traceInfo!!.showTime!! > 0) (System.currentTimeMillis() - baseView.traceInfo!!.showTime!!) else 0
        if (pageId > 0 && duration > 0) {
            val params = Hashtable<String, Any>()
            params[MODULE_ID] = "1.$pageId"
            params[EVENT_TYPE] = StatEventType.STAY
            params[DURATION] = duration
            StatEventManager.addStatEvent(params, baseView)
        }
    }

    /**
     * 添加外链打开统计
     *
     * @param source 来源
     * @param url 打开的链接
     */
    @JvmStatic
    fun addLinkEvent(source: String?, url: String?, pushId: String?) {
        if (source == null || url == null) {
            return
        }

        val params = Hashtable<String, Any>()
        params[EVENT_TYPE] = StatEventType.DEEPLINK
        params[MODULE_ID] = "1"
        params[StatCommonConstants.URL] = url
        if (pushId != null) {
            params[PUSH_ID] = pushId
        }
        params[LINK_TYPE] = source
        StatEventManager.addStatEvent(params)
    }


    /**
     * 添加统计
     *
     * @param view 需要添加统计的view
     * @param moduleId 模块id
     * @param eventType 事件类型
     */
    @JvmStatic
    @JvmOverloads
    fun addEvent(view: View? = null, componentId: Int, baseView: IBaseView,
                 params: Hashtable<String, Any>? = Hashtable(), moduleId: Int = INVALID_MODULE_ID,
                 eventType: String = StatEventType.CLICK, isSubComponent: Boolean = false) {
        // mid 获取失败 直接吞掉日志
        val mid = getMid(view, componentId, moduleId, isSubComponent, baseView) ?: return
        val table = params ?: Hashtable()
        table[EVENT_TYPE] = eventType
        table[MODULE_ID] = mid
        StatEventManager.addStatEvent(table, baseView)
    }

    /**
     * 添加统计
     *
     * @param view 需要添加统计的view
     * @param moduleId 模块id
     * @param eventType 事件类型
     */
    @JvmStatic
    fun addEvent(componentId: Int, baseView: IBaseView,
                 params: Hashtable<String, Any>? = Hashtable(), moduleId: Int = INVALID_MODULE_ID,
                 eventType: String = StatEventType.CLICK, isSubComponent: Boolean = false) {
        addEvent(null, componentId, baseView, params, moduleId, eventType, isSubComponent)
    }

    /**
     * 添加统计
     *
     * @param view 需要添加统计的view
     * @param moduleId 模块id
     * @param eventType 事件类型
     */
    @JvmStatic
    fun addEvent(componentId: Int, baseView: IBaseView,
                 params: Hashtable<String, Any>? = Hashtable(), moduleId: Int = INVALID_MODULE_ID) {
        addEvent(null, componentId, baseView, params, moduleId, StatEventType.CLICK, false)
    }

    /**
     * 获取mid（1.1.1.1.1）
     *
     * @param view 需要添加统计的view
     * @param id 组件id，isSubComponent true时传的是 subComponentId, 否则是 componentId
     * @param isSubComponent 是否子组件
     */
    private fun getMid(view: View?, componentId: Int, moduleId: Int,
                       isSubComponent: Boolean = false, baseView: IBaseView): String? {
        val subComponentId = if (isSubComponent) componentId else 0
        val finalModuleId: Int = if (moduleId == INVALID_MODULE_ID && view != null)
            getId(view, R.id.module_id) else moduleId
        val pageId: Int = baseView.pageId
        val finalComponentId = if (isSubComponent && view != null) {
            getId(view, R.id.component_id)
        } else {
            componentId
        }
        if (pageId == 0 || finalModuleId == 0 || finalComponentId == 0) {
            throwException(InvalidStatParamException("pageId or moduleId or componentId must not be 0," +
                    " pageId $pageId moduleId $finalModuleId componentId $finalComponentId"))
            return null
        }

        return "1.$pageId.$finalModuleId.$finalComponentId${if (subComponentId > 0) ".$subComponentId" else ""}"
    }


    /**
     * 获取对应的id
     *
     * @param view 需要添加统计的view
     * @param type 需要获取的类型，支持 `R.string.pageId` `R.string.moduleId` `R.string.componentId` 三种
     * @return int 获取的id
     */
    @JvmStatic
    fun getId(v: View, type: Int): Int {
        var tag = v.getTag(type)
        var parent = v.parent
        while (tag == null && parent is View) {
            tag = parent.getTag(type)
            parent = (parent as View).parent
        }
        return tag as Int? ?: 0
    }

    /**
     * 上报系统通知的状态
     */
    @JvmStatic
    fun reportNotificationStatus(context: Context) {
        val isEnabled = NotificationManagerCompat.from(context).areNotificationsEnabled()
        val params = Hashtable<String, Any>()
        params[EVENT_TYPE] = StatEventType.NOTIFICATION_SWITCH
        params[MODULE_ID] = "1"
        params[STATE] = if (isEnabled) 1 else 0
        StatEventManager.addStatEvent(params, null)
    }


    /**
     * 上报RN状态
     */
    @JvmStatic
    fun reportRnStat(state: String, rnVersion: String, appVersion: String, env: String) {

        // 1. 自身服务器数据埋点
        val params = Hashtable<String, Any>()
        params[EVENT_TYPE] = StatEventType.RN_STATUS
        params[MODULE_ID] = "1"
        params[STATE] = state
        params[INFO] = rnVersion
        StatEventManager.addStatEvent(params, null)


        // 2. Fabric服务器埋点
        val eventName = when (state) {
            "launch" -> "RN-Launch-$appVersion"
            "update_start" -> "RN-All-Update-Start-$appVersion"
            "update_finish" -> "RN-All-Update-Finish-$appVersion"
            else -> null
        }
        if (!TextUtils.isEmpty(eventName)) {
            Answers.getInstance().logCustom(CustomEvent(state).putCustomAttribute("version", "$env-v$rnVersion"))
        }
    }

    /**
     * 上报RN状态
     */
    @JvmStatic
    fun reportBundleDownload(downloadState: String, componentName: String, appVersion: String, rnVersion: String) {
        val params = Hashtable<String, Any>()
        params[EVENT_TYPE] = StatEventType.RN_STATUS
        params[MODULE_ID] = "1"
        params[STATE] = "rn_download"
        params[INFO] = "rnVersion=$rnVersion,name=$componentName,state=$downloadState"
        StatEventManager.addStatEvent(params, null)


        // fabric统计
        var eventName: String = if ("downloadStart" == downloadState || "downloadSuccess" == downloadState || "downloadFail" == downloadState) {
            "RN-Single-Download-$appVersion"
        } else {
            "RN-Single-Check-$appVersion"
        }
        Answers.getInstance().logCustom(CustomEvent(eventName).putCustomAttribute("version", "$rnVersion - $componentName - $downloadState"))

    }
}