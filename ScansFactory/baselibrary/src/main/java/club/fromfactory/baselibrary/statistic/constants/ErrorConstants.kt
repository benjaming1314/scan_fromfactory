package club.fromfactory.baselibrary.statistic.constants

/**
 * 错误码常量
 * @author 王佳斌
 * @date 2018/10/11
 */
/**
 * 网络请求错误的code(服务端没有返回code的情况下使用这个)
 */
const val ERROR_CODE_HTTP = 1
/**
 * 网络请求成功的code
 */
const val SUCCESS_CODE_HTTP = 0