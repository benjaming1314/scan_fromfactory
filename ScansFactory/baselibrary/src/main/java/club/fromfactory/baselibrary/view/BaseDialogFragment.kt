package club.fromfactory.baselibrary.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import club.fromfactory.baselibrary.model.TraceInfo
import club.fromfactory.baselibrary.statistic.PagePerformanceRecorder
//import club.fromfactory.baselibrary.widget.BaseWebView

/**
 * DialogFragment基类
 *
 * @author Jellybean
 * @date 2018/6/26
 */
abstract class BaseDialogFragment : RxDialogFragment(), IBaseView {
    private lateinit var aContext: Context
    /**
     * 本页面的追踪信息
     */
    private lateinit var traceInfo: TraceInfo

    override fun onAttach(context: Context) {
        super.onAttach(context)
        aContext = context
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(layoutResId, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initData()
        initView()
        fetchData()
    }

    override fun isAlive(): Boolean {
        return isAdded && !isHidden
    }


    override fun initData() {

    }

    override fun initView() {

    }

    override fun fetchData() {

    }

    override fun showErrorMessage(message: String) {

    }

    override fun showLoadingView() {

    }

    override fun hideLoadingView() {
    }

    override fun getTraceInfo(): TraceInfo {
        return traceInfo
    }

    override fun getWebViewUrl(): String {
        return ""
    }

    override fun getContext(): Context {
        return aContext
    }

    override fun getPagePerformanceRecorder(): PagePerformanceRecorder {
        return null!!
    }

//    override fun getWebView(): BaseWebView? {
//        return null
//    }
}