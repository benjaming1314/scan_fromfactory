package club.fromfactory.baselibrary.statistic

import android.content.Context
import android.os.AsyncTask
import android.os.Build
import android.support.v4.util.ArrayMap
import android.text.TextUtils
import android.util.Log
import club.fromfactory.baselibrary.BaseApplication
import club.fromfactory.baselibrary.BuildConfig
import club.fromfactory.baselibrary.net.retrofit.BaseRetrofit
import club.fromfactory.baselibrary.utils.NetWorkUtils
import club.fromfactory.baselibrary.statistic.constants.EVENT_TYPE
import club.fromfactory.baselibrary.statistic.constants.StatCommonConstants
import club.fromfactory.baselibrary.statistic.exceptions.InvaildEventException
import club.fromfactory.baselibrary.statistic.utils.StatUtil
import club.fromfactory.baselibrary.view.BaseActivity
import com.crashlytics.android.Crashlytics
import org.json.JSONArray
import org.json.JSONObject
import java.util.*

/**
 * stat manager
 * Created by nichenjian on 2018/5/21.
 */
@Deprecated("")
object StatManager {
    // 间隔30s上报一次
    private const val flushDelayInMillis = 30000L
    // 初始化的间隔时间 1s
    private const val flushDelayInitialInMillis = 10000L
    // 上报的条数限制
    private const val FLUSH_EVENT_MAX = 60
    /**
     * stat event collections
     */
    private val events: Vector<Hashtable<String, Any>> = Vector();
    private var errorRetryNum = 0;

    init {
        val timer = Timer();
        timer.schedule(FlushEventsTimerTask(), flushDelayInitialInMillis, flushDelayInMillis)
    }

    /**
     * add stat event
     */
    public fun addStatEvent(event: Hashtable<String, Any>?, context: Context? = null) {
        if (event == null
                || !event.containsKey(EVENT_TYPE)
                || !event.containsKey(StatCommonConstants.EVENTOBJECT)
                || !event.containsKey(StatCommonConstants.OBJECTTYPE)) {
            throw InvaildEventException()
        }

        // add uid stat param
        if (!event.containsKey(StatCommonConstants.UID)) {
            event.put(StatCommonConstants.UID, StatUtil.getUid());
        }

        // add cid stat param
        if (!event.containsKey(StatCommonConstants.CID)) {
            event.put(StatCommonConstants.CID, StatUtil.getCid());
        }

        // add gender stat param
        if (!event.containsKey(StatCommonConstants.GENDER)) {
            event.put(StatCommonConstants.GENDER, StatUtil.getGender());
        }

        // add country code stat param
        if (!event.containsKey(StatCommonConstants.COUNTRYCODE)) {
            event.put(StatCommonConstants.COUNTRYCODE, StatUtil.getCountryCode());
        }

        // add network stat param
        if (!event.containsKey(StatCommonConstants.NETWORKTYPE)) {
            event.put(StatCommonConstants.NETWORKTYPE, StatUtil.getNetwokType());
        }

        // add timestamp stat param
        if (!event.containsKey(StatCommonConstants.TIMESTAMP)) {
            event.put(StatCommonConstants.TIMESTAMP, StatUtil.getTimeStamp());
        }

        // add device id stat param
        if (!event.contains(StatCommonConstants.DEVICEID)) {
            event.put(StatCommonConstants.DEVICEID, StatUtil.getDeviceId());
        }

        // add from stat param
        if (!event.contains(StatCommonConstants.FROM)
                && context is BaseActivity
                && !TextUtils.isEmpty(context.getFrom())) {
            event.put(StatCommonConstants.FROM, context.getFrom())
        }

        printLog(event)

        events.add(event);
        if (events.size >= FLUSH_EVENT_MAX) {
            flushEventsQueneImmediately()
        }
    }

    private fun printLog(event: Hashtable<String, Any>?) {
        if (BuildConfig.DEBUG && Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            val log = StringBuilder()
            event?.forEach { t, u ->
                log.append(t)
                log.append(" ")
                log.append(u)
                log.append(" ")
            }
            log.append("\n")
            Log.e("StatManager", log.toString())
        }
    }

    /**
     * flush events
     */
    public fun flushEventsQueneImmediately() {
        FlushEventsTask().execute();
    }

    private class FlushEventsTimerTask : TimerTask() {
        override fun run() {
            FlushEventsTask().execute();
        }
    }

    @Synchronized
    private fun uploadEvents() {
        val jsonArray = JSONArray();
        val eventsClone: Vector<Hashtable<String, Any>> = events.clone() as Vector<Hashtable<String, Any>>;
        eventsClone.forEach {
            val jsonObject = JSONObject()
            it.forEach {
                jsonObject.put(it.key, it.value)
            }
            jsonArray.put(jsonObject);
        }

        val params = ArrayMap<String, String>();
        params.put("events", jsonArray.toString());
        events.clear()

//        BaseRetrofit
//                .getTrackInstance()
//                .create(TrackApi::class.java)
//                .traceNativeStat(params)
//                .subscribe({
//                    // upload success
//                }, {
//                    // upload failed
//                    if (errorRetryNum < 5) {
//                        events.addAll(eventsClone)
//                    }
//
//                    errorRetryNum++
//                })
    }

    private class FlushEventsTask : AsyncTask<Void, Void, Void?>() {
        override fun doInBackground(vararg params: Void?): Void? {
            if (events.isEmpty()) {
                return null
            } else {
                try {
                    if (NetWorkUtils.isNetworkAvailable(BaseApplication.instance)) {
                        uploadEvents()
                    }
                } catch (e: Exception) {
                    Crashlytics.logException(e)
                }
            }

            return null;
        }
    }
}