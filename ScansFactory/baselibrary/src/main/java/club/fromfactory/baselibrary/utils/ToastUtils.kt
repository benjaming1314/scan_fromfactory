package club.fromfactory.baselibrary.utils

import android.annotation.SuppressLint
import android.content.Context
import android.os.Looper
import android.support.annotation.StringRes
import android.widget.Toast
import club.fromfactory.baselibrary.BaseApplication
import club.fromfactory.baselibrary.extention.log
import io.reactivex.android.schedulers.AndroidSchedulers

/**
 * toast 工具包
 *
 * @author Jellybean
 */
@SuppressLint("StaticFieldLeak")
object ToastUtils {
    private var context: Context = BaseApplication.instance

    private var shortToast: Toast? = null


    /**
     * 显示toast，默认是短时长的
     *
     * @param text 需要显示的文本
     */
    @JvmStatic
    fun show(text: String) {
        if (shortToast == null) {
            shortToast = Toast.makeText(context, text, Toast.LENGTH_SHORT)
        } else {
            shortToast?.setText(text)
        }
        if (isMainThread()) {
            showToast()
        } else {
            AndroidSchedulers.mainThread().scheduleDirect {
                showToast()
            }
        }
    }

    private fun showToast() {
        try {
            // fix crash https://fabric.io/jiayunshuju/android/apps/club.fromfactory/issues/5bcc0581f8b88c296397d7c3?time=last-seven-days
            shortToast?.show()
        } catch (e: Exception) {
            e.log()
        }
    }

    private fun isMainThread() = Looper.myLooper() == Looper.getMainLooper()

    /**
     * 显示toast，默认是短时长的
     *
     * @param textResId 需要显示的文本id
     */
    @JvmStatic
    fun show(@StringRes textResId: Int) {
        if (shortToast == null) {
            shortToast = Toast.makeText(context, textResId, Toast.LENGTH_SHORT)
        } else {
            shortToast?.setText(textResId)
        }
        if (isMainThread()) {
            showToast()
        } else {
            AndroidSchedulers.mainThread().scheduleDirect {
                showToast()
            }
        }
    }

}
