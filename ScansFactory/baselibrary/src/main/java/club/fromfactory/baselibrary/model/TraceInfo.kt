package club.fromfactory.baselibrary.model

import android.os.Parcelable
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

/**
 * 页面的追踪数据
 *
 * @author nichenjian
 * @date 2018/7/31
 */

/**
 * 统计model
 *
 * @param refererPageId 页面来源id
 * @param pageId 当前页面id
 * @param refererUrl 页面来源url
 * @param url 当前页面url
 */
@Parcelize
data class TraceInfo(val refererPageId: Int,
                     val pageId: Int = 0,
                     val refererUrl: String?,
                     val url: String?) : Parcelable {
    /**
     * 来源组件id
     */
    @IgnoredOnParcel
    var fromMid: String? = null

    /**
     * 页面访问深度
     */
    @IgnoredOnParcel
    var index: Int? = 1

    /**
     * 开始展示时间
     */
    @IgnoredOnParcel
    var showTime: Long? = null

    /**
     * 获取当前页面的referer id
     */
    fun getPageRefererId(): String? {
        return if (refererPageId > 0) "1.$refererPageId" else null
    }
}
