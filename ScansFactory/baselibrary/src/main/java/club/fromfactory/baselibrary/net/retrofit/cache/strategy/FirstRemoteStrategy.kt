package club.fromfactory.baselibrary.net.retrofit.cache.strategy

import club.fromfactory.baselibrary.net.retrofit.cache.RxCache
import club.fromfactory.baselibrary.net.retrofit.cache.model.Reply
import io.reactivex.Observable
import java.lang.reflect.Type
import java.util.concurrent.TimeUnit

/**
 * 网络优先(只执行一次回调)
 *
 * 网络请求成功，则执行回调
 * 网络请求失败，则请求缓存
 *
 * @author nichenjian
 * @date 2018/6/26
 */
class FirstRemoteStrategy : IStrategy() {
    override fun <T> execute(rxCache: RxCache, cacheKey: String?, maxAge: Long?, timeUnit: TimeUnit?, source: Observable<T>?, type: Type?): Observable<Reply<T>> {
        val cacheObservable = loadCache<T>(rxCache, cacheKey, type, maxAge, timeUnit)
        val remoteObservable = loadRemote(rxCache, cacheKey, source)
        return Observable.concatDelayError(listOf(remoteObservable, cacheObservable)).take(1)
    }
}