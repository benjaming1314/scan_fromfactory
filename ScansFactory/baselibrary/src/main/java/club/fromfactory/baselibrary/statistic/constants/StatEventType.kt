package club.fromfactory.baselibrary.statistic.constants

/**
 * Created by nichenjian on 2018/5/21.
 */
object StatEventType {
    /**
     * 曝光
     */
    const val IMPRESSION = "impression"
    /**
     * 点击
     */
    const val CLICK = "click"
    /**
     * 视频相关
     */
    const val PLAY = "play"
    /**
     * 外部链接相关
     */
    const val DEEPLINK = "deep_link"
    /**
     * 推送到达
     */
    const val PUSH_ARRIVE = "push_arrive"
    /**
     * 停留时长
     */
    const val STAY = "stay"
    /**
     * 性能指标
     */
    const val PERFORMANCE = "performance"
    /**
     * 请求相关
     */
    const val API = "api"
    /**
     * 商品曝光
     */
    const val PRODUCT = "product"

    /**
     * 通知状态
     */
    const val NOTIFICATION_SWITCH = "notice_switch"

    /**
     * RN组件状态
     */
    const val RN_STATUS = "rn_status"

    @Deprecated("")
    val playTime = "play_time"
    @Deprecated("")
    val startPlay = "start_to_play"
    @Deprecated("")
    val time = "time"
    @Deprecated("")
    val buffer = "buffer"
    @Deprecated("")
    val playEnd = "play_end"
}