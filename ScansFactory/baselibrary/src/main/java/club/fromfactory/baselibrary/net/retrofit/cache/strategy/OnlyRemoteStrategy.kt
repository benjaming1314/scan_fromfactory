package club.fromfactory.baselibrary.net.retrofit.cache.strategy

import club.fromfactory.baselibrary.net.retrofit.cache.RxCache
import club.fromfactory.baselibrary.net.retrofit.cache.model.Reply
import io.reactivex.Observable
import java.lang.reflect.Type
import java.util.concurrent.TimeUnit

/**
 * 只读取网络请求 数据被缓存
 *
 * @author nichenjian
 * @date 2018/6/26
 */
class OnlyRemoteStrategy : IStrategy() {
    override fun <T> execute(rxCache: RxCache, cacheKey: String?, maxAge: Long?, timeUnit: TimeUnit?, source: Observable<T>?, type: Type?): Observable<Reply<T>> {
        return loadRemote(rxCache, cacheKey, source)
    }
}