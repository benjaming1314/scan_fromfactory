package club.fromfactory.baselibrary.statistic.pages

/**
 *@author lxm
 *@date 2018/7/27/027
 */

/**
 * 页面初始化时间戳
 */
const val INIT_TIME = "init_time"
/**
 * Native开始渲染时间戳(生命周期)
 */
const val NATIVE_START_TIME = "native_start_time"
/**
 * Native结束渲染时间戳(生命周期)
 */
const val NATIVE_END_TIME = "native_end_time"
/**
 * 请求开始时间戳
 */
const val NETWORK_START_TIME = "network_start_time"
/**
 * 请求结束时间戳
 */
const val NETWORK_END_TIME = "network_end_time"
/**
 * web页面开始加载时间戳
 */
const val WEBVIEW_START_TIME = "webview_start_time"
/**
 * web页面结束渲染时间戳
 */
const val WEBVIEW_END_TIME = "webview_end_time"
/**
 * 页面开始渲染时间戳
 */
const val PAGE_RENDER_START_TIME = "page_render_start_time"
/**
 * 页面结束渲染时间戳
 */
const val PAGE_RENDER_END_TIME = "page_render_end_time"
