package club.fromfactory.baselibrary.statistic.utils

import android.text.TextUtils
import club.fromfactory.baselibrary.BaseApplication
import club.fromfactory.baselibrary.view.IBaseView
import club.fromfactory.baselibrary.utils.StringUtils
import club.fromfactory.baselibrary.net.CookieHelper
import club.fromfactory.baselibrary.utils.NetWorkUtils

/**
 * 统计的工具类
 * Created by nichenjian on 2018/5/21.
 */

object StatUtil {
    private var netWorkType: String? = null
    private var uid: String? = null
    private var cid: String? = null
    private var gender: String? = null
    private var countryCode: String? = null
    private var deviceId: String? = null

    fun getNetwokType(): String {
        if (TextUtils.isEmpty(netWorkType)) {
            netWorkType = NetWorkUtils.getNetWorkType(BaseApplication.instance)
        }

        return netWorkType!!;
    }

    fun setNetworkType(network: String) {
        if (!TextUtils.isEmpty(network)) {
            netWorkType = network
        }
    }

    /**
     * 获取用户的uid
     */
    fun getUid(): String {
        if (!TextUtils.isEmpty(uid)) {
            return uid!!
        }

        val ruid = CookieHelper.getCookieString("r_uid");
        uid = if (StringUtils.isNotBlank(ruid)) ruid!! else "0";
        return uid!!;
    }

    /**
     * 设置用户的uid
     */
    fun setUid(uid: String) {
        if (!TextUtils.isEmpty(uid)) {
            this.uid = uid;
        }
    }

    /**
     * 获取用户的cid
     */
    fun getCid(): String {
        if (!TextUtils.isEmpty(cid)) {
            return cid!!
        }

        val ga = CookieHelper.getCookieString("_ga");
        if (!TextUtils.isEmpty(ga)) {
            cid = ga.substring(6)
        }
        return if (StringUtils.isNotBlank(cid)) cid!! else "0"
    }

    /**
     * 获取设备id
     */
    fun getDeviceId(): String {
        if (!TextUtils.isEmpty(deviceId)) {
            return deviceId!!
        }

        deviceId = CookieHelper.getCookieString("android_id");
        return if (StringUtils.isNotBlank(deviceId)) deviceId!! else "0"
    }

    /**
     * 获取用户的性别
     * 如果获取失败 默认M
     */
    fun getGender(): String {
        if (!TextUtils.isEmpty(gender)) {
            return gender!!
        }

        gender = CookieHelper.getCookieString("gender");
        return if (StringUtils.isNotBlank(gender)) gender!! else "M";
    }

    /**
     * 获取用户的国家
     * 如果获取失败 默认in
     */
    fun getCountryCode(): String {
        if (!TextUtils.isEmpty(countryCode)) {
            return countryCode!!
        }

        countryCode = CookieHelper.getCookieString("country_code");
        return if (StringUtils.isNotBlank(countryCode)) countryCode!! else "in";
    }

    fun setCountryCode(code: String) {
        if (!TextUtils.isEmpty(code)) {
            countryCode = code
        }
    }

    /**
     * 获取当前的时间戳
     */
    fun getTimeStamp(): Long {
        return System.currentTimeMillis()
    }

    /**
     * 通过注解获取pageId
     *
     * @param obj 需要获取的类
     */
    @JvmStatic
    fun getPageId(baseView: IBaseView): Int {
        return baseView.pageId
    }
}
