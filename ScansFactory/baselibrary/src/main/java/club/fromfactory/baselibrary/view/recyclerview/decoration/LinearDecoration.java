package club.fromfactory.baselibrary.view.recyclerview.decoration;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.annotation.IntDef;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * desc: 线性布局RecyclerView的分割线 <br/>
 * author: 王佳斌 <br/>
 * time: 2017/6/26 <br/>
 * since V6.6 <br/>
 */

public class LinearDecoration extends RecyclerView.ItemDecoration {
    private static final int[] ATTRS = new int[]{android.R.attr.listDivider};
    public static final int HORIZONTAL = LinearLayoutManager.HORIZONTAL;
    public static final int VERTICAL = LinearLayoutManager.VERTICAL;
    /**
     * 无颜色，表示不需要绘制
     */
    public static final int NO_COLOR = -1;

    private Paint mPaint;
    /**
     * 分割用 Drawable
     */
    private Drawable mDivider;
    /**
     * 分割线尺寸，默认为1px
     */
    private int mDividerSize = 1;

    private VisibilityProvider visibilityProvider;

    @IntDef({HORIZONTAL, VERTICAL})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Orientation {
    }

    /**
     * 列表的方向：纵向{@link #VERTICAL} or 水平方向{@link #HORIZONTAL}
     */
    private int mOrientation;


    /**
     * 默认分割线：使用系统的divider，需要Context
     *
     * @param context
     * @param orientation 列表方向
     */
    public LinearDecoration(Context context, @Orientation int orientation) {
        this(context, orientation, 0);
    }

    /**
     * 自定义drawable 作为分割物，需要Context
     *
     * @param context
     * @param orientation 列表方向
     * @param drawableId  分割物 drawableId
     */
    public LinearDecoration(Context context, @Orientation int orientation, int drawableId) {
        if (orientation != VERTICAL && orientation != HORIZONTAL) {
            throw new IllegalArgumentException("请输入正确的方向参数！");
        }
        if (drawableId == 0) {
            final TypedArray a = context.obtainStyledAttributes(ATTRS);
            mDivider = a.getDrawable(0);
            a.recycle();
        } else {
            mDivider = ContextCompat.getDrawable(context, drawableId);
        }
        mOrientation = orientation;
        mDividerSize = orientation == VERTICAL ? mDivider.getIntrinsicHeight() : mDivider
                .getIntrinsicWidth();
    }

    /**
     * 自定义分割线尺寸和颜色，不需要Context
     *
     * @param orientation  列表方向
     * @param dividerSize  分割线尺寸,单位：px
     * @param dividerColor 分割线颜色
     */
    public LinearDecoration(@Orientation int orientation, int dividerSize, int dividerColor) {
        mOrientation = orientation;
        mDividerSize = dividerSize;
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        if (dividerColor != NO_COLOR) {
            mPaint.setColor(dividerColor);
        } else {
            mPaint.setAlpha(0);
        }
        mPaint.setStyle(Paint.Style.FILL);
    }

    /**
     * 自定义分割线尺寸和颜色，不需要Context，，使用竖直的方向
     *
     * @param dividerSize  分割线尺寸,单位：px
     * @param dividerColor 分割线颜色
     */
    public LinearDecoration(int dividerSize, int dividerColor) {
        this(VERTICAL, dividerSize, dividerColor);
    }

    public void setVisibilityProvider(VisibilityProvider visibilityProvider) {
        this.visibilityProvider = visibilityProvider;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                               RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view);
        if (visibilityProvider != null && visibilityProvider.shouldHideDivider(position, parent)) {
            return;
        }
        int size;
        if (mOrientation == VERTICAL) {
            size = mDivider == null ? mDividerSize : mDivider.getIntrinsicHeight();
            outRect.set(0, 0, 0, size);
        } else {
            size = mDivider == null ? mDividerSize : mDivider.getIntrinsicWidth();
            outRect.set(0, 0, size, 0);
        }
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        if (mDivider == null && mPaint.getAlpha() == 0) {
            return;
        }
        if (mOrientation == VERTICAL) {
            drawVertical(c, parent);
        } else {
            drawHorizontal(c, parent);
        }
    }

    /**
     * 绘制纵向布局的分割线
     *
     * @param canvas 画布
     * @param parent RecyclerView控件
     */
    private void drawVertical(Canvas canvas, RecyclerView parent) {
        final int childCount = parent.getChildCount();
        if (childCount > 1) {
            final int left = parent.getPaddingLeft();
            final int right = parent.getWidth() - parent.getPaddingRight();
            for (int i = 0; i < childCount - 1; i++) {
                final View child = parent.getChildAt(i);
                int position = parent.getChildAdapterPosition(child);
                if (visibilityProvider != null && visibilityProvider.shouldHideDivider(position,
                        parent)) {
                    continue;
                }
                RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) child
                        .getLayoutParams();
                final int top = child.getBottom() + layoutParams.bottomMargin;
                final int bottom = top + mDividerSize;
                if (mDivider != null) {
                    mDivider.setBounds(left, top, right, bottom);
                    mDivider.draw(canvas);
                    continue;
                }
                if (mPaint != null) {
                    canvas.drawRect(left, top, right, bottom, mPaint);
                }
            }
        }

    }

    /**
     * 绘制横向布局的分割线
     *
     * @param canvas 画布
     * @param parent RecyclerView控件
     */
    private void drawHorizontal(Canvas canvas, RecyclerView parent) {
        final int childCount = parent.getChildCount();
        if (childCount > 1) {
            final int top = parent.getPaddingTop();
            final int bottom = parent.getHeight() - parent.getPaddingBottom();
            for (int i = 0; i < childCount - 1; i++) {
                final View child = parent.getChildAt(i);
                int position = parent.getChildAdapterPosition(child);
                if (visibilityProvider != null && visibilityProvider.shouldHideDivider(position,
                        parent)) {
                    continue;
                }
                RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) child
                        .getLayoutParams();
                final int left = child.getRight() + layoutParams.rightMargin;
                final int right = left + mDividerSize;
                if (mDivider != null) {
                    mDivider.setBounds(left, top, right, bottom);
                    mDivider.draw(canvas);
                    continue;
                }
                if (mPaint != null) {
                    canvas.drawRect(left, top, right, bottom, mPaint);
                }
            }
        }
    }

    /**
     * 控制是否显示分割线
     */
    public interface VisibilityProvider {

        /**
         * Returns true 表示需要隐藏分割线
         *
         * @param position 分割线的位置
         * @param parent   RecyclerView
         * @return True if the divider at position should be hidden
         */
        boolean shouldHideDivider(int position, RecyclerView parent);
    }
}
