package club.fromfactory.baselibrary.rx

import club.fromfactory.baselibrary.model.BaseResponse
import club.fromfactory.baselibrary.utils.JsonUtils
import io.reactivex.observers.DefaultObserver
import retrofit2.HttpException

/**
 * 网络请求观察者
 *
 * @author Jellybean
 * @date 2018/6/21
 */
/**
 * python服务端正常返回的code
 */
const val NET_SUCCESS_PYTHON = 0

/**
 * 将错误解析为BaseResponse
 */
fun parseThrowableToBaseResponse(e: Throwable): BaseResponse<Any>? {
    if (e is HttpException && e.code() in 400..403) {
        val string = e.response()?.errorBody()?.string()
        if (string != null) {
            return JsonUtils.parseJson<BaseResponse<Any>>(string)
        }
    }
    return null
}

abstract class NetObserver<T> : DefaultObserver<BaseResponse<T>>() {
    override fun onNext(t: BaseResponse<T>) {
        if (t.isSuccess) {
            onSuccess(t.model)
        } else {
            onFailureInternal(t.errorCode, t.firstErrorMessage.displayMessage?:"")
        }
        onAfter()
    }


    override fun onError(e: Throwable) {
        val errorResponse = parseThrowableToBaseResponse(e)
        if (errorResponse != null) {
            onFailureInternal(errorResponse.errorCode, errorResponse.firstErrorMessage.displayMessage?:"")
        } else {
            e.message?.apply {
                onFailure(this)
            }
        }

        onAfter()
    }


    /**
     * 内部错误处理，默认直接调用[onFailure],子类可重写此方法来针对性地处理
     */
    open fun onFailureInternal(code: Int, message: String) {
        onFailure(message)
    }

    /**
     * onComplete 做空实现 没有作用
     */
    override fun onComplete() {}

    /**
     * 网络请求成功的回调
     */
    abstract fun onSuccess(t: T?)

    /**
     * 网络请求失败的回调
     */
    abstract fun onFailure(message: String)

    /**
     * 网络调用结束, 可在此做关闭进度条等结束操作
     */
    open fun onAfter() {}

}