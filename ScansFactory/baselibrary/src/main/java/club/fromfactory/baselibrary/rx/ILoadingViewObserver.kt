package club.fromfactory.baselibrary.rx

import android.os.Looper
import club.fromfactory.baselibrary.view.IBaseView
import io.reactivex.android.schedulers.AndroidSchedulers

/**
 * 自动显示和隐藏加载视图的观察者
 *
 * @author Jellybean
 * @date 2018/6/21
 */
interface ILoadingViewObserver {
    var baseView: IBaseView?
    fun onStart() {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            showLoadingView()
        } else {
            AndroidSchedulers.mainThread().scheduleDirect { showLoadingView() }
        }
    }


    private fun showLoadingView() {
        if (baseView != null && baseView!!.isAlive()) {
            baseView!!.showLoadingView()
        }
    }

    fun onAfter() {
        if (baseView != null && baseView!!.isAlive()) {
            baseView!!.hideLoadingView()
            baseView = null
        }
    }
}