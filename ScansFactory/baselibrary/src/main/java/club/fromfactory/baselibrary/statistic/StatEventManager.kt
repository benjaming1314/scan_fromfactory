package club.fromfactory.baselibrary.statistic

import android.annotation.SuppressLint
import android.os.AsyncTask
import android.support.v4.util.ArrayMap
import android.text.TextUtils
import club.fromfactory.baselibrary.BaseApplication
import club.fromfactory.baselibrary.BuildConfig
import club.fromfactory.baselibrary.net.retrofit.BaseRetrofit
import club.fromfactory.baselibrary.statistic.constants.*
import club.fromfactory.baselibrary.statistic.pages.*
import club.fromfactory.baselibrary.statistic.utils.StatUtil
import club.fromfactory.baselibrary.utils.NetWorkUtils
import club.fromfactory.baselibrary.utils.PreferenceStorageUtils
import club.fromfactory.baselibrary.utils.decode
import club.fromfactory.baselibrary.utils.encode
import club.fromfactory.baselibrary.view.IBaseView
import com.blankj.utilcode.util.LogUtils
import com.crashlytics.android.Crashlytics
import java.util.*

/**
 * 统计事件管理器
 *
 * @author nichenjian
 * @date 2018/7/30
 */

const val FLUSH_IMMEDIATELY = "flush"

object StatEventManager {
    /**
     * 上个点击事件
     */
    private var lastClickEvent: Map<String, Any>? = null

    val TAG: String = StatEventManager::class.java.simpleName
    // 间隔10s上报一次
    private var flushDelayInMillis =  10000L
    // 初始化的间隔时间 1s
    private const val flushDelayInitialInMillis = 1000L
    // 上报的条数限制
    private var flushEventMax =  20
    // 用户的会话Id
    private var traceId = UUID.randomUUID()
    // 页面访问深度
    private var pageIndex = 1

    /**
     * stat event collections
     */
    private var events: Vector<MutableMap<String, Any>> = Vector()
    private var errorRetryNum = 0

    init {
        val timer = Timer()
        timer.schedule(FlushEventsTimerTask(), flushDelayInitialInMillis, flushDelayInMillis)
    }

    /**
     * App退到后台
     */
    fun onAppBackground() {
        // todo
    }

    /**
     * App回到前台
     */
    fun onAppForeground() {
        pageIndex = 1
        generateTraceId()
    }

    /**
     * 重新生成traceId
     */
    private fun generateTraceId() {
        traceId = UUID.randomUUID()
    }

    /**
     * 设置间隔时间
     */
    @JvmStatic
    fun setFlushDelayInMillis(duration: Long) {
        if (duration > 0) {
            flushDelayInMillis = duration
        }
    }

    /**
     * 设置上报条数限制
     */
    @JvmStatic
    fun setFlushEventMax(length: Int) {
        if (length > 0) {
            flushEventMax = length
        }
    }

    private fun isPageImpression(event: Map<String, Any>): Boolean {
        return StatEventType.IMPRESSION == event[EVENT_TYPE]
                && (event[MODULE_ID] as String).split(".").size == 2
    }

    /**
     * add stat event
     *
     * @param event 单条日志
     * @param context 上下文信息
     */
    @JvmOverloads
    fun addStatEvent(event: MutableMap<String, Any>?, baseView: IBaseView? = null) {
        if (event == null
                || !event.containsKey(EVENT_TYPE)
                || !event.containsKey(MODULE_ID)) {
            return
        }

        val flushImmediately = event.containsKey(FLUSH_IMMEDIATELY) && event[FLUSH_IMMEDIATELY].toString() == "1"
        if (flushImmediately) {
            event.remove(FLUSH_IMMEDIATELY)
        }

        val isPageImpression = isPageImpression(event)
        // 如果是页面曝光统计判断是否需要添加点击来源统计
        if (isPageImpression
                && lastClickEvent != null
                && lastClickEvent!![MODULE_ID] != null
                && TextUtils.isEmpty(baseView?.traceInfo?.fromMid)) {
            baseView?.traceInfo?.fromMid = lastClickEvent!![MODULE_ID] as String
            lastClickEvent = null
        }

        // 页面曝光，访问深度自增
        if (isPageImpression) {
            baseView?.traceInfo?.index = pageIndex
            baseView?.traceInfo?.showTime = System.currentTimeMillis()
            pageIndex++
        }

        // 记录上次点击的事件id
        lastClickEvent = if (StatEventType.CLICK == event[EVENT_TYPE]) event else null

        // 添加traceId
        if (!event.containsKey(TRACE_ID)) {
            event[TRACE_ID] = traceId
        }

        // 添加timestamp
        if (!event.containsKey(StatCommonConstants.TIMESTAMP)) {
            event[StatCommonConstants.TIMESTAMP] = StatUtil.getTimeStamp()
        }

        // 添加url
        val url: String? = if (event.containsKey(StatCommonConstants.URL) && event[StatCommonConstants.URL] is String) event[StatCommonConstants.URL] as String else baseView?.webViewUrl
        event[StatCommonConstants.URL] = if (url != null) encode(url) else ""

        if (!event.containsKey(StatCommonConstants.FROM)) {
            // 添加from
            event[StatCommonConstants.FROM] = baseView?.traceInfo?.getPageRefererId() ?: ""
        }

        // 添加referer
        val refererUrl = baseView?.traceInfo?.refererUrl
        event[StatCommonConstants.REFERER] = if (refererUrl != null) encode(refererUrl) else ""

        // 添加点击来源mid
        event[StatCommonConstants.FROMMID] = baseView?.traceInfo?.fromMid ?: ""

        // 添加页面访问深度
        event[StatCommonConstants.INDEX] = baseView?.traceInfo?.index ?: 1

        printLog(event)

        events.add(event)
        if (events.size >= flushEventMax || flushImmediately) {
            flushEventsQueneImmediately()
        }
    }

    /**
     * 打印日志
     */
    private fun printLog(event: Map<String, Any>) {
        if (BuildConfig.DEBUG) {
            val log = StringBuilder()
            for (entry in event.entries) {
                log.append(entry.key)
                log.append(" ")
                log.append(decode(entry.value.toString()))
                log.append(" ")
            }
            log.append("\n")
            LogUtils.i(TAG, log.toString())
            printPerformanceLog(event)
        }
    }

    /**
     * 打印性能统计日志
     */
    private fun printPerformanceLog(event: Map<String, Any>) {
        if (BuildConfig.DEBUG && event[EVENT_TYPE] == StatEventType.PERFORMANCE) {
            val mid = event[MODULE_ID]
            val log = StringBuilder()
            if (mid == "1") {
                log.append(" app startup time (${event[END_TIME].toString().toLong() - event[START_TIME].toString().toLong()}) \n")
            } else {
                log.append(" page ${event[MODULE_ID]}: \n")
                // 打印native渲染时间
                if (event.containsKey(NATIVE_END_TIME) && event.containsKey(NATIVE_START_TIME)) {
                    log.append(" page native render ${event[NATIVE_END_TIME].toString().toLong() - event[NATIVE_START_TIME].toString().toLong()} \n")
                }
                // 打印network请求时间
                if (event.containsKey(NETWORK_END_TIME) && event.containsKey(NATIVE_START_TIME)) {
                    log.append(" page network ${event[NETWORK_END_TIME].toString().toLong() - event[NETWORK_START_TIME].toString().toLong()} \n")
                }
                // 打印webview请求时间
                if (event.containsKey(WEBVIEW_END_TIME) && event.containsKey(WEBVIEW_START_TIME)) {
                    log.append(" page webview ${event[WEBVIEW_END_TIME].toString().toLong() - event[WEBVIEW_START_TIME].toString().toLong()} \n")
                }

                // 打印绘制时间
                if (event.containsKey(PAGE_RENDER_START_TIME) && event.containsKey(PAGE_RENDER_END_TIME)) {
                    log.append(" page render ${event[PAGE_RENDER_END_TIME].toString().toLong() - event[PAGE_RENDER_START_TIME].toString().toLong()} \n")
                }

                // 打印总时间
                log.append(" page render total ${event[PAGE_RENDER_END_TIME].toString().toLong() - event[PAGE_RENDER_START_TIME].toString().toLong()} \n")
            }

            LogUtils.i(log)
        }
    }

    /**
     * flush events
     */
    fun flushEventsQueneImmediately() {
        FlushEventsTask().execute()
    }

    private class FlushEventsTimerTask : TimerTask() {
        override fun run() {
            FlushEventsTask().execute()
        }
    }

    @SuppressLint("CheckResult")
    @Synchronized
    private fun uploadEvents() {
        val eventsToUpload = events
        val params = ArrayMap<String, Any>()
        params["events"] = eventsToUpload
        events = Vector()

//        BaseRetrofit
//                .getTrackInstance()
//                .create(TrackApi::class.java)
//                .postNativeStat(params)
//                .subscribe({
//                    // upload success
//                }, {
//                    // upload failed
//                    if (errorRetryNum < 5) {
//                        events.addAll(eventsToUpload)
//                    }
//
//                    errorRetryNum++
//                })
//        if (BuildConfig.DEBUG || PreferenceStorageUtils.getInstance().debugState) {
//            BaseRetrofit
//                    .getTrackInstance()
//                    .create(TrackApi::class.java)
//                    .postStatForDebug(params)
//                    .subscribe({
//                        // upload success
//                    }, {
//                        it.printStackTrace()
//                    })
//        }
    }

    private class FlushEventsTask : AsyncTask<Void, Void, Void?>() {
        override fun doInBackground(vararg params: Void?): Void? {
            if (events.isEmpty()) {
                return null
            } else {
                // fix crash https://fabric.io/jiayunshuju/android/apps/club.fromfactory/issues/5b5838686007d59fcdaedbdf
                try {
                    if (NetWorkUtils.isNetworkAvailable(BaseApplication.instance)) {
                        uploadEvents()
                    }
                } catch (e: Exception) {
                    Crashlytics.logException(e)
                }
            }

            return null
        }
    }
}
