package club.fromfactory.baselibrary.widget.recyclerview;

/**
 * @author lxm
 * @date 2018/4/27/027
 */
public interface RecyclerItemViewClickListener<T> extends BaseRecyclerItemViewClickListener<T> {

    void reloadingMore();

}
