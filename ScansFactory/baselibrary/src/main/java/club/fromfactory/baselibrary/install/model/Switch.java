package club.fromfactory.baselibrary.install.model;

import com.google.gson.annotations.SerializedName;

import club.fromfactory.baselibrary.model.NoProguard;

public class Switch implements NoProguard {
    @SerializedName("app_install")
    public boolean appInstall;

    /**
     * jni加密的时候需要，不能删除
     *
     * @return
     */
    public static String getPassword() {
        return "Q";
    }
}
