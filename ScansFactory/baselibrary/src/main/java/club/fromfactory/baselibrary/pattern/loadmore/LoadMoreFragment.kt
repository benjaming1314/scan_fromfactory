package club.fromfactory.baselibrary.pattern.loadmore

import club.fromfactory.baselibrary.view.BaseMVPFragment
import club.fromfactory.baselibrary.widget.recyclerview.BaseRecyclerAdapter


/**
 * 加载更多页面的Fragment抽象
 * @author 王佳斌
 * @date 2018/10/23
 */
abstract class LoadMoreFragment<T> : BaseMVPFragment<LoadMoreContract.Presenter<T>>(),
        LoadMoreView<T> {
    /**
     * 在抽象类中引入一个成员变量做中转
     */
    private var adapterInstance: BaseRecyclerAdapter<T>? = null

    override val adapter: BaseRecyclerAdapter<T>
        get() = if (adapterInstance != null) adapterInstance!! else {
            adapterInstance = createAdapter()
            adapterInstance!!
        }

    override fun initView() {
        super<LoadMoreView>.initView()
    }

    /**
     * 子类生成适配器的实例
     */
    abstract fun createAdapter(): BaseRecyclerAdapter<T>

    override fun fetchData() {
        super<LoadMoreView>.fetchData()
    }

    override fun showLoadingView() {
        super<LoadMoreView>.showLoadingView()
    }

    override fun hideLoadingView() {
        super<LoadMoreView>.hideLoadingView()
    }
}