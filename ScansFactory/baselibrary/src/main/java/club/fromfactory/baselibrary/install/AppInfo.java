package club.fromfactory.baselibrary.install;

import club.fromfactory.baselibrary.model.NoProguard;

public class AppInfo implements NoProguard {
    private String packageName;
    private String appName;


    public void setAppName(String mAppName) {
        this.appName = mAppName;
    }

    public String getAppName() {
        return appName;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }
}
