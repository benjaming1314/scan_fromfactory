package club.fromfactory.baselibrary.view;

import club.fromfactory.baselibrary.presenter.IPresenter;

/**
 * BaseMVP视图接口
 *
 * @author Jellybean
 * @date 2018/6/4
 */
public interface IBaseMVPView<P extends IPresenter> extends IBaseView, IMVPView<P> {
}
