package club.fromfactory.baselibrary.router

import android.content.Context
import android.net.Uri

/**
 * 路由拦截器
 *
 * @author nichenjian
 * @date 2018/9/28
 */
const val DEEPLINK_SCHEME = "fromfactory"
interface BaseRouterInterceptor {
    /**
     * 是否能处理路由的统一接口
     *
     * @param context 上下文
     * @param url 路由地址
     * @param path 路径
     * @return 是否能处理 true表示处理，false表示不能处理
     */
    fun handle(context: Context, url: Uri, path: String?): Boolean
}
