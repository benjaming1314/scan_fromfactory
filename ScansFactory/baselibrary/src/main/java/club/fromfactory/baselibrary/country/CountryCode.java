package club.fromfactory.baselibrary.country;

import club.fromfactory.baselibrary.model.NoProguard;

/**
 * Created by lxm on 2016/11/17.
 * 国家相关信息
 * {"areaCode":"0","city":"Hangzhou","country":"China","countryCode":"CN","latitude":"30.293594","longitude":"120.16141","region":"02"}
 */

public class CountryCode implements NoProguard {

    private String areaCode;
    private String city;
    private String country;
    private String countryCode;
    private String latitude;
    private String longitude;
    private String region;

    private String image;
    private String currency_id;
    private String phone_code;

    private boolean isCurrent;

    public String getName() {
        return country;
    }

    public void setName(String country) {
        this.country = country;
    }

    public String getPhone_code() {
        return phone_code;
    }

    public void setPhone_code(String phone_code) {
        this.phone_code = phone_code;
    }

    public String getCurrency_id() {
        return currency_id;
    }

    public void setCurrency_id(String currency_id) {
        this.currency_id = currency_id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCode() {
        return countryCode;
    }

    public void setCode(String countryCode) {
        this.countryCode = countryCode;
    }


    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public boolean isCurrent() {
        return isCurrent;
    }

    public void setCurrent(boolean current) {
        isCurrent = current;
    }

    @Override
    public String toString() {
        return "CountryCode{" +
                "areaCode='" + areaCode + '\'' +
                ", city='" + city + '\'' +
                ", country='" + country + '\'' +
                ", countryCode='" + countryCode + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", region='" + region + '\'' +
                '}';
    }
}
