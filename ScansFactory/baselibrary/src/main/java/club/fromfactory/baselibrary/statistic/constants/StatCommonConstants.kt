package club.fromfactory.baselibrary.statistic.constants

/**
 * common stat key
 * Created by nichenjian on 2018/5/21.
 */
/**
 * 事件类型
 */
const val EVENT_TYPE = "et"
/**
 * 组件id
 */
const val MODULE_ID = "mid"
/**
 * 用户跟踪id
 */
const val TRACE_ID = "tid"
/**
 * 来源统计
 */
const val SOURCE = "source"
/**
 * 按钮
 */
const val BUTTON = "b"
/**
 * 搜索
 */
const val QUERY = "q"
/**
 * 跳转
 */
const val JUMP = "jump"
/**
 * 其他人的个人中心uid
 */
const val OTHER_UID = "uid"
/**
 * 分类ID
 */
const val CATEGORY_ID = "catid"
/**
 * 商品id
 */
const val PRODUCT_ID = "pid"
/**
 * 状态
 */
const val STATE = "state"
/**
 * 停留时间
 */
const val DURATION = "dur"
/**
 * 帖子id
 */
const val NOTE_ID = "noteid"
/**
 * 用户id
 */
const val USER_ID = "uid"
/**
 * 推送id
 */
const val PUSH_ID = "pushid"

/**
 * 链接类型
 */
const val LINK_TYPE = "linktype"

/**
 * 点击位置
 */
const val POSITION = "p"

/**
 * 输入内容
 */
const val INPUT = "inp"

/**
 * 补充信息
 */
const val INFO = "info"


class StatCommonConstants {
    companion object {
        /**
         * 事件对象
         */
        @Deprecated("")
        const val EVENTOBJECT = "o"
        /**
         * 对象类型
         */
        @Deprecated("")
        const val OBJECTTYPE = "ot"
        /**
         * 手机的cid
         */
        @Deprecated("")
        const val CID = "cid"
        /**
         * 用户的uid
         */
        const val UID = "uid"
        /**
         * 性别
         */
        const val GENDER = "g"
        /**
         * 国家
         */
        const val COUNTRYCODE = "cc"
        /**
         * 页面地址
         */
        const val URL = "url"
        /**
         * 来源
         */
        const val FROM = "fr"
        /**
         * networkType
         */
        const val NETWORKTYPE = "nt"
        /**
         * 时间戳
         */
        const val TIMESTAMP = "ts"
        /**
         * 停留时间
         */
        const val DURATION = "dur"
        /**
         * 设备id
         */
        @Deprecated("")
        const val DEVICEID = "did"
        /**
         * 按钮状态
         */
        const val BUTTONSTATUS = "bs"

        /**
         * referer url
         * 页面的来源url
         */
        const val REFERER = "ref"

        /**
         * 点击来源组件id
         */
        const val FROMMID = "fmid"

        /**
         * 页面访问深度索引
         */
        const val INDEX = "in"
    }
}