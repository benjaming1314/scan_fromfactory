package club.fromfactory.baselibrary.utils

import com.crashlytics.android.Crashlytics
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken

/**
 * Json工具
 * @author 王佳斌
 * @date 2018/8/17
 */
val gson = Gson()
val disableHtmlEscapingGson : Gson = GsonBuilder().disableHtmlEscaping().create()
object JsonUtils {

    /**
     * 反序列化json字符串
     *
     * @param json json字符串
     * @return 对应的对象
     */
    inline fun <reified T> parseJson(json: String): T? {
        return try {
            gson.fromJson(json, object : TypeToken<T>() {}.type)
        } catch (e: Exception) {
            Crashlytics.logException(e)
            null
        }
    }

    /**
     * 将对象序列化为json字符串
     *
     * @param o 对象
     * @return 对应的字符串
     */
    fun toJson(o: Any): String {
        return gson.toJson(o)
    }

    /**
     * 根据指定的类型将对象序列化为json字符串
     *
     * @param o 对象
     * @return 对应的字符串
     */
    inline fun <reified T> toJsonByType(o: Any): String {
        return gson.toJson(o, object : TypeToken<T>() {}.type)
    }
}