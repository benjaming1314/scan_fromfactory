package club.fromfactory.baselibrary.networkspeed;

import android.os.AsyncTask;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

public class DefaultBandWidthSampler {
    private static final String URL = "http://img02.tooopen.com/images/20160509/tooopen_sy_161967094653.jpg";
    private DeviceBandWidthSampler mDeviceBandWidthSampler;

    private DefaultBandWidthSampler() {
        mDeviceBandWidthSampler = DeviceBandWidthSampler.getInstance();
    }

    /**
     * singleton
     */
    private static class DefaultBandWidthSamplerHolder {
        public static DefaultBandWidthSampler instance = new DefaultBandWidthSampler();
    }

    public static DefaultBandWidthSampler getInstance() {
        return DefaultBandWidthSamplerHolder.instance;
    }

    public void startSampling() {
        new DownloadImage().execute(URL);
    }

    /**
     * AsyncTask for handling downloading and making calls to the timer.
     */
    private class DownloadImage extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            mDeviceBandWidthSampler.startBandWidthSampling();
        }

        @Override
        protected Void doInBackground(String... url) {
            String imageURL = url[0];
            try {
                // Open a stream to download the image from our URL.
                URLConnection connection = new URL(imageURL).openConnection();
                connection.setUseCaches(false);
                connection.connect();
                InputStream input = connection.getInputStream();
                try {
                    byte[] buffer = new byte[1024];

                    // Do some busy waiting while the stream is open.
                    while (input.read(buffer) != -1) {
                    }
                } finally {
                    input.close();
                }
            } catch (IOException e) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            mDeviceBandWidthSampler.stopBandWidthSampling();
        }
    }
}
