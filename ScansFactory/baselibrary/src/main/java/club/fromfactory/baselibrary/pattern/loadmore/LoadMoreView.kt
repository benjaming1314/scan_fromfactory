package club.fromfactory.baselibrary.pattern.loadmore

import android.support.v7.widget.RecyclerView
import android.view.View
import club.fromfactory.baselibrary.widget.recyclerview.BaseRecyclerAdapter
import com.scwang.smartrefresh.layout.SmartRefreshLayout

/**
 * 加载更多的基础视图
 * @author 王佳斌
 * @date 2018/10/24
 */
interface LoadMoreView<T> : LoadMoreContract.View<T> {
    /**
     * 列表适配器
     */
    val adapter: BaseRecyclerAdapter<T>
    /**
     * 子类提供SmartRefreshLayout实例
     */
    val refreshLayout: SmartRefreshLayout

    /**
     * 子类提供RecyclerView实例
     */
    val recyclerView: RecyclerView

    /**
     * 子类提供RecyclerView实例
     */
    val progressView: View

    override fun initView() {
        refreshLayout.setOnLoadMoreListener {
            presenter.getMoreData()
        }
    }

    override fun showData(data: List<T>) {
        if (recyclerView.adapter == null) {
            adapter.data = data
            recyclerView.adapter = adapter
        } else {
            adapter.setData(data, true)
        }
    }

    override fun showMoreData(data: List<T>) {
        adapter.addAll(data)
    }

    override fun onLoadMoreSucceed() {
        refreshLayout.finishLoadMore(true)
    }

    override fun onLoadMoreFailed() {
        refreshLayout.finishLoadMore(false)
    }

    override fun onRefreshSucceed() {
        refreshLayout.finishRefresh(true)
    }

    override fun onRefreshFailed() {
        refreshLayout.finishRefresh(false)
    }

    override fun onNoMoreData() {
        refreshLayout.finishLoadMoreWithNoMoreData()
    }

    override fun fetchData() {
        presenter.getData()
    }

    override fun showLoadingView() {
        progressView.visibility = View.VISIBLE
    }

    override fun hideLoadingView() {
        progressView.visibility = View.GONE
    }

}