package club.fromfactory.baselibrary.rx

/**
 * 没有更多数据的错误码
 */
const val NO_MORE_DATA_CODE_ONE = 51003
const val NO_MORE_DATA_CODE_TWO = 23103

/**
 * 分页数据的观察者
 *
 * @author Jellybean
 * @date 2018/6/21
 */

interface IPagingObserver {

    fun onFailureInternal(code: Int, message: String) {
        if (code == NO_MORE_DATA_CODE_ONE || code == NO_MORE_DATA_CODE_TWO) {
            onNoMoreData()
        }
    }


    /**
     * 没有更多数据
     */
    fun onNoMoreData()
}