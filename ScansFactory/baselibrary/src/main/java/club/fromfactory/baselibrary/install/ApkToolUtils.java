package club.fromfactory.baselibrary.install;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import java.util.ArrayList;
import java.util.List;

public class ApkToolUtils {

    public static List<AppInfo> getInstalledAppList(PackageManager packageManager) {
        List<AppInfo> appInfos = new ArrayList<>();
        try {
            List<PackageInfo> packageInfoList = packageManager.getInstalledPackages(0);
            for (int i = 0; i < packageInfoList.size(); i++) {
                PackageInfo packageInfo = packageInfoList.get(i);

                // 过滤系统的app
                if ((ApplicationInfo.FLAG_SYSTEM & packageInfo.applicationInfo.flags) != 0) {
                    continue;
                }

                AppInfo appInfo = new AppInfo();
                appInfo.setPackageName(packageInfo.packageName);
                appInfo.setAppName(packageInfo.applicationInfo.loadLabel(packageManager).toString());

                appInfos.add(appInfo);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return appInfos;
    }
}
