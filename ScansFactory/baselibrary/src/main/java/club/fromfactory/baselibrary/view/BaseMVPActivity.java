package club.fromfactory.baselibrary.view;

import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;

import club.fromfactory.baselibrary.presenter.IPresenter;

/**
 * MVP模式的Activity基类
 *
 * @author Jellybean
 * @date 2018/6/1
 */
public abstract class BaseMVPActivity<P extends IPresenter> extends BaseActivity implements
        IMVPView<P> {
    protected P presenter;


    /**
     * 在初始化数据中创建Presenter, 子类覆写此方法时务必调用此方法。
     * 如果创建presenter依赖初始化话数据的操作，可以先获取数据，再调用此方法。
     */
    @Override
    @CallSuper
    public void initData() {
        super.initData();
        presenter = createPresenter();
    }

    @NonNull
    @Override
    public P getPresenter() {
        return presenter;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (presenter != null) {
            presenter.unsubscribe();
        }
    }
}
