package club.fromfactory.baselibrary.objectpool

/**
 * 对象池对象
 *
 * @author nichenjian
 * @date 2018/11/1
 */
class PoolObject<T>(
        /**
         * 实际缓存对象
         */
        var objection: T?,
        /**
         * 是否在用
         */
        var busy: Boolean = false)
