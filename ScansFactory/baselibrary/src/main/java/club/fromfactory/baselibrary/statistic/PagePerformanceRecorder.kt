package club.fromfactory.baselibrary.statistic

/**
 * 页面性能记录者
 * @author 王佳斌
 * @date 2018/8/1
 */
interface PagePerformanceRecorder {
    /**
     * 设置web页面开始加载时间
     */
    fun setWebViewStartTime()

    /**
     * 设置web页面结束渲染时间
     */
    fun setWebViewEndTime()

    /**
     * 设置页面结束渲染时间
     */
    fun setPageRenderEndTime()

    /**
     * 设置页面开始渲染时间
     */
    fun setPageRenderStartTime()

    /**
     * 设置请求开始时间
     */
    fun setNetworkStartTime()

    /**
     * 设置请求结束时间
     */
    fun setNetworkEndTimed()
}