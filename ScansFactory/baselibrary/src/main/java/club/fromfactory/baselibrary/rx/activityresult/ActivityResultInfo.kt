package club.fromfactory.baselibrary.rx.activityresult

import android.content.Intent

/**
 * Activity的回调信息
 *
 * @param resultCode 请求码
 * @param data 数据
 */
data class ActivityResultInfo(var resultCode: Int, var data: Intent?)