package club.fromfactory.baselibrary.utils

import android.app.Activity
import android.content.Intent
import android.text.TextUtils
import club.fromfactory.baselibrary.BuildConfig
import java.io.BufferedReader
import java.io.FileReader
import java.io.IOException

/**
 *@author lxm
 *@date 2018/9/20
 */
object AppUtils {

    /**
     * 重启app
     */
    @JvmStatic
    fun restartApp(mActivity: Activity) {
        val i = mActivity.baseContext.packageManager
                .getLaunchIntentForPackage(mActivity.baseContext.packageName)
        i!!.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        mActivity.startActivity(i)

    }

    @JvmStatic
    fun getRealVersionName(): String {
        return if (BuildConfig.DEBUG && BuildConfig.VERSION_NAME.endsWith("d")) {
            BuildConfig.VERSION_NAME.replace("d", "")
        } else BuildConfig.VERSION_NAME
    }

    /**
     * 获取进程号对应的进程名
     *
     * @param pid 进程号
     * @return 进程名
     */
    @JvmStatic
    fun getProcessName(pid: Int): String? {
        var reader: BufferedReader? = null
        try {
            reader = BufferedReader(FileReader("/proc/$pid/cmdline"))
            var processName = reader.readLine()
            if (!TextUtils.isEmpty(processName)) {
                processName = processName.trim { it <= ' ' }
            }
            return processName
        } catch (throwable: Throwable) {
            throwable.printStackTrace()
        } finally {
            try {
                reader?.close()
            } catch (exception: IOException) {
                exception.printStackTrace()
            }
        }

        return null
    }
}