package club.fromfactory.baselibrary.view;

import android.support.annotation.NonNull;

import club.fromfactory.baselibrary.presenter.IPresenter;
import club.fromfactory.baselibrary.statistic.PagePerformanceRecorder;

/**
 * MVP模式的View
 *
 * @param <P> 对应的Presenter
 * @author Jellybean
 * @date 2018/6/1
 */
public interface IMVPView<P extends IPresenter> {
    /**
     * 生成 presenter
     *
     * @return 生成对应的Presenter
     */
    @NonNull
    P createPresenter();

    /**
     * 生成 presenter
     *
     * @return 生成对应的Presenter
     */
    @NonNull
    P getPresenter();

    /**
     * 获取页面性能统计的对象
     */
    @NonNull
    PagePerformanceRecorder getPagePerformanceRecorder();

}
