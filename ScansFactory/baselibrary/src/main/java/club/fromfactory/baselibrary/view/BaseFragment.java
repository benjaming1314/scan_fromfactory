package club.fromfactory.baselibrary.view;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import butterknife.ButterKnife;
import club.fromfactory.baselibrary.model.TraceInfo;
import club.fromfactory.baselibrary.statistic.PageId;
import club.fromfactory.baselibrary.statistic.PagePerformanceRecorder;
import club.fromfactory.baselibrary.statistic.PerformanceStatisticsUtils;
import club.fromfactory.baselibrary.statistic.utils.StatAddEventUtil;
import club.fromfactory.baselibrary.statistic.utils.StatUtil;
import club.fromfactory.baselibrary.utils.StatusBarUtils;
//import club.fromfactory.baselibrary.widget.BaseWebView;
import org.jetbrains.annotations.NotNull;


/**
 * fragment基类 Created by zhoulei on 2017/7/4.
 */

public abstract class BaseFragment extends RxFragment implements IBaseView {

    /**
     * 是否ViewPager下的fragment
     */
    private boolean isViewPager = false;

    /**
     * 是否通过fragmentManager的show或者hide方法控制显示
     */
    private boolean isVisibleByShowAndHide = false;
    protected Context context;

    /**
     * 本页面的追踪信息
     */
    protected TraceInfo traceInfo;

    /**
     * 页面性能统计
     */
    private PerformanceStatisticsUtils.PerformancePageData performancePageData = new
            PerformanceStatisticsUtils.PerformancePageData(this);

    /**
     * 是否fragmentPagerAdapter
     */
    private boolean isFragmentPagerAdapter = false;

    @Override
    public void onAttach(@Nullable Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
            Bundle savedInstanceState) {
        performancePageData.setNativeStartTime().setPageRenderStartTime();
        View view = inflater.inflate(getLayoutResId(), container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NotNull View view, @org.jetbrains.annotations.Nullable Bundle
            savedInstanceState) {
        performancePageData.setNativeEndTime();
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initData();
        initView();
        setPageStatInfo();
        performancePageData.setNetworkStartTime();
        fetchData();
    }

    @Override
    public void onResume() {
        super.onResume();
        updateStatusBar();
        if (!isViewPager && !isVisibleByShowAndHide) {
            onPageShow();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (!isViewPager && !isVisibleByShowAndHide) {
            onPageHide();
        }
    }

    // fragment 显示的生命周期
    protected void onPageShow() {
//        if (getWebView() != null) {
//            getWebView().executeJsCode(ON_PAGE_SHOW);
//        }
    }

    // fragment 隐藏的声明周期
    protected void onPageHide() {
//        if (getWebView() != null) {
//            getWebView().executeJsCode(ON_PAGE_HIDE);
//        }
        StatAddEventUtil.addPageStayTimeEvent(this);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        isFragmentPagerAdapter = true;
        this.isViewPager = true;
        if (isVisibleToUser) {
            onPageShow();
        } else {
            if (isAdded()) {
                onPageHide();
            }
        }
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            addPageImpressionStat();
        }

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        isVisibleByShowAndHide = true;
        if (!hidden) {
            updateStatusBar();
            if (isAdded()) {
                onPageShow();
            }
        } else {
            if (isAdded()) {
                onPageHide();
            }
        }
        if (isAlive() && !hidden) {
            addPageImpressionStat();
        }
    }

    // 更新statusBar的状态
    public void updateStatusBar() {
        StatusBarUtils
                .updateStatusBar(getActivity(), StatusBarUtils.StatusBarColor.WHITE, false, true);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean isAlive() {
        return isAdded() && !isHidden();
    }


    @Override
    public void initData() {

    }

    @Override
    public void initView() {

    }

    @Override
    public void fetchData() {

    }

    @Override
    public void showErrorMessage(String message) {

    }

    @Override
    public void setPageStatInfo() {
        generateTraceInfo();
    }

    private void generateTraceInfo() {
        int previousPageId = 0;
        String refererUrl = null;
        if (context instanceof BaseActivity && ((BaseActivity) context).getTraceInfo() != null) {
            TraceInfo activityTraceInfo = ((BaseActivity) context).getTraceInfo();
            previousPageId = activityTraceInfo.getPageId();
            refererUrl = activityTraceInfo.getUrl();
        }

        traceInfo = new TraceInfo(previousPageId,
                StatUtil.getPageId(this),
                refererUrl,
                getWebViewUrl());
    }


    @Override
    @NonNull
    public PagePerformanceRecorder getPagePerformanceRecorder() {
        return performancePageData;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (isAlive() && !isFragmentPagerAdapter) {
            addPageImpressionStat();
        }
    }

    protected void addPageImpressionStat() {
        StatAddEventUtil.addPageImpressionEvent(this);
    }

    @Override
    public int getPageId() {
        PageId pageId = getClass().getAnnotation(PageId.class);
        if (pageId != null) {
            return pageId.value();
        }
        return 0;
    }

    /**
     * 页面结束渲染记录,有三种情况： 1. 二次渲染，IO处理结束，绑定数据到视图操作结束后再调用此方法 2. 没有网络请求，第一次渲染结束就调用 3. WebView加载结束
     */
    protected void logPageRenderEnd() {
        final View view = getView();
        if (view == null) {
            return;
        }
        view.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {

            @Override
            public boolean onPreDraw() {
                view.getViewTreeObserver().removeOnPreDrawListener(this);
                performancePageData.setPageRenderEndTime();
                return false;
            }
        });
    }

    @Override
    public void showLoadingView() {

    }

    @Override
    public void hideLoadingView() {

    }

    @Override
    public String getWebViewUrl() {
        return null;
    }


    @Override
    public TraceInfo getTraceInfo() {
        return traceInfo;
    }

    @Nullable
    @Override
    public Context getContext() {
        return context;
    }

//    @Override
//    public BaseWebView getWebView() {
//        return null;
//    }
}

