package club.fromfactory.baselibrary.utils

import android.os.Parcel
import android.os.Parcelable

/**
 * clone工具类
 * @author 王佳斌
 * @date 2018/9/5
 */

object CloneUtils {
    /**
     * 使用json序列化实现深度拷贝
     */
    inline fun <reified T> copyByJson(t: T): T? {
        t ?: return null
        return JsonUtils.parseJson<T>(JsonUtils.toJsonByType<T>(t))
    }

    /**
     * 使用parcel进行深度拷贝（单个parcelable的对象））
     */
    fun <T> copyByParcel(data: T, creator: Parcelable.Creator<T>): T? {
        return if (data is Parcelable) {
            val parcel = Parcel.obtain()
            data.writeToParcel(parcel, 0)
            parcel.setDataPosition(0)
            creator.createFromParcel(parcel)
        } else null
    }

    /**
     * 使用parcel进行ParcelableList深度拷贝
     */
    fun <T> copyByParcel(dataList: List<T>, creator: Parcelable.Creator<T>): List<T>? {
        val newArray = creator.newArray(dataList.size)
        if (!dataList.isEmpty()) {
            for ((i, data) in dataList.withIndex()) {
                newArray[i] = copyByParcel(data, creator)
            }
        }
        return newArray.asList()
    }

    /**
     * 使用parcel进行深度拷贝，无需creator，利用反射实现
     * 注意： parcel为参数的构造器得是public的
     */
    inline fun <reified T> copyByParcel(data: T): T? {
        return if (data is Parcelable) {
            val parcel = Parcel.obtain()
            data.writeToParcel(parcel, 0)
            parcel.setDataPosition(0)
            val javaClass = T::class.java
            try {
                val constructor = javaClass.getConstructor(Parcel::class.java)
                constructor.newInstance(parcel)
            } catch (e: Exception) {
                null
            }
        } else null
    }

    /**
     * 使用parcel进行列表深度拷贝，无需creator，利用反射实现
     * 注意： parcel为参数的构造器得是public的
     */
    inline fun <reified T> copyByParcel(dataList: List<T>): List<T>? {
        if (!dataList.isEmpty()) {
            val list = ArrayList<T>()
            for (data in dataList) {
                // 有一个失败就返回null
                val element = copyByParcel(data) ?: return null
                list.add(element)
            }
            return list
        }
        return null
    }
}