package club.fromfactory.baselibrary

import android.app.Application
import android.content.Context
import android.os.StrictMode
import android.support.multidex.MultiDex
import com.facebook.cache.disk.DiskCacheConfig
import com.facebook.common.memory.MemoryTrimType
import com.facebook.common.memory.NoOpMemoryTrimmableRegistry
import com.facebook.common.util.ByteConstants
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.imagepipeline.core.ImagePipelineConfig
import com.facebook.imagepipeline.core.ImagePipelineFactory
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import com.scwang.smartrefresh.layout.footer.BallPulseFooter
import java.util.*

/**
 * Application 基类
 */
open class BaseApplication : Application() {

    /**
     * 系统默认的local
     *
     *
     * 在这里保存了系统的语言设置是因为在后续会覆盖手机的语言设置，导致后续无法获取真实的系统语言 在 google uac 接口中需要使用到这个参数
     */
    private var systemLocalDefault = ""

    override fun attachBaseContext(base: Context) {
        systemLocalDefault = Locale.getDefault().toString()
        super.attachBaseContext(base)
        MultiDex.install(this)
        instance = this
    }

    fun getSystemDefaultLocal(): String {
        return systemLocalDefault
    }

    override fun onCreate() {
        super.onCreate()
//        initLeakCanary()
        initStetho()
        initFresco()
        initStrictMode()
        setRefreshLayout()
        onCheckedProcess()
    }

    /**
     * 设置刷新加载布局的属性
     */
    private fun setRefreshLayout() {
        SmartRefreshLayout.setDefaultRefreshFooterCreator { context, _ ->
            val color = 0xff666666.toInt()
            BallPulseFooter(context).setNormalColor(color)
                    .setIndicatorColor(color).setAnimatingColor(color)
        }
    }

    /**
     * 检查完进程之后的回调
     */
    protected open fun onCheckedProcess() {
        com.blankj.utilcode.util.Utils.init(this)
    }

    /**
     * 初始化stetho
     *
     * 在debug的时候开启stetho方便调试
     */
    private fun initStetho() {
        if (BuildConfig.DEBUG) {
            try {
                val stetho = Class.forName("com.facebook.stetho.Stetho")
                val method = stetho.getDeclaredMethod("initializeWithDefaults", Context::class.java)
                method.invoke(null, this)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    /**
     * 初始化fresco
     */
    private fun initFresco() {

        val memoryTrimmableRegistry = NoOpMemoryTrimmableRegistry.getInstance()
        memoryTrimmableRegistry.registerMemoryTrimmable {

            val suggestedTrimRatio = it.suggestedTrimRatio;

            if (!(MemoryTrimType.OnCloseToDalvikHeapLimit.suggestedTrimRatio != suggestedTrimRatio
                            && MemoryTrimType.OnSystemLowMemoryWhileAppInBackground.suggestedTrimRatio != suggestedTrimRatio
                            && MemoryTrimType.OnSystemLowMemoryWhileAppInForeground.suggestedTrimRatio != suggestedTrimRatio)) {
                //清空内存缓存
                ImagePipelineFactory.getInstance().getImagePipeline().clearMemoryCaches();
            }
        }
        val config = ImagePipelineConfig.newBuilder(this)
                .setMainDiskCacheConfig(DiskCacheConfig.newBuilder(this)
                        .setBaseDirectoryPath(externalCacheDir)//设置磁盘缓存的路径
                        .setBaseDirectoryName("scan_app")//设置磁盘缓存文件夹的名称
                        .setMaxCacheSize((20 * ByteConstants.MB).toLong())//设置磁盘缓存的大小
                        .build())
                .setDownsampleEnabled(true)
                .setMemoryTrimmableRegistry(memoryTrimmableRegistry)
                .build()
        Fresco.initialize(this, config)

    }

    /**
     * 初始化严格模式
     */
    private fun initStrictMode() {
        if (BuildConfig.DEBUG) {
            StrictMode.setThreadPolicy(StrictMode.ThreadPolicy.Builder()
                    .detectDiskReads()
                    .detectDiskWrites()
                    .detectNetwork()
                    .detectCustomSlowCalls()
                    .penaltyDeathOnNetwork()
                    .build())

            StrictMode.setVmPolicy(StrictMode.VmPolicy.Builder()
                    .detectLeakedSqlLiteObjects()
                    .detectLeakedClosableObjects()
                    .detectLeakedRegistrationObjects()
                    .detectActivityLeaks()
                    .penaltyLog()
                    .build())
        }
    }


    companion object {
        lateinit var instance: BaseApplication
    }
}
