package club.fromfactory.baselibrary.objectpool

import java.util.*

/**
 * 对象池
 *
 * @author nichenjian
 * @date 2018/11/1
 */
abstract class ObjectPool<T> {
    companion object {
        /**
         * 最小的对象数
         */
        const val MIN_NUM_OBJECTS = 3
        /**
         * 最大对象数
         */
        const val MAX_NUM_OBJECTS = 6
        /**
         * 默认增长值
         */
        const val DEFAULT_INCREMENT = 1
    }

    /**
     * 对象集合
     */
    private var objects: Vector<PoolObject<T>>? = null

    @Synchronized
    fun createPool() {
        if (objects != null) {
            return
        }

        objects = Vector()
        for (i in 0 until getMinNumObjects()) {
            objects?.addElement(createObject())
        }
    }

    open fun getMinNumObjects(): Int {
        return MIN_NUM_OBJECTS
    }

    /**
     * 创建对象方法
     */
    abstract fun createObject(): PoolObject<T>?

    /**
     * 获取对象
     *
     * @return T? 对象
     */
    @Synchronized
    fun getObject(): T? {
        if (objects == null) {
            return null
        }

        return getFreeObject()
    }

    /**
     * 释放对象
     *
     * @param objection 需要释放的对象
     */
    @Synchronized
    fun returnObject(objection: T?) {
        if (objects == null) {
            return
        }

        var obj: PoolObject<T>?
        val enumeration = objects!!.elements()
        while (enumeration.hasMoreElements()) {
            obj = enumeration.nextElement()
            if (objection == obj?.objection) {
                obj.busy = false
                break
            }
        }
    }

    /**
     * 获取空闲对象
     *
     * @return T? 空闲对象
     */
    private fun getFreeObject(): T? {
        var objection = findFreeObjectFromPool()
        if (objection == null) {
            // 如果缓存池中没有找到可以使用的对象，则再创建一个对象
            createObjectsIfNeed()
            // 再从对象池中找一遍
            objection = findFreeObjectFromPool()
        }

        return objection
    }

    /**
     * 创建对象
     */
    private fun createObjectsIfNeed() {
        for (i in 0 until DEFAULT_INCREMENT) {
            if (objects != null && objects!!.size > MAX_NUM_OBJECTS) {
                return
            }

            objects?.addElement(createObject())
        }
    }

    /**
     * 从对象池中获取空闲对象
     * 获取之后将空闲对象设置非空闲
     *
     * @return T? 空闲对象
     */
    private fun findFreeObjectFromPool(): T? {
        if (objects == null || objects!!.isEmpty()) {
            return null
        }

        var poolObject: PoolObject<T>?
        var objection: T? = null
        val enumeration = objects!!.elements()
        while (enumeration.hasMoreElements()) {
            poolObject = enumeration.nextElement() as PoolObject<T>
            if (!poolObject.busy) {
                objection = poolObject.objection
                poolObject.busy = true
                break
            }
        }

        return objection
    }
}
