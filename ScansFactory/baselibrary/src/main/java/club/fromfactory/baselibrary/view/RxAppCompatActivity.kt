package club.fromfactory.baselibrary.view

import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.annotation.CheckResult
import android.support.v7.app.AppCompatActivity
import com.trello.rxlifecycle2.LifecycleProvider
import com.trello.rxlifecycle2.LifecycleTransformer
import com.trello.rxlifecycle2.RxLifecycle
import com.trello.rxlifecycle2.android.ActivityEvent
import com.trello.rxlifecycle2.android.RxLifecycleAndroid
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject

/**
 * 响应式Activity，将Activity各生命周期转换为对应的Transformer
 *
 * @author Jellybean
 * @date 2018/5/30
 */
abstract class RxAppCompatActivity : AppCompatActivity(), LifecycleProvider<ActivityEvent> {

    /**
     * 生命周期分发
     */
    private val lifecycleBehaviorSubject = BehaviorSubject.create<ActivityEvent>()

    @CheckResult
    override fun lifecycle(): Observable<ActivityEvent> {
        return lifecycleBehaviorSubject.hide()
    }

    @CheckResult
    override fun <T : Any?> bindToLifecycle(): LifecycleTransformer<T> {
        return RxLifecycleAndroid.bindActivity(lifecycleBehaviorSubject)
    }

    @CheckResult
    override fun <T : Any?> bindUntilEvent(event: ActivityEvent): LifecycleTransformer<T> {
        return RxLifecycle.bindUntilEvent(lifecycleBehaviorSubject, event)
    }

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        lifecycleBehaviorSubject.onNext(ActivityEvent.CREATE)
        super.onCreate(savedInstanceState)
    }

    @CallSuper
    override fun onStart() {
        lifecycleBehaviorSubject.onNext(ActivityEvent.START)
        super.onStart()
    }

    @CallSuper
    override fun onResume() {
        lifecycleBehaviorSubject.onNext(ActivityEvent.RESUME)
        super.onResume()
    }

    @CallSuper
    override fun onPause() {
        lifecycleBehaviorSubject.onNext(ActivityEvent.PAUSE)
        super.onPause()
    }

    @CallSuper
    override fun onStop() {
        lifecycleBehaviorSubject.onNext(ActivityEvent.STOP)
        super.onStop()
    }

    @CallSuper
    override fun onDestroy() {
        lifecycleBehaviorSubject.onNext(ActivityEvent.DESTROY)
        super.onDestroy()
    }

}