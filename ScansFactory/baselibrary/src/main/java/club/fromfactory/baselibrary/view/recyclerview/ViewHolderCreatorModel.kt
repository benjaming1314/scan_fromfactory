package club.fromfactory.baselibrary.view.recyclerview

data class ViewHolderCreatorModel<T>(var type : Int ,
                              var data : T)