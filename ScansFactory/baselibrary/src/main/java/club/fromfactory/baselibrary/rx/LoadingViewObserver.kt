package club.fromfactory.baselibrary.rx

import club.fromfactory.baselibrary.view.IBaseView

/**
 * 自动显示和隐藏加载视图的观察者
 *
 * @author Jellybean
 * @date 2018/6/21
 */
abstract class LoadingViewObserver<T>(override var baseView: IBaseView?) : NetObserver<T>(),
        ILoadingViewObserver {

    override fun onStart() {
        super<ILoadingViewObserver>.onStart()
    }

    override fun onAfter() {
        super<ILoadingViewObserver>.onAfter()
    }

}