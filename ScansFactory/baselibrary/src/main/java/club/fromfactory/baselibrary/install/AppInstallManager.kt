package club.fromfactory.baselibrary.install

import android.annotation.SuppressLint
import club.fromfactory.baselibrary.BaseApplication
import club.fromfactory.baselibrary.extention.log
import club.fromfactory.baselibrary.net.retrofit.BaseRetrofit
import club.fromfactory.baselibrary.utils.AESEncryptUtil
import club.fromfactory.baselibrary.utils.PreferenceUtils
import club.fromfactory.baselibrary.utils.encode
import com.google.gson.Gson

/**
 * app安装管理类
 */
class AppInstallManager {
    /**
     * 记录上次上报的时间戳
     */
    private val LAST_UPLOAD_TIMESTAMP = "last_upload_timestamp"
    /**
     * 一天的毫秒数
     */
    private val ONE_DAY_MILLSECONDS = 1000 * 60 * 60 * 24

    public fun tryUploadAppInfo() {
        val lastUploadTimestamp = PreferenceUtils.getInstance().getLongValue(LAST_UPLOAD_TIMESTAMP, 0)
        if ((System.currentTimeMillis() - lastUploadTimestamp) < ONE_DAY_MILLSECONDS) {
            return
        }
        requestSwitch()
    }

    /**
     * 请求开关
     */
    @SuppressLint("CheckResult")
    private fun requestSwitch() {
//        BaseRetrofit
//                .getTrackInstance()
//                .create(IAppInstallService::class.java)
//                .requestSwitch()
//                .subscribe({
//                    if (it.body.appInstall) {
//                        uploadAppInstallInfo()
//                    }
//                }, {
//
//                })
    }


    /**
     * 上报用户已安装的app信息
     */
    @SuppressLint("CheckResult")
    private fun uploadAppInstallInfo() {
        val list = ApkToolUtils.getInstalledAppList(BaseApplication.instance.packageManager)

        if (list.isEmpty()) {
            return
        }

        // 缓存上次上报的时间戳
        PreferenceUtils.getInstance().setLongValue(LAST_UPLOAD_TIMESTAMP, System.currentTimeMillis())

        val data = HashMap<String, List<String>>()
        val encryptList = ArrayList<String>()

        // 针对每一条数据进行加密处理
        list.forEach {
            try {
                encryptList.add(AESEncryptUtil.encrypt(encode(Gson().toJson(it))))
            } catch (e: Throwable) {
                e.log()
            }
        }

        data["app_list"] = encryptList
//        BaseRetrofit
//                .getTrackInstance()
//                .create(IAppInstallService::class.java)
//                .uploadAppInstall(data)
//                .subscribe({
//
//                }, {
//
//                })
    }
}
