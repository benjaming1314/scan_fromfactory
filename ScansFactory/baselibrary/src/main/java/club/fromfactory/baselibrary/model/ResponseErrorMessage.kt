package club.fromfactory.baselibrary.model

data class ResponseErrorMessage(
        val errorCode : String ?,
        val message : String ?,
        val displayMessage : String ?
):NoProguard