package club.fromfactory.baselibrary.statistic

import club.fromfactory.baselibrary.BuildConfig
import club.fromfactory.baselibrary.statistic.constants.*
import club.fromfactory.baselibrary.statistic.pages.*
import club.fromfactory.baselibrary.view.IBaseView
import com.blankj.utilcode.util.LogUtils
import java.util.*

/**
 *@author lxm
 *@date 2018/7/26/026
 */
class PerformanceStatisticsUtils {

    class StartUpData {

        private var startTime = -1L
        private var endTime = -1L

        @JvmOverloads
        fun setStartTime(startTime: Long = System.currentTimeMillis()): StartUpData {
            PerformanceStatisticsUtils().log("setStartTime")
            this.startTime = startTime
            return this@StartUpData
        }

        @JvmOverloads
        fun setEndTime(endTime: Long = System.currentTimeMillis()) {
            PerformanceStatisticsUtils().log("setEndTime")
            this.endTime = endTime
            addEvent()
        }

        private fun addEvent() {
            val event = Hashtable<String, Any>()
            event[EVENT_TYPE] = StatEventType.PERFORMANCE
            event[MODULE_ID] = "1"
            event[START_TIME] = startTime
            event[END_TIME] = endTime
            StatEventManager.addStatEvent(event)
        }
    }

    class PerformancePageData(private val baseView: IBaseView) : PagePerformanceRecorder {
        private var nativeStartTime = -1L
        private var nativeEndTime = -1L
        private var networkStartTime = -1L
        private var networkEndTime = -1L
        private var webviewStartTime = -1L
        private var webviewEndTime = -1L
        private var pageRenderStartTime = -1L
        private var pageRenderEndTime = -1L
        private var isAlreadySendStat = false

        fun setNativeStartTime(): PerformancePageData {
            this.nativeStartTime = System.currentTimeMillis()
            if (BuildConfig.DEBUG) {
                PerformanceStatisticsUtils().log("setNativeStartTime")
            }
            return this@PerformancePageData
        }


        fun setNativeEndTime(): PerformancePageData {
            this.nativeEndTime = System.currentTimeMillis()
            if (BuildConfig.DEBUG) {
                PerformanceStatisticsUtils().log("setNativeEndTime")
            }
            return this@PerformancePageData
        }

        override fun setNetworkStartTime() {
            this.networkStartTime = System.currentTimeMillis()
            if (BuildConfig.DEBUG) {
                PerformanceStatisticsUtils().log("setNetworkStartTime")
            }
        }

        override fun setNetworkEndTimed() {
            val currentTimeMillis = System.currentTimeMillis()
            this.networkEndTime = currentTimeMillis
            pageRenderStartTime = currentTimeMillis
            if (BuildConfig.DEBUG) {
                PerformanceStatisticsUtils().log("setNetworkEndTimed")
            }
        }


        override fun setWebViewStartTime() {
            this.webviewStartTime = System.currentTimeMillis()
            if (BuildConfig.DEBUG) {
                PerformanceStatisticsUtils().log("setWebViewStartTime")
            }
        }

        override fun setWebViewEndTime() {
            this.webviewEndTime = System.currentTimeMillis()
            if (BuildConfig.DEBUG) {
                PerformanceStatisticsUtils().log("setWebvViewEndTime")
            }
        }

        override fun setPageRenderStartTime() {
            this.pageRenderStartTime = System.currentTimeMillis()
            if (BuildConfig.DEBUG) {
                PerformanceStatisticsUtils().log("setPageRenderStartTime")
            }
        }

        override fun setPageRenderEndTime() {
            this.pageRenderEndTime = System.currentTimeMillis()
            if (BuildConfig.DEBUG) {
                PerformanceStatisticsUtils().log("setPageRenderEndTime")
            }
            addEvent()
        }

        /**
         * 添加事件
         */
        private fun addEvent() {
            if (isAlreadySendStat) {
                return
            }

            if (baseView.pageId == 0) {
                if (BuildConfig.DEBUG) {
                    LogUtils.w(IllegalArgumentException("page id not valid"))
                } else {
//                    Crashlytics.logException(IllegalArgumentException("page id not valid "))
                    return
                }
            }

            isAlreadySendStat = true
            val event = Hashtable<String, Any>()
            event[EVENT_TYPE] = StatEventType.PERFORMANCE
            event[MODULE_ID] = "1.${baseView.pageId}"
            event[NATIVE_START_TIME] = nativeStartTime
            event[NATIVE_END_TIME] = nativeEndTime
            event[NETWORK_START_TIME] = networkStartTime
            event[NETWORK_END_TIME] = networkEndTime
            event[WEBVIEW_START_TIME] = webviewStartTime
            event[WEBVIEW_END_TIME] = webviewEndTime
            event[PAGE_RENDER_START_TIME] = pageRenderStartTime
            event[PAGE_RENDER_END_TIME] = pageRenderEndTime

            if (networkStartTime == -1L && networkEndTime == -1L) {
                removeNetworkTime(event)
            }

            if (webviewStartTime == -1L && webviewEndTime == -1L) {
                removeWebViewTime(event)
            }

            // 性能统计网络时间必须成对出现，否则报错
            if (networkStartTime == -1L && networkEndTime > -1 || networkStartTime > -1 && networkEndTime == -1L) {
                removeNetworkTime(event)
                if (BuildConfig.DEBUG) {
                    LogUtils.e(StatEventManager.TAG, "network time is not available, networkStartTime $networkStartTime networkEndTime $networkEndTime ")
                }
            }

            // 性能统计webView时间必须成对出现，否则报错
            if (webviewStartTime == -1L && webviewEndTime > -1 || webviewStartTime > -1 && webviewEndTime == -1L) {
                removeWebViewTime(event)
                if (BuildConfig.DEBUG) {
                    LogUtils.e(StatEventManager.TAG, "webView time is not available, webviewStartTime $webviewStartTime webviewEndTime $webviewEndTime ")
                }
            }

            // pageRenderStartTime 也有可能没有设置
            if (pageRenderStartTime == -1L) {
                event.remove(PAGE_RENDER_START_TIME)
                if (BuildConfig.DEBUG) {
                    LogUtils.e(StatEventManager.TAG, "pageRenderStartTime time is not available, pageRenderStartTime $pageRenderStartTime")
                }
            }
            StatEventManager.addStatEvent(event, baseView)
        }

        private fun removeNetworkTime(event: Hashtable<String, Any>) {
            event.remove(NETWORK_START_TIME)
            event.remove(NETWORK_END_TIME)
        }

        private fun removeWebViewTime(event: Hashtable<String, Any>) {
            event.remove(WEBVIEW_START_TIME)
            event.remove(WEBVIEW_END_TIME)
        }
    }


    private fun log(method: String) {

    }
}