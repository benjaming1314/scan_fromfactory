package club.fromfactory.baselibrary.view

import club.fromfactory.baselibrary.presenter.IPresenter

/**
 * MVP模式的DialogFragment基类
 *
 * @author Jellybean
 * @date 2018/6/26
 */
abstract class BaseMVPDialogFragment<P : IPresenter<*>> : BaseDialogFragment(), IMVPView<P> {
    lateinit var aPresenter: P
    override fun initData() {
        super.initData()
        aPresenter = createPresenter()
    }

    override fun getPresenter(): P {
        return aPresenter
    }

    override fun onDestroy() {
        super.onDestroy()
        if (this::aPresenter.isInitialized) {
            presenter.unsubscribe()
        }
    }


}