package club.fromfactory.baselibrary.widget.recyclerview;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 复杂的数据类型列表Adapter，这里没有Header,Footer的概念，所有的item都对应一个ViewHolder
 * Created by lxm on 2017/9/6.
 */

public class MultiTypeAdapter extends RecyclerAdapter {
    private final String TAG = "MultiTypeAdapter";
    private ViewHolderManager mViewHolderManager;
    /**
     * 元素的viewType列表
     */
    private final List<Integer> mViewTypeList = new ArrayList<>();

    public MultiTypeAdapter(Context context) {
        super(context);
        mViewHolderManager = new ViewHolderManager();
    }

    /**
     * 添加单个数据
     *
     * @param viewHolder 需要显示的ViewHolder
     * @param data       数据类型
     * @param <T>        和ViewHolder绑定的数据类型
     */
    public <T> void add(Class<? extends BaseViewHolder<T>> viewHolder, T data) {
        add(viewHolder, data, true);
    }

    /**
     * 添加数据
     *
     * @param viewHolder   需要展示的viewHolder
     * @param data         绑定的数据
     * @param isNeedNotify 是否需要notify
     * @param <T>
     */
    public <T> void add(Class<? extends BaseViewHolder<T>> viewHolder, T data, boolean
            isNeedNotify) {
        if (isShowNoMore) {
            return;
        }
        mData.add(data);
        mViewHolderManager.addViewHolder(viewHolder);
        int viewType = mViewHolderManager.getViewType(viewHolder);
        mViewTypeList.add(viewType);
        int positionStart = mViewCount - 1;
        mViewCount++;
        if (isNeedNotify) {
            notifyItemChangeInserted(positionStart, 1);
        }

    }

    public <T> void addAll(Class<? extends BaseViewHolder<T>> viewHolder, T[] data) {
        addAll(viewHolder, Arrays.asList(data));
    }

    public <T> void addAll(Class<? extends BaseViewHolder<T>> viewHolder, List<T> data) {
        final int size = data.size();
        if (isShowNoMore || size == 0) {
            return;
        }
        mData.addAll(data);
        mViewHolderManager.addViewHolder(viewHolder);
        int viewType = mViewHolderManager.getViewType(viewHolder);
        int positionStart = mViewCount - 1;
        for (int i = 0; i < size; i++) {
            mViewTypeList.add(viewType);
            mViewCount++;
        }
        notifyItemChangeInserted(positionStart, size);
    }

    private void notifyItemChangeInserted(final int start, final int size) {
        try {
            notifyItemRangeInserted(start, size);
        } catch (Exception e) {
            e.printStackTrace();
            log("notifyItemChangeInserted Exception " + e.getMessage());
        }
    }

    @Override
    public void remove(int itemPosition) {
        if (itemPosition > -1 && itemPosition < mViewTypeList.size()) {
            mViewTypeList.remove(itemPosition);
        }
        super.remove(itemPosition);
    }

    /**
     * 清空所有数据
     */
    public void clear() {
        if (mData == null) {
            log("clear() mData is null");
            return;
        }
        mData.clear();
        mViewTypeList.clear();
        mViewCount = 1;
        isShowNoMore = false;
        mLoadMoreView.setVisibility(View.GONE);
        mNoMoreView.setVisibility(View.GONE);
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == mViewCount - 1) {
            return STATUS_TYPE;
        }

        return mViewTypeList.get(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        log("onCreateViewHolder -- viewType : " + viewType);
        if (viewType == STATUS_TYPE) {
            return new BaseViewHolder(mStatusView);
        }
        Class clazzViewHolder = mViewHolderManager.getViewHolder(viewType);
        try {
            //这里只适配了ViewHolder构造函数只有ViewGroup.class参数或者无参情况的构造函数
            BaseViewHolder holder;
            Constructor constructor = clazzViewHolder.getDeclaredConstructor(ViewGroup.class);
            constructor.setAccessible(true);
            holder = (BaseViewHolder) constructor.newInstance(new Object[]{parent});
            if (holder == null) {
                constructor = clazzViewHolder.getDeclaredConstructor();
                holder = (BaseViewHolder) constructor.newInstance();
            }

            if (mRecyclerItemViewClickListener != null) {
                holder.setOnItemViewClickListener(mRecyclerItemViewClickListener);
            }
            return holder;
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            Log.i(TAG, "onCreateBaseViewHolder : " + e.getMessage());
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            Log.i(TAG, "onCreateBaseViewHolder : " + e.getMessage());
        } catch (InstantiationException e) {
            e.printStackTrace();
            Log.i(TAG, "onCreateBaseViewHolder : " + e.getMessage());
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            Log.i(TAG, "onCreateBaseViewHolder : " + e.getMessage());
        }
        return null;

    }

    @Override
    public BaseViewHolder onCreateBaseViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        log("onBindViewHolder -- position : " + position);
        if (position == 0 && mViewCount == 1) {

        } else if (position == mViewCount - 1) {
            // 显示加载更多
            if (loadMoreAble && mLoadMoreAction != null && !isShowNoMore) {
                openLoadMore();
                mLoadMoreAction.onAction();
            }
        } else {
            holder.setData(mData.get(position), position);
        }
    }

    public List getList() {
        return mData;
    }
}
