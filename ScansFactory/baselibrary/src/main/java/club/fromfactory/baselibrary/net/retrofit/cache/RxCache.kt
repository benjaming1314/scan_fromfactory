package club.fromfactory.baselibrary.net.retrofit.cache

import android.content.Context
import club.fromfactory.baselibrary.BaseApplication
import club.fromfactory.baselibrary.net.retrofit.cache.converter.GsonConverter
import club.fromfactory.baselibrary.net.retrofit.cache.converter.IConverter
import club.fromfactory.baselibrary.net.retrofit.cache.core.ICache
import club.fromfactory.baselibrary.net.retrofit.cache.core.INVALID_TIME
import club.fromfactory.baselibrary.net.retrofit.cache.core.LruDiskCache
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.ObservableOnSubscribe
import java.io.File
import java.lang.reflect.Type
import java.util.concurrent.TimeUnit

/**
 * 缓存统一的入口类
 *
 * @author nichenjian
 * @date
 */
val defaultCacheDir = File(BaseApplication.instance.cacheDir, "http_response")
class RxCache(builder: Builder) {
    /**
     * 转换器
     */
    private val converter: IConverter = GsonConverter
    /**
     * 缓存大小
     */
    private val cacheSize: Long
    /**
     * 缓存时间(-1表示永久缓存)
     */
    private val cacheTime: Long
    /**
     * 缓存的版本
     */
    private val appVersion: Int
    /**
     * 上下文
     */
    private val context: Context
    /**
     * 缓存目录
     */
    private val diskDir: File
    /**
     * 缓存的时间类型
     */
    private val timeUnit: TimeUnit
    private val cache: ICache

    init {
        cacheSize = builder.getCacheSize()
        appVersion = builder.getAppVersion()
        cacheTime = builder.getCacheTime()
        context = builder.getContext()
        timeUnit = builder.getTimeUnit()
        diskDir = builder.getDiskDir()
        cache = LruDiskCache(converter, diskDir, appVersion, cacheSize)
    }

    class Builder {
        /**
         * 缓存大小限制，默认5M
         */
        private var cacheSize: Long = 5 * 1024 * 1024
        /**
         * 缓存时间
         */
        private var cacheTime: Long = -1
        /**
         * 缓存版本号
         */
        private var appVersion: Int = 1
        /**
         * 时间格式
         */
        private var timeUnit = TimeUnit.SECONDS
        /**
         * 缓存目录
         */
        private var diskDir = defaultCacheDir
        /**
         * 上下文
         */
        private var context: Context = BaseApplication.instance

        fun setCacheSize(cacheSize: Long): Builder {
            this.cacheSize = cacheSize
            return this@Builder
        }

        fun setCacheTime(cacheTime: Long): Builder {
            this.cacheTime = cacheTime
            return this@Builder
        }

        fun setAppVersion(appVersion: Int): Builder {
            this.appVersion = appVersion
            return this@Builder
        }

        fun setTimeUnit(timeUnit: TimeUnit): Builder {
            this.timeUnit = timeUnit
            return this@Builder
        }

        fun setContext(context: Context): Builder {
            this.context = context
            return this@Builder
        }

        fun setDiskDir(diskDir: File): Builder {
            this.diskDir = diskDir
            return this@Builder
        }


        fun getCacheSize(): Long {
            return this.cacheSize
        }

        fun getCacheTime(): Long {
            return this.cacheTime
        }

        fun getAppVersion(): Int {
            return this.appVersion
        }

        fun getTimeUnit(): TimeUnit {
            return this.timeUnit
        }

        fun getContext(): Context {
            return this.context
        }

        fun getDiskDir(): File {
            return this.diskDir
        }

        fun build(): RxCache {
            if (!this.diskDir.exists()) {
                this.diskDir.mkdirs()
            }
            return RxCache(this)
        }
    }

    /**
     * 读取缓存
     *
     * @param type 类型
     * @param cacheKey 缓存Key
     */
    fun <T> load(type: Type, cacheKey: String): Observable<T?> {
        return load(type, cacheKey, INVALID_TIME, TimeUnit.SECONDS)
    }

    /**
     * 读取缓存
     *
     * @param type 类型
     * @param cacheKey 缓存的key
     * @param cacheTime 缓存时间
     */
    fun <T> load(type: Type, cacheKey: String, cacheTime: Long, timeUnit: TimeUnit): Observable<T?> {
        return Observable.create(object : SimpleSubscribe<T>() {
            override fun execute(): T? {
                return cache.load<T>(type, cacheKey, cacheTime, timeUnit)
            }
        })
    }

    /**
     * 保存数据
     *
     * @param cacheKey 缓存的key
     * @param value 缓存的对象
     */
    fun <T> save(cacheKey: String, value: T): Observable<Boolean> {
        return Observable.create(object : SimpleSubscribe<Boolean>() {
            override fun execute(): Boolean? {
                return cache.save(cacheKey, value)
            }
        })
    }

    /**
     * 删除数据
     *
     * @param cacheKey 缓存key
     */
    fun remove(cacheKey: String): Observable<Boolean> {
        return Observable.create(object : SimpleSubscribe<Boolean>() {
            override fun execute(): Boolean? {
                return cache.remove(cacheKey)
            }
        })
    }

    /**
     * 清空缓存
     */
    fun clear(): Observable<Boolean> {
        return Observable.create(object : SimpleSubscribe<Boolean>() {
            override fun execute(): Boolean? {
                return cache.clear()
            }
        })
    }

    abstract class SimpleSubscribe<T> : ObservableOnSubscribe<T> {
        override fun subscribe(emitter: ObservableEmitter<T>) {
            try {
                val data = execute()
                if (!emitter.isDisposed && data != null) {
                    emitter.onNext(data)
                }
            } catch (e: Throwable) {
                e.printStackTrace()
                if (!emitter.isDisposed) {
                    emitter.onError(e)
                }
                return
            }

            if (!emitter.isDisposed) {
                emitter.onComplete()
            }
        }

        @Throws(Throwable::class)
        abstract fun execute(): T?
    }
}
