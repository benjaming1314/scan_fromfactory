package club.fromfactory.baselibrary.statistic.exceptions

/**
 * evnet invalid exception
 * Created by nichenjian on 2018/5/21.
 */
class InvaildEventException : RuntimeException {
    constructor() : super("Stat event is not valid, Please check again");
}