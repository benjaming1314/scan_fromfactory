package club.fromfactory.baselibrary.utils;

import android.os.Looper;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import java.util.concurrent.TimeUnit;

/**
 * 线程辅助类
 *
 * @author nichenjian
 * @date 2018/6/24
 */
public class ThreadUtils {
    /**
     * 主线程执行
     *
     * @param runnable 任务
     */
    public static void runOnUi(Runnable runnable) {
        runOnUi(runnable, 0L, TimeUnit.MILLISECONDS);
    }

    /**
     * 主线程执行
     *
     * @param runnable 任务
     * @param delay    延迟时间
     * @param timeUnit 时间类型
     */
    public static void runOnUi(Runnable runnable, Long delay, TimeUnit timeUnit) {
        if (delay <= 0 && Looper.myLooper() == Looper.getMainLooper()) {
            runnable.run();
        } else {
            AndroidSchedulers.mainThread().scheduleDirect(runnable, delay, timeUnit);
        }
    }

    /**
     * 在子线程执行
     *
     * @param runnable 任务
     */
    public static void runOnIO(Runnable runnable) {
        runOnIO(runnable, 0L, TimeUnit.MILLISECONDS);
    }

    /**
     * 在子线程执行
     *
     * @param runnable 任务
     * @param delay    延迟时间
     * @param timeUnit 时间类型
     */
    public static void runOnIO(Runnable runnable, Long delay, TimeUnit timeUnit) {
        Schedulers.io().scheduleDirect(runnable, delay, timeUnit);
    }
}
