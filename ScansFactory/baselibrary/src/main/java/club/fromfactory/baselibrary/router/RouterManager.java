package club.fromfactory.baselibrary.router;

import static club.fromfactory.baselibrary.utils.UriEncodeUtilsKt.decode;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import club.fromfactory.baselibrary.utils.AppLinkUtils;
import club.fromfactory.baselibrary.utils.ExceptionUtilsKt;
import club.fromfactory.baselibrary.view.IBaseView;
import club.fromfactory.router.IRouter;
import club.fromfactory.router.IRouterCallback;
import club.fromfactory.router.IRouterMap;
import club.fromfactory.router.PageInfo;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * manager routers
 */
public class RouterManager {

    private static final HashMap<String, PageInfo> ROUTER_MAP = new HashMap<>();
    public static final String DEEP_LINK_SCHEME = "scanclubfactory";
    public static final String ROUTER_PAGE_NAME = "router_page_name";
    /**
     * 路由拦截器
     */
    private static final List<BaseRouterInterceptor> routerInterceptors = new ArrayList<>();

    public static void init() {
        if (ROUTER_MAP.isEmpty()) {
            ROUTER_MAP.putAll(getRouterMap());
        }
    }

    private static HashMap<String, PageInfo> getRouterMap() {
        HashMap<String, PageInfo> routers = new HashMap<>();
        try {
            String routerMapName = "club.fromfactory.router.RouterMap";
            Class<? extends IRouterMap> routerMapClass = (Class<? extends IRouterMap>) Class
                    .forName(routerMapName);
            IRouterMap routerMap = routerMapClass.newInstance();
            routers = routerMap.init();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            ExceptionUtilsKt.logException(e);
        } catch (InstantiationException e) {
            ExceptionUtilsKt.logException(e);
        } catch (NoClassDefFoundError e) {
            // fix https://fabric.io/jiayunshuju/android/apps/club.fromfactory/issues/5bc8a0c9f8b88c2963470fb1
            ExceptionUtilsKt.logException(e);
        }

        return routers;
    }

    /**
     * open uri
     *
     * @return if can handle the uri, then return true, otherwise return false
     */
    public static boolean open(Context context, String uri) {
        if (context == null || TextUtils.isEmpty(uri)) {
            return false;
        }

        return open(context, Uri.parse(uri), null);
    }

    /**
     * open uri
     *
     * @return if can handle the uri, then return true, otherwise return false
     */
    public static boolean open(Context context, String uri, IBaseView baseView) {
        if (context == null || TextUtils.isEmpty(uri)) {
            return false;
        }

        return open(context, Uri.parse(uri), null);
    }

    /**
     * open uri
     *
     * @return if can handle the uri, then return true, otherwise return false
     */
    public static boolean open(Context context, String uri, int requestCode) {
        if (context == null || TextUtils.isEmpty(uri)) {
            return false;
        }

        return open(context, Uri.parse(uri), requestCode, null);
    }

    /**
     * open uri
     *
     * @return if can handle the uri, then return true, otherwise return false
     */
    public static boolean open(Context context, String uri, IRouterCallback routerCallback) {
        if (context == null || TextUtils.isEmpty(uri)) {
            return false;
        }

        return open(context, Uri.parse(uri), routerCallback);
    }

    /**
     * open uri
     */
    public static boolean open(Context context, String uri, int requestCode,
            IRouterCallback routerCallback) {
        if (context == null || TextUtils.isEmpty(uri)) {
            return false;
        }

        return open(context, Uri.parse(uri), requestCode, routerCallback);
    }

    /**
     * open uri
     *
     * @return if can handle the uri, then return true, otherwise return false
     */
    public static boolean open(Context context, Uri uri) {
        if (context == null || uri == null) {
            return false;
        }

        return open(context, uri, null);
    }


    /**
     * open uri
     */
    public static boolean open(Context context, Uri uri, IRouterCallback routerCallback) {
        return open(context, uri, -1, routerCallback);
    }

    /**
     * 添加路由拦截器
     */
    public static void addRouterInterceptor(BaseRouterInterceptor baseRouter) {
        if (!routerInterceptors.contains(baseRouter)) {
            routerInterceptors.add(baseRouter);
        }
    }

    /**
     * 移除拦截器
     */
    public static void removeRouterInterceptor(BaseRouterInterceptor baseRouter) {
        if (routerInterceptors.contains(baseRouter)) {
            routerInterceptors.remove(baseRouter);
        }
    }

    /**
     * open uri
     *
     * @return if can handle the uri, then return true, otherwise return false
     */
    public static boolean open(Context context, Uri uri, int requestCode,
            IRouterCallback routerCallback) {
        if (context == null || uri == null) {
            return false;
        }

        if (routerCallback != null) {
            routerCallback.beforeOpenRouter(context, uri);
        }

        if (isHandledByInterceptors(context, uri, routerCallback)) {
            return true;
        }

        boolean isHandled = false;

        String scheme = uri.getScheme();
        if (!DEEP_LINK_SCHEME.equals(scheme) && !AppLinkUtils.INSTANCE.isAppLink(uri)) {
            return false;
        }

        Intent intent = parseUriToIntent(context, uri);
        if (intent != null) {
            isHandled = true;
            if (!(context instanceof Activity)) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }

            if (context instanceof Activity && requestCode != -1) {
                ((Activity) context).startActivityForResult(intent, requestCode);
            } else {
                context.startActivity(intent);
            }
        }

        if (routerCallback != null) {
            routerCallback.afterOpenRouter(context, uri);
        }

        return isHandled;
    }

    /**
     * 尝试用路由拦截器打开
     */
    public static boolean tryOpenWithRouterInterceptors(Context context, Uri uri) {
        return isHandledByInterceptors(context, uri, null);
    }

    private static boolean isHandledByInterceptors(Context context, Uri uri,
            IRouterCallback routerCallback) {
        Boolean isHandled = false;
        try {
            String path = uri.getPath();
            for (BaseRouterInterceptor routerInterceptor : routerInterceptors) {
                if (routerInterceptor.handle(context, uri, path)) {
                    isHandled = true;
                    break;
                }
            }

            if (routerCallback != null) {
                routerCallback.afterOpenRouter(context, uri);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return isHandled;
    }

    /**
     * 将uri解析成intent
     *
     * @param context 上下文
     * @param uri 需要打开的url
     */
    public static Intent parseUriToIntent(Context context, Uri uri) {
        String path = uri.getPath();
        Intent intent = null;
        for (Map.Entry<String, PageInfo> entry : ROUTER_MAP.entrySet()) {
            if (entry != null && path.equals(entry.getKey())) {
                PageInfo pageInfo = entry.getValue();
                intent = new Intent(context, pageInfo.getActivityClass());
                Set<String> queryNames = uri.getQueryParameterNames();
                if (queryNames != null) {
                    for (String queryName : queryNames) {
                        String queryValue = decode(uri.getQueryParameter(queryName));
                        if (pageInfo.getParams().containsKey(queryName)) {
                            Class className = pageInfo.getParams().get(queryName);
                            setIntentPutExtra(intent, queryName, queryValue, className);
                        } else {
                            intent.putExtra(queryName, queryValue);
                        }
                    }

                    // bundle 中添加 router_page_name
                    if (!TextUtils.isEmpty(path)) {
                        String pageName = path.substring(1);
                        intent.putExtra(ROUTER_PAGE_NAME, pageName);
                    }
                }

                break;
            }
        }

        return intent;
    }

    private static void setIntentPutExtra(Intent intent, String queryName, String queryValue,
            Class className) {
        try {
            if (className == Integer.class) {
                intent.putExtra(queryName, Integer.valueOf(queryValue));
            } else if (className == Long.class) {
                intent.putExtra(queryName, Long.valueOf(queryValue));
            } else if (className == Boolean.class) {
                intent.putExtra(queryName, Boolean.valueOf(queryValue));
            } else if (className == Double.class) {
                intent.putExtra(queryName, Double.valueOf(queryValue));
            } else if (className == Short.class) {
                intent.putExtra(queryName, Short.valueOf(queryValue));
            } else if (className == Float.class) {
                intent.putExtra(queryName, Float.valueOf(queryValue));
            } else if (className == Byte.class) {
                intent.putExtra(queryName, Byte.valueOf(queryValue));
            } else if (className == Character.class) {
                intent.putExtra(queryName, Character.valueOf(queryValue.charAt(0)));
            } else {
                intent.putExtra(queryName, queryValue);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void bind(Object routerObj, Bundle bundle) {
        try {
            Class<?> routerClass = Class.forName(routerObj.getClass().getName() + "$$Router");
            IRouter router = (IRouter) routerClass.newInstance();
            router.bind(routerObj, bundle);
        } catch (ClassNotFoundException e) {
            // no-op
        } catch (InstantiationException e) {
            ExceptionUtilsKt.logException(e);
        } catch (IllegalAccessException e) {
            ExceptionUtilsKt.logException(e);
        }
    }

    /**
     * cast操作
     *
     * @param obj cast的对象
     */
    public static <T> T castValue(Object obj) throws ClassCastException {
        return (T) obj;
    }
}
