package club.fromfactory.baselibrary.net.retrofit

import club.fromfactory.baselibrary.BaseApplication
import club.fromfactory.baselibrary.BuildConfig
import club.fromfactory.baselibrary.net.NetUtils
import com.ihsanbal.logging.Level
import com.ihsanbal.logging.LoggingInterceptor
import io.reactivex.schedulers.Schedulers
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit

/**
 * Retrofit 网络请求单例
 */
object BaseRetrofit {

    private val services: HashMap<Class<*>, Any> = HashMap()
    private var instance: Retrofit? = null
    private var trackInstance: Retrofit? = null
    /**
     * 设置缓存大小2M
     */
    private const val CACHE_SIZE_BYTES = 1024 * 1024 * 2L
    /**
     * 缓存目录
     */
    private const val CACHE_DIR_NAME = "okhttp"

    fun getInstance(): Retrofit {
        if (instance == null) {
            instance = Retrofit.Builder()
                    .client(client)
                    .baseUrl(NetUtils.APP_MAIN_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxMainThreadCallAdapterFactory())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                    .build()
        }

        return instance!!
    }

    /**
     * 获取自定义的retrofit实例
     *
     * @param client 自定义的OkHttpClient
     * @param serverPath 请求的服务端地址
     */
    fun getCustomInstance(client: OkHttpClient = BaseRetrofit.client, serverPath: String ): Retrofit {
        return Retrofit.Builder()
                .client(client)
                .baseUrl(serverPath)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxMainThreadCallAdapterFactory())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build()
    }

    /**
     * 利用默认的Retrofit实例[getInstance]  生成服务
     */
    fun <T> createService(serviceClass: Class<T>): T {
        var service = services[serviceClass]
        if (service == null) {
            service = getInstance().create(serviceClass)
            if (service != null) {
                services[serviceClass] = service
            }
        }
        return service as T

    }
//
//    fun getTrackInstance(): Retrofit {
//        if (trackInstance == null) {
//            trackInstance = Retrofit.Builder()
//                    .client(client)
//                    .baseUrl(BuildConfig.TRACK_SERVER_PATH)
//                    .addConverterFactory(GsonConverterFactory.create())
//                    .addCallAdapterFactory(RxMainThreadCallAdapterFactory())
//                    .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
//                    .build()
//        }
//
//        return trackInstance!!
//    }

    private val client: OkHttpClient
        get() = OkHttpClient.Builder()
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .cache(Cache(File(BaseApplication.instance.cacheDir.absolutePath + "/" + CACHE_DIR_NAME), CACHE_SIZE_BYTES))
                .addInterceptor(LoggingInterceptor.Builder()
                        .loggable(BuildConfig.DEBUG)
                        .setLevel(Level.BODY)
                        .build())
                .addInterceptor(BaseInterceptor()).build()
}