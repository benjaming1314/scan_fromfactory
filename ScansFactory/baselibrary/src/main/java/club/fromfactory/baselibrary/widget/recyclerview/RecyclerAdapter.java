package club.fromfactory.baselibrary.widget.recyclerview;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import club.fromfactory.baselibrary.R;
import club.fromfactory.baselibrary.utils.BaseLog;
import club.fromfactory.baselibrary.utils.StringUtils;

/**
 * Created by lxm on 2017/9/6..
 */
public abstract class RecyclerAdapter<T> extends RecyclerView.Adapter<BaseViewHolder<T>> {

    private static final String TAG = "RecyclerAdapter";

    private static final int HEADER_TYPE = 111;
    private static final int FOOTER_TYPE = 222;
    protected static final int STATUS_TYPE = 333;
    protected int mViewCount = 0;

    private boolean hasHeader = false;
    private boolean hasFooter = false;
    protected boolean isShowNoMore = false;   //停止加载
    protected boolean loadMoreAble = false;   //是否可加载更多

    protected RecyclerAction mLoadMoreAction;

    protected List<T> mData = new ArrayList<>();

    private View headerView;
    private View footerView;
    protected View mStatusView;
    protected View mLoadMoreView;
    protected TextView mTxtReloading;
    public TextView mNoMoreView;

    private Context mContext;

    protected RecyclerItemViewClickListener<T> mRecyclerItemViewClickListener;

    public RecyclerAdapter(Context context) {
        mContext = context;
        initStatusView(context);
    }

    public RecyclerAdapter(Context context, T[] data) {
        this(context, Arrays.asList(data));
    }

    public RecyclerAdapter(Context context, List<T> data) {
        mContext = context;
        initStatusView(context);
        this.mData = data;
        mViewCount += data.size();
        notifyDataSetChanged();
    }

    public void initStatusView(Context context) {
        mStatusView = LayoutInflater.from(context).inflate(R.layout.view_status_last, null);
        mStatusView.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams
                .MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        mLoadMoreView = mStatusView.findViewById(R.id.load_more_view);
        mTxtReloading = mStatusView.findViewById(R.id.txt_reloading);
        mNoMoreView = mStatusView.findViewById(R.id.no_more_view);
        mViewCount++;

        mTxtReloading.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mRecyclerItemViewClickListener != null) {
                    reloadingMore();
                    mRecyclerItemViewClickListener.reloadingMore();
                }
            }
        });

    }

    @Override
    public BaseViewHolder<T> onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == HEADER_TYPE) {
            return new BaseViewHolder<>(headerView);
        } else if (viewType == FOOTER_TYPE) {
            return new BaseViewHolder<>(footerView);
        } else if (viewType == STATUS_TYPE) {
            return new BaseViewHolder<>(mStatusView);
        } else {
            BaseViewHolder<T> baseViewHolder = onCreateBaseViewHolder(parent, viewType);
            if (mRecyclerItemViewClickListener != null) {
                baseViewHolder.setOnItemViewClickListener(mRecyclerItemViewClickListener);
            }
            return baseViewHolder;

        }
    }

    /**
     * /**
     * 将RecyclerView的网格布局中的某个item设置为独占一行、只占一列，只占两列、等等
     *
     * @param gridLayoutManager
     */
    public void setGridSpanSizeLookup(final GridLayoutManager gridLayoutManager) {
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {

                int type = getItemViewType(position);
                if (type == STATUS_TYPE) {
                    return gridLayoutManager.getSpanCount();//只占一行中的一列
                } else {
                    return 1;//只占一行中的一列，
                }
            }
        });
    }

    public abstract BaseViewHolder<T> onCreateBaseViewHolder(ViewGroup parent, int viewType);

    /* ViewHolder 绑定数据，这里的 position 和 getItemViewType() 方法的 position 不一样
        这里的 position 指当前可见的 item 的 position 的位置。
        注意 ：每个 ViewHolder 绑定数据时值调用此方法一次
     */
    @Override
    public void onBindViewHolder(BaseViewHolder<T> holder, int position) {
        log("onBindViewHolder()  viewCount : " + mViewCount + " position : " + position);
        if (position == 0) {
            // 最先加载 mStatusView 时不需要绑定数据
            if (mViewCount == 1 || hasHeader) {
                return;
            } else {
                holder.setData(mData.get(0), position);
            }
        } else if (!hasHeader && !hasFooter && position < mData.size()) { //没有Header和Footer
            holder.setData(mData.get(position), position);
        } else if (hasHeader && !hasFooter && position > 0 && position < mViewCount - 1) {
            //有Header没有Footer
            holder.setData(mData.get(position - 1), position);
        } else if (!hasHeader && position < mViewCount - 2) { //没有Header，有Footer
            holder.setData(mData.get(position), position);
        } else if (position > 0 && position < mViewCount - 2) { //都有
            holder.setData(mData.get(position - 1), position);
        }

        // 最后一个可见的 item 时 加载更多。解决 remove 时 bug
        int positionEnd;
        if ((hasHeader && hasFooter) || (!hasHeader && hasFooter)) {
            positionEnd = mViewCount - 3;
        } else {
            positionEnd = mViewCount - 2;
        }
        if (loadMoreAble && !isShowNoMore && position == positionEnd) {
            openLoadMore();
            if (mLoadMoreAction != null) {
                mLoadMoreAction.onAction();
            }
        }
    }

    /**
     * ViewHolder 更新 Item 的位置选择 ViewType , 和 UI 是同步的
     */
    @Override
    public int getItemViewType(int position) {
        if (hasHeader && position == 0) {   //header
            return HEADER_TYPE;
        }
        if (hasFooter && position == mViewCount - 2) { //footer
            return FOOTER_TYPE;
        }
        if (position == mViewCount - 1) {  //最后View的状态
            return STATUS_TYPE;
        }

        return super.getItemViewType(position);
    }

    /**
     * 包含了 Header , Footer , 状态显示 Item
     */
    @Override
    public int getItemCount() {
        return mViewCount;
    }

    public void showNoMore(final String noMoreStr) {
        isShowNoMore = true;
        mLoadMoreView.post(new Runnable() {
            @Override
            public void run() {
                mLoadMoreView.setVisibility(View.GONE);
                if (StringUtils.isNotBlank(noMoreStr)) {
                    mNoMoreView.setText(noMoreStr);
                }
                mNoMoreView.setVisibility(View.VISIBLE);
            }
        });
    }

    public void showNoMore(final String noMoreStr, final boolean isHide) {
        isShowNoMore = true;

        mLoadMoreView.post(new Runnable() {
            @Override
            public void run() {
                mLoadMoreView.setVisibility(View.GONE);
                if (StringUtils.isNotBlank(noMoreStr)) {
                    mNoMoreView.setText(noMoreStr);
                }
                if (isHide) {
                    mNoMoreView.setVisibility(View.GONE);
                } else {
                    mNoMoreView.setVisibility(View.VISIBLE);
                }

            }
        });
    }

    public void loadMoreFailed(String reloadingStr) {
        if (StringUtils.isNotBlank(reloadingStr)) {
            mTxtReloading.setText(reloadingStr);
        }
        mTxtReloading.setVisibility(View.VISIBLE);
        mLoadMoreView.setVisibility(View.GONE);
    }

    private void reloadingMore() {
        mLoadMoreView.setVisibility(View.VISIBLE);
        mTxtReloading.setVisibility(View.GONE);
    }

    public void openLoadMore() {
        isShowNoMore = false;
        mLoadMoreView.post(new Runnable() {
            @Override
            public void run() {
                mTxtReloading.setVisibility(View.GONE);
                mLoadMoreView.setVisibility(View.VISIBLE);
                mNoMoreView.setVisibility(View.GONE);
            }
        });
    }

    public void setLoadMoreAction(RecyclerAction action) {
        mLoadMoreAction = action;
    }

    public void add(T object) {
        if (!isShowNoMore) {
            mData.add(object);
            int position;
            if (hasFooter) {
                position = mViewCount - 2;
            } else {
                position = mViewCount - 1;
            }
            mViewCount++;
            notifyItemInserted(position);
        }
    }

    public void insert(T object, int itemPosition) {
        if (mData != null && itemPosition < mViewCount) {
            int dataPosition;
            if (hasHeader) {
                dataPosition = itemPosition - 1;
            } else {
                dataPosition = itemPosition;
            }
            mData.add(dataPosition, object);
            mViewCount++;
            notifyItemInserted(itemPosition);
        }
    }

    public void addAll(List<T> data) {
        int size = data.size();
        if (!isShowNoMore && size > 0) {
            mData.addAll(data);
            int positionStart;
            if (hasFooter) {
                positionStart = mViewCount - 2;
            } else {
                positionStart = mViewCount - 1;
            }
            mViewCount += size;
            notifyItemRangeInserted(positionStart, size);
            log("addAll()  startPosition : " + positionStart + "  itemCount : " + size);
        }
    }

    public void addAll(T[] objects) {
        addAll(Arrays.asList(objects));
    }

    public void replace(T object, int itemPosition) {
        if (mData != null) {
            int dataPosition;
            if (hasHeader) {
                dataPosition = itemPosition - 1;
            } else {
                dataPosition = itemPosition;
            }
            if (dataPosition < mData.size()) {
                mData.set(dataPosition, object);
                notifyItemChanged(itemPosition);
            }
        }
    }

    //position start with 0
    public void remove(T object) {
        if (!mData.contains(object)) {
            Toast.makeText(getContext(), "remove failed", Toast.LENGTH_SHORT).show();
            log("remove()  without the object : " + object.getClass().getName());
            return;
        }
        int dataPosition = mData.indexOf(object);
        int itemPosition;
        if (hasHeader) {
            itemPosition = dataPosition + 1;
        } else {
            itemPosition = dataPosition;
        }
        remove(itemPosition);
    }

    //positionItem start with 0
    public void remove(int itemPosition) {
        int dataPosition;
        int dataSize = mData.size();
        if (hasHeader) {
            dataPosition = itemPosition - 1;
            if (dataPosition >= 0 && dataPosition < dataSize) {
                mData.remove(dataPosition);
                notifyItemRemoved(itemPosition);
                mViewCount--;
            } else if (dataPosition >= dataSize) {
                Toast.makeText(getContext(), "remove failed", Toast.LENGTH_SHORT).show();
            } else {
                throw new IndexOutOfBoundsException("RecyclerView has header,position is should " +
                        "more than 0 ." +
                        "if you want remove header , pleasure user removeHeader()");
            }
        } else {
            dataPosition = itemPosition;
            if (dataPosition >= dataSize) {
                Toast.makeText(getContext(), "remove failed", Toast.LENGTH_SHORT).show();
            } else {
                mData.remove(dataPosition);
                notifyItemRemoved(itemPosition);
                mViewCount--;
            }
        }
    }

    public void clear() {
        if (mData == null || mData.size() == 0) {
            return;
        }
        mData.clear();
        mViewCount = 1;
        if (hasHeader) {
            mViewCount++;
        }
        if (hasFooter) {
            mViewCount++;
        }
        isShowNoMore = false;
        mLoadMoreView.setVisibility(View.GONE);
        mNoMoreView.setVisibility(View.GONE);
        notifyDataSetChanged();
    }


    public void setHeader(View header) {
        hasHeader = true;
        headerView = header;
        mViewCount++;
    }

    public void setHeader(@LayoutRes int res) {
        setHeader(LayoutInflater.from(mContext).inflate(res, null));
    }

    public View getHeader() {
        return headerView;
    }

    public View getFooter() {
        return footerView;
    }

    public void setFooter(View footer) {
        hasFooter = true;
        footerView = footer;
        mViewCount++;
    }

    public void setFooter(@LayoutRes int res) {
        setFooter(LayoutInflater.from(mContext).inflate(res, null));
    }

    public void removeHeader() {
        if (hasHeader) {
            hasHeader = false;
            notifyItemRemoved(0);
        }
    }

    public void removeFooter() {
        if (hasFooter) {
            hasFooter = false;
            notifyItemRemoved(mViewCount - 2);
        }
    }

    public List<T> getData() {
        return mData;
    }

    public Context getContext() {
        return mContext;
    }

    public void setOnItemViewClickListener(RecyclerItemViewClickListener<T>
                                                   mRecyclerItemViewClickListener) {
        this.mRecyclerItemViewClickListener = mRecyclerItemViewClickListener;
    }


    public void log(String content) {
        BaseLog.ii(RecyclerAdapter.class.getName() + " " + content);
    }
}
