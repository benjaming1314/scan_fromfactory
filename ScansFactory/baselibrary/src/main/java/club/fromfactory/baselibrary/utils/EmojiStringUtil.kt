package club.fromfactory.baselibrary.utils

import android.text.TextUtils
import java.util.regex.Pattern

/**
 * 兼容emoji的字符串截取工具
 *
 * @author nichenjian
 * @date 2018/10/31
 */
object EmojiStringUtil {
    private val range = Pattern.compile("\ud83c[\udffb-\udfff](?=\ud83c[\udffb-\udfff])|(?:[^\ud800-\udfff][\u0300-\u036f\ufe20-\ufe23\u20d0-\u20f0]?|[\u0300-\u036f\ufe20-\ufe23\u20d0-\u20f0]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\ud800-\udfff])[\ufe0e\ufe0f]?(?:[\u0300-\u036f\ufe20-\ufe23\u20d0-\u20f0]|\ud83c[\udffb-\udfff])?(?:\u200d(?:[^\ud800-\udfff]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff])[\ufe0e\ufe0f]?(?:[\u0300-\u036f\ufe20-\ufe23\u20d0-\u20f0]|\ud83c[\udffb-\udfff])?)*")

    /**
     * 截取子字符串
     *
     * @param source 原始字符串
     * @param begin 起始位置
     * @param end 结束位置
     */
    fun subString(source: String, begin: Int = 0, end: Int): String {
        if (TextUtils.isEmpty(source)) {
            return source
        }

        val matcher = range.matcher(source)
        val result = ArrayList<String>()
        while (matcher.find()) {
            result.add(matcher.group())
        }

        if (result.isEmpty()) {
            return source
        }

        val beginIndex = if (begin < 0 || begin >= result.size) 0 else begin
        val endIndex = if (end < 0 || end >= result.size || end < begin) result.size - 1 else end
        return result.subList(beginIndex, endIndex).joinToString(separator = "")
    }
}