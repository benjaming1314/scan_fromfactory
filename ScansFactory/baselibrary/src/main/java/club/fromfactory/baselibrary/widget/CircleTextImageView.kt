package club.fromfactory.baselibrary.widget

import android.content.Context
import android.graphics.*
import android.support.v7.widget.AppCompatTextView
import android.text.TextUtils
import android.util.AttributeSet
import android.view.Gravity
import android.widget.TextView
import club.fromfactory.baselibrary.R
import java.util.*


/**
 * 圆形的文本View
 *
 * @author lxm
 * @date 2017/7/3
 */

class CircleTextImageView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = -1) : AppCompatTextView(context, attrs, defStyleAttr) {
    companion object {
        private const val DEFAULT_CIRCLE_BG_COLOR = Color.RED
        private const val DEFAULT_CIRCLE_TEXT_COLOR = Color.WHITE
    }

    private val pfd = PaintFlagsDrawFilter(0, Paint.ANTI_ALIAS_FLAG or Paint.FILTER_BITMAP_FLAG)
    private var mCircleColor = DEFAULT_CIRCLE_BG_COLOR//Default background color
    private var mCircleTextColor = DEFAULT_CIRCLE_TEXT_COLOR//text color
    private var mBgPaint: Paint? = null
    private var mBgPaintTwo: Paint? = null

    private var isRandom = false

    init {
        initAttr(context, attrs)
    }

    private fun initAttr(context: Context, attrs: AttributeSet?) {
        if (attrs == null) {
            return
        }
        val a = context.obtainStyledAttributes(attrs, R.styleable.CircleTextImageView) ?: return
        val n = a.indexCount
        for (i in 0 until n) {
            val attr = a.getIndex(i)
            if (attr == R.styleable.CircleTextImageView_circle_color) {
                mCircleColor = a.getInteger(attr, DEFAULT_CIRCLE_BG_COLOR)
            } else if (attr == R.styleable.CircleTextImageView_circle_text_color) {
                mCircleTextColor = a.getInteger(attr, DEFAULT_CIRCLE_TEXT_COLOR)
            } else if (attr == R.styleable.CircleTextImageView_random_bg_color) {
                isRandom = a.getBoolean(attr, false)
            }
        }
        a.recycle()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val measuredWidth = measuredWidth
        val measuredHeight = measuredHeight
        val max = Math.max(measuredWidth, measuredHeight)
        setMeasuredDimension(max, max)
    }

    override fun setBackgroundColor(color: Int) {
        //设置画笔的样式
        mBgPaint?.style = Paint.Style.STROKE
        mBgPaintTwo?.style = Paint.Style.STROKE

        mBgPaint?.color = color
        mBgPaint?.alpha = 50
        mBgPaintTwo?.color = color
        invalidate()
    }

    override fun draw(canvas: Canvas) {
        canvas.drawFilter = pfd//给Canvas加上抗锯齿标志
        canvas.drawCircle((width / 2).toFloat(), (height / 2).toFloat(), (Math.max(width, height) / 2).toFloat(), mBgPaintTwo)
//        canvas.drawCircle((width / 2).toFloat(), (height / 2).toFloat(), (Math.max(width, height) / 2 - 5).toFloat(), mBgPaintTwo)
        super.draw(canvas)
    }

    /**
     * 设置paint
     * 根据文本的hashcode显示文本的样式，保证同一个文案显示的颜色一致
     *
     * @param  text 展示的文本
     */
    private fun setPaint(text: CharSequence) {
        initPaintIfNeed()
        if (!TextUtils.isEmpty(text)) {
            if (isRandom) {
                mCircleColor = Color
                        .parseColor(CircleTextImageUtil.getRandomColor(text.hashCode() % CircleTextImageUtil.colorList.size))
            }
            gravity = Gravity.CENTER
            this.typeface = Typeface.DEFAULT_BOLD
            mBgPaint?.color = mCircleColor
            mBgPaint?.alpha = 50
            mBgPaintTwo?.color = mCircleColor
            mBgPaint?.isAntiAlias = true
            invalidate()
        }
    }

    private fun initPaintIfNeed() {
        if (mBgPaint == null) {
            mBgPaint = Paint()
        }

        if (mBgPaintTwo == null) {
            mBgPaintTwo = Paint()
        }
    }

    override fun setText(text: CharSequence, type: TextView.BufferType) {
        setPaint(text)
        super.setText(CircleTextImageUtil.subFirstCharacter(text), type)
    }

    private object CircleTextImageUtil {

        val colorList = ArrayList<String>()

        init {
            colorList.add("#40BBD7")
            colorList.add("#F72435")
            colorList.add("#FF8000")
            colorList.add("#7030BF")
            colorList.add("#F95387")
        }

        /**
         * Get the random color.
         */
        fun getRandomColor(position: Int): String {
            return colorList[position % colorList.size]
        }

        /**
         * Interception of the first string of characters.
         */
        fun subFirstCharacter(str: CharSequence?): String {
            if (str == null || str.isEmpty()) {
                return ""
            }

            return if (Character.isLetter(str[0])) {
                Character.toUpperCase(str[0]) + str.subSequence(1, str.length)
                        .toString()
            } else {
                str.toString()
            }
        }
    }

}
