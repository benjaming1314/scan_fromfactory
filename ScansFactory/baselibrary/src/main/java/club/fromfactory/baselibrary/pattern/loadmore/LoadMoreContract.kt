package club.fromfactory.baselibrary.pattern.loadmore

import club.fromfactory.baselibrary.presenter.IPresenter
import club.fromfactory.baselibrary.view.IBaseMVPView

/**
 * 加载更多的接口封装
 * @author 王佳斌
 * @date 2018/10/23
 */
interface LoadMoreContract {

    interface View<T> : IBaseMVPView<Presenter<T>> {
        /**
         * 展示第一页数据
         */
        fun showData(data: List<T>)

        /**
         * 展示更多数据
         */
        fun showMoreData(data: List<T>)

        /**
         * 刷新成功
         */
        fun onRefreshSucceed()

        /**
         * 刷新失败
         */
        fun onRefreshFailed()

        /**
         * 加载更多成功
         */
        fun onLoadMoreSucceed()

        /**
         * 加载更多失败
         */
        fun onLoadMoreFailed()

        /**
         * 没有更数据的回调
         */
        fun onNoMoreData()

    }

    interface Presenter<T> : IPresenter<View<T>> {

        /**
         * 获取第一页的数据
         */
        fun getData()

        /**
         * 获取更多数据
         */
        fun getMoreData()
    }
}