package club.fromfactory.baselibrary.extention

import android.content.Context
import android.support.v4.app.Fragment

/**
 * 尺寸相关的扩展函数
 * @author 王佳斌
 * @date 2018/7/5
 */
/**
 * 将dp数值转换为px
 */
fun Context.dip(value: Int) = (value * resources.displayMetrics.density).toInt()

fun Context.dip(value: Float) = (value * resources.displayMetrics.density).toInt()

//the same for Fragments
fun Fragment.dip(value: Int) = (value * resources.displayMetrics.density).toInt()

fun Fragment.dip(value: Float) = (value * resources.displayMetrics.density).toInt()