package club.fromfactory.baselibrary.net.retrofit.cache.converter

import java.io.InputStream
import java.io.OutputStream
import java.lang.reflect.Type

/**
 * 转换器
 *
 * @author nichenjian
 * @date 2018/6/26
 */
interface IConverter {
    /**
     * 读取接口
     *
     * @param source 输入流
     * @param type 转换类型
     */
    fun <T> read(source: InputStream, type: Type): T?

    /**
     * 写入接口
     *
     * @param stream 输出流
     * @param data 需要保存的数据
     */
    fun <T> write(stream: OutputStream, data: T): Boolean
}
