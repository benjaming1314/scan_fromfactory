package club.fromfactory.baselibrary.adapter;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.List;

/**
 * 扩展ArrayAdapter，对选中文字高亮显示
 *
 * @author 王佳斌
 * @date 2018/7/23
 */
public class HighlightAdapter extends ArrayAdapter<String> {

    /**
     * 选中的序号
     */
    private int mSelectedIndex = 0;
    /**
     * 选中的文本颜色
     */
    private int selectedColor;
    /**
     * 未选中的文本颜色
     */
    private int unSelectedColor;

    private int textViewId;

    private int selectViewId;

    public HighlightAdapter(@NonNull Context context, @LayoutRes int resource,
            @IdRes int textViewId, @NonNull List<String>
            objects) {
        super(context, resource, textViewId, objects);
    }

    public void setSelectedIndex(int selectedIndex) {
        this.mSelectedIndex = selectedIndex;
    }

    public void setSelectedColor(int selectedColor) {
        this.selectedColor = selectedColor;
    }

    public void setUnSelectedColor(int unSelectedColor) {
        this.unSelectedColor = unSelectedColor;
    }

    public void setTextViewId(@IdRes int textViewId) {
        this.textViewId = textViewId;
    }

    public void setSelectViewId(@IdRes int selectViewId) {
        this.selectViewId = selectViewId;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = super.getView(position, convertView, parent);
        TextView textView = view.findViewById(textViewId);
        View selectedView = view.findViewById(selectViewId);

        boolean isSelected = position == mSelectedIndex;
        int textColor = isSelected ? selectedColor : unSelectedColor;
        selectedView.setVisibility(isSelected ? View.VISIBLE : View.GONE);

        if (textColor != 0) {
            textView.setTextColor(textColor);
        } else {
            textView.setSelected(isSelected);
        }
        return view;
    }
}
