package club.fromfactory.baselibrary.widget.recyclerview;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.jetbrains.annotations.Nullable;


import kotlinx.android.extensions.LayoutContainer;

/**
 * Created by lxm on 2017/9/6..
 */
public class BaseViewHolder<T> extends RecyclerView.ViewHolder implements LayoutContainer {
    /**
     * 点击监听
     */
    protected BaseRecyclerItemViewClickListener<T> mRecyclerItemViewClickListener;
    private T mData;

    public BaseViewHolder(ViewGroup parent, int layoutId) {
        this(LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false));
    }

    public BaseViewHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRecyclerItemViewClickListener != null) {
                    mRecyclerItemViewClickListener.onItemViewClick(mData, v, getLayoutPosition());
                }
                onItemViewClick(mData);
            }
        });
    }

    public void setOnItemViewClickListener(BaseRecyclerItemViewClickListener<T>
                                                   mRecyclerItemViewClickListener) {
        this.mRecyclerItemViewClickListener = mRecyclerItemViewClickListener;
    }

    /**
     * 绑定数据
     *
     * @param data 当前需要绑定的数据
     */
    public void bindData(@NonNull final T data) {

    }

    /**
     * 绑定数据
     *
     * @param data 当前需要绑定的数据
     */
    public void bindData(@NonNull final T data, int position) {
        bindData(data);
    }

    void setData(final T data, int position) {
        if (data != null) {
            mData = data;
            bindData(data, position);
        }
    }

    public T getData() {
        return mData;
    }


    public void onItemViewClick(T data) {

    }

    @Nullable
    @Override
    public View getContainerView() {
        return itemView;
    }
}
