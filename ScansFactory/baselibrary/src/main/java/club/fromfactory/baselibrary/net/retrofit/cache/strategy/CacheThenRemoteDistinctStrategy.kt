package club.fromfactory.baselibrary.net.retrofit.cache.strategy

import club.fromfactory.baselibrary.model.BaseResponse
import club.fromfactory.baselibrary.net.retrofit.cache.RxCache
import club.fromfactory.baselibrary.net.retrofit.cache.converter.GsonConverter
import club.fromfactory.baselibrary.net.retrofit.cache.model.Reply
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okio.ByteString
import java.lang.reflect.Type
import java.nio.ByteBuffer
import java.util.concurrent.TimeUnit

/**
 * 先显示缓存，再请求网络
 *
 * 如果两次的数据不一致，则执行两次回调
 * 否则只执行一次回调
 *
 *
 * @author nichenjian
 * @date 2018/6/26
 */
class CacheThenRemoteDistinctStrategy : IStrategy() {
    private var cachedData: Any? = null
    override fun <T> execute(rxCache: RxCache?, cacheKey: String?, maxAge: Long?, timeUnit: TimeUnit?, source: Observable<T>?, type: Type?): Observable<Reply<T>> {
        val cacheObservable = loadCache<T>(rxCache, cacheKey, type, maxAge, timeUnit)
        val remoteObservable = loadRemote(rxCache, cacheKey, source)

        return Observable
                .concatDelayError(listOf(cacheObservable, remoteObservable))
                .filter {
                    if (it.isFromCache) {
                        cachedData = it.data
                        return@filter it.data != null
                    } else {
                        if (cachedData == null) {
                            return@filter true
                        }
                        val remoteData = it.data ?: return@filter false
                        val cacheHash = getHash(cachedData!!)
                        val remoteHash = getHash(remoteData)
                        return@filter cacheHash != remoteHash
                    }
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    /**
     * 优化获取hash
     * 优先读取后端返回的值，如果不存在则对比本地计算的md5
     */
    private fun getHash(data: Any): String {
        var remoteMd5: String? = null
//        if ((data is BaseResponse<*>) && !data.md5.isNullOrEmpty()) {
//            remoteMd5 = (cachedData as BaseResponse<*>).md5
//        }

        return remoteMd5 ?: ByteString.of(ByteBuffer.wrap(GsonConverter.gson.toJson(data).toString().toByteArray())).md5().hex()
    }
}
