package club.fromfactory.baselibrary.rx.activityresult

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.SparseArray
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

/**
 * 空fragment实现
 */
class AvoidOnResultFragment : Fragment() {
    private val subjects = SparseArray<PublishSubject<ActivityResultInfo>>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    fun startForResult(intent: Intent,
                       requestCode: Int): Observable<ActivityResultInfo> {
        val subject = PublishSubject.create<ActivityResultInfo>()
        subjects.append(requestCode, subject)
        return subject.doOnSubscribe { startActivityForResult(intent, requestCode) }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        val subject = subjects.get(requestCode)
        subjects.remove(requestCode)
        if (subject != null) {
            subject.onNext(ActivityResultInfo(resultCode, data))
            subject.onComplete()
        }
    }
}