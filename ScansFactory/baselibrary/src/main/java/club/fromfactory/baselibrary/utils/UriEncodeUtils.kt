package club.fromfactory.baselibrary.utils

import java.io.UnsupportedEncodingException
import java.net.URLDecoder
import java.net.URLEncoder

/**
 *@author lxm
 *@date 2018/9/27
 */
/**
 * 通用的encode方法，替换+号位%20
 */
fun encode(str: String?): String {
    var str = str
    try {
        str = URLEncoder.encode(str, "UTF-8").replace("\\+".toRegex(), "%20")
    } catch (e: UnsupportedEncodingException) {
        logException(e)
    } catch (e: IllegalArgumentException) {
        logException(e)
    }

    return str ?: ""
}

/**
 * 通用的decode方法
 */
fun decode(str: String?): String {
    var str = str
    if (str == null || str.isEmpty()) {
        return ""
    }

    try {
        str = URLDecoder.decode(str, "UTF-8")
    } catch (e: UnsupportedEncodingException) {
        logException(e)
    } catch (e: IllegalArgumentException) {
        logException(e)
    }

    return str ?: ""
}