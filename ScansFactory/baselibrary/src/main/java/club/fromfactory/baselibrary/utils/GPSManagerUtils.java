package club.fromfactory.baselibrary.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;

import com.crashlytics.android.Crashlytics;

import club.fromfactory.baselibrary.BaseApplication;


/**
 * Created by lxm on 2018/1/9.
 */

public class GPSManagerUtils {

    private static GPSManagerUtils mGpsManagerUtils;
    private LocationManager locationManager;


    private GPSManagerUtils() {
        locationManager = (LocationManager) BaseApplication.instance.getSystemService(Context
                .LOCATION_SERVICE);
    }

    public static GPSManagerUtils getInstance() {
        if (mGpsManagerUtils == null) {
            mGpsManagerUtils = new GPSManagerUtils();
        }
        return mGpsManagerUtils;
    }

    public Location getLocation() {
        if (ActivityCompat.checkSelfPermission(BaseApplication.instance,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(BaseApplication.instance,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return null;
        }

        // fix crash https://fabric.io/jiayunshuju/android/apps/club
        // .fromfactory/issues/5b0abf796007d59fcd1ad03a
        try {
            return locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        } catch (Exception e) {
            Crashlytics.logException(e);
        }

        return null;
    }


}
