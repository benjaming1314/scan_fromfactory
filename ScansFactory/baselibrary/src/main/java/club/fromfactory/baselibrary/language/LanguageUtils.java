package club.fromfactory.baselibrary.language;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import club.fromfactory.baselibrary.BaseApplication;
import club.fromfactory.baselibrary.utils.PreferenceStorageUtils;
import club.fromfactory.baselibrary.utils.SDK26Utils;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * 语言管理工具 Created by nichenjian on 2018/3/28.
 */

public class LanguageUtils {

    private static final String LANGUAGE_ZH = "zh";
    private static final String LANGUAGE_EN = "en";
    private static final String LANGUAGE_IN = "in";
    private static final String LANGUAGE_PT = "pt";
    private static final String LANGUAGE_ES = "es";
    public static final String LANGUAGE_AR = "ar";
    private static final String LANGUAGE_HI = "hi";
    private static final String LANGUAGE_IT = "it";

    private static final String LANGUAGE_ZH_COOKIE = "zh";
    private static final String LANGUAGE_EN_COOKIE = "en";
    private static final String LANGUAGE_HI_COOKIE = "hi";
    private static final String LANGUAGE_ES_COOKIE = "es";
    private static final String LANGUAGE_AR_COOKIE = "ar";
    private static final String LANGUAGE_PT_COOKIE = "pt";
    private static final String LANGUAGE_IN_COOKIE = "id";
    private static final String LANGUAGE_IT_COOKIE = "it";
    private static final List<LanguageInfo> list = new ArrayList<>();

    // app使用的语言
    private volatile static String mAppLanguage = "";

    public static Context onAttach(Context context) {
        return setLocale(context, LanguageUtils.getLanguageCode());
    }

    public static String getLanguageCookie(String language) {
        if (language == null) {
            return LANGUAGE_EN_COOKIE;
        }

        for (LanguageInfo languageInfo : list) {
            if (languageInfo != null && language.equals(languageInfo.getLanguageCode())) {
                return languageInfo.getLanguageCookie();
            }
        }

        return LANGUAGE_EN_COOKIE;
    }

    static {
        LanguageInfo languageInfo = new LanguageInfo();
        languageInfo.setLanguageStr("English");
        languageInfo.setLanguageCookie(LANGUAGE_EN_COOKIE);
        languageInfo.setLanguageCode(LanguageUtils.LANGUAGE_EN);
        list.add(languageInfo);

        languageInfo = new LanguageInfo();
        languageInfo.setLanguageStr("中文");
        languageInfo.setLanguageCookie(LANGUAGE_ZH_COOKIE);
        languageInfo.setLanguageCode(LanguageUtils.LANGUAGE_ZH);
        list.add(languageInfo);

    }

    public static String getCurrentLanguageName() {

        List<LanguageInfo> languageList = getLanguageInfoList();

        String language = getLanguageCode();

        for (int i = 0; i < languageList.size(); i++) {
            LanguageInfo languageInor = languageList.get(i);
            String language_code = languageInor.getLanguageCode();
            if (language_code.equalsIgnoreCase(language)) {

                return languageInor.getLanguageStr();
            }
        }
        return "";
    }


    public static List<LanguageInfo> getLanguageInfoList() {
        return list;
    }

    private static Context setLocale(Context context, String language) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return updateResources(context, language);
        }

        return updateResourcesLegacy(context, language);
    }

    @TargetApi(Build.VERSION_CODES.N)
    private static Context updateResources(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Configuration configuration = context.getResources().getConfiguration();
        configuration.setLocale(locale);
        configuration.setLayoutDirection(locale);

        return context.createConfigurationContext(configuration);
    }

    @SuppressWarnings("deprecation")
    private static Context updateResourcesLegacy(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Resources resources = context.getResources();

        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;
        configuration.setLayoutDirection(locale);

        resources.updateConfiguration(configuration, resources.getDisplayMetrics());

        return context;
    }

    /**
     * 获取支持的语言 如果都获取不到，则默认显示英文
     */
    public static String getLanguageCode() {
        if (isLanguageValid(mAppLanguage)) {
            return mAppLanguage;
        }

        // 从缓存中读取设置的语言
        String selectedLanguage = PreferenceStorageUtils.getInstance().getAppSelectLanguage();
        if (isLanguageValid(selectedLanguage)) {
            mAppLanguage = selectedLanguage;
            return mAppLanguage;
        }

        // 读取系统的语言
        String systemLanguage = SDK26Utils.getLocalInstance().getLanguage();
        if (isLanguageValid(systemLanguage)) {
            mAppLanguage = systemLanguage;
            return mAppLanguage;
        }

        // 没有获取到，设置为英文
        mAppLanguage = LANGUAGE_EN;
        return mAppLanguage;
    }

    /**
     * 是否需要设置语言
     */
    public static boolean isNeedchangeLanguage(String language) {
        return isLanguageValid(language) && !language.equals(mAppLanguage);
    }

    /**
     * 重设手机语言
     *
     * @param language 语言
     */
    public static void changeAppLanguage(String language) {
        if (isLanguageValid(language)) {
            mAppLanguage = language;
            Resources resources = BaseApplication.instance.getResources();//获得res资源对象
            Configuration config = resources.getConfiguration();//获得设置对象
            DisplayMetrics dm = resources.getDisplayMetrics();//获得屏幕参数：主要是分辨率，像素等。
            Locale locale = new Locale(language);
            Locale.setDefault(locale);
            config.setLocale(locale);
            resources.updateConfiguration(config, dm);
        }
    }

    /**
     * 当前的语言是否支持
     */
    private static boolean isLanguageValid(String language) {
        if (TextUtils.isEmpty(language)) {
            return false;
        }

        for (LanguageInfo languageInfo : list) {
            if (languageInfo != null && language
                    .equalsIgnoreCase(languageInfo.getLanguageCode())) {
                return true;
            }
        }

        return false;
    }

    /**
     * 当前的语言是否要rtl布局
     */
    public static boolean isRtl() {
        return getLanguageCode().equals(LANGUAGE_AR);
    }
}
