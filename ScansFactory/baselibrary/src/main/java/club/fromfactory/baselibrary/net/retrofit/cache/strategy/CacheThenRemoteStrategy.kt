package club.fromfactory.baselibrary.net.retrofit.cache.strategy

import club.fromfactory.baselibrary.net.retrofit.cache.RxCache
import club.fromfactory.baselibrary.net.retrofit.cache.model.Reply
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.lang.reflect.Type
import java.util.concurrent.TimeUnit

/**
 * 先读取缓存，再请求网络，执行两次回调
 *
 * @author nichenjian
 * @date 2018/6/26
 */
class CacheThenRemoteStrategy : IStrategy() {
    override fun <T> execute(rxCache: RxCache?, cacheKey: String?, maxAge: Long?, timeUnit: TimeUnit?, source: Observable<T>?, type: Type?): Observable<Reply<T>> {
        val cacheObservable = loadCache<T>(rxCache, cacheKey, type, maxAge, timeUnit)
        val remoteObservable = loadRemote(rxCache, cacheKey, source)
        return Observable
                .concatDelayError(listOf(cacheObservable, remoteObservable))
                .filter {
                    it.data != null
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}

