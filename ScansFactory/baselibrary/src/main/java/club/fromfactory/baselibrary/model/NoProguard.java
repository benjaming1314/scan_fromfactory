package club.fromfactory.baselibrary.model;

/**
 * Created by nichenjian on 2018/3/19.
 */

/**
 * 类不混淆的接口，实现该接口后，release包不会混淆继承的类
 */
public interface NoProguard {
}
