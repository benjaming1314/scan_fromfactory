package club.fromfactory.baselibrary.pattern

import android.content.Context
import android.support.annotation.LayoutRes
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 *
 * @author 王佳斌
 * @date 2018/11/29
 */
abstract class ViewBinder<T>(context: Context, parent: ViewGroup) {
    var view: View

    init {
        view = LayoutInflater.from(context).inflate(getLayoutResId(), parent,
                false)
    }

    /**
     * 绑定数据，返回绑定后的视图
     */
    fun bindData(t: T): View {
        return view.bind(t)
    }

    /**
     * 绑定数据，返回绑定后的视图
     */
    abstract fun View.bind(t: T): View

    /**
     * 获取布局文件的id
     *
     * @return 布局文件的id
     */
    @LayoutRes
    abstract fun getLayoutResId(): Int
}