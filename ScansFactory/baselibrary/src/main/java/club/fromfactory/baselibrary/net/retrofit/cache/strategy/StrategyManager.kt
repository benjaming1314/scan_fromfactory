package club.fromfactory.baselibrary.net.retrofit.cache.strategy

import club.fromfactory.baselibrary.net.retrofit.cache.model.CacheMode
import java.util.*

/**
 * 策略管理
 *
 * @author nichenjian
 * @date 2018/6/27
 */
object StrategyManager {
    val map = HashMap<CacheMode, Class<out IStrategy>>()

    init {
        map[CacheMode.NO_CACHE] = NoCacheStrategy::class.java
        map[CacheMode.FIRST_REMOTE] = FirstRemoteStrategy::class.java
        map[CacheMode.FIRST_CACHE] = FirstCacheStrategy::class.java
        map[CacheMode.ONLY_CACHE] = OnlyCacheStrategy::class.java
        map[CacheMode.ONLY_REMOTE] = OnlyRemoteStrategy::class.java
        map[CacheMode.CACHE_THEN_REMOTE] = CacheThenRemoteStrategy::class.java
        map[CacheMode.CACHE_THEN_REMOTE_DISTINCT] = CacheThenRemoteDistinctStrategy::class.java
    }
}
