package club.fromfactory.baselibrary.widget.recyclerview;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by lxm on 2017/9/6..
 */

public class ViewHolderManager {

    private final String TAG = "ViewHolderManager";
    private int mViewType = 10;
    private Map<Class<? extends BaseViewHolder>, Integer> mHolderType;
    private Map<Integer, Class<? extends BaseViewHolder>> mTypeHolder;

    public ViewHolderManager() {
        mHolderType = new HashMap<>();
        mTypeHolder = new HashMap<>();
    }

    public void addViewHolder(Class<? extends BaseViewHolder> viewHolder) {
        if (!mHolderType.containsKey(viewHolder)) {
//            Class dataClass;
//
//            if (viewHolder.getGenericSuperclass() instanceof ParameterizedType) {
//                dataClass = (Class) ((ParameterizedType) viewHolder.getGenericSuperclass())
// .getActualTypeArguments()[0]; //获取ViewHolder的泛型数据class
//            } else {
//                dataClass = viewHolder.getClass();
//            }

            mViewType++;
            mHolderType.put(viewHolder, mViewType);
            mTypeHolder.put(mViewType, viewHolder);
//            Log.i(TAG, "addCreator dataClassType : " + dataClass.getName());
        }
    }

    public int getViewType(Class<? extends BaseViewHolder> holder) {
        if (!mHolderType.containsKey(holder)) {
            throw new NullPointerException("please invoke addCreator method");
        }
        return mHolderType.get(holder);
    }

    public Class<? extends BaseViewHolder> getViewHolder(int viewType) {
        if (!mTypeHolder.containsKey(viewType)) {
            throw new NullPointerException("please invoke addCreator method");
        }
        return mTypeHolder.get(viewType);
    }
}
