package club.fromfactory.baselibrary.widget.recyclerview

data class ViewHolderCreatorModel<T>(var type : Int ,
                              var data : T)