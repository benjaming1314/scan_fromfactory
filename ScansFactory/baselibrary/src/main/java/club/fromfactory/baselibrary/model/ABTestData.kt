package club.fromfactory.baselibrary.model

/**
 * AB Test的数据模型
 *
 * Created by nichenjian on 2018/4/10
 */

data class ABTestData(val pageStatus: PageStatus) {

    class PageStatus {
        /**
         * 登录页
         */
        val loginPage: LoginPage? = null

        /**
         * 商品详情页
         */
        val productDetailPage: ProductDetailPage? = null

        /**
         * 列表页
         */
        val productListPage: ProductListPage? = null
    }

    class ProductDetailPage(
            /**
             * 是否新商品详情页
             */
            val newProductDetail: Boolean = true
    )

    class LoginPage(
            /**
             * 是否新的登录
             */
            val isNewLogin: Boolean = false)

    class ProductListPage(
            /**
             * ab实验的版本
             * 0,1 标识进入老版本，2,3标识进入新版本
             */
            val abVersion: Int = 2
    )
}
