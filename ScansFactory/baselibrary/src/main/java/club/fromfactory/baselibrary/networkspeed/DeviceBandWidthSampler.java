package club.fromfactory.baselibrary.networkspeed;

import android.net.TrafficStats;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;


/**
 * 网络测速工具
 */
public class DeviceBandWidthSampler {
    /**
     * 是否在采样中
     */
    private boolean isSampling = false;
    private final ConnectionNetworkManager mConnectionNetworkManager;
    private final SamplingHandler mHandler;
    private volatile long sPreviousBytes = -1;
    private long mLastTimeReading;

    /**
     * Singleton
     */
    private static class DeviceBandWidthSamplerHolder {
        public static final DeviceBandWidthSampler instance = new DeviceBandWidthSampler();
    }

    public static DeviceBandWidthSampler getInstance() {
        return DeviceBandWidthSamplerHolder.instance;
    }

    private DeviceBandWidthSampler() {
        HandlerThread handlerThread = new HandlerThread("SamplingThread");
        handlerThread.start();
        mConnectionNetworkManager = ConnectionNetworkManager.getInstance();
        mHandler = new SamplingHandler(handlerThread.getLooper());
    }

    /**
     * 开始网络采样
     */
    public void startBandWidthSampling() {
        if (isSampling) {
            return;
        }

        isSampling = true;
        mHandler.startSamplingThread();
    }

    /**
     * 结束网络采样
     */
    public void stopBandWidthSampling() {
        isSampling = false;
        mHandler.stopSamplingThread();
    }

    /**
     * 添加采样
     */
    private void addSample() {
        long newBytes = TrafficStats.getTotalRxBytes();
        long byteDiff = newBytes - sPreviousBytes;

        if (sPreviousBytes >= 0) {
            synchronized (this) {
                long curTimeReading = SystemClock.elapsedRealtime();
                mConnectionNetworkManager.addSample(byteDiff, curTimeReading - mLastTimeReading);
                mLastTimeReading = curTimeReading;
            }
        }

        sPreviousBytes = newBytes;
    }

    private class SamplingHandler extends Handler {
        private static final long SAMPLE_TIME = 1000;
        private static final int MSG_SAMPLING = 1;
        private static final int MSG_START = 2;
        private static final int MSG_STOP = 3;

        SamplingHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_SAMPLING:
                    addSample();
                    // 间隔1s收集一次手机的流量信息
                    sendEmptyMessageDelayed(MSG_SAMPLING, SAMPLE_TIME);
                    break;
                case MSG_START:
                    mLastTimeReading = SystemClock.elapsedRealtime();
                    sPreviousBytes = -1;
                    break;
                case MSG_STOP:
                    addSample();
//                   避免cookie引用
//                    final long result = mConnectionNetworkManager.calculateBandWidth();
                    // 设置网络速度cookie
//                    new Handler(Looper.getMainLooper()).post(new Runnable() {
//                        @Override
//                        public void run() {
//                            if (result > 0) {
//                                CookieHelper.syncNetworkSpeed(result);
//                            }
//                        }
//                    });
                    mConnectionNetworkManager.reset();
                    break;
                default:
                    break;
            }
        }

        void startSamplingThread() {
            sendEmptyMessage(SamplingHandler.MSG_START);
            sendEmptyMessage(SamplingHandler.MSG_SAMPLING);
        }

        void stopSamplingThread() {
            sendEmptyMessage(SamplingHandler.MSG_STOP);
            removeMessages(SamplingHandler.MSG_SAMPLING);
        }
    }
}
