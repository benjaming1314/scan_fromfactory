package club.fromfactory.baselibrary.statistic.constants;

/**
 * pageId常量
 *
 * @author nichenjian
 * @date 2018/7/30
 */
public class PageIdConstants {

    public static final int INDEX = 1;
    public static final int INDEX_CATEGORIES = 2;
    public static final int INDEX_CART = 3;
    public static final int INDEX_ACCOUNT = 4;
    public static final int CATEGORY = 8;
    public static final int SEARCH = 14;
    public static final int SEARCH_RESULT = 15;
    public static final int FILTER = 17;
    public static final int MESSAGE_CENTER = 20;
    public static final int MESSAGE_LIST = 21;
    public static final int SELECT_COUNTRY = 18;
    public static final int SELECT_LANGUAGE = 19;
    public static final int SNS = 26;
    public static final int PUBLISH = 28;
    public static final int SNS_USER_CENTER = 27;
    public static final int SNS_USER_CENTER_FOLLOWER_OR_FOLLOWING = 29;
    public static final int SNS_USER_CENTER_LIKED = 30;
    public static final int SNS_DETAIL = 31;
    public static final int SNS_REVIEW = 33;
    public static final int LOGIN = 22;
    public static final int PAY = 7;
    public static final int SPLASH = 35;
    public static final int SPLASH_AD = 36;
    public static final int SELECT_GENDER = 34;
    public static final int LOGIN_INDEX = 41;
    public static final int PHONE_LOGIN = 42;
    public static final int EMAIL_LOGIN = 43;
    public static final int PHONE_SIGN_UP = 44;
    public static final int EMAIL_SIGN_UP = 45;
    public static final int RESET_PASSWORD = 46;

    public static final int EDIT_INTRODUCE = 47;
    public static final int CFLOOKS_MESSAGE = 48;
    public static final int CFLOOKS_MESSAGE_COMMENTS = 49;
    public static final int CFLOOKS_MESSAGE_LIKES = 50;
    public static final int CFLOOKS_MESSAGE_NEW_FANS = 51;
    public static final int SNS_DISCOVER_VIDEO = 52;
    public static final int SNS_DISCOVER_PHOTOS = 53;

    public static final int CFLOOKS_MESSAGE_NOTIFICATION = 54;
}
