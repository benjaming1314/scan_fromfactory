package club.fromfactory.baselibrary.net.retrofit.cache.strategy;

import club.fromfactory.baselibrary.model.BaseResponse;
import club.fromfactory.baselibrary.net.retrofit.cache.RxCache;
import club.fromfactory.baselibrary.net.retrofit.cache.model.Reply;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

import java.lang.reflect.Type;
import java.util.concurrent.TimeUnit;

/**
 * 请求策略
 *
 * @author nichenjian
 * @date 2018/6/29
 */
public abstract class IStrategy {
    /**
     * 成功code码200
     */
    private static final int SUCCESS_CODE_200 = 200;

    /**
     * 成功code码0
     */
    private static final int SUCCESS_CODE_0 = 0;

    /**
     * 执行请求
     *
     * @param rxCache  缓存
     * @param cacheKey 缓存的key
     * @param maxAge   缓存时间
     * @param timeUnit 缓存的时间标识
     * @param source   网络请求的Observable
     * @param type     转换的类型
     * @return Reply封装的数据
     */
    public abstract <T> Observable<Reply<T>> execute(RxCache rxCache, String cacheKey, Long maxAge, TimeUnit
            timeUnit, Observable<T> source, Type type);

    /**
     * 加载缓存的数据
     *
     * @param rxCache   缓存
     * @param cacheKey  缓存的Key
     * @param type      类型
     * @param cacheTime 缓存时间
     * @param timeUnit  时间类型
     */
    public <T> Observable<Reply<T>> loadCache(RxCache rxCache, String cacheKey, Type type, Long cacheTime,
                                              TimeUnit timeUnit) {
        Observable<T> observable = rxCache
                .load(type, cacheKey, cacheTime, timeUnit);

        return observable.flatMap(new Function<T, ObservableSource<Reply<T>>>() {
            @Override
            public ObservableSource<Reply<T>> apply(T t) {
                return Observable.just(new Reply<T>(true, t));
            }
        });
    }

    /**
     * 从服务端请求数据
     * <p>
     * 需要注意的是如果返回的数据模型是BaseResponse 且 code 200 才缓存，其他的状态码暂时不缓存
     *
     * @param rxCache  缓存
     * @param cacheKey 缓存的key
     * @param source   请求的实体
     * @return
     */
    public <T> Observable<Reply<T>> loadRemote(final RxCache rxCache, final String cacheKey, Observable<T> source) {
        return source
                .flatMap(new Function<T, ObservableSource<Reply<T>>>() {
                    @Override
                    public ObservableSource<Reply<T>> apply(T t) {
                        if (t instanceof BaseResponse) {

                            if (((BaseResponse) t).isSuccess()) {
                                // 缓存请求的数据
                                rxCache.save(cacheKey, t)
                                        .subscribeOn(Schedulers.io())
                                        .subscribe();
                            }
//                            // 只保存成功数据，其他情况不保存
//                            if (((BaseResponse) t).code == SUCCESS_CODE_200 || ((BaseResponse) t).code ==
//                                    SUCCESS_CODE_0) {
//                                // 缓存请求的数据
//                                rxCache.save(cacheKey, t)
//                                        .subscribeOn(Schedulers.io())
//                                        .subscribe();
//                            }
                        } else {
                            // 缓存请求的数据
                            rxCache.save(cacheKey, t)
                                    .subscribeOn(Schedulers.io())
                                    .subscribe();
                        }

                        return Observable.just(new Reply<T>(false, t));
                    }
                });
    }
}
