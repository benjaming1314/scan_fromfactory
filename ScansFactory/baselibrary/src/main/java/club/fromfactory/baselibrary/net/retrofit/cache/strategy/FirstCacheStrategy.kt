package club.fromfactory.baselibrary.net.retrofit.cache.strategy

import club.fromfactory.baselibrary.net.retrofit.cache.RxCache
import club.fromfactory.baselibrary.net.retrofit.cache.model.Reply
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.lang.reflect.Type
import java.util.concurrent.TimeUnit

/**
 * 缓存优先(只执行一次回调)
 *
 * 缓存成功，则执行缓存回调
 * 缓存失败，则请求网络
 *
 * @author nichenjian
 * @date 2018/6/27
 */
class FirstCacheStrategy : IStrategy() {
    override fun <T> execute(rxCache: RxCache?, cacheKey: String?, maxAge: Long?, timeUnit: TimeUnit?, source: Observable<T>?, type: Type?): Observable<Reply<T>> {
        val cacheObservable = loadCache<T>(rxCache, cacheKey, type, maxAge, timeUnit)
        val remoteObservable = loadRemote(rxCache, cacheKey, source)
        return Observable
                .concatDelayError(listOf(cacheObservable, remoteObservable))
                .take(1)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}
