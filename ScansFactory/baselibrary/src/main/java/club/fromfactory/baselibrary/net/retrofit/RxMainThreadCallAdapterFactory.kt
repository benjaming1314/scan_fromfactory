package club.fromfactory.baselibrary.net.retrofit

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Retrofit
import java.lang.reflect.Type

/**
 * 将下游返回的Observable序列切换到主线程的CallAdapterFactory
 *
 * @author Jellybean
 * @date 2018/5/31
 */
class RxMainThreadCallAdapterFactory : CallAdapter.Factory() {

    override fun get(returnType: Type?, annotations: Array<out Annotation>?, retrofit: Retrofit?): CallAdapter<*, *>? {
        if (getRawType(returnType) != Observable::class.java) {
            return null
        } else {
            val delegate = retrofit?.nextCallAdapter(this, returnType, annotations) as CallAdapter<Any, Observable<Any>>
            return object : CallAdapter<Any, Observable<Any>> {
                override fun responseType(): Type {
                    return delegate.responseType()
                }

                override fun adapt(call: Call<Any>?): Observable<Any> {
                    return delegate.adapt(call).observeOn(AndroidSchedulers.mainThread())
                }
            }
        }
    }
}