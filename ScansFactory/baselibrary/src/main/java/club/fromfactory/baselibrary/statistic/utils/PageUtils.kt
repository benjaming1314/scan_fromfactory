package club.fromfactory.baselibrary.statistic.utils

import android.content.Context
import android.text.TextUtils
import club.fromfactory.baselibrary.statistic.StatManager
import club.fromfactory.baselibrary.statistic.constants.EVENT_TYPE
import club.fromfactory.baselibrary.statistic.constants.StatCommonConstants
import club.fromfactory.baselibrary.statistic.constants.StatEventType
import java.util.*

/**
 * 页面辅助类
 *
 * @author nichenjian
 * @date 2018/7/26
 */
object PageUtils {

    /**
     * 添加页面曝光的统计
     *
     * @param pageName 页面
     * @param context 上下文
     */
    fun addPageImpressionStat(context: Context, pageName: String) {
        if (!TextUtils.isEmpty(pageName)) {
            val params = Hashtable<String, Any>()
            params[EVENT_TYPE] = StatEventType.IMPRESSION
            params[StatCommonConstants.EVENTOBJECT] = pageName
            params[StatCommonConstants.OBJECTTYPE] = "page"
            params[StatCommonConstants.URL] = pageName
            StatManager.addStatEvent(params, context)
        }
    }

    /**
     * 添加页面停留时长统计
     *
     * @param pageName 页面
     * @param context 上下文
     */
    fun addPageDurationStat(context: Context, pageName: String, startTime: Long) {
        if (!TextUtils.isEmpty(pageName) && startTime > 0) {
            val params = Hashtable<String, Any>()
            params[EVENT_TYPE] = StatEventType.time
            params[StatCommonConstants.EVENTOBJECT] = pageName
            params[StatCommonConstants.OBJECTTYPE] = "page"
            params[StatCommonConstants.URL] = pageName
            params[StatCommonConstants.DURATION] = System.currentTimeMillis() - startTime
            StatManager.addStatEvent(params, context)
        }
    }
}
