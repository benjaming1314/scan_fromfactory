package club.fromfactory.baselibrary

/**
 * 存放一些公用的细碎的常量
 * @author 王佳斌
 * @date 2018/9/21
 */

/**
 * 性别的常量
 */
const val GENDER_FEMALE = "F"
const val GENDER_MALE = "M"

const val YES = "yes"
const val NO = "no"