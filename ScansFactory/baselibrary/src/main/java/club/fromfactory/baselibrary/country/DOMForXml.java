package club.fromfactory.baselibrary.country;

import android.text.TextUtils;
import android.util.Log;
import android.util.Xml;
import club.fromfactory.baselibrary.BaseApplication;
import club.fromfactory.baselibrary.utils.PreferenceStorageUtils;
import club.fromfactory.baselibrary.utils.StringUtils;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

//国家列表的工具
public class DOMForXml {

    public static final String DEFAULT_COUNTRY_CODELIST = "au,bh,bd,br,ca,cn,eg,fr,in,id,il,kw," +
            "lb,my,mx,om,ph,qa,sa,sg,es,lk,th,ae,gb,us,vn";

    private ArrayList<CountryCode> results = new ArrayList<CountryCode>();//初始化集合;
    private static DOMForXml domForXml;
//    private ArrayList<CountryResult> sortResult = new ArrayList<>();

    private DOMForXml() {
        super();
        parseCountry();
    }

    public static DOMForXml getInstance() {
        if (domForXml == null) {
            domForXml = new DOMForXml();
        }
        return domForXml;
    }

//    public List<CountryCode> getCountryList() {
//
//        ArrayList<CountryCode> sortResult = new ArrayList<>();
//        try {
//            String country = DEFAULT_COUNTRY_CODELIST ;
//
//            String[] showList = country.split(",");
//            if (results.size() == 0) {
//                parseCountry();
//            }
//
//            for (int i = 0; i < showList.length; i++) {
//                showList[i] = showList[i].toLowerCase();
//                for (int j = 0; j < results.size(); j++) {
//                    if (results.get(j).getCode().equals(showList[i])) {
//                        sortResult.add(results.get(j));
//                    }
//                }
//            }
//
//            String currentCountryCode = CountryUtils.getCountryCode();
//            for (int i = 0; i < sortResult.size(); i++) {
//                CountryCode countryCode = sortResult.get(i);
//                if (countryCode.getCode().equals(currentCountryCode)) {
//                    countryCode.setCurrent(true);
//                } else {
//                    countryCode.setCurrent(false);
//                }
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return sortResult;
//    }

    public CountryCode getCountry(String code) {
        if (TextUtils.isEmpty(code)) {
            return null;
        }

        if (results.size() == 0) {
            parseCountry();
        }
        code = code.toLowerCase();
        for (int j = 0; j < results.size(); j++) {
            CountryCode countryResult = results.get(j);
            if (countryResult != null) {
                String codes = countryResult.getCode();
                if (codes.equals(code)) {
                    return countryResult;
                }
            }
        }
        return null;
    }

    /**
     * 获取全部国家列表
     *
     * @return 从xml中解析的全部国家列表
     */
    public List<CountryCode> getFullCountryList() {
        if (results.size() == 0) {
            parseCountry();
        }

        return results;
    }

    private void parseCountry() {
        CountryCode result = null;
        InputStream inputStream = null;
        try {
            inputStream = BaseApplication.instance.getResources().getAssets().open
                    ("res_country_data.xml");
            XmlPullParser parser = Xml.newPullParser();
            parser.setInput(inputStream, "UTF-8");
            int event = parser.getEventType();//产生第一个事件
            while (event != XmlPullParser.END_DOCUMENT) {
                switch (event) {
                    case XmlPullParser.START_DOCUMENT://判断当前事件是否是文档开始事件
                        break;
                    case XmlPullParser.START_TAG://判断当前事件是否是标签元素开始事件
                        if ("record".equals(parser.getName())) {//判断开始标签元素是否是book
                            result = new CountryCode();
//                        result.setId(Integer.parseInt(parser.getAttributeValue(0)));
// 得到book标签的属性值，并设置book的id
                        }
                        if (result != null) {
                            if ("field".equals(parser.getName())) {
                                Log.d("field", parser.getAttributeValue(0));
                                if ("name".equals(parser.getAttributeValue(0))) {
                                    result.setName(parser.nextText());
                                } else if ("code".equals(parser.getAttributeValue(0))) {
                                    result.setCode(parser.nextText());
                                } else {
                                    parser.nextText();
                                }
                            }
                        }
                        break;
                    case XmlPullParser.END_TAG://判断当前事件是否是标签元素结束事件
                        if ("record".equals(parser.getName())) {
                            //将book添加到books集合
                            results.add(result);
                            result = null;
                        }
                        break;
                }
                event = parser.next();//进入下一个元素并触发相应事件
            }//end while

        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
