package club.fromfactory.baselibrary.country;

import android.content.Context;
import club.fromfactory.baselibrary.BaseApplication;
import club.fromfactory.baselibrary.utils.JsonUtil;
import club.fromfactory.baselibrary.utils.PreferenceStorageUtils;
import club.fromfactory.baselibrary.utils.SDK26Utils;
import club.fromfactory.baselibrary.utils.StringUtils;
import java.util.Locale;

/**
 * Created by lxm on 2017/10/27.
 */

public class CountryUtils {

    public static final String COUNTRY_CODE_INDIA = "in";

    public static boolean isIndia() {
        String[] countryArr = getSelectCountry();

        if (countryArr != null && countryArr.length == 2) {
            return COUNTRY_CODE_INDIA.equals(countryArr[1]);
        }

        return false;
    }


    public static String getCountryCode() {
        String country_code = "in";

        String[] selectCountry = CountryUtils.getSelectCountry();

        if (selectCountry != null && selectCountry.length >= 2) {
            country_code = selectCountry[1];
        }

        return country_code.toLowerCase();
    }


    public static void saveSelectCountry(String[] countryArr) {
        if (countryArr == null || countryArr.length < 2) {
            return;
        }
        String countryJson = JsonUtil.getInstance().toJson(countryArr);
//        PreferenceStorageUtils.getInstance().saveCountrySelect(countryJson);
    }

    public static String[] getSelectCountry() {
        String country = "";
//        String country = PreferenceStorageUtils.getInstance().getCountrySelect();
        if (StringUtils.isNull(country)) {
            return getAndSetDefaultCountry();
        }
        String[] strArray = JsonUtil.getInstance().parseJson(country, String[].class);
        if (strArray == null) {
            strArray = getAndSetDefaultCountry();
        }
        return strArray;
    }

    private static String[] getAndSetDefaultCountry() {
        String[] defaultCountry = getDefaultCountry();
//        saveSelectCountry(defaultCountry);
        return defaultCountry;
    }

    /**
     * 判断国家有没有变化
     */
    public static boolean isChangedForCountry(String[] countryArr) {
        if (countryArr == null || countryArr.length < 2) {
            return false;
        }
        String[] strArray = getSelectCountry();
        if (strArray != null && strArray.length == 2) {
            if (!strArray[1].equals(countryArr[1])) {
                return true;
            }
        }
        return false;
    }

    private static String[] getDefaultCountry() {
        String countryCode = getCountryZipCode(BaseApplication.instance);
        CountryCode countryResult = DOMForXml.getInstance().getCountry(countryCode);
        String[] str = new String[2];

        if (countryResult != null) {
            str[0] = countryResult.getName();
            str[1] = countryResult.getCode();
        } else {
            str[0] = "United States";
            str[1] = "us";
        }

        return str;

    }

    /**
     * 获取国家码
     */
    private static String getCountryZipCode(Context context) {

        if (context == null) {
            context = BaseApplication.instance;
        }
        String country = "";

        try {
            country = SDK26Utils.getLocalInstance().getCountry();
            if (StringUtils.isNull(country)) {
                Locale locale = context.getResources().getConfiguration().locale;
                country = locale.getCountry();
            }

        } catch (Exception e) {
        }

        return country;
    }
}
