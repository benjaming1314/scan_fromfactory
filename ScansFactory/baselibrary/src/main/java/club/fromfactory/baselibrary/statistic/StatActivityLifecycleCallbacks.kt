package club.fromfactory.baselibrary.statistic

import android.app.Activity
import android.app.Application
import android.os.Bundle
import club.fromfactory.baselibrary.statistic.utils.PageUtils
import club.fromfactory.baselibrary.view.BaseActivity

/**
 * 统计用的activity生命周期回调
 */
object StatActivityLifecycleCallbacks : Application.ActivityLifecycleCallbacks {
    /**
     * Activity onStart的时间戳（统计时长）
     */
    private var activityStartTimeStamp: Long = 0
    private var isAppBackground = false
    private var activityCount = 0
    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
        addPageImpressionStat(activity)
    }

    override fun onActivityStarted(activity: Activity) {
        activityStartTimeStamp = System.currentTimeMillis()
        activityCount++
        // 从后台切换到前台，重新设置统计的traceId
        if (isAppBackground) {
            isAppBackground = false
            StatEventManager.onAppForeground()
        }
    }

    override fun onActivityResumed(activity: Activity) {

    }

    override fun onActivityPaused(activity: Activity) {

    }

    override fun onActivityStopped(activity: Activity) {
        addPageDurationStat(activity)
        activityCount--
        // 当前app切换到后台
        if (activityCount == 0) {
            isAppBackground = true
            StatEventManager.onAppBackground()
        }
    }

    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle?) {

    }

    override fun onActivityDestroyed(activity: Activity) {

    }

    /**
     * 添加页面停留时间统计
     *
     * @param activity 页面
     */
    private fun addPageDurationStat(activity: Activity) {
        if (activity is BaseActivity) {
            val pageName = activity.getPageName()
            PageUtils.addPageDurationStat(activity, pageName, activityStartTimeStamp)
        }
    }

    /**
     * 添加页面曝光的统计
     *
     * @param activity
     */
    private fun addPageImpressionStat(activity: Activity) {
        if (activity is BaseActivity) {
            val pageName = activity.getPageName()
            PageUtils.addPageImpressionStat(activity, pageName)
        }
    }
}



