package club.fromfactory.baselibrary.rx

import club.fromfactory.baselibrary.model.BaseResponse
import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.ObservableTransformer
import java.net.HttpURLConnection

/**
 * 用于处理有缓存设置接口的观察者
 * @author 王佳斌
 * @date 2018/8/15
 */

/**
 * 缓存，值是服务端返回的数据
 */
val cacheMap = HashMap<String, BaseResponse<*>>()

///**
// * 根据缓存的key获取缓存的回复的MD5
// *
// */
//fun getMd5(cacheKey: String): String {
//    val baseResponse = cacheMap[cacheKey]
//    return if (baseResponse == null) "" else baseResponse.md5
//}

/**
 * 获取缓存
 */
fun <T> getCache(cacheKey: String, className: Class<T>): BaseResponse<T> {
    if (cacheMap.containsKey(cacheKey) && cacheMap[cacheKey] is BaseResponse) {
        return cacheMap[cacheKey] as BaseResponse<T>
    }

    return BaseResponse()
}

class CacheComposer<T>(val cacheKey: String) : ObservableTransformer<BaseResponse<T>,
        BaseResponse<T>> {
    override fun apply(upstream: Observable<BaseResponse<T>>): ObservableSource<BaseResponse<T>> {
        return upstream.flatMap {

            if (it.isSuccess) {
                cacheMap[cacheKey] = it
                Observable.just(it)
            }else {
                val cachedResponse = cacheMap[cacheKey] as BaseResponse<T>
                Observable.just(cachedResponse)
            }
//            when (it.code) {
//                HttpURLConnection.HTTP_OK, NET_SUCCESS_PYTHON -> {
//                    cacheMap[cacheKey] = it
//                    Observable.just(it)
//                }
//                HttpURLConnection.HTTP_NOT_MODIFIED -> {
//                    val cachedResponse = cacheMap[cacheKey] as BaseResponse<T>
//                    Observable.just(cachedResponse)
//                }
//                else -> Observable.just(it)
//            }
        }
    }
}


fun <T> Observable<BaseResponse<T>>.cacheServer(cacheKey: String): Observable<BaseResponse<T>> {
    return this.compose(CacheComposer(cacheKey))
}