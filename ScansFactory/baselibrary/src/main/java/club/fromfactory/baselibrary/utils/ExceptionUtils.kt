package club.fromfactory.baselibrary.utils

import club.fromfactory.baselibrary.BuildConfig
import com.blankj.utilcode.util.LogUtils
import com.crashlytics.android.Crashlytics

/**
 * 异常相关的方法
 * @author 王佳斌
 * @date 2018/9/27
 */

/**
 * 抛出异常，debug模式下直接抛出，release时上传到crashlytics
 */
fun throwException(e: Throwable) {
    if (BuildConfig.DEBUG) {
        throw e
    } else {
        Crashlytics.logException(e)
    }
}

/**
 * 记录错误，debug模式下通过logcat记录，release时上传到crashlytics
 */
fun logException(e: Throwable) {
    if (BuildConfig.DEBUG) {
        LogUtils.e(e)
    } else {
        Crashlytics.logException(e)
    }
}