package club.fromfactory.baselibrary.router;

/**
 * Created by nichenjian on 2018/5/23.
 */

public class PageConstants {
    public static final String MAIN = "main";
    public static final String SIGNING_TYPE_LIST = "signing_type_list";
    public static final String SIGNING_TASK_LIST = "signing_task_list";
    public static final String WMS_SIGNING_DETAIL = "wms_signing_detail";
    public static final String INVENTORY_INQUIRE = "inventory_inquire";
    public static final String INVENTORY_INQUIRE_RESULT = "inventory_inquire_result";
    public static final String INVENTORY_INQUIRE_RESULT_DETAIL = "inventory_inquire_result_detail";
    public static final String CONTAINER_CONFIRM = "container_confirm";
    public static final String SHELF_BOX = "shelf_box";
    public static final String CONTAINER_SHELF = "container_shelf";
    public static final String MOVE_OUT_MUL_GOODS = "move_out_mul_goods";
    public static final String RF_INVENTORY_LIST = "rf_inventory_list";
    public static final String MOVE_IN_CONFIRM = "move_in_confirm";

    public static final String PARCEL_TASK_LIST = "parcel_task_list";
    public static final String INVENTORY = "inventory";
    public static final String MOVE_LOCATION_SELECT_LOCATION = "move_location_select_location";
    public static final String MAWB_LIST = "mawb_list";
    public static final String INVENTORY_CONVERSION = "inventory_conversion";
    public static final String CONVERSION_TASK = "conversion_task";
    public static final String CONVERSION_MOVE_IN_CONTAINER = "conversion_move_in_container";

    public static final String MOVE_OUT = "move_out";
    public static final String MOVE_OUT_GOODS_LIST = "move_out_goods_list";

    public static final String SPACE_TRANSFER = "space_transfer";
    public static final String HAWB_BINDING = "hawb_binding";

    public static final String SPARE_PARTS_MOVE_IN = "spare_parts_move_in";
    public static final String SPARE_PARTS_MOVE_OUT = "spare_parts_move_out";

}
