package club.fromfactory.baselibrary.view;

import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;

import club.fromfactory.baselibrary.presenter.IPresenter;

/**
 * MVP模式的Fragment基类
 *
 * @author Jellybean
 * @date 2018/6/1
 */
public abstract class BaseMVPFragment<P extends IPresenter> extends BaseFragment
        implements IMVPView<P> {
    protected P presenter;

    @Override
    @CallSuper
    public void initData() {
        super.initData();
        presenter = createPresenter();
    }

    @NonNull
    @Override
    public P getPresenter() {
        return presenter;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (presenter != null) {
            presenter.unsubscribe();
        }
    }

}
