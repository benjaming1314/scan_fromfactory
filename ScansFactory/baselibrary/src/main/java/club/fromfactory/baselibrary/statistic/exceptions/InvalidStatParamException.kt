package club.fromfactory.baselibrary.statistic.exceptions

import java.security.InvalidParameterException

/**
 * 统计参数异常
 *
 * @author nichenjian
 * @date 2018/7/31
 */
class InvalidStatParamException : InvalidParameterException {
    constructor() : super("Stat params is not valid, Please check again")
    constructor(params: String) : super("Stat params is not valid, Please check again $params")
}
