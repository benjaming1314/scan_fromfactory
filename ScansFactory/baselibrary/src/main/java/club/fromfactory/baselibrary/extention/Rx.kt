package club.fromfactory.baselibrary.extention

import android.support.annotation.IntRange
import com.trello.rxlifecycle2.LifecycleProvider
import io.reactivex.Observable
import java.util.concurrent.TimeUnit

/**
 *
 * @author 王佳斌
 * @date 2018/8/28
 */

/**
 * 生命周期扩展
 */
fun <T> Observable<T>.lifecycle(provider: LifecycleProvider<*>): Observable<T> {
    return this.compose(provider.bindToLifecycle())
}

fun <T> Observable<T>.lifecycle(provider: Any) =
        if (provider is LifecycleProvider<*>) {
            this.lifecycle(provider)
        } else {
            this
        }

/**
 * 倒计时功能, 注意是在计算线程中进行的
 */
fun countdown(@IntRange(from = 1) seconds: Int): Observable<Int> {
    return Observable.interval(0, 1, TimeUnit.SECONDS)
            .map { aLong -> seconds - aLong.toInt() - 1 }
            .takeUntil { integer -> integer == 0 }
}
