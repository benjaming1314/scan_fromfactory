package club.fromfactory.baselibrary.presenter;

import club.fromfactory.baselibrary.view.IMVPView;

/**
 * MVP中的Presenter
 *
 * @param <V> 对应的View
 * @author Jellybean
 * @date 2018/6/1
 */
public interface IPresenter<V extends IMVPView> {


    /**
     * 取消订阅，一般在Fragment 和 Activity 要销毁的时候调用
     * 可在此做取消网络请求的收尾工作
     */
    void unsubscribe();
}
