package club.fromfactory.baselibrary.net.retrofit.cache.model

/**
 * 缓存的模式
 *
 * @author nichenjian
 * @date 2018/6/21
 */
enum class CacheMode(val type: String) {
    /**
     * 无缓存
     */
    NO_CACHE("NO_CACHE"),
    /**
     * 优先网络请求，请求失败加载缓存
     */
    FIRST_REMOTE("FIRST_REMOTE"),
    /**
     * 优先缓存，没有缓存再去请求网络
     */
    FIRST_CACHE("FIRST_CACHE"),
    /**
     * 只读取缓存
     */
    ONLY_CACHE("ONLY_CACHE"),
    /**
     * 只读取网络请求，数据被缓存
     */
    ONLY_REMOTE("ONLY_REMOTE"),
    /**
     * 先读取缓存，不管是否存在，然后请求网络，执行两次回调
     */
    CACHE_THEN_REMOTE("CACHE_THEN_REMOTE"),
    /**
     * 先读取缓存，然后请求网络，先执行缓存的回调
     * 如果网络返回的数据和缓存数据不一致，则执行回调，否则不执行回调
     */
    CACHE_THEN_REMOTE_DISTINCT("CACHE_THEN_REMOTE_DISTINCT")

}

