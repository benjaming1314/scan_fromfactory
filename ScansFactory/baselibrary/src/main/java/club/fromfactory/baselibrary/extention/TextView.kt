package club.fromfactory.baselibrary.extention

import android.support.annotation.ColorRes
import android.text.InputFilter
import android.widget.TextView

/**
 * TextView的扩展
 * @author 王佳斌
 * @date 2018/9/13
 */

/**
 * 清空文本
 */
fun TextView.clear() {
    this.text = ""
}

/**
 * 设置最大长度
 */
fun TextView.setMaxLength(maxLength: Int) {
    this.filters = Array<InputFilter>(1) { InputFilter.LengthFilter(maxLength) }
}

/**
 * 根据色值资源id设置文本颜色
 */
fun TextView.setTextColorRes(@ColorRes colorRes: Int) {
    val color = context.resources.getColor(colorRes)
    setTextColor(color)
}