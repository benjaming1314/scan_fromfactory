package club.fromfactory.baselibrary.statistic.model

/**
 * web页面的统计配置
 *
 * @author nichenjian
 * @date 2018/8/1
 */
data class WebPageInfo(val path: String, val id: Int)
