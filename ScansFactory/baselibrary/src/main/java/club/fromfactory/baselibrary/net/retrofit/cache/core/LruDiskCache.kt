package club.fromfactory.baselibrary.net.retrofit.cache.core

import club.fromfactory.baselibrary.utils.CloseableUtils
import club.fromfactory.baselibrary.net.retrofit.cache.converter.IConverter
import com.jakewharton.disklrucache.DiskLruCache
import java.io.File
import java.io.IOException
import java.lang.reflect.Type
import java.util.concurrent.TimeUnit

/**
 * 磁盘缓存
 *
 * @author nichenjian
 * @date 2018/6/22
 */
/**
 * 非法的时间，如果使用这个值作为有效时间则表示永久有效
 */
const val INVALID_TIME = -1L
class LruDiskCache(var converter: IConverter, diskDir: File, appVersion: Int, diskMaxSize: Long) : ICache() {
    companion object {
        private var diskLruCache: DiskLruCache? = null
    }

    init {
        try {
            if (diskLruCache == null) {
                diskLruCache = DiskLruCache.open(diskDir, appVersion, 1, diskMaxSize)
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    /**
     * 是否包含某个缓存
     *
     * @param cacheKey
     * @return 是否包含缓存
     */
    override fun doContainsKey(cacheKey: String): Boolean {
        if (diskLruCache == null) {
            return false
        }
        diskLruCache?.get(cacheKey).use {
            return it != null
        }
    }

    /**
     * 缓存是否过期
     *
     * @param cacheKey 缓存的key
     * @param cacheTime 缓存时间
     * @param timeUnit 时间类型
     */
    override fun isExpired(cacheKey: String, cacheTime: Long, timeUnit: TimeUnit): Boolean {
        if (diskLruCache == null) {
            return false
        }

        if (cacheTime > INVALID_TIME) {
            val cacheTimeInMillSeconds = getCacheTimeInMilliSeconds(cacheTime, timeUnit)
            val file = File(diskLruCache?.directory, "$cacheKey.0")
            if (isCacheDataNotValid(file, cacheTimeInMillSeconds)) {
                return true
            }
        }

        return false
    }

    /**
     * 将时间转换成毫秒
     * @param cacheTime 缓存时间
     * @param timeUnit 缓存时间类型
     */
    private fun getCacheTimeInMilliSeconds(cacheTime: Long, timeUnit: TimeUnit): Long =
            when (timeUnit) {
                TimeUnit.SECONDS -> cacheTime * 1000
                TimeUnit.MILLISECONDS -> cacheTime
                TimeUnit.HOURS -> cacheTime * 1000 * 60 * 60
                TimeUnit.MINUTES -> cacheTime * 1000 * 60
                TimeUnit.DAYS -> cacheTime * 1000 * 60 * 60 * 24
                TimeUnit.MICROSECONDS -> cacheTime / 1000
                TimeUnit.NANOSECONDS -> cacheTime / 1000000
                else -> cacheTime
            }

    /**
     * 缓存的数据是否失效
     *
     * @param file 文件
     * @param time 缓存时间(ms)
     */
    private fun isCacheDataNotValid(file: File, time: Long): Boolean {
        if (!file.exists()) {
            return false
        }

        val existTime = System.currentTimeMillis() - file.lastModified()
        return existTime > time
    }

    /**
     * 加载
     *
     * @param type 类型
     * @param cacheKey 缓存的Key
     *
     * @return 获取的对象
     */
    override fun <T> doLoad(type: Type, cacheKey: String): T? {
        if (diskLruCache == null) {
            return null
        }

        val editor = diskLruCache?.edit(cacheKey)
        editor ?: return null
        val input = editor.newInputStream(0)
        val result: T?
        try {
            if (input != null) {
                result = converter.read<T>(input, type)
                editor.commit()
                return result
            }

            editor.abort()
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            CloseableUtils.close(input)
        }

        return null
    }

    /**
     * 保存
     *
     * @param cacheKey 缓存的key
     * @param value 缓存的值
     * @return 保存是否成功
     */
    override fun <T> doSave(cacheKey: String, value: T): Boolean {
        if (diskLruCache == null) {
            return false
        }

        try {
            val editor = diskLruCache?.edit(cacheKey)
            editor ?: return false
            val sink = editor.newOutputStream(0)
            if (sink != null) {
                val result = converter.write(sink, value)
                CloseableUtils.close(sink)
                editor.commit()
                return result
            }
            editor.abort()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return false
    }

    /**
     * 删除某条缓存
     *
     * @param cacheKey 缓存的Key
     * @return 删除是否成功
     */
    override fun doRemove(cacheKey: String): Boolean {
        if (diskLruCache == null) {
            return false
        }

        try {
            return diskLruCache!!.remove(cacheKey)
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return false
    }

    /**
     * 清空缓存
     *
     * @return
     */
    override fun doClear(): Boolean {
        if (diskLruCache == null) {
            return false
        }

        var result = false
        try {
            diskLruCache!!.delete()
            result = true
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return result
    }
}
