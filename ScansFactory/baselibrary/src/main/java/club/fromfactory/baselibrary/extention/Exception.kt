package club.fromfactory.baselibrary.extention

import club.fromfactory.baselibrary.BuildConfig
import com.blankj.utilcode.util.LogUtils
import com.crashlytics.android.Crashlytics

/**
 * 异常扩展方法
 *
 * @author nichenjian
 * @date 2018/10/10
 */

fun Throwable.throws() {
    if (BuildConfig.DEBUG) {
        throw this
    } else {
        Crashlytics.logException(this)
    }
}

fun Throwable.log() {
    if (BuildConfig.DEBUG) {
        LogUtils.e(this)
    } else {
        Crashlytics.logException(this)
    }
}
