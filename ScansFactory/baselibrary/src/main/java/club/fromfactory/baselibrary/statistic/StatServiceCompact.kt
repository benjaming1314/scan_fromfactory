package club.fromfactory.baselibrary.statistic

import android.annotation.TargetApi
import android.app.Service
import android.app.job.JobParameters
import android.app.job.JobService
import android.content.Intent
import android.os.Build

/**
 * 统计Service，兼容8.0之后的版本
 *
 * @author nichenjian
 * @date 2018/11/20
 */
@TargetApi(Build.VERSION_CODES.O)
class StatServiceCompact : JobService() {
    companion object {
        const val JOB_ID = 10112
    }
    override fun onStopJob(params: JobParameters?): Boolean {
        return false
    }

    override fun onStartJob(params: JobParameters?): Boolean {
        return false
    }

    override fun onDestroy() {
        shutdown()
        super.onDestroy()
    }

    override fun onTaskRemoved(rootIntent: Intent?) {
        shutdown()
        super.onTaskRemoved(rootIntent)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        return Service.START_NOT_STICKY
    }

    private fun shutdown() {
        StatManager.flushEventsQueneImmediately()
        StatEventManager.flushEventsQueneImmediately()
        try {
            Thread.sleep(1000L)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}
