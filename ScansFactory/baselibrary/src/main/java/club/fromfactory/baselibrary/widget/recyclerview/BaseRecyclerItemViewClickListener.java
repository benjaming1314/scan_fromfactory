package club.fromfactory.baselibrary.widget.recyclerview;

import android.support.annotation.Nullable;
import android.view.View;

/**
 * @author lxm
 * @date 2018/5/24/024
 */
public interface BaseRecyclerItemViewClickListener<T> {
    /**
     * item被点击的回调
     *
     * @param data        被点击的视图对应的数据
     * @param clickedView 被点击的视图
     * @param position    被点击的位置
     */
    void onItemViewClick(@Nullable T data, View clickedView, int position);
}
