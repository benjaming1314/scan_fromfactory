package club.fromfactory.baselibrary.statistic;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 页面id的注解
 *
 * @author nichenjian
 * @date 2018/7/30
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface PageId {

    /**
     * 页面id
     */
    int value();
}
