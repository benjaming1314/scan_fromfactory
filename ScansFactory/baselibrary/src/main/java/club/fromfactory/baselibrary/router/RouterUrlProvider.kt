package club.fromfactory.baselibrary.router

import club.fromfactory.baselibrary.net.NetUtils
import club.fromfactory.baselibrary.utils.StringUtils
import club.fromfactory.baselibrary.utils.encode

/**
 * router url
 *
 * Created by nichenjian on 2018/5/31.
 */
const val BASE_URL = "scanclubfactory://scanclubfactory"

object RouterUrlProvider {

    fun getMoveOutList(boxName:String,is_stock:Int?=0): String {
        return "$BASE_URL${RouterConstants.MOVE_OUT_GOODS_LIST}?box_name=$boxName&is_stock=$is_stock"
    }
    fun getMoveOut(): String {
        return "$BASE_URL${RouterConstants.MOVE_OUT}"
    }

    fun getMoveInConfirm(): String {
        return "$BASE_URL${RouterConstants.MOVE_IN_CONFIRM}"
    }
    fun getParcelTaskList(): String {
        return "$BASE_URL${RouterConstants.PARCEL_TASK_LIST}"
    }
    fun getInventoryUrl(): String {
        return "$BASE_URL${RouterConstants.INVENTORY}"
    }
    fun getRFInventoryList(): String {
        return "$BASE_URL${RouterConstants.RF_INVENTORY_LIST}"
    }

    fun getMoveOutMulGoods(boxName:String,barcodeNum:Int,is_stock:Int?=0): String {
        return "$BASE_URL${RouterConstants.MOVE_OUT_MUL_GOODS}?barcodeNum=$barcodeNum&boxName=$boxName&is_stock=$is_stock"
    }

    fun getContainerShelf(): String {
        return "$BASE_URL${RouterConstants.CONTAINER_SHELF}"
    }

    fun getContainerConfirmUrl(): String {
        return "$BASE_URL${RouterConstants.CONTAINER_CONFIRM}"
    }
    fun getShelfBoxUrl(containerNum:String,boxName:String,total_to_shelve:Int): String {
        return "$BASE_URL${RouterConstants.SHELF_BOX}?container_num=$containerNum&boxName=$boxName&total_to_shelve=$total_to_shelve"
    }

    fun getWmsSigningTaskListUrl(title: String, type: Int): String {
        return "$BASE_URL${RouterConstants.SIGNING_TASK_LIST}?title=$title&type=$type"
    }
    fun getWmsSigningTypeListUrl(): String {
        return "$BASE_URL${RouterConstants.SIGNING_TYPE_LIST}"
    }
    fun getMoveLocationSelectLocationUrl(): String {
        return "$BASE_URL${RouterConstants.MOVE_LOCATION_SELECT_LOCATION}"
    }
    fun getMawbListUrl(): String {
        return "$BASE_URL${RouterConstants.MAWB_LIST}"
    }
    fun getInventoryConversionUrl(): String {
        return "$BASE_URL${RouterConstants.INVENTORY_CONVERSION}"
    }
    fun getConversionTaskUrl(): String {
        return "$BASE_URL${RouterConstants.CONVERSION_TASK}"
    }
    fun getConversionMoveInContainerUrl(): String {
        return "$BASE_URL${RouterConstants.CONVERSION_MOVE_IN_CONTAINER}"
    }
    fun getInventoryInquire(): String {
        return "$BASE_URL${RouterConstants.INVENTORY_INQUIRE}"
    }

    fun getInquireResultUrl(map : HashMap<String,String>): String {

        val params = StringBuffer()
        val iterator = map.keys.iterator()
        while (iterator.hasNext()) {
            val next = iterator.next()
            params.append(next).append('=').append(map[next])

            if (iterator.hasNext()) {
                params.append("&")
            }
        }
        if (StringUtils.isNull(params.toString())) {
            return "$BASE_URL${RouterConstants.INVENTORY_INQUIRE_RESULT}"
        }else {
            return "$BASE_URL${RouterConstants.INVENTORY_INQUIRE_RESULT}?"+ params.toString()
        }
    }

    fun getSpaceTransefer(): String {
        return "$BASE_URL${RouterConstants.SPACE_TRANSFER}"
    }
    fun getHawbBindingUrl(): String {
        return "$BASE_URL${RouterConstants.HAWB_BINDING}"
    }
    fun getSparePartsMoveOutUrl(): String {
        return "$BASE_URL${RouterConstants.SPARE_PARTS_MOVE_OUT}"
    }
    fun getSparePartsMoveInUrl(barcode_list_str :String): String {
        return "$BASE_URL${RouterConstants.SPARE_PARTS_MOVE_IN}?barcode_list_str=$barcode_list_str"
    }

}