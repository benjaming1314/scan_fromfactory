package club.fromfactory.baselibrary.rx.activityresult

import android.content.Intent
import android.support.v4.app.FragmentActivity
import club.fromfactory.baselibrary.extention.log
import io.reactivex.Observable

/**
 * 封装请求activity的回调
 *
 * @param activity 当前的Activity，注意必须是fragmentActivity
 */
class AvoidOnResult(val activity: FragmentActivity?) {
    private val avoidOnResultFragment: AvoidOnResultFragment
        get() = getAvoidOnResultFragment(activity)

    private fun getAvoidOnResultFragment(activity: FragmentActivity?): AvoidOnResultFragment {
        var avoidOnResultFragment: AvoidOnResultFragment? = findAvoidOnResultFragment(activity!!)
        if (avoidOnResultFragment == null) {
            avoidOnResultFragment = AvoidOnResultFragment()
            // fix crash https://fabric.io/jiayunshuju/android/apps/club.fromfactory/issues/5bef179df8b88c29633a9fd2?time=last-thirty-days
            try {
                val fragmentManager = activity
                        .supportFragmentManager
                fragmentManager
                        .beginTransaction()
                        .add(avoidOnResultFragment, TAG)
                        .commitAllowingStateLoss()
                fragmentManager.executePendingTransactions()
            } catch (e: Exception) {
                e.log()
            }
        }
        return avoidOnResultFragment
    }

    private fun findAvoidOnResultFragment(activity: FragmentActivity): AvoidOnResultFragment? {
        val tempFragment = activity.supportFragmentManager.findFragmentByTag(TAG)
        return if (tempFragment != null) tempFragment as AvoidOnResultFragment else null
    }

    fun startForResult(intent: Intent, requestCode: Int): Observable<ActivityResultInfo> {
        return avoidOnResultFragment.startForResult(intent, requestCode)
    }

    fun startForResult(clazz: Class<*>, requestCode: Int): Observable<ActivityResultInfo> {
        val intent = Intent(avoidOnResultFragment.activity, clazz)
        return startForResult(intent, requestCode)
    }

    companion object {
        private val TAG = AvoidOnResult::class.java.simpleName
    }
}