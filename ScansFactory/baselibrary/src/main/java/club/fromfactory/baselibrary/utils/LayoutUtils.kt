package club.fromfactory.baselibrary.utils

import android.support.v4.text.TextUtilsCompat
import android.util.LayoutDirection
import java.util.*

/**
 * 布局的工具类
 *
 * @author nichenjian
 * @date 2018/7/5
 */
object LayoutUtils {

    @JvmStatic
    fun isRtlLayout(): Boolean {
        return TextUtilsCompat.getLayoutDirectionFromLocale(SDK26Utils.getLocalInstance()) == LayoutDirection.RTL
    }
}
