package club.fromfactory.baselibrary.net.retrofit.cache.core

import java.lang.reflect.Type
import java.util.concurrent.TimeUnit

/**
 * 缓存接口
 *
 * @author nichenjian
 * @date 2018/6/21
 */
abstract class ICache {
    /**
     * 加载缓存
     *
     * @param type 类型
     * @param cacheKey 缓存的key
     * @param cacheTime 缓存的有效时间
     */
    @Synchronized
    fun <T> load(type: Type, cacheKey: String, cacheTime: Long, timeUnit: TimeUnit): T? {
        if (!containsKey(cacheKey)) {
            return null
        }

        if (isExpired(cacheKey, cacheTime, timeUnit)) {
            remove(cacheKey)
            return null
        }

        return doLoad(type, cacheKey)
    }

    /**
     * 保存数据
     *
     * @param cacheKey 缓存的key
     * @param value 缓存的对象
     */
    @Synchronized
    fun <T> save(cacheKey: String, value: T): Boolean {
        if (value == null) {
            remove(cacheKey)
        }

        return doSave(cacheKey, value)
    }

    /**
     * 清除数据
     *
     * @param cacheKey 缓存的key
     */
    @Synchronized
    fun remove(cacheKey: String): Boolean {
        return doRemove(cacheKey)
    }

    /**
     * 判断是否缓存
     *
     * @param cacheKey 缓存的Key
     */
    @Synchronized
    fun containsKey(cacheKey: String): Boolean {
        return doContainsKey(cacheKey)
    }

    /**
     * 清空缓存
     */
    @Synchronized
    fun clear(): Boolean {
        return doClear()
    }

    /**
     * 是否已经包含key
     *
     * @param cacheKey 缓存的key
     */
    protected abstract fun doContainsKey(cacheKey: String): Boolean

    /**
     * 是否过期
     *
     * @param cacheKey 缓存的key
     * @param cacheTime 缓存的时长
     */
    protected abstract fun isExpired(cacheKey: String, cacheTime: Long, timeUnit: TimeUnit): Boolean

    /**
     * 读取缓存
     *
     * @param type 类型
     * @param cacheKey 缓存的key
     */
    protected abstract fun <T> doLoad(type: Type, cacheKey: String): T?

    /**
     * 保存缓存
     */
    protected abstract fun <T> doSave(cacheKey: String, value: T): Boolean

    /**
     * 删除某条缓存
     */
    protected abstract fun doRemove(cacheKey: String): Boolean

    /**
     * 清空缓存
     */
    protected abstract fun doClear(): Boolean
}
