package club.fromfactory.baselibrary.utils

import android.net.Uri
import java.util.*

object AppLinkUtils {
    /**
     * unlink的域名 (目前作为 appLink 的入口)
     */
    private val APP_LINK_HOST = ArrayList<String>()
    private const val HTTP_SCHEME = "http"
    private const val HTTPS_SCHEME = "https"
    private const val ULINK_HOST = "ulink.fromfactory.club"
    private const val WWW_HOST = "www.clubfactory.com"

    init {
        APP_LINK_HOST.add(ULINK_HOST)
        APP_LINK_HOST.add(WWW_HOST)
    }

    /**
     * 判断是否是appink 目前支持的host
     *
     * @param uri 需要跳转的url
     * @return 是否是appLink链接
     */
    fun isAppLink(uri: Uri?): Boolean {
        if (uri == null) {
            return false
        }

        val scheme = uri.scheme
        val host = uri.host

        return APP_LINK_HOST.contains(host) && (HTTP_SCHEME == scheme || HTTPS_SCHEME == scheme)
    }
}
