package club.fromfactory.baselibrary.statistic.constants;

/**
 * 模块Id常量
 * <p>
 * 目前设置最多12个模块，基本能覆盖所有页面
 *
 * @author nichenjian
 * @date 2018/7/30
 */
public class ModuleId {
    public static final int ID_1 = 1;
    public static final int ID_2 = 2;
    public static final int ID_3 = 3;
    public static final int ID_4 = 4;
    public static final int ID_5 = 5;
    public static final int ID_6 = 6;
    public static final int ID_7 = 7;
    public static final int ID_8 = 8;
    public static final int ID_9 = 9;
    public static final int ID_10 = 10;
    public static final int ID_11 = 11;
    public static final int ID_12 = 12;
}
