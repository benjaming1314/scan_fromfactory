package club.fromfactory.baselibrary.model;


import java.util.List;

/**
 * Created by nichenjian on 2018/3/16.
 */

public class BaseResponse<T> implements NoProguard {
    public List<ResponseErrorMessage> errorMessages;
    public ResponseErrorMessage firstErrorMessage;
    private boolean success ;
    public T model;


    public BaseResponse() {
    }

    public boolean isSuccess() {
        return success;
    }

    public int getErrorCode() {
        try {
            return Integer.parseInt(firstErrorMessage.getErrorCode());
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return -1;
        }
    }


}
