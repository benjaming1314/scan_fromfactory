package club.fromfactory.baselibrary.statistic

import android.app.Service
import android.content.Intent
import android.os.IBinder

/**
 * stat service
 *
 * The main purpose is to flush events immediately when service is destroy and the task is removed
 * Created by nichenjian on 2018/5/21.
 */
class StatService : Service() {
    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onDestroy() {
        shutdown()
        super.onDestroy()
    }

    override fun onTaskRemoved(rootIntent: Intent?) {
        shutdown()
        super.onTaskRemoved(rootIntent)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        return Service.START_NOT_STICKY
    }

    private fun shutdown() {
        StatManager.flushEventsQueneImmediately()
        StatEventManager.flushEventsQueneImmediately()
        try {
            Thread.sleep(1000L)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}
