package club.fromfactory.baselibrary.utils;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import club.fromfactory.baselibrary.ConstantsKt;
import com.crashlytics.android.Crashlytics;

/**
 * Created by Administrator on 2018/4/10/010.
 */

public class PreferenceStorageUtils {

    private static final String IS_FIRST_INSTALL = "is_first_install";

    private static final String HOUSE_SERVER_PATH = "house_server_path";
    private static final String HOUSE_SERVER_PATH_LOGIN = "house_server_path_login";
    private static final String HOUSE_SERVER_PATH_PROCUREMENT = "house_server_path_procurement";
    private static final String HOUSE_SERVER_PATH_TRACKING = "house_server_path_tracking";
    private static final String HOUSE_NAME = "house_name";
    private static final String HOUSE_PDA_TYPE = "house_pda_type";
    private static final String HOUSE_SERVER_LOCATION = "house_server_location";
    private static final String WAREHOUSE_SERVER = "warehouse_server";

    private static final String COUNTRY_SELECT = "country_select";//存储选中的国家
    private static final String SELECT_LANGUAGE = "select_language";//当前语言的

    private final String DEVELOPENT_STATE = "developentstate_state";
    private final String DEBUG_STATE = "debug_state";

    //保存用户名和密码
    private static final String USER_USERNAME = "username";
    private static final String USER_PASSWORD = "password";
    private static final String USER_TOKEN = "token";


    private static final String WAREHOUSE_PDA_INFO= "warehouse_pda_info";

    private static PreferenceStorageUtils mPreferenceStorageUtils = new PreferenceStorageUtils();

    public static PreferenceStorageUtils getInstance() {
        return mPreferenceStorageUtils;
    }

    private PreferenceUtils getDefaultPreferenceUtils() {
        return PreferenceUtils.getInstance();
    }

    public void setWarehousePdaInfo(String str) {
        getDefaultPreferenceUtils().setStringValue(WAREHOUSE_PDA_INFO,str);
    }

    public String getWarehousePdaInfo() {
        return getDefaultPreferenceUtils().getStringValue(WAREHOUSE_PDA_INFO);
    }

    /**
     * 获取debug开关状态
     */
    public boolean getIsFirstInstall() {
        return getDefaultPreferenceUtils().getBooleanValue(IS_FIRST_INSTALL, true);
    }

    /**
     * 保存debug开关状态
     */
    public void saveIsFirstInstall(boolean isFirst) {
        getDefaultPreferenceUtils().setBooleanValue(IS_FIRST_INSTALL, isFirst);
    }
    public void clearIsFirstData() {
        getDefaultPreferenceUtils().clearData(IS_FIRST_INSTALL);
    }
    /**
     * 获取debug开关状态
     */
    public boolean getDebugState() {
        return getDefaultPreferenceUtils().getBooleanValue(DEBUG_STATE, false);
    }

    /**
     * 保存debug开关状态
     */
    public void saveDebugState(boolean isState) {
        getDefaultPreferenceUtils().setBooleanValue(DEBUG_STATE, isState);
    }

    /**
     */
    public boolean getDevelopentState() {
        return getDefaultPreferenceUtils().getBooleanValue(DEVELOPENT_STATE, false);
    }

    /**
     */
    public void saveDevelopentState(boolean isState) {
        getDefaultPreferenceUtils().setBooleanValue(DEVELOPENT_STATE, isState);
    }
    /**
     * 获取Username
     */
    public String getUserName() {
        return getDefaultPreferenceUtils().getStringValue(USER_USERNAME);
    }

    /**
     * 保存Ussername
     */
    public void saveUserName(String str) {
        getDefaultPreferenceUtils().setStringValue(USER_USERNAME, str);
    }

    /**
     */
    public String getPassword() {
        return getDefaultPreferenceUtils().getStringValue(USER_PASSWORD);
    }

    /**
     */
    public void savePassword(String str) {
        getDefaultPreferenceUtils().setStringValue(USER_PASSWORD, str);
    }

    /**
     */
    public String getToken() {
        return getDefaultPreferenceUtils().getStringValue(USER_TOKEN);
    }

    /**
     */
    public void saveToken(String str) {
        getDefaultPreferenceUtils().setStringValue(USER_TOKEN, str);
    }
    /**
     */
    public String getPdaType() {
        return getDefaultPreferenceUtils().getStringValue(HOUSE_PDA_TYPE);
    }

    /**
     */
    public void savePdaType(String str) {
        getDefaultPreferenceUtils().setStringValue(HOUSE_PDA_TYPE, str);
    }
    /**
     */
    public String getHouseName() {
        return getDefaultPreferenceUtils().getStringValue(HOUSE_NAME);
    }

    /**
     */
    public void saveHouseName(String str) {
        getDefaultPreferenceUtils().setStringValue(HOUSE_NAME, str);
    }
    /**
     */
    public String getHouseServerPath() {
        return getDefaultPreferenceUtils().getStringValue(HOUSE_SERVER_PATH);
    }

    /**
     */
    public void saveHouseServerPath(String str) {
        getDefaultPreferenceUtils().setStringValue(HOUSE_SERVER_PATH, str);
    }
    /**
     */
    public String getHouseServerPathLogin() {
        return getDefaultPreferenceUtils().getStringValue(HOUSE_SERVER_PATH_LOGIN);
    }

    /**
     */
    public void saveHouseServerPathLogin(String str) {
        getDefaultPreferenceUtils().setStringValue(HOUSE_SERVER_PATH_LOGIN, str);
    }

    /**
     */
    public String getHouseServerPathProcurement() {
        return getDefaultPreferenceUtils().getStringValue(HOUSE_SERVER_PATH_PROCUREMENT);
    }

    /**
     */
    public void saveHouseServerPathProcurement(String str) {
        getDefaultPreferenceUtils().setStringValue(HOUSE_SERVER_PATH_PROCUREMENT, str);
    }

    /**
     */
    public String getHouseServerPathTracking() {
        return getDefaultPreferenceUtils().getStringValue(HOUSE_SERVER_PATH_TRACKING);
    }

    /**
     */
    public void saveHouseServerPathTracking(String str) {
        getDefaultPreferenceUtils().setStringValue(HOUSE_SERVER_PATH_TRACKING, str);
    }

    /**
     */
    public String getHouseServerLocation() {
        return getDefaultPreferenceUtils().getStringValue(HOUSE_SERVER_LOCATION);
    }

    /**
     */
    public String getWareHouseInfoServer() {
        return getDefaultPreferenceUtils().getStringValue(WAREHOUSE_SERVER);
    }

    /**
     */
    public void saveWareHouseInfoServer(String str) {
        getDefaultPreferenceUtils().setStringValue(WAREHOUSE_SERVER, str);
    }

    /**
     */
    public void saveHouseServerLocation(String str) {
        getDefaultPreferenceUtils().setStringValue(HOUSE_SERVER_LOCATION, str);
    }

    /**
     */
    public String getAppSelectLanguage() {
        return getDefaultPreferenceUtils().getStringValue(SELECT_LANGUAGE);
    }
    /**
     */
    public void saveAppSelectLanguage(String str) {
        getDefaultPreferenceUtils().setStringValue(SELECT_LANGUAGE, str);
    }

    /**
     */
    public String getSelectCountry() {
        return getDefaultPreferenceUtils().getStringValue(COUNTRY_SELECT);
    }
    /**
     */
    public void saveSelectCountry(String str) {
        getDefaultPreferenceUtils().setStringValue(COUNTRY_SELECT, str);
    }

}
