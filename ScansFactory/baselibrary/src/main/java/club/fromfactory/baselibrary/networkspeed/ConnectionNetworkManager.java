package club.fromfactory.baselibrary.networkspeed;

/**
 * 网络连接管理
 */
class ConnectionNetworkManager {
    /*
     * The factor used to calculate the current bandwidth
     * depending upon the previous calculated value for bandwidth.
     * <p>
     * The smaller this value is, the less responsive to new samples the moving average becomes.
     */
    private static final double DEFAULT_DECAY_CONSTANT = 0.05;

    private static final int BYTES_TO_BITS = 8;
    private static final long BANDWIDTH_LOWER_BOUND = 10;

    private ExponentialGeometricAverage mDownloadBandwidth
            = new ExponentialGeometricAverage(DEFAULT_DECAY_CONSTANT);

    /**
     * singleton
     */
    private static class ConnectionNetworkManagerHolder {
        public static ConnectionNetworkManager instance = new ConnectionNetworkManager();
    }

    public static ConnectionNetworkManager getInstance() {
        return ConnectionNetworkManagerHolder.instance;
    }

    public void addSample(long byteDiff, long time) {
        if (time == 0 || (byteDiff) * 1.0 / (time) * BYTES_TO_BITS < BANDWIDTH_LOWER_BOUND) {
            return;
        }

        double bandwidth = (byteDiff) * 1.0 / (time) * BYTES_TO_BITS;
        if (mDownloadBandwidth != null) {
            mDownloadBandwidth.addMeasurement(bandwidth);
        }
    }

    public long calculateBandWidth() {
        return (long) mDownloadBandwidth.getAverage();
    }

    public void reset() {
        if (mDownloadBandwidth != null) {
            mDownloadBandwidth.reset();
        }
    }
}
