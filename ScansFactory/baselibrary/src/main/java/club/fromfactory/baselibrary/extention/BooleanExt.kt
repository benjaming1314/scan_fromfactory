package club.fromfactory.baselibrary.extention

/**
 *
 * @author 王佳斌
 * @date 2018/12/4
 */
/**
 * boolean 扩展，定义成协变
 */
sealed class BooleanExt<out T>

object Otherwise : BooleanExt<Nothing>()

class TransferData<T>(val data: T) : BooleanExt<T>()

//声明成inline函数
inline fun <T> Boolean.yes(block: () -> T): BooleanExt<T> = when {
    this -> {
        TransferData(block.invoke())
    }
    else -> Otherwise
}

inline fun <T> BooleanExt<T>.otherwise(block: () -> T): T = when (this) {
    is Otherwise ->
        block()
    is TransferData ->
        this.data
}
