package club.fromfactory.baselibrary.widget.recyclerview

import android.view.ViewGroup

/**
 * 基础的多类型适配器
 *
 * @author Jellybean
 * @date 2018/6/25
 */
abstract class BaseMultiTypeAdapter : BaseRecyclerAdapter<Any> {

    val manager = ViewHolderCreatorManager()

    constructor() : super()
    constructor(data: List<Any>) : super(data)

    override fun getItemViewType(position: Int): Int {
        return manager.getItemViewType(data, position)
    }


    override fun onCreateBaseViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Any> {
        return manager.onCreateViewHolder(parent, viewType) as BaseViewHolder<Any>
    }


}