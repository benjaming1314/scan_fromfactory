package club.fromfactory.baselibrary.view

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.support.annotation.IdRes
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatDelegate
import android.view.*
import android.view.inputmethod.InputMethodManager
import androidx.core.view.doOnPreDraw
import butterknife.ButterKnife
import club.fromfactory.baselibrary.extention.log
import club.fromfactory.baselibrary.language.LanguageUtils
import club.fromfactory.baselibrary.model.TraceInfo
import club.fromfactory.baselibrary.router.RouterManager
import club.fromfactory.baselibrary.router.RouterUrlProvider
import club.fromfactory.baselibrary.statistic.PageId
import club.fromfactory.baselibrary.statistic.PagePerformanceRecorder
import club.fromfactory.baselibrary.statistic.PerformanceStatisticsUtils
import club.fromfactory.baselibrary.statistic.utils.StatAddEventUtil
import club.fromfactory.baselibrary.statistic.utils.StatUtil
import club.fromfactory.baselibrary.utils.AdjustLayoutSize
import club.fromfactory.baselibrary.utils.StatusBarUtils
//import club.fromfactory.baselibrary.widget.BaseWebView
import club.fromfactory.baselibrary.widget.StatusBarCompatRootLayout
import club.fromfactory.routerannotaions.Router
import com.blankj.utilcode.util.ActivityUtils
import org.greenrobot.eventbus.EventBus
import java.lang.ref.WeakReference

abstract class BaseActivity : RxAppCompatActivity(), IBaseView {

    /**
     * 本页面的追踪信息
     */
    @JvmField
    protected var pageTraceInfo: TraceInfo? = null

    /**
     * 上一个页面的traceInfo
     */
    protected var previousPageTraceInfo: TraceInfo? = null

    /**
     * 页面性能统计
     */
    @JvmField
    protected val performancePageData = PerformanceStatisticsUtils.PerformancePageData(this)

    private var statusBarCompatRootLayout: StatusBarCompatRootLayout? = null

    init {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    /**
     * 是否在Activity栈空的时候需要打开首页
     */
    open fun isNeedOpenMainActivityWhenFinish(): Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        topActivity = WeakReference(this)
        performancePageData.setNativeStartTime().setPageRenderStartTime()
        // fix crash https://fabric.io/jiayunshuju/android/apps/club.fromfactory/issues/5bcfd024f8b88c2963f150cc
        try {
            super.onCreate(savedInstanceState)
        } catch (e: Throwable) {
            e.printStackTrace()
        }
        initInner()
        setContentView()
        window?.decorView?.doOnPreDraw {
            performancePageData.setNativeEndTime()
        }
        ButterKnife.bind(this)
        initData()
        initPageTraceInfo()
        setPageStatInfo()
        initView()
        performancePageData.setNetworkStartTime()
        fetchData()
    }


    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {

        when (event.keyCode) {
            KeyEvent.ACTION_DOWN -> {
            }
            KeyEvent.KEYCODE_BACK -> {
                clickKeyCodeBack()
                return true
            }
        }
        return super.onKeyDown(keyCode, event)
    }

    open fun clickKeyCodeBack() {
        finish()
    }

    /**
     * 页面曝光日志
     */
    open fun addPageImpressionStat() {
        StatAddEventUtil.addPageImpressionEvent(this)
    }

    override fun startActivityFromFragment(fragment: Fragment?, intent: Intent?, requestCode: Int, options: Bundle?) {
        injectTraceInfoInIntent(intent)
        super.startActivityFromFragment(fragment, intent, requestCode, options)
    }

    private fun injectTraceInfoInIntent(intent: Intent?) {
        if (intent != null && (intent.action != null && intent.action.contains("club.fromfactory")
                        || intent.component != null && intent.component.className.contains("club.fromfactory"))) {
            intent.putExtra(PAGE_TRACE_INFO, traceInfo)
        }
    }

    override fun startActivityForResult(intent: Intent, requestCode: Int) {
        injectTraceInfoInIntent(intent)
        super.startActivityForResult(intent, requestCode)
    }

    /**
     * Activity内部初始化
     */
    protected open fun initInner() {
        previousPageTraceInfo = intent?.extras?.getParcelable(PAGE_TRACE_INFO)

    }

    /**
     * 设置视图
     */
    protected open fun setContentView() {
        if (useCustomStatusBar()) {
            setContentViewWithCustomStatusBar(layoutResId)
        } else {
            setContentView(layoutResId)
        }
    }

    /**
     * 设置是否需要占位的statusBar
     *
     * @param layoutResId 渲染的layoutId
     */
    private fun setContentViewWithCustomStatusBar(@LayoutRes layoutResId: Int) {
        statusBarCompatRootLayout = StatusBarCompatRootLayout(this)
        statusBarCompatRootLayout!!.showStatusBar(true)
        statusBarCompatRootLayout!!.setStatusBarViewBg(Color.WHITE)
        val view = LayoutInflater.from(this)
                .inflate(layoutResId, statusBarCompatRootLayout, false)
        statusBarCompatRootLayout!!.addView(view)
        super.setContentView(statusBarCompatRootLayout)
        AdjustLayoutSize.assistActivity(findViewById(android.R.id.content))
        setDefaultSystemStatusBarStatus()
    }

    protected open fun useCustomStatusBar(): Boolean {
        return Build.VERSION.SDK_INT >= 21
    }

    /**
     * 设置默认的沉浸式为全透明
     */
    private fun setDefaultSystemStatusBarStatus() {
        if (Build.VERSION.SDK_INT >= 21) {
            val window = window
            window.statusBarColor = Color.TRANSPARENT
            this.window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View
                    .SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        } else {
            statusBarCompatRootLayout?.showStatusBar(false)
        }
    }

    override fun onResume() {
        // fix crash https://fabric.io/jiayunshuju/android/apps/club.fromfactory/issues/5bc8041bf8b88c296337b63e
        try {
            super.onResume()
            updateStatusBar()
        } catch (e: Exception) {
            e.log()
        }
    }

    fun showStatusBarView(isShow: Boolean) {
        if (statusBarCompatRootLayout != null) {
            statusBarCompatRootLayout!!.showStatusBar(isShow)
        }
    }

    // 更新statusBar的状态
    open fun updateStatusBar() {
        StatusBarUtils.updateStatusBar(this, StatusBarUtils.StatusBarColor.WHITE, false, true)
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(LanguageUtils.onAttach(newBase))
    }

    override fun getTraceInfo(): TraceInfo? {
        return pageTraceInfo
    }

    /**
     * 请求全屏显示
     */
    protected fun requestFullScreen() {
        requestWindowFeature(Window.FEATURE_NO_TITLE)//remove title bar  即隐藏标题栏
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)//remove notification bar  即全屏
    }

    protected fun hideBottomUIMenu() {
        val decorView = window.decorView
        val uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or View.SYSTEM_UI_FLAG_FULLSCREEN
        decorView.systemUiVisibility = uiOptions
    }

    fun showOrHideSoftInputWindow() {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

    }

    override fun onStart() {
        super.onStart()
        topActivity = WeakReference(this)
        addPageImpressionStat()
        onPageShow()
    }

    private fun onPageShow() {
//        webView?.executeJsCode(IBaseView.ON_PAGE_SHOW)
    }

    private fun onPageHide() {
        StatAddEventUtil.addPageStayTimeEvent(this)
//        webView?.executeJsCode(IBaseView.ON_PAGE_HIDE)
    }

    override fun onStop() {
        // fix crash https://fabric.io/jiayunshuju/android/apps/club.fromfactory/issues/5bc86996f8b88c29634080e5
        try {
            onPageHide()
            super.onStop()
        } catch (e: Exception) {
            e.log()
        }
    }


    override fun finish() {
        // fix crash https://fabric.io/jiayunshuju/android/apps/club.fromfactory/issues/5bca207df8b88c29636e5e66?time=last-seven-days
        try {
            super.finish()
            if (ActivityUtils.getActivityList().size == 1 && isNeedOpenMainActivityWhenFinish()) {
//                RouterManager.open(this, RouterUrlProvider.getMainUrl())
            }
        } catch (e: Exception) {
            e.log()
        }
    }

    fun registerEventBus() {
        if (!EventBus.getDefault().isRegistered(this)) {
            // fix crash https://fabric.io/jiayunshuju/android/apps/club.fromfactory/issues/5b3b46e56007d59fcd7e67bd?time=last-thirty-days
            try {
                EventBus.getDefault().register(this)
            } catch (e: java.lang.Exception) {
                e.log()
            }
        }
    }

    fun unregisterEventBus() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this)
        }
    }

    override fun initData() {
        // 依赖注入
        RouterManager.bind(this, intent.extras)
    }


    override fun initView() {
        // default empty
    }

    override fun fetchData() {
        // default empty
    }

    override fun showErrorMessage(message: String) {
        // default empty
    }

    private fun initPageTraceInfo() {
        // 生成当前页面的traceInfo
        pageTraceInfo = TraceInfo(previousPageTraceInfo?.pageId ?: 0,
                StatUtil.getPageId(this), previousPageTraceInfo?.url, webViewUrl)
    }

    override fun setPageStatInfo() {
        // default empty
    }

    override fun showLoadingView() {
        // default empty
    }

    override fun hideLoadingView() {
        // default empty
    }

    override fun isAlive(): Boolean {
        return !isDestroyed && !isFinishing
    }

    /**
     * 页面结束渲染记录,有三种情况：
     * 1. 二次渲染，IO处理结束，绑定数据到视图操作结束后再调用此方法
     * 2. 没有网络请求，第一次渲染结束就调用
     * 3. WebView加载结束
     */
    protected fun logPageRenderEnd() {
        window?.decorView?.doOnPreDraw {
            performancePageData.setPageRenderEndTime()
        }
    }


    override fun getPagePerformanceRecorder(): PagePerformanceRecorder {
        return performancePageData
    }

    override fun getWebViewUrl(): String? {
        return null
    }

    override fun getContext(): Context {
        return this
    }

    companion object {
        private const val DEFAULT_LINK_PREFIX = "fromfactory://app.fromfactory.club"
        const val PAGE_TRACE_INFO = "page_trace_info"
        var topActivity: WeakReference<Activity>? = null
        fun getTopActivity(): Activity? {
            return topActivity?.get()
        }
    }

    /**
     * 获取from来源参数 主要用来统计
     */
    @Deprecated("")
    open fun getFrom(): String {
        return ""
    }

    /**
     * 页面的标识 主要用来做统计
     *
     * 只要 PageName 不为空 就会 自动上报页面的PV和页面的停留时长
     */
    @Deprecated("")
    open fun getPageName(): String {
        return ""
    }

    override fun getPageId(): Int {
        val pageId = this.javaClass.getAnnotation(PageId::class.java)
        if (pageId != null) {
            return pageId.value
        }
        return 0
    }

//    override fun getWebView(): BaseWebView? {
//        return null
//    }

    fun hideFragment(transaction: FragmentTransaction, baseFragment: BaseFragment?) {
        if (baseFragment != null && baseFragment.isAdded) {
            transaction.hide(baseFragment)
        }
    }

    open fun showFragment(transaction: FragmentTransaction,
                          baseFragment: BaseFragment?,
                          bundle: Bundle?,
                          @IdRes id: Int) {
        if (baseFragment == null) {
            return
        }

        if (!baseFragment.isAdded) {
            if (bundle != null) {
                baseFragment.arguments = bundle
            }
            transaction.add(id, baseFragment)
        } else {
            transaction.show(baseFragment)
        }
    }

    /**
     * 获取页面Url
     *
     * 默认提供 deepLink 打开的链接
     */
    open fun getPageUrl(): String? {
        val pathArr = this.javaClass.getAnnotation(Router::class.java)
        if (pathArr != null) {
            val path = pathArr.value[0]
            return "$DEFAULT_LINK_PREFIX$path"
        }

        return null
    }

}
