package club.fromfactory.baselibrary.view;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import club.fromfactory.baselibrary.model.TraceInfo;
import club.fromfactory.baselibrary.statistic.PagePerformanceRecorder;
//import club.fromfactory.baselibrary.widget.BaseWebView;

/**
 * Created by lxm on 2017/8/3. UI update
 */

public interface IBaseView {

    /**
     * 页面显示的js生命周期方法
     */
    String ON_PAGE_SHOW = "if (window.CFNative != null && window.CFNative" +
            ".hasOwnProperty(\"onPageShow\")) {window.CFNative.onPageShow()}";

    /**
     * 页面隐藏的js生命周期方法
     */
    String ON_PAGE_HIDE = "if (window.CFNative != null && window.CFNative" +
            ".hasOwnProperty(\"onPageHide\")) {window.CFNative.onPageHide()}";

    /**
     * 获取布局文件的id
     *
     * @return 布局文件的id
     */
    @LayoutRes
    int getLayoutResId();

    /**
     * 初始化数据
     */
    void initData();

    /**
     * 初始化View
     */
    void initView();

    /**
     * 请求数据
     */
    void fetchData();

    /**
     * 显示错误信息
     *
     * @param message 信息
     */
    void showErrorMessage(String message);

    /**
     * 显示加载提示视图
     */
    void showLoadingView();

    /**
     * 隐藏加载视图
     */
    void hideLoadingView();

    /**
     * 视图是否还活跃
     */
    boolean isAlive();

    /**
     * 设置页面统计相关数据
     */
    void setPageStatInfo();

    /**
     * 获取webView的请求url
     *
     * @return 请求的url
     */
    @Nullable
    String getWebViewUrl();


    /**
     * 获取跟踪数据
     *
     * @return traceInfo 页面的跟踪数据
     */
    @Nullable
    TraceInfo getTraceInfo();

    /**
     * 获取context
     *
     * @return 获取当前的context
     */
    @NonNull
    Context getContext();

    /**
     * 获取页面性能统计的对象
     */
    @NonNull
    PagePerformanceRecorder getPagePerformanceRecorder();

    /**
     * 获取页面的统计id
     *
     * @return 页面统计id
     */
    int getPageId();

//    /**
//     * 获取页面的webView
//     *
//     * @return 页面webView
//     */
//    BaseWebView getWebView();
}
