package club.fromfactory.baselibrary.net.retrofit.cache.model

/**
 * 网络请求封装类
 *
 * @author nichenjian
 * @date 2018/6/21
 */
class Reply<T>(
        /**
         * 是否从缓存中读取
         */
        var isFromCache: Boolean,
        /**
         * 被包装的数据
         */
        var data: T)

