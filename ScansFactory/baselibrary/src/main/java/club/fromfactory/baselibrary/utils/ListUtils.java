package club.fromfactory.baselibrary.utils;

import java.util.List;

public class ListUtils {
    public static <T> T getItem(List<T> list, int position) {
        if (list == null || list.isEmpty() || position < 0 || position > list.size() - 1) {
            return null;
        }

        return list.get(position);
    }

    /**
     * 列表是否是空
     *
     * @param list 列表
     * @return 如果list是null 或者空的则返回true
     */
    public static boolean isNullOrEmpty(List list) {
        return list == null || list.isEmpty();
    }

    /**
     * 列表不为空
     *
     * @param list 列表
     * @return 如果list不为空则返回true
     */
    public static boolean isNotBlank(List list) {
        return !isNullOrEmpty(list);
    }
}
