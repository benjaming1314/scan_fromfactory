package club.fromfactory.baselibrary.model

/**
 * 分页信息
 *
 * @author Jellybean
 * @date 2018/6/20
 */
/**
 * 默认的每页条数
 */
const val DEFAULT_PAGE_SIZE = 20
/**
 * 起始页码
 */
const val START_PAGE_NO = 1

open class PageInfo : NoProguard {
    /**
     * 页码
     */
    var pageNo: Int = START_PAGE_NO
    /**
     * 每页条数
     */
    var pageSize: Int = DEFAULT_PAGE_SIZE
}
