package club.fromfactory.baselibrary.utils

import java.math.BigDecimal

object DoubleUtils {

    /**
     * 对double数据进行取精度.
     * @param value  double数据.
     * @param scale  精度位数(保留的小数位数).
     * @param roundingMode  精度取值方式.
     * @return 精度计算后的数据.
     */
    fun round(value: Double, scale: Int, roundingMode: Int): Double {
        var bd: BigDecimal? = BigDecimal(value)
        bd = bd!!.setScale(scale, roundingMode)
        val d = bd!!.toDouble()
        bd = null
        return d
    }

    /**
     *
     * double 相加
     *
     * @param d1
     *
     * @param d2
     *
     * @return
     */

    fun sum(d1: Double, d2: Double): Double {

        val bd1 = BigDecimal(java.lang.Double.toString(d1))

        val bd2 = BigDecimal(java.lang.Double.toString(d2))

        return bd1.add(bd2).toDouble()

    }


    /**
     *
     * double 相减
     *
     * @param d1
     *
     * @param d2
     *
     * @return
     */

    fun sub(d1: Double, d2: Double): Double {

        val bd1 = BigDecimal(java.lang.Double.toString(d1))

        val bd2 = BigDecimal(java.lang.Double.toString(d2))

        return bd1.subtract(bd2).toDouble()

    }


    /**
     *
     * double 乘法
     *
     * @param d1
     *
     * @param d2
     *
     * @return
     */

    fun mul(d1: Double, d2: Double): Double {

        val bd1 = BigDecimal(java.lang.Double.toString(d1))

        val bd2 = BigDecimal(java.lang.Double.toString(d2))

        return bd1.multiply(bd2).toDouble()

    }


    /**
     *
     * double 除法
     *
     * @param d1
     *
     * @param d2
     *
     * @param scale 四舍五入 小数点位数
     *
     * @return
     */

    fun div(d1: Double, d2: Double, scale: Int): Double {

        //  当然在此之前，你要判断分母是否为0，

        //  为0你可以根据实际需求做相应的处理

        val bd1 = BigDecimal(java.lang.Double.toString(d1))

        val bd2 = BigDecimal(java.lang.Double.toString(d2))

        return bd1.divide(bd2, scale, BigDecimal.ROUND_HALF_UP).toDouble()

    }

}

