package club.fromfactory.baselibrary.statistic.constants

/**
 *@author lxm
 *@date 2018/7/27/027
 */
/**
 * 页面id
 */
const val MID = "mid"
/**
 * app初始化开始时间戳
 */
const val START_TIME = "start_time"

/**
 * app初始化结束时间戳
 */
const val END_TIME = "end_time"