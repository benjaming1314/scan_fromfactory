package club.fromfactory.baselibrary.install;

import club.fromfactory.baselibrary.install.model.Switch;
import club.fromfactory.baselibrary.model.BaseResponse;
import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

import java.util.List;
import java.util.Map;

public interface IAppInstallService {
    @GET("switch")
    Observable<BaseResponse<Switch>> requestSwitch();

    @POST("app_install")
    Observable<BaseResponse> uploadAppInstall(@Body Map<String, List<String>> appInfo);
}
