//package club.fromfactory.baselibrary.widget
//
//import android.content.Context
//import android.os.Build
//import android.util.AttributeSet
//import com.github.lzyzsd.jsbridge.BridgeWebView
//
///**
// * 通用的webView
// *
// * @author nichenjian
// * @date 2018/8/20
// */
//open class BaseWebView : BridgeWebView {
//    constructor(context: Context) : super(context)
//    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet)
//    constructor(context: Context, attributeSet: AttributeSet, defStyleAttr: Int) : super(context, attributeSet, defStyleAttr)
//
//    /**
//     * 调用js方法
//     *
//     * @param jsCode 需要调用的js方法
//     */
//    fun executeJsCode(jsCode: String) {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            evaluateJavascript(jsCode, null)
//        } else {
//            loadUrl("javascript:$jsCode")
//        }
//    }
//}
