package club.fromfactory.baselibrary.rx

import club.fromfactory.baselibrary.view.IBaseView
import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.ObservableTransformer
import io.reactivex.android.schedulers.AndroidSchedulers

/**
 * 显示/隐藏加载视图的Observable转换器
 * @author 王佳斌
 * @date 2018/9/12
 */
class LoadingViewComposer<T>(val baseView: IBaseView) : ObservableTransformer<T, T> {
    override fun apply(upstream: Observable<T>): ObservableSource<T> {
        return upstream.doOnSubscribe { baseView.showLoadingView() }
                .subscribeOn(AndroidSchedulers.mainThread())
                .doOnTerminate { baseView.hideLoadingView() }
    }

}

fun <T> Observable<T>.loadingView(baseView: IBaseView): Observable<T> {
    return this.compose(LoadingViewComposer(baseView))
}