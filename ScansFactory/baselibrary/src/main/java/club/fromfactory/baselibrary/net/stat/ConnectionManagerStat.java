package club.fromfactory.baselibrary.net.stat;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;

/**
 * 网络连接状态管理
 */
public class ConnectionManagerStat {
    private boolean isSampling = false;
    private HandlerThread mThread;
    private SamplingHandler mHandler;

    private ConnectionManagerStat() {
        mThread = new HandlerThread("connectManager");
        mThread.start();
        mHandler = new SamplingHandler(mThread.getLooper());
    }

    // singleton
    private static class ConnectionManagerStatHolder {
        static final ConnectionManagerStat instance = new ConnectionManagerStat();
    }

    public static ConnectionManagerStat getInstance() {
        return ConnectionManagerStatHolder.instance;
    }

    // 添加采样
    private void addSample() {

    }

    /**
     * 开始采样
     */
    public void startSampling() {
        if (isSampling) {
            return;
        }

        isSampling = true;
        mHandler.startSamplingThread();
    }

    /**
     * 结束采样
     */
    public void stopSampling() {
        if (isSampling) {
            isSampling = false;
        }

        mHandler.stopSamplingThread();
    }

    private class SamplingHandler extends Handler {
        // 采样消息类型
        static private final int MSG_START = 1;
        // 采样间隔时间
        private static final int SAMPLE_TIME = 1000;

        SamplingHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_START:
                    addSample();
                    sendEmptyMessageDelayed(MSG_START, SAMPLE_TIME);
                    break;
            }
        }

        // 发送开始采样的消息
        private void startSamplingThread() {
            sendEmptyMessage(SamplingHandler.MSG_START);
        }

        private void stopSamplingThread() {
            removeMessages(SamplingHandler.MSG_START);
        }
    }
}