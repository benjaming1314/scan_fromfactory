package club.fromfactory.baselibrary.net.retrofit

import club.fromfactory.baselibrary.net.CookieHelper
import com.blankj.utilcode.util.LogUtils
import okhttp3.Interceptor
import okhttp3.Response
import org.json.JSONObject
import java.io.IOException

open class BaseInterceptor : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()

        val requestBuilder = request.newBuilder()

        // 请求带上cookie参数
        requestBuilder.url(request.url())
                .addHeader("Accept", "application/json, text/javascript, */*; q=0.01")

        // 如果没有设置
        if (request.header("Cookie") == null) {
            requestBuilder.addHeader("Cookie",CookieHelper.getBaseCookieString(null))
        }else {
            requestBuilder.header("Cookie",CookieHelper.getBaseCookieString(request.header("Cookie")))
        }

        // 如果没有设置
        if (request.header("Content-Type") == null) {
            requestBuilder.addHeader("Content-Type", "application/json")
        }


        // 获取返回
        val response = chain.proceed(requestBuilder.build())

        // 获取返回的cookie值
        val headers = response.headers("set-cookie")

        if (headers.isNotEmpty()) {
            // 将cookie设置到webView中
            headers.forEach {
                CookieHelper.setCookie(it)
            }

            CookieHelper.flush()
        }

        return response
    }

}
