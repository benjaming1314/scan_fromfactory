package club.fromfactory.baselibrary.statistic.pages

/**
 * sns社区页面的统计常量
 * Created by nichenjian on 2018/5/21.
 */
class SnsPageConstants {
    companion object {
        /**
         * 视频是否有声音
         */
        val sound = "snd"
        /**
         * 播放时长
         */
        val playTime = "pt"
        /**
         * 性能
         */
        val performance = "val"
    }
}