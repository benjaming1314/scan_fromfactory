package club.fromfactory.baselibrary.router;

public class RouterConstants {

    public static final String MAIN = "/" + PageConstants.MAIN;
    public static final String SIGNING_TYPE_LIST = "/" + PageConstants.SIGNING_TYPE_LIST;
    public static final String SIGNING_TASK_LIST = "/" + PageConstants.SIGNING_TASK_LIST;
    public static final String WMS_SIGNING_DETAIL = "/" + PageConstants.WMS_SIGNING_DETAIL;
    public static final String INVENTORY_INQUIRE = "/" + PageConstants.INVENTORY_INQUIRE;
    public static final String INVENTORY_INQUIRE_RESULT = "/" + PageConstants.INVENTORY_INQUIRE_RESULT;
    public static final String INVENTORY_INQUIRE_RESULT_DETAIL = "/" + PageConstants.INVENTORY_INQUIRE_RESULT_DETAIL;
    public static final String CONTAINER_CONFIRM = "/" + PageConstants.CONTAINER_CONFIRM;
    public static final String SHELF_BOX = "/" + PageConstants.SHELF_BOX;
    public static final String CONTAINER_SHELF = "/" + PageConstants.CONTAINER_SHELF;
    public static final String MOVE_OUT_MUL_GOODS = "/" + PageConstants.MOVE_OUT_MUL_GOODS;
    public static final String RF_INVENTORY_LIST = "/" + PageConstants.RF_INVENTORY_LIST;
    public static final String MOVE_IN_CONFIRM = "/" + PageConstants.MOVE_IN_CONFIRM;

    public static final String PARCEL_TASK_LIST = "/" + PageConstants.PARCEL_TASK_LIST;
    public static final String INVENTORY = "/" + PageConstants.INVENTORY;

    public static final String MOVE_LOCATION_SELECT_LOCATION = "/" + PageConstants.MOVE_LOCATION_SELECT_LOCATION;
    public static final String MAWB_LIST = "/" + PageConstants.MAWB_LIST;
    public static final String INVENTORY_CONVERSION = "/" + PageConstants.INVENTORY_CONVERSION;
    public static final String CONVERSION_TASK = "/" + PageConstants.CONVERSION_TASK;
    public static final String CONVERSION_MOVE_IN_CONTAINER = "/" + PageConstants.CONVERSION_MOVE_IN_CONTAINER;

    public static final String MOVE_OUT = "/" + PageConstants.MOVE_OUT;
    public static final String MOVE_OUT_GOODS_LIST = "/" + PageConstants.MOVE_OUT_GOODS_LIST;
    public static final String SPACE_TRANSFER = "/" + PageConstants.SPACE_TRANSFER;
    public static final String HAWB_BINDING = "/" + PageConstants.HAWB_BINDING;

    public static final String SPARE_PARTS_MOVE_IN = "/" + PageConstants.SPARE_PARTS_MOVE_IN;
    public static final String SPARE_PARTS_MOVE_OUT = "/" + PageConstants.SPARE_PARTS_MOVE_OUT;
}