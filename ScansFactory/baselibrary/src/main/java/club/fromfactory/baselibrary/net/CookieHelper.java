package club.fromfactory.baselibrary.net;

import android.content.Context;
import android.os.Build;
import android.os.Build.VERSION;
import android.webkit.CookieManager;

import com.blankj.utilcode.util.LogUtils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import club.fromfactory.baselibrary.BuildConfig;
import club.fromfactory.baselibrary.language.LanguageUtils;
import club.fromfactory.baselibrary.utils.AppUtils;
import club.fromfactory.baselibrary.utils.DateUtil;
import club.fromfactory.baselibrary.utils.DeviceUtils;
import club.fromfactory.baselibrary.utils.NetWorkUtils;
import club.fromfactory.baselibrary.utils.ScreenUtils;
import club.fromfactory.baselibrary.utils.StringUtils;

/**
 * 管理cookie工具类
 *
 * @author zhoulei  2017/6/5.
 */
public class CookieHelper {

    private static final String COOKIE_KEY_ANDROID_ID = "android_id";
    private static final String COOKIE_KEY_OS_VERSION = "os_version";


    private static final String COOKIE_KEY_SOURCE = "source";
    public static final String COOKIE_KEY_LANGUAGE = "language";
    public static final String COOKIE_KEY_COUNTRY_NAME = "country_name";
    private static final String COOKIE_KEY_TIME_ZONE = "time_zone";
    private static final String COOKIE_KEY_MODEL = "model";
    private static final String COOKIE_KEY_VERSION_NAME = "v";
    private static final String COOKIE_KEY_IS_NEW_CUSTOMER = "isNew_customer";
    private static final String COOKIE_KEY_NETWORK_SPEED = "network_speed";
    private static final String COOKIE_KEY_NETWORK_TYPE = "network_type";
    public static final String COOKIE_KEY_UID = "uid";
    public static final String COOKIE_KEY_USER_UID = "r_uid";
    private static final String COOKIE_KEY_SCREEN_WIDTH = "screen_width";
    private static final String COOKIE_KEY_SCREEN_HEIGHT = "screen_height";


    private static Map<String, Object> getBaseCookieString() {

        Map<String, Object> cookieMap = new HashMap<>();
        String language = LanguageUtils.getLanguageCode();
        cookieMap.put("language", language);
        cookieMap.put("version_name", AppUtils.getRealVersionName());
        cookieMap.put("version_code", BuildConfig.VERSION_CODE);
        String currentTimeZone = DateUtil.INSTANCE.getCurrentTimeZone();
        cookieMap.put(COOKIE_KEY_TIME_ZONE, currentTimeZone.substring(currentTimeZone.length()-5));
        cookieMap.put(COOKIE_KEY_ANDROID_ID, DeviceUtils.getAndroidId());
        cookieMap.put(CookieHelper.COOKIE_KEY_OS_VERSION, "Android_" + VERSION.RELEASE);
        return cookieMap;
    }

    public static String getBaseCookieString(String str) {
        Map<String, Object> baseCookieString = getBaseCookieString();
        if (StringUtils.isNotBlank(str)) {
            try {
                String[] cookieListArr = str.split(";");
                for (int i = 0; i < cookieListArr.length; i++) {
                    String cookie = cookieListArr[i];
                    String[] cookeArra = cookie.split("=");
                    baseCookieString.put(cookeArra[0],cookeArra[1]);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return getCookieString(baseCookieString);
    }

    public static String getCookieString(Map<String, Object> cookieMap) {


        StringBuilder cookieHeader = new StringBuilder();

        Iterator<String> iterator = cookieMap.keySet().iterator();


        while (iterator.hasNext()) {

            String next = iterator.next();
            cookieHeader.append(next).append('=').append(cookieMap.get(next));

            if (iterator.hasNext()) {
                cookieHeader.append(";");
            }
        }
        return cookieHeader.toString();
    }

    //    //本地的cookie
    public static void setCookie(String cookie) {
//        setCookieToWebView(NetUtils.APP_MAIN_URL, cookie);
    }

    public static void setCookie(String cookieKey, String cookieValue) {
//        setCookieToWebView(NetUtils.APP_MAIN_URL, cookieKey, cookieValue);
    }

//    private static void setCookieToWebView(String url, String cookie) {
//        try {
//            CookieManager cookieManager = CookieManager.getInstance();
//            cookieManager.setAcceptCookie(true);
//            cookieManager.setCookie(url, cookie);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    private static void setCookieToWebView(String url, String cookieKey, String cookieValue) {
//        setCookieToWebView(url, cookieKey + "=" + cookieValue);
//    }

    public static String getCookieString() {
        String cookie = null;
        try {
            cookie = CookieManager.getInstance().getCookie(NetUtils.APP_MAIN_URL);
            // fix crash https://fabric.io/jiayunshuju/android/apps/club.fromfactory/issues/5c92f929f8b88c2963e0cd5a
        } catch (Throwable e) {
        }

        if (cookie == null) {
            cookie = "";
        }
        return cookie;
    }

    public static String getCookieString(String cookie, String key) {
        try {
            if (cookie != null && cookie.length() > 0) {
                String[] array = cookie.split(";");
                for (String cook : array) {
                    if (cook != null && cook.trim().startsWith(key + "=")) {
                        String[] cookieKeyArr = cook.split("=");
                        return cookieKeyArr[1];
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    public static String getCookieString(String cookieKey) {
        return getCookieString(getCookieString(), cookieKey);
    }

    public static void clearCookie() {
        CookieManager cookieManager = CookieManager.getInstance();
        if (VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            cookieManager.removeAllCookies(null);
        } else {
            cookieManager.removeAllCookie();
        }
    }

    /***
     * 获取Cookie里的uid
     * @return
     */
    public static String getUidNew() {
        String uid = getCookieString(COOKIE_KEY_UID);
        //去除双引号
        if (StringUtils.isNotBlank(uid)) {
            if (uid.contains("\"")) {
                uid = uid.replaceAll("\"", "");
            }
        } else {
            uid = "0";
        }

        return uid;
    }

    public static long getUserUid() {
        String uid = getCookieString(COOKIE_KEY_USER_UID);
        //去除双引号
        if (StringUtils.isNotBlank(uid)) {
            if (uid.contains("\"")) {
                uid = uid.replaceAll("\"", "");
            }
        } else {
            uid = "0";
        }

        try {
            return Long.parseLong(uid);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        return 0;
    }

    public static String getCountryName(String cookie) {
        String country_name = getCookieString(COOKIE_KEY_COUNTRY_NAME);
        return StringUtils.isNotBlank(country_name) ? country_name : "0";
    }

    public static void syncSourceCookie(String cookie) {
        CookieHelper.setCookie(CookieHelper.COOKIE_KEY_SOURCE, cookie);
    }

    /**
     * 同步系统版本cookie
     */
    public static void syncOsVersionCookie() {
        String osVersion = VERSION.RELEASE;
        CookieHelper.setCookie(CookieHelper.COOKIE_KEY_OS_VERSION, "Android_" + osVersion);
    }

    /**
     * 同步屏幕宽度cookie
     */
    public static void syncScreenWidthCookie() {
        int screenWidth = ScreenUtils.getScreenWidth();
        CookieHelper.setCookie(CookieHelper.COOKIE_KEY_SCREEN_WIDTH, String.valueOf(screenWidth));
    }

    /**
     * 同步屏幕高度cookie
     */
    public static void syncScreenHeightCookie() {
        int screenHeight = ScreenUtils.getScreenHeight();
        CookieHelper.setCookie(CookieHelper.COOKIE_KEY_SCREEN_HEIGHT, String.valueOf(screenHeight));
    }

    public static void syncLanguageCookie() {
        String language = LanguageUtils.getLanguageCode();
        String languageCookie = LanguageUtils.getLanguageCookie(language);
        CookieHelper.setCookie(COOKIE_KEY_LANGUAGE, languageCookie);
    }

    public static void syncLanguageCookie(String language) {
        String languageCookie = LanguageUtils.getLanguageCookie(language);
        CookieHelper.setCookie(COOKIE_KEY_LANGUAGE, languageCookie);
    }

    public static void syncAndroidIdCookie() {
        String mAndroidId = DeviceUtils.getAndroidId();
        CookieHelper.setCookie(COOKIE_KEY_ANDROID_ID, mAndroidId);
    }

    public static void syncModelCookie() {
        CookieHelper.setCookie(COOKIE_KEY_MODEL, DeviceUtils.getPhoneModel()); //3.8.0 加
    }

    public static void syncTimeZoneCookie() {
        String currentTimeZone = DateUtil.INSTANCE.getCurrentTimeZone();
        CookieHelper.setCookie(COOKIE_KEY_TIME_ZONE, currentTimeZone);
    }

    public static void syncVersionCookie() {
        try {
            String versionName = AppUtils.getRealVersionName();
            CookieHelper.setCookie(COOKIE_KEY_VERSION_NAME, "android_" + versionName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 设置网络速度
     */
    public static void syncNetworkSpeed(long networkSpeed) {
        CookieHelper.setCookie(COOKIE_KEY_NETWORK_SPEED, String.valueOf(networkSpeed));
    }

    /**
     * 设置网络类型
     */
    public static void syncNetworkType(Context context) {
        String networkType = NetWorkUtils.getNetWorkType(context);
        CookieHelper.setCookie(COOKIE_KEY_NETWORK_TYPE, networkType);
    }


    public static String getCustomerCookie() {
        return CookieHelper.getCookieString(CookieHelper.COOKIE_KEY_IS_NEW_CUSTOMER);
    }

    public static void flush() {
        // fix crash https://fabric.io/jiayunshuju/android/apps/club.fromfactory/issues/5b900d466007d59fcdce682c
        try {
            // 手动将cookie的值刷新到存储中，防止cookie丢失
            if (VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                CookieManager.getInstance().flush();
            }
        } catch (Exception e) {
        }
    }
}



