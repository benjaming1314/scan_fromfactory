package club.fromfactory.baselibrary.pattern.loadmore

import club.fromfactory.baselibrary.model.BaseResponse
import club.fromfactory.baselibrary.model.START_PAGE_NO
import club.fromfactory.baselibrary.presenter.BasePresenter
import club.fromfactory.baselibrary.rx.PagingObserver
import club.fromfactory.baselibrary.utils.ToastUtils
import io.reactivex.Observable

/**
 * 具有加载更多功能的Presenter
 * @author 王佳斌
 * @date 2018/10/23
 */
abstract class LoadMorePresenter<T>(v: LoadMoreContract.View<T>) : BasePresenter<
        LoadMoreContract.View<T>>(v), LoadMoreContract.Presenter<T> {
    /**
     * 接下来需要请求的页面序号
     */
    protected var pageNumber = START_PAGE_NO

    /**
     * 是否第一次获取数据，第一次请求是自动请求的，之后的都是下拉刷新或者分页加载触发的，不需要另外显示加载视图
     */
    private var isFirstFetchData = true

    override fun getData() {
        if (isFirstFetchData) {
            view.showLoadingView()
        }
        pageNumber = START_PAGE_NO
        realGetData()
    }

    override fun getMoreData() {
        pageNumber++
        realGetData()
    }

    /**
     * 真的网络请求数据
     */
    abstract fun realGetData()

    /**
     * 设置数据
     */
    fun setData(t: List<T>?) {
        t?.run {
            if (pageNumber == START_PAGE_NO) {
                view.showData(this)
            } else {
                view.showMoreData(this)
            }
        }
    }

    /**
     * 网络请求失败的处理
     */
    fun onRequestFailure(message: String) {
        ToastUtils.show(message)
        if (pageNumber > 1) {
            pageNumber--
        }
        if (!isFirstFetchData) {
            if (pageNumber == START_PAGE_NO) {
                view.onRefreshFailed()
            } else {
                view.onLoadMoreFailed()
            }
        }

    }

    private fun onRequestSucceed(t: List<T>?) {
        setData(t)
        if (!isFirstFetchData) {
            if (pageNumber == START_PAGE_NO) {
                view.onRefreshSucceed()
            } else {
                view.onLoadMoreSucceed()
            }
        }
    }


    inner class LoadMoreObserver : PagingObserver<List<T>?>() {

        override fun onNoMoreData() {
            view.onNoMoreData()
        }

        override fun onSuccess(t: List<T>?) {
            onRequestSucceed(t)
        }

        override fun onFailure(message: String) {
            onRequestFailure(message)
        }

        override fun onAfter() {
            super.onAfter()
            if (isFirstFetchData) {
                isFirstFetchData = false
                view.hideLoadingView()
            }
        }

    }

    /**
     * 转换body的类型 方便使用[LoadMoreObserver]
     */
    fun <O> Observable<BaseResponse<O>>.mapType(mapper: (O?) -> List<T>?):
            Observable<BaseResponse<List<T>?>> {
        return this.map {
            val data = mapper.invoke(it.model)
            (it as BaseResponse<List<T>?>).run {
                it.model = data
            }
            (it as BaseResponse<List<T>?>)
        }
    }

}