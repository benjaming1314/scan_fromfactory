package club.fromfactory.baselibrary.utils;

import android.content.Context;
import android.content.SharedPreferences;
import club.fromfactory.baselibrary.BaseApplication;

// 键值对的存储方式
public class PreferenceUtils {

    private static Context context = BaseApplication.instance;
    private volatile SharedPreferences mPref;
    private static final String SHARE_NAME = "itemSpref"; // yunsj_club
    private static final String WEB_CACHE_NAME = "webcache"; // web资源缓存
    private static final String WEB_CACHE_TIMEOUT = "timeout"; // 缓存超时时间

    private static volatile PreferenceUtils timeoutInstance;
    private static volatile PreferenceUtils instance;
    private static volatile PreferenceUtils webCacheInstance;

    /**
     * 获取默认的SP
     */
    public static PreferenceUtils getInstance() {
        if (instance == null) {
            synchronized (PreferenceUtils.class) {
                if (instance == null) {
                    instance = new PreferenceUtils();
                    instance.setConfigName(SHARE_NAME);
                }
            }
        }
        return instance;
    }

    /**
     * 超时时间的SP
     */
    public static PreferenceUtils getTimeoutInstance() {
        if (timeoutInstance == null) {
            synchronized (PreferenceUtils.class) {
                if (timeoutInstance == null) {
                    timeoutInstance = new PreferenceUtils();
                    timeoutInstance.setConfigName(WEB_CACHE_TIMEOUT);
                }
            }
        }

        return timeoutInstance;
    }

    /**
     * 获取资源缓存的SP
     */
    public static PreferenceUtils getWebCacheInstance() {
        if (webCacheInstance == null) {
            synchronized (PreferenceUtils.class) {
                if (webCacheInstance == null) {
                    webCacheInstance = new PreferenceUtils();
                    webCacheInstance.setConfigName(WEB_CACHE_NAME);
                }
            }
        }

        return webCacheInstance;
    }

    /**
     * 获取指定名称的SP
     *
     * @param configName 文件名
     */
    public static PreferenceUtils getInstance(String configName) {
        if (instance == null) {
            instance = new PreferenceUtils();
            instance.setConfigName(configName);
        }
        return instance;
    }


    // 设置配置文件的名字
    public synchronized void setConfigName(String configname) {
        mPref = context.getSharedPreferences(configname, Context.MODE_PRIVATE);
    }

    public boolean isContains(String key) {
        return mPref.contains(key);
    }

    // 获取int类型的值
    public int getIntValue(String key, int defaultValue) {
        return mPref.getInt(key, defaultValue);
    }

    // 获取string类型的值
    public String getStringValue(String key, String defaultValue) {
        return mPref.getString(key, defaultValue);
    }

    // 获取string类型的值
    public String getStringValue(String key) {
        return mPref.getString(key, "");
    }

    // 获取float类型的值
    public float getFloatValue(String key, float defaultValue) {
        return mPref.getFloat(key, defaultValue);
    }

    // 获取long类型的值
    public long getLongValue(String key, long defaultValue) {
        return mPref.getLong(key, defaultValue);
    }

    // 获取Boolean类型的值
    public Boolean getBooleanValue(String key, boolean defaultValue) {
        return mPref.getBoolean(key, defaultValue);
    }

    // 设置Boolean类型的值到配置文件
    public void setIntValue(String key, int value) {
        mPref.edit().putInt(key, value).apply();
    }

    // 设置String类型的值到配置文件
    public void setStringValue(String key, String value) {
        mPref.edit().putString(key, value).apply();
    }

    // 设置float类型的值到配置文件
    public void setFloatValue(String key, float value) {
        mPref.edit().putFloat(key, value).apply();
    }

    // 设置long类型的值到配置文件
    public void setLongValue(String key, long value) {
        mPref.edit().putLong(key, value).apply();
    }

    // 设置boolean类型的值到配置文件
    public void setBooleanValue(String key, boolean value) {
        // fix crash https://fabric.io/jiayunshuju/android/apps/club.fromfactory/issues/5be4f49df8b88c2963248da7
        try {
            mPref.edit().putBoolean(key, value).apply();
        } catch (Exception e) {
            ExceptionUtilsKt.logException(e);
        }
    }

    public void clearData(String key) {
        mPref.edit().remove(key).apply();
    }

    public void clearAll() {
        mPref.edit().clear().apply();
    }

}
