package club.fromfactory.baselibrary.utils


import android.annotation.SuppressLint
import android.net.ParseException
import com.blankj.utilcode.util.LogUtils
import java.text.SimpleDateFormat
import java.util.*


/**
 * 时间格式化
 *
 * @author nichenjian
 * @date 2018-10-18
 */
@SuppressLint("SimpleDateFormat")
object DateUtil {

    const val YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss"
    private const val YYYY_MM_DD_HH_MM = "yyyy-MM-dd HH:mm"
    private const val YYYY_MM_DD = "yyyy-MM-dd"
    const val YYYY_MM_DD_T_Z = "yyyy-MM-dd'T'HH:mm:ss'Z'"

    private const val SECONDS_MINUTE = 60
    private const val SECONDS_HOUR = 60 * 60
    /**
     * 获取当前时区
     */
    val currentTimeZone: String
        get() {
            try {
                val tz = TimeZone.getDefault()
                LogUtils.eTag("lxm",tz.getDisplayName(false, TimeZone.SHORT))
                return tz.getDisplayName(false, TimeZone.SHORT)
            } catch (e: Exception) {
            }

            return ""
        }

    @JvmOverloads
    fun long2string(date1: Long, formatStr1: String = DateUtil.YYYY_MM_DD_HH_MM): String {
        val d1 = Date(date1)

        val sdf1 = SimpleDateFormat(formatStr1)
        return sdf1.format(d1)
    }

    fun getCurrentUtcTime(dateTime: Long = System.currentTimeMillis()): String {

        val localFormater = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        localFormater.timeZone = TimeZone.getDefault()
        return localFormater.format(Date(dateTime))

    }

    fun utc2Local(utcTime: String = getCurrentUtcTime()): String {
        val utcFormater = SimpleDateFormat("yyyy-MM-dd HH:mm")//UTC时间格式
        utcFormater.timeZone = TimeZone.getTimeZone("UTC")
        var gpsUTCDate: Date? = null
        try {
            gpsUTCDate = utcFormater.parse(utcTime)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        val localFormater = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")//当地时间格式
        localFormater.timeZone = TimeZone.getDefault()
        val localTime = localFormater.format(gpsUTCDate?.time)
        return localTime
    }

    fun utc2Local(utcTime: String = getCurrentUtcTime(),utcFormatStr: String = YYYY_MM_DD_T_Z ,localFormatStr: String = YYYY_MM_DD_HH_MM_SS): String {
        val utcFormater = SimpleDateFormat(utcFormatStr)//UTC时间格式
        utcFormater.timeZone = TimeZone.getTimeZone("UTC")
        var gpsUTCDate: Date? = null
        try {
            gpsUTCDate = utcFormater.parse(utcTime)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        val localFormater = SimpleDateFormat(localFormatStr)//当地时间格式
        localFormater.timeZone = TimeZone.getDefault()
        val localTime = localFormater.format(gpsUTCDate?.time)
        return localTime
    }

    fun getDateMills(date1: String, formatStr1: String): Long {

        val df2 = SimpleDateFormat(formatStr1)
        val date = df2.parse(date1)

        return date.time

    }

    /**
     * 获取两个时间戳的间隔天数
     *
     * @param time1 时间戳1
     * @param time2 时间戳2
     * @return days 间隔天数
     */
    private fun getDays(time1: Long, time2: Long): Int {
        val format = SimpleDateFormat(YYYY_MM_DD)
        val date1 = Date(time1)
        val date2 = Date(time2)
        val day1 = format.format(date1)
        val day2 = format.format(date2)

        return Math.abs((format.parse(day1).time - format.parse(day2).time) / (1000 * 3600 * 24)).toInt()
    }
}
