package club.fromfactory.baselibrary.net.retrofit.cache.converter

import club.fromfactory.baselibrary.utils.CloseableUtils
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.InputStream
import java.io.InputStreamReader
import java.io.OutputStream
import java.lang.reflect.Type

/**
 * gson转换器
 *
 * @author nichenjian
 * @date 2018/6/26
 */

object GsonConverter : IConverter {
    val gson = Gson()

    override fun <T> read(source: InputStream, type: Type): T? {
        var value: T? = null
        try {
            val adapter = gson.getAdapter(TypeToken.get(type))
            val jsonReader = gson.newJsonReader(InputStreamReader(source))
            value = adapter.read(jsonReader) as T
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            CloseableUtils.close(source)
        }
        return value
    }

    override fun <T> write(stream: OutputStream, data: T): Boolean {
        try {
            val json = gson.toJson(data)
            val bytes = json.toByteArray()
            stream.write(bytes, 0, bytes.size)
            stream.flush()
            return true
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            CloseableUtils.close(stream)
        }

        return false
    }
}
