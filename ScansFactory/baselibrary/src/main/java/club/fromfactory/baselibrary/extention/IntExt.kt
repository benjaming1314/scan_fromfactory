package club.fromfactory.baselibrary.extention

/**
 * Int的扩展
 * @author 王佳斌
 * @date 2018/12/4
 */

fun Int.biggerThan(number: Int): Boolean {
    return this > number
}
