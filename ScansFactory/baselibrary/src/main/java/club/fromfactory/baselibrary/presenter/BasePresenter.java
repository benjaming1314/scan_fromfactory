package club.fromfactory.baselibrary.presenter;

import android.support.annotation.NonNull;

import club.fromfactory.baselibrary.view.IMVPView;

/**
 * Presenter 基类
 *
 * @author Jellybean
 * @date 2018/6/1
 */
public abstract class BasePresenter<V extends IMVPView> implements IPresenter<V> {

    protected V view;
    /**
     * 是否已经记录了网络结束时间
     */
    private boolean loggedNetWork;

    public BasePresenter(@NonNull V view) {
        this.view = view;
    }


    @Override
    public void unsubscribe() {

    }

    /**
     * 记录网络结束
     */
    protected void logNetwork() {
        if (!loggedNetWork) {
            view.getPagePerformanceRecorder().setNetworkEndTimed();
            loggedNetWork = true;
        }
    }
}