package club.fromfactory.baselibrary.language

import club.fromfactory.baselibrary.model.NoProguard

/**
 * Created by lxm on 2018/3/28.
 */

class LanguageInfo(var languageCode: String? = null,
                   var languageStr: String? = null,
                   var languageCookie: String? = null,
                   var isCurrentSelect: Boolean = false) : NoProguard
