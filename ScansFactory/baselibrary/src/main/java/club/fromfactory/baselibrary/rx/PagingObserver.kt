package club.fromfactory.baselibrary.rx


/**
 *  分页数据的观察者
 *
 * @author Jellybean
 * @date 2018/6/21
 */
abstract class PagingObserver<T> : NetObserver<T>(), IPagingObserver {
    override fun onFailureInternal(code: Int, message: String) {
        super<IPagingObserver>.onFailureInternal(code, message)
    }
}