package club.fromfactory.baselibrary.statistic

import club.fromfactory.baselibrary.model.EmptyResponse
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * native stat service interface
 * Created by nichenjian on 2018/5/21.
 */
const val NATIVE = "native"
const val TRACE = "trace"

interface TrackApi {
    /**
     * 上传native业务日志的接口
     */
    @Deprecated("")
    @POST(NATIVE)
    fun traceNativeStat(@Body params: Map<String, String>): Observable<EmptyResponse>

    /**
     * 上传native业务日志的接口
     */
    @POST(TRACE)
    fun postNativeStat(@Body params: Map<String, @JvmSuppressWildcards Any>): Observable<EmptyResponse>

    @POST("http://47.110.43.217//event")
    fun postStatForDebug(@Body params: Map<String, @JvmSuppressWildcards Any>): Observable<Unit>
}